package eu.eudat.queryable.jpa.predicates;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public interface SinglePredicate<T> {
    Predicate applyPredicate(CriteriaBuilder builder, Root<T> root);
}
