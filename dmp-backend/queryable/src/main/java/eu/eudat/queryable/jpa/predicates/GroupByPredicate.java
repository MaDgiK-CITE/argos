package eu.eudat.queryable.jpa.predicates;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;

public interface GroupByPredicate<T> {
    Expression<T> applyPredicate(CriteriaBuilder builder, Root<T> root);

}
