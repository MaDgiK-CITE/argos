package eu.eudat.queryable.types;

/**
 * Created by ikalyvas on 2/7/2018.
 */
public enum FieldSelectionType {
    FIELD, COMPOSITE_FIELD, COUNT, MAX
}
