package eu.eudat.queryable.jpa.predicates;

public interface SelectPredicate<T, R> {
    R applySelection(T item);
}
