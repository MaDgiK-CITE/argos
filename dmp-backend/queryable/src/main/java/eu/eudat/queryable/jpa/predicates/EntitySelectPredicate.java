package eu.eudat.queryable.jpa.predicates;

import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.util.function.Predicate;

/**
 * Created by ikalyvas on 10/10/2018.
 */
public interface EntitySelectPredicate<T> {
    Path applyPredicate(Root<T> root);

}
