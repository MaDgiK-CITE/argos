package eu.eudat.queryable.jpa.predicates;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;

public interface OrderByPredicate<T> {
    Order applyPredicate(CriteriaBuilder builder, Root<T> root);

}
