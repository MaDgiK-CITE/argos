package eu.eudat.queryable.jpa.predicates;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * Created by ikalyvas on 2/7/2018.
 */
public interface NestedQuerySinglePredicate<T> {
    Predicate applyPredicate(CriteriaBuilder builder, Root<T> root, Root<T> nestedQueryRoot);

}
