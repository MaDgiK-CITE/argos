package eu.eudat.queryable.queryableentity;

import javax.persistence.Tuple;
import java.util.List;

public interface DataEntity<T, K> {
	void update(T entity);

	K getKeys();

	T buildFromTuple(List<Tuple> tuple, List<String> fields, String base);
}
