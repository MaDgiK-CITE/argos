package eu.eudat.configurations;

import eu.eudat.controllers.interceptors.RequestInterceptor;
import eu.eudat.logic.handlers.PrincipalArgumentResolver;
import eu.eudat.logic.services.ApiContext;
import eu.eudat.logic.services.operations.authentication.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.List;

@EnableAsync
@Configuration
@EnableScheduling
public class WebMVCConfiguration extends WebMvcConfigurerAdapter {

	private ApiContext apiContext;

	private AuthenticationService verifiedUserAuthenticationService;
	private AuthenticationService nonVerifiedUserAuthenticationService;

	@Autowired
	public WebMVCConfiguration(ApiContext apiContext, AuthenticationService verifiedUserAuthenticationService, AuthenticationService nonVerifiedUserAuthenticationService) {
		this.apiContext = apiContext;
		this.verifiedUserAuthenticationService = verifiedUserAuthenticationService;
		this.nonVerifiedUserAuthenticationService = nonVerifiedUserAuthenticationService;
	}

	@Autowired
	@Override
	public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
		argumentResolvers.add(new PrincipalArgumentResolver(verifiedUserAuthenticationService, nonVerifiedUserAuthenticationService));
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
//		registry.addInterceptor(new RequestInterceptor(this.apiContext.getHelpersService().getLoggerService()));
	}
}
