package eu.eudat.configurations.dynamicproject;

import eu.eudat.configurations.dynamicproject.entities.Configuration;
import eu.eudat.models.data.dynamicfields.DynamicField;

import java.util.List;

public interface DynamicProjectConfiguration {
	Configuration getConfiguration();

	List<DynamicField> getFields();
}
