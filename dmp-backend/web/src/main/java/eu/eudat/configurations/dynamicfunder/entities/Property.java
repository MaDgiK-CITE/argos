package eu.eudat.configurations.dynamicfunder.entities;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.List;

public class Property {
    private String id;
    private String name;
    private String sourceUrl;
    private String queryProperty;
    private String externalFieldId;
    private String externalFieldLabel;
    private List<Dependency> dependencies;
    private Boolean required;


    public String getId() {
        return id;
    }

    @XmlElement(name = "id")
    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    @XmlElement(name = "name")
    public void setName(String name) {
        this.name = name;
    }

    public String getSourceUrl() {
        return sourceUrl;
    }

    @XmlElement(name = "sourceUrl")
    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }

    public String getExternalFieldId() {
        return externalFieldId;
    }

    @XmlElement(name = "externalFieldId")
    public void setExternalFieldId(String externalFieldId) {
        this.externalFieldId = externalFieldId;
    }

    public String getExternalFieldLabel() {
        return externalFieldLabel;
    }

    @XmlElement(name = "externalFieldLabel")
    public void setExternalFieldLabel(String externalFieldLabel) {
        this.externalFieldLabel = externalFieldLabel;
    }

    public List<Dependency> getDependencies() {
        return dependencies;
    }

    @XmlElementWrapper
    @XmlElement(name = "dependency")
    public void setDependencies(List<Dependency> dependencies) {
        this.dependencies = dependencies;
    }

    public Boolean getRequired() {
        return required;
    }

    @XmlElement(name = "required")
    public void setRequired(Boolean required) {
        this.required = required;
    }

    public String getQueryProperty() {
        return queryProperty;
    }

    @XmlElement(name = "queryProperty")
    public void setQueryProperty(String queryProperty) {
        this.queryProperty = queryProperty;
    }
}
