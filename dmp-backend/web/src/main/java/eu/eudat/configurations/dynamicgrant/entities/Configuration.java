package eu.eudat.configurations.dynamicgrant.entities;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by ikalyvas on 3/23/2018.
 */
@XmlRootElement
public class Configuration {
    private List<Property> configurationProperties;
    private MainProperty mainExternalField;

    public MainProperty getMainExternalField() {
        return mainExternalField;
    }

    @XmlElement(name = "mainExternalField")
    public void setMainExternalField(MainProperty mainExternalField) {
        this.mainExternalField = mainExternalField;
    }

    public List<Property> getConfigurationProperties() {
        return configurationProperties;
    }

    @XmlElementWrapper
    @XmlElement(name = "property")
    public void setConfigurationProperties(List<Property> configurationProperties) {
        this.configurationProperties = configurationProperties;
    }
}
