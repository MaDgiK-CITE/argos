package eu.eudat.configurations.dynamicproject.entities;

import eu.eudat.logic.proxy.config.UrlConfiguration;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.List;

public class MainProperty {
    private String id;
    private String name;
    private String queryProperty;
    private String externalFieldId;
    private List<UrlConfiguration> urlConfig;
    private String externalFieldLabel;
    private List<Dependency> dependencies;
    private Boolean required;
    private List<Language> language;


    public String getId() {
        return id;
    }

    @XmlElement(name = "id")
    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    @XmlElement(name = "name")
    public void setName(String name) {
        this.name = name;
    }

    public String getExternalFieldId() {
        return externalFieldId;
    }

    @XmlElement(name = "externalFieldId")
    public void setExternalFieldId(String externalFieldId) {
        this.externalFieldId = externalFieldId;
    }

    public String getExternalFieldLabel() {
        return externalFieldLabel;
    }

    @XmlElement(name = "externalFieldLabel")
    public void setExternalFieldLabel(String externalFieldLabel) {
        this.externalFieldLabel = externalFieldLabel;
    }

    public List<Dependency> getDependencies() {
        return dependencies;
    }

    @XmlElementWrapper
    @XmlElement(name = "dependency")
    public void setDependencies(List<Dependency> dependencies) {
        this.dependencies = dependencies;
    }

    public Boolean getRequired() {
        return required;
    }

    @XmlElement(name = "required")
    public void setRequired(Boolean required) {
        this.required = required;
    }

    public String getQueryProperty() {
        return queryProperty;
    }

    @XmlElement(name = "queryProperty")
    public void setQueryProperty(String queryProperty) {
        this.queryProperty = queryProperty;
    }

    public List<UrlConfiguration> getUrlConfig() {
        return urlConfig;
    }

    @XmlElement(name = "urlConfig")
    public void setUrlConfig(List<UrlConfiguration> urlConfig) {
        this.urlConfig = urlConfig;
    }

    public List<Language> getLanguage() {
        return language;
    }

    @XmlElementWrapper
    @XmlElement(name = "languageProperty")
    public void setLanguage(List<Language> language) {
        this.language = language;
    }
}
