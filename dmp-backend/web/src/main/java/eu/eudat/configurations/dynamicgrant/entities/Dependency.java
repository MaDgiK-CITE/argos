package eu.eudat.configurations.dynamicgrant.entities;

import javax.xml.bind.annotation.XmlElement;

/**
 * Created by ikalyvas on 3/23/2018.
 */
public class Dependency {
    private String id;
    private String queryProperty;

    public String getId() {
        return id;
    }

    public String getQueryProperty() {
        return queryProperty;
    }

    @XmlElement(name = "id")
    public void setId(String id) {
        this.id = id;
    }

    @XmlElement(name = "queryProperty")
    public void setQueryProperty(String queryProperty) {
        this.queryProperty = queryProperty;
    }
}
