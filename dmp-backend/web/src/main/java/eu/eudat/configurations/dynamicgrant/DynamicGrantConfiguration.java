package eu.eudat.configurations.dynamicgrant;

import eu.eudat.configurations.dynamicgrant.entities.Configuration;
import eu.eudat.models.data.dynamicfields.DynamicField;

import java.util.List;

/**
 * Created by ikalyvas on 3/23/2018.
 */
public interface DynamicGrantConfiguration {
    Configuration getConfiguration();

    List<DynamicField> getFields();
}
