package eu.eudat.configurations.dynamicfunder;

import eu.eudat.configurations.dynamicfunder.entities.Configuration;
import eu.eudat.models.data.dynamicfields.DynamicField;

import java.util.List;

public interface DynamicFunderConfiguration {
	Configuration getConfiguration();

	List<DynamicField> getFields();
}
