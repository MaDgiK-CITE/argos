package eu.eudat.configurations.dynamicproject.entities;

import javax.xml.bind.annotation.XmlElement;

public class Language {
    private String key;
    private String languageKey;

    public String getKey() {
        return key;
    }

    @XmlElement(name = "key")
    public void setKey(String key) {
        this.key = key;
    }

    public String getLanguageKey() {
        return languageKey;
    }

    @XmlElement(name = "languageKey")
    public void setLanguageKey(String languageKey) {
        this.languageKey = languageKey;
    }
}
