package eu.eudat.configurations.dynamicproject.entities;

import javax.xml.bind.annotation.XmlElement;

public class Dependency {
    private String id;
    private String queryProperty;

    public String getId() {
        return id;
    }

    public String getQueryProperty() {
        return queryProperty;
    }

    @XmlElement(name = "id")
    public void setId(String id) {
        this.id = id;
    }

    @XmlElement(name = "queryProperty")
    public void setQueryProperty(String queryProperty) {
        this.queryProperty = queryProperty;
    }
}
