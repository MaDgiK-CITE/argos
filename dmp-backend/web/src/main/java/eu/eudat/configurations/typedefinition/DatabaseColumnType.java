package eu.eudat.configurations.typedefinition;

public interface DatabaseColumnType {
    public String getType(DataType dt);
}
