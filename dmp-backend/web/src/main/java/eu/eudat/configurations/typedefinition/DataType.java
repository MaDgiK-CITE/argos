package eu.eudat.configurations.typedefinition;

public enum DataType {
    TINY,
    SHORT,
    INTEGER,
    LONG,
    DOUBLE,
    FLOAT,
    DATE,
    STRING,
    TEXT
}
