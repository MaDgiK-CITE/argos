package eu.eudat.configurations.dynamicgrant.entities;

import javax.xml.bind.annotation.XmlElement;

/**
 * Created by ikalyvas on 3/28/2018.
 */
public class Language {
    private String key;
    private String languageKey;

    public String getKey() {
        return key;
    }

    @XmlElement(name = "key")
    public void setKey(String key) {
        this.key = key;
    }

    public String getLanguageKey() {
        return languageKey;
    }

    @XmlElement(name = "languageKey")
    public void setLanguageKey(String languageKey) {
        this.languageKey = languageKey;
    }
}
