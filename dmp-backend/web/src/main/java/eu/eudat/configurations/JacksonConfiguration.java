package eu.eudat.configurations;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import eu.eudat.criteria.entities.Criteria;
import eu.eudat.criteria.serialzier.CriteriaSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;

@Configuration
public class JacksonConfiguration {
	@Bean
	public ObjectMapper buildObjectMapper() {

		ArrayList<Module> modules = new ArrayList<>();
		SimpleModule criteriaSerializerModule = new SimpleModule();
		criteriaSerializerModule.addDeserializer(Criteria.class, new CriteriaSerializer());
		modules.add(criteriaSerializerModule);

		return new ObjectMapper()
				.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
				.registerModules(modules);
	}
}
