package eu.eudat.publicapi.models.datasetprofile;

import eu.eudat.data.entities.DescriptionTemplate;
import eu.eudat.models.DataModel;

import java.util.UUID;

public class DatasetProfilePublicModel implements DataModel<DescriptionTemplate, DatasetProfilePublicModel>  {
    private UUID id;
    private String label;

    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public DatasetProfilePublicModel fromDataModel(DescriptionTemplate entity) {
        this.id = entity.getId();
        this.label = entity.getLabel();
        return this;
    }

    @Override
    public DescriptionTemplate toDataModel() {
        DescriptionTemplate entity = new DescriptionTemplate();
        entity.setId(this.getId());
        entity.setLabel(this.getLabel());
        return entity;
    }

    @Override
    public String getHint() {
        return null;
    }
}
