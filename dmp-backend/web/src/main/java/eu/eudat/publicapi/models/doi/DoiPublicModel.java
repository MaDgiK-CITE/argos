package eu.eudat.publicapi.models.doi;

import eu.eudat.data.entities.EntityDoi;
import eu.eudat.logic.utilities.helpers.LabelGenerator;
import eu.eudat.models.DataModel;

import java.util.UUID;

public class DoiPublicModel implements DataModel<EntityDoi, DoiPublicModel>, LabelGenerator {
    private UUID id;
    private String repositoryId;
    private String doi;

    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }

    public String getRepositoryId() {
        return repositoryId;
    }
    public void setRepositoryId(String repositoryId) {
        this.repositoryId = repositoryId;
    }

    public String getDoi() {
        return doi;
    }
    public void setDoi(String doi) {
        this.doi = doi;
    }

    @Override
    public DoiPublicModel fromDataModel(EntityDoi entity) {
        this.id = entity.getId();
        this.repositoryId = entity.getRepositoryId();
        this.doi = entity.getDoi();
        return this;
    }

    @Override
    public EntityDoi toDataModel() throws Exception {
        EntityDoi entity = new EntityDoi();
        entity.setId(this.getId());
        entity.setRepositoryId(this.getRepositoryId());
        entity.setDoi(this.getDoi());
        return entity;
    }

    @Override
    public String generateLabel() {
        return this.getDoi();
    }

    @Override
    public String getHint() {
        return null;
    }
}
