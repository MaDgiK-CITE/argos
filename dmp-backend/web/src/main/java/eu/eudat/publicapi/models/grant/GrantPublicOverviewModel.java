package eu.eudat.publicapi.models.grant;

import eu.eudat.data.entities.Grant;
import eu.eudat.models.DataModel;
import eu.eudat.publicapi.models.funder.FunderPublicOverviewModel;

import java.util.Date;
import java.util.UUID;

public class GrantPublicOverviewModel implements DataModel<Grant, GrantPublicOverviewModel> {
    private UUID id;
    private String label;
    private String abbreviation;
    private String description;
    private Date startDate;
    private Date endDate;
    private String uri;
    private FunderPublicOverviewModel funder;

    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }

    public String getAbbreviation() {
        return abbreviation;
    }
    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getUri() {
        return uri;
    }
    public void setUri(String uri) {
        this.uri = uri;
    }

    public FunderPublicOverviewModel getFunder() {
        return funder;
    }
    public void setFunder(FunderPublicOverviewModel funder) {
        this.funder = funder;
    }

    @Override
    public GrantPublicOverviewModel fromDataModel(Grant entity) {
        this.id = entity.getId();
        this.label = entity.getLabel();
        this.abbreviation = entity.getAbbreviation();
        this.description = entity.getDescription();
        this.startDate = entity.getStartdate();
        this.endDate = entity.getEnddate();
        this.uri = entity.getUri();
        this.funder = new FunderPublicOverviewModel();
        if (entity.getFunder() != null)
            this.funder.fromDataModel(entity.getFunder());
        return this;
    }

    @Override
    public Grant toDataModel() throws Exception {
        return null;
    }

    @Override
    public String getHint() {
        return null;
    }
}
