package eu.eudat.publicapi.models.listingmodels;

import eu.eudat.data.entities.DMP;
import eu.eudat.data.entities.Grant;
import eu.eudat.models.DataModel;
import eu.eudat.publicapi.models.researcher.ResearcherPublicModel;
import eu.eudat.publicapi.models.user.UserInfoPublicModel;

import java.util.*;
import java.util.stream.Collectors;

public class DataManagementPlanPublicListingModel implements DataModel<DMP, DataManagementPlanPublicListingModel> {
    private String id;
    private String label;
    private String grant;
    private Date createdAt;
    private Date modifiedAt;
    private int version;
    private UUID groupId;
    private List<UserInfoPublicModel> users;
    private List<ResearcherPublicModel> researchers;
    private Date finalizedAt;
    private Date publishedAt;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }

    public String getGrant() {
        return grant;
    }
    public void setGrant(String grant) {
        this.grant = grant;
    }

    public Date getCreatedAt() {
        return createdAt;
    }
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getModifiedAt() {
        return modifiedAt;
    }
    public void setModifiedAt(Date modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    public int getVersion() {
        return version;
    }
    public void setVersion(int version) {
        this.version = version;
    }

    public UUID getGroupId() {
        return groupId;
    }
    public void setGroupId(UUID groupId) {
        this.groupId = groupId;
    }

    public List<UserInfoPublicModel> getUsers() {
        return users;
    }
    public void setUsers(List<UserInfoPublicModel> users) {
        this.users = users;
    }

    public List<ResearcherPublicModel> getResearchers() {
        return researchers;
    }
    public void setResearchers(List<ResearcherPublicModel> researchers) {
        this.researchers = researchers;
    }

    public Date getFinalizedAt() {
        return finalizedAt;
    }
    public void setFinalizedAt(Date finalizedAt) {
        this.finalizedAt = finalizedAt;
    }

    public Date getPublishedAt() {
        return publishedAt;
    }
    public void setPublishedAt(Date publishedAt) {
        this.publishedAt = publishedAt;
    }

    @Override
    public DataManagementPlanPublicListingModel fromDataModel(DMP entity) {
        this.id = entity.getId().toString();
        this.label = entity.getLabel();
        this.groupId = entity.getGroupId();
        return this;
    }

    public DataManagementPlanPublicListingModel fromDataModelAssociatedProfiles(DMP entity) {
        this.id = entity.getId().toString();
        this.label = entity.getLabel();
        this.groupId = entity.getGroupId();
        this.createdAt = entity.getCreated();
        return this;
    }

    public DataManagementPlanPublicListingModel fromDataModelAutoComplete(DMP entity) {
        this.id = entity.getId().toString();
        this.label = entity.getLabel();
        this.groupId = entity.getGroupId();
        this.createdAt = entity.getCreated();
        return this;
    }

    public DataManagementPlanPublicListingModel fromDataModelNoDatasets(DMP entity) {
        this.fromDataModel(entity);
        this.version = entity.getVersion();
        if (entity.getGrant() != null) {
            this.grant = entity.getGrant().getLabel();
        }
        this.createdAt = entity.getCreated();
        this.modifiedAt = entity.getModified();
        try {
            this.users = entity.getUsers() != null ? entity.getUsers().stream().map(x -> new UserInfoPublicModel().fromDataModel(x)).collect(Collectors.toList()) : new ArrayList<>();
            this.researchers = entity.getResearchers() != null ? entity.getResearchers().stream().map(x -> new ResearcherPublicModel().fromDataModel(x)).collect(Collectors.toList()) : new ArrayList<>();
        }
        catch(Exception ex){
            this.users = new ArrayList<>();
            this.researchers = new ArrayList<>();
        }
        this.finalizedAt = entity.getFinalizedAt();
        this.publishedAt = entity.getPublishedAt();

        return this;
    }

    @Override
    public DMP toDataModel() {
        DMP entity = new DMP();
        entity.setId(UUID.fromString(this.getId()));
        entity.setLabel(this.getLabel());
        entity.setGroupId(this.getGroupId());
        entity.setCreated(this.getCreatedAt());
        entity.setFinalizedAt(this.getFinalizedAt());
        entity.setModified(this.getModifiedAt());
        entity.setPublishedAt(this.getPublishedAt());
        entity.setVersion(this.getVersion());

        if (this.getGrant() != null) {
            Grant grant = new Grant();
            grant.setLabel(this.getGrant());
            entity.setGrant(grant);
        }
        entity.setUsers(this.getUsers().stream().map(UserInfoPublicModel::toDataModel).collect(Collectors.toSet()));
        entity.setResearchers(this.getResearchers().stream().map(ResearcherPublicModel::toDataModel).collect(Collectors.toSet()));
        return entity;
    }

    @Override
    public String getHint() {
        return "fullyDetailed";
    }
}
