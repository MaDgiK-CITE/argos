package eu.eudat.publicapi.models.overviewmodels;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.eudat.data.entities.DMP;
import eu.eudat.data.entities.DMPDatasetProfile;
import eu.eudat.data.entities.Dataset;
import eu.eudat.data.entities.DescriptionTemplate;
import eu.eudat.logic.utilities.builders.XmlBuilder;
import eu.eudat.models.DataModel;
import eu.eudat.models.data.dmp.AssociatedProfile;
import eu.eudat.models.data.user.composite.PagedDatasetProfile;
import eu.eudat.publicapi.models.associatedprofile.AssociatedProfilePublicModel;
import eu.eudat.publicapi.models.doi.DoiPublicModel;
import eu.eudat.publicapi.models.grant.GrantPublicOverviewModel;
import eu.eudat.publicapi.models.organisation.OrganizationPublicModel;
import eu.eudat.publicapi.models.researcher.ResearcherPublicModel;
import eu.eudat.publicapi.models.user.UserInfoPublicModel;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.util.*;
import java.util.stream.Collectors;

public class DataManagementPlanPublicModel implements DataModel<DMP, DataManagementPlanPublicModel> {
    private String id;
    private String label;
    private String profile;
    private GrantPublicOverviewModel grant;
    private Date createdAt;
    private Date modifiedAt;
    private Date finalizedAt;
    private List<OrganizationPublicModel> organisations;
    private int version;
    private UUID groupId;
    private List<DatasetPublicModel> datasets;
    private List<AssociatedProfilePublicModel> associatedProfiles;
    private List<ResearcherPublicModel> researchers;
    private List<UserInfoPublicModel> users;
    private String description;
    private Date publishedAt;
    private List<DoiPublicModel> dois;


    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }

    public String getProfile() {
        return profile;
    }
    public void setProfile(String profile) {
        this.profile = profile;
    }

    public GrantPublicOverviewModel getGrant() {
        return grant;
    }
    public void setGrant(GrantPublicOverviewModel grant) {
        this.grant = grant;
    }

    public Date getCreatedAt() {
        return createdAt;
    }
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getModifiedAt() {
        return modifiedAt;
    }
    public void setModifiedAt(Date modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    public Date getFinalizedAt() {
        return finalizedAt;
    }
    public void setFinalizedAt(Date finalizedAt) {
        this.finalizedAt = finalizedAt;
    }

    public List<OrganizationPublicModel> getOrganisations() {
        return organisations;
    }
    public void setOrganisations(List<OrganizationPublicModel> organizations) {
        this.organisations = organizations;
    }

    public int getVersion() {
        return version;
    }
    public void setVersion(int version) {
        this.version = version;
    }

    public UUID getGroupId() {
        return groupId;
    }
    public void setGroupId(UUID groupId) {
        this.groupId = groupId;
    }

    public List<DatasetPublicModel> getDatasets() {
        return datasets;
    }
    public void setDatasets(List<DatasetPublicModel> datasets) {
        this.datasets = datasets;
    }

    public List<AssociatedProfilePublicModel> getAssociatedProfiles() {
        return associatedProfiles;
    }
    public void setAssociatedProfiles(List<AssociatedProfilePublicModel> associatedProfiles) {
        this.associatedProfiles = associatedProfiles;
    }

    public List<UserInfoPublicModel> getUsers() {
        return users;
    }
    public void setUsers(List<UserInfoPublicModel> users) {
        this.users = users;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public List<ResearcherPublicModel> getResearchers() {
        return researchers;
    }
    public void setResearchers(List<ResearcherPublicModel> researchers) {
        this.researchers = researchers;
    }

    public Date getPublishedAt() {
        return publishedAt;
    }
    public void setPublishedAt(Date publishedAt) {
        this.publishedAt = publishedAt;
    }

    public List<DoiPublicModel> getDois() {
        return dois;
    }
    public void setDois(List<DoiPublicModel> dois) {
        this.dois = dois;
    }

    @Override
    public DataManagementPlanPublicModel fromDataModel(DMP entity) {
        this.id = entity.getId().toString();
        this.label = entity.getLabel();
        this.groupId = entity.getGroupId();
        if (entity.getResearchers() != null) {
            this.researchers = entity.getResearchers().stream().map(item -> new ResearcherPublicModel().fromDataModel(item)).collect(Collectors.toList());
        }
        return this;
    }

    public DataManagementPlanPublicModel fromDataModelDatasets(DMP entity) {
        this.fromDataModel(entity);
        this.version = entity.getVersion();
        if (entity.getGrant() != null) {
            this.grant = new GrantPublicOverviewModel().fromDataModel(entity.getGrant());
        }
        if (entity.getProfile() != null) this.profile = entity.getProfile().getLabel();
        this.createdAt = entity.getCreated();
        this.modifiedAt = entity.getModified();
        this.finalizedAt = entity.getFinalizedAt();
        this.organisations = entity.getOrganisations().stream().map(item -> new OrganizationPublicModel().fromDataModel(item)).collect(Collectors.toList());
        this.datasets = entity.getDataset().stream().filter(dataset -> !dataset.getStatus().equals(Dataset.Status.DELETED.getValue()) && !dataset.getStatus().equals(Dataset.Status.CANCELED.getValue()))
                .map(datasetEntity-> {
                    DatasetPublicModel dataset = new DatasetPublicModel();
                    dataset.setDatasetProfileDefinition(this.getPagedProfile(dataset.getStatus(), datasetEntity));
                    dataset.fromDataModel(datasetEntity);
                    return dataset;
                }).collect(Collectors.toList());
        this.users = entity.getUsers().stream().map(x -> new UserInfoPublicModel().fromDataModel(x)).collect(Collectors.toList());
        this.description = entity.getDescription();
        if (entity.getResearchers() != null) {
            this.researchers = entity.getResearchers().stream().map(item -> new ResearcherPublicModel().fromDataModel(item)).collect(Collectors.toList());
        }

        if (entity.getAssociatedDmps() != null && !entity.getAssociatedDmps().isEmpty()) {
            this.associatedProfiles = new LinkedList<>();
            for (DMPDatasetProfile dmpDescriptionProfile : entity.getAssociatedDmps()) {
                AssociatedProfilePublicModel associatedProfile = new AssociatedProfilePublicModel().fromData(dmpDescriptionProfile.getDatasetprofile());
                associatedProfile.setId(dmpDescriptionProfile.getId());
                try {
                    associatedProfile.setData(new ObjectMapper().readValue(dmpDescriptionProfile.getData(), new TypeReference<Map<String, Object>>() {}));
                }
                catch (Exception e) {
                    associatedProfile.setData(null);
                }
                this.associatedProfiles.add(associatedProfile);
            }
        }
        this.publishedAt = entity.getPublishedAt();
        this.dois = entity.getDois().stream().map(item -> new DoiPublicModel().fromDataModel(item)).collect(Collectors.toList());

        return this;
    }

    private PagedDatasetProfile getPagedProfile(int status, Dataset datasetEntity){
        eu.eudat.models.data.user.composite.DatasetProfile datasetprofile = this.generateDatasetProfileModel(datasetEntity.getProfile());
        datasetprofile.setStatus(status);
        if (datasetEntity.getProperties() != null) {
            JSONObject jObject = new JSONObject(datasetEntity.getProperties());
            Map<String, Object> properties = jObject.toMap();
            datasetprofile.fromJsonObject(properties);
        }
        PagedDatasetProfile pagedDatasetProfile = new PagedDatasetProfile();
        pagedDatasetProfile.buildPagedDatasetProfile(datasetprofile);
        return pagedDatasetProfile;
    }

    private eu.eudat.models.data.user.composite.DatasetProfile generateDatasetProfileModel(DescriptionTemplate profile) {
        Document viewStyleDoc = XmlBuilder.fromXml(profile.getDefinition());
        Element root = (Element) viewStyleDoc.getDocumentElement();
        eu.eudat.models.data.entities.xmlmodels.datasetprofiledefinition.ViewStyleModel viewstyle = new eu.eudat.models.data.entities.xmlmodels.datasetprofiledefinition.ViewStyleModel().fromXml(root);

        eu.eudat.models.data.user.composite.DatasetProfile datasetprofile = new eu.eudat.models.data.user.composite.DatasetProfile();
        datasetprofile.buildProfile(viewstyle);

        return datasetprofile;
    }

    @Override
    public DMP toDataModel() {
        return null;
    }

    @Override
    public String getHint() {
        return "dataManagementPlanOverviewModel";
    }
}
