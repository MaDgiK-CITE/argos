package eu.eudat.publicapi.models.datasetwizard;

import eu.eudat.data.entities.Service;
import eu.eudat.logic.utilities.helpers.LabelGenerator;
import eu.eudat.models.DataModel;

import java.util.Date;
import java.util.UUID;

public class ServicePublicModel implements DataModel<Service, ServicePublicModel>, LabelGenerator {
    private UUID id;
    private String label;
    private String abbreviation;
    private String reference;
    private String uri;
    private String definition;
    private String source;

    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }

    public String getAbbreviation() {
        return abbreviation;
    }
    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getReference() {
        return reference;
    }
    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getUri() {
        return uri;
    }
    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getDefinition() {
        return definition;
    }
    public void setDefinition(String definition) {
        this.definition = definition;
    }

    public String getSource() {
        return source;
    }
    public void setSource(String source) {
        this.source = source;
    }

    public ServicePublicModel fromDataModel(Service entity) {
        this.id = entity.getId();
        this.label = entity.getLabel();
        this.abbreviation = entity.getAbbreviation();
        this.reference = entity.getReference();
        this.uri = entity.getUri();
        this.definition = entity.getDefinition();
        String source1 = entity.getReference().substring(0, entity.getReference().indexOf(":"));
        if (source1.equals("dmp")) {
            this.source = "Internal";
        } else {
            this.source = source1;
        }
        return this;
    }

    public Service toDataModel() {
        Service entity = new Service();
        entity.setId(this.id != null ? this.id : UUID.randomUUID());
        entity.setLabel(this.label);
        entity.setAbbreviation(this.abbreviation);
        if (this.source != null && this.source.equals("Internal")) this.source = "dmp";
        if (this.reference != null && !this.reference.trim().isEmpty()
                && this.source != null && !this.source.trim().isEmpty()) {
            if (this.source.equals(this.reference.substring(0, this.source.length()))) {
                entity.setReference(this.reference);
            } else {
                entity.setReference(this.source.toLowerCase() + ":" + this.reference);
            }
        }
        entity.setUri(this.uri);
        entity.setModified(new Date());
        entity.setStatus((short)0);
        return entity;
    }

    @Override
    public String generateLabel() {
        return this.label;
    }

    @Override
    public String getHint() {
        return null;
    }
}
