package eu.eudat.publicapi.models.listingmodels;

import eu.eudat.data.entities.DMP;
import eu.eudat.data.entities.Dataset;
import eu.eudat.data.entities.Grant;
import eu.eudat.models.DataModel;
import eu.eudat.publicapi.models.datasetprofile.DatasetProfilePublicModel;
import eu.eudat.publicapi.models.user.UserInfoPublicModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class DatasetPublicListingModel implements DataModel<Dataset, DatasetPublicListingModel> {
    private String id;
    private String label;
    private String grant;
    private String dmp;
    private String dmpId;
    private DatasetProfilePublicModel profile;
    private Date createdAt;
    private Date modifiedAt;
    private String description;
    private Date finalizedAt;
    private Date dmpPublishedAt;
    private int version;
    private List<UserInfoPublicModel> users;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }

    public String getGrant() {
        return grant;
    }
    public void setGrant(String grant) {
        this.grant = grant;
    }

    public String getDmp() {
        return dmp;
    }
    public void setDmp(String dmp) {
        this.dmp = dmp;
    }

    public String getDmpId() {
        return dmpId;
    }
    public void setDmpId(String dmpId) {
        this.dmpId = dmpId;
    }

    public DatasetProfilePublicModel getProfile() {
        return profile;
    }
    public void setProfile(DatasetProfilePublicModel profile) {
        this.profile = profile;
    }

    public Date getCreatedAt() {
        return createdAt;
    }
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getModifiedAt() {
        return modifiedAt;
    }
    public void setModifiedAt(Date modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public Date getFinalizedAt() {
        return finalizedAt;
    }
    public void setFinalizedAt(Date finalizedAt) {
        this.finalizedAt = finalizedAt;
    }

    public Date getDmpPublishedAt() {
        return dmpPublishedAt;
    }
    public void setDmpPublishedAt(Date dmpPublishedAt) {
        this.dmpPublishedAt = dmpPublishedAt;
    }

    public int getVersion() {
        return version;
    }
    public void setVersion(int version) {
        this.version = version;
    }

    public List<UserInfoPublicModel> getUsers() {
        return users;
    }

    public void setUsers(List<UserInfoPublicModel> users) {
        this.users = users;
    }

    @Override
    public DatasetPublicListingModel fromDataModel(Dataset entity) {
        this.id = entity.getId() != null ? entity.getId().toString() : "";
        this.label = entity.getLabel();
        this.createdAt = entity.getCreated();
        this.modifiedAt = entity.getModified();
        if(entity.getDmp() != null && entity.getDmp().getGrant() != null) {
            this.grant = entity.getDmp().getGrant().getLabel();
        }
        this.dmp = entity.getDmp() != null ? entity.getDmp().getLabel() : "";
        this.dmpId = entity.getDmp() != null ? entity.getDmp().getId().toString() : "";
        this.profile = entity.getProfile() != null ? new DatasetProfilePublicModel().fromDataModel(entity.getProfile()) : null;
        this.description = entity.getDescription();
        if (entity.getFinalizedAt() == null && entity.getStatus() == Dataset.Status.FINALISED.getValue()) {
            this.finalizedAt = entity.getDmp().getFinalizedAt();
        } else {
            this.finalizedAt = entity.getFinalizedAt();
        }
        this.dmpPublishedAt = entity.getDmp().getPublishedAt();
        this.version = entity.getDmp().getVersion();
        this.users = entity.getDmp() != null ? entity.getDmp().getUsers().stream().map(x -> new UserInfoPublicModel().fromDataModel(x)).collect(Collectors.toList()) : new ArrayList<>();
        return this;
    }

    @Override
    public Dataset toDataModel() {
        Dataset entity = new Dataset();
        entity.setId(UUID.fromString(this.getId()));
        entity.setLabel(this.getLabel());
        entity.setCreated(this.getCreatedAt());
        entity.setModified(this.getModifiedAt());
        entity.setDescription(this.getDescription());
        entity.setFinalizedAt(this.getFinalizedAt());
        entity.setStatus(Dataset.Status.FINALISED.getValue());
        DMP dmp = new DMP();
        if (this.getGrant() != null && !this.getGrant().isEmpty()) {
            Grant grant = new Grant();
            grant.setLabel(this.getGrant());
            dmp.setGrant(grant);
        }
        dmp.setLabel(this.getDmp());
        dmp.setId(UUID.fromString(this.getDmpId()));
        dmp.setPublishedAt(this.getDmpPublishedAt());
        dmp.setVersion(this.getVersion());
        dmp.setUsers(this.getUsers().stream().map(UserInfoPublicModel::toDataModel).collect(Collectors.toSet()));
        dmp.setFinalizedAt(this.getFinalizedAt());
        entity.setDmp(dmp);
        entity.setProfile(this.getProfile() != null ? this.getProfile().toDataModel() : null);
        return entity;
    }

    @Override
    public String getHint() {
        return "datasetListingModel";
    }
}
