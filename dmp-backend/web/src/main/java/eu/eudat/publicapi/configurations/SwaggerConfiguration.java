package eu.eudat.publicapi.configurations;

import eu.eudat.models.data.security.Principal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.Collections;
import java.util.HashSet;

@Configuration
public class SwaggerConfiguration {

   // private static final TypeResolver resolver = new TypeResolver();

    @Autowired
    private Environment environment;

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .host(this.environment.getProperty("publicapi.host"))
                .protocols(new HashSet<>(Collections.singleton(this.environment.getProperty("publicapi.schema"))))
                .select()
                .apis(RequestHandlerSelectors.basePackage("eu.eudat.publicapi.controllers"))
                .paths(PathSelectors.regex("/api/public/(dmps|datasets)/?.*"))
                .build().apiInfo(apiInfo())//.ignoredParameterTypes(Principal.class)
                .pathMapping(this.environment.getProperty("publicapi.basePath"))
                .useDefaultResponseMessages(false);
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                "OpenDMP public API",
                "Argos public API.",
                "1.0",
                "https://argos.openaire.eu/terms-and-conditions",
                new Contact("Argos", "https://argos.openaire.eu/", "argos@openaire.eu "),
                null, null, Collections.emptyList());
    }
}
