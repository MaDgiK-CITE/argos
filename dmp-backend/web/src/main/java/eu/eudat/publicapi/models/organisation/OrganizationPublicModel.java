package eu.eudat.publicapi.models.organisation;

import eu.eudat.data.entities.Organisation;
import eu.eudat.logic.utilities.helpers.LabelGenerator;
import eu.eudat.models.DataModel;

import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

public class OrganizationPublicModel implements DataModel<Organisation, OrganizationPublicModel>, LabelGenerator  {
    private String label;
    private String name;
    private String id;
    private String reference;
    private int status;
    private String tag; // how the external source is displayed. ex: "Cristin".
    private String key; // the external source. ex: "cristin".

    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getReference() {
        return reference;
    }
    public void setReference(String reference) {
        this.reference = reference;
    }

    public int getStatus() {
        return status;
    }
    public void setStatus(int status) {
        this.status = status;
    }

    public String getTag() {
        return tag;
    }
    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getKey() {
        return key;
    }
    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public OrganizationPublicModel fromDataModel(Organisation entity) {
        this.id = entity.getId().toString();
        this.name = entity.getLabel();
        this.label = entity.getUri();
        if (entity.getReference() != null) {
            this.reference = entity.getReference();
            this.key = entity.getReference().substring(0, entity.getReference().indexOf(":"));
        }
        return this;
    }

    @Override
    public Organisation toDataModel() {
        Organisation organisationEntity = new Organisation();
        if (this.key != null && this.reference != null) {
            if (this.reference.startsWith(this.key + ":")) {
                organisationEntity.setReference(this.reference);
            } else {
                organisationEntity.setReference(this.key + ":" + this.reference);
            }
        }
        if (this.id != null) {
            organisationEntity.setId(UUID.fromString(this.id));
        }
        organisationEntity.setLabel(this.name);
        organisationEntity.setUri(this.label);
        organisationEntity.setCreated(new Date());
        organisationEntity.setStatus((short) this.status);
        return organisationEntity;
    }

    public static OrganizationPublicModel fromMap(HashMap<String, Object> map) {
        OrganizationPublicModel model = new OrganizationPublicModel();
        if (map != null) {
            model.id = (String) map.get("id");
            model.key = (String) map.get("key");
            model.label = (String) map.get("label");
            model.name = (String) map.get("name");
            model.reference = (String) map.get("reference");
            model.status = (int) map.get("status");
            model.tag = (String) map.get("tag");
        }
        return model;
    }

    @Override
    public String generateLabel() {
        return this.getName();
    }

    @Override
    public String getHint() {
        return null;
    }
}
