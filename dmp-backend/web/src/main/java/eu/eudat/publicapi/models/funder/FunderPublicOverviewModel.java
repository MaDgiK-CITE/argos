package eu.eudat.publicapi.models.funder;

import eu.eudat.data.entities.Funder;
import eu.eudat.models.DataModel;

import java.util.UUID;

public class FunderPublicOverviewModel implements DataModel<Funder, FunderPublicOverviewModel> {
    private UUID id;
    private String label;

    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public FunderPublicOverviewModel fromDataModel(Funder entity) {
        this.id = entity.getId();
        this.label = entity.getLabel();
        return this;
    }

    @Override
    public Funder toDataModel() throws Exception {
        return null;
    }

    @Override
    public String getHint() {
        return null;
    }
}
