package eu.eudat.exceptions.datasetprofile;

public class DatasetProfileWithDatasetsExeption extends RuntimeException  {

    public DatasetProfileWithDatasetsExeption() {
    }

    public DatasetProfileWithDatasetsExeption(String message) {
        super(message);
    }

    public DatasetProfileWithDatasetsExeption(String message, Throwable cause) {
        super(message, cause);
    }

    public DatasetProfileWithDatasetsExeption(Throwable cause) {
        super(cause);
    }
}
