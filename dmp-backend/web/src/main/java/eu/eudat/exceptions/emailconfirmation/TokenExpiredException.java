package eu.eudat.exceptions.emailconfirmation;

public class TokenExpiredException extends Exception {

	public TokenExpiredException(String msg) {
		super(msg);
	}
}
