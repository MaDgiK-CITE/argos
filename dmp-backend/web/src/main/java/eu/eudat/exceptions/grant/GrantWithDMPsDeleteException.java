package eu.eudat.exceptions.grant;

/**
 * Created by ikalyvas on 11/30/2018.
 */
public class GrantWithDMPsDeleteException extends RuntimeException {
    public GrantWithDMPsDeleteException() {
        super();
    }

    public GrantWithDMPsDeleteException(String message, Throwable cause) {
        super(message, cause);
    }

    public GrantWithDMPsDeleteException(String message) {
        super(message);
    }

    public GrantWithDMPsDeleteException(Throwable cause) {
        super(cause);
    }
}