package eu.eudat.exceptions.dmpblueprint;

public class DmpBlueprintUsedException extends RuntimeException {
    public DmpBlueprintUsedException() {
    }

    public DmpBlueprintUsedException(String message) {
        super(message);
    }

    public DmpBlueprintUsedException(String message, Throwable cause) {
        super(message, cause);
    }

    public DmpBlueprintUsedException(Throwable cause) {
        super(cause);
    }
}
