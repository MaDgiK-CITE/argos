package eu.eudat.exceptions.datamanagementplan;

public class DMPNewVersionException extends RuntimeException {

    public DMPNewVersionException(String message) {
        super(message);
    }
}
