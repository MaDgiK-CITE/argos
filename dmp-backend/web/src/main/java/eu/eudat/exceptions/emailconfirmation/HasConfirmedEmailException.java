package eu.eudat.exceptions.emailconfirmation;

public class HasConfirmedEmailException extends Exception {

	public HasConfirmedEmailException(String msg) {
		super(msg);
	}
}
