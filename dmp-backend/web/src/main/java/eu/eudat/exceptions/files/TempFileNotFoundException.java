package eu.eudat.exceptions.files;

/**
 * Created by ikalyvas on 3/16/2018.
 */
public class TempFileNotFoundException extends RuntimeException {
    public TempFileNotFoundException() {
        super();
    }

    public TempFileNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public TempFileNotFoundException(String message) {
        super(message);
    }

    public TempFileNotFoundException(Throwable cause) {
        super(cause);
    }
}