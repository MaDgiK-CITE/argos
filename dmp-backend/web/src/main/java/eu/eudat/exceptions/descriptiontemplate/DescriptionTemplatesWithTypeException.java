package eu.eudat.exceptions.descriptiontemplate;

public class DescriptionTemplatesWithTypeException extends RuntimeException {

    public DescriptionTemplatesWithTypeException(String message) {
        super(message);
    }

}
