package eu.eudat.exceptions.datasetwizard;

/**
 * Created by ikalyvas on 10/12/2018.
 */
public class DatasetWizardCannotUnlockException extends RuntimeException {
    public DatasetWizardCannotUnlockException() {
        super();
    }

    public DatasetWizardCannotUnlockException(String message, Throwable cause) {
        super(message, cause);
    }

    public DatasetWizardCannotUnlockException(String message) {
        super(message);
    }

    public DatasetWizardCannotUnlockException(Throwable cause) {
        super(cause);
    }
}