package eu.eudat.exceptions.datamanagementplan;

/**
 * Created by ikalyvas on 2/5/2018.
 */
public class DMPWithDatasetsDeleteException extends RuntimeException {
    public DMPWithDatasetsDeleteException() {
        super();
    }

    public DMPWithDatasetsDeleteException(String message, Throwable cause) {
        super(message, cause);
    }

    public DMPWithDatasetsDeleteException(String message) {
        super(message);
    }

    public DMPWithDatasetsDeleteException(Throwable cause) {
        super(cause);
    }
}
