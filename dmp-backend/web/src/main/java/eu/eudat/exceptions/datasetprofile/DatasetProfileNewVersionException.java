package eu.eudat.exceptions.datasetprofile;

public class DatasetProfileNewVersionException extends RuntimeException {

    public DatasetProfileNewVersionException(String message) {
        super(message);
    }

}
