package eu.eudat.exceptions.security;

public class ExpiredTokenException extends Exception {

	public ExpiredTokenException(String message) {
		super(message);
	}
}
