package eu.eudat.types;

/**
 * Created by ikalyvas on 2/8/2018.
 */
public enum Rights {
    CAN_EDIT_ROLES(0), CAN_INVITE_USERS(1), CAN_CREATE_DMP(2);

    private Integer value;

    private Rights(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }

    public static Rights fromInteger(Integer value) {
        switch (value) {
            case 0:
                return CAN_EDIT_ROLES;
            case 1:
                return CAN_INVITE_USERS;
            case 2:
                return CAN_CREATE_DMP;
            default:
                throw new RuntimeException("Unsupported Rights Type");
        }
    }
}
