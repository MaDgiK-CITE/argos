package eu.eudat.types;

public class MetricNames {
    public static final String DATASET_TEMPLATE = "argos_dmp_templates";
    public static final String INSTALLATIONS = "installations";
    public static final String USERS = "argos_users";
    public static final String DMP = "argos_managed_dmps";
    public static final String DATASET = "argos_managed_dataset_descriptions";
    public static final String RESEARCHER = "argos_researchers";
    public static final String PROJECT = "argos_projects";
    public static final String FUNDERS = "argos_funders";
    public static final String GRANTS = "argos_grants";
    public static final String LANGUAGES = "argos_languages";
    public static final String DMP_WITH_GRANT = "argos_managed_dmps_with_grantid";
    public static final String DRAFT = "draft";
    public static final String FINALIZED = "finalized";
    public static final String PUBLISHED = "published";
    public static final String DOIED = "doied";
    public static final String ACTIVE = "active";
    public static final String USED = "used";
    public static final String LOGGEDIN = "loggedin";
    public static final String TOTAL = "total";
    public static final String NEXUS = "nexus_";
}
