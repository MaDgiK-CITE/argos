package eu.eudat.types.searchbar;

/**
 * Created by ikalyvas on 7/26/2018.
 */
public enum SearchBarItemType {
    DATASET(0), DMP(1), GRANT(2);

    private Integer value;

    private SearchBarItemType(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }

    public static SearchBarItemType fromInteger(Integer value) {
        switch (value) {
            case 0:
                return DATASET;
            case 1:
                return DMP;
            case 2:
                return GRANT;
            default:
                throw new RuntimeException("Unsupported Search bar item Code");
        }
    }

}
