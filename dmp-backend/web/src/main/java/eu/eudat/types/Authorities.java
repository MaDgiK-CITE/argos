package eu.eudat.types;

import java.util.Arrays;
import java.util.List;


public enum Authorities {
    USER(0), MANAGER(1), ADMIN(2), DATASET_PROFILE_MANAGER(3), ANONYMOUS(99);

    private Integer value;

    private Authorities(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }

    public static Authorities fromInteger(Integer value) {
        switch (value) {
            case 0:
                return USER;
            case 1:
                return MANAGER;
            case 2:
                return ADMIN;
            case 3:
                return DATASET_PROFILE_MANAGER;
            case 99:
                return ANONYMOUS;
            default:
                throw new RuntimeException("Unsupported Authority Type");
        }
    }

    public static List<Authorities> all() {
        return Arrays.asList(USER, ADMIN, MANAGER, DATASET_PROFILE_MANAGER);
    }
}
