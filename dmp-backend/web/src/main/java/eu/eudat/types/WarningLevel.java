package eu.eudat.types;

/**
 * Created by ikalyvas on 3/1/2018.
 */
public enum WarningLevel {
    INFO, WARN, DEBUG, ERROR
}
