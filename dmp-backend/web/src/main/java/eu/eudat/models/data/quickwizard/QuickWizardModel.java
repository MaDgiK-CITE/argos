package eu.eudat.models.data.quickwizard;


public class QuickWizardModel {

    private FunderQuickWizardModel funder;
    private GrantQuickWizardModel grant;
    private ProjectQuickWizardModel project;
    private DmpQuickWizardModel dmp;
    private DatasetQuickWizardModel datasets;
    private int status;

    public FunderQuickWizardModel getFunder() {
        return funder;
    }
    public void setFunder(FunderQuickWizardModel funder) {
        this.funder = funder;
    }

    public GrantQuickWizardModel getGrant() {
        return grant;
    }
    public void setGrant(GrantQuickWizardModel grant) {
        this.grant = grant;
    }

    public ProjectQuickWizardModel getProject() {
        return project;
    }
    public void setProject(ProjectQuickWizardModel project) {
        this.project = project;
    }

    public DmpQuickWizardModel getDmp() {
        return dmp;
    }
    public void setDmp(DmpQuickWizardModel dmp) {
        this.dmp = dmp;
    }

    public DatasetQuickWizardModel getDatasets() {
        return datasets;
    }
    public void setDatasets(DatasetQuickWizardModel datasets) {
        this.datasets = datasets;
    }

    public int getStatus() {
        return status;
    }
    public void setStatus(int status) {
        this.status = status;
    }
}
