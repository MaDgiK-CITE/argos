package eu.eudat.models.data.entities.xmlmodels.datasetprofiledefinition;

import eu.eudat.logic.utilities.builders.XmlBuilder;
import eu.eudat.logic.utilities.interfaces.XmlSerializable;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.LinkedList;
import java.util.List;

public class Section implements DatabaseViewStyleDefinition, XmlSerializable<Section> {
    private String id;
    private int ordinal;
    private boolean defaultVisibility;
    private String numbering;
    private String page;
    private String title;
    private String description;
    private String extendedDescription;
    private List<Section> sections;
    private List<FieldSet> fieldSets;
    private Boolean multiplicity;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(int ordinal) {
        this.ordinal = ordinal;
    }

    public boolean isDefaultVisibility() {
        return defaultVisibility;
    }

    public void setDefaultVisibility(boolean defaultVisibility) {
        this.defaultVisibility = defaultVisibility;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Section> getSections() {
        return sections;
    }

    public void setSections(List<Section> sections) {
        this.sections = sections;
    }

    public List<FieldSet> getFieldSets() {
        return fieldSets;
    }

    public void setFieldSets(List<FieldSet> fieldSets) {
        this.fieldSets = fieldSets;
    }

    public String getExtendedDescription() {
        return extendedDescription;
    }

    public void setExtendedDescription(String extendedDescription) {
        this.extendedDescription = extendedDescription;
    }

    public String getNumbering() {
        return numbering;
    }

    public void setNumbering(String numbering) {
        this.numbering = numbering;
    }

    public Boolean getMultiplicity() {
        return multiplicity;
    }

    public void setMultiplicity(Boolean multiplicity) {
        this.multiplicity = multiplicity;
    }

    @Override
    public Element toXml(Document doc) {
        Element rootElement = doc.createElement("section");
        rootElement.setAttribute("id", this.id);
        rootElement.setAttribute("ordinal", "" + this.ordinal);
        rootElement.setAttribute("defaultVisibility", "" + this.defaultVisibility);
        rootElement.setAttribute("page", "" + this.page);
        rootElement.setAttribute("multiplicity", (this.multiplicity != null ? "" + this.multiplicity : "false"));

        Element description = doc.createElement("description");
        description.setTextContent(this.description);

        Element extendedDescription = doc.createElement("extendedDescription");
        extendedDescription.setTextContent(this.extendedDescription);

        Element numbering = doc.createElement("numbering");
        numbering.setTextContent(this.numbering);

        Element title = doc.createElement("title");
        title.setTextContent(this.title);

        if (sections != null) {
            Element sections = doc.createElement("sections");
            for (Section section : this.sections) {
                section.setNumbering(this.numbering + "." + (this.sections.indexOf(section) + 1));
                sections.appendChild(section.toXml(doc));
            }
            rootElement.appendChild(sections);
        }

        if (this.fieldSets != null) {
            Element formGroups = doc.createElement("fieldSets");
            for (FieldSet fieldSet : this.fieldSets) {
                fieldSet.setNumbering(this.numbering + "." + (this.fieldSets.indexOf(fieldSet) + 1));
                formGroups.appendChild(fieldSet.toXml(doc));
            }
            rootElement.appendChild(formGroups);
        }

        rootElement.appendChild(numbering);
        rootElement.appendChild(title);
        rootElement.appendChild(extendedDescription);
        rootElement.appendChild(description);

        return rootElement;
    }

    @Override
    public Section fromXml(Element element) {

        this.id = element.getAttribute("id");
        this.ordinal = Integer.parseInt(element.getAttribute("ordinal"));
        this.defaultVisibility = Boolean.valueOf(element.getAttribute("defaultVisibility"));
        this.page = element.getAttribute("page");
        this.multiplicity = element.hasAttribute("multiplicity") ? Boolean.valueOf(element.getAttribute("multiplicity")) : false;

        Element description = XmlBuilder.getNodeFromListByTagName(element.getChildNodes(), "description");
        if (description != null) this.description = description.getTextContent();

        Element extendedDescription = XmlBuilder.getNodeFromListByTagName(element.getChildNodes(), "extendedDescription");
        if (extendedDescription != null) this.extendedDescription = extendedDescription.getTextContent();

        Element numbering = XmlBuilder.getNodeFromListByTagName(element.getChildNodes(), "numbering");
        if (numbering != null) this.numbering = numbering.getTextContent();

        Element title = XmlBuilder.getNodeFromListByTagName(element.getChildNodes(), "title");
        if (title != null) this.title = title.getTextContent();

        this.sections = new LinkedList<Section>();
        Element sections = (Element) XmlBuilder.getNodeFromListByTagName(element.getChildNodes(), "sections");
        if (sections != null) {
            NodeList sectionElements = sections.getChildNodes();
            for (int temp = 0; temp < sectionElements.getLength(); temp++) {
                Node sectionElement = sectionElements.item(temp);
                if (sectionElement.getNodeType() == Node.ELEMENT_NODE) {
                    this.sections.add(new Section().fromXml((Element) sectionElement));
                }
            }
        }

        this.fieldSets = new LinkedList<FieldSet>();
        Element fieldGroups = (Element) XmlBuilder.getNodeFromListByTagName(element.getChildNodes(), "fieldSets");

        if (fieldGroups != null) {
            NodeList fieldGroupElements = fieldGroups.getChildNodes();
            for (int temp = 0; temp < fieldGroupElements.getLength(); temp++) {
                Node fieldGroupElement = fieldGroupElements.item(temp);
                if (fieldGroupElement.getNodeType() == Node.ELEMENT_NODE) {
                    this.fieldSets.add(new FieldSet().fromXml((Element) fieldGroupElement));
                }
            }
        }

        return this;
    }


}
