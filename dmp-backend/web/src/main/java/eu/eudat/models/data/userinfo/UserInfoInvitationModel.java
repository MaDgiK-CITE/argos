package eu.eudat.models.data.userinfo;

import eu.eudat.data.entities.UserInfo;
import eu.eudat.models.DataModel;

import java.util.UUID;


public class UserInfoInvitationModel implements DataModel<eu.eudat.data.entities.UserInfo, UserInfoInvitationModel> {
    private UUID id;
    private String email;
    private String name;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public UserInfoInvitationModel fromDataModel(eu.eudat.data.entities.UserInfo entity) {
        this.id = entity.getId();
        this.email = entity.getEmail();
        this.name = entity.getName();
        return this;
    }

    @Override
    public eu.eudat.data.entities.UserInfo toDataModel() {
        eu.eudat.data.entities.UserInfo userInfo = new UserInfo();
        userInfo.setId(this.id);
        userInfo.setEmail(this.email);
        userInfo.setName(this.name);
        return userInfo;
    }

    @Override
    public String getHint() {
        return null;
    }
}
