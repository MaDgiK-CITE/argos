package eu.eudat.models.data.dmp;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "dmp")
public class DmpImportModel {
    private String description;
    private String dmpName;
    private DmpProfileImportModel profile;
    private FunderImportModels funderImportModels;
    private GrantImportModels grantImportModels;
    private ProjectImportModels projectImportModels;
    private List<AssociatedProfileImportModels> profilesImportModels;
    private List<OrganisationImportModel> organisationImportModels;
    private List<ResearcherImportModels> researchersImportModels;
    private List<UserInfoImportModels> associatedUsersImportModels;
    private List<ExtraFieldsImportModels> extraFieldsImportModels;
    private List<DynamicFieldWithValueImportModels> dynamicFieldsImportModels;
    private List<DatasetImportModels> datasetImportModels;
    private String language;
    private Boolean visibility;
    private String publicDate;
    private String costs;

    @XmlElement(name = "description")
    public String getDescriptionImport() {
        return description;
    }
    public void setDescriptionImport(String description) {
        this.description = description;
    }

    @XmlElement(name = "dmpName")
    public String getDmpNameImport() {
        return dmpName;
    }
    public void setDmpNameImport(String dmpName) {
        this.dmpName = dmpName;
    }

    @XmlElement(name = "dmpProfile")
    public DmpProfileImportModel getDmpProfile() { return profile; }
    public void setDmpProfile(DmpProfileImportModel profile) { this.profile = profile; }

    @XmlElement(name = "grant")
    public GrantImportModels getGrantImport() {
        return grantImportModels;
    }
    public void setGrantImport(GrantImportModels grantImportModels) {
        this.grantImportModels = grantImportModels;
    }

    @XmlElement(name = "funder")
    public FunderImportModels getFunderImportModels() {
        return funderImportModels;
    }
    public void setFunderImportModels(FunderImportModels funderImportModels) {
        this.funderImportModels = funderImportModels;
    }

    @XmlElement(name = "project")
    public ProjectImportModels getProjectImportModels() {
        return projectImportModels;
    }
    public void setProjectImportModels(ProjectImportModels projectImportModels) {
        this.projectImportModels = projectImportModels;
    }

    @XmlElementWrapper(name="organisations")
    @XmlElement(name = "organisation")
    public List<OrganisationImportModel> getOrganisationImportModels() {
        return organisationImportModels;
    }
    public void setOrganisationImportModels(List<OrganisationImportModel> organisationImportModels) {
        this.organisationImportModels = organisationImportModels;
    }

    @XmlElementWrapper(name="profiles")
    @XmlElement(name = "profile")
    public List<AssociatedProfileImportModels> getProfilesImportModels() {
        return profilesImportModels;
    }
    public void setProfilesImportModels(List<AssociatedProfileImportModels> profilesImportModels) {
        this.profilesImportModels = profilesImportModels;
    }

    @XmlElementWrapper(name="researchers")
    @XmlElement(name = "researcher")
    public List<ResearcherImportModels> getResearchersImportModels() {
        return researchersImportModels;
    }
    public void setResearchersImportModels(List<ResearcherImportModels> researchersImportModels) {
        this.researchersImportModels = researchersImportModels;
    }

    @XmlElementWrapper(name="UserInfos")
    @XmlElement(name = "UserInfo")
    public List<UserInfoImportModels> getAssociatedUsersImportModels() {
        return associatedUsersImportModels;
    }
    public void setAssociatedUsersImportModels(List<UserInfoImportModels> associatedUsersImportModels) {
        this.associatedUsersImportModels = associatedUsersImportModels;
    }

    @XmlElementWrapper(name="extraFields")
    @XmlElement(name = "extraField")
    public List<ExtraFieldsImportModels> getExtraFieldsImportModels() {
        return extraFieldsImportModels;
    }
    public void setExtraFieldsImportModels(List<ExtraFieldsImportModels> extraFieldsImportModels) {
        this.extraFieldsImportModels = extraFieldsImportModels;
    }

    @XmlElementWrapper(name="dynamicFieldWithValues")
    @XmlElement(name = "dynamicFieldWithValue")
    public List<DynamicFieldWithValueImportModels> getDynamicFieldsImportModels() {
        return dynamicFieldsImportModels;
    }
    public void setDynamicFieldsImportModels(List<DynamicFieldWithValueImportModels> dynamicFieldsImportModels) {
        this.dynamicFieldsImportModels = dynamicFieldsImportModels;
    }

    @XmlElementWrapper(name="datasets")
    @XmlElement(name = "dataset")
    public List<DatasetImportModels> getDatasetImportModels() { return datasetImportModels; }
    public void setDatasetImportModels(List<DatasetImportModels> datasetImportModels) { this.datasetImportModels = datasetImportModels; }

    @XmlElement(name = "language")
    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
    @XmlElement(name = "visibility")
    public Boolean getVisibility() {
        return visibility;
    }

    public void setVisibility(Boolean visibility) {
        this.visibility = visibility;
    }
    @XmlElement(name = "publicDate")
    public String getPublicDate() {
        return publicDate;
    }

    public void setPublicDate(String publicDate) {
        this.publicDate = publicDate;
    }
    @XmlElement(name = "costs")
    public String getCosts() {
        return costs;
    }

    public void setCosts(String costs) {
        this.costs = costs;
    }
}
