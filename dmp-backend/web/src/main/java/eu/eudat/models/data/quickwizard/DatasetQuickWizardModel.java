package eu.eudat.models.data.quickwizard;

import java.util.List;

public class DatasetQuickWizardModel {
    private List<DatasetDescriptionQuickWizardModel> datasetsList;

    public List<DatasetDescriptionQuickWizardModel> getDatasetsList() {
        return datasetsList;
    }

    public void setDatasetsList(List<DatasetDescriptionQuickWizardModel> datasetsList) {
        this.datasetsList = datasetsList;
    }
}
