package eu.eudat.models.data.components.commons.datafield;

import eu.eudat.logic.utilities.interfaces.XmlSerializable;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.util.HashMap;
import java.util.Map;

public abstract class ComboBoxData<T> extends FieldData<T> {
    public static class Option implements XmlSerializable<Option> {
        private String label;
        private String value;
        private String source;
        private String uri;

        public String getLabel() {
            return label;
        }
        public void setLabel(String label) {
            this.label = label;
        }

        public String getValue() {
            return value;
        }
        public void setValue(String value) {
            this.value = value;
        }

        public String getSource() {
            return source;
        }
        public void setSource(String source) {
            this.source = source;
        }

        public String getUri() {
            return uri;
        }

        public void setUri(String uri) {
            this.uri = uri;
        }

        @Override
        public Element toXml(Document doc) {
            Element option = doc.createElement("option");
            option.setAttribute("label", this.label);
            option.setAttribute("value", this.value);
            option.setAttribute("source", this.source);
            option.setAttribute("uri", this.uri);
            return option;
        }

        @Override
        public ComboBoxData.Option fromXml(Element item) {
            this.label = item.getAttribute("label");
            this.value = item.getAttribute("value");
            this.source = item.getAttribute("source");
            this.uri = item.getAttribute("uri");
            return this;
        }

    }

    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public Element toXml(Document doc) {
        Element root = doc.createElement("data");
        root.setAttribute("type", this.type);
        root.setAttribute("label", this.getLabel());
        return root;
    }

    @Override
    public T fromXml(Element item) {
        this.setLabel(item.getAttribute("label"));
        this.type = item.getAttribute("type");
        return (T) this;
    }

    @Override
    public T fromData(Object data) {

        if (data != null) {
            this.type = (String) ((Map<String, Object>) data).get("type");
            this.setLabel((String) ((Map<String, Object>) data).get("label"));
        }

        return (T) this;
    }

    @Override
    public Object toData() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Map<String, Object> toMap(Element item) {
        HashMap dataMap = new HashMap();
        dataMap.put("label", item != null ? item.getAttribute("label") : "");
        dataMap.put("type", item != null ? item.getAttribute("type") : "");
        return dataMap;
    }
}
