package eu.eudat.models.validators;

import eu.eudat.data.query.items.table.dmp.DataManagementPlanTableRequest;
import eu.eudat.models.validators.fluentvalidator.FluentValidator;
import org.springframework.stereotype.Component;


@Component("dataManagementPlanTableRequestValidator")
public class DataManagementPlanTableRequestValidator extends FluentValidator<DataManagementPlanTableRequest> {

    public DataManagementPlanTableRequestValidator() {
        ruleFor(x -> x.getOffset()).compareAs((x -> 0), (leftItem, rightItem) -> leftItem < rightItem)
                .withName("offset").withMessage("datamanagementplanrequest.offset.negative");
        ruleFor(x -> x.getLength()).compareAs((x -> 0), (leftItem, rightItem) -> leftItem < rightItem)
                .withName("length").withMessage("datamanagementplanrequest.length.negative");
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return DataManagementPlanTableRequest.class.equals(clazz);
    }

    public static boolean supportsType(Class<?> clazz) {
        return DataManagementPlanTableRequest.class.equals(clazz);
    }
}
