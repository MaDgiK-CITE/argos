package eu.eudat.models.data.dashboard.recent;

import java.util.List;

/**
 * Created by ikalyvas on 3/14/2018.
 */
public class RecentActivity {
    private List<RecentActivityData> recentGrantActivities;
    private List<RecentActivityData> recentDatasetActivities;
    private List<RecentActivityData> recentDmpActivities;

    public List<RecentActivityData> getRecentGrantActivities() {
        return recentGrantActivities;
    }

    public void setRecentGrantActivities(List<RecentActivityData> recentGrantActivities) {
        this.recentGrantActivities = recentGrantActivities;
    }

    public List<RecentActivityData> getRecentDatasetActivities() {
        return recentDatasetActivities;
    }

    public void setRecentDatasetActivities(List<RecentActivityData> recentDatasetActivities) {
        this.recentDatasetActivities = recentDatasetActivities;
    }

    public List<RecentActivityData> getRecentDmpActivities() {
        return recentDmpActivities;
    }

    public void setRecentDmpActivities(List<RecentActivityData> recentDmpActivities) {
        this.recentDmpActivities = recentDmpActivities;
    }
}
