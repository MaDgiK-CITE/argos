package eu.eudat.models.data.datasetImport;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;
import java.util.List;
import java.util.stream.Collectors;

@XmlRootElement(name = "field")
public class DatasetImportField implements Comparable{

    private String id;
    private Integer ordinal;
    private String value;
    private DatasetImportViewStyle viewStyle;
    private String datatype;
    private String numbering;
    private int page;
    private DatasetImportDefaultValue defaultValue;
    private DatasetImportMultiplicity multiplicity;
    private Object data;
    private List<DatasetImportFields> multiplicityItems;
    private List<eu.eudat.models.data.admin.components.datasetprofile.Field.ValidationType> validations;
    private DatasetImportVisibility visible;

    public List<DatasetImportFields> getMultiplicityItems() {
        return multiplicityItems;
    }
    public void setMultiplicityItems(List<DatasetImportFields> multiplicityItems) {
        this.multiplicityItems = multiplicityItems;
    }

    @XmlAttribute(name = "id")
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public int getOrdinal() {
        return ordinal;
    }
    public void setOrdinal(Integer ordinal) {
        this.ordinal = ordinal;
    }

    @XmlElement(name = "value")
    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }

    public DatasetImportViewStyle getViewStyle() {
        return viewStyle;
    }
    public void setViewStyle(DatasetImportViewStyle viewStyle) {
        this.viewStyle = viewStyle;
    }

    public int getPage() {
        return page;
    }
    public void setPage(int page) {
        this.page = page;
    }

    public DatasetImportDefaultValue getDefaultValue() {
        return defaultValue;
    }
    public void setDefaultValue(DatasetImportDefaultValue defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getDatatype() {
        return datatype;
    }
    public void setDatatype(String datatype) {
        this.datatype = datatype;
    }

    public DatasetImportMultiplicity getMultiplicity() {
        return multiplicity;
    }
    public void setMultiplicity(DatasetImportMultiplicity multiplicity) {
        this.multiplicity = multiplicity;
    }

    public Object getData() {
        return data;
    }
    public void setData(Object data) {
        this.data = data;
    }

    public DatasetImportVisibility getVisible() {
        return visible;
    }
    public void setVisible(DatasetImportVisibility visible) {
        this.visible = visible;
    }

    public List<Integer> getValidations() {
        return this.validations.stream().map(item -> (int) item.getValue()).collect(Collectors.toList());
    }
    public void setValidations(List<Integer> validations) {
        this.validations = eu.eudat.models.data.admin.components.datasetprofile.Field.ValidationType.fromIntegers(validations);
    }

    public String getNumbering() {
        return numbering;
    }
    public void setNumbering(String numbering) {
        this.numbering = numbering;
    }

    @Override
    public int compareTo(Object o) {
        return this.ordinal.compareTo(((DatasetImportField) o).getOrdinal());
    }
}
