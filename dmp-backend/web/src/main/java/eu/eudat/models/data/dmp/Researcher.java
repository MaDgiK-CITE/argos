package eu.eudat.models.data.dmp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import eu.eudat.models.DataModel;
import eu.eudat.logic.utilities.helpers.LabelGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Researcher implements DataModel<eu.eudat.data.entities.Researcher, Researcher>, LabelGenerator {
    private static final Logger logger = LoggerFactory.getLogger(Researcher.class);
    private String label;
    private String name;
    private String id;
    private String reference;
    private int status;
    private String tag;
    private String key;

    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getReference() {
        return reference;
    }
    public void setReference(String reference) {
        this.reference = reference;
    }

    public int getStatus() {
        return status;
    }
    public void setStatus(int status) {
        this.status = status;
    }

    public String getTag() {
        return tag;
    }
    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getKey() {
        return key;
    }
    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public Researcher fromDataModel(eu.eudat.data.entities.Researcher entity) {
        this.id = entity.getId().toString();
        this.label = entity.getUri();
        this.name = entity.getLabel();
        this.status = entity.getStatus();
        this.reference = entity.getReference();
        String refParts[] = entity.getReference().split(":");
        String source = refParts[0];
        if (source.equals("dmp"))
            this.key = "Internal";
        else
            this.key = source;
        return this;
    }

    @Override
    public eu.eudat.data.entities.Researcher toDataModel() {
        eu.eudat.data.entities.Researcher researcher = new eu.eudat.data.entities.Researcher();
        if (this.id == null) {
            this.id = UUID.randomUUID().toString();
        }
        researcher.setId(UUID.fromString(this.id));
        if (this.key != null) {
            if (this.key.toLowerCase().equals("internal")) {
                if (this.reference != null && !this.reference.startsWith("dmp:")) {
                    researcher.setReference("dmp:" + this.reference);
                } else if (this.reference == null) {
                    researcher.setReference("dmp:" + this.id);
                } else {
                    researcher.setReference(this.reference);
                }
            } else {
                if ((this.key + ":").equals(this.reference.substring(0, this.key.length() + 1))) {
                    researcher.setReference(this.reference);
                } else {
                    researcher.setReference(this.key + ":" + this.reference);
                }
            }
        } else {
            try {
                throw new Exception("Researcher has no key value");
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }

        researcher.setLabel(this.name);
        researcher.setUri(this.label);
        researcher.setCreated(new Date());
        researcher.setStatus((short) this.status);
        return researcher;
    }

    @Override
    public String generateLabel() {
        return this.getName();
    }

    @Override
    public String getHint() {
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Researcher that = (Researcher) o;

        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return reference.hashCode();
    }
}
