package eu.eudat.models.data.components.commons.datafield;

import eu.eudat.logic.utilities.interfaces.XmlSerializable;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class RadioBoxData extends FieldData<RadioBoxData> {

    public class Option implements XmlSerializable<Option> {
        private String label;
        private String value;

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        @Override
        public Element toXml(Document doc) {
            Element option = doc.createElement("option");
            option.setAttribute("label", this.label);
            option.setAttribute("value", this.value);
            return option;
        }

        @Override
        public Option fromXml(Element item) {
            this.label = item.getAttribute("label");
            this.value = item.getAttribute("value");
            return this;
        }


    }

    private List<Option> options;

    public List<Option> getOptions() {
        return options;
    }

    public void setOptions(List<Option> options) {
        this.options = options;
    }

    @Override
    public RadioBoxData fromData(Object data) {
        this.options = new LinkedList();
        if (data != null) {
            List<Map<String, String>> options = ((Map<String, List<Map<String, String>>>) data).get("options");
            for (Map<String, String> map : options) {
                Option newOption = new Option();
                newOption.setLabel(map.get("label"));
                newOption.setValue(map.get("value"));
                this.options.add(newOption);
            }
            this.setLabel(((Map<String, String>) data).get("label"));
        }
        return this;
    }

    @Override
    public Object toData() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Element toXml(Document doc) {
        Element root = doc.createElement("data");
        Element element = doc.createElement("options");
        for (Option option : this.options) {
            element.appendChild(option.toXml(doc));
        }
        root.setAttribute("label", this.getLabel());
        root.appendChild(element);
        return root;
    }

    @Override
    public RadioBoxData fromXml(Element item) {

        this.options = new LinkedList<>();
        Element optionsElement = (Element) item.getElementsByTagName("options").item(0);
        this.setLabel(item.getAttribute("label"));
        if (optionsElement != null) {
            NodeList optionElements = optionsElement.getChildNodes();
            for (int temp = 0; temp < optionElements.getLength(); temp++) {
                Node optionElement = optionElements.item(temp);
                if (optionElement.getNodeType() == Node.ELEMENT_NODE) {
                    this.options.add(new Option().fromXml((Element) optionElement));
                }
            }
        }
        return this;
    }


    @Override
    public Map<String, Object> toMap(Element item) {
        HashMap dataMap = new HashMap();
        dataMap.put("label", item != null ? item.getAttribute("label") : "");
        Element optionsElement = (Element) item.getElementsByTagName("options").item(0);
        List<Map<String,String>> option =new LinkedList<>();

        if (optionsElement != null) {
            NodeList optionElements = optionsElement.getChildNodes();
            for (int temp = 0; temp < optionElements.getLength(); temp++) {
                Node optionElement = optionElements.item(temp);
                if (optionElement.getNodeType() == Node.ELEMENT_NODE) {
                    option.add(optionToMap((Element) optionElement));
                }
            }
        }

        dataMap.put("options", option != null ? option : null);
        return dataMap;
    }

    private Map<String, String> optionToMap(Element item){
        HashMap dataMap = new HashMap();
        dataMap.put("label",item.getAttribute("label"));
        dataMap.put("value",item.getAttribute("value"));

        return dataMap;
    }
}
