package eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition;

import eu.eudat.logic.utilities.interfaces.XmlSerializable;
import eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition.types.SystemFieldType;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.util.UUID;

public class SystemField implements XmlSerializable<SystemField> {

    private UUID id;
    private SystemFieldType type;
    private String label;
    private String placeholder;
    private String description;
    private boolean required;
    private Integer ordinal;

    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }

    public SystemFieldType getType() {
        return type;
    }
    public void setType(SystemFieldType type) {
        this.type = type;
    }

    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }

    public String getPlaceholder() {
        return placeholder;
    }
    public void setPlaceholder(String placeholder) {
        this.placeholder = placeholder;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isRequired() {
        return required;
    }
    public void setRequired(boolean required) {
        this.required = required;
    }

    public Integer getOrdinal() {
        return ordinal;
    }
    public void setOrdinal(Integer ordinal) {
        this.ordinal = ordinal;
    }


    @Override
    public Element toXml(Document doc) {
        Element rootElement = doc.createElement("systemField");
        rootElement.setAttribute("id", this.getId().toString());
        rootElement.setAttribute("type", String.valueOf(this.type.getType()));
        rootElement.setAttribute("label", this.label);
        rootElement.setAttribute("placeholder", this.placeholder);
        rootElement.setAttribute("description", this.description);
        rootElement.setAttribute("required", String.valueOf(this.required));
        rootElement.setAttribute("ordinal", String.valueOf(this.ordinal));
        return rootElement;
    }

    @Override
    public SystemField fromXml(Element item) {
        this.id = UUID.fromString(item.getAttribute("id"));
        this.type = SystemFieldType.fromInteger(Integer.parseInt(item.getAttribute("type")));
        this.label = item.getAttribute("label");
        this.placeholder = item.getAttribute("placeholder");
        this.description = item.getAttribute("description");
        this.required = Boolean.parseBoolean(item.getAttribute("required"));
        this.ordinal = Integer.valueOf(item.getAttribute("ordinal"));
        return this;
    }
}
