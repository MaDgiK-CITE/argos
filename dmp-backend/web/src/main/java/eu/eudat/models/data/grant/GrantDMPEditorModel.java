package eu.eudat.models.data.grant;

public class GrantDMPEditorModel {
    private Grant existGrant;
    private String label;
    private String description;
    private String reference;

    public Grant getExistGrant() {
        return existGrant;
    }
    public void setExistGrant(Grant existGrant) {
        this.existGrant = existGrant;
    }

    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }
}
