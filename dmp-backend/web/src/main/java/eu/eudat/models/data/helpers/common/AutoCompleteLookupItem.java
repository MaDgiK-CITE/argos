package eu.eudat.models.data.helpers.common;

import eu.eudat.data.dao.criteria.Criteria;

public class AutoCompleteLookupItem extends Criteria<AutoCompleteLookupItem>{
    private String profileID;
    private String fieldID;

    public String getProfileID() {
        return profileID;
    }

    public void setProfileID(String profileID) {
        this.profileID = profileID;
    }

    public String getFieldID() {
        return fieldID;
    }

    public void setFieldID(String fieldID) {
        this.fieldID = fieldID;
    }

}
