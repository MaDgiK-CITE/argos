package eu.eudat.models.validators.fluentvalidator.rules;

import eu.eudat.models.validators.fluentvalidator.predicates.FieldSelector;

/**
 * Created by ikalyvas on 8/31/2018.
 */
public class NotEmptyRule<T> extends AbstractFluentValidatorRule<T> {

    public NotEmptyRule(FieldSelector fieldSelector) {
        super(fieldSelector);
    }

    @Override
    public boolean assertValue(T item) {
        return this.getFieldSelector().apply(item) != null;
    }
}
