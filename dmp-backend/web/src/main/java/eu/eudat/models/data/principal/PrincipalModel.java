package eu.eudat.models.data.principal;

import eu.eudat.models.data.security.Principal;
import eu.eudat.types.Authorities;

import java.util.Date;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

public class PrincipalModel {
	private UUID id;
	private UUID token;
	private String name;
	private String email;
	private Date expiresAt;
	private String avatarUrl;
	private Set<Authorities> authorities;
	private String culture;
	private String language;
	private String timezone;
	private String zenodoEmail;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public UUID getToken() {
		return token;
	}

	public void setToken(UUID token) {
		this.token = token;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getExpiresAt() {
		return expiresAt;
	}

	public void setExpiresAt(Date expiresAt) {
		this.expiresAt = expiresAt;
	}

	public String getAvatarUrl() {
		return avatarUrl;
	}

	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}

	public Set<Authorities> getAuthz() {
		return authorities;
	}

	public Set<Integer> getAuthorities() {
		return authorities.stream().map(authz -> authz.getValue()).collect(Collectors.toSet());
	}

	public void setAuthorities(Set<Authorities> authorities) {
		this.authorities = authorities;
	}

	public String getCulture() {
		return culture;
	}

	public void setCulture(String culture) {
		this.culture = culture;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public String getZenodoEmail() {
		return zenodoEmail;
	}

	public void setZenodoEmail(String zenodoEmail) {
		this.zenodoEmail = zenodoEmail;
	}

	public static PrincipalModel fromEntity(Principal principal) {
		PrincipalModel model = new PrincipalModel();
		model.setId(principal.getId());
		model.setToken(principal.getToken());
		model.setAuthorities(principal.getAuthz());
		model.setAvatarUrl(principal.getAvatarUrl());
		model.setCulture(principal.getCulture());
		model.setEmail(principal.getEmail());
		model.setExpiresAt(principal.getExpiresAt());
		model.setLanguage(principal.getLanguage());
		model.setName(principal.getName());
		model.setTimezone(principal.getTimezone());
		model.setZenodoEmail(principal.getZenodoEmail());
		return model;
	}
}
