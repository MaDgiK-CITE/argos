package eu.eudat.models.data.datasetImport;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collections;
import java.util.List;

@XmlRootElement(name = "composite-fields")
public class DatasetImportFieldSets implements Comparable {
    private List<DatasetImportFieldSet> compositeField;

    @XmlElement(name = "composite-field")
    public List<DatasetImportFieldSet> getCompositeField() {
        //Collections.sort(this.compositeField);
        return compositeField;
    }
    public void setCompositeField(List<DatasetImportFieldSet> compositeField) {
        this.compositeField = compositeField;
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }
}
