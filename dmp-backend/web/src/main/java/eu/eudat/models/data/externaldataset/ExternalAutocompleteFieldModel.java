package eu.eudat.models.data.externaldataset;

public class ExternalAutocompleteFieldModel {
	private String id;
	private String label;
	private String source;
	private String uri;

	public ExternalAutocompleteFieldModel(String id, String label, String source, String uri) {
		this.id = id;
		this.label = label;
		this.source = source;
		this.uri = uri;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}

	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}
}
