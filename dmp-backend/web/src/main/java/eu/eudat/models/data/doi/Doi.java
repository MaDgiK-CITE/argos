package eu.eudat.models.data.doi;

import eu.eudat.data.entities.EntityDoi;
import eu.eudat.models.DataModel;
import eu.eudat.models.data.dmp.DataManagementPlan;

import java.util.Date;
import java.util.UUID;

public class Doi implements DataModel<EntityDoi, Doi> {
    private UUID id;
    private String repositoryId;
    private String doi;
    private Date createdAt;
    private Date updatedAt;
    private DataManagementPlan dmp;

    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }

    public String getRepositoryId() {
        return repositoryId;
    }
    public void setRepositoryId(String repositoryId) {
        this.repositoryId = repositoryId;
    }

    public String getDoi() {
        return doi;
    }
    public void setDoi(String doi) {
        this.doi = doi;
    }

    public Date getCreatedAt() {
        return createdAt;
    }
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public DataManagementPlan getDmp() {
        return dmp;
    }
    public void setDmp(DataManagementPlan dmp) {
        this.dmp = dmp;
    }

    @Override
    public Doi fromDataModel(EntityDoi entity) {
        this.id = entity.getId();
        this.repositoryId = entity.getRepositoryId();
        this.doi = entity.getDoi();
        this.createdAt = entity.getCreatedAt();
        this.updatedAt = entity.getUpdatedAt();
        return this;
    }

    @Override
    public EntityDoi toDataModel() throws Exception {
        EntityDoi entityDoi = new EntityDoi();
        entityDoi.setId(this.id);
        entityDoi.setRepositoryId(this.repositoryId);
        entityDoi.setDoi(this.doi);
        entityDoi.setCreatedAt(this.createdAt);
        entityDoi.setUpdatedAt(this.updatedAt);
        return entityDoi;
    }

    @Override
    public String getHint() {
        return null;
    }
}
