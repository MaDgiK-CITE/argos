package eu.eudat.models.data.rda;

import eu.eudat.data.entities.UserInfo;

import java.util.LinkedList;
import java.util.List;

public class DmpContributorRDAExportModel {
	private IdRDAExportModel contributor_id;
	private String mbox;
	private String name;
	private List<String> role;

	public IdRDAExportModel getContributor_id() {
		return contributor_id;
	}
	public void setContributor_id(IdRDAExportModel contributor_id) {
		this.contributor_id = contributor_id;
	}

	public String getMbox() {
		return mbox;
	}
	public void setMbox(String mbox) {
		this.mbox = mbox;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public List<String> getRole() {
		return role;
	}
	public void setRole(List<String> role) {
		this.role = role;
	}

	public DmpContributorRDAExportModel fromDataModel(UserInfo user, String role) {
		DmpContributorRDAExportModel contributor = new DmpContributorRDAExportModel();
		contributor.contributor_id = new IdRDAExportModel(user.getId().toString(), "other");
		contributor.mbox = user.getEmail();
		contributor.name = user.getName();
		contributor.role = new LinkedList<>();
		contributor.role.add(role);

		return contributor;
	}
}
