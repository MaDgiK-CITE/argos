
package eu.eudat.models.rda;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;


/**
 * The Dataset Metadata Standard ID Schema
 * <p>
 * 
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "identifier",
    "type"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class MetadataStandardId implements Serializable
{

    /**
     * The Dataset Metadata Standard Identifier Value Schema
     * <p>
     * Identifier for the metadata standard used.
     * (Required)
     * 
     */
    @JsonProperty("identifier")
    @JsonPropertyDescription("Identifier for the metadata standard used.")
    private String identifier;
    /**
     * The Dataset Metadata Standard Identifier Type Schema
     * <p>
     * Identifier type. Allowed values: url, other
     * (Required)
     * 
     */
    @JsonProperty("type")
    @JsonPropertyDescription("Identifier type. Allowed values: url, other")
    private MetadataStandardId.Type type;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = -7641042701935397947L;

    /**
     * The Dataset Metadata Standard Identifier Value Schema
     * <p>
     * Identifier for the metadata standard used.
     * (Required)
     * 
     */
    @JsonProperty("identifier")
    public String getIdentifier() {
        return identifier;
    }

    /**
     * The Dataset Metadata Standard Identifier Value Schema
     * <p>
     * Identifier for the metadata standard used.
     * (Required)
     * 
     */
    @JsonProperty("identifier")
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    /**
     * The Dataset Metadata Standard Identifier Type Schema
     * <p>
     * Identifier type. Allowed values: url, other
     * (Required)
     * 
     */
    @JsonProperty("type")
    public MetadataStandardId.Type getType() {
        return type;
    }

    /**
     * The Dataset Metadata Standard Identifier Type Schema
     * <p>
     * Identifier type. Allowed values: url, other
     * (Required)
     * 
     */
    @JsonProperty("type")
    public void setType(MetadataStandardId.Type type) {
        this.type = type;
    }

    @JsonProperty("additional_properties")
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonProperty("additional_properties")
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public enum Type {

        URL("url"),
        OTHER("other");
        private final String value;
        private final static Map<String, MetadataStandardId.Type> CONSTANTS = new HashMap<String, MetadataStandardId.Type>();

        static {
            for (MetadataStandardId.Type c: values()) {
                CONSTANTS.put(c.value, c);
            }
        }

        private Type(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return this.value;
        }

        @JsonValue
        public String value() {
            return this.value;
        }

        @JsonCreator
        public static MetadataStandardId.Type fromValue(String value) {
            MetadataStandardId.Type constant = CONSTANTS.get(value);
            if (constant == null) {
                throw new IllegalArgumentException(value);
            } else {
                return constant;
            }
        }

    }

}
