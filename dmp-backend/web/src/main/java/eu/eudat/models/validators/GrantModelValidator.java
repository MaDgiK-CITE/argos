package eu.eudat.models.validators;

import eu.eudat.models.data.grant.Grant;
import eu.eudat.models.validators.fluentvalidator.FluentValidator;
import org.springframework.stereotype.Component;


@Component("grantModelValidator")
public class GrantModelValidator extends FluentValidator<Grant> {

    public GrantModelValidator() {
        ruleFor(x -> x.getType()).compareAs((x -> eu.eudat.data.entities.Grant.GrantType.EXTERNAL.getValue()), (leftItem, rightItem) -> leftItem == rightItem)
                .withName("type").withMessage("grant.external.edit");
        ruleFor(x -> x.getStartDate()).compareAs((x -> x.getEndDate()), (leftItem, rightItem) -> leftItem.after(rightItem))
                .withName("startDate").withMessage("grant.startDate.overlapping")
                .ruleIf(x -> x.getStartDate() != null && x.getEndDate() != null);

    }

    @Override
    public boolean supports(Class<?> aClass) {
        return Grant.class.equals(aClass);
    }

    public static boolean supportsType(Class<?> aClass) {
        return Grant.class.equals(aClass);
    }
}
