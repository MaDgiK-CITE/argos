package eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition;

import eu.eudat.logic.utilities.builders.XmlBuilder;
import eu.eudat.logic.utilities.interfaces.XmlSerializable;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.LinkedList;
import java.util.List;

public class DataManagementPlanBlueprint implements XmlSerializable<DataManagementPlanBlueprint> {

    private List<Section> sections;

    public List<Section> getSections() {
        return sections;
    }
    public void setSections(List<Section> sections) {
        this.sections = sections;
    }

    @Override
    public Element toXml(Document doc) {
        Element root = doc.createElement("root");
        Element sections = doc.createElement("sections");
        for (Section section : this.sections) {
            sections.appendChild(section.toXml(doc));
        }
        root.appendChild(sections);
        return root;
    }

    @Override
    public DataManagementPlanBlueprint fromXml(Element item) {
        this.sections = new LinkedList<>();
        Element sections = (Element) XmlBuilder.getNodeFromListByTagName(item.getChildNodes(), "sections");
        if (sections != null) {
            NodeList sectionElements = sections.getChildNodes();
            for (int temp = 0; temp < sectionElements.getLength(); temp++) {
                Node sectionElement = sectionElements.item(temp);
                if (sectionElement.getNodeType() == Node.ELEMENT_NODE) {
                    this.sections.add(new Section().fromXml((Element) sectionElement));
                }
            }
        }
        return this;
    }
}
