package eu.eudat.models.data.listingmodels;

import eu.eudat.data.entities.UserDMP;
import eu.eudat.data.entities.UserInfo;
import eu.eudat.models.DataModel;

import java.util.UUID;

public class UserInfoListingModel implements DataModel<UserDMP, UserInfoListingModel> {

    private UUID id;
    private String name;
    private int role;
    private String email;

    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public int getRole() {
        return role;
    }
    public void setRole(int role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public UserInfoListingModel fromDataModel(UserDMP entity) {
        this.id = entity.getUser().getId();
        this.name = entity.getUser().getName();
        this.role = entity.getRole();
        this.email = entity.getUser().getEmail();
        return this;
    }

    @Override
    public UserDMP toDataModel() {
        UserDMP entity = new UserDMP();
        entity.setId(this.getId());
        entity.setRole(this.getRole());
        UserInfo userInfo = new UserInfo();
        userInfo.setName(this.getName());
        userInfo.setEmail(this.getEmail());
        entity.setUser(userInfo);
        return entity;
    }

    @Override
    public String getHint() {
        return "UserInfoListingModel";
    }
}
