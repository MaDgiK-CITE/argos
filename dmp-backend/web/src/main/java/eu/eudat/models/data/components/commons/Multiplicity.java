package eu.eudat.models.data.components.commons;

public class Multiplicity {

    private int min;
    private int max;
    private String placeholder;
    private boolean tableView;

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public String getPlaceholder() {
        return placeholder;
    }

    public void setPlaceholder(String placeholder) {
        this.placeholder = placeholder;
    }

    public boolean getTableView() {
        return tableView;
    }

    public void setTableView(boolean tableView) {
        this.tableView = tableView;
    }
}
