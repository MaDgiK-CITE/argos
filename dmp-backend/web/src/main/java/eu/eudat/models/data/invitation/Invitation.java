package eu.eudat.models.data.invitation;

import eu.eudat.models.data.userinfo.UserInfoInvitationModel;

import java.util.List;
import java.util.UUID;


public class Invitation {
    private UUID dataManagementPlan;
    private List<UserInfoInvitationModel> users;
    private Integer role;

    public UUID getDataManagementPlan() {
        return dataManagementPlan;
    }

    public void setDataManagementPlan(UUID dataManagementPlan) {
        this.dataManagementPlan = dataManagementPlan;
    }

    public List<UserInfoInvitationModel> getUsers() {
        return users;
    }

    public void setUsers(List<UserInfoInvitationModel> users) {
        this.users = users;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }
}
