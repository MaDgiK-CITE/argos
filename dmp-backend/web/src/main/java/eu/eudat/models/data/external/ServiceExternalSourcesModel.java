package eu.eudat.models.data.external;

import java.util.List;
import java.util.Map;


public class ServiceExternalSourcesModel extends ExternalListingItem<ServiceExternalSourcesModel> {
    @Override
    public ServiceExternalSourcesModel fromExternalItem(List<Map<String, String>> values) {
        for (Map<String, String> item : values) {
            ExternalSourcesItemModel model = new ExternalSourcesItemModel();
            model.setId((String)item.get("pid"));
            model.setUri((String)item.get("label"));
            model.setName((String)item.get("name"));
            model.setTag((String)item.get("tag"));
            this.add(model);
        }
        return this;
    }
}
