package eu.eudat.models.data.project;

import eu.eudat.data.entities.UserInfo;
import eu.eudat.models.DataModel;
import eu.eudat.models.data.dmp.DataManagementPlan;
import eu.eudat.models.data.files.ContentFile;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class Project implements DataModel<eu.eudat.data.entities.Project, Project> {

	private UUID id;
	private List<DataManagementPlan> dmps;
	private String label;
	private int type;
	private String abbreviation;
	private String reference;
	private String uri;
	private String definition;
	private Date startDate;
	private Date endDate;
	private Short status;
	private UserInfo creationUser;
	private Date created;
	private Date modified;
	private String description;
	private List<ContentFile> files;
	private String source;
	private String key;

	public UUID getId() {
		return id;
	}
	public void setId(UUID id) {
		this.id = id;
	}

	public List<DataManagementPlan> getDmps() {
		return dmps;
	}
	public void setDmps(List<DataManagementPlan> dmps) {
		this.dmps = dmps;
	}

	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}

	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}

	public String getAbbreviation() {
		return abbreviation;
	}
	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getDefinition() {
		return definition;
	}
	public void setDefinition(String definition) {
		this.definition = definition;
	}

	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Short getStatus() {
		return status;
	}
	public void setStatus(Short status) {
		this.status = status;
	}

	public UserInfo getCreationUser() {
		return creationUser;
	}
	public void setCreationUser(UserInfo creationUser) {
		this.creationUser = creationUser;
	}

	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getModified() {
		return modified;
	}
	public void setModified(Date modified) {
		this.modified = modified;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public List<ContentFile> getFiles() {
		return files;
	}
	public void setFiles(List<ContentFile> files) {
		this.files = files;
	}

	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}

	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}

	@Override
	public Project fromDataModel(eu.eudat.data.entities.Project entity) {
		this.id = entity.getId();
		this.label = entity.getLabel();
		this.abbreviation = entity.getAbbreviation();
		this.reference = entity.getReference();
		this.uri = entity.getUri();
		if (entity.getType() != null) {
			this.type = entity.getType();
		}
		this.definition = entity.getDefinition();
		this.startDate = entity.getStartdate();
		this.endDate = entity.getEnddate();
		this.setStatus(entity.getStatus());
		this.created = entity.getCreated();
		this.modified = entity.getModified();
		this.description = entity.getDescription();
		this.files = entity.getContent() != null ? Arrays.asList(new ContentFile(entity.getContent().getLabel(), UUID.fromString(entity.getContent().getUri().split(":")[1]), "final", entity.getContent().getExtension())) : Arrays.asList(new ContentFile("default.png", null, null, null));
		if (entity.getReference() != null) {
			String source = entity.getReference().substring(0, entity.getReference().indexOf(":"));
			if (source.equals("dmp")) {
				this.key = "Internal";
			} else {
				this.key = source;
			}
		}

		return this;
	}

	@Override
	public eu.eudat.data.entities.Project toDataModel() {
		eu.eudat.data.entities.Project entity = new eu.eudat.data.entities.Project();
		entity.setId(UUID.randomUUID());
		entity.setAbbreviation(this.abbreviation);
		entity.setLabel(this.label);
		entity.setType(this.type);
		if ((this.source != null && this.source.equals("Internal")) || (this.key != null && this.key.equals("Internal"))) this.key = "dmp";
		if (this.reference != null && this.reference.startsWith("dmp:")) {
			entity.setReference(this.reference);
		} else if (this.reference != null && !this.reference.trim().isEmpty() && this.key != null && !this.key.trim().isEmpty()) {
			if (this.key.equals(this.reference.substring(0, this.key.length()))) {
				entity.setReference(this.reference);
			} else {
				entity.setReference(this.key.toLowerCase() + ":" + this.reference);
			}
		}
		entity.setUri(this.uri);
		entity.setDefinition(this.definition);
		entity.setStartdate(this.startDate);
		entity.setCreated(this.created == null ? new Date() : this.created);
		entity.setEnddate(this.endDate);
		entity.setStatus(this.status != null ? this.getStatus() : eu.eudat.data.entities.Grant.Status.ACTIVE.getValue());
		entity.setModified(new Date());
		entity.setDescription(this.description);
		return entity;
	}

	@Override
	public String getHint() {
		return null;
	}
}
