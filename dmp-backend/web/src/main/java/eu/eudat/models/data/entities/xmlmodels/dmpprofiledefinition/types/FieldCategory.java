package eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition.types;

public enum FieldCategory {
    SYSTEM(0),
    EXTRA(1);

    private Integer value;

    private FieldCategory(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }

    public static FieldCategory fromInteger(Integer value) {
        switch (value) {
            case 0:
                return SYSTEM;
            case 1:
                return EXTRA;
            default:
                throw new RuntimeException("Unsupported FieldCategory Type");
        }
    }
}
