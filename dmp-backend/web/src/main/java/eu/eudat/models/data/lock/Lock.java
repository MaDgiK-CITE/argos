package eu.eudat.models.data.lock;

import eu.eudat.models.DataModel;
import eu.eudat.models.data.userinfo.UserInfo;

import java.util.Date;
import java.util.UUID;

public class Lock implements DataModel<eu.eudat.data.entities.Lock, Lock> {
	private UUID id;
	private UUID target;
	private UserInfo lockedBy;
	private Date lockedAt;
	private Date touchedAt;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public UUID getTarget() {
		return target;
	}

	public void setTarget(UUID target) {
		this.target = target;
	}

	public UserInfo getLockedBy() {
		return lockedBy;
	}

	public void setLockedBy(UserInfo lockedBy) {
		this.lockedBy = lockedBy;
	}

	public Date getLockedAt() {
		return lockedAt;
	}

	public void setLockedAt(Date lockedAt) {
		this.lockedAt = lockedAt;
	}

	public Date getTouchedAt() {
		return touchedAt;
	}

	public void setTouchedAt(Date touchedAt) {
		this.touchedAt = touchedAt;
	}

	@Override
	public Lock fromDataModel(eu.eudat.data.entities.Lock entity) {
		this.id = entity.getId();
		this.target = entity.getTarget();
		this.lockedBy = new UserInfo().fromDataModel(entity.getLockedBy());
		this.lockedAt = entity.getLockedAt();
		this.touchedAt = entity.getTouchedAt();
		return this;
	}

	@Override
	public eu.eudat.data.entities.Lock toDataModel() throws Exception {
		eu.eudat.data.entities.Lock entity = new eu.eudat.data.entities.Lock();
		entity.setId(this.getId());
		entity.setTarget(this.getTarget());
		entity.setLockedAt(this.getLockedAt());
		entity.setTouchedAt(this.getTouchedAt());
		entity.setLockedBy(this.getLockedBy().toDataModel());
		return entity;
	}

	@Override
	public String getHint() {
		return null;
	}
}
