package eu.eudat.models.data.dashboard.searchbar;

public class SearchBarItem {
    private String id;
    private String label;
    private int type;
    private boolean isPublished;

    public SearchBarItem(String id, String label, int type, boolean isPublished) {
        this.id = id;
        this.label = label;
        this.type = type;
        this.isPublished = isPublished;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public boolean getIsPublished() {
        return isPublished;
    }

    public void setPublished(boolean published) {
        isPublished = published;
    }
}
