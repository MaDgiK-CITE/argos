package eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition.types;

public enum SystemFieldType {

    TEXT(0),
    HTML_TEXT(1),
    RESEARCHERS(2),
    ORGANIZATIONS(3),
    LANGUAGE(4),
    CONTACT(5),
    FUNDER(6),
    GRANT(7),
    PROJECT(8),
    LICENSE(9),
    ACCESS_RIGHTS(10);

    private int type;

    SystemFieldType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public static SystemFieldType fromInteger(int type) {
        switch (type) {
            case 0:
                return TEXT;
            case 1:
                return HTML_TEXT;
            case 2:
                return RESEARCHERS;
            case 3:
                return ORGANIZATIONS;
            case 4:
                return LANGUAGE;
            case 5:
                return CONTACT;
            case 6:
                return FUNDER;
            case 7:
                return GRANT;
            case 8:
                return PROJECT;
            case 9:
                return LICENSE;
            case 10:
                return ACCESS_RIGHTS;
            default:
                throw new RuntimeException("Unsupported System Section Type");
        }
    }
}