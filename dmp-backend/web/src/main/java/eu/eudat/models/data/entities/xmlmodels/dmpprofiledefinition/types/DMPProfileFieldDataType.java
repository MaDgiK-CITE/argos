package eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition.types;

/**
 * Created by ikalyvas on 3/21/2018.
 */
public enum DMPProfileFieldDataType {
    DATE(0), NUMBER(1), TEXT(2), EXTERNAL_AUTOCOMPLETE(3);

    private Integer value;

    private DMPProfileFieldDataType(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }

    public static DMPProfileFieldDataType fromInteger(Integer value) {
        switch (value) {
            case 0:
                return DATE;
            case 1:
                return NUMBER;
            case 2:
                return TEXT;
            case 3:
                return EXTERNAL_AUTOCOMPLETE;
            default:
                throw new RuntimeException("Unsupported DMPProfileFieldData Type");
        }
    }
}
