package eu.eudat.models.data.datasetprofile;

import java.util.UUID;

public class DatasetProfileWithPrefillingPropertyModel {
    private UUID id;
    private String label;
    private boolean enablePrefilling;

    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isEnablePrefilling() {
        return enablePrefilling;
    }
    public void setEnablePrefilling(boolean enablePrefilling) {
        this.enablePrefilling = enablePrefilling;
    }
}
