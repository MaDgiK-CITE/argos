package eu.eudat.models.data.rda;

import eu.eudat.data.entities.Grant;

import java.util.Date;

public class ProjectRDAExportModel {
	private String title;
	private String description;
	private Date project_start;
	private Date project_end;
	private FundingRDAExportModel funding;

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public Date getProject_start() {
		return project_start;
	}
	public void setProject_start(Date project_start) {
		this.project_start = project_start;
	}

	public Date getProject_end() {
		return project_end;
	}
	public void setProject_end(Date project_end) {
		this.project_end = project_end;
	}

	public FundingRDAExportModel getFunding() {
		return funding;
	}
	public void setFunding(FundingRDAExportModel funding) {
		this.funding = funding;
	}


	public ProjectRDAExportModel fromDataModel(Grant grant) {
		this.funding = new FundingRDAExportModel().fromDataModel(grant.getFunder(), grant);
		return this;
	}
}
