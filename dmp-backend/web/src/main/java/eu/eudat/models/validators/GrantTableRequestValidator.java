package eu.eudat.models.validators;

import eu.eudat.data.query.items.table.grant.GrantTableRequest;
import eu.eudat.models.validators.fluentvalidator.FluentValidator;
import org.springframework.stereotype.Component;


@Component("grantTableRequestValidator")
public class GrantTableRequestValidator extends FluentValidator<GrantTableRequest> {

    public GrantTableRequestValidator() {
        ruleFor(x -> x.getOffset()).compareAs(x -> 0, (leftItem, rightItem) -> leftItem < rightItem)
                .withName("offset").withMessage("grantTableRequest.offset.negative");

        ruleFor(x -> x.getLength()).compareAs(x -> 0, (leftItem, rightItem) -> leftItem < rightItem)
                .withName("length").withMessage("grantTableRequest.length.negative");

        ruleFor(x -> x.getCriteria().getPeriodStart())
                .compareAs(x -> x.getCriteria().getPeriodEnd(), (leftItem, rightItem) -> rightItem.before(leftItem))
                .withName("criteria.periodStart").withMessage("grantTableRequest.periodStart.overlapping")
                .ruleIf(x -> x.getCriteria() != null && x.getCriteria().getPeriodStart() != null && x.getCriteria().getPeriodEnd() != null);
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return GrantTableRequest.class.equals(aClass);
    }

    public static boolean supportsType(Class<?> clazz) {
        return GrantTableRequest.class.equals(clazz);
    }
}
