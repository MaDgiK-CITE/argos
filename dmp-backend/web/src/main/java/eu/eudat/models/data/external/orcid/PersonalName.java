package eu.eudat.models.data.external.orcid;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="name", namespace = "http://www.orcid.org/ns/personal-details")
public class PersonalName {

	private String id;
	private String givenNames;
	private String familyName;

	public PersonalName(String givenNames, String familyName) {
		this.givenNames = givenNames;
		this.familyName = familyName;
	}

	public PersonalName() {
	}

	@XmlAttribute(name = "path")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	@XmlElement(name = "given-names", namespace = "http://www.orcid.org/ns/personal-details")
	public String getGivenNames() {
		return givenNames;
	}
	public void setGivenNames(String givenNames) {
		this.givenNames = givenNames;
	}
	@XmlElement(name = "family-name", namespace = "http://www.orcid.org/ns/personal-details")
	public String getFamilyName() {
		return familyName;
	}
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}
}
