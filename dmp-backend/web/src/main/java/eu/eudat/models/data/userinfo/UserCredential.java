package eu.eudat.models.data.userinfo;

public class UserCredential {
	private String email;
	private Integer provider;
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Integer getProvider() {
		return provider;
	}
	public void setProvider(Integer provider) {
		this.provider = provider;
	}
	
}
