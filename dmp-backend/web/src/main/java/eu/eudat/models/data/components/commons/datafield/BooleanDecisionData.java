package eu.eudat.models.data.components.commons.datafield;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.util.HashMap;
import java.util.Map;

public class BooleanDecisionData extends FieldData<BooleanDecisionData> {

    @Override
    public BooleanDecisionData fromData(Object data) {
        if (data != null) {
            this.setLabel((String) ((Map<String, Object>) data).get("label"));
        }
        return this;
    }

    @Override
    public Object toData() {
        return null;
    }

    @Override
    public Element toXml(Document doc) {
        Element element = doc.createElement("data");
        element.setAttribute("label", this.getLabel());
        return element;
    }

    @Override
    public BooleanDecisionData fromXml(Element item) {
        this.setLabel(item.getAttribute("label"));
        return this;
    }

    @Override
    public Map<String, Object> toMap(Element item) {
        HashMap dataMap = new HashMap();
        dataMap.put("label", item != null ? item.getAttribute("label") : "");
        return dataMap;
    }

}
