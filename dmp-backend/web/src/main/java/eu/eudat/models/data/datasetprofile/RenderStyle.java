package eu.eudat.models.data.datasetprofile;

public enum RenderStyle {
    COMBO_BOX("combobox"),
    INTERNAL_DMP_ENTITIES("internalDmpEntities"),
    BOOLEAN_DECISION("booleanDecision"),
    RADIO_BOX("radiobox"),
    CHECKBOX("checkBox"),
    FREETEXT("freetext"),
    TEXTAREA("textarea"),
    RICH_TEXTAREA("richTextarea"),
    DATE_PICKER("datePicker"),
    EXTERNAL_DATASETS("externalDatasets"),
    DATASET_REPOSITORIES("dataRepositories"),
    REGISTRIES("registries"),
    SERVICES("services"),
    TAGS("tags"),
    RESEARCHERS("researchers"),
    ORGANIZATIONS("organizations"),
    DATASET_IDENTIFIER("datasetIdentifier"),
    CURRENCY("currency"),
    UPLOAD("upload"),
    LICENSES("licenses"),
    VALIDATION("validation");

    private final String name;

    RenderStyle(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static RenderStyle fromValue(String value) {
        for(RenderStyle renderStyle: RenderStyle.values()) {
            if (renderStyle.getName().equals(value)) {
                return renderStyle;
            }
        }
        throw new IllegalArgumentException("RenderStyle [" + value + "] is not available");
    }
}
