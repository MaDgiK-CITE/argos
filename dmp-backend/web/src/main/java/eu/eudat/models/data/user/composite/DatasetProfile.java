package eu.eudat.models.data.user.composite;

import eu.eudat.models.data.admin.components.datasetprofile.Page;
import eu.eudat.models.data.user.components.commons.Rule;
import eu.eudat.models.data.user.components.datasetprofile.Section;
import eu.eudat.logic.utilities.builders.ModelBuilder;
import eu.eudat.logic.utilities.helpers.ModelBuilderCollector;

import java.util.List;
import java.util.Map;

public class DatasetProfile implements PropertiesModelBuilder {
    private String description;
    private String language;
    private String type;
    private boolean enablePrefilling;
    private List<Section> sections;
    private List<Rule> rules;
    private List<Page> pages;
    private int status;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isEnablePrefilling() {
        return enablePrefilling;
    }

    public void setEnablePrefilling(boolean enablePrefilling) {
        this.enablePrefilling = enablePrefilling;
    }

    public List<Section> getSections() {
        return sections;
    }

    public void setSections(List<Section> sections) {
        this.sections = sections;
    }


    public List<Rule> getRules() {
        return rules;
    }

    public void setRules(List<Rule> rules) {
        this.rules = rules;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<Page> getPages() {
        return pages;
    }

    public void setPages(List<Page> pages) {
        this.pages = pages;
    }

    public void buildProfile(eu.eudat.models.data.entities.xmlmodels.datasetprofiledefinition.ViewStyleModel viewStyle) {
        this.enablePrefilling = viewStyle.isEnablePrefilling();
        this.sections = new ModelBuilder().fromViewStyleDefinition(viewStyle.getSections(), Section.class);
        this.pages = new ModelBuilder().fromViewStyleDefinition(viewStyle.getPages(), Page.class);
        this.rules = ModelBuilderCollector.collectRules(viewStyle.getSections());
    }

    @Override
    public void fromJsonObject(Map<String, Object> properties) {
        this.sections.forEach(item -> item.fromJsonObject(properties));

    }

    @Override
    public void fromJsonObject(Map<String, Object> properties, String index) {
        // TODO Auto-generated method stub

    }

}
