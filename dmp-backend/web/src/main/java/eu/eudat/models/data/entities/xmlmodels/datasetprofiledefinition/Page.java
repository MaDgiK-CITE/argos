package eu.eudat.models.data.entities.xmlmodels.datasetprofiledefinition;

import eu.eudat.logic.utilities.interfaces.XmlSerializable;
import org.w3c.dom.Document;
import org.w3c.dom.Element;


public class Page implements DatabaseViewStyleDefinition, XmlSerializable<Page> {
    private String id;
    private int ordinal;
    private String title;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(int ordinal) {
        this.ordinal = ordinal;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public Element toXml(Document doc) {
        Element root = doc.createElement("page");
        root.setAttribute("title", this.title);
        root.setAttribute("ordinal", "" + this.ordinal);
        root.setAttribute("id", this.id);
        return root;
    }

    @Override
    public Page fromXml(Element item) {
        this.ordinal = Integer.parseInt(item.getAttribute("ordinal"));
        this.id = item.getAttribute("id");
        this.title = item.getAttribute("title");
        return this;
    }
}
