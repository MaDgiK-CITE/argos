package eu.eudat.models.data.admin.components.datasetprofile;

import eu.eudat.models.data.components.commons.Multiplicity;
import eu.eudat.logic.utilities.interfaces.ViewStyleDefinition;
import eu.eudat.logic.utilities.builders.ModelBuilder;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.Collections;
import java.util.List;

public class FieldSet implements Comparable, ViewStyleDefinition<eu.eudat.models.data.entities.xmlmodels.datasetprofiledefinition.FieldSet> {
    private String id;
    private Integer ordinal;
    private Multiplicity multiplicity;
    private String title;
    private String description;
    private String extendedDescription;
    private String additionalInformation;
    private boolean hasCommentField;
    private List<Field> fields;

    public List<Field> getFields() {
        return fields;
    }
    public void setFields(List<Field> fields) {
        this.fields = fields;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public Integer getOrdinal() {
        return ordinal;
    }
    public void setOrdinal(int ordinal) {
        this.ordinal = ordinal;
    }
    public void setOrdinal(Integer ordinal) {
        this.ordinal = ordinal;
    }

    public Multiplicity getMultiplicity() {
        return multiplicity;
    }
    public void setMultiplicity(Multiplicity multiplicity) {
        this.multiplicity = multiplicity;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public String getExtendedDescription() {
        return extendedDescription;
    }
    public void setExtendedDescription(String extendedDescription) {
        this.extendedDescription = extendedDescription;
    }

    public boolean getHasCommentField() {
        return hasCommentField;
    }
    public void setHasCommentField(boolean hasCommentField) {
        this.hasCommentField = hasCommentField;
    }

    public String getAdditionalInformation() {
        return additionalInformation;
    }
    public void setAdditionalInformation(String additionalInformation) {
        this.additionalInformation = additionalInformation;
    }

    @Override
    public eu.eudat.models.data.entities.xmlmodels.datasetprofiledefinition.FieldSet toDatabaseDefinition(eu.eudat.models.data.entities.xmlmodels.datasetprofiledefinition.FieldSet item) {
        if (this.id == null || this.id.isEmpty()) this.id = "fieldSet_" + RandomStringUtils.random(5, true, true);
        List<eu.eudat.models.data.entities.xmlmodels.datasetprofiledefinition.Field> viewStyleFields = new ModelBuilder().toViewStyleDefinition(this.fields, eu.eudat.models.data.entities.xmlmodels.datasetprofiledefinition.Field.class);
        item.setFields(viewStyleFields);
        item.setId(this.id);
        item.setDescription(this.description);
        item.setTitle(this.title);
        item.setExtendedDescription(this.extendedDescription);
        item.setAdditionalInformation(this.additionalInformation);
        item.setOrdinal(this.ordinal);
        item.setMultiplicity(this.multiplicity);
        item.setHasCommentField(this.hasCommentField);

        return item;
    }

    @Override
    public void fromDatabaseDefinition(eu.eudat.models.data.entities.xmlmodels.datasetprofiledefinition.FieldSet item) {
        this.fields = new ModelBuilder().fromViewStyleDefinition(item.getFields(), Field.class);
        this.id = item.getId();
        this.ordinal = item.getOrdinal();
        this.description = item.getDescription();
        this.extendedDescription = item.getExtendedDescription();
        this.additionalInformation=item.getAdditionalInformation();
        this.title = item.getTitle();
        this.multiplicity = item.getMultiplicity();
        this.hasCommentField = item.getHasCommentField();
    }

    @Override
    public int compareTo(Object o) {
        return this.ordinal.compareTo(((FieldSet) o).ordinal);
    }


    public FieldSet toShort() {
        FieldSet shortenFieldSet = new FieldSet();
        shortenFieldSet.setId(this.id);
        shortenFieldSet.setMultiplicity(this.multiplicity);
        shortenFieldSet.setTitle(this.title);
        shortenFieldSet.setDescription(this.description);
        shortenFieldSet.setExtendedDescription(this.extendedDescription);
        shortenFieldSet.setAdditionalInformation(this.additionalInformation);
        shortenFieldSet.setHasCommentField(this.hasCommentField);

        List<Field> fieldToShort = this.fields;
        Collections.sort(fieldToShort);
        shortenFieldSet.setFields(fieldToShort);
        return shortenFieldSet;
    }

}
