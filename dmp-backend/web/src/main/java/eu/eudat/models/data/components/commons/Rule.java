package eu.eudat.models.data.components.commons;

import eu.eudat.models.data.entities.xmlmodels.modeldefinition.DatabaseModelDefinition;
import eu.eudat.logic.utilities.interfaces.XmlSerializable;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class Rule implements XmlSerializable<Rule>, DatabaseModelDefinition {
    private String ruleType;
    private String target;
    private String ruleStyle;
    private String value;
    private String valueType;

    public String getRuleType() {
        return ruleType;
    }

    public void setRuleType(String ruleType) {
        this.ruleType = ruleType;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getRuleStyle() {
        return ruleStyle;
    }

    public void setRuleStyle(String ruleStyle) {
        this.ruleStyle = ruleStyle;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValueType() {
        return valueType;
    }

    public void setValueType(String valueType) {
        this.valueType = valueType;
    }

    @Override
    public Element toXml(Document doc) {
        Element rule = doc.createElement("rule");
        rule.setAttribute("type", this.ruleType);
        rule.setAttribute("target", this.target);
        rule.setAttribute("ruleStyle", this.ruleStyle);

        Element value = doc.createElement("value");
        value.setAttribute("type", this.valueType);
        value.setTextContent(this.value);

        rule.appendChild(value);
        return rule;
    }

    @Override
    public Rule fromXml(Element item) {
        this.ruleType = item.getAttribute("type");
        this.target = item.getAttribute("target");
        this.ruleStyle = item.getAttribute("ruleStyle");

        Element value = (Element) item.getElementsByTagName("value").item(0);
        if (value != null) {
            this.valueType = value.getAttribute("type");
            this.value = value.getTextContent();
        }

        return this;
    }


}
