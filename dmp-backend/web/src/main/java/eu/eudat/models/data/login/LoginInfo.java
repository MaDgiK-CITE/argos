package eu.eudat.models.data.login;

import eu.eudat.logic.security.validators.TokenValidatorFactoryImpl;


public class LoginInfo {
    private String ticket;
    private TokenValidatorFactoryImpl.LoginProvider provider;
    private Object data;

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public TokenValidatorFactoryImpl.LoginProvider getProvider() {
        return provider;
    }

    public void setProvider(Integer provider) {
        this.provider = TokenValidatorFactoryImpl.LoginProvider.fromInteger(provider);
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
