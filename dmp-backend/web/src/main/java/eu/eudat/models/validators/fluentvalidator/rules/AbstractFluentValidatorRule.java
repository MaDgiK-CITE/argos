package eu.eudat.models.validators.fluentvalidator.rules;

import eu.eudat.models.validators.fluentvalidator.predicates.FieldSelector;

/**
 * Created by ikalyvas on 8/31/2018.
 */
public abstract class AbstractFluentValidatorRule<T> {
    private FieldSelector fieldSelector;

    public FieldSelector getFieldSelector() {
        return fieldSelector;
    }

    public AbstractFluentValidatorRule(FieldSelector fieldSelector) {
        this.fieldSelector = fieldSelector;
    }

    public abstract boolean assertValue(T item);
}
