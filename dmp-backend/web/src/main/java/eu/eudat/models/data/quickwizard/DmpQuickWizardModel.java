package eu.eudat.models.data.quickwizard;

import eu.eudat.data.entities.DescriptionTemplate;
import eu.eudat.data.entities.Grant;
import eu.eudat.data.entities.Project;
import eu.eudat.models.data.dmp.AssociatedProfile;
import eu.eudat.models.data.security.Principal;
import eu.eudat.models.data.userinfo.UserListingModel;

import java.util.*;


public class DmpQuickWizardModel {
	private UUID id;
	private String label;
	private int status;
	private AssociatedProfile datasetProfile;
	private String description;
	private eu.eudat.models.data.grant.Grant grant;
	private String language;


	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public void setDatasetProfile(AssociatedProfile datasetProfile) {
		this.datasetProfile = datasetProfile;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public eu.eudat.models.data.grant.Grant getGrant() {
		return grant;
	}

	public void setGrant(eu.eudat.models.data.grant.Grant grant) {
		this.grant = grant;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public eu.eudat.models.data.dmp.DataManagementPlan toDataDmp(Grant grant, Project project, Principal principal) {
		eu.eudat.models.data.dmp.DataManagementPlan dataManagementPlanEntity = new eu.eudat.models.data.dmp.DataManagementPlan();

		dataManagementPlanEntity.setId(this.id);
		dataManagementPlanEntity.setVersion(0);

		dataManagementPlanEntity.setLabel(this.label);
		if (grant != null) {
			eu.eudat.models.data.grant.Grant importGrant = new eu.eudat.models.data.grant.Grant();
			dataManagementPlanEntity.setGrant(importGrant.fromDataModel(grant));
		}
		if (project != null) {
			eu.eudat.models.data.project.Project importProject = new eu.eudat.models.data.project.Project();
			dataManagementPlanEntity.setProject(importProject.fromDataModel(project));
		}
		if (this.datasetProfile != null) {
			List<AssociatedProfile> assProfile = new LinkedList<>();
			assProfile.add(this.datasetProfile);
			dataManagementPlanEntity.setProfiles(assProfile);
		}
		dataManagementPlanEntity.setStatus((short) this.status);
		dataManagementPlanEntity.setDescription(this.description);
		dataManagementPlanEntity.setProperties(null);
		dataManagementPlanEntity.setCreated(new Date());
		List<UserListingModel> user = new LinkedList<>();
		eu.eudat.models.data.userinfo.UserInfo userInfo = new eu.eudat.models.data.userinfo.UserInfo();
		userInfo.setId(principal.getId());
		dataManagementPlanEntity.setAssociatedUsers(user);
		dataManagementPlanEntity.setExtraProperties(new HashMap<>());
		dataManagementPlanEntity.getExtraProperties().put("language", this.language);
		dataManagementPlanEntity.getExtraProperties().put("visible", false);
		dataManagementPlanEntity.getExtraProperties().put("contact", principal.getId().toString());
		return dataManagementPlanEntity;
	}


	public DescriptionTemplate getDatasetProfile() {
		DescriptionTemplate descriptionTemplate = new DescriptionTemplate();
		descriptionTemplate.setDefinition(this.datasetProfile.getLabel());
		descriptionTemplate.setLabel(this.datasetProfile.getLabel());
		descriptionTemplate.setId(this.datasetProfile.getDescriptionTemplateId());
		return descriptionTemplate;
	}


}
