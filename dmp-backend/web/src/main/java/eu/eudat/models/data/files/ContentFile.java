package eu.eudat.models.data.files;

import java.util.UUID;

/**
 * Created by ikalyvas on 3/15/2018.
 */
public class ContentFile {
    private String filename;
    private UUID id;
    private String location;
    private String type;

    public ContentFile() {
    }

    public ContentFile(String filename, UUID id, String location, String type) {
        this.filename = filename;
        this.id = id;
        this.location = location;
        this.type = type;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
