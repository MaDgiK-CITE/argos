package eu.eudat.models.validators.fluentvalidator;

/**
 * Created by ikalyvas on 8/31/2018.
 */
public enum Rule {
    NOT_EMPTY, GREATER, GREATER_THAN, EQUAL, LESS, LESS_THAN,
}
