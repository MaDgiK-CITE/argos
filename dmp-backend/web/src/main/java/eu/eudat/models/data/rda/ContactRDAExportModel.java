package eu.eudat.models.data.rda;

import eu.eudat.data.entities.UserInfo;

public class ContactRDAExportModel {
    private String mbox;
    private String name;
    private IdRDAExportModel contact_id;

    public String getMbox() {
        return mbox;
    }
    public void setMbox(String mbox) {
        this.mbox = mbox;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public IdRDAExportModel getContact_id() {
        return contact_id;
    }
    public void setContact_id(IdRDAExportModel contact_id) {
        this.contact_id = contact_id;
    }

    public ContactRDAExportModel fromDataModel(UserInfo entity) {
        ContactRDAExportModel contact = new ContactRDAExportModel();
        contact.mbox = entity.getEmail();
        contact.name = entity.getName();
        // TODO: we should use a contact_id and not our UUID.
        if (!entity.getId().toString().isEmpty()) {
            contact.contact_id = new IdRDAExportModel(entity.getId().toString(), "other");
        }
        else {
            contact.contact_id = null;
        }
        return contact;
    }
}
