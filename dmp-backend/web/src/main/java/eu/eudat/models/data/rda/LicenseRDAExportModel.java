package eu.eudat.models.data.rda;

public class LicenseRDAExportModel {
	private String license_ref;
	private String start_date; // If date is set in the future, it indicates embargo period.

	public String getLicense_ref() {
		return license_ref;
	}
	public void setLicense_ref(String license_ref) {
		this.license_ref = license_ref;
	}

	public String getStart_date() {
		return start_date;
	}
	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}
}
