package eu.eudat.models.data.funder;

import eu.eudat.models.DataModel;

import java.util.Date;
import java.util.UUID;

public class Funder implements DataModel<eu.eudat.data.entities.Funder, Funder> {

	private UUID id;
	private String label;
	private String reference;
	private String definition;
	private Short status;
	private Date created;
	private Date modified;
	private Integer type;
	private String source;
	private String key;

	public UUID getId() {
		return id;
	}
	public void setId(UUID id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}

	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getDefinition() {
		return definition;
	}
	public void setDefinition(String definition) {
		this.definition = definition;
	}

	public Short getStatus() {
		return status;
	}
	public void setStatus(Short status) {
		this.status = status;
	}

	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getModified() {
		return modified;
	}
	public void setModified(Date modified) {
		this.modified = modified;
	}

	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}

	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}

	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}

	@Override
	public Funder fromDataModel(eu.eudat.data.entities.Funder entity) {
		this.id = entity.getId();
		this.label = entity.getLabel();
		this.reference = entity.getReference();
		this.type = entity.getType();
		this.definition = entity.getDefinition();
		this.status = entity.getStatus();
		this.created = entity.getCreated();
		this.modified = entity.getModified();
		if (entity.getReference() != null) {
			String source = entity.getReference().substring(0, entity.getReference().indexOf(":"));
			if (source.equals("dmp")) {
				this.key = "Internal";
			} else {
				this.key = source;
			}
		}

		return this;
	}

	@Override
	public eu.eudat.data.entities.Funder toDataModel() {
		eu.eudat.data.entities.Funder entity = new eu.eudat.data.entities.Funder();
		if (this.getId() != null) {
			entity.setId(this.getId());
		} else {
			entity.setId(UUID.randomUUID());
		}
		entity.setLabel(this.label);
		entity.setType(this.type);
		// If internal, key has no value, fill it.
		if ((this.source != null && this.source.equals("Internal")) || (this.key != null && this.key.equals("Internal"))) this.key = "dmp";
		// Logic for the different "key" cases.
		if ((this.key == null || this.key.trim().isEmpty()) && (this.source != null && !this.source.trim().isEmpty())) {
			this.key = this.source;
		}
		if (this.reference != null && this.reference.startsWith("dmp:")) {
			entity.setReference(this.reference);
		} else if (this.reference != null && !this.reference.trim().isEmpty() && this.key != null && !this.key.trim().isEmpty()) {
			if (this.key.equals(this.reference.substring(0, this.key.length()))) {
				entity.setReference(this.reference);
			} else {
				entity.setReference(this.key.toLowerCase() + ":" + this.reference);
			}
		}
		entity.setDefinition(this.definition);
		entity.setCreated(this.created == null ? new Date() : this.created);
		entity.setStatus(this.status != null ? this.getStatus() : eu.eudat.data.entities.Grant.Status.ACTIVE.getValue());
		entity.setModified(new Date());
		return entity;
	}

	@Override
	public String getHint() {
		return null;
	}
}
