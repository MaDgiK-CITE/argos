package eu.eudat.models.data.saml2;

public class AuthnRequestModel {

    String authnRequestXml;
    String relayState;
    String algorithm;
    String signature;

    public AuthnRequestModel() {}

    public AuthnRequestModel(String authnRequestXml, String relayState, String algorithm, String signature) {
        this.authnRequestXml = authnRequestXml;
        this.relayState = relayState;
        this.algorithm = algorithm;
        this.signature = signature;
    }

    public String getAuthnRequestXml() {
        return authnRequestXml;
    }
    public void setAuthnRequestXml(String authnRequestXml) {
        this.authnRequestXml = authnRequestXml;
    }

    public String getRelayState() {
        return relayState;
    }
    public void setRelayState(String relayState) {
        this.relayState = relayState;
    }

    public String getAlgorithm() {
        return algorithm;
    }
    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    public String getSignature() {
        return signature;
    }
    public void setSignature(String signature) {
        this.signature = signature;
    }

}
