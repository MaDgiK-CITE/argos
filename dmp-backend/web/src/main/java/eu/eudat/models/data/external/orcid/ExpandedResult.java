package eu.eudat.models.data.external.orcid;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "expanded-result", namespace = "http://www.orcid.org/ns/expanded-search")
public class ExpandedResult {

	private String pid;
	private String givenNames;
	private String familyName;

	public ExpandedResult(String givenNames, String familyName) {
		this.givenNames = givenNames;
		this.familyName = familyName;
	}

	public ExpandedResult() {
	}

	@XmlElement(name = "orcid-id", namespace = "http://www.orcid.org/ns/expanded-search")
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}

	@XmlElement(name = "given-names", namespace = "http://www.orcid.org/ns/expanded-search")
	public String getGivenNames() {
		return givenNames;
	}
	public void setGivenNames(String givenNames) {
		this.givenNames = givenNames;
	}
	@XmlElement(name = "family-names", namespace = "http://www.orcid.org/ns/expanded-search")
	public String getFamilyName() {
		return familyName;
	}
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}
}
