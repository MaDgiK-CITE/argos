package eu.eudat.models.data.listingmodels;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.eudat.data.entities.DMP;
import eu.eudat.data.entities.DMPDatasetProfile;
import eu.eudat.data.entities.Dataset;
import eu.eudat.data.entities.DescriptionTemplate;
import eu.eudat.models.DataModel;
import eu.eudat.models.data.dataset.DatasetOverviewModel;
import eu.eudat.models.data.dmp.AssociatedProfile;
import eu.eudat.models.data.dmp.Organisation;
import eu.eudat.models.data.dmp.Researcher;
import eu.eudat.models.data.doi.Doi;
import eu.eudat.models.data.grant.GrantOverviewModel;

import java.util.*;
import java.util.stream.Collectors;

public class DataManagementPlanOverviewModel implements DataModel<DMP, DataManagementPlanOverviewModel> {
    private String id;
    private String label;
    private String profile;
    private GrantOverviewModel grant;
    private Date creationTime;
    private Date modifiedTime;
    private Date finalizedAt;
    private List<Organisation> organisations;
    private int version;
    private int status;
    private UUID groupId;
    private List<DatasetOverviewModel> datasets;
    private List<AssociatedProfile> associatedProfiles;
    private List<Researcher> researchers;
    private List<UserInfoListingModel> users;
    private String description;
    private boolean isPublic;
    private Date publishedAt;
    private List<Doi> dois;


    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }

    public String getProfile() {
        return profile;
    }
    public void setProfile(String profile) {
        this.profile = profile;
    }

    public GrantOverviewModel getGrant() {
        return grant;
    }
    public void setGrant(GrantOverviewModel grant) {
        this.grant = grant;
    }

    public Date getCreationTime() {
        return creationTime;
    }
    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public Date getModifiedTime() {
        return modifiedTime;
    }
    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    public Date getFinalizedAt() {
        return finalizedAt;
    }
    public void setFinalizedAt(Date finalizedAt) {
        this.finalizedAt = finalizedAt;
    }

    public List<Organisation> getOrganisations() {
        return organisations;
    }
    public void setOrganisations(List<Organisation> organizations) {
        this.organisations = organizations;
    }

    public int getVersion() {
        return version;
    }
    public void setVersion(int version) {
        this.version = version;
    }

    public UUID getGroupId() {
        return groupId;
    }
    public void setGroupId(UUID groupId) {
        this.groupId = groupId;
    }

    public List<DatasetOverviewModel> getDatasets() {
        return datasets;
    }
    public void setDatasets(List<DatasetOverviewModel> datasets) {
        this.datasets = datasets;
    }

    public int getStatus() {
        return status;
    }
    public void setStatus(int status) {
        this.status = status;
    }

    public List<AssociatedProfile> getAssociatedProfiles() {
        return associatedProfiles;
    }
    public void setAssociatedProfiles(List<AssociatedProfile> associatedProfiles) {
        this.associatedProfiles = associatedProfiles;
    }

    public List<UserInfoListingModel> getUsers() {
        return users;
    }
    public void setUsers(List<UserInfoListingModel> users) {
        this.users = users;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public List<Researcher> getResearchers() {
        return researchers;
    }
    public void setResearchers(List<Researcher> researchers) {
        this.researchers = researchers;
    }

    public boolean getIsPublic() {
        return isPublic;
    }
    public void setIsPublic(boolean aPublic) {
        isPublic = aPublic;
    }

    public Date getPublishedAt() {
        return publishedAt;
    }
    public void setPublishedAt(Date publishedAt) {
        this.publishedAt = publishedAt;
    }

    public List<Doi> getDois() {
        return dois;
    }
    public void setDois(List<Doi> dois) {
        this.dois = dois;
    }

    @Override
    public DataManagementPlanOverviewModel fromDataModel(DMP entity) {
        this.id = entity.getId().toString();
        this.label = entity.getLabel();
        this.groupId = entity.getGroupId();
        this.status = entity.getStatus();
        if (entity.getResearchers() != null) {
            this.researchers = entity.getResearchers().stream().map(item -> new Researcher().fromDataModel(item)).collect(Collectors.toList());
        }
        if(entity.getDois() != null){
            this.dois = entity.getDois().stream().map(item -> new Doi().fromDataModel(item)).collect(Collectors.toList());
        }
        return this;
    }

    public DataManagementPlanOverviewModel fromDataModelDatasets(DMP entity) {
        this.fromDataModel(entity);
        this.version = entity.getVersion();
        if (entity.getGrant() != null) this.grant = new GrantOverviewModel().fromDataModel(entity.getGrant());
        if (entity.getProfile() != null) this.profile = entity.getProfile().getLabel();
        this.creationTime = entity.getCreated();
        this.modifiedTime = entity.getModified();
        this.finalizedAt = entity.getFinalizedAt();
        this.organisations = entity.getOrganisations().stream().map(item -> new Organisation().fromDataModel(item)).collect(Collectors.toList());
        this.datasets = entity.getDataset().stream().filter(dataset -> !dataset.getStatus().equals(Dataset.Status.DELETED.getValue()) && !dataset.getStatus().equals(Dataset.Status.CANCELED.getValue())).map(x-> new DatasetOverviewModel().fromDataModel(x)).collect(Collectors.toList());
        this.users = entity.getUsers().stream().map(x -> new UserInfoListingModel().fromDataModel(x)).collect(Collectors.toList());
        this.description = entity.getDescription();
        if (entity.getResearchers() != null) {
            this.researchers = entity.getResearchers().stream().map(item -> new Researcher().fromDataModel(item)).collect(Collectors.toList());
        }

        if (entity.getAssociatedDmps() != null && !entity.getAssociatedDmps().isEmpty()) {
            this.associatedProfiles = new LinkedList<>();
            for (DMPDatasetProfile dmpDescriptionProfile : entity.getAssociatedDmps()) {
                AssociatedProfile associatedProfile = new AssociatedProfile().fromData(dmpDescriptionProfile.getDatasetprofile());
                try {
                    associatedProfile.setData(new ObjectMapper().readValue(dmpDescriptionProfile.getData(), new TypeReference<Map<String, Object>>() {}));
                }
                catch (Exception e) {
                    associatedProfile.setData(null);
                }
                this.associatedProfiles.add(associatedProfile);
            }
        }
        this.isPublic = entity.isPublic();
        this.publishedAt = entity.getPublishedAt();
        this.dois = entity.getDois().stream().map(item -> new Doi().fromDataModel(item)).collect(Collectors.toList());

        return this;
    }

    @Override
    public DMP toDataModel() {
        return null;
    }

    @Override
    public String getHint() {
        return "dataManagementPlanOverviewModel";
    }
}
