package eu.eudat.models.data.dashboard.recent.model;

import eu.eudat.models.DataModel;
import eu.eudat.models.data.datasetprofile.DatasetProfileOverviewModel;
import eu.eudat.models.data.listingmodels.UserInfoListingModel;
import eu.eudat.queryable.queryableentity.DataEntity;

import java.util.Date;
import java.util.List;

public abstract class RecentActivityModel<T extends DataEntity, S extends DataModel> implements DataModel<T, S> {
	private String id;
	private String title;
	private Date created;
	private Date modified;
	private int status;
	private int version;
	private String grant;
	private Date finalizedAt;
	private Date publishedAt;
	private DatasetProfileOverviewModel profile;
	private int type;
	private List<UserInfoListingModel> users;
	private Boolean isPublic;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getModified() {
		return modified;
	}

	public void setModified(Date modified) {
		this.modified = modified;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getGrant() {
		return grant;
	}

	public void setGrant(String grant) {
		this.grant = grant;
	}

	public Date getFinalizedAt() {
		return finalizedAt;
	}

	public void setFinalizedAt(Date finalizedAt) {
		this.finalizedAt = finalizedAt;
	}

	public Date getPublishedAt() {
		return publishedAt;
	}

	public void setPublishedAt(Date publishedAt) {
		this.publishedAt = publishedAt;
	}

	public DatasetProfileOverviewModel getProfile() {
		return profile;
	}

	public void setProfile(DatasetProfileOverviewModel profile) {
		this.profile = profile;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public List<UserInfoListingModel> getUsers() {
		return users;
	}

	public void setUsers(List<UserInfoListingModel> users) {
		this.users = users;
	}

	public Boolean getPublic() {
		return isPublic;
	}

	public void setPublic(Boolean aPublic) {
		isPublic = aPublic;
	}

	public abstract RecentActivityModel fromEntity(T entity);

	public enum RecentActivityType {
		DMP(2), DATASET(1);

		private final int index;

		RecentActivityType(int index) {
			this.index = index;
		}

		public int getIndex() {
			return index;
		}

		public static RecentActivityType fromIndex(int index) {
			switch (index) {
				case 2:
					return DMP;
				case 1:
					return DATASET;
				default:
					throw new IllegalArgumentException("Recent Activity Type : \"" + index + "\" is not supported.");
			}

		}
	}
}
