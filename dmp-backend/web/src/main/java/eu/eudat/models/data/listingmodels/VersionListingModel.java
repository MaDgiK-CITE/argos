package eu.eudat.models.data.listingmodels;

import eu.eudat.data.entities.DMP;
import eu.eudat.models.DataModel;

public class VersionListingModel implements DataModel<DMP, VersionListingModel> {
	private String id;
	private String groupId;
	private Integer version;


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	@Override
	public VersionListingModel fromDataModel(DMP entity) {
		this.id = entity.getId().toString();
		this.groupId = entity.getGroupId().toString();
		this.version = entity.getVersion();
		return this;
	}

	@Override
	public DMP toDataModel() throws Exception {
		return null;
	}

	@Override
	public String getHint() {
		return "versionListingModel";
	}
}
