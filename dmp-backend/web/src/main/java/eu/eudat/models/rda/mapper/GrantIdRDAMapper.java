package eu.eudat.models.rda.mapper;

import eu.eudat.models.rda.GrantId;

public class GrantIdRDAMapper {

	public static GrantId toRDA(String id) {
		GrantId rda = new GrantId();
		rda.setIdentifier(id);
		rda.setType(GrantId.Type.OTHER);
		return rda;
	}
}
