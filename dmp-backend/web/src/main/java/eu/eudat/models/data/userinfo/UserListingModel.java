package eu.eudat.models.data.userinfo;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.eudat.data.entities.UserInfo;
import eu.eudat.models.DataModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;


public class UserListingModel implements DataModel<eu.eudat.data.entities.UserInfo, UserListingModel> {
	private static final Logger logger = LoggerFactory.getLogger(UserListingModel.class);

    private UUID id;
    private String email;
    private Short usertype;
    private Boolean verified_email;
    private String name;
    private Date created;
    private Date lastloggedin;
    //private String additionalinfo;
    private List<Integer> appRoles;
    private String avatarUrl;

    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public Short getUsertype() {
        return usertype;
    }
    public void setUsertype(Short usertype) {
        this.usertype = usertype;
    }

    public Boolean getVerified_email() {
        return verified_email;
    }
    public void setVerified_email(Boolean verified_email) {
        this.verified_email = verified_email;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public Date getCreated() {
        return created;
    }
    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastloggedin() {
        return lastloggedin;
    }
    public void setLastloggedin(Date lastloggedin) {
        this.lastloggedin = lastloggedin;
    }

    /*public String getAdditionalinfo() {
        return additionalinfo;
    }
    public void setAdditionalinfo(String additionalinfo) {
        this.additionalinfo = additionalinfo;
    }*/

    public List<Integer> getAppRoles() {
        return appRoles;
    }
    public void setAppRoles(List<Integer> appRoles) {
        this.appRoles = appRoles;
    }

    public String getAvatarUrl() {
		return avatarUrl;
	}
	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}
	@Override
    public UserListingModel fromDataModel(UserInfo entity) {
        this.id = entity.getId();
        this.email = entity.getEmail();
        this.usertype = entity.getUsertype();
        this.verified_email = entity.getVerified_email();
        this.name = entity.getName();
        this.created = entity.getCreated();
        this.lastloggedin = entity.getLastloggedin();
       // this.additionalinfo = entity.getAdditionalinfo();
        this.appRoles = entity.getUserRoles().stream().map(item -> item.getRole()).collect(Collectors.toList());
        if (entity.getAdditionalinfo() != null) {
            try {
                Map<String, Object> additionalInfo = new ObjectMapper().readValue(entity.getAdditionalinfo(), HashMap.class);
                this.avatarUrl = (String) additionalInfo.get("avatarUrl");
            } catch (IOException e) {
                logger.error(e.getMessage(), e);
            }
        }
        return this;
    }

    @Override
    public UserInfo toDataModel() throws Exception {
        UserInfo userInfo = new UserInfo();
        userInfo.setId(this.id);
        userInfo.setEmail(this.email);
        userInfo.setUsertype(this.usertype);
        userInfo.setVerified_email(this.verified_email);
        userInfo.setName(this.name);
        userInfo.setCreated(this.created);
        userInfo.setLastloggedin(this.lastloggedin);
       // userInfo.setAdditionalinfo(this.additionalinfo);

        return userInfo;
    }

    @Override
    public String getHint() {
        return "userInfo";
    }
}
