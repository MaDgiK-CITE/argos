package eu.eudat.models.data.external.orcid;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "expanded-search", namespace = "http://www.orcid.org/ns/expanded-search")
public class ExpandedSearch {
	List<ExpandedResult> expandedResults;

	@XmlElement(name = "expanded-result", namespace = "http://www.orcid.org/ns/expanded-search")
	public List<ExpandedResult> getExpandedResults() {
		return expandedResults;
	}
	public void setExpandedResults(List<ExpandedResult> expandedResults) {
		this.expandedResults = expandedResults;
	}
}
