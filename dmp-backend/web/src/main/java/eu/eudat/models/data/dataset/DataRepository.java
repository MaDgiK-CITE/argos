package eu.eudat.models.data.dataset;

import eu.eudat.models.DataModel;
import eu.eudat.logic.utilities.helpers.LabelGenerator;

import java.util.Date;
import java.util.UUID;

public class DataRepository implements DataModel<eu.eudat.data.entities.DataRepository, DataRepository>, LabelGenerator {
    private String id;
    private String pid;
    private String name;
    private String uri;
    private String info;
    private String reference;
    private String abbreviation;
    private String tag;
    private String source;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getPid() {
        return pid;
    }
    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getUri() {
        return uri;
    }
    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }

    public String getReference() {
        return reference;
    }
    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getAbbreviation() {
        return abbreviation;
    }
    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getTag() {
        return tag;
    }
    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getSource() {
        return source;
    }
    public void setSource(String source) {
        this.source = source;
    }

    public DataRepository fromDataModel(eu.eudat.data.entities.DataRepository entity) {
        this.id = entity.getId().toString();
        this.pid = entity.getReference();
        this.name = entity.getLabel();
        this.uri = entity.getUri();
        this.abbreviation = entity.getAbbreviation();
        this.reference = entity.getReference();
        String source1 = entity.getReference().substring(0, entity.getReference().indexOf(":"));
        if (source1.equals("dmp")) {
            this.source = "Internal";
        } else {
            this.source = source1;
        }
        return this;
    }

    public eu.eudat.data.entities.DataRepository toDataModel() {
        eu.eudat.data.entities.DataRepository entity = new eu.eudat.data.entities.DataRepository();
        if (this.id != null) {
            entity.setId(UUID.fromString(this.id));
        }
        entity.setReference(this.pid);
        entity.setLabel(this.name);
        entity.setUri(this.uri);
        entity.setCreated(new Date());
        entity.setModified(new Date());
        entity.setStatus((short) 0);
        if (this.source != null && this.source.equals("Internal")) this.source = "dmp";
        if (this.reference != null && !this.reference.trim().isEmpty()
                && this.source != null && !this.source.trim().isEmpty()) {
            if (this.source.equals(this.reference.substring(0, this.source.length()))) {
                entity.setReference(this.reference);
            } else {
                entity.setReference(this.source.toLowerCase() + ":" + this.reference);
            }
        }

        if (this.abbreviation != null)
            entity.setAbbreviation(this.abbreviation);

        return entity;
    }

    @Override
    public String generateLabel() {
        return this.getName();
    }

    @Override
    public String getHint() {
        return null;
    }
}
