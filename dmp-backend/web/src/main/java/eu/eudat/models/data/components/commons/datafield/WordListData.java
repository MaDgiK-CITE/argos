package eu.eudat.models.data.components.commons.datafield;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class WordListData extends ComboBoxData<WordListData> {
    private List<Option> options;
    private Boolean multiList;

    public List<Option> getOptions() {
        return options;
    }

    public void setOptions(List<Option> options) {
        this.options = options;
    }

    public Boolean getMultiList() {
        return multiList;
    }

    public void setMultiList(Boolean multiList) {
        this.multiList = multiList;
    }

    @Override
    public Element toXml(Document doc) {
        Element root = super.toXml(doc);
        root.setAttribute("multiList", this.multiList != null ? this.multiList.toString() : "false");
        Element element = doc.createElement("options");
        if (this.options != null) {
            for (Option option : this.options) {
                element.appendChild(option.toXml(doc));
            }
        }
        root.appendChild(element);
        return root;
    }

    @Override
    public WordListData fromXml(Element item) {
        super.fromXml(item);
        this.options = new LinkedList<>();
        Element optionsElement = (Element) item.getElementsByTagName("options").item(0);

        if (optionsElement != null) {
            NodeList optionElements = optionsElement.getChildNodes();
            for (int temp = 0; temp < optionElements.getLength(); temp++) {
                Node optionElement = optionElements.item(temp);
                if (optionElement.getNodeType() == Node.ELEMENT_NODE) {
                    this.options.add(new Option().fromXml((Element) optionElement));
                }
            }
        }
        Boolean temp = Boolean.parseBoolean(item.getAttribute("multiList"));
        this.multiList = temp != null ? temp : false;
        return this;
    }

    @Override
    public WordListData fromData(Object data) {
        super.fromData(data);
        this.options = new LinkedList();
        if (data != null) {
            List<Map<String, String>> options = ((Map<String, List<Map<String, String>>>) data).get("options");
            if (options != null) {
                for (Map<String, String> map : options) {
                    Option newOption = new Option();
                    newOption.setLabel(map.get("label"));
                    newOption.setValue(map.get("value"));
                    this.options.add(newOption);
                }
            }
            Object multiList1 = ((Map<String, Object>) data).get("multiList");
            this.multiList = multiList1 != null && (multiList1 instanceof String ? Boolean.parseBoolean((String) multiList1) : (Boolean) multiList1);
        }
        return this;
    }

    @Override
    public Object toData() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Map<String, Object> toMap(Element item) {
        HashMap dataMap = new HashMap();
        dataMap.put("multiList", item != null ? item.getAttribute("multiList") : "false");
        dataMap.put("label", item != null ? item.getAttribute("label") : "");
        dataMap.put("type", item != null ? item.getAttribute("type") : "wordlist");
        Element optionsElement = (Element) item.getElementsByTagName("options").item(0);
        List<Map<String,String>> option =new LinkedList<>();

        if (optionsElement != null) {
            NodeList optionElements = optionsElement.getChildNodes();
            for (int temp = 0; temp < optionElements.getLength(); temp++) {
                Node optionElement = optionElements.item(temp);
                if (optionElement.getNodeType() == Node.ELEMENT_NODE) {
                    option.add(optionToMap((Element) optionElement));
                }
            }
        }

        dataMap.put("options", option != null ? option : null);
        return dataMap;
    }

    private Map<String, String> optionToMap(Element item){
        HashMap dataMap = new HashMap();
        dataMap.put("label",item.getAttribute("label"));
        dataMap.put("value",item.getAttribute("value"));

        return dataMap;
    }
}
