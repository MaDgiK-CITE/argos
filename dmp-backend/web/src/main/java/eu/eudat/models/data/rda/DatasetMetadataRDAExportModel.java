package eu.eudat.models.data.rda;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class DatasetMetadataRDAExportModel {
	private String description; // Not mandatory.
	private String language;
	private IdRDAExportModel metadata_standard_id;

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}

	public IdRDAExportModel getMetadata_standard_id() {
		return metadata_standard_id;
	}
	public void setMetadata_standard_id(IdRDAExportModel metadata_standard_id) {
		this.metadata_standard_id = metadata_standard_id;
	}

	public DatasetMetadataRDAExportModel fromDataModel(String key, Object value) {
		DatasetMetadataRDAExportModel metadataRDAExportModel = new DatasetMetadataRDAExportModel();
		if (key.contains("metadata_standard_id"))
			metadataRDAExportModel.setMetadata_standard_id(new IdRDAExportModel(value.toString(), "other"));
		else if (key.contains("language"))
			metadataRDAExportModel.setLanguage(value.toString());
		else if (key.contains("description"))
			metadataRDAExportModel.setDescription(value.toString());

		return metadataRDAExportModel;
	}

	@JsonIgnore
	public boolean isValid() {
		return description != null || language != null || metadata_standard_id != null;
	}
}
