package eu.eudat.models.data.quickwizard;

import eu.eudat.data.entities.DMP;

public class DatasetCreateWizardModel {

    private DmpCreateWizardModel dmpMeta;
    private DatasetQuickWizardModel datasets;

    public DmpCreateWizardModel getDmpMeta() { return dmpMeta; }
    public void setDmpMeta(DmpCreateWizardModel dmpMeta) { this.dmpMeta = dmpMeta; }

    public DatasetQuickWizardModel getDatasets() {
        return datasets;
    }
    public void setDatasets(DatasetQuickWizardModel datasets) {
        this.datasets = datasets;
    }
}
