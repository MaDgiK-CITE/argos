package eu.eudat.models.data.datasetImport;

import java.util.List;

public class DatasetImportVisibility {
    private List<DatasetImportRule> rules;
    private String style;

    public List<DatasetImportRule> getRules() {
        return rules;
    }
    public void setRules(List<DatasetImportRule> rules) {
        this.rules = rules;
    }

    public String getStyle() {
        return style;
    }
    public void setStyle(String style) {
        this.style = style;
    }
}
