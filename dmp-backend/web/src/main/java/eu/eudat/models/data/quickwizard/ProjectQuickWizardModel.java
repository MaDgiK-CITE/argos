package eu.eudat.models.data.quickwizard;

import eu.eudat.models.data.project.Project;

import java.util.UUID;

public class ProjectQuickWizardModel {
	private String label;
	private String description;
	private Project existProject;

	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public Project getExistProject() {
		return existProject;
	}
	public void setExistProject(Project existProject) {
		this.existProject = existProject;
	}

	public Project toDataProject() {
		Project toProject = new Project();
		toProject.setId(UUID.randomUUID());
		toProject.setAbbreviation("");
		toProject.setLabel(this.label);
		toProject.setReference("dmp:" + toProject.getId());
		toProject.setUri("");
		toProject.setDefinition("");
		toProject.setDescription(this.description);
		return toProject;
	}
}
