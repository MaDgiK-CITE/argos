package eu.eudat.models.data.rda;

public class DatasetSecurityAndPrivacyRDAExportModel {
	private String description;
	private String title;

	public DatasetSecurityAndPrivacyRDAExportModel() {

	}

	public DatasetSecurityAndPrivacyRDAExportModel(String description, String title) {
		this.description = description;
		this.title = title;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
}
