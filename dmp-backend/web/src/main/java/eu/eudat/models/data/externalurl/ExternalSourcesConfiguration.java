package eu.eudat.models.data.externalurl;

import java.util.List;

/**
 * Created by ikalyvas on 5/17/2018.
 */
public class ExternalSourcesConfiguration {

    public static class ExternalSourcesUrlModel{
        private String key;
        private String label;

        public ExternalSourcesUrlModel(String key, String label) {
            this.key = key;
            this.label = label;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }
    }

    private List<ExternalSourcesUrlModel> registries;
    private List<ExternalSourcesUrlModel> dataRepositories;
    private List<ExternalSourcesUrlModel> services;
    private List<ExternalSourcesUrlModel> externalDatasets;
   /* private List<ExternalSourcesUrlModel> tags;

    public List<ExternalSourcesUrlModel> getTags() {
        return tags;
    }

    public void setTags(List<ExternalSourcesUrlModel> tags) {
        this.tags = tags;
    }*/

    public List<ExternalSourcesUrlModel> getRegistries() {
        return registries;
    }

    public void setRegistries(List<ExternalSourcesUrlModel> registries) {
        this.registries = registries;
    }

    public List<ExternalSourcesUrlModel> getDataRepositories() {
        return dataRepositories;
    }

    public void setDataRepositories(List<ExternalSourcesUrlModel> dataRepositories) {
        this.dataRepositories = dataRepositories;
    }

    public List<ExternalSourcesUrlModel> getServices() {
        return services;
    }

    public void setServices(List<ExternalSourcesUrlModel> services) {
        this.services = services;
    }

    public List<ExternalSourcesUrlModel> getExternalDatasets() {
        return externalDatasets;
    }

    public void setExternalDatasets(List<ExternalSourcesUrlModel> externalDatasets) {
        this.externalDatasets = externalDatasets;
    }
}
