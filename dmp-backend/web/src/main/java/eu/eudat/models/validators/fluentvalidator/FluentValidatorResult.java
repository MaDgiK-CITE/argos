package eu.eudat.models.validators.fluentvalidator;

import java.util.List;

/**
 * Created by ikalyvas on 8/31/2018.
 */
public class FluentValidatorResult {
    private String field;
    private String error;

    public FluentValidatorResult(String field, String error) {
        this.field = field;
        this.error = error;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
