package eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition;

import eu.eudat.logic.utilities.builders.XmlBuilder;
import eu.eudat.logic.utilities.interfaces.XmlSerializable;
import eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition.types.FieldCategory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class Section implements XmlSerializable<Section> {

    private UUID id;
    private String label;
    private String description;
    private Integer ordinal;
    private List<FieldModel> fields;
    private Boolean hasTemplates;
    private List<DescriptionTemplate> descriptionTemplates;

    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getOrdinal() {
        return ordinal;
    }
    public void setOrdinal(Integer ordinal) {
        this.ordinal = ordinal;
    }

    public List<FieldModel> getFields() {
        return fields;
    }
    public void setFields(List<FieldModel> fields) {
        this.fields = fields;
    }

    public Boolean getHasTemplates() {
        return hasTemplates;
    }
    public void setHasTemplates(Boolean hasTemplates) {
        this.hasTemplates = hasTemplates;
    }

    public List<DescriptionTemplate> getDescriptionTemplates() {
        return descriptionTemplates;
    }
    public void setDescriptionTemplates(List<DescriptionTemplate> descriptionTemplates) {
        this.descriptionTemplates = descriptionTemplates;
    }

    @Override
    public Element toXml(Document doc) {
        Element rootElement = doc.createElement("section");
        rootElement.setAttribute("id", this.getId().toString());
        rootElement.setAttribute("label", this.label);
        rootElement.setAttribute("description", this.description);
        rootElement.setAttribute("ordinal", String.valueOf(this.ordinal));
        rootElement.setAttribute("hasTemplates", String.valueOf(this.hasTemplates));
        List<FieldModel> temp = this.fields.stream().filter(f -> f.getCategory().equals(FieldCategory.SYSTEM)).collect(Collectors.toList());
        List<SystemField> systemFieldsList = temp.stream().map(FieldModel::toSystemField).collect(Collectors.toList());
        Element systemFields = doc.createElement("systemFields");
        for (SystemField systemField : systemFieldsList) {
            systemFields.appendChild(systemField.toXml(doc));
        }
        rootElement.appendChild(systemFields);
        Element descriptionTemplates = doc.createElement("descriptionTemplates");
        for (DescriptionTemplate descriptionTemplate : this.descriptionTemplates) {
            descriptionTemplates.appendChild(descriptionTemplate.toXml(doc));
        }
        rootElement.appendChild(descriptionTemplates);
        temp = this.fields.stream().filter(f -> f.getCategory().equals(FieldCategory.EXTRA)).collect(Collectors.toList());
        List<ExtraField> extraFieldList = temp.stream().map(FieldModel::toExtraField).collect(Collectors.toList());
        Element extraFields = doc.createElement("extraFields");
        for (ExtraField extraField : extraFieldList) {
            extraFields.appendChild(extraField.toXml(doc));
        }
        rootElement.appendChild(extraFields);

        return rootElement;
    }

    @Override
    public Section fromXml(Element item) {
        this.id = UUID.fromString(item.getAttribute("id"));
        this.label = item.getAttribute("label");
        this.description = item.getAttribute("description");
        this.ordinal = Integer.valueOf(item.getAttribute("ordinal"));
        this.hasTemplates = Boolean.valueOf(item.getAttribute("hasTemplates"));
        this.fields = new LinkedList<>();
        Element systemFields = (Element) XmlBuilder.getNodeFromListByTagName(item.getChildNodes(), "systemFields");
        if (systemFields != null) {
            NodeList systemFieldElements = systemFields.getChildNodes();
            for (int temp = 0; temp < systemFieldElements.getLength(); temp++) {
                Node systemFieldElement = systemFieldElements.item(temp);
                if (systemFieldElement.getNodeType() == Node.ELEMENT_NODE) {
                    SystemField systemField = new SystemField().fromXml((Element) systemFieldElement);
                    this.fields.add(new FieldModel().fromSystemField(systemField));
                }
            }
        }
        this.descriptionTemplates = new LinkedList<>();
        Element descriptionTemplates = (Element) XmlBuilder.getNodeFromListByTagName(item.getChildNodes(), "descriptionTemplates");
        if (descriptionTemplates != null) {
            NodeList descriptionTemplateElements = descriptionTemplates.getChildNodes();
            for (int temp = 0; temp < descriptionTemplateElements.getLength(); temp++) {
                Node descriptionTemplateElement = descriptionTemplateElements.item(temp);
                if (descriptionTemplateElement.getNodeType() == Node.ELEMENT_NODE) {
                    this.descriptionTemplates.add(new DescriptionTemplate().fromXml((Element) descriptionTemplateElement));
                }
            }
        }
        Element extraFields = (Element) XmlBuilder.getNodeFromListByTagName(item.getChildNodes(), "extraFields");
        if (extraFields != null) {
            NodeList extraFieldElements = extraFields.getChildNodes();
            for (int temp = 0; temp < extraFieldElements.getLength(); temp++) {
                Node extraFieldElement = extraFieldElements.item(temp);
                if (extraFieldElement.getNodeType() == Node.ELEMENT_NODE) {
                    ExtraField extraField = new ExtraField().fromXml((Element) extraFieldElement);
                    this.fields.add(new FieldModel().fromExtraField(extraField));
                }
            }
        }
        return this;
    }
}
