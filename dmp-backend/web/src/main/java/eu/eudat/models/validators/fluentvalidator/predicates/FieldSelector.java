package eu.eudat.models.validators.fluentvalidator.predicates;

/**
 * Created by ikalyvas on 8/28/2018.
 */
public interface FieldSelector<T,R> {
    R apply(T item);
}
