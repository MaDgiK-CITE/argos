package eu.eudat.models.data.properties;

import java.util.List;
import java.util.Map;

public class Group implements PropertiesGenerator {
    private List<FieldSet> compositeFields;


    public List<FieldSet> getCompositeFields() {
        return compositeFields;
    }

    public void setCompositeFields(List<FieldSet> compositeFields) {
        this.compositeFields = compositeFields;
    }

    @Override
    public void toMap(Map<String, Object> fieldValues) {
        this.compositeFields.forEach(item -> item.toMap(fieldValues));
    }

    @Override
    public void toMap(Map<String, Object> fieldValues, int index) {
        // TODO Auto-generated method stub

    }

}
