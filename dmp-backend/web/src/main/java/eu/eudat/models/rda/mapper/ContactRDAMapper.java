package eu.eudat.models.rda.mapper;

import eu.eudat.data.entities.UserInfo;
import eu.eudat.models.rda.Contact;
import eu.eudat.models.rda.ContactId;

public class ContactRDAMapper {

	public static Contact toRDA(UserInfo creator) {
		Contact rda = new Contact();
		if (creator.getName() == null) {
			throw new IllegalArgumentException("Contact Name is missing");
		}
		rda.setName(creator.getName());
		if (creator.getEmail() == null) {
			throw new IllegalArgumentException("Contact Email is missing");
		}
		rda.setMbox(creator.getEmail());
		rda.setContactId(ContactIdRDAMapper.toRDA(creator.getId()));
		return rda;
	}

	public static UserInfo toEntity(Contact rda) {
		UserInfo entity = new UserInfo();
		entity.setId(ContactIdRDAMapper.toEntity(rda.getContactId()));
		entity.setName(rda.getName());
		entity.setEmail(rda.getMbox());
		return entity;
	}
}
