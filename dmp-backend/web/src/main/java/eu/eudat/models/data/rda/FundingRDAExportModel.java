package eu.eudat.models.data.rda;

import eu.eudat.data.entities.Funder;
import eu.eudat.data.entities.Grant;

public class FundingRDAExportModel {
	private IdRDAExportModel funder_id;
	private IdRDAExportModel grant_id;
	private String funding_status;

	public IdRDAExportModel getFunder_id() {
		return funder_id;
	}
	public void setFunder_id(IdRDAExportModel funder_id) {
		this.funder_id = funder_id;
	}

	public IdRDAExportModel getGrant_id() {
		return grant_id;
	}
	public void setGrant_id(IdRDAExportModel grant_id) {
		this.grant_id = grant_id;
	}

	public String getFunding_status() {
		return funding_status;
	}
	public void setFunding_status(String funding_status) {
		this.funding_status = funding_status;
	}

	public FundingRDAExportModel fromDataModel(Funder funder, Grant grant) {
		FundingRDAExportModel funding = new FundingRDAExportModel();
		funding.funding_status = "planned"; // mock data
		if (funder != null) {
			funding.funder_id = new IdRDAExportModel(funder.getReference(), "other");
		}
		if (grant != null) {
			funding.grant_id = new IdRDAExportModel(grant.getReference(), "other");
		}
		return funding;
	}
}
