package eu.eudat.models.data.listingmodels;

import eu.eudat.data.entities.DMPProfile;
import eu.eudat.models.DataModel;
import eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition.DataManagementPlanProfile;
import eu.eudat.logic.utilities.builders.XmlBuilder;
import org.w3c.dom.Document;

import java.util.Date;
import java.util.UUID;

/**
 * Created by ikalyvas on 3/21/2018.
 */
public class DataManagementPlanProfileListingModel implements DataModel<DMPProfile, DataManagementPlanProfileListingModel> {

    private UUID id;

    private String label;

    private DataManagementPlanProfile definition;

    private int status;

    private Date created = null;

    private Date modified = new Date();

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public DataManagementPlanProfile getDefinition() {
        return definition;
    }

    public void setDefinition(DataManagementPlanProfile definition) {
        this.definition = definition;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    @Override
    public DataManagementPlanProfileListingModel fromDataModel(DMPProfile entity) {
        this.id = entity.getId();
        this.created = entity.getCreated();
        this.definition = new DataManagementPlanProfile().fromXml(XmlBuilder.fromXml(entity.getDefinition()).getDocumentElement());
        this.modified = entity.getModified();
        this.label = entity.getLabel();
        this.status = entity.getStatus();
        return this;
    }

    @Override
    public DMPProfile toDataModel() throws Exception {
        Document document = XmlBuilder.getDocument();
        document.appendChild(this.definition.toXml(document));
        DMPProfile dmpProfile = new DMPProfile();
        dmpProfile.setCreated(this.created == null ? new Date() : this.created);
        dmpProfile.setDefinition(XmlBuilder.generateXml(document));
        dmpProfile.setId(this.id);
        dmpProfile.setLabel(this.label);
        dmpProfile.setStatus(this.status);
        dmpProfile.setModified(this.modified == null ? new Date() : this.modified);
        return dmpProfile;
    }

    @Override
    public String getHint() {
        return null;
    }
}
