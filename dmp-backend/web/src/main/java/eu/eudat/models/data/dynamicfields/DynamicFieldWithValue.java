package eu.eudat.models.data.dynamicfields;

import eu.eudat.models.data.helpermodels.Tuple;

import java.util.List;

/**
 * Created by ikalyvas on 3/27/2018.
 */
public class DynamicFieldWithValue {
    private String id;
    private String name;
    private boolean required;
    private String queryProperty;
    private Tuple<String,String> value;
    private List<Dependency> dependencies;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQueryProperty() {
        return queryProperty;
    }

    public void setQueryProperty(String queryProperty) {
        this.queryProperty = queryProperty;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public List<Dependency> getDependencies() {
        return dependencies;
    }

    public void setDependencies(List<Dependency> dependencies) {
        this.dependencies = dependencies;
    }

    public Tuple<String, String> getValue() {
        return value;
    }

    public void setValue(Tuple<String, String> value) {
        this.value = value;
    }
}
