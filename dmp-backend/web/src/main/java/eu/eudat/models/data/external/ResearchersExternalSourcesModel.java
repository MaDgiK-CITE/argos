package eu.eudat.models.data.external;

import java.util.List;
import java.util.Map;


public class ResearchersExternalSourcesModel extends ExternalListingItem<ResearchersExternalSourcesModel> {
    @Override
    public ResearchersExternalSourcesModel fromExternalItem(List<Map<String, String>> values) {
        for (Map<String, String> item : values) {
            ExternalSourcesItemModel model = new ExternalSourcesItemModel();
            model.setRemoteId(item.get("pid"));
            model.setUri(item.get("uri"));
            switch (item.get("tag")) {
                case "ORCID":
                    model.setName(item.get("name") + " (orcid:" + item.get("pid") + ")");
                    break;
                default:
                    model.setName(item.get("name"));
            }
            model.setTag(item.get("tag"));
            model.setKey(item.get("key"));
            this.add(model);
        }
        return this;
    }
}
