package eu.eudat.models.data.user.components.datasetprofile;

import eu.eudat.models.data.properties.PropertiesGenerator;
import eu.eudat.models.data.user.composite.PropertiesModelBuilder;
import eu.eudat.logic.utilities.interfaces.ViewStyleDefinition;
import eu.eudat.logic.utilities.builders.ModelBuilder;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class Section implements Comparable, ViewStyleDefinition<eu.eudat.models.data.entities.xmlmodels.datasetprofiledefinition.Section>, PropertiesModelBuilder, PropertiesGenerator {
    private List<Section> sections;
    private List<FieldSet> compositeFields;
    private Boolean defaultVisibility;
    private String numbering;
    private String page;
    private Integer ordinal;
    private String id;
    private String title;
    private String description;
    private Boolean multiplicity;

    public List<Section> getSections() {
        Collections.sort(sections);
        return sections;
    }

    public void setSections(List<Section> sections) {
        this.sections = sections;
    }

    public List<FieldSet> getCompositeFields() {
        Collections.sort(compositeFields);
        return compositeFields;
    }

    public void setCompositeFields(List<FieldSet> compositeFields) {
        this.compositeFields = compositeFields;
    }

    public Boolean getDefaultVisibility() {
        return defaultVisibility;
    }

    public void setDefaultVisibility(Boolean defaultVisibility) {
        this.defaultVisibility = defaultVisibility;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(int ordinal) {
        this.ordinal = ordinal;
    }

    public String getNumbering() {
        return numbering;
    }

    public void setNumbering(String numbering) {
        this.numbering = numbering;
    }

    public Boolean getMultiplicity() {
        return multiplicity;
    }

    public void setMultiplicity(Boolean multiplicity) {
        this.multiplicity = multiplicity;
    }

    @Override
    public eu.eudat.models.data.entities.xmlmodels.datasetprofiledefinition.Section toDatabaseDefinition(eu.eudat.models.data.entities.xmlmodels.datasetprofiledefinition.Section item) {
        item.setDefaultVisibility(this.defaultVisibility);
        item.setDescription(this.description);
        if (this.compositeFields != null)
            item.setFieldSets(new ModelBuilder().toViewStyleDefinition(this.compositeFields, eu.eudat.models.data.entities.xmlmodels.datasetprofiledefinition.FieldSet.class));
        item.setId(this.id);
        item.setOrdinal(this.ordinal);
        item.setPage(this.page);
        if (this.sections != null)
            item.setSections(new ModelBuilder().toViewStyleDefinition(this.sections, eu.eudat.models.data.entities.xmlmodels.datasetprofiledefinition.Section.class));
        item.setTitle(this.title);
        item.setMultiplicity(this.multiplicity);
        return item;
    }

    @Override
    public void fromDatabaseDefinition(eu.eudat.models.data.entities.xmlmodels.datasetprofiledefinition.Section item) {
        this.defaultVisibility = item.isDefaultVisibility();
        this.description = item.getDescription();
        this.compositeFields = new ModelBuilder().fromViewStyleDefinition(item.getFieldSets(), FieldSet.class);
        this.id = item.getId();
        this.ordinal = item.getOrdinal();
        this.numbering = item.getNumbering();
        this.page = item.getPage();
        this.sections = new ModelBuilder().fromViewStyleDefinition(item.getSections(), Section.class);
        this.title = item.getTitle();
        this.multiplicity = item.getMultiplicity();
    }

    @Override
    public void fromJsonObject(Map<String, Object> properties) {
        this.sections.forEach(item -> item.fromJsonObject(properties));
        this.compositeFields.forEach(item -> item.fromJsonObject(properties));
    }

    @Override
    public int compareTo(Object o) {
        return this.ordinal.compareTo(((Section) o).getOrdinal());
    }

    @Override
    public void fromJsonObject(Map<String, Object> properties, String index) {
        // TODO Auto-generated method stub

    }

    @Override
    public void toMap(Map<String, Object> fieldValues) {
        this.sections.forEach(item -> item.toMap(fieldValues));
        this.compositeFields.forEach(item -> item.toMap(fieldValues));
    }

    @Override
    public void toMap(Map<String, Object> fieldValues, int index) {

    }
}
