package eu.eudat.models.data.externaldataset;

import eu.eudat.data.entities.ExternalDataset;
import eu.eudat.models.DataModel;
import eu.eudat.types.externalsourcetype.ExternalDatasetType;

import java.util.Date;
import java.util.UUID;

public class ExternalDatasetListingModel implements DataModel<ExternalDataset, ExternalDatasetListingModel> {

    private UUID id;
    private String name;
    private String abbreviation;
    private String reference;
    private Date created;
    private Date modified;
    private String info;
    private ExternalDatasetType type;
    private String pid;
    private String pidTypeField;
    private String uri;
    private String tag; // Api fetching the data
    private String source; // Actual harvested source

    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getAbbreviation() {
        return abbreviation;
    }
    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getReference() {
        return reference;
    }
    public void setReference(String reference) {
        this.reference = reference;
    }

    public Date getCreated() {
        return created;
    }
    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }
    public void setModified(Date modified) {
        this.modified = modified;
    }

    public Integer getType() {
        return type != null ? type.getValue() : null;
    }
    public void setType(Integer type) {
        this.type = ExternalDatasetType.fromInteger(type);
    }

    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }

    public String getPid() {
        return pid;
    }
    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getPidTypeField() {
        return pidTypeField;
    }
    public void setPidTypeField(String pidTypeField) {
        this.pidTypeField = pidTypeField;
    }

    public String getUri() {
        return uri;
    }
    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getTag() {
        return tag;
    }
    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getSource() {
        return source;
    }
    public void setSource(String source) {
        this.source = source;
    }

    @Override
    public ExternalDatasetListingModel fromDataModel(ExternalDataset entity) {
        this.id = entity.getId();
        this.abbreviation = entity.getAbbreviation();
        this.name = entity.getLabel();
        this.modified = entity.getModified();
        this.created = entity.getCreated();
        this.reference = entity.getReference();
        String source1 = entity.getReference().substring(0, entity.getReference().indexOf(":"));
        if (source1.equals("dmp")) {
            this.source = "Internal";
        } else {
            this.source = source1;
        }
        return this;
    }

    @Override
    public ExternalDataset toDataModel() throws Exception {
        ExternalDataset externalDataset = new ExternalDataset();
        externalDataset.setAbbreviation(this.abbreviation);
        externalDataset.setCreated(this.created != null ? this.created : new Date());
        externalDataset.setLabel(this.name);
        externalDataset.setId(this.id);
        externalDataset.setModified(this.modified);
        if (this.source != null && this.source.equals("Internal")) this.source = "dmp";
        if (this.reference != null && !this.reference.trim().isEmpty()
                && this.source != null && !this.source.trim().isEmpty()) {
            if (this.source.equals(this.reference.substring(0, this.source.length()))) {
                externalDataset.setReference(this.reference);
            } else {
                externalDataset.setReference(this.source.toLowerCase() + ":" + this.reference);
            }
        }
        if (externalDataset.getReference() == null) {
            externalDataset.setReference(this.pid);
        }
        externalDataset.setModified(new Date());
        return externalDataset;
    }

    @Override
    public String getHint() {
        return null;
    }
}
