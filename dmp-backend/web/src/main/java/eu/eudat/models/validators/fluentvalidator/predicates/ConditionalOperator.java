package eu.eudat.models.validators.fluentvalidator.predicates;

/**
 * Created by ikalyvas on 8/31/2018.
 */
public interface ConditionalOperator<T> {
    boolean apply(T item);
}
