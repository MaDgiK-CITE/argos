package eu.eudat.models.data.datasetImport;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collections;
import java.util.List;

@XmlRootElement(name = "section")
public class DatasetImportSection implements Comparable{
    private List<DatasetImportSections> sections;
    private List<DatasetImportFieldSets> compositeFields;
    private Boolean defaultVisibility;
    private String numbering;
    private String page;
    private Integer ordinal;
    private String id;
    private String title;
    private String description;

    @XmlElement(name = "sections")
    public List<DatasetImportSections> getSections() {
        //Collections.sort(sections);
        return sections;
    }
    public void setSections(List<DatasetImportSections> sections) {
        this.sections = sections;
    }

    //@XmlElementWrapper(name="composite-field")
    @XmlElement(name = "composite-fields")
    public List<DatasetImportFieldSets> getCompositeFields() {
        //Collections.sort(compositeFields);
        return compositeFields;
    }
    public void setCompositeFields(List<DatasetImportFieldSets> compositeFields) {
        this.compositeFields = compositeFields;
    }

    public Boolean getDefaultVisibility() {
        return defaultVisibility;
    }
    public void setDefaultVisibility(Boolean defaultVisibility) {
        this.defaultVisibility = defaultVisibility;
    }

    public String getPage() {
        return page;
    }
    public void setPage(String page) {
        this.page = page;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public int getOrdinal() {
        return ordinal;
    }
    public void setOrdinal(int ordinal) {
        this.ordinal = ordinal;
    }

    public String getNumbering() {
        return numbering;
    }
    public void setNumbering(String numbering) {
        this.numbering = numbering;
    }

    @Override
    public int compareTo(Object o) {
        return this.ordinal.compareTo(((DatasetImportSection) o).getOrdinal()); }
}
