package eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition;

import eu.eudat.logic.utilities.builders.XmlBuilder;
import eu.eudat.logic.utilities.interfaces.XmlSerializable;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by ikalyvas on 3/21/2018.
 */
public class DataManagementPlanProfile implements XmlSerializable<DataManagementPlanProfile> {

    private List<Field> fields;

    public List<Field> getFields() {
        return fields;
    }

    public void setFields(List<Field> fields) {
        this.fields = fields;
    }

    @Override
    public Element toXml(Document doc) {
        Element root = doc.createElement("root");
        Element fields = doc.createElement("fields");
        for (Field field : this.fields) {
            fields.appendChild(field.toXml(doc));
        }
        root.appendChild(fields);
        return root;
    }

    @Override
    public DataManagementPlanProfile fromXml(Element item) {
        this.fields = new LinkedList();
        Element fields = (Element) XmlBuilder.getNodeFromListByTagName(item.getChildNodes(), "fields");
        if (fields != null) {
            NodeList fieldElements = fields.getChildNodes();
            for (int temp = 0; temp < fieldElements.getLength(); temp++) {
                Node fieldElement = fieldElements.item(temp);
                if (fieldElement.getNodeType() == Node.ELEMENT_NODE) {
                    this.fields.add(new Field().fromXml((Element) fieldElement));
                }
            }
        }
        return this;
    }
}
