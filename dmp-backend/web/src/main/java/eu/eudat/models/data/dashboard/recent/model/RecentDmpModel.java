package eu.eudat.models.data.dashboard.recent.model;

import eu.eudat.data.entities.DMP;
import eu.eudat.models.data.listingmodels.UserInfoListingModel;
import eu.eudat.models.data.urls.DatasetUrlListing;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

public class RecentDmpModel extends RecentActivityModel<DMP, RecentDmpModel> {
	private Map<String, Object> extraProperties;
	private List<DatasetUrlListing> datasets;
	private UUID groupId;

	public Map<String, Object> getExtraProperties() {
		return extraProperties;
	}

	public void setExtraProperties(Map<String, Object> extraProperties) {
		this.extraProperties = extraProperties;
	}

	public List<DatasetUrlListing> getDatasets() {
		return datasets;
	}

	public void setDatasets(List<DatasetUrlListing> datasets) {
		this.datasets = datasets;
	}

	public UUID getGroupId() {
		return groupId;
	}

	public void setGroupId(UUID groupId) {
		this.groupId = groupId;
	}

	@Override
	@Transactional
	public RecentActivityModel fromEntity(DMP entity) {
		this.setType(RecentActivityType.DMP.getIndex());
		this.setId(entity.getId().toString());
		this.setTitle(entity.getLabel());
		this.setCreated(entity.getCreated());
		this.setModified(entity.getModified());
		this.setStatus(entity.getStatus());
		this.setVersion(entity.getVersion());
		this.datasets = entity.getDataset().stream().map(dataset -> new DatasetUrlListing().fromDataModel(dataset)).collect(Collectors.toList());
		this.setFinalizedAt(entity.getFinalizedAt());
		if (entity.getGrant() != null) {
			this.setGrant(entity.getGrant().getLabel());
		}
		this.groupId = entity.getGroupId();
		this.setPublic(entity.isPublic());
		this.setPublishedAt(entity.getPublishedAt());
		this.setUsers(entity.getUsers().stream().map(x -> new UserInfoListingModel().fromDataModel(x)).collect(Collectors.toList()));
		return this;
	}

	@Override
	public RecentDmpModel fromDataModel(DMP entity) {
		return (RecentDmpModel) this.fromEntity(entity);
	}

	@Override
	public DMP toDataModel() throws Exception {
		return null;
	}

	@Override
	public String getHint() {
		return "recentDmpModel";
	}
}
