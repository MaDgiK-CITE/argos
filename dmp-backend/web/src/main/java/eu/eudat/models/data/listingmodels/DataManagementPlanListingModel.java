package eu.eudat.models.data.listingmodels;

import eu.eudat.data.entities.DMP;
import eu.eudat.data.entities.Grant;
import eu.eudat.models.DataModel;
import eu.eudat.models.data.urls.DatasetUrlListing;

import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;


public class DataManagementPlanListingModel implements DataModel<DMP, DataManagementPlanListingModel> {
    private String id;
    private String label;
    private String grant;
    private Date creationTime;
    private Date modifiedTime;
    private int version;
    private int status;
    private UUID groupId;
    private List<DatasetUrlListing> datasets;
    private List<UserInfoListingModel> users;
    private Date finalizedAt;
    private Boolean isPublic;
    private Date publishedAt;


    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }

    public String getGrant() {
        return grant;
    }
    public void setGrant(String grant) {
        this.grant = grant;
    }

    public Date getCreationTime() {
        return creationTime;
    }
    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public Date getModifiedTime() {
        return modifiedTime;
    }
    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    public int getVersion() {
        return version;
    }
    public void setVersion(int version) {
        this.version = version;
    }

    public UUID getGroupId() {
        return groupId;
    }
    public void setGroupId(UUID groupId) {
        this.groupId = groupId;
    }

    public List<DatasetUrlListing> getDatasets() {
        return datasets;
    }
    public void setDatasets(List<DatasetUrlListing> datasets) {
        this.datasets = datasets;
    }

    public int getStatus() {
        return status;
    }
    public void setStatus(int status) {
        this.status = status;
    }

    public List<UserInfoListingModel> getUsers() {
        return users;
    }
    public void setUsers(List<UserInfoListingModel> users) {
        this.users = users;
    }

    public Date getFinalizedAt() {
        return finalizedAt;
    }
    public void setFinalizedAt(Date finalizedAt) {
        this.finalizedAt = finalizedAt;
    }

    public Boolean getPublic() {
        return isPublic;
    }
    public void setPublic(Boolean aPublic) {
        isPublic = aPublic;
    }

    public Date getPublishedAt() {
        return publishedAt;
    }
    public void setPublishedAt(Date publishedAt) {
        this.publishedAt = publishedAt;
    }

    @Override
    public DataManagementPlanListingModel fromDataModel(DMP entity) {
        this.id = entity.getId().toString();
        this.label = entity.getLabel();
        this.groupId = entity.getGroupId();
        return this;
    }

    public DataManagementPlanListingModel fromDataModelAssociatedProfiles(DMP entity) {
        this.id = entity.getId().toString();
        this.label = entity.getLabel();
        this.groupId = entity.getGroupId();
        this.creationTime = entity.getCreated();
        return this;
    }

    public DataManagementPlanListingModel fromDataModelAutoComplete(DMP entity) {
        this.id = entity.getId().toString();
        this.label = entity.getLabel();
        this.groupId = entity.getGroupId();
        this.creationTime = entity.getCreated();
        return this;
    }

    public DataManagementPlanListingModel fromDataModelDatasets(DMP entity) {
        this.fromDataModel(entity);
        this.status = entity.getStatus();
        this.version = entity.getVersion();
        this.grant = (entity.getGrant() != null) ? entity.getGrant().getLabel() : null;
        this.creationTime = entity.getCreated();
        this.modifiedTime = entity.getModified();
        this.datasets = entity.getDataset().stream().map(x-> new DatasetUrlListing().fromDataModel(x)).collect(Collectors.toList());
        this.users = entity.getUsers().stream().map(x -> new UserInfoListingModel().fromDataModel(x)).collect(Collectors.toList());
        this.finalizedAt = entity.getFinalizedAt();
        this.isPublic = entity.isPublic();
        this.publishedAt = entity.getPublishedAt();

        return this;
    }

    @Override
    public DMP toDataModel() {
        DMP entity = new DMP();
        entity.setId(UUID.fromString(this.getId()));
        entity.setLabel(this.getLabel());
        entity.setGroupId(this.getGroupId());
        entity.setStatus(Integer.valueOf(this.getStatus()).shortValue());
        entity.setCreated(this.getCreationTime());
        entity.setFinalizedAt(this.getFinalizedAt());
        entity.setModified(this.getModifiedTime());
        entity.setPublic(this.getPublic());
        entity.setPublishedAt(this.getPublishedAt());
        entity.setVersion(this.getVersion());

        entity.setDataset(this.getDatasets().stream().map(DatasetUrlListing::toDataModel).collect(Collectors.toCollection(LinkedHashSet::new)));
        if (this.getGrant() != null && !this.getGrant().isEmpty()) {
            Grant grant = new Grant();
            grant.setLabel(this.getGrant());
            entity.setGrant(grant);
        }
        entity.setUsers(this.getUsers().stream().map(UserInfoListingModel::toDataModel).collect(Collectors.toSet()));
        return entity;
    }

    @Override
    public String getHint() {
        return "dataManagementPlanListingModel";
    }
}
