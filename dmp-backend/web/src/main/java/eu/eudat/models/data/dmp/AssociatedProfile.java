package eu.eudat.models.data.dmp;

import eu.eudat.data.entities.DescriptionTemplate;
import eu.eudat.logic.utilities.interfaces.XmlSerializable;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.util.Map;
import java.util.UUID;


public class AssociatedProfile implements XmlSerializable<AssociatedProfile> {
    private UUID id;
    private UUID descriptionTemplateId;
    private String label;
    private Map<String, Object> data;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getDescriptionTemplateId() {
        return descriptionTemplateId;
    }

    public void setDescriptionTemplateId(UUID descriptionTemplateId) {
        this.descriptionTemplateId = descriptionTemplateId;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }

    @Override
    public Element toXml(Document doc) {
        Element profile = doc.createElement("profile");
        profile.setAttribute("profileId", this.descriptionTemplateId.toString());
        profile.setAttribute("label", this.label);
        return profile;
    }

    @Override
    public AssociatedProfile fromXml(Element item) {
        this.descriptionTemplateId = UUID.fromString(item.getAttribute("profileId"));
        this.label = item.getAttribute("label");
        return this;
    }

    public DescriptionTemplate toData() {
        DescriptionTemplate profile = new DescriptionTemplate();
        profile.setId(this.descriptionTemplateId);
        profile.setLabel(this.label);
        return profile;
    }

    public AssociatedProfile fromData(DescriptionTemplate entity) {
        this.descriptionTemplateId = entity.getId();
        this.label = entity.getLabel();
        return this;
    }
}
