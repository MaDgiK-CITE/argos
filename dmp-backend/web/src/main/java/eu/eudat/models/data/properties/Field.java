package eu.eudat.models.data.properties;

import java.util.Map;

public class Field implements PropertiesGenerator {
    private String id;

    private String value;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void toMap(Map<String, Object> fieldValues) {
        fieldValues.put(this.id, this.value);
    }

    @Override
    public void toMap(Map<String, Object> fieldValues, int index) {
        fieldValues.put(this.id, this.value);
    }

}