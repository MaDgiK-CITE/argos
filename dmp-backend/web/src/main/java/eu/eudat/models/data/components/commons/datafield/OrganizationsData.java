package eu.eudat.models.data.components.commons.datafield;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.util.HashMap;
import java.util.Map;

public class OrganizationsData extends FieldData<OrganizationsData> {
    private Boolean multiAutoComplete;

    public Boolean getMultiAutoComplete() {
        return multiAutoComplete;
    }

    public void setMultiAutoComplete(Boolean multiAutoComplete) {
        this.multiAutoComplete = multiAutoComplete;
    }

    @Override
    public OrganizationsData fromData(Object data) {
        if (data != null) {
            this.setLabel((String) ((Map<String, Object>) data).get("label"));
            this.setMultiAutoComplete(((Map<String, Object>) data).get("multiAutoComplete") != null && !((Map<String, Object>) data).get("multiAutoComplete").toString().isEmpty()? Boolean.parseBoolean( ((Map<String, Object>) data).get("multiAutoComplete").toString()) : false);
        }
        return this;
    }

    @Override
    public Object toData() {
        return null;
    }

    @Override
    public Element toXml(Document doc) {
        Element root = doc.createElement("data");
        root.setAttribute("label", this.getLabel());
        if (this.getMultiAutoComplete() != null) {
            root.setAttribute("multiAutoComplete", this.getMultiAutoComplete().toString());
        }
        return root;
    }

    @Override
    public OrganizationsData fromXml(Element item) {
        this.setLabel(item != null ? item.getAttribute("label") : "");
        this.setMultiAutoComplete(Boolean.parseBoolean(item.getAttribute("multiAutoComplete")));
        return this;
    }

    @Override
    public Map<String, Object> toMap(Element item) {
        HashMap dataMap = new HashMap();
        dataMap.put("label", item != null && item.getAttributes().getLength() > 0? item.getAttribute("label") : "");
        dataMap.put("multiAutoComplete", item != null && item.getAttributes().getLength() > 0? Boolean.parseBoolean(item.getAttribute("multiAutocomplete")) : false);
        return dataMap;
    }
}
