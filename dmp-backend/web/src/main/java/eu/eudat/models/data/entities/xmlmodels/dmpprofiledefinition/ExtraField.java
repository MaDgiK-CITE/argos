package eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition;

import eu.eudat.logic.utilities.interfaces.XmlSerializable;
import eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition.types.ExtraFieldType;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.util.UUID;

public class ExtraField implements XmlSerializable<ExtraField> {

    private UUID id;
    private String label;
    private String description;
    private String placeholder;
    private ExtraFieldType type;
    private Boolean required;
    private Integer ordinal;

    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public String getPlaceholder() {
        return placeholder;
    }
    public void setPlaceholder(String placeholder) {
        this.placeholder = placeholder;
    }

    public ExtraFieldType getType() {
        return type;
    }
    public void setType(ExtraFieldType type) {
        this.type = type;
    }

    public Boolean getRequired() {
        return required;
    }
    public void setRequired(Boolean required) {
        this.required = required;
    }

    public Integer getOrdinal() {
        return ordinal;
    }
    public void setOrdinal(Integer ordinal) {
        this.ordinal = ordinal;
    }

    @Override
    public Element toXml(Document doc) {
        Element rootElement = doc.createElement("extraField");
        rootElement.setAttribute("id", this.getId().toString());
        rootElement.setAttribute("label", this.label);
        rootElement.setAttribute("description", this.description);
        rootElement.setAttribute("placeholder", this.placeholder);
        rootElement.setAttribute("type", String.valueOf(this.type.getValue()));
        rootElement.setAttribute("required", String.valueOf(this.required));
        rootElement.setAttribute("ordinal", String.valueOf(this.ordinal));
        return rootElement;
    }

    @Override
    public ExtraField fromXml(Element item) {
        this.id = UUID.fromString(item.getAttribute("id"));
        this.label = item.getAttribute("label");
        this.description = item.getAttribute("description");
        this.placeholder = item.getAttribute("placeholder");
        this.type = ExtraFieldType.fromInteger(Integer.parseInt(item.getAttribute("type")));
        this.required = Boolean.parseBoolean(item.getAttribute("required"));
        this.ordinal = Integer.valueOf(item.getAttribute("ordinal"));
        return this;
    }
}
