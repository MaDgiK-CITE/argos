package eu.eudat.models.data.dashboard.statistics;

public class DashBoardStatistics {
    private Long totalDataManagementPlanCount;
    private Long totalGrantCount;
    private Long totalDataSetCount;
    private Long totalOrganisationCount;


    public Long getTotalDataManagementPlanCount() {
        return totalDataManagementPlanCount;
    }

    public void setTotalDataManagementPlanCount(Long totalDataManagementPlanCount) {
        this.totalDataManagementPlanCount = totalDataManagementPlanCount;
    }

    public Long getTotalGrantCount() {
        return totalGrantCount;
    }

    public void setTotalGrantCount(Long totalGrantCount) {
        this.totalGrantCount = totalGrantCount;
    }

    public Long getTotalDataSetCount() {
        return totalDataSetCount;
    }

    public void setTotalDataSetCount(Long totalDataSetCount) {
        this.totalDataSetCount = totalDataSetCount;
    }

    public Long getTotalOrganisationCount() {
        return totalOrganisationCount;
    }

    public void setTotalOrganisationCount(Long totalOrganisationCount) {
        this.totalOrganisationCount = totalOrganisationCount;
    }
}
