package eu.eudat.models.data.external.orcid;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Arrays;

@XmlRootElement(name = "personal-details", namespace = "http://www.orcid.org/ns/personal-details")
public class PersonalDetails {

	private PersonalName name;

	public PersonalDetails(PersonalName name) {
		this.name = name;
	}

	public PersonalDetails() {
	}

	@XmlElement(name="name", namespace = "http://www.orcid.org/ns/personal-details")
	public PersonalName getName() {
		return name;
	}
	public void setName(PersonalName name) {
		this.name = name;
	}
}
