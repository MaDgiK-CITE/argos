package eu.eudat.models.rda.mapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.eudat.elastic.entities.Dataset;
import eu.eudat.elastic.entities.Tag;
import eu.eudat.logic.services.ApiContext;
import eu.eudat.logic.utilities.json.JavaToJson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class KeywordRDAMapper {
	private static final Logger logger = LoggerFactory.getLogger(KeywordRDAMapper.class);
	private static final ObjectMapper mapper = new ObjectMapper();

	public static List<String> toRDA(String value) {
		if (!value.isEmpty() && !value.equals("null")) {
			try {
				Tag tag = mapper.readValue(value, Tag.class);
				return new ArrayList<>(Collections.singletonList(tag.getName()));
			} catch (JsonProcessingException e) {
				logger.warn(e.getMessage() + ". Attempting to parse it as a String since its a new tag.");
				return new ArrayList<>(Collections.singletonList(value));
			}
		}

		return new ArrayList<>();
	}
}
