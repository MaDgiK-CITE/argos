package eu.eudat.models.data.datasetImport;

public class DatasetImportViewStyle {
    private String renderStyle;
    private String cssClass;

    public String getRenderStyle() {
        return renderStyle;
    }
    public void setRenderStyle(String renderStyle) {
        this.renderStyle = renderStyle;
    }

    public String getCssClass() {
        return cssClass;
    }
    public void setCssClass(String cssClass) {
        this.cssClass = cssClass;
    }
}
