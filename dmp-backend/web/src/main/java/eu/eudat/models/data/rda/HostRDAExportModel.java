package eu.eudat.models.data.rda;

import java.util.List;

public class HostRDAExportModel {
	private String availability;
	private String backup__frequency;
	private String backup_type;
	private String certified_with; // Repository certified with one the following standards: DIN31644 / DINI-Zertifikat / DSA / ISO16363 / ISO16919 /TRAC / WDS / CoreTrustSeal
	private String description;
	private String geo_location;  // Physical location of the data expressed using ISO 3166-1 country code.
	private List<String> pid_system; // PID System: ark arxiv bibcode doi ean13 eissn handle igsn isbn issn istc lissn lsid pmid purl upc url urn other
	private String storage_type;
	private String support_versioning; // Allowed values: yes / no / unknown
	private String title;

	public String getAvailability() {
		return availability;
	}
	public void setAvailability(String availability) {
		this.availability = availability;
	}

	public String getBackup__frequency() {
		return backup__frequency;
	}
	public void setBackup__frequency(String backup__frequency) {
		this.backup__frequency = backup__frequency;
	}

	public String getBackup_type() {
		return backup_type;
	}
	public void setBackup_type(String backup_type) {
		this.backup_type = backup_type;
	}

	public String getCertified_with() {
		return certified_with;
	}
	public void setCertified_with(String certified_with) {
		this.certified_with = certified_with;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public String getGeo_location() {
		return geo_location;
	}
	public void setGeo_location(String geo_location) {
		this.geo_location = geo_location;
	}

	public List<String> getPid_system() {
		return pid_system;
	}
	public void setPid_system(List<String> pid_system) {
		this.pid_system = pid_system;
	}

	public String getStorage_type() {
		return storage_type;
	}
	public void setStorage_type(String storage_type) {
		this.storage_type = storage_type;
	}

	public String getSupport_versioning() {
		return support_versioning;
	}
	public void setSupport_versioning(String support_versioning) {
		this.support_versioning = support_versioning;
	}

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
}
