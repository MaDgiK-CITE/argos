
package eu.eudat.models.rda;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;


/**
 * The Contact ID Schema
 * <p>
 * 
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "identifier",
    "type"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class ContactId implements Serializable
{

    /**
     * The DMP Contact Identifier Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("identifier")
    private String identifier;
    /**
     * The DMP Contact Identifier Type Schema
     * <p>
     * Identifier type. Allowed values: orcid, isni, openid, other
     * (Required)
     * 
     */
    @JsonProperty("type")
    @JsonPropertyDescription("Identifier type. Allowed values: orcid, isni, openid, other")
    private ContactId.Type type;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = -7066973565810615822L;

    /**
     * The DMP Contact Identifier Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("identifier")
    public String getIdentifier() {
        return identifier;
    }

    /**
     * The DMP Contact Identifier Schema
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("identifier")
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    /**
     * The DMP Contact Identifier Type Schema
     * <p>
     * Identifier type. Allowed values: orcid, isni, openid, other
     * (Required)
     * 
     */
    @JsonProperty("type")
    public ContactId.Type getType() {
        return type;
    }

    /**
     * The DMP Contact Identifier Type Schema
     * <p>
     * Identifier type. Allowed values: orcid, isni, openid, other
     * (Required)
     * 
     */
    @JsonProperty("type")
    public void setType(ContactId.Type type) {
        this.type = type;
    }

    @JsonProperty("additional_properties")
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonProperty("additional_properties")
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public enum Type {

        ORCID("orcid"),
        ISNI("isni"),
        OPENID("openid"),
        OTHER("other");
        private final String value;
        private final static Map<String, ContactId.Type> CONSTANTS = new HashMap<String, ContactId.Type>();

        static {
            for (ContactId.Type c: values()) {
                CONSTANTS.put(c.value, c);
            }
        }

        private Type(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return this.value;
        }

        @JsonValue
        public String value() {
            return this.value;
        }

        @JsonCreator
        public static ContactId.Type fromValue(String value) {
            ContactId.Type constant = CONSTANTS.get(value);
            if (constant == null) {
                throw new IllegalArgumentException(value);
            } else {
                return constant;
            }
        }

    }

}
