package eu.eudat.models.data.rda;

import eu.eudat.data.entities.*;
import eu.eudat.logic.managers.DatasetManager;
import eu.eudat.models.data.security.Principal;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class DmpRDAExportModel {
    private ContactRDAExportModel contact;
    private List<DmpContributorRDAExportModel> contributor;
    private List<DmpCostRDAExportModel> cost;
    private String created;
    private List<DatasetRDAExportModel> dataset;
    private String description;
    private IdRDAExportModel dmp_id;
    private String ethical_issues_description;
    private String ethical_issues_exist; // Allowed Values: yes no unknown.
    private String ethical_issues_report;
    private String language;
    private String modified;
    private ProjectRDAExportModel project;
    private String title;

    public ContactRDAExportModel getContact() {
        return contact;
    }
    public void setContact(ContactRDAExportModel contact) {
        this.contact = contact;
    }

    public List<DmpContributorRDAExportModel> getContributor() {
        return contributor;
    }
    public void setContributor(List<DmpContributorRDAExportModel> contributor) {
        this.contributor = contributor;
    }

    public List<DmpCostRDAExportModel> getCost() {
        return cost;
    }
    public void setCost(List<DmpCostRDAExportModel> cost) {
        this.cost = cost;
    }

    public String getCreated() {
        return created;
    }
    public void setCreated(String created) {
        this.created = created;
    }

    public List<DatasetRDAExportModel> getDataset() {
        return dataset;
    }
    public void setDataset(List<DatasetRDAExportModel> dataset) {
        this.dataset = dataset;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public IdRDAExportModel getDmp_id() {
        return dmp_id;
    }
    public void setDmp_id(IdRDAExportModel dmp_id) {
        this.dmp_id = dmp_id;
    }

    public String getEthical_issues_description() {
        return ethical_issues_description;
    }
    public void setEthical_issues_description(String ethical_issues_description) {
        this.ethical_issues_description = ethical_issues_description;
    }

    public String getEthical_issues_exist() {
        return ethical_issues_exist;
    }
    public void setEthical_issues_exist(String ethical_issues_exist) {
        this.ethical_issues_exist = ethical_issues_exist;
    }

    public String getEthical_issues_report() {
        return ethical_issues_report;
    }
    public void setEthical_issues_report(String ethical_issues_report) {
        this.ethical_issues_report = ethical_issues_report;
    }

    public String getLanguage() {
        return language;
    }
    public void setLanguage(String language) {
        this.language = language;
    }

    public String getModified() {
        return modified;
    }
    public void setModified(String modified) {
        this.modified = modified;
    }

    public ProjectRDAExportModel getProject() {
        return project;
    }
    public void setProject(ProjectRDAExportModel project) {
        this.project = project;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public DmpRDAExportModel fromDataModel(DMP entity, DatasetManager datasetManager, Principal principal) {
        DmpRDAExportModel dmpRda = new DmpRDAExportModel();
        dmpRda.contact = new ContactRDAExportModel().fromDataModel(entity.getUsers().stream().filter(x -> x.getRole().equals(UserDMP.UserDMPRoles.OWNER.getValue())).findFirst().get().getUser());
    	if (entity.getUsers().stream().anyMatch(x -> x.getRole().equals(UserDMP.UserDMPRoles.USER.getValue()))) {
            dmpRda.contributor = new LinkedList<>();
    	    for (UserDMP userdmp : entity.getUsers().stream().filter(x -> x.getRole().equals(UserDMP.UserDMPRoles.USER.getValue())).collect(Collectors.toList())) {
                dmpRda.contributor.add(new DmpContributorRDAExportModel().fromDataModel(userdmp.getUser(), UserDMP.UserDMPRoles.fromInteger(userdmp.getRole()).toString()));
            }
        }
        dmpRda.cost = null;
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        dmpRda.created = formatter.format(entity.getCreated());
        dmpRda.dataset = new LinkedList<>();
        for (Dataset dataset : entity.getDataset()) {
            if (dataset.getStatus() != Dataset.Status.DELETED.getValue() && dataset.getStatus() != Dataset.Status.CANCELED.getValue())
                dmpRda.dataset.add(new DatasetRDAExportModel().fromDataModel(dataset, datasetManager, principal));
        }
        dmpRda.description = entity.getDescription().replace("\n", " ");
        if (entity.getDois() != null && !entity.getDois().isEmpty()) {
            boolean zenodoDoi = false;
            for(EntityDoi doi: entity.getDois()){
                if(doi.getRepositoryId().equals("Zenodo")){
                    dmpRda.dmp_id = new IdRDAExportModel(doi.getDoi(), "zenodo");
                    zenodoDoi = true;
                }
            }
            if(!zenodoDoi){
                dmpRda.dmp_id = new IdRDAExportModel(entity.getId().toString(), "other");
            }
        }
        else {
            dmpRda.dmp_id = new IdRDAExportModel(entity.getId().toString(), "other");
        }
        // Mock up data on "language" and "ethical_issues_*" for now.
        dmpRda.ethical_issues_exist = "unknown";
        dmpRda.language = "en";
        dmpRda.modified = formatter.format(new Date());
        if (entity.getGrant() != null) {
            dmpRda.project = new ProjectRDAExportModel().fromDataModel(entity.getGrant());
        }
        dmpRda.title = entity.getLabel();

        return dmpRda;
    }
}
