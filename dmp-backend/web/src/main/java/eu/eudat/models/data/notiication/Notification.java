package eu.eudat.models.data.notiication;

import eu.eudat.data.enumeration.notification.ActiveStatus;
import eu.eudat.data.enumeration.notification.ContactType;
import eu.eudat.data.enumeration.notification.NotificationType;
import eu.eudat.data.enumeration.notification.NotifyState;
import eu.eudat.models.DataModel;
import eu.eudat.models.data.userinfo.UserInfo;

import java.util.Date;
import java.util.UUID;

public class Notification implements DataModel<eu.eudat.data.entities.Notification, Notification> {

	private UUID id;
	private UserInfo userId;
	private ActiveStatus isActive;
	private NotificationType type;
	private ContactType contactTypeHint;
	private String contactHint;
	private String data;
	private NotifyState notifyState;
	private Date notifiedAt;
	private Integer retryCount;
	private Date createdAt;
	private Date updatedAt;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public UserInfo getUserId() {
		return userId;
	}

	public void setUserId(UserInfo userId) {
		this.userId = userId;
	}

	public ActiveStatus getIsActive() {
		return isActive;
	}

	public void setIsActive(ActiveStatus isActive) {
		this.isActive = isActive;
	}

	public NotificationType getType() {
		return type;
	}

	public void setType(NotificationType type) {
		this.type = type;
	}

	public ContactType getContactTypeHint() {
		return contactTypeHint;
	}

	public void setContactTypeHint(ContactType contactTypeHint) {
		this.contactTypeHint = contactTypeHint;
	}

	public String getContactHint() {
		return contactHint;
	}

	public void setContactHint(String contactHint) {
		this.contactHint = contactHint;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public NotifyState getNotifyState() {
		return notifyState;
	}

	public void setNotifyState(NotifyState notifyState) {
		this.notifyState = notifyState;
	}

	public Date getNotifiedAt() {
		return notifiedAt;
	}

	public void setNotifiedAt(Date notifiedAt) {
		this.notifiedAt = notifiedAt;
	}

	public Integer getRetryCount() {
		return retryCount;
	}

	public void setRetryCount(Integer retryCount) {
		this.retryCount = retryCount;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Override
	public Notification fromDataModel(eu.eudat.data.entities.Notification entity) {
		this.id = entity.getId();
		this.contactHint = entity.getContactHint();
		this.contactTypeHint = entity.getContactTypeHint();
		this.createdAt = entity.getCreatedAt();
		this.data = entity.getData();
		this.isActive = entity.getIsActive();
		this.notifiedAt = entity.getNotifiedAt();
		this.notifyState = entity.getNotifyState();
		this.retryCount = entity.getRetryCount();
		this.type = entity.getType();
		this.updatedAt = entity.getUpdatedAt();
		this.userId = new UserInfo().fromDataModel(entity.getUserId());
		return this;
	}

	@Override
	public eu.eudat.data.entities.Notification toDataModel() throws Exception {
		eu.eudat.data.entities.Notification entity = new eu.eudat.data.entities.Notification();
		entity.setId(this.id);
		entity.setContactHint(this.contactHint);
		entity.setContactTypeHint(this.contactTypeHint);
		entity.setCreatedAt(this.createdAt);
		entity.setData(this.data);
		entity.setIsActive(this.isActive);
		entity.setNotifiedAt(this.notifiedAt);
		entity.setNotifyState(this.notifyState);
		entity.setRetryCount(this.retryCount);
		entity.setType(this.type);
		entity.setUpdatedAt(this.updatedAt);
		entity.setUserId(this.userId.toDataModel());
		return entity;
	}

	@Override
	public String getHint() {
		return null;
	}
}
