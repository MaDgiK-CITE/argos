package eu.eudat.models.data.dmp;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.eudat.data.entities.*;
import eu.eudat.logic.utilities.builders.XmlBuilder;
import eu.eudat.models.DataModel;
import eu.eudat.models.data.datasetwizard.DatasetWizardModel;
import eu.eudat.models.data.dynamicfields.DynamicFieldWithValue;
import eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition.DataManagementPlanProfile;
import eu.eudat.models.data.funder.FunderDMPEditorModel;
import eu.eudat.models.data.helpermodels.Tuple;
import eu.eudat.models.data.listingmodels.UserInfoListingModel;
import eu.eudat.models.data.grant.GrantDMPEditorModel;
import eu.eudat.models.data.project.ProjectDMPEditorModel;
import eu.eudat.models.data.userinfo.UserListingModel;
import net.minidev.json.JSONObject;

import java.util.*;
import java.util.stream.Collectors;

public class DataManagementPlanEditorModel implements DataModel<DMP, DataManagementPlanEditorModel> {
    private UUID id;
    private String label;
    private UUID groupId;
    private UUID profile;
    private int version;
    private int status;
    private boolean lockable;
    private String description;
    private List<DatasetWizardModel> datasets;
    private List<AssociatedProfile> profiles;
    private eu.eudat.models.data.grant.GrantDMPEditorModel grant;
    private List<Organisation> organisations;
    private List<Researcher> researchers;
    private List<UserListingModel> associatedUsers;
    private List<ExtraFieldModel> extraFields;
    private eu.eudat.models.data.userinfo.UserInfo creator;
    private Date modified;
    private Date created;
    private List<DynamicFieldWithValue> dynamicFields;
    private Map<String, Object> properties;
    private List<UserInfoListingModel> users;
    private List<UUID> datasetsToBeFinalized;
    private ProjectDMPEditorModel project;
    private FunderDMPEditorModel funder;
    private Map<String, Object> extraProperties;

    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getProfile() {
        return profile;
    }
    public void setProfile(UUID profile) {
        this.profile = profile;
    }

    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }

    public UUID getGroupId() {
        return groupId;
    }
    public void setGroupId(UUID groupId) {
        this.groupId = groupId;
    }

    public List<UserListingModel> getAssociatedUsers() {
        return associatedUsers;
    }
    public void setAssociatedUsers(List<UserListingModel> associatedUsers) {
        this.associatedUsers = associatedUsers;
    }

    public int getVersion() {
        return version;
    }
    public void setVersion(int version) {
        this.version = version;
    }

    public List<Organisation> getOrganisations() {
        return organisations;
    }
    public void setOrganisations(List<Organisation> organizations) {
        this.organisations = organizations;
    }

    public List<Researcher> getResearchers() {
        return researchers;
    }
    public void setResearchers(List<Researcher> researchers) {
        this.researchers = researchers;
    }

    public int getStatus() {
        return status;
    }
    public void setStatus(int status) {
        this.status = status;
    }

    public GrantDMPEditorModel getGrant() {
        return grant;
    }
    public void setGrant(GrantDMPEditorModel grant) {
        this.grant = grant;
    }

    public eu.eudat.models.data.userinfo.UserInfo getCreator() {
        return creator;
    }
    public void setCreator(eu.eudat.models.data.userinfo.UserInfo creator) {
        this.creator = creator;
    }

    public List<AssociatedProfile> getProfiles() {
        return profiles;
    }
    public void setProfiles(List<AssociatedProfile> profiles) {
        this.profiles = profiles;
    }

    public Date getModified() {
        return modified;
    }
    public void setModified(Date modified) {
        this.modified = modified;
    }

    public Date getCreated() {
        return created;
    }
    public void setCreated(Date created) {
        this.created = created;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public List<ExtraFieldModel> getExtraFields() {
        return extraFields;
    }
    public void setExtraFields(List<ExtraFieldModel> extraFields) {
        this.extraFields = extraFields;
    }

    public Map<String, Object> getProperties() {
        return properties;
    }
    public void setProperties(Map<String, Object> properties) {
        this.properties = properties;
    }

    public List<DynamicFieldWithValue> getDynamicFields() {
        return dynamicFields;
    }
    public void setDynamicFields(List<DynamicFieldWithValue> dynamicFields) {
        this.dynamicFields = dynamicFields;
    }

    public boolean getLockable() {
        return lockable;
    }
    public void setLockable(boolean lockable) {
        this.lockable = lockable;
    }

    public List<DatasetWizardModel> getDatasets() {
        return datasets;
    }

    public void setDatasets(List<DatasetWizardModel> datasets) {
        this.datasets = datasets;
    }

    public List<UserInfoListingModel> getUsers() {
        return users;
    }
    public void setUsers(List<UserInfoListingModel> users) {
        this.users = users;
    }

    public List<UUID> getDatasetsToBeFinalized() {
        return datasetsToBeFinalized;
    }
    public void setDatasetsToBeFinalized(List<UUID> datasetsToBeFinalized) {
        this.datasetsToBeFinalized = datasetsToBeFinalized;
    }

    public ProjectDMPEditorModel getProject() {
        return project;
    }
    public void setProject(ProjectDMPEditorModel project) {
        this.project = project;
    }

    public FunderDMPEditorModel getFunder() {
        return funder;
    }
    public void setFunder(FunderDMPEditorModel funder) {
        this.funder = funder;
    }

    public Map<String, Object> getExtraProperties() {
        return extraProperties;
    }

    public void setExtraProperties(Map<String, Object> extraProperties) {
        this.extraProperties = extraProperties;
    }

    @Override
    public DataManagementPlanEditorModel fromDataModel(DMP entity) {
        this.id = entity.getId();
        this.profile = entity.getProfile() != null ? entity.getProfile().getId() : null;
        this.organisations = entity.getOrganisations().stream().map(item -> new Organisation().fromDataModel(item)).collect(Collectors.toList());
        this.researchers = entity.getResearchers().stream().map(item -> new Researcher().fromDataModel(item)).collect(Collectors.toList());
        this.version = entity.getVersion();
        this.groupId = this.groupId == null ? null : entity.getGroupId();
        this.label = entity.getLabel();
        this.properties = entity.getProperties() != null ? new org.json.JSONObject(entity.getProperties()).toMap() : null;
        if (entity.getGrant() != null) {
            this.grant = new GrantDMPEditorModel();
            this.grant.getExistGrant().fromDataModel(entity.getGrant());
            this.grant.getExistGrant().setSource("");
        }
        this.creator = new eu.eudat.models.data.userinfo.UserInfo();
        this.groupId = entity.getGroupId();
        this.lockable = entity.getDataset().stream().findAny().isPresent();
        if (this.properties != null) {
            this.extraFields = new ArrayList<>();
            this.properties.forEach((id, value) -> {
                ExtraFieldModel extraField = new ExtraFieldModel();
                extraField.setId(id);
                extraField.setValue(value.toString());
                this.extraFields.add(extraField);
            });
        }
        if (entity.getUsers().stream().anyMatch(userDMP -> userDMP.getRole().equals(UserDMP.UserDMPRoles.OWNER.getValue())))
            this.creator.fromDataModel(entity.getUsers().stream().filter(user -> user.getRole().equals(UserDMP.UserDMPRoles.OWNER.getValue())).findFirst().get().getUser());

        if (entity.getAssociatedDmps() != null && !entity.getAssociatedDmps().isEmpty()) {
            this.profiles = new LinkedList<>();
            for (DMPDatasetProfile dmpDescriptionProfile : entity.getAssociatedDmps()) {
                AssociatedProfile associatedProfile = new AssociatedProfile().fromData(dmpDescriptionProfile.getDatasetprofile());
                try {
                    associatedProfile.setData(new ObjectMapper().readValue(dmpDescriptionProfile.getData(), new TypeReference<Map<String, Object>>() {}));
                }
                catch (Exception e) {
                    associatedProfile.setData(null);
                }
                this.profiles.add(associatedProfile);
            }
        }
        this.datasets = entity.getDataset().stream().map(item -> new DatasetWizardModel().fromDataModelNoDmp(item)).collect(Collectors.toList());
        this.modified = entity.getModified();
        this.created = entity.getCreated();
        this.description = entity.getDescription();
        this.status = entity.getStatus();
        this.project = new ProjectDMPEditorModel();
        this.project.getExistProject().fromDataModel(entity.getProject());
        this.associatedUsers = entity.getUsers().stream().map(item -> new UserListingModel().fromDataModel(item.getUser())).collect(Collectors.toList());
        this.users = entity.getUsers().stream().map(item -> new UserInfoListingModel().fromDataModel(item)).collect(Collectors.toList());
        if (entity.getGrant() != null) {
            this.funder = new FunderDMPEditorModel();
            this.funder.getExistFunder().fromDataModel(entity.getGrant().getFunder());
        }
        this.extraProperties = entity.getExtraProperties() != null ? new org.json.JSONObject(entity.getExtraProperties()).toMap() : null;

        return this;
    }

    @Override
    public DMP toDataModel() throws Exception {
        DMP dataManagementPlanEntity = new DMP();
        if (this.profile != null) {
            DMPProfile dmpProfile = new DMPProfile();
            dmpProfile.setId(this.profile);
            dataManagementPlanEntity.setProfile(dmpProfile);
        }
        dataManagementPlanEntity.setId(this.id);
        if (this.organisations != null && !this.organisations.isEmpty())
            dataManagementPlanEntity.setOrganisations(new HashSet<>(this.organisations.stream().map(Organisation::toDataModel).collect(Collectors.toList())));
        if (this.researchers != null && !this.researchers.isEmpty())
            dataManagementPlanEntity.setResearchers(new HashSet<>(this.researchers.stream().map(Researcher::toDataModel).collect(Collectors.toList())));
        dataManagementPlanEntity.setVersion(this.version);
        dataManagementPlanEntity.setLabel(this.label);
        if (this.grant != null) {
            if (this.grant.getExistGrant() != null && this.grant.getLabel() == null && this.grant.getDescription() == null)
                dataManagementPlanEntity.setGrant(this.grant.getExistGrant().toDataModel());
            else if (this.grant.getLabel() != null) {
                Grant grant = new Grant();
                grant.setId(UUID.randomUUID());
                grant.setAbbreviation("");
                grant.setLabel(this.grant.getLabel());
                grant.setType(Grant.GrantType.INTERNAL.getValue());
                grant.setReference("dmp:" + (this.grant.getReference() != null ? this.grant.getReference() : grant.getId()));
                grant.setUri("");
                grant.setDefinition("");
                grant.setCreated(new Date());
                grant.setStatus(Grant.Status.ACTIVE.getValue());
                grant.setModified(new Date());
                grant.setDescription(this.grant.getDescription());

                dataManagementPlanEntity.setGrant(grant);
            }
        }

        if (this.funder != null) {
            if (this.funder.getLabel() != null) {
                Funder funder = new Funder();
                funder.setId(UUID.randomUUID());
                funder.setLabel(this.funder.getLabel());
                funder.setType(Funder.FunderType.INTERNAL.getValue());
                funder.setReference("dmp:" + (this.funder.getReference() != null ? this.funder.getReference() : funder.getId()));
                funder.setDefinition("");
                funder.setCreated(new Date());
                funder.setStatus(Funder.Status.ACTIVE.getValue());
                funder.setModified(new Date());

                dataManagementPlanEntity.getGrant().setFunder(funder);
            } else if (this.funder.getExistFunder() != null && this.funder.getLabel() == null){
                dataManagementPlanEntity.getGrant().setFunder(this.funder.getExistFunder().toDataModel());
                dataManagementPlanEntity.getGrant().getFunder().setType(Funder.FunderType.EXTERNAL.getValue());
            }
        }

        if (this.project != null) {
            if (this.project.getExistProject() != null && this.project.getLabel() == null && this.project.getDescription() == null)
                dataManagementPlanEntity.setProject(this.project.getExistProject().toDataModel());
            else if (this.project.getLabel() != null) {
                Project project = new Project();
                project.setId(UUID.randomUUID());
                project.setAbbreviation("");
                project.setLabel(this.project.getLabel());
                project.setType(Project.ProjectType.INTERNAL.getValue());
                project.setReference("dmp:" + (this.project.getReference() != null ? this.project.getReference() : project.getId()));
                project.setUri("");
                project.setDefinition("");
                project.setCreated(new Date());
                project.setStatus(Project.Status.ACTIVE.getValue());
                project.setModified(new Date());
                project.setDescription(this.project.getDescription());

                dataManagementPlanEntity.setProject(project);
            }
        }

        dataManagementPlanEntity.setStatus((short) this.status);
        if (this.status == (int) DMP.DMPStatus.FINALISED.getValue()) {
            dataManagementPlanEntity.setFinalizedAt(new Date());
        }
        dataManagementPlanEntity.setDescription(this.description);
        if (this.profiles != null) {
            Set<DMPDatasetProfile> dmpDatasetProfiles = new HashSet<>();
            for (AssociatedProfile profile : this.profiles) {
                DMPDatasetProfile dmpDatasetProfile = new DMPDatasetProfile();
                dmpDatasetProfile.setId(profile.getId());
                dmpDatasetProfile.setDmp(dataManagementPlanEntity);
                dmpDatasetProfile.setDatasetprofile(profile.toData());
                dmpDatasetProfile.setData(new ObjectMapper().writeValueAsString(profile.getData()));
                dmpDatasetProfiles.add(dmpDatasetProfile);
            }
            dataManagementPlanEntity.setAssociatedDmps(dmpDatasetProfiles);
        }
        if(this.extraFields != null) {
            this.properties = new HashMap<>();
            this.extraFields.forEach((extraField) -> this.properties.put(extraField.getId(), extraField.getValue()));
        }
        dataManagementPlanEntity.setProperties(this.properties != null ? JSONObject.toJSONString(this.properties) : null);
        dataManagementPlanEntity.setGroupId(this.groupId != null ? this.groupId : UUID.randomUUID());
        dataManagementPlanEntity.setModified(this.modified != null ? this.modified : new Date());
        dataManagementPlanEntity.setCreated(this.created != null ? this.created : new Date());
        if (this.dynamicFields != null)
            dataManagementPlanEntity.setDmpProperties(JSONObject.toJSONString(this.dynamicFields.stream().filter(item -> item.getValue() != null).collect(Collectors.toMap(DynamicFieldWithValue::getId, DynamicFieldWithValue::getValue))));

        dataManagementPlanEntity.setExtraProperties(this.extraProperties != null ? JSONObject.toJSONString(this.extraProperties) : null);

        return dataManagementPlanEntity;
    }

    @Override
    public String getHint() {
        return "fullyDetailed";
    }
}
