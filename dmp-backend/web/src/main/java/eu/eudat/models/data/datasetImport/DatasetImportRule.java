package eu.eudat.models.data.datasetImport;

public class DatasetImportRule {
    private String ruleType;
    private String target;
    private String ruleStyle;
    private String value;
    private String valueType;

    public String getRuleType() {
        return ruleType;
    }
    public void setRuleType(String ruleType) {
        this.ruleType = ruleType;
    }

    public String getTarget() {
        return target;
    }
    public void setTarget(String target) {
        this.target = target;
    }

    public String getRuleStyle() {
        return ruleStyle;
    }
    public void setRuleStyle(String ruleStyle) {
        this.ruleStyle = ruleStyle;
    }

    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }

    public String getValueType() {
        return valueType;
    }
    public void setValueType(String valueType) {
        this.valueType = valueType;
    }
}
