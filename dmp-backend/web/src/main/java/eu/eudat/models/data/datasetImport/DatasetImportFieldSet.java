package eu.eudat.models.data.datasetImport;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collections;
import java.util.List;

@XmlRootElement(name = "composite-field")
public class DatasetImportFieldSet implements Comparable{
    //@XmlAttribute(name = "id")
    private String id;
    private Integer ordinal;
    private String title;
    private String numbering;
    private String description;
    private String extendedDescription;
    private String additionalInformation;
    private DatasetImportMultiplicity multiplicity;
    private List<DatasetImportFields> fields;
    private List<DatasetImportFieldSets> compositeFields;
    private boolean hasCommentField;
    private String commentFieldValue;

    @XmlElement(name = "fields")
    public List<DatasetImportFields> getFields() {
        //Collections.sort(this.fields);
        return fields;
    }
    public void setFields(List<DatasetImportFields> fields) {
        this.fields = fields;
    }

    @XmlElement(name = "title")
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    @XmlElement(name = "description")
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public String getExtendedDescription() { return extendedDescription; }
    public void setExtendedDescription(String extendedDescription) {
        this.extendedDescription = extendedDescription;
    }

    @XmlAttribute(name = "id")
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public int getOrdinal() {
        return ordinal;
    }
    public void setOrdinal(Integer ordinal) {
        this.ordinal = ordinal;
    }

    public DatasetImportMultiplicity getMultiplicity() {
        return multiplicity;
    }
    public void setMultiplicity(DatasetImportMultiplicity multiplicity) {
        this.multiplicity = multiplicity;
    }

    @XmlElement(name = "composite-fields")
    public List<DatasetImportFieldSets> getcompositeFields() {
        //if (compositeFields != null) Collections.sort(compositeFields);
        return compositeFields;
    }
    public void setcompositeFields(List<DatasetImportFieldSets> compositeFields) {
        this.compositeFields = compositeFields;
    }

    public String getNumbering() {
        return numbering;
    }
    public void setNumbering(String numbering) {
        this.numbering = numbering;
    }

    public void setHasCommentField(boolean hasCommentField) {
        this.hasCommentField = hasCommentField;
    }
    public boolean getHasCommentField() {
        return hasCommentField;
    }

    public String getCommentFieldValue() {
        return commentFieldValue;
    }
    public void setCommentFieldValue(String commentFieldValue) {
        this.commentFieldValue = commentFieldValue;
    }

    public String getAdditionalInformation() {
        return additionalInformation;
    }
    public void setAdditionalInformation(String additionalInformation) {
        this.additionalInformation = additionalInformation;
    }

    @Override
    public int compareTo(Object o) {
        return this.ordinal.compareTo(((DatasetImportFieldSet) o).getOrdinal());
    }


}
