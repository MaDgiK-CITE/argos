package eu.eudat.models.data.userinfo;

import java.util.UUID;

public class UserMergeRequestModel {
	private UUID userId;
	private String email;
	private Integer provider;
	public UUID getUserId() {
		return userId;
	}
	public void setUserId(UUID userId) {
		this.userId = userId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getProvider() {
		return provider;
	}

	public void setProvider(Integer provider) {
		this.provider = provider;
	}
}
