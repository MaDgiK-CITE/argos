package eu.eudat.models.rda.mapper;

import eu.eudat.models.rda.Contributor;
import eu.eudat.models.rda.ContributorId;

import java.util.UUID;

public class ContributorIdRDAMapper {

	public static ContributorId toRDA(Object id) {
		ContributorId rda = new ContributorId();
		String idParts[] = id.toString().split(":");
		String prefix = idParts.length > 1 ? idParts[0] : id.toString();
		if (prefix.equals("orcid")) {
			String finalId = id.toString().replace(prefix + ":", "");
			rda.setIdentifier("http://orcid.org/" + finalId);
			rda.setType(ContributorId.Type.ORCID);
		} else {
			rda.setIdentifier(id.toString());
			rda.setType(ContributorId.Type.OTHER);
		}
		return rda;
	}
}
