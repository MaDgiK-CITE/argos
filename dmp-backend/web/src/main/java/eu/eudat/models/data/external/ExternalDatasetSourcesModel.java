package eu.eudat.models.data.external;

import java.util.List;
import java.util.Map;


public class ExternalDatasetSourcesModel extends ExternalListingItem<ExternalDatasetSourcesModel> {
    @Override
    public ExternalDatasetSourcesModel fromExternalItem(List<Map<String, String>> values) {
        for (Map<String, String> item : values) {
            ExternalSourcesItemModel model = new ExternalSourcesItemModel();
            model.setId((String)item.get("pid"));
            model.setUri((String)item.get("uri"));
            model.setName((String)item.get("name"));
            model.setSource((String)item.get("source"));
            this.add(model);
        }
        return this;
    }
}
