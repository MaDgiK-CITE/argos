
package eu.eudat.models.rda;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;


/**
 * The Funding Grant ID Schema
 * <p>
 * Grant ID of the associated project
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "identifier",
    "type"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class GrantId implements Serializable
{

    /**
     * The Funding Grant ID Value Schema
     * <p>
     * Grant ID
     * (Required)
     * 
     */
    @JsonProperty("identifier")
    @JsonPropertyDescription("Grant ID")
    private String identifier;
    /**
     * The Funding Grant ID Type Schema
     * <p>
     * Identifier type. Allowed values: url, other
     * (Required)
     * 
     */
    @JsonProperty("type")
    @JsonPropertyDescription("Identifier type. Allowed values: url, other")
    private GrantId.Type type;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = -7738072672837592065L;

    /**
     * The Funding Grant ID Value Schema
     * <p>
     * Grant ID
     * (Required)
     * 
     */
    @JsonProperty("identifier")
    public String getIdentifier() {
        return identifier;
    }

    /**
     * The Funding Grant ID Value Schema
     * <p>
     * Grant ID
     * (Required)
     * 
     */
    @JsonProperty("identifier")
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    /**
     * The Funding Grant ID Type Schema
     * <p>
     * Identifier type. Allowed values: url, other
     * (Required)
     * 
     */
    @JsonProperty("type")
    public GrantId.Type getType() {
        return type;
    }

    /**
     * The Funding Grant ID Type Schema
     * <p>
     * Identifier type. Allowed values: url, other
     * (Required)
     * 
     */
    @JsonProperty("type")
    public void setType(GrantId.Type type) {
        this.type = type;
    }

    @JsonProperty("additional_properties")
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonProperty("additional_properties")
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public enum Type {

        URL("url"),
        OTHER("other");
        private final String value;
        private final static Map<String, GrantId.Type> CONSTANTS = new HashMap<String, GrantId.Type>();

        static {
            for (GrantId.Type c: values()) {
                CONSTANTS.put(c.value, c);
            }
        }

        private Type(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return this.value;
        }

        @JsonValue
        public String value() {
            return this.value;
        }

        @JsonCreator
        public static GrantId.Type fromValue(String value) {
            GrantId.Type constant = CONSTANTS.get(value);
            if (constant == null) {
                throw new IllegalArgumentException(value);
            } else {
                return constant;
            }
        }

    }

}
