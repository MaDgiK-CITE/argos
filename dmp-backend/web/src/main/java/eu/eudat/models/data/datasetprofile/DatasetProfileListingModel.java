package eu.eudat.models.data.datasetprofile;

import eu.eudat.data.entities.DescriptionTemplate;
import eu.eudat.models.DataModel;

import java.util.Date;
import java.util.UUID;

public class DatasetProfileListingModel implements DataModel<DescriptionTemplate, DatasetProfileListingModel> {

    private UUID id;
    private String label;
    private Short status;
    private Date created;
    private Date modified = new Date();
    private String description;
    private Short version;
    private UUID groupId;

    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }

    public Short getStatus() {
        return status;
    }
    public void setStatus(Short status) {
        this.status = status;
    }

    public Date getCreated() {
        return created;
    }
    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }
    public void setModified(Date modified) {
        this.modified = modified;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public Short getVersion() { return version; }
    public void setVersion(Short version) { this.version = version; }

    public UUID getGroupId() { return groupId; }
    public void setGroupId(UUID groupId) { this.groupId = groupId; }

    @Override
    public DatasetProfileListingModel fromDataModel(DescriptionTemplate entity) {
        this.id = entity.getId();
        this.label = entity.getLabel();
        this.status = entity.getStatus();
        this.created = entity.getCreated();
        this.modified = entity.getModified();
        this.description = entity.getDescription();
        this.version = entity.getVersion();
        this.groupId = entity.getGroupId();
        return this;
    }

    @Override
    public DescriptionTemplate toDataModel() {
        DescriptionTemplate profile = new DescriptionTemplate();
        profile.setId(this.id);
        return profile;
    }

    @Override
    public String getHint() {
        return null;
    }
}
