package eu.eudat.models.data.datasetImport;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "fields")
public class DatasetImportFields implements Comparable{

    private List<DatasetImportField> field;

    @XmlElement(name = "field")
    public List<DatasetImportField> getField() {
        return field;
    }
    public void setField(List<DatasetImportField> field) {
        this.field = field;
    }

    @Override
    public int compareTo(Object o) { return 0; }
}
