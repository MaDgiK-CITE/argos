package eu.eudat.models.data.components.commons;

public class ViewStyle {
    private String renderStyle;
    private String cssClass;

    public String getRenderStyle() {
        return renderStyle;
    }

    public void setRenderStyle(String renderStyle) {
        this.renderStyle = renderStyle;
    }

    public String getCssClass() {
        return cssClass;
    }

    public void setCssClass(String cssClass) {
        this.cssClass = cssClass;
    }

    public enum Type {
        COMBO_BOX("combobox"),
        BOOLEAN_DECISION("booleanDecision"),
        RADIO_BOX("radiobox"),
        INTERNAL_DMP_ENTRIES("internalDmpEntities"),
        CHECK_BOX("checkBox"),
        FREE_TEXT("freetext"),
        TEXT_AREA("textarea"),
        RICH_TEXT_AREA("richTextarea"),
        UPLOAD("upload"),
//        TABLE("table"),
        DATE_PICKER("datePicker"),
        EXTERNAL_DATASETS("externalDatasets"),
        DATA_REPOSITORIES("dataRepositories"),
        JOURNAL_REPOSITORIES("journalRepositories"),
        PUB_REPOSITORIES("pubRepositories"),
        LICENSES("licenses"),
        TAXONOMIES("taxonomies"),
        PUBLICATIONS("publications"),
        REGISTRIES("registries"),
        SERVICES("services"),
        TAGS("tags"),
        RESEARCHERS("researchers"),
        ORGANIZATIONS("organizations"),
        DATASET_IDENTIFIER("datasetIdentifier"),
        CURRENCY("currency"),
        VALIDATION("validation");

        private final String name;

        Type(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public static Type fromName(String name) {
            for (Type type: Type.values()) {
                if (name.equals(type.getName())) {
                    return type;
                }
            }
            throw new IllegalArgumentException("View Style Type [" + name + "] is not valid");
        }
    }

}
