package eu.eudat.models.data.datasetImport;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collections;
import java.util.List;

@XmlRootElement(name = "sections")
public class DatasetImportSections{
    private List<DatasetImportSection> section;

    @XmlElement(name = "section")
    public List<DatasetImportSection> getSection() {
        return section;
    }
    public void setSection(List<DatasetImportSection> section) {
        this.section = section;
    }
}
