package eu.eudat.models.data.rda;

public class RdaField {
	private String rdaProperty;
	private String rdaValue;
	private String fieldId;
	private String fieldSetId;

	public String getRdaProperty() {
		return rdaProperty;
	}
	public void setRdaProperty(String rdaProperty) {
		this.rdaProperty = rdaProperty;
	}

	public String getRdaValue() {
		return rdaValue;
	}
	public void setRdaValue(String rdaValue) {
		this.rdaValue = rdaValue;
	}

	public String getFieldId() {
		return fieldId;
	}
	public void setFieldId(String fieldId) {
		this.fieldId = fieldId;
	}

	public String getFieldSetId() {
		return fieldSetId;
	}
	public void setFieldSetId(String fieldSetId) {
		this.fieldSetId = fieldSetId;
	}
}
