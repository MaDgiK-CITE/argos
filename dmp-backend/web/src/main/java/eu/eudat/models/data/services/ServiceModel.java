package eu.eudat.models.data.services;

import eu.eudat.data.entities.Service;
import eu.eudat.data.entities.UserInfo;
import eu.eudat.models.DataModel;

import java.util.Date;
import java.util.UUID;

public class ServiceModel implements DataModel<Service, ServiceModel> {
    private UUID id;
    private String label;
    private String name;
    private String pid;
    private String abbreviation;
    private String uri;
    private Date created;
    private Date modified;
    private String reference;
    private String tag;
    private String source;

    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getPid() {
        return pid;
    }
    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getAbbreviation() {
        return abbreviation;
    }
    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getUri() {
        return uri;
    }
    public void setUri(String uri) {
        this.uri = uri;
    }

    public Date getCreated() {
        return created;
    }
    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }
    public void setModified(Date modified) {
        this.modified = modified;
    }

    public String getReference() {
        return reference;
    }
    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getTag() {
        return tag;
    }
    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getSource() {
        return source;
    }
    public void setSource(String source) {
        this.source = source;
    }

    @Override
    public ServiceModel fromDataModel(Service entity) {
        this.abbreviation = entity.getAbbreviation();
        this.created = entity.getCreated();
        this.id = entity.getId();
        this.label = entity.getLabel();
        this.name = entity.getLabel();
        this.modified = entity.getModified();
        this.uri = entity.getUri();
        String source = entity.getReference().substring(0, entity.getReference().indexOf(":"));
        if (source.equals("dmp")) {
            this.source = "Internal";
        } else {
            this.source = source;
        }
        return this;
    }

    @Override
    public Service toDataModel() throws Exception {
        Service service = new Service();
        service.setId(this.id != null ? this.id : UUID.randomUUID());
        service.setAbbreviation(this.abbreviation);
        service.setCreated(this.created != null ? this.created : new Date());
        service.setLabel(this.label != null ? this.label : this.name);
        service.setModified(new Date());
        service.setUri(this.uri);
        if (this.source == null) this.source = "dmp";
        if (this.reference == null) this.reference = service.getId().toString();
        if (this.source.equals(this.reference.substring(0, this.source.length()))) {
            service.setReference(this.reference);
        } else {
            service.setReference(this.source + ":" + this.reference);
        }
        service.setModified(new Date());
        service.setStatus((short) 0);
        service.setCreationUser(new UserInfo());
        return service;
    }

    @Override
    public String getHint() {
        return null;
    }
}
