package eu.eudat.models.data.dataset;

import eu.eudat.data.entities.Dataset;
import eu.eudat.models.DataModel;
import eu.eudat.models.data.datasetprofile.DatasetProfileOverviewModel;
import eu.eudat.models.data.grant.GrantOverviewModel;
import eu.eudat.models.data.listingmodels.DataManagementPlanOverviewModel;
import eu.eudat.models.data.listingmodels.UserInfoListingModel;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class DatasetOverviewModel implements DataModel<Dataset, DatasetOverviewModel> {

    private UUID id;
    private String label;
    private short status;
    private DatasetProfileOverviewModel datasetTemplate;
    private List<UserInfoListingModel> users;
    private DataManagementPlanOverviewModel dmp;
    private GrantOverviewModel grant;
    private String description;
    private Boolean isPublic;
    private Date modified;
    private Date created;

    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }

    public short getStatus() {
        return status;
    }
    public void setStatus(short status) {
        this.status = status;
    }

    public DatasetProfileOverviewModel getDatasetTemplate() {
        return datasetTemplate;
    }
    public void setDatasetTemplate(DatasetProfileOverviewModel datasetTemplate) {
        this.datasetTemplate = datasetTemplate;
    }

    public List<UserInfoListingModel> getUsers() {
        return users;
    }

    public void setUsers(List<UserInfoListingModel> users) {
        this.users = users;
    }

    public DataManagementPlanOverviewModel getDmp() {
        return dmp;
    }

    public void setDmp(DataManagementPlanOverviewModel dmp) {
        this.dmp = dmp;
    }

    public GrantOverviewModel getGrant() {
        return grant;
    }

    public void setGrant(GrantOverviewModel grant) {
        this.grant = grant;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getPublic() {
        return isPublic;
    }

    public void setPublic(Boolean aPublic) {
        isPublic = aPublic;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Override
    public DatasetOverviewModel fromDataModel(Dataset entity) {
        this.id = entity.getId();
        this.label = entity.getLabel();
        this.status = entity.getStatus();
        this.datasetTemplate = new DatasetProfileOverviewModel().fromDataModel(entity.getProfile());
        this.users = entity.getDmp().getUsers().stream().map(x -> new UserInfoListingModel().fromDataModel(x)).collect(Collectors.toList());
        this.dmp = new DataManagementPlanOverviewModel().fromDataModel(entity.getDmp());
        if (entity.getDmp().getGrant() != null) {
            this.grant = new GrantOverviewModel().fromDataModel(entity.getDmp().getGrant());
        }
        this.description = entity.getDescription();
        this.isPublic = entity.getDmp().isPublic();
        this.modified = entity.getModified();
        this.created = entity.getCreated();

        return this;
    }

    @Override
    public Dataset toDataModel() throws Exception {
        return null;
    }

    @Override
    public String getHint() {
        return null;
    }
}
