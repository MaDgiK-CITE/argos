package eu.eudat.models.data.urls;

import eu.eudat.data.entities.DMP;

/**
 * Created by ikalyvas on 3/19/2018.
 */
public class DataManagementPlanUrlListing extends UrlListing<DMP, DataManagementPlanUrlListing> {

    @Override
    public DataManagementPlanUrlListing fromDataModel(DMP entity) {
        this.setLabel(entity.getLabel());
        this.setUrl(entity.getId().toString());
        return this;
    }

    @Override
    public DMP toDataModel() throws Exception {
        return null;
    }

    @Override
    public String getHint() {
        return null;
    }
}
