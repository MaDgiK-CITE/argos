package eu.eudat.models.data.funder;

public class FunderDMPEditorModel {
	private Funder existFunder;
	private String label;
	private String reference;

	public Funder getExistFunder() {
		return existFunder;
	}
	public void setExistFunder(Funder existFunder) {
		this.existFunder = existFunder;
	}

	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}
}
