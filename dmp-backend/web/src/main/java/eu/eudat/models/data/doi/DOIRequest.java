package eu.eudat.models.data.doi;

import eu.eudat.logic.security.validators.zenodo.helpers.ZenodoRequest;

public class DOIRequest {

	private ZenodoRequest zenodoRequest;
	private String redirectUri;

	public ZenodoRequest getZenodoRequest() {
		return zenodoRequest;
	}

	public void setZenodoRequest(ZenodoRequest zenodoRequest) {
		this.zenodoRequest = zenodoRequest;
	}

	public String getRedirectUri() {
		return redirectUri;
	}

	public void setRedirectUri(String redirectUri) {
		this.redirectUri = redirectUri;
	}
}
