package eu.eudat.models;


import eu.eudat.queryable.queryableentity.DataEntity;

public interface DataModel<T extends DataEntity, M extends DataModel> {
    M fromDataModel(T entity);

    T toDataModel() throws Exception;

    String getHint();
}
