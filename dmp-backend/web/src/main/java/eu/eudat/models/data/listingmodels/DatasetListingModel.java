package eu.eudat.models.data.listingmodels;

import eu.eudat.data.entities.DMP;
import eu.eudat.data.entities.Dataset;
import eu.eudat.data.entities.Grant;
import eu.eudat.models.DataModel;
import eu.eudat.models.data.datasetprofile.DatasetProfileOverviewModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;


public class DatasetListingModel implements DataModel<Dataset, DatasetListingModel> {
    private String id;
    private String label;
    private String grant;
    private String dmp;
    private String dmpId;
    private DatasetProfileOverviewModel profile;
    private int status;
    private Date created;
    private Date modified;
    private String description;
    private Date finalizedAt;
    private Date dmpPublishedAt;
    private int version;
    private List<UserInfoListingModel> users;
    private Boolean isPublic;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }

    public String getGrant() {
        return grant;
    }
    public void setGrant(String grant) {
        this.grant = grant;
    }

    public String getDmp() {
        return dmp;
    }
    public void setDmp(String dmp) {
        this.dmp = dmp;
    }

    public String getDmpId() {
        return dmpId;
    }
    public void setDmpId(String dmpId) {
        this.dmpId = dmpId;
    }

    public DatasetProfileOverviewModel getProfile() {
        return profile;
    }

    public void setProfile(DatasetProfileOverviewModel profile) {
        this.profile = profile;
    }

    public int getStatus() {
        return status;
    }
    public void setStatus(int status) {
        this.status = status;
    }

    public Date getCreated() {
        return created;
    }
    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }
    public void setModified(Date modified) {
        this.modified = modified;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public Date getFinalizedAt() {
        return finalizedAt;
    }
    public void setFinalizedAt(Date finalizedAt) {
        this.finalizedAt = finalizedAt;
    }

    public Date getDmpPublishedAt() {
        return dmpPublishedAt;
    }
    public void setDmpPublishedAt(Date dmpPublishedAt) {
        this.dmpPublishedAt = dmpPublishedAt;
    }

    public int getVersion() {
        return version;
    }
    public void setVersion(int version) {
        this.version = version;
    }

    public List<UserInfoListingModel> getUsers() {
        return users;
    }

    public void setUsers(List<UserInfoListingModel> users) {
        this.users = users;
    }

    public Boolean getPublic() {
        return isPublic;
    }

    public void setPublic(Boolean aPublic) {
        isPublic = aPublic;
    }

    @Override
    public DatasetListingModel fromDataModel(Dataset entity) {
        this.id = entity.getId() != null ? entity.getId().toString() : "";
        this.label = entity.getLabel();
        this.created = entity.getCreated();
        this.modified = entity.getModified();
        this.grant = (entity.getDmp() != null && entity.getDmp().getGrant() != null) ? entity.getDmp().getGrant().getLabel() : "";
        this.dmp = entity.getDmp() != null ? entity.getDmp().getLabel() : "";
        this.dmpId = entity.getDmp() != null ? entity.getDmp().getId().toString() : "";
        this.profile = entity.getProfile() != null ? new DatasetProfileOverviewModel().fromDataModel(entity.getProfile()) : null;
        this.description = entity.getDescription();
        this.status = entity.getStatus();
        if (entity.getFinalizedAt() == null && entity.getStatus() == Dataset.Status.FINALISED.getValue()) {
            this.finalizedAt = entity.getDmp().getFinalizedAt();
        } else {
            this.finalizedAt = entity.getFinalizedAt();
        }
        this.dmpPublishedAt = entity.getDmp().getPublishedAt();
        this.version = entity.getDmp().getVersion();
        this.users = entity.getDmp() != null ? entity.getDmp().getUsers().stream().map(x -> new UserInfoListingModel().fromDataModel(x)).collect(Collectors.toList()) : new ArrayList<>();
        this.isPublic = entity.getDmp() != null ? entity.getDmp().isPublic() : false;
        return this;
    }

    @Override
    public Dataset toDataModel() {
        Dataset entity = new Dataset();
        entity.setId(UUID.fromString(this.getId()));
        entity.setLabel(this.getLabel());
        entity.setCreated(this.getCreated());
        entity.setModified(this.getModified());
        entity.setDescription(this.getDescription());
        entity.setFinalizedAt(this.getFinalizedAt());
        entity.setStatus(Integer.valueOf(this.getStatus()).shortValue());
        DMP dmp = new DMP();
        if (this.getGrant() != null && !this.getGrant().isEmpty()) {
            Grant grant = new Grant();
            grant.setLabel(this.getGrant());
            dmp.setGrant(grant);
        }
        dmp.setLabel(this.getDmp());
        dmp.setId(UUID.fromString(this.getDmpId()));
        dmp.setPublishedAt(this.getDmpPublishedAt());
        dmp.setVersion(this.getVersion());
        dmp.setUsers(this.getUsers().stream().map(UserInfoListingModel::toDataModel).collect(Collectors.toSet()));
        dmp.setPublic(this.getPublic());
        dmp.setFinalizedAt(this.getFinalizedAt());
        entity.setDmp(dmp);
        entity.setProfile(this.getProfile() != null ? this.getProfile().toDataModel() : null);
        return entity;
    }

    @Override
    public String getHint() {
        return "datasetListingModel";
    }
}
