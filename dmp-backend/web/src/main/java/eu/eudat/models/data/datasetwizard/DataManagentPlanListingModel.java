package eu.eudat.models.data.datasetwizard;

import eu.eudat.data.entities.DMP;
import eu.eudat.models.DataModel;


public class DataManagentPlanListingModel implements DataModel<DMP, DataManagentPlanListingModel> {

    private String id;
    private String label;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public DataManagentPlanListingModel fromDataModel(DMP entity) {
        this.id = entity.getId().toString();
        this.label = entity.getLabel();
        return this;
    }

    @Override
    public DMP toDataModel() {
        return null;
    }

    @Override
    public String getHint() {
        return null;
    }
}
