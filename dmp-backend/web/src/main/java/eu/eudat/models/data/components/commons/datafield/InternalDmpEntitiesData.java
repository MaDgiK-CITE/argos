package eu.eudat.models.data.components.commons.datafield;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.util.HashMap;
import java.util.Map;

public abstract class InternalDmpEntitiesData<T> extends FieldData<T> {
	private String type;

	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	@Override
	public Element toXml(Document doc) {
		Element root = doc.createElement("data");
		root.setAttribute("type", this.type);
		root.setAttribute("label", this.getLabel());
		return root;
	}

	@Override
	public T fromXml(Element item) {
		this.setLabel(item.getAttribute("label"));
		this.type = item.getAttribute("type");
		return (T) this;
	}

	@Override
	public T fromData(Object data) {

		if (data != null) {
			this.type = (String) ((Map<String, Object>) data).get("type");
			this.setLabel((String) ((Map<String, Object>) data).get("label"));
		}

		return (T) this;
	}

	@Override
	public Object toData() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Object> toMap(Element item) {
		HashMap dataMap = new HashMap();
		dataMap.put("label", item != null ? item.getAttribute("label") : "");
		dataMap.put("type", item != null ? item.getAttribute("type") : "");
		dataMap.put("type", item != null ? item.getAttribute("type") : "researchers");
		return dataMap;
	}
}
