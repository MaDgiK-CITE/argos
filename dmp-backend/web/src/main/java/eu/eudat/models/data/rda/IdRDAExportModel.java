package eu.eudat.models.data.rda;

public class IdRDAExportModel {
	private String identifier;
	private String type;

	public String getIdentifier() {
		return identifier;
	}
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	IdRDAExportModel(String identifier, String type) {
		this.identifier = identifier;
		this.type = type;
	}
}
