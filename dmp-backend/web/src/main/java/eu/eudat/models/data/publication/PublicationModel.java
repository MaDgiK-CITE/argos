package eu.eudat.models.data.publication;

import eu.eudat.data.entities.DataRepository;
import eu.eudat.data.entities.UserInfo;
import eu.eudat.models.DataModel;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class PublicationModel implements DataModel<DataRepository, PublicationModel> {
    private UUID id;
    private List<String> titles;
    private List<String> ids;
    private String name;
    private String pid;
    private String pidTypeField;
    private String abbreviation;
    private String uri;
    private Date created;
    private Date modified;
    private String tag; // Api fetching the data
    private String source; // Actual harvested source

    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }

    public List<String> getTitles() {
        return titles;
    }

    public void setTitles(List<String> titles) {
        this.titles = titles;
    }

    public List<String> getIds() {
        return ids;
    }

    public void setIds(List<String> ids) {
        this.ids = ids;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getPidTypeField() {
        return pidTypeField;
    }

    public void setPidTypeField(String pidTypeField) {
        this.pidTypeField = pidTypeField;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public PublicationModel fromDataModel(DataRepository entity) {
        this.setAbbreviation(entity.getAbbreviation());
        this.setName(entity.getLabel());

        this.setTitles(new ArrayList<>());
        this.getTitles().add(entity.getLabel());
        this.setUri(entity.getUri());
        this.setId(entity.getId());
        this.setPid(entity.getReference());
        String source1 = entity.getReference().substring(0, entity.getReference().indexOf(":"));
        if (source1.equals("dmp")) {
            this.source = "Internal";
        } else {
            this.source = source1;
        }
        return this;
    }

    @Override
    public DataRepository toDataModel() throws Exception {
        DataRepository dataRepository = new DataRepository();
        dataRepository.setId(this.id != null ? this.id : UUID.randomUUID());
        dataRepository.setAbbreviation(this.abbreviation);
        dataRepository.setCreated(this.created != null ? this.created : new Date());
        dataRepository.setModified(new Date());
        dataRepository.setLabel(this.name);
        if (this.source != null) {
            if (this.source.equals("Internal") || this.source.equals(this.id.toString().substring(0, this.source.length()))) {
                dataRepository.setReference(this.id.toString());
            } else {
                dataRepository.setReference(this.source + ":" + dataRepository.getId());
            }
        } else {
            dataRepository.setReference("dmp:" + dataRepository.getId());
        }
        dataRepository.setUri(this.uri);
        dataRepository.setStatus((short) 0);
        dataRepository.setCreationUser(new UserInfo());
        return dataRepository;
    }

    @Override
    public String getHint() {
        return null;
    }
}
