package eu.eudat.models.validators.fluentvalidator.predicates;

/**
 * Created by ikalyvas on 8/31/2018.
 */
public interface ComparisonOperator<L, R> {
    boolean compare(L leftItem, R rightItem);
}
