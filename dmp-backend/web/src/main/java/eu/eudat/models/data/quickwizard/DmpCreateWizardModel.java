package eu.eudat.models.data.quickwizard;

import eu.eudat.data.entities.DMP;
import eu.eudat.models.data.dmp.DataManagementPlan;

public class DmpCreateWizardModel {
    private DatasetProfileCreateWizardModel datasetProfile;
    private DataManagementPlan dmp;

    public DatasetProfileCreateWizardModel getDatasetProfile() {return datasetProfile; }
    public void setDatasetProfile(DatasetProfileCreateWizardModel datasetProfileCreateWizardModel) {
        this.datasetProfile = datasetProfileCreateWizardModel;
    }

    public DataManagementPlan getDmp() { return dmp; }
    public void setDmp(DataManagementPlan dmp) { this.dmp = dmp; }
}
