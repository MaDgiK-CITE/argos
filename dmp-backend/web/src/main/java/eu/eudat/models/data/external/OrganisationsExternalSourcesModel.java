package eu.eudat.models.data.external;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class OrganisationsExternalSourcesModel extends ExternalListingItem<OrganisationsExternalSourcesModel> {
    @Override
    public OrganisationsExternalSourcesModel fromExternalItem(List<Map<String, String>> values) {
        for (Map<String, String> item : values) {
            ExternalSourcesItemModel model = new ExternalSourcesItemModel();
            model.setRemoteId((String)item.get("pid"));
            model.setUri((String)item.get("uri"));
            model.setName((String)item.get("name"));
            model.setTag((String)item.get("tag"));
            model.setKey((String)item.get("key"));
            this.add(model);
        }
        return this;
    }
}
