package eu.eudat.models.rda.mapper;

import eu.eudat.models.rda.ContactId;

import java.util.UUID;

public class ContactIdRDAMapper {

	public static ContactId toRDA(UUID id) {
		ContactId rda = new ContactId();
		rda.setIdentifier(id.toString());
		rda.setType(ContactId.Type.OTHER);
		return rda;
	}

	public static UUID toEntity(ContactId rda) {
		return UUID.fromString(rda.getIdentifier());
	}
}
