package eu.eudat.models.data.components.commons.datafield;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.util.HashMap;
import java.util.Map;

public class DMPsAutoCompleteData extends InternalDmpEntitiesData<DMPsAutoCompleteData>{
	private Boolean multiAutoComplete;

	public Boolean getMultiAutoComplete() {
		return multiAutoComplete;
	}
	public void setMultiAutoComplete(Boolean multiAutoComplete) {
		this.multiAutoComplete = multiAutoComplete;
	}

	@Override
	public Element toXml(Document doc) {
		Element root = super.toXml(doc);
		root.setAttribute("multiAutoComplete", this.multiAutoComplete.toString());

		return root;
	}

	@Override
	public DMPsAutoCompleteData fromXml(Element item) {
		super.fromXml(item);
		this.multiAutoComplete = Boolean.parseBoolean(item.getAttribute("multiAutoComplete"));

		return this;
	}

	@Override
	public DMPsAutoCompleteData fromData(Object data) {
		super.fromData(data);
		if (data != null) {
			this.multiAutoComplete = (Boolean) ((Map<Boolean, Object>) data).get("multiAutoComplete");
		}

		return this;
	}

	@Override
	public Object toData() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Object> toMap(Element item) {
		HashMap dataMap = new HashMap();
		dataMap.put("label", item != null ? item.getAttribute("label") : "");
		dataMap.put("type", item != null ? item.getAttribute("type") : "dmps");
		dataMap.put("multiAutoComplete", item != null ? Boolean.parseBoolean(item.getAttribute("multiAutocomplete")) : false);

		return dataMap;
	}
}
