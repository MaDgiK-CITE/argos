package eu.eudat.models.data.loginprovider;

import eu.eudat.logic.security.validators.TokenValidatorFactoryImpl;


public class LoginProviderUser {
    private String name;
    private String id;
    private String email;
    private String secret;
    private String avatarUrl;
    private boolean isVerified;
    private TokenValidatorFactoryImpl.LoginProvider provider;
    private String zenodoId;
    private Integer zenodoExpire;
    private String zenodoRefresh;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean getIsVerified() {
        return isVerified;
    }

    public void setIsVerified(boolean verified) {
        isVerified = verified;
    }

    public TokenValidatorFactoryImpl.LoginProvider getProvider() {
        return provider;
    }

    public void setProvider(TokenValidatorFactoryImpl.LoginProvider provider) {
        this.provider = provider;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getZenodoId() {
        return zenodoId;
    }

    public void setZenodoId(String zenodoId) {
        this.zenodoId = zenodoId;
    }

    public Integer getZenodoExpire() {
        return zenodoExpire;
    }

    public void setZenodoExpire(Integer zenodoExpire) {
        this.zenodoExpire = zenodoExpire;
    }

    public String getZenodoRefresh() {
        return zenodoRefresh;
    }

    public void setZenodoRefresh(String zenodoRefresh) {
        this.zenodoRefresh = zenodoRefresh;
    }
}
