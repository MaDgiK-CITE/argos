package eu.eudat.models.data.quickwizard;

import eu.eudat.models.data.funder.Funder;

import java.util.UUID;

public class FunderQuickWizardModel {
	private String label;
	private Funder existFunder;

	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}

	public Funder getExistFunder() {
		return existFunder;
	}
	public void setExistFunder(Funder existFunder) {
		this.existFunder = existFunder;
	}

	public Funder toDataFunder() {
		Funder toFunder = new Funder();
		toFunder.setId(UUID.randomUUID());
		toFunder.setLabel(this.label);
		toFunder.setReference("dmp:" + toFunder.getId());
		toFunder.setDefinition("");
		toFunder.setType(eu.eudat.data.entities.Funder.FunderType.INTERNAL.getValue());
		return  toFunder;
	}
}
