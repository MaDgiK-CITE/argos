package eu.eudat.models.data.properties;

import java.util.Map;

public interface PropertiesGenerator {
    void toMap(Map<String, Object> fieldValues);

    void toMap(Map<String, Object> fieldValues, int index);
}
