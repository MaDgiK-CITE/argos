package eu.eudat.models.data.grant;

import eu.eudat.data.entities.Grant;
import eu.eudat.models.DataModel;

import java.util.UUID;


public class GrantCriteriaModel implements DataModel<eu.eudat.data.entities.Grant, GrantCriteriaModel> {
    private UUID id;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    @Override
    public GrantCriteriaModel fromDataModel(Grant entity) {
        this.id = entity.getId();
        return this;
    }

    @Override
    public Grant toDataModel() {
        Grant grant = new Grant();
        grant.setId(this.id);
        return grant;
    }

    @Override
    public String getHint() {
        return null;
    }
}
