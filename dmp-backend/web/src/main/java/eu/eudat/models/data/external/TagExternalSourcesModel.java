package eu.eudat.models.data.external;

import java.util.List;
import java.util.Map;

/**
 * Created by ikalyvas on 7/9/2018.
 */
public class TagExternalSourcesModel extends ExternalListingItem<TagExternalSourcesModel> {

    @Override
    public TagExternalSourcesModel fromExternalItem(List<Map<String, String>> values) {
        for (Map<String, String> item : values) {
            ExternalSourcesItemModel model = new ExternalSourcesItemModel();
            model.setId((String)item.get("pid"));
            model.setUri((String)item.get("label"));
            model.setName((String)item.get("name"));
            model.setTag((String)item.get("tag"));
            this.add(model);
        }
        return this;
    }
}
