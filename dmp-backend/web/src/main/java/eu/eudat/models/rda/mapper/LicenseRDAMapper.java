package eu.eudat.models.rda.mapper;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import eu.eudat.logic.utilities.json.JsonSearcher;
import eu.eudat.models.rda.License;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LicenseRDAMapper {
	private static final Logger logger = LoggerFactory.getLogger(LicenseRDAMapper.class);

	public static License toRDA(List<JsonNode> nodes) {
		License rda = new License();
		for (JsonNode node: nodes) {
			String rdaProperty = "";
			JsonNode schematics = node.get("schematics");
			if(schematics.isArray()){
				int index = 0;
				for(JsonNode schematic: schematics){
					if(schematic.asText().startsWith("rda.dataset.distribution.license")){
						rdaProperty = schematic.asText();
						((ArrayNode)schematics).remove(index);
						break;
					}
					index++;
				}
			}
			else{
				continue;
			}
			String value = node.get("value").asText();
			if(value == null || value.isEmpty()){
				continue;
			}
			for (LicenceProperties licenceProperties: LicenceProperties.values()) {
				if (rdaProperty.contains(licenceProperties.getName())) {
					switch (licenceProperties) {
						case LICENSE_REF:
							try {
								rda.setLicenseRef(URI.create(value));
							} catch (IllegalArgumentException e) {
								logger.warn(e.getLocalizedMessage() + ". Skipping url parsing");
							}
							break;
						case START_DATE:
							rda.setStartDate(value);
							break;
					}
				}
			}

			/*if (rdaProperty.contains("license_ref")) {
				rda.setLicenseRef(URI.create(value));
				rda.setAdditionalProperty("license_refId", node.get("id").asText());
			} else if (rdaProperty.contains("start_date")) {
				rda.setStartDate(value);
				rda.setAdditionalProperty("start_dateId", node.get("id").asText());
			}*/
		}

		if(rda.getLicenseRef() == null || rda.getStartDate() == null){
			return null;
		}

		return rda;
	}

	public static Map<String, String> toProperties(List<License> rdas) {
		Map<String, String> properties = new HashMap<>();

		rdas.forEach(rda -> {
			rda.getAdditionalProperties().entrySet().forEach(entry -> {
				switch (entry.getKey()) {
					case "license_refId":
						properties.put(entry.getValue().toString(), rda.getLicenseRef().toString());
						break;
					case "start_dateId":
						properties.put(entry.getValue().toString(), rda.getStartDate());
						break;
				}

			});
		});

		return properties;
	}

	public static Map<String, String> toProperties(License rda, JsonNode root) {
		Map<String, String> properties = new HashMap<>();

		List<JsonNode> licenseNodes = JsonSearcher.findNodes(root, "schematics", "rda.dataset.distribution.license");

		for (JsonNode licenseNode: licenseNodes) {
			for (LicenceProperties licenceProperty: LicenceProperties.values()) {
				JsonNode schematics = licenseNode.get("schematics");
				if(schematics.isArray()) {
					for (JsonNode schematic : schematics) {
						if (schematic.asText().endsWith(licenceProperty.getName())) {
							switch (licenceProperty) {
								case LICENSE_REF:
									if (rda.getLicenseRef() != null) {
										properties.put(licenseNode.get("id").asText(), rda.getLicenseRef().toString());
									}
									break;
								case START_DATE:
									properties.put(licenseNode.get("id").asText(), rda.getStartDate());
									break;
							}
						}
						break;
					}
				}
			}
		}

		return properties;
	}

	public enum LicenceProperties {
		LICENSE_REF("license_ref"),
		START_DATE("start_date");

		private String name;

		LicenceProperties(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}
	}
}
