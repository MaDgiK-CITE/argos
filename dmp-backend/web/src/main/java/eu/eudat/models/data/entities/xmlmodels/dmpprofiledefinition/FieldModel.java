package eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition;

import eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition.types.ExtraFieldType;
import eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition.types.FieldCategory;
import eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition.types.SystemFieldType;

import java.util.UUID;

public class FieldModel {

    private UUID id;
    private FieldCategory category;
    private Integer type;
    private String label;
    private String placeholder;
    private String description;
    private Integer ordinal;
    private boolean required;

    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }

    public FieldCategory getCategory() {
        return category;
    }
    public void setCategory(FieldCategory category) {
        this.category = category;
    }

    public Integer getType() {
        return type;
    }
    public void setType(Integer type) {
        this.type = type;
    }

    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }

    public String getPlaceholder() {
        return placeholder;
    }
    public void setPlaceholder(String placeholder) {
        this.placeholder = placeholder;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getOrdinal() {
        return ordinal;
    }
    public void setOrdinal(Integer ordinal) {
        this.ordinal = ordinal;
    }

    public boolean isRequired() {
        return required;
    }
    public void setRequired(boolean required) {
        this.required = required;
    }

    public SystemField toSystemField(){
        SystemField systemField = new SystemField();
        if (this.category == FieldCategory.SYSTEM) {
            systemField.setId(this.id);
            systemField.setType(SystemFieldType.fromInteger(this.type));
            systemField.setLabel(this.label);
            systemField.setPlaceholder(this.placeholder);
            systemField.setDescription(this.description);
            systemField.setOrdinal(this.ordinal);
            systemField.setRequired(this.required);
        }
        return systemField;
    }

    public FieldModel fromSystemField(SystemField systemField){
        this.setId(systemField.getId());
        this.setCategory(FieldCategory.SYSTEM);
        this.setType(systemField.getType().getType());
        this.setLabel(systemField.getLabel());
        this.setPlaceholder(systemField.getPlaceholder());
        this.setDescription(systemField.getDescription());
        this.setOrdinal(systemField.getOrdinal());
        this.setRequired(systemField.isRequired());
        return this;
    }

    public ExtraField toExtraField(){
        ExtraField extraField = new ExtraField();
        if (this.category == FieldCategory.EXTRA) {
            extraField.setId(this.id);
            extraField.setType(ExtraFieldType.fromInteger(this.type));
            extraField.setLabel(this.label);
            extraField.setPlaceholder(this.placeholder);
            extraField.setDescription(this.description);
            extraField.setOrdinal(this.ordinal);
            extraField.setRequired(this.required);
        }
        return extraField;
    }

    public FieldModel fromExtraField(ExtraField extraField){
        this.setId(extraField.getId());
        this.setCategory(FieldCategory.EXTRA);
        this.setType(extraField.getType().getValue());
        this.setLabel(extraField.getLabel());
        this.setPlaceholder(extraField.getPlaceholder());
        this.setDescription(extraField.getDescription());
        this.setOrdinal(extraField.getOrdinal());
        this.setRequired(extraField.getRequired());
        return this;
    }
}
