package eu.eudat.models.data.quickwizard;


import eu.eudat.data.entities.DescriptionTemplate;
import eu.eudat.models.data.datasetprofile.DatasetProfileOverviewModel;
import eu.eudat.models.data.dmp.DataManagementPlan;
import eu.eudat.models.data.datasetwizard.DatasetWizardModel;
import eu.eudat.models.data.user.composite.PagedDatasetProfile;

import java.util.Date;

public class DatasetDescriptionQuickWizardModel extends PagedDatasetProfile {

    private String datasetLabel;

    public String getDatasetLabel() {
        return datasetLabel;
    }

    public void setDatasetLabel(String datasetLabel) {
        this.datasetLabel = datasetLabel;
    }


    public  DatasetWizardModel  toDataModel(DataManagementPlan dmp, DescriptionTemplate profile){
        DatasetWizardModel newDataset = new  DatasetWizardModel();
        newDataset.setLabel(datasetLabel);
        newDataset.setCreated(new Date());
        newDataset.setProfile(new DatasetProfileOverviewModel().fromDataModel(profile));
        newDataset.setDmp(dmp);
        newDataset.setStatus((short) this.getStatus());
        //newDataset.setStatus(Dataset.Status.SAVED.getValue());


        newDataset.setDatasetProfileDefinition((PagedDatasetProfile) this);
        return newDataset;
    }


}
