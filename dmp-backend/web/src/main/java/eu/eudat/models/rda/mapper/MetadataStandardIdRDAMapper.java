package eu.eudat.models.rda.mapper;

import eu.eudat.models.rda.MetadataStandardId;

public class MetadataStandardIdRDAMapper {

	public static MetadataStandardId toRDA(String uri) {
		MetadataStandardId rda = new MetadataStandardId();
		rda.setIdentifier(uri);
		rda.setType(MetadataStandardId.Type.URL);
		return rda;
	}
}
