package eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition.types;

/**
 * Created by ikalyvas on 3/21/2018.
 */
public enum DMPProfileType {
    INPUT(0);

    private Integer value;

    DMPProfileType(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }

    public static DMPProfileType fromInteger(Integer value) {
        switch (value) {
            case 0:
                return INPUT;
            default:
                throw new RuntimeException("Unsupported DMPProfile Type");
        }
    }
}
