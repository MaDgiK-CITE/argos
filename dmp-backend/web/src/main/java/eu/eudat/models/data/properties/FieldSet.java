package eu.eudat.models.data.properties;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FieldSet implements PropertiesGenerator {
    private String id;
    private List<Field> fields;
    private List<FieldSet> multiplicityItems;
    private String commentFieldValue;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCommentFieldValue() {
        return commentFieldValue;
    }

    public void setCommentFieldValue(String commentFieldValue) {
        this.commentFieldValue = commentFieldValue;
    }

    public List<FieldSet> getMultiplicityItems() {
        return multiplicityItems;
    }

    public void setMultiplicityItems(List<FieldSet> multiplicityItems) {
        this.multiplicityItems = multiplicityItems;
    }

    public List<Field> getFields() {
        return fields;
    }

    public void setFields(List<Field> fields) {
        this.fields = fields;
    }

    @Override
    public void toMap(Map<String, Object> fieldValues) {
        fieldValues.put("commentFieldValue" + this.id, this.commentFieldValue);
        this.fields.forEach(item -> item.toMap(fieldValues));
        Map<String, Object> multiplicity = new HashMap<String, Object>();
        if (this.multiplicityItems != null) {
            this.multiplicityItems.forEach(item -> item.toMap(fieldValues, this.multiplicityItems.indexOf(item)));
        }
        //fieldValues.put(this.id,multiplicity);
    }

    @Override
    public void toMap(Map<String, Object> fieldValues, int index) {
        this.fields.forEach(item -> item.toMap(fieldValues, index));
        //this.multiplicityItems.forEach(item->item.toMap(fieldValues,index));
    }

}
