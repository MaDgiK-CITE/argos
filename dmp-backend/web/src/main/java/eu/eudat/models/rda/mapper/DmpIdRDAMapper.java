package eu.eudat.models.rda.mapper;

import eu.eudat.models.rda.DmpId;

import java.util.UUID;

public class DmpIdRDAMapper {

	public static DmpId toRDA(Object id) {
		DmpId rda = new DmpId();
		rda.setIdentifier(id.toString());
		if (id instanceof UUID) {
			rda.setType(DmpId.Type.OTHER);
		} else {
			rda.setType(DmpId.Type.DOI);
		}
		return rda;
	}
}
