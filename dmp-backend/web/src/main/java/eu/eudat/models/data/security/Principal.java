package eu.eudat.models.data.security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import eu.eudat.types.Authorities;

import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;


public class Principal {
    private UUID id;
    private UUID token;
    private String name;
    private String email;
    private Date expiresAt;
    private String avatarUrl;
    private Set<Authorities> authorities;
    private String culture;
    private String language;
    private String timezone;
    private String zenodoToken;
    private Instant zenodoDuration;
    private String zenodoRefresh;
    private String zenodoEmail;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getToken() {
        return token;
    }

    public void setToken(UUID token) {
        this.token = token;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getExpiresAt() {
        return expiresAt;
    }

    public void setExpiresAt(Date expiresAt) {
        this.expiresAt = expiresAt;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public Set<Integer> getAuthorities() {
        return authorities.stream().map(authz -> authz.getValue()).collect(Collectors.toSet());
    }

    public String getCulture() {
        return culture;
    }

    public void setCulture(String culture) {
        this.culture = culture;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getZenodoToken() {
        return zenodoToken;
    }

    public void setZenodoToken(String zenodoToken) {
        this.zenodoToken = zenodoToken;
    }

    public Instant getZenodoDuration() {
        return zenodoDuration;
    }

    public void setZenodoDuration(Instant zenodoDuration) {
        this.zenodoDuration = zenodoDuration;
    }

    public String getZenodoRefresh() {
        return zenodoRefresh;
    }

    public void setZenodoRefresh(String zenodoRefresh) {
        this.zenodoRefresh = zenodoRefresh;
    }

    public String getZenodoEmail() {
        return zenodoEmail;
    }

    public void setZenodoEmail(String zenodoEmail) {
        this.zenodoEmail = zenodoEmail;
    }

    @JsonIgnore
    public Set<Authorities> getAuthz() {
        return this.authorities;
    }

    public void setAuthorities(Set<Authorities> authorities) {
        this.authorities = authorities;
    }

    public boolean isAuthorized(List<Authorities> requiredAuthorities) {
        if (!Collections.disjoint(this.authorities, requiredAuthorities) || requiredAuthorities.size() == 0)
            return true;
        else return false;
    }
}
