package eu.eudat.models.data.helpers.common;

import eu.eudat.data.dao.criteria.Criteria;
import eu.eudat.models.data.components.commons.datafield.AutoCompleteData;

import java.util.List;

public class AutoCompleteOptionsLookupItem extends Criteria<AutoCompleteOptionsLookupItem> {

    private List<AutoCompleteData.AutoCompleteSingleData> autoCompleteSingleDataList;

    public List<AutoCompleteData.AutoCompleteSingleData> getAutoCompleteSingleDataList() {
        return autoCompleteSingleDataList;
    }
    public void setAutoCompleteSingleDataList(List<AutoCompleteData.AutoCompleteSingleData> autoCompleteSingleDataList) {
        this.autoCompleteSingleDataList = autoCompleteSingleDataList;
    }

}
