package eu.eudat.models.data.datasetImport;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "root")
public class DatasetImportPagedDatasetProfile {
    private String datasetProfileId;
    private List<DatasetImportPage> pages;
    private List<DatasetImportRule> rules;
    private int status;

    @XmlElement(name = "datasetProfileId")
    public String getDatasetProfileId() {
        return datasetProfileId;
    }
    public void setDatasetProfileId(String datasetProfileId) {
        this.datasetProfileId = datasetProfileId;
    }

    @XmlElementWrapper(name="pages")
    @XmlElement(name = "page")
    public List<DatasetImportPage> getPages() { return pages; }
    public void setPages(List<DatasetImportPage> pages) { this.pages = pages; }

    public List<DatasetImportRule> getRules() { return rules; }
    public void setRules(List<DatasetImportRule> rules) { this.rules = rules; }

    public int getStatus() { return status; }
    public void setStatus(int status) { this.status = status; }
}
