package eu.eudat.models.data.datasetwizard;

import java.util.List;
import java.util.UUID;

public class DatasetsToBeFinalized {
    private List<UUID> uuids;

    public List<UUID> getUuids() {
        return uuids;
    }
    public void setUuids(List<UUID> uuids) {
        this.uuids = uuids;
    }
}
