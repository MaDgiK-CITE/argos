package eu.eudat.models.data.userinfo;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.eudat.data.entities.*;
import eu.eudat.data.entities.UserInfo;
import eu.eudat.models.DataModel;
import eu.eudat.models.data.dmp.DataManagementPlan;
import eu.eudat.models.data.dmp.Organisation;
import net.minidev.json.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by ikalyvas on 8/24/2018.
 */
public class UserProfile implements DataModel<eu.eudat.data.entities.UserInfo, UserProfile> {
    private static final Logger logger = LoggerFactory.getLogger(UserProfile.class);

    private UUID id;
    private String email;
    private Short usertype;
    private String name;
    private Date lastloggedin;
    //private String additionalinfo;
    private List<DataManagementPlan> associatedDmps;
    private String zenodoEmail;
    private Map<String, Object> language;
    private String timezone;
    private Map<String, Object> culture;
    private String avatarUrl;
    private Organisation organization;
    private String roleOrganization;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Short getUsertype() {
        return usertype;
    }

    public void setUsertype(Short usertype) {
        this.usertype = usertype;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getLastloggedin() {
        return lastloggedin;
    }

    public void setLastloggedin(Date lastloggedin) {
        this.lastloggedin = lastloggedin;
    }

    /*public String getAdditionalinfo() {
        return additionalinfo;
    }

    public void setAdditionalinfo(String additionalinfo) {
        this.additionalinfo = additionalinfo;
    }*/

    public List<DataManagementPlan> getAssociatedDmps() {
        return associatedDmps;
    }

    public void setAssociatedDmps(List<DataManagementPlan> associatedDmps) {
        this.associatedDmps = associatedDmps;
    }

    public String getZenodoEmail() {
        return zenodoEmail;
    }

    public void setZenodoEmail(String zenodoEmail) {
        this.zenodoEmail = zenodoEmail;
    }

    public Map<String, Object> getLanguage() {
        return language;
    }

    public void setLanguage(Map<String, Object> language) {
        this.language = language;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public Map<String, Object> getCulture() {
        return culture;
    }

    public void setCulture(Map<String, Object> culture) {
        this.culture = culture;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }
    
    

    public Organisation getOrganization() {
		return organization;
	}

	public void setOrganization(Organisation organization) {
		this.organization = organization;
	}

	public String getRoleOrganization() {
		return roleOrganization;
	}

	public void setRoleOrganization(String roleOrganization) {
		this.roleOrganization = roleOrganization;
	}

	@Override
    public UserProfile fromDataModel(UserInfo entity) {
        this.id = entity.getId();
        this.email = entity.getEmail();
        this.usertype = entity.getUsertype();
        this.name = entity.getName();
        this.lastloggedin = entity.getLastloggedin();
        //this.additionalinfo = entity.getAdditionalinfo();
        try {
            Map<String, Object> additionalInfo = new ObjectMapper().readValue(entity.getAdditionalinfo(), HashMap.class);
            this.language = (Map)additionalInfo.get("language");
            this.culture = (Map) additionalInfo.get("culture");
            this.timezone = (String) additionalInfo.get("timezone");
            this.zenodoEmail = (String) additionalInfo.get("zenodoEmail");
            this.avatarUrl = (String) additionalInfo.get("avatarUrl");
            this.organization = Organisation.fromMap((HashMap<String, Object>) additionalInfo.get("organization"));
            this.roleOrganization = (String) additionalInfo.get("roleOrganization");
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
        return this;
    }

    @Override
    public UserInfo toDataModel() throws Exception {
        return null;
    }

    @Override
    public String getHint() {
        return null;
    }
}
