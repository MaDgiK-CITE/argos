package eu.eudat.models.rda.mapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.eudat.data.entities.*;
import eu.eudat.logic.managers.DataManagementProfileManager;
import eu.eudat.logic.services.ApiContext;
import eu.eudat.logic.utilities.helpers.StreamDistinctBy;
import eu.eudat.models.data.dmp.AssociatedProfile;
import eu.eudat.models.rda.Dmp;
import eu.eudat.models.rda.DmpId;
import net.minidev.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.NoResultException;
import javax.transaction.Transactional;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class DmpRDAMapper {

	private static final Logger logger = LoggerFactory.getLogger(DmpRDAMapper.class);

	private DatasetRDAMapper datasetRDAMapper;
	private ApiContext apiContext;
	private DataManagementProfileManager dataManagementProfileManager;

	@Autowired
	public DmpRDAMapper(DatasetRDAMapper datasetRDAMapper, ApiContext apiContext, DataManagementProfileManager dataManagementProfileManager) throws IOException {
		this.datasetRDAMapper = datasetRDAMapper;
		this.apiContext = apiContext;
		this.dataManagementProfileManager = dataManagementProfileManager;
	}

	@Transactional
	public Dmp toRDA(DMP dmp) {
		if (dmp.getDataset() == null || dmp.getDataset().isEmpty()) {
			throw new IllegalArgumentException("DMP has no Datasets");
		}
		Map<String, Object> extraProperties;
		if (dmp.getExtraProperties() == null) {
			throw new IllegalArgumentException("DMP is missing language and contact properties");
		} else {
			extraProperties = new org.json.JSONObject(dmp.getExtraProperties()).toMap();
			/*if (extraProperties.get("language") == null) {
				throw new IllegalArgumentException("DMP must have it's language property defined");
			}*/
			if (extraProperties.get("contact") == null) {
				throw new IllegalArgumentException("DMP must have it's contact property defined");
			}
		}
		Dmp rda = new Dmp();
		if (dmp.getDois() != null && !dmp.getDois().isEmpty()) {
			for(EntityDoi doi: dmp.getDois()){
				if(doi.getRepositoryId().equals("Zenodo")){
					rda.setDmpId(DmpIdRDAMapper.toRDA(doi.getDoi()));
				}
			}
		} else {
			rda.setDmpId(DmpIdRDAMapper.toRDA(dmp.getId()));
		}
		if (dmp.getCreated() == null) {
			throw new IllegalArgumentException("DMP Created is missing");
		}
		if (dmp.getModified() == null) {
			throw new IllegalArgumentException("DMP Modified is missing");
		}
		if (dmp.getLabel() == null) {
			throw new IllegalArgumentException("DMP Label is missing");
		}
		rda.setCreated(dmp.getCreated());
		rda.setDescription(dmp.getDescription());
		rda.setModified(dmp.getModified());
		rda.setTitle(dmp.getLabel());

		if (!extraProperties.isEmpty()) {
			if (extraProperties.get("ethicalIssues") != null) {
				rda.setEthicalIssuesExist(Dmp.EthicalIssuesExist.fromValue(extraProperties.get("ethicalIssues").toString()));
			} else {
				rda.setEthicalIssuesExist(Dmp.EthicalIssuesExist.UNKNOWN);
			}
			rda.setLanguage(LanguageRDAMapper.mapLanguageIsoToRDAIso(extraProperties.get("language") != null ? extraProperties.get("language").toString() : "en"));
			if (extraProperties.get("costs") != null) {
				rda.setCost(new ArrayList<>());
				((List) extraProperties.get("costs")).forEach(costl -> {
					rda.getCost().add(CostRDAMapper.toRDA((Map)costl));
				});
			}
			UserInfo contactDb = apiContext.getOperationsContext().getDatabaseRepository().getUserInfoDao().find(UUID.fromString((String) extraProperties.get("contact")));
			UserInfo contact = new UserInfo();
			contact.setId(contactDb.getId());
			contact.setName(contactDb.getName());
			contact.setEmail(contactDb.getEmail());
			if(contact.getEmail() == null){
				for(UserDMP userDMP: dmp.getUsers()){
					if(userDMP.getDmp().getId() == dmp.getId() && userDMP.getUser().getEmail() != null){
						contact.setEmail(userDMP.getUser().getEmail());
						break;
					}
				}
			}
			rda.setContact(ContactRDAMapper.toRDA(contact));
		}

		/*UserInfo creator;
		if (dmp.getCreator() != null) {
			creator = dmp.getCreator();
		} else {
			creator = dmp.getUsers().stream().filter(userDMP -> userDMP.getRole().equals(UserDMP.UserDMPRoles.OWNER.getValue())).map(UserDMP::getUser).findFirst().orElse(new UserInfo());
		}
		rda.setContact(ContactRDAMapper.toRDA(creator));*/
		rda.setContributor(new ArrayList<>());
		if (dmp.getResearchers() != null && !dmp.getResearchers().isEmpty()) {
			rda.getContributor().addAll(dmp.getResearchers().stream().map(ContributorRDAMapper::toRDA).collect(Collectors.toList()));
		}
//		rda.getContributor().addAll(dmp.getUsers().stream().map(ContributorRDAMapper::toRDA).collect(Collectors.toList()));
		rda.setDataset(dmp.getDataset().stream().filter(dataset -> dataset.getStatus() != eu.eudat.elastic.entities.Dmp.DMPStatus.DELETED.getValue()).map(dataset -> datasetRDAMapper.toRDA(dataset, rda)).collect(Collectors.toList()));
		rda.setProject(Collections.singletonList(ProjectRDAMapper.toRDA(dmp.getProject(), dmp.getGrant())));

		rda.setAdditionalProperty("templates", dmp.getAssociatedDmps().stream().map(item -> item.getDatasetprofile().getId().toString()).toArray());
		rda.setAdditionalProperty("blueprintId", dmp.getProfile().getId());
		rda.setAdditionalProperty("license", extraProperties.get("license"));
		rda.setAdditionalProperty("visible", extraProperties.get("visible"));
		rda.setAdditionalProperty("publicDate", extraProperties.get("publicDate"));
		rda.setAdditionalProperty("contact", extraProperties.get("contact"));
		rda.setAdditionalProperty("dmpProperties", dmp.getProperties());
		return rda;
	}

	public DMP toEntity(Dmp rda, String[] profiles) {
		DMP entity = new DMP();
		entity.setLabel(rda.getTitle());
		if (rda.getDmpId().getType() == DmpId.Type.DOI) {
			try {
				EntityDoi doi = apiContext.getOperationsContext().getDatabaseRepository().getEntityDoiDao().findFromDoi(rda.getDmpId().getIdentifier());
				Set<EntityDoi> dois = new HashSet<>();
				dois.add(doi);
				entity.setDois(dois);
			}
			catch (NoResultException e) {
				logger.warn("No entity doi: " + rda.getDmpId().getIdentifier() + " found in database. No dois are added to dmp.");
				entity.setDois(new HashSet<>());
			}
		}

		String blueprintId = (String) rda.getAdditionalProperties().get("blueprintId");
			DMPProfile blueprint = apiContext.getOperationsContext().getDatabaseRepository().getDmpProfileDao().find(UUID.fromString(blueprintId));
			entity.setProfile(blueprint);

		if (((List<String>) rda.getAdditionalProperties().get("templates")) != null && !((List<String>) rda.getAdditionalProperties().get("templates")).isEmpty()) {
			List<DescriptionTemplate> descriptionTemplates = ((List<String>) rda.getAdditionalProperties().get("templates")).stream().map(this::getProfile).filter(Objects::nonNull).collect(Collectors.toList());
			Set<DMPDatasetProfile> dmpDatasetProfiles = new HashSet<>();
			for (DescriptionTemplate profile : descriptionTemplates) {
				DMPDatasetProfile dmpDatasetProfile = new DMPDatasetProfile();
				dmpDatasetProfile.setDmp(entity);
				dmpDatasetProfile.setDatasetprofile(profile);
				String indexes = this.dataManagementProfileManager.sectionIndexesForDescriptionTemplate(blueprint, profile.getId()).stream()
						.map(String::valueOf) // convert each int to a string
						.collect(Collectors.joining(","));
				dmpDatasetProfile.setData("{\"dmpSectionIndex\":[" +  indexes + "]}");
				dmpDatasetProfiles.add(dmpDatasetProfile);
			}
			entity.setAssociatedDmps(dmpDatasetProfiles);
		}

//		if (entity.getAssociatedDmps() == null) {
//			entity.setAssociatedDmps(new HashSet<>());
//		}
//		if (profiles != null && entity.getId() != null) {
//			for (String profile : profiles) {
//				entity.getAssociatedDmps().add(this.getProfile(profile, entity.getId()));
//			}
//		}
		if (rda.getContributor() != null && !rda.getContributor().isEmpty() && rda.getContributor().get(0).getContributorId() != null) {
			entity.setResearchers(rda.getContributor().stream().filter(r -> r.getContributorId() != null).map(ContributorRDAMapper::toEntity).filter(StreamDistinctBy.distinctByKey(Researcher::getReference)).collect(Collectors.toSet()));
		}
		entity.setCreated(rda.getCreated());
		entity.setModified(rda.getModified());
		entity.setDescription(rda.getDescription());
		if(entity.getAssociatedDmps().size() > 0) {
			DescriptionTemplate defaultProfile = entity.getAssociatedDmps().stream().findFirst().get().getDatasetprofile();
			entity.setDataset(rda.getDataset().stream().map(rda1 -> datasetRDAMapper.toEntity(rda1, defaultProfile)).collect(Collectors.toSet()));
		}
        if (rda.getProject() != null && rda.getProject().size() > 0) {
			Map<String, Object> result = ProjectRDAMapper.toEntity(rda.getProject().get(0), apiContext);
			entity.setProject((Project) result.get("project"));
			result.entrySet().stream().filter(entry -> entry.getKey().startsWith("grant")).forEach(entry -> entity.setGrant((Grant) entry.getValue()));
		}

		Map<String, Object> extraProperties = new HashMap<>();
		extraProperties.put("language", LanguageRDAMapper.mapRDAIsoToLanguageIso(rda.getLanguage()));
		if (rda.getAdditionalProperties().get("license") != null) extraProperties.put("license", rda.getAdditionalProperties().get("license"));
		if (rda.getAdditionalProperties().get("visible") != null) extraProperties.put("visible", rda.getAdditionalProperties().get("visible"));
		if (rda.getAdditionalProperties().get("publicDate") != null) extraProperties.put("publicDate", rda.getAdditionalProperties().get("publicDate"));
		if (rda.getAdditionalProperties().get("contact") != null) extraProperties.put("contact", rda.getAdditionalProperties().get("contact"));
		entity.setExtraProperties(JSONObject.toJSONString(extraProperties));

		if (rda.getAdditionalProperties().get("dmpProperties") != null) entity.setProperties(rda.getAdditionalProperties().get("dmpProperties").toString());


		return entity;
	}

	private DescriptionTemplate getProfile(String id) {
		return  apiContext.getOperationsContext().getDatabaseRepository().getDatasetProfileDao().asQueryable().where(((builder, root) -> builder.equal(root.get("id"), UUID.fromString(id)))).getSingleOrDefault();
	}
}
