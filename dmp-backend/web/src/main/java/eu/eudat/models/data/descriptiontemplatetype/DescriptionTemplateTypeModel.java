package eu.eudat.models.data.descriptiontemplatetype;

import eu.eudat.data.entities.DescriptionTemplateType;
import eu.eudat.models.DataModel;

import java.util.UUID;

public class DescriptionTemplateTypeModel  implements DataModel<DescriptionTemplateType, DescriptionTemplateTypeModel> {

    private UUID id;
    private String name;
    private short status;

    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public short getStatus() {
        return status;
    }
    public void setStatus(short status) {
        this.status = status;
    }

    @Override
    public DescriptionTemplateTypeModel fromDataModel(DescriptionTemplateType entity) {
        this.id = entity.getId();
        this.name = entity.getName();
        this.status = entity.getStatus();
        return this;
    }

    @Override
    public DescriptionTemplateType toDataModel() throws Exception {
        DescriptionTemplateType descriptionTemplateType = new DescriptionTemplateType();
        descriptionTemplateType.setId(this.id);
        descriptionTemplateType.setName(this.name);
        descriptionTemplateType.setStatus(this.status);
        return descriptionTemplateType;
    }

    @Override
    public String getHint() {
        return null;
    }
}
