package eu.eudat.models.data.userinfo;

import eu.eudat.models.DataModel;

import java.util.Date;
import java.util.UUID;

public class UserInfo implements DataModel<eu.eudat.data.entities.UserInfo, UserInfo> {
    private UUID id;

    private String email = null;

    private Short authorization_level; //0 admin, 1 user

    private Short usertype; // 0 internal, 1 external

    private Boolean verified_email = null;

    private String name = null;

    private Date created = null;

    private Date lastloggedin = null;

  //  private String additionalinfo;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Short getAuthorization_level() {
        return authorization_level;
    }

    public void setAuthorization_level(Short authorization_level) {
        this.authorization_level = authorization_level;
    }

    public Short getUsertype() {
        return usertype;
    }

    public void setUsertype(Short usertype) {
        this.usertype = usertype;
    }

    public Boolean getVerified_email() {
        return verified_email;
    }

    public void setVerified_email(Boolean verified_email) {
        this.verified_email = verified_email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastloggedin() {
        return lastloggedin;
    }

    public void setLastloggedin(Date lastloggedin) {
        this.lastloggedin = lastloggedin;
    }

    /*public String getAdditionalinfo() {
        return additionalinfo;
    }

    public void setAdditionalinfo(String additionalinfo) {
        this.additionalinfo = additionalinfo;
    }*/

    @Override
    public UserInfo fromDataModel(eu.eudat.data.entities.UserInfo entity) {
        this.name = entity.getName();
        this.email = entity.getEmail();
        this.usertype = entity.getUsertype();
        this.id = entity.getId();
        return this;
    }

    @Override
    public eu.eudat.data.entities.UserInfo toDataModel() {
        eu.eudat.data.entities.UserInfo entity = new eu.eudat.data.entities.UserInfo();
        entity.setId(this.getId());
        entity.setEmail(this.getEmail());
        entity.setName(this.getName());
        //entity.setAdditionalinfo(this.getAdditionalinfo());
        entity.setAuthorization_level(this.getAuthorization_level());
        entity.setCreated(this.getCreated());
        entity.setLastloggedin(this.getLastloggedin());
        entity.setUsertype(this.getUsertype());
        entity.setVerified_email(this.getVerified_email());
        return entity;
    }

    @Override
    public String getHint() {
        return null;
    }
}
