package eu.eudat.models.data.doi;

public class DepositRequest {

    private String repositoryId;
    private String dmpId;
    private String accessToken;

    public String getRepositoryId() {
        return repositoryId;
    }
    public void setRepositoryId(String repositoryId) {
        this.repositoryId = repositoryId;
    }

    public String getDmpId() {
        return dmpId;
    }
    public void setDmpId(String dmpId) {
        this.dmpId = dmpId;
    }

    public String getAccessToken() {
        return accessToken;
    }
    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
