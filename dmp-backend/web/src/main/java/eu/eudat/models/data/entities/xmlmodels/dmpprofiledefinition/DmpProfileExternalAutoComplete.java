package eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition;

import eu.eudat.logic.utilities.interfaces.XmlSerializable;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class DmpProfileExternalAutoComplete implements XmlSerializable<DmpProfileExternalAutoComplete> {
	private String url;
	private String optionsRoot;
	private Boolean multiAutoComplete;
	private String label;
	private String value;

	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}

	public String getOptionsRoot() {
		return optionsRoot;
	}
	public void setOptionsRoot(String optionsRoot) {
		this.optionsRoot = optionsRoot;
	}

	public Boolean getMultiAutoComplete() { return multiAutoComplete; }
	public void setMultiAutoComplete(Boolean multiAutoComplete) { this.multiAutoComplete = multiAutoComplete; }

	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}

	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public Element toXml(Document doc) {
		Element root = doc.createElement("externalAutocomplete");
		root.setAttribute("url", this.url);
		root.setAttribute("optionsRoot", this.optionsRoot);
		if (this.multiAutoComplete == null) this.multiAutoComplete = false;
		root.setAttribute("multiAutoComplete", this.multiAutoComplete.toString());
		root.setAttribute("label", this.label);
		root.setAttribute("value", this.value);

		return root;
	}

	@Override
	public DmpProfileExternalAutoComplete fromXml(Element item) {
		this.url = item.getAttribute("url");
		this.optionsRoot = item.getAttribute("optionsRoot");
		this.multiAutoComplete = Boolean.valueOf(item.getAttribute("multiAutoComplete"));
		this.label = item.getAttribute("label");
		this.value = item.getAttribute("value");

		return this;
	}
}
