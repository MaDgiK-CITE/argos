package eu.eudat.models.data.external;

import java.util.List;
import java.util.Map;


public interface ExternalItem<T> {
    T fromExternalItem(List<Map<String, String>> values);
}
