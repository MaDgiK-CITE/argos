package eu.eudat.models.data.user.components.commons;

public class Rule {
    private String sourceField;
    private String targetField;
    private String requiredValue;
    private String type;

    public String getSourceField() {
        return sourceField;
    }

    public void setSourceField(String sourceField) {
        this.sourceField = sourceField;
    }

    public String getTargetField() {
        return targetField;
    }

    public void setTargetField(String targetField) {
        this.targetField = targetField;
    }

    public String getRequiredValue() {
        return requiredValue;
    }

    public void setRequiredValue(String requiredValue) {
        this.requiredValue = requiredValue;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Rule fromDefinitionRule(eu.eudat.models.data.components.commons.Rule rule) {
        this.targetField = rule.getTarget();
        this.requiredValue = rule.getValue();
        this.type = rule.getRuleStyle();
        return this;
    }

}
