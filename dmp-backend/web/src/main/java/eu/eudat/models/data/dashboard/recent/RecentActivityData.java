package eu.eudat.models.data.dashboard.recent;

import java.util.Date;

/**
 * Created by ikalyvas on 3/14/2018.
 */
public class RecentActivityData {

    private String label;
    private String id;
    private Date timestamp;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

}
