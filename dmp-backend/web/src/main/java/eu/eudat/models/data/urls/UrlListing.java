package eu.eudat.models.data.urls;
import eu.eudat.models.DataModel;
import eu.eudat.queryable.queryableentity.DataEntity;

/**
 * Created by ikalyvas on 3/19/2018.
 */
public abstract class UrlListing<T extends DataEntity,M extends DataModel> implements DataModel<T,M>{
    private String label;
    private String url;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
