package eu.eudat.models.data.externaldataset;

import eu.eudat.data.entities.ExternalDataset;
import eu.eudat.data.entities.UserInfo;
import eu.eudat.models.DataModel;

import java.util.Date;
import java.util.UUID;

public class ExternalDatasetModel implements DataModel<ExternalDataset, ExternalDatasetModel> {
    public UUID id;
    public String name;
    public String abbreviation;
    public Date created;
    public Date modified;
    private String source;

    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getAbbreviation() {
        return abbreviation;
    }
    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public Date getCreated() {
        return created;
    }
    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }
    public void setModified(Date modified) {
        this.modified = modified;
    }

    public String getSource() {
        return source;
    }
    public void setSource(String source) {
        this.source = source;
    }

    @Override
    public ExternalDatasetModel fromDataModel(ExternalDataset entity) {
        this.id = entity.getId();
        this.name = entity.getLabel();
        this.abbreviation = entity.getAbbreviation();
        this.created = entity.getCreated();
        this.modified = new Date();
        String source1 = entity.getReference().substring(0, entity.getReference().indexOf(":"));
        if (source1.equals("dmp")) {
            this.source = "Internal";
        } else {
            this.source = source1;
        }
        return this;
    }

    @Override
    public ExternalDataset toDataModel() throws Exception {
        ExternalDataset externalDataset = new ExternalDataset();
        externalDataset.setAbbreviation(this.abbreviation);
        externalDataset.setCreated(this.created != null ? this.created : new Date());
        externalDataset.setModified(new Date());
        externalDataset.setId(this.id != null ? this.id : UUID.randomUUID());
        externalDataset.setLabel(this.name);
        externalDataset.setReference("dmp:" + externalDataset.getId());
        externalDataset.setCreationUser(new UserInfo());
        return externalDataset;
    }

    @Override
    public String getHint() {
        return null;
    }
}
