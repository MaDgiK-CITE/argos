package eu.eudat.models.data.external;

import java.util.List;
import java.util.Map;


public class RegistriesExternalSourcesModel extends ExternalListingItem<RegistriesExternalSourcesModel> {
    @Override
    public RegistriesExternalSourcesModel fromExternalItem(List<Map<String, String>> values) {
        for (Map<String, String> item : values) {
            ExternalSourcesItemModel model = new ExternalSourcesItemModel();
            model.setId((String)item.get("pid"));
            model.setUri((String)item.get("uri"));
            model.setName((String)item.get("name"));
            model.setTag((String)item.get("tag"));
            this.add(model);
        }
        return this;
    }
}
