package eu.eudat.models.data.dmp;

import eu.eudat.models.data.dmp.adpter.PageAdapter;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@XmlRootElement(name = "dataset")
public class DatasetImportModels {

    private String name;
    private UUID profile;
    private int dmpSectionIndex;
    private HashMap<String, String> fieldImportModels;

    @XmlAttribute(name ="name")
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @XmlElement(name = "profile-id")
    public UUID getProfile() {
        return profile;
    }
    public void setProfile(UUID profile) {
        this.profile = profile;
    }

    @XmlElement(name = "dmpSectionIndex")
    public int getDmpSectionIndex() {
        return dmpSectionIndex;
    }
    public void setDmpSectionIndex(int dmpSectionIndex) {
        this.dmpSectionIndex = dmpSectionIndex;
    }

    @XmlJavaTypeAdapter(PageAdapter.class)
    @XmlElement(name = "pages")
    public HashMap<String, String> getFieldImportModels() {
        return fieldImportModels;
    }

    public void setFieldImportModels(HashMap<String, String> fieldImportModels) {
        this.fieldImportModels = fieldImportModels;
    }
}
