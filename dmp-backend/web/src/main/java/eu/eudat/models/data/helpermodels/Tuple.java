package eu.eudat.models.data.helpermodels;

/**
 * Created by ikalyvas on 3/23/2018.
 */
public class Tuple<L, R> {
    private L id;
    private R label;

    public Tuple() {
    }

    public Tuple(L id, R label) {
        this.id = id;
        this.label = label;
    }

    public L getId() {
        return id;
    }

    public void setId(L id) {
        this.id = id;
    }

    public R getLabel() {
        return label;
    }

    public void setLabel(R label) {
        this.label = label;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tuple<?, ?> tuple = (Tuple<?, ?>) o;

        if (id != null ? !id.equals(tuple.getId()) : tuple.id != null) return false;
        return label != null ? label.equals(tuple.label) : tuple.label == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (label != null ? label.hashCode() : 0);
        return result;
    }
}
