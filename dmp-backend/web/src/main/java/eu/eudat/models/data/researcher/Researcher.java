package eu.eudat.models.data.researcher;

import eu.eudat.models.DataModel;

import java.util.Date;

/**
 * Created by ikalyvas on 2/5/2018.
 */
public class Researcher implements DataModel<eu.eudat.data.entities.Researcher, Researcher> {

    private String firstName;

    private String lastName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public Researcher fromDataModel(eu.eudat.data.entities.Researcher entity) {
        return null;
    }

    @Override
    public eu.eudat.data.entities.Researcher toDataModel() throws Exception {
        eu.eudat.data.entities.Researcher researcher = new eu.eudat.data.entities.Researcher();
        researcher.setCreated(new Date());
        researcher.setLabel(this.firstName + " " + this.lastName);
        researcher.setModified(new Date());
        researcher.setReference("dmp:" + researcher.getReference());
        researcher.setStatus((short) 0);
        return researcher;
    }

    @Override
    public String getHint() {
        return null;
    }
}
