package eu.eudat.models.data.components.commons.datafield;

import eu.eudat.logic.utilities.interfaces.XmlSerializable;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.util.Map;

public abstract class FieldData<T> implements XmlSerializable<T> {
    private String label;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public T fromData(Object data) {
        return null;
    }

    public Object toData() {
        return null;
    }

    public Element toXml(Document doc) {
        return null;
    }

    public T fromXml(Element item) {
        return null;
    }

    public abstract Map<String, Object> toMap(Element item);
}
