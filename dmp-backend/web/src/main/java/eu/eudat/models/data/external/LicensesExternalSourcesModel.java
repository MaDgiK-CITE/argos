package eu.eudat.models.data.external;

import java.util.List;
import java.util.Map;


public class LicensesExternalSourcesModel extends ExternalListingItem<LicensesExternalSourcesModel> {
    @Override
    public LicensesExternalSourcesModel fromExternalItem(List<Map<String, String>> values) {
        for (Map<String, String> item : values) {
            ExternalSourcesItemModel model = new ExternalSourcesItemModel();
            model.setId((String)item.get("id"));
            model.setUri((String)item.get("uri"));
            model.setName((String)item.get("name"));
            this.add(model);
        }
        return this;
    }
}
