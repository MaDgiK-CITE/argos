package eu.eudat.models.data.components.commons.datafield;

import eu.eudat.logic.utilities.interfaces.XmlSerializable;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


public class UploadData extends FieldData<UploadData> {
    public class Option implements XmlSerializable<UploadData.Option> {
        private String label;
        private String value;

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        @Override
        public Element toXml(Document doc) {
            Element option = doc.createElement("option");
            option.setAttribute("label", this.label);
            option.setAttribute("value", this.value);
            return option;
        }

        @Override
        public UploadData.Option fromXml(Element item) {
            this.label = item.getAttribute("label");
            this.value = item.getAttribute("value");
            return this;
        }


    }

    private List<UploadData.Option> types;

    public List<UploadData.Option> getTypes() {
        return types;
    }

    public void setTypes(List<UploadData.Option> types) {
        this.types = types;
    }

    private Integer maxFileSizeInMB;

    public Integer getMaxFileSizeInMB() {
        return maxFileSizeInMB;
    }

    public void setMaxFileSizeInMB(Integer maxFileSizeInMB) {
        this.maxFileSizeInMB = maxFileSizeInMB;
    }

    @Override
    public UploadData fromData(Object data) {
        this.types = new LinkedList();
        if (data != null) {
            List<Map<String, String>> types = ((Map<String, List<Map<String, String>>>) data).get("types");
            for (Map<String, String> map : types) {
                UploadData.Option newOption = new UploadData.Option();
                newOption.setLabel(map.get("label"));
                newOption.setValue(map.get("value"));
                this.types.add(newOption);
            }
            this.setLabel(((Map<String, String>) data).get("label"));
            Object maxFileSizeInMB = ((Map<String, Object>) data).get("maxFileSizeInMB");
            if(maxFileSizeInMB instanceof String){ // template export
                if(!((String)maxFileSizeInMB).isEmpty())
                    this.setMaxFileSizeInMB(Integer.valueOf((String)maxFileSizeInMB));
            }
            else if(maxFileSizeInMB instanceof Integer){ // template preview
                this.setMaxFileSizeInMB((Integer)maxFileSizeInMB);
            }
        }
        return this;
    }

    @Override
    public Object toData() {
        return null;
    }

    @Override
    public Element toXml(Document doc) {
        Element root = doc.createElement("data");
        Element element = doc.createElement("types");
        for (UploadData.Option type : this.types) {
            element.appendChild(type.toXml(doc));
        }
        root.setAttribute("label", this.getLabel());
        root.setAttribute("maxFileSizeInMB", this.getMaxFileSizeInMB().toString());
        root.appendChild(element);
        return root;
    }

    @Override
    public UploadData fromXml(Element item) {
        this.types = new LinkedList<>();
        Element optionsElement = (Element) item.getElementsByTagName("types").item(0);
        this.setLabel(item.getAttribute("label"));
        if(item.getAttribute("maxFileSizeInMB") != null) {
            this.setMaxFileSizeInMB(Integer.parseInt(item.getAttribute("maxFileSizeInMB")));
        }
        if (optionsElement != null) {
            NodeList optionElements = optionsElement.getChildNodes();
            for (int temp = 0; temp < optionElements.getLength(); temp++) {
                Node optionElement = optionElements.item(temp);
                if (optionElement.getNodeType() == Node.ELEMENT_NODE) {
                    this.types.add(new UploadData.Option().fromXml((Element) optionElement));
                }
            }
        }
        return this;
    }

    @Override
    public Map<String, Object> toMap(Element item) {
        HashMap dataMap = new HashMap();
        dataMap.put("label", item != null ? item.getAttribute("label") : "");
        dataMap.put("maxFileSizeInMB", item != null ? item.getAttribute("maxFileSizeInMB") : "");
        Element optionsElement = (Element) item.getElementsByTagName("types").item(0);
        List<Map<String,String>> type =new LinkedList<>();

        if (optionsElement != null) {
            NodeList optionElements = optionsElement.getChildNodes();
            for (int temp = 0; temp < optionElements.getLength(); temp++) {
                Node optionElement = optionElements.item(temp);
                if (optionElement.getNodeType() == Node.ELEMENT_NODE) {
                    type.add(optionToMap((Element) optionElement));
                }
            }
        }

        dataMap.put("types", type != null ? type : null);
        return dataMap;
    }

    private Map<String, String> optionToMap(Element item){
        HashMap dataMap = new HashMap();
        dataMap.put("label",item.getAttribute("label"));
        dataMap.put("value",item.getAttribute("value"));

        return dataMap;
    }
}
