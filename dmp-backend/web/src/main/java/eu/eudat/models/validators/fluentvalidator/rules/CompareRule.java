package eu.eudat.models.validators.fluentvalidator.rules;

import eu.eudat.models.validators.fluentvalidator.predicates.ComparisonOperator;
import eu.eudat.models.validators.fluentvalidator.predicates.FieldSelector;

/**
 * Created by ikalyvas on 8/31/2018.
 */
public class CompareRule<T> extends AbstractFluentValidatorRule<T> {

    private FieldSelector comparisonSelector;
    private ComparisonOperator comparisonOperator;

    public CompareRule(FieldSelector fieldSelector, FieldSelector comparisonSelector, ComparisonOperator comparisonOperator) {
        super(fieldSelector);
        this.comparisonOperator = comparisonOperator;
        this.comparisonSelector = comparisonSelector;
    }

    @Override
    public boolean assertValue(T item) {
        return this.comparisonOperator.compare(this.getFieldSelector().apply(item), this.comparisonSelector.apply(item));
    }
}
