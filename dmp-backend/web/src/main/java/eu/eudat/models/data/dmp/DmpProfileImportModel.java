package eu.eudat.models.data.dmp;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.UUID;

@XmlRootElement(name = "dmpProfile")
public class DmpProfileImportModel {

    private String dmpProfileName;
    private UUID dmpProfileId;
    private String values;

    @XmlElement(name = "dmpProfileName")
    public String getDmpProfileName() { return dmpProfileName; }
    public void setDmpProfileName(String dmpProfileName) { this.dmpProfileName = dmpProfileName; }

    @XmlElement(name = "dmpProfileId")
    public UUID getDmpProfileId() { return dmpProfileId; }
    public void setDmpProfileId(UUID dmpProfileId) { this.dmpProfileId = dmpProfileId; }

    @XmlElement(name = "values")
    public String getValues() { return values; }
    public void setValues(String values) { this.values = values; }
}
