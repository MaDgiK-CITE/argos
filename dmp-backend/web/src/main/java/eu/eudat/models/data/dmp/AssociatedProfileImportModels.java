package eu.eudat.models.data.dmp;

import java.util.List;
import java.util.UUID;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "profile")
public class AssociatedProfileImportModels {
    private UUID id;
    private String label;
    private List<Integer> section;

    @XmlElement(name = "profileId")
    public UUID getId() { return id; }
    public void setId(UUID id) { this.id = id; }

    @XmlElement(name = "profileLabel")
    public String getLabel() { return label; }
    public void setLabel(String label) { this.label = label; }

    @XmlElementWrapper(name="profileInSections")
    @XmlElement(name = "section")
    public List<Integer> getSection() {
        return section;
    }
    public void setSection(List<Integer> section) {
        this.section = section;
    }
}
