package eu.eudat.models.data.pid;

import java.util.ArrayList;
import java.util.List;

public class PidLinks {

    private List<PidLink> pidLinks = new ArrayList<>();

    public List<PidLink> getPidLinks() {
        return pidLinks;
    }
    public void setPidLinks(List<PidLink> pidLinks) {
        this.pidLinks = pidLinks;
    }
}
