package eu.eudat.models.data.errormodels;

import java.util.HashMap;


public class ValidationErrorContext extends HashMap<String, String> {

    public ValidationErrorContext() {

    }

    public void addFieldError(String path, String message) {
        this.put(path, message);
    }
}
