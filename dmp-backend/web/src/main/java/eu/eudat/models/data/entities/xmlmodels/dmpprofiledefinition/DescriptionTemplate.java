package eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition;

import eu.eudat.logic.utilities.interfaces.XmlSerializable;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.util.UUID;

public class DescriptionTemplate implements XmlSerializable<DescriptionTemplate> {

    private UUID id;
    private UUID descriptionTemplateId;
    private String label;
    private Integer minMultiplicity;
    private Integer maxMultiplicity;

    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getDescriptionTemplateId() {
        return descriptionTemplateId;
    }
    public void setDescriptionTemplateId(UUID descriptionTemplateId) {
        this.descriptionTemplateId = descriptionTemplateId;
    }

    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getMinMultiplicity() {
        return minMultiplicity;
    }
    public void setMinMultiplicity(Integer minMultiplicity) {
        this.minMultiplicity = minMultiplicity;
    }

    public Integer getMaxMultiplicity() {
        return maxMultiplicity;
    }
    public void setMaxMultiplicity(Integer maxMultiplicity) {
        this.maxMultiplicity = maxMultiplicity;
    }

    @Override
    public Element toXml(Document doc) {
        Element rootElement = doc.createElement("descriptionTemplate");
        rootElement.setAttribute("id", this.getId().toString());
        rootElement.setAttribute("descriptionTemplateId", this.getDescriptionTemplateId().toString());
        rootElement.setAttribute("label", this.label);
        if (this.minMultiplicity != null) rootElement.setAttribute("minMultiplicity", String.valueOf(this.minMultiplicity));
        if (this.maxMultiplicity != null) rootElement.setAttribute("maxMultiplicity", String.valueOf(this.maxMultiplicity));
        return rootElement;
    }

    @Override
    public DescriptionTemplate fromXml(Element item) {
        this.id = UUID.fromString(item.getAttribute("id"));
        this.descriptionTemplateId = UUID.fromString(item.getAttribute("descriptionTemplateId"));
        this.label = item.getAttribute("label");
        this.minMultiplicity = item.hasAttribute("minMultiplicity") && !item.getAttribute("minMultiplicity").equals("null") ? Integer.parseInt(item.getAttribute("minMultiplicity")) : null;
        this.maxMultiplicity = item.hasAttribute("maxMultiplicity") && !item.getAttribute("minMultiplicity").equals("null") ? Integer.parseInt(item.getAttribute("maxMultiplicity")) : null;
        return this;
    }
}
