package eu.eudat.models.data.userguide;

public class UserGuide {

	private String name;
	private String html;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHtml() {
		return html;
	}

	public void setHtml(String html) {
		this.html = html;
	}
}
