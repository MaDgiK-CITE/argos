package eu.eudat.models;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HintedModelFactory {
    private static final Logger logger = LoggerFactory.getLogger(HintedModelFactory.class);

    public static <T extends DataModel> String getHint(Class<T> clazz) {
        try {
            return clazz.newInstance().getHint();
        } catch (InstantiationException e) {
            logger.error(e.getMessage(), e);
            return null;
        } catch (IllegalAccessException e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }
}
