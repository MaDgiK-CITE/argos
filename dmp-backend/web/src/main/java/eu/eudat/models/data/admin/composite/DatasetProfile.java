package eu.eudat.models.data.admin.composite;

import eu.eudat.models.data.admin.components.datasetprofile.Page;
import eu.eudat.models.data.admin.components.datasetprofile.Section;
import eu.eudat.logic.utilities.builders.ModelBuilder;
import eu.eudat.models.data.listingmodels.UserInfoListingModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class DatasetProfile {
    private String label;
    private String description;
    private String type;
    private boolean enablePrefilling;
    private List<Page> pages;
    private List<Section> sections;
    private Short status;
    private Short version;
    private String language;
    private List<UserInfoListingModel> users;


    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    public boolean isEnablePrefilling() {
        return enablePrefilling;
    }
    public void setEnablePrefilling(boolean enablePrefilling) {
        this.enablePrefilling = enablePrefilling;
    }

    public List<Page> getPages() {
        return pages;
    }
    public void setPages(List<Page> pages) {
        this.pages = pages;
    }

    public List<Section> getSections() {
        return sections;
    }
    public void setSections(List<Section> sections) { this.sections = sections; }

    public Short getStatus() {
        return status;
    }
    public void setStatus(Short status) {
        this.status = status;
    }

    public Short getVersion() { return version; }
    public void setVersion(Short version) { this.version = version; }

    public String getLanguage() {
        return language;
    }
    public void setLanguage(String language) {
        this.language = language;
    }

    public List<UserInfoListingModel> getUsers() {
        return users;
    }
    public void setUsers(List<UserInfoListingModel> users) {
        this.users = users;
    }

    public void buildProfile(eu.eudat.models.data.entities.xmlmodels.datasetprofiledefinition.ViewStyleModel viewStyle) {
        this.enablePrefilling = viewStyle.isEnablePrefilling();
        this.sections = new ModelBuilder().fromViewStyleDefinition(viewStyle.getSections(), Section.class);
        this.pages = new ModelBuilder().fromViewStyleDefinition(viewStyle.getPages(), Page.class);
    }

    public DatasetProfile toShort() {
        DatasetProfile shortProfile = new DatasetProfile();
        shortProfile.setLabel(this.label);
        shortProfile.setDescription(this.description);
        shortProfile.setType(this.type);
        shortProfile.setEnablePrefilling(this.enablePrefilling);
        List<Section> shortSection = new LinkedList<>();
        for (Section toshortSection : this.getSections()) {
            shortSection.add(toshortSection.toShort());
        }
        Collections.sort(shortSection);
        shortProfile.setSections(shortSection);
        shortProfile.setPages(this.pages);
        shortProfile.setStatus(this.status);
        shortProfile.setVersion(this.version);
        shortProfile.setLanguage(this.language);
        shortProfile.setUsers(new ArrayList<>());
        return shortProfile;
    }
}
