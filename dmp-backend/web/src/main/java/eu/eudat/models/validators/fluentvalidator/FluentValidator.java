package eu.eudat.models.validators.fluentvalidator;

import eu.eudat.models.validators.fluentvalidator.predicates.FieldSelector;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by ikalyvas on 8/28/2018.
 */
public abstract class FluentValidator<T> implements Validator {

    private List<FluentValidatorBuilder> fluentValidatorBuilders = new LinkedList<>();

    public <R> FluentValidatorBuilder<T> ruleFor(FieldSelector<T, R> selector) {
        FluentValidatorBuilder<T> fluentValidatorBuilder = new FluentValidatorBuilder<>(selector);
        this.fluentValidatorBuilders.add(fluentValidatorBuilder);
        return fluentValidatorBuilder;
    }

    @Override
    public abstract boolean supports(Class<?> clazz);

    @Override
    public void validate(Object target, Errors errors) {
        List<FluentValidatorResult> validatorResults = new LinkedList<>();
        //this.fluentValidatorBuilders.forEach(x-> validatorResults.addAll(x.validate(target)));
        //validatorResults.forEach(x-> errors.rejectValue(x.getField(),x.getError()));
    }
}
