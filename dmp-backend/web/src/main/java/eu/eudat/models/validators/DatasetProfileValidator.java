package eu.eudat.models.validators;

import eu.eudat.models.data.admin.composite.DatasetProfile;
import eu.eudat.models.data.grant.Grant;
import eu.eudat.models.validators.fluentvalidator.FluentValidator;
import org.springframework.stereotype.Component;


@Component("datasetProfileValidator")
public class DatasetProfileValidator extends FluentValidator<Grant> {

    public DatasetProfileValidator() {
        ruleFor(x -> x.getLabel()).notEmpty()
                .withName("label").withMessage("datasetprofile.label.null");
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return DatasetProfile.class.equals(aClass);
    }

    public static boolean supportsType(Class<?> clazz) {
        return DatasetProfile.class.equals(clazz);
    }

}
