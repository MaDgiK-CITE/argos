package eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition;

import eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition.types.DMPProfileFieldDataType;
import eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition.types.DMPProfileType;
import eu.eudat.logic.utilities.interfaces.XmlSerializable;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.util.UUID;

/**
 * Created by ikalyvas on 3/21/2018.
 */
public class Field implements XmlSerializable<Field> {

    private UUID id;
    private DMPProfileType type;
    private DMPProfileFieldDataType dataType;
    private Boolean required;
    private String label;
    private Object value;
    private DmpProfileExternalAutoComplete externalAutocomplete;

    public UUID getId() {
        if (this.id == null) this.id = UUID.randomUUID();
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }

    public Integer getType() {
        return type.getValue();
    }
    public void setType(DMPProfileType type) {
        this.type = type;
    }

    public Integer getDataType() {
        return dataType.getValue();
    }
    public void setDataType(DMPProfileFieldDataType dataType) {
        this.dataType = dataType;
    }

    public Boolean getRequired() {
        return required;
    }
    public void setRequired(Boolean required) {
        this.required = required;
    }

    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }

    public Object getValue() {
        return value;
    }
    public void setValue(Object value) {
        this.value = value;
    }

	public DmpProfileExternalAutoComplete getExternalAutocomplete() {
		return externalAutocomplete;
	}
	public void setExternalAutocomplete(DmpProfileExternalAutoComplete externalAutocomplete) {
		this.externalAutocomplete = externalAutocomplete;
	}

	@Override
    public Element toXml(Document doc) {
        Element rootElement = doc.createElement("field");
        rootElement.setAttribute("id", this.getId().toString());
        rootElement.setAttribute("type", this.type.getValue().toString());
        rootElement.setAttribute("datatype", "" + this.dataType.getValue().toString());
        rootElement.setAttribute("required", this.required.toString());
        rootElement.setAttribute("label", this.label);
        if (this.externalAutocomplete != null)
        	rootElement.appendChild(this.externalAutocomplete.toXml(doc));

        return rootElement;
    }

    @Override
    public eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition.Field fromXml(Element item) {
        this.id = UUID.fromString(item.getAttribute("id"));
        this.label = item.getAttribute("label");
        this.dataType = DMPProfileFieldDataType.fromInteger(Integer.parseInt(item.getAttribute("datatype")));
        this.required = Boolean.valueOf(item.getAttribute("required"));
        this.type = DMPProfileType.fromInteger(Integer.parseInt(item.getAttribute("type")));
		Element optionElement = (Element) item.getElementsByTagName("externalAutocomplete").item(0);
		if (optionElement != null) {
			this.externalAutocomplete = new DmpProfileExternalAutoComplete().fromXml(optionElement);
		}
        return this;
    }
}
