package eu.eudat.models.data.properties;

import java.util.List;
import java.util.Map;

public class PropertiesModel implements PropertiesGenerator {
    private int status;
    private List<eu.eudat.models.data.properties.DatasetProfilePage> pages;

    public List<eu.eudat.models.data.properties.DatasetProfilePage> getPages() {
        return pages;
    }

    public void setPages(List<eu.eudat.models.data.properties.DatasetProfilePage> pages) {
        this.pages = pages;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public void toMap(Map<String, Object> fieldValues) {
        this.pages.forEach(item -> item.getSections().forEach(sectionItem -> sectionItem.toMap(fieldValues)));
    }

    @Override
    public void toMap(Map<String, Object> fieldValues, int index) {
        // TODO Auto-generated method stub

    }

}
