package eu.eudat.models.data.admin.components.datasetprofile;

import eu.eudat.logic.utilities.interfaces.ViewStyleDefinition;
import eu.eudat.logic.utilities.builders.ModelBuilder;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.Collections;
import java.util.List;

public class Section implements Comparable, ViewStyleDefinition<eu.eudat.models.data.entities.xmlmodels.datasetprofiledefinition.Section> {
    private List<Section> sections;
    private List<FieldSet> fieldSets;
    private Boolean defaultVisibility;
    private String page;
    private Integer ordinal;
    private String id;
    private String title;
    private String description;
    private Boolean multiplicity;

    public List<Section> getSections() {
        return sections;
    }

    public void setSections(List<Section> sections) {
        this.sections = sections;
    }

    public List<FieldSet> getFieldSets() {
        return fieldSets;
    }

    public void setFieldSets(List<FieldSet> fieldSets) {
        this.fieldSets = fieldSets;
    }

    public Boolean getDefaultVisibility() {
        return defaultVisibility;
    }

    public void setDefaultVisibility(Boolean defaultVisibility) {
        this.defaultVisibility = defaultVisibility;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(int ordinal) {
        this.ordinal = ordinal;
    }

    public Boolean getMultiplicity() {
        return multiplicity;
    }

    public void setMultiplicity(Boolean multiplicity) {
        this.multiplicity = multiplicity;
    }

    @Override
    public eu.eudat.models.data.entities.xmlmodels.datasetprofiledefinition.Section toDatabaseDefinition(eu.eudat.models.data.entities.xmlmodels.datasetprofiledefinition.Section item) {
        if (this.id == null || this.id.isEmpty()) this.id = "section_" + RandomStringUtils.random(5, true, true);
        item.setDefaultVisibility(this.defaultVisibility);
        item.setDescription(this.description);
        item.setMultiplicity(this.multiplicity);
        if (this.fieldSets != null)
            item.setFieldSets(new ModelBuilder().toViewStyleDefinition(this.fieldSets, eu.eudat.models.data.entities.xmlmodels.datasetprofiledefinition.FieldSet.class));
        item.setId(this.id);
        item.setOrdinal(this.ordinal);
        item.setPage(this.page);
        if (this.sections != null)
            item.setSections(new ModelBuilder().toViewStyleDefinition(this.sections, eu.eudat.models.data.entities.xmlmodels.datasetprofiledefinition.Section.class));
        item.setTitle(this.title);
        return item;
    }

    @Override
    public void fromDatabaseDefinition(eu.eudat.models.data.entities.xmlmodels.datasetprofiledefinition.Section item) {

        this.defaultVisibility = item.isDefaultVisibility();
        this.description = item.getDescription();
        this.fieldSets = new ModelBuilder().fromViewStyleDefinition(item.getFieldSets(), FieldSet.class);
        this.id = item.getId();
        this.ordinal = item.getOrdinal();
        this.page = item.getPage();
        this.sections = new ModelBuilder().fromViewStyleDefinition(item.getSections(), Section.class);
        this.title = item.getTitle();
        this.multiplicity = item.getMultiplicity();
    }

    @Override
    public int compareTo(Object o) {
        return this.ordinal.compareTo(((Section) o).getOrdinal());
    }

    public Section toShort(){
       Section shortenSection = new Section();

        List<Section> toShortsections = this.sections;
        List<FieldSet> toShortFieldSets = this.fieldSets;
        Collections.sort(toShortsections);
        Collections.sort(toShortFieldSets);
        for (Section shortsections : toShortsections ) { shortsections.toShort(); }
        for (FieldSet shortFieldSets : toShortFieldSets ) { shortFieldSets.toShort(); }

        shortenSection.setSections(toShortsections);
        shortenSection.setFieldSets(toShortFieldSets);

        shortenSection.setDefaultVisibility(this.defaultVisibility);
        shortenSection.setPage(this.page);
        shortenSection.setOrdinal(this.ordinal);
        shortenSection.setId(this.id);
        shortenSection.setTitle(this.title);
        shortenSection.setDescription(this.description);
        shortenSection.setMultiplicity(this.multiplicity);
        return shortenSection;
    }

}
