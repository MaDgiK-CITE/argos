package eu.eudat.models.data.grant;

import eu.eudat.data.entities.Grant;
import eu.eudat.data.entities.UserInfo;
import eu.eudat.models.DataModel;
import eu.eudat.models.data.files.ContentFile;
import eu.eudat.models.data.urls.DataManagementPlanUrlListing;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;


public class GrantListingModel implements DataModel<eu.eudat.data.entities.Grant, GrantListingModel> {

    private UUID id;

    private String label;

    private String abbreviation;

    private String reference;

    private String uri;

    private String definition;

    private Date startDate;

    private Date endDate;

    private Grant.Status status;

    private UserInfo creationUser;

    private Date created;

    private Date modified;

    private String description;

    private List<ContentFile> files;

    private List<DataManagementPlanUrlListing> dmps;


    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public List<DataManagementPlanUrlListing> getDmps() {
        return dmps;
    }

    public void setDmps(List<DataManagementPlanUrlListing> dmps) {
        this.dmps = dmps;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    public Short getStatus() {
        return status != null ? status.getValue() : null;
    }

    public void setStatus(Short status) {
        this.status = Grant.Status.fromInteger(status);
    }

    public UserInfo getCreationUser() {
        return creationUser;
    }

    public void setCreationUser(UserInfo creationUser) {
        this.creationUser = creationUser;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public void setStatus(Grant.Status status) {
        this.status = status;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ContentFile> getFiles() {
        return files;
    }

    public void setFiles(List<ContentFile> files) {
        this.files = files;
    }

    @Override
    public GrantListingModel fromDataModel(eu.eudat.data.entities.Grant entity) {
        this.id = entity.getId();
        this.label = entity.getLabel();
        return this;
    }

    public GrantListingModel fromDataModelWIthDmps(eu.eudat.data.entities.Grant entity) {
        this.fromDataModel(entity);
        this.abbreviation = entity.getAbbreviation();
        this.reference = entity.getReference();
        this.uri = entity.getUri();
        this.definition = entity.getDefinition();
        this.startDate = entity.getStartdate();
        this.endDate = entity.getEnddate();
        this.setStatus(entity.getStatus());
        this.created = entity.getCreated();
        this.modified = entity.getModified();
        this.description = entity.getDescription();
        this.files = entity.getContent() != null ? Arrays.asList(new ContentFile(entity.getContent().getLabel(), UUID.fromString(entity.getContent().getUri().split(":")[1]), "final", entity.getContent().getExtension())) : Arrays.asList(new ContentFile("default.png", null, null, null));
        this.dmps = entity.getDmps().stream().map(item -> new DataManagementPlanUrlListing().fromDataModel(item)).collect(Collectors.toList());
        return this;
    }

    public GrantListingModel fromDataModelDetails(eu.eudat.data.entities.Grant entity) {
        this.fromDataModel(entity);
        this.abbreviation = entity.getAbbreviation();
        this.reference = entity.getReference();
        this.uri = entity.getUri();
        this.definition = entity.getDefinition();
        this.startDate = entity.getStartdate();
        this.endDate = entity.getEnddate();
        this.setStatus(entity.getStatus());
        this.created = entity.getCreated();
        this.modified = entity.getModified();
        this.description = entity.getDescription();
        return this;
    }

    @Override
    public eu.eudat.data.entities.Grant toDataModel() throws Exception {
        throw new Exception("Not Implemented");
    }

    @Override
    public String getHint() {
        return "grantListingItem";
    }
}
