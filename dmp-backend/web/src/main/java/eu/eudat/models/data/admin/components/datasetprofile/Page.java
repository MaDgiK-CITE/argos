package eu.eudat.models.data.admin.components.datasetprofile;

import eu.eudat.logic.utilities.interfaces.ViewStyleDefinition;


public class Page implements Comparable, ViewStyleDefinition<eu.eudat.models.data.entities.xmlmodels.datasetprofiledefinition.Page> {
    private String id;
    private Integer ordinal;
    private String title;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(int ordinal) {
        this.ordinal = ordinal;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public eu.eudat.models.data.entities.xmlmodels.datasetprofiledefinition.Page toDatabaseDefinition(eu.eudat.models.data.entities.xmlmodels.datasetprofiledefinition.Page item) {
        item.setId(this.id);
        item.setOrdinal(this.ordinal);
        item.setTitle(this.title);
        return item;
    }

    @Override
    public void fromDatabaseDefinition(eu.eudat.models.data.entities.xmlmodels.datasetprofiledefinition.Page item) {
        this.title = item.getTitle();
        this.ordinal = item.getOrdinal();
        this.id = item.getId();
    }

    @Override
    public int compareTo(Object o) {
        return this.ordinal.compareTo((Integer) o);
    }
}
