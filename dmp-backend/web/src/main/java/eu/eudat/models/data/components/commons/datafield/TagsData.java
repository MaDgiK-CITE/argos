package eu.eudat.models.data.components.commons.datafield;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.util.HashMap;
import java.util.Map;

public class TagsData extends FieldData<TagsData> {
    @Override
    public TagsData fromData(Object data) {
        if (data != null) {
            this.setLabel((String) ((Map<String, Object>) data).get("label"));
        }
        return this;
    }

    @Override
    public Object toData() {
        return null;
    }

    @Override
    public Element toXml(Document doc) {
        Element root = doc.createElement("data");
        root.setAttribute("label", this.getLabel());
        return root;
    }

    @Override
    public TagsData fromXml(Element item) {
        this.setLabel(item != null ? item.getAttribute("label") : "");
        return this;
    }

    @Override
    public Map<String, Object> toMap(Element item) {
        HashMap dataMap = new HashMap();
        dataMap.put("label", item != null ? item.getAttribute("label") : "");
        return dataMap;
    }
}
