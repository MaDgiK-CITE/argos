package eu.eudat.models.rda.mapper;

import eu.eudat.models.rda.FunderId;

import java.util.UUID;

public class FunderIdRDAMapper {

	public static FunderId toRDA(Object id) {
		FunderId rda = new FunderId();
		rda.setIdentifier(id.toString());
		if (id instanceof UUID) {
			rda.setType(FunderId.Type.OTHER);
		} else {
			rda.setType(FunderId.Type.FUNDREF);
		}
		return rda;
	}
}
