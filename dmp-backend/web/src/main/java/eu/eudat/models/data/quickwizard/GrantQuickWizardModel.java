package eu.eudat.models.data.quickwizard;


import eu.eudat.models.data.grant.Grant;

import java.util.UUID;

public class GrantQuickWizardModel {
    private String label;
    private String description;
    private Grant existGrant;


    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Grant getExistGrant() {
        return existGrant;
    }

    public void setExistGrant(Grant existGrant) {
        this.existGrant = existGrant;
    }

    public eu.eudat.models.data.grant.Grant toDataGrant() {
        eu.eudat.models.data.grant.Grant toGrant = new eu.eudat.models.data.grant.Grant();
        toGrant.setId(UUID.randomUUID());
        toGrant.setAbbreviation("");
        toGrant.setLabel(this.label);
        toGrant.setReference("dmp:" + toGrant.getId());
        toGrant.setUri("");
        toGrant.setDefinition("");
        toGrant.setDescription(this.description);
        return toGrant;
    }
}
