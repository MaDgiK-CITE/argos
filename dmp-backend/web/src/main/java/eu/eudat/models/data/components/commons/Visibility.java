package eu.eudat.models.data.components.commons;

import eu.eudat.logic.utilities.interfaces.XmlSerializable;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.LinkedList;
import java.util.List;

public class Visibility implements XmlSerializable<Visibility> {
    private List<Rule> rules;
    private String style;

    public List<Rule> getRules() {
        return rules;
    }

    public void setRules(List<Rule> rules) {
        this.rules = rules;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    @Override
    public Element toXml(Document doc) {
        Element root = doc.createElement("visible");
        root.setAttribute("style", this.style);
        if (rules != null && !rules.isEmpty()) {
            for (Rule rule : rules) {
                root.appendChild(rule.toXml(doc));
            }
        }
        return root;
    }

    @Override
    public Visibility fromXml(Element item) {
        this.style = item.getAttribute("style");
        this.rules = new LinkedList();
        NodeList rulesElements = item.getChildNodes();
        for (int temp = 0; temp < rulesElements.getLength(); temp++) {
            Node ruleElement = rulesElements.item(temp);
            if (ruleElement.getNodeType() == Node.ELEMENT_NODE) {
                this.rules.add(new Rule().fromXml((Element) ruleElement));
            }
        }
        return this;
    }
}
