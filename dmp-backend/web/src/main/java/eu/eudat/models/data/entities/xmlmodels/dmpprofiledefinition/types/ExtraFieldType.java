package eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition.types;

public enum ExtraFieldType {
    TEXT(0), RICH_TEXT(1), DATE(2), NUMBER(3);

    private Integer value;

    private ExtraFieldType(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }

    public static ExtraFieldType fromInteger(Integer value) {
        switch (value) {
            case 0:
                return TEXT;
            case 1:
                return RICH_TEXT;
            case 2:
                return DATE;
            case 3:
                return NUMBER;
            default:
                throw new RuntimeException("Unsupported ExtraFieldType Type");
        }
    }
}
