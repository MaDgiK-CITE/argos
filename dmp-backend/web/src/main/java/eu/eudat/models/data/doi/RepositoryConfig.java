package eu.eudat.models.data.doi;

import eu.eudat.depositinterface.repository.RepositoryDepositConfiguration;

public class RepositoryConfig {

    private int depositType;
    private String repositoryId;
    private String repositoryAuthorizationUrl;
    private String repositoryRecordUrl;
    private String repositoryClientId;
    private String redirectUri;
    private boolean hasLogo;

    public int getDepositType() {
        return depositType;
    }
    public void setDepositType(int depositType) {
        this.depositType = depositType;
    }

    public String getRepositoryId() {
        return repositoryId;
    }
    public void setRepositoryId(String repositoryId) {
        this.repositoryId = repositoryId;
    }

    public String getRepositoryAuthorizationUrl() {
        return repositoryAuthorizationUrl;
    }
    public void setRepositoryAuthorizationUrl(String repositoryAuthorizationUrl) {
        this.repositoryAuthorizationUrl = repositoryAuthorizationUrl;
    }

    public String getRepositoryRecordUrl() {
        return repositoryRecordUrl;
    }
    public void setRepositoryRecordUrl(String repositoryRecordUrl) {
        this.repositoryRecordUrl = repositoryRecordUrl;
    }

    public String getRepositoryClientId() {
        return repositoryClientId;
    }
    public void setRepositoryClientId(String repositoryClientId) {
        this.repositoryClientId = repositoryClientId;
    }

    public String getRedirectUri() {
        return redirectUri;
    }
    public void setRedirectUri(String redirectUri) {
        this.redirectUri = redirectUri;
    }

    public boolean isHasLogo() {
        return hasLogo;
    }
    public void setHasLogo(boolean hasLogo) {
        this.hasLogo = hasLogo;
    }

    public RepositoryConfig toModel(RepositoryDepositConfiguration r){
        this.setDepositType(r.getDepositType());
        this.setRepositoryId(r.getRepositoryId());
        this.setRepositoryAuthorizationUrl(r.getRepositoryAuthorizationUrl());
        this.setRepositoryRecordUrl(r.getRepositoryRecordUrl());
        this.setRepositoryClientId(r.getRepositoryClientId());
        this.setRedirectUri(r.getRedirectUri());
        this.setHasLogo(r.isHasLogo());
        return this;
    }
}
