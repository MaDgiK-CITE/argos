package eu.eudat.models.data.datasetprofile;

import eu.eudat.data.entities.DescriptionTemplate;
import eu.eudat.models.DataModel;

import java.util.UUID;


public class DatasetProfileAutocompleteItem implements DataModel<DescriptionTemplate, DatasetProfileAutocompleteItem> {

    private UUID id;
    private String label;
    private String description;

    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public DatasetProfileAutocompleteItem fromDataModel(DescriptionTemplate entity) {
        this.id = entity.getId();
        this.label = entity.getLabel();
        this.description = entity.getDescription();
        return this;
    }

    @Override
    public DescriptionTemplate toDataModel() {
        DescriptionTemplate profile = new DescriptionTemplate();
        profile.setId(this.id);
        return profile;
    }

    @Override
    public String getHint() {
        return null;
    }
}
