package eu.eudat.models.data.datasetImport;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "page")
public class DatasetImportPage {
    private Integer ordinal;
    private String title;
    private DatasetImportSections sections;

    public Integer getOrdinal() {
        return ordinal;
    }
    public void setOrdinal(Integer ordinal) {
        this.ordinal = ordinal;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    @XmlElement(name = "sections")
    public DatasetImportSections getSections() {
        return sections;
    }
    public void setSections(DatasetImportSections sections) {
        this.sections = sections;
    }
}
