package eu.eudat.models.data.dmp;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "researcher")
public class ResearcherImportModels {
    private String label;
    private String name;
    private String id;
    private int status;
    private String reference;

    @XmlElement(name = "label")
    public String getResearcherImportLabel() {
        return label;
    }
    public void setResearcherImportLabel(String label) {
        this.label = label;
    }

    @XmlElement(name = "name")
    public String getResearcherImportName() {
        return name;
    }
    public void setResearcherImportName(String name) {
        this.name = name;
    }

    @XmlElement(name = "id")
    public String getResearcherImportId() {
        return id;
    }
    public void setResearcherImportId(String id) {
        this.id = id;
    }

    @XmlElement(name = "status")
    public int getResearcherImportStatus() {
        return status;
    }
    public void setResearcherImportStatus(int status) {
        this.status = status;
    }

    @XmlElement(name = "reference")
    public String getResearcherImportReference() { return reference; }
    public void setResearcherImportReference(String reference) { this.reference = reference; }
}
