package eu.eudat.models.data.dynamicfields;

/**
 * Created by ikalyvas on 3/23/2018.
 */
public class Dependency {
    private String id;
    private String queryProperty;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQueryProperty() {
        return queryProperty;
    }

    public void setQueryProperty(String queryProperty) {
        this.queryProperty = queryProperty;
    }
}
