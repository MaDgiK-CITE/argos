package eu.eudat.models.data.dataset;

import eu.eudat.data.entities.DatasetDataRepository;
import eu.eudat.data.entities.DatasetService;
import eu.eudat.models.DataModel;
import eu.eudat.models.data.datasetprofile.DatasetProfileListingModel;
import eu.eudat.models.data.dmp.DataManagementPlan;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class Dataset implements DataModel<eu.eudat.data.entities.Dataset, Dataset> {
    private UUID id;
    private String label;
    private String reference;
    private String uri;
    private String description;
    private short status;
    private String properties;
    private Date created;
    private DataManagementPlan dmp;
    private DatasetProfileListingModel profile;
    private List<Registry> registries;
    private List<Service> services;
    private List<DataRepository> dataRepositories;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public short getStatus() {
        return status;
    }

    public void setStatus(short status) {
        this.status = status;
    }

    public String getProperties() {
        return properties;
    }

    public void setProperties(String properties) {
        this.properties = properties;
    }

    public List<Registry> getRegistries() {
        return registries;
    }

    public void setRegistries(List<Registry> registries) {
        this.registries = registries;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

    public List<DataRepository> getDataRepositories() {
        return dataRepositories;
    }

    public void setDataRepositories(List<DataRepository> dataRepositories) {
        this.dataRepositories = dataRepositories;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public DataManagementPlan getDmp() {
        return dmp;
    }

    public void setDmp(DataManagementPlan dmp) {
        this.dmp = dmp;
    }

    public DatasetProfileListingModel getProfile() {
        return profile;
    }

    public void setProfile(DatasetProfileListingModel profile) {
        this.profile = profile;
    }

    public Dataset fromDataModel(eu.eudat.data.entities.Dataset entity) {
        this.id = entity.getId();
        this.label = entity.getLabel();
        this.properties = entity.getProperties();
        this.reference = entity.getReference();
        this.description = entity.getDescription();
        this.profile = new DatasetProfileListingModel();
        this.profile.fromDataModel(entity.getProfile());
        this.registries = entity.getRegistries().stream().map(item -> new Registry().fromDataModel(item)).collect(Collectors.toList());
        this.dataRepositories = entity.getDatasetDataRepositories().stream().map(item -> new DataRepository().fromDataModel(item.getDataRepository())).collect(Collectors.toList());
        this.services = entity.getServices().stream().map(item -> new Service().fromDataModel(item.getService())).collect(Collectors.toList());
        this.created = entity.getCreated();
        return this;
    }

    public eu.eudat.data.entities.Dataset toDataModel() throws Exception {
        eu.eudat.data.entities.Dataset entity = new eu.eudat.data.entities.Dataset();
        entity.setId(this.id);
        entity.setLabel(this.label);
        entity.setReference(this.reference);
        entity.setUri(this.uri);
        entity.setProperties(this.properties);
        entity.setStatus(this.status);
        entity.setDmp(dmp.toDataModel());
        entity.setDescription(this.description);
        entity.setCreated(this.created != null ? this.created : new Date());
        entity.setModified(new Date());
        entity.setProfile(profile.toDataModel());  ///TODO
        if (!this.registries.isEmpty()) {
            entity.setRegistries(new HashSet<eu.eudat.data.entities.Registry>());
            for (Registry registry : this.registries) {
                entity.getRegistries().add(registry.toDataModel());
            }
        }

        if (!this.dataRepositories.isEmpty()) {
            entity.setDatasetDataRepositories(new HashSet<>());
            for (DataRepository dataRepositoryModel : this.dataRepositories) {
                DatasetDataRepository datasetDataRepository = new DatasetDataRepository();
                eu.eudat.data.entities.DataRepository dataRepository = dataRepositoryModel.toDataModel();
                datasetDataRepository.setDataRepository(dataRepository);
                entity.getDatasetDataRepositories().add(datasetDataRepository);
            }
        }

        if (!this.services.isEmpty()) {
            entity.setServices(new HashSet<DatasetService>());
            for (Service serviceModel : this.services) {
                eu.eudat.data.entities.Service service = serviceModel.toDataModel();
                DatasetService datasetService = new DatasetService();
                datasetService.setService(service);
                entity.getServices().add(datasetService);
            }
        }
        return entity;
    }

    @Override
    public String getHint() {
        return null;
    }
}
