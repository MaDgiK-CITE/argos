package eu.eudat.models.data.dashboard.recent.tablerequest;

import eu.eudat.criteria.RecentActivityCriteria;
import eu.eudat.data.query.definition.helpers.ColumnOrderings;

public class RecentActivityTableRequest {
	private ColumnOrderings orderings;
	private RecentActivityCriteria criteria;
	private int dmpOffset;
	private int datasetOffset;
	private int length;

	public int getDmpOffset() {
		return dmpOffset;
	}

	public void setDmpOffset(int dmpOffset) {
		this.dmpOffset = dmpOffset;
	}

	public int getDatasetOffset() {
		return datasetOffset;
	}

	public void setDatasetOffset(int datasetOffset) {
		this.datasetOffset = datasetOffset;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public RecentActivityCriteria getCriteria() {
		return criteria;
	}

	public void setCriteria(RecentActivityCriteria criteria) {
		this.criteria = criteria;
	}

	public ColumnOrderings getOrderings() {
		return orderings;
	}

	public void setOrderings(ColumnOrderings orderings) {
		this.orderings = orderings;
	}
}
