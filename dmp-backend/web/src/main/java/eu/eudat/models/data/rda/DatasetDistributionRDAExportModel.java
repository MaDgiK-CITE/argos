package eu.eudat.models.data.rda;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;

public class DatasetDistributionRDAExportModel {
	private String access_url;
	private String available_till;
	private String byte_size;
	private String data_access; // Allowed values: open / shared / closed
	private String description;
	private String download_url;
	private List<String> format; // Format according to: https://www.iana.org/assignments/media-types/media-types.xhtml if appropriate, otherwise use the common name for this format
	private HostRDAExportModel host;
	private List<LicenseRDAExportModel> license;
	private String title;

	public String getAccess_url() {
		return access_url;
	}
	public void setAccess_url(String access_url) {
		this.access_url = access_url;
	}

	public String getAvailable_till() {
		return available_till;
	}
	public void setAvailable_till(String available_till) {
		this.available_till = available_till;
	}

	public String getByte_size() {
		return byte_size;
	}
	public void setByte_size(String byte_size) {
		this.byte_size = byte_size;
	}

	public String getData_access() {
		return data_access;
	}
	public void setData_access(String data_access) {
		this.data_access = data_access;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public String getDownload_url() {
		return download_url;
	}
	public void setDownload_url(String download_url) {
		this.download_url = download_url;
	}

	public List<String> getFormat() {
		return format;
	}
	public void setFormat(List<String> format) {
		this.format = format;
	}

	public HostRDAExportModel getHost() {
		return host;
	}
	public void setHost(HostRDAExportModel host) {
		this.host = host;
	}

	public List<LicenseRDAExportModel> getLicense() {
		return license;
	}
	public void setLicense(List<LicenseRDAExportModel> license) {
		this.license = license;
	}

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	@JsonIgnore
	public boolean isValid() {
		return title != null || data_access != null;
	}
}
