package eu.eudat.models.data.registries;

import com.fasterxml.jackson.annotation.JsonProperty;
import eu.eudat.data.entities.Registry;
import eu.eudat.data.entities.UserInfo;
import eu.eudat.models.DataModel;

import java.util.Date;
import java.util.Map;
import java.util.UUID;

public class RegistryModel implements DataModel<Registry, RegistryModel> {
    private UUID id;
    private String name;
    private String label;
    private String pid;
    private String abbreviation;
    private String uri;
    private Date created;
    private Date modified;
    private String reference;
    private String tag; // Api fetching the data
    private String source; // Actual harvested source

    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }

    public String getAbbreviation() {
        return abbreviation;
    }
    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getUri() {
        return uri;
    }
    public void setUri(String uri) {
        this.uri = uri;
    }

    public Date getCreated() {
        return created;
    }
    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }
    public void setModified(Date modified) {
        this.modified = modified;
    }

    public String getReference() {
        return reference;
    }
    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getTag() {
        return tag;
    }
    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getPid() {
        return pid;
    }
    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getSource() {
        return source;
    }
    public void setSource(String source) {
        this.source = source;
    }

    @Override
    public RegistryModel fromDataModel(Registry entity) {
        this.id = entity.getId();
        this.abbreviation = entity.getAbbreviation();
        this.created = entity.getCreated();
        this.label = entity.getLabel();
        this.name = entity.getLabel();
        this.modified = entity.getModified();
        this.uri = entity.getUri();
        String source1 = entity.getReference().substring(0, entity.getReference().indexOf(":"));
        if (source1.equals("dmp")) {
            this.source = "Internal";
        } else {
            this.source = source1;
        }
        this.reference = entity.getReference();
        return this;
    }

    @Override
    public Registry toDataModel() throws Exception {
        Registry registry = new Registry();
        registry.setAbbreviation(this.abbreviation);
        registry.setCreated(this.created != null ? this.created : new Date());
        registry.setId(this.id != null ? this.id : UUID.randomUUID());
        registry.setLabel(this.label != null ? this.label : this.name);
        registry.setUri(this.uri);
        registry.setModified(new Date());
        if (this.source == null) this.source = "dmp";
        if (this.source.equals(registry.getId().toString().substring(0, this.source.length()))) {
            registry.setReference(registry.getId().toString());
        } else {
            registry.setReference(this.source + ":" + registry.getId());
        }
        registry.setStatus((short)0);
        registry.setCreationUser(new UserInfo());
        return registry;
    }

    @Override
    public String getHint() {
        return null;
    }
}
