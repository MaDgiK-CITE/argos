package eu.eudat.models.data.properties;

import java.util.List;
import java.util.Map;

public class Section implements PropertiesGenerator {

    private List<Section> sections;
    private List<FieldSet> compositeFields;

    public List<Section> getSections() {
        return sections;
    }

    public void setSections(List<Section> sections) {
        this.sections = sections;
    }

    public List<FieldSet> getCompositeFields() {
        return compositeFields;
    }

    public void setCompositeFields(List<FieldSet> compositeFields) {
        this.compositeFields = compositeFields;
    }

    @Override
    public void toMap(Map<String, Object> fieldValues) {
        this.sections.forEach(item -> item.toMap(fieldValues));
        this.compositeFields.forEach(item -> item.toMap(fieldValues));
    }

    @Override
    public void toMap(Map<String, Object> fieldValues, int index) {
        // TODO Auto-generated method stub

    }
}
