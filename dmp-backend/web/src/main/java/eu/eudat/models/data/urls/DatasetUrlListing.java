package eu.eudat.models.data.urls;

import eu.eudat.data.entities.Dataset;

import java.util.UUID;

/**
 * Created by ikalyvas on 7/23/2018.
 */
public class DatasetUrlListing extends UrlListing<Dataset, DatasetUrlListing> {
    @Override
    public DatasetUrlListing fromDataModel(Dataset entity) {
        this.setLabel(entity.getLabel());
        this.setUrl(entity.getId().toString());
        return this;
    }

    @Override
    public Dataset toDataModel() {
        Dataset entity = new Dataset();
        entity.setId(UUID.fromString(this.getUrl()));
        entity.setLabel(this.getLabel());
        return entity;
    }

    @Override
    public String getHint() {
        return null;
    }
}
