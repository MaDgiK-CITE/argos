package eu.eudat.models.data.dashboard.recent.model;

import eu.eudat.data.entities.Dataset;
import eu.eudat.models.data.datasetprofile.DatasetProfileOverviewModel;
import eu.eudat.models.data.listingmodels.UserInfoListingModel;

import java.util.Date;
import java.util.stream.Collectors;

public class RecentDatasetModel extends RecentActivityModel<Dataset, RecentDatasetModel> {
	private String dmp;
	private String dmpId;

	public String getDmp() {
		return dmp;
	}

	public void setDmp(String dmp) {
		this.dmp = dmp;
	}

	public String getDmpId() {
		return dmpId;
	}

	public void setDmpId(String dmpId) {
		this.dmpId = dmpId;
	}

	@Override
	public RecentActivityModel fromEntity(Dataset entity) {
		this.setType(RecentActivityType.DATASET.getIndex());
		this.setId(entity.getId().toString());
		this.setTitle(entity.getLabel());
		this.setCreated(entity.getCreated());
		this.setModified(entity.getModified());
		this.setStatus(entity.getStatus());
		this.setVersion(entity.getDmp() != null ? entity.getDmp().getVersion(): 0);
		this.setFinalizedAt(entity.getFinalizedAt());
		this.setPublishedAt(entity.getDmp() != null ? entity.getDmp().getPublishedAt() : new Date());
		this.setProfile(entity.getProfile() != null ? new DatasetProfileOverviewModel().fromDataModel(entity.getProfile()): null);
		if (entity.getDmp() != null && entity.getDmp().getGrant() != null) {
			this.setGrant(entity.getDmp().getGrant().getLabel());
		}
		this.setDmp( entity.getDmp() != null ? entity.getDmp().getLabel() : "");
		this.setDmpId(entity.getDmp() != null ? entity.getDmp().getId().toString() : "");
		this.setPublic(entity.getDmp().isPublic());
		this.setUsers(entity.getDmp().getUsers().stream().map(x -> new UserInfoListingModel().fromDataModel(x)).collect(Collectors.toList()));
		return this;
	}

	public RecentDatasetModel fromDmpEntity(Dataset entity) {
		this.setType(RecentActivityType.DATASET.getIndex());
		this.setId(entity.getId().toString());
		this.setTitle(entity.getLabel());
		this.setCreated(entity.getCreated());
		this.setModified(entity.getModified());
		this.setStatus(entity.getStatus());
		this.setVersion(entity.getDmp() != null ? entity.getDmp().getVersion(): 0);
		this.setFinalizedAt(entity.getFinalizedAt());
		this.setPublishedAt(entity.getDmp() != null ? entity.getDmp().getPublishedAt() : new Date());
		this.setProfile(entity.getProfile() != null ? new DatasetProfileOverviewModel().fromDataModel(entity.getProfile()) : null);
		if (entity.getDmp() != null && entity.getDmp().getGrant() != null) {
			this.setGrant(entity.getDmp().getGrant().getLabel());
		}
		this.setDmp( entity.getDmp() != null ? entity.getDmp().getLabel() : "");
		this.setDmpId(entity.getDmp() != null ? entity.getDmp().getId().toString() : "");
		return this;
	}


	@Override
	public RecentDatasetModel fromDataModel(Dataset entity) {
		return (RecentDatasetModel) this.fromEntity(entity);
	}

	@Override
	public Dataset toDataModel() throws Exception {
		return null;
	}

	@Override
	public String getHint() {
		return "recentDatasetModel";
	}
}
