package eu.eudat.models.data.external;

import java.util.List;
import java.util.Map;

public class FundersExternalSourcesModel extends ExternalListingItem<FundersExternalSourcesModel> {

	@Override
	public FundersExternalSourcesModel fromExternalItem(List<Map<String, String>> values) {
		for (Map<String, String> item : values) {
			ExternalSourcesItemModel model = new ExternalSourcesItemModel();
			model.setRemoteId((String)item.get("pid"));
			model.setUri((String)item.get("uri"));
			model.setName((String)item.get("name"));
			model.setDescription((String)item.get("description"));
			model.setSource((String)item.get("source"));
			model.setTag((String)item.get("tag"));
			model.setKey((String)item.get("key"));
			this.add(model);
		}
		return this;
	}
}
