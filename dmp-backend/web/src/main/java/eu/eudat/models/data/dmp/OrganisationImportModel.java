package eu.eudat.models.data.dmp;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "organisation")
public class OrganisationImportModel {
    private String source;
    private String name;
    private String id;
    private String reference;

    @XmlElement(name = "source")
    public String getOrganaisationSourceImport() {
        return source;
    }
    public void setOrganaisationSourceImport(String source) {
        this.source = source;
    }

    @XmlElement(name = "name")
    public String getOrganaisationNameImport() {
        return name;
    }
    public void setOrganaisationNameImport(String name) {
        this.name = name;
    }

    @XmlElement(name = "id")
    public String getOrganaisationIdImport() {
        return id;
    }
    public void setOrganaisationIdImport(String id) {
        this.id = id;
    }

    @XmlElement(name = "reference")
    public String getOrganaisationReferenceImport() { return reference; }
    public void setOrganaisationReferenceImport(String reference) { this.reference = reference; }
}
