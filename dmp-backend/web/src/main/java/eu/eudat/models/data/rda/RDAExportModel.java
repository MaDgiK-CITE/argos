package eu.eudat.models.data.rda;

import eu.eudat.data.entities.DMP;
import eu.eudat.data.entities.Dataset;
import eu.eudat.logic.managers.DatasetManager;
import eu.eudat.models.data.security.Principal;

import java.util.LinkedList;
import java.util.List;

public class RDAExportModel {
    private DmpRDAExportModel dmp;

    public DmpRDAExportModel getDmp() {
        return dmp;
    }
    public void setDmp(DmpRDAExportModel dmp) {
        this.dmp = dmp;
    }

    public RDAExportModel fromDataModel(DMP dmp, DatasetManager datasetManager, Principal principal) {
        this.dmp = new DmpRDAExportModel().fromDataModel(dmp, datasetManager, principal);
        return this;
    }
}
