package eu.eudat.cache;


import com.github.benmanes.caffeine.cache.Caffeine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.caffeine.CaffeineCache;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;


@Component
@EnableCaching
public class ResponsesCache {
    private static final Logger logger = LoggerFactory.getLogger(ResponsesCache.class);

    public static long HOW_MANY = 30;
    public static TimeUnit TIME_UNIT = TimeUnit.MINUTES;


    @Bean
    public CacheManager cacheManager() {
        logger.info("Loading ResponsesCache...");
        SimpleCacheManager simpleCacheManager = new SimpleCacheManager();
        List<CaffeineCache> caches = new ArrayList<CaffeineCache>();
        caches.add(new CaffeineCache("repositories", Caffeine.newBuilder().expireAfterAccess(HOW_MANY, TIME_UNIT).build()));
        caches.add(new CaffeineCache("pubrepos", Caffeine.newBuilder().expireAfterAccess(HOW_MANY, TIME_UNIT).build()));
        caches.add(new CaffeineCache("journals", Caffeine.newBuilder().expireAfterAccess(HOW_MANY, TIME_UNIT).build()));
        caches.add(new CaffeineCache("taxonomies", Caffeine.newBuilder().expireAfterAccess(HOW_MANY, TIME_UNIT).build()));
        caches.add(new CaffeineCache("publications", Caffeine.newBuilder().expireAfterAccess(HOW_MANY, TIME_UNIT).build()));
        caches.add(new CaffeineCache("grants", Caffeine.newBuilder().expireAfterAccess(HOW_MANY, TIME_UNIT).build()));
        caches.add(new CaffeineCache("projects", Caffeine.newBuilder().expireAfterAccess(HOW_MANY, TIME_UNIT).build()));
        caches.add(new CaffeineCache("funders", Caffeine.newBuilder().expireAfterAccess(HOW_MANY, TIME_UNIT).build()));
        caches.add(new CaffeineCache("organisations", Caffeine.newBuilder().expireAfterAccess(HOW_MANY, TIME_UNIT).build()));
        caches.add(new CaffeineCache("registries", Caffeine.newBuilder().expireAfterAccess(HOW_MANY, TIME_UNIT).build()));
        caches.add(new CaffeineCache("services", Caffeine.newBuilder().expireAfterAccess(HOW_MANY, TIME_UNIT).build()));
        caches.add(new CaffeineCache("tags", Caffeine.newBuilder().expireAfterAccess(HOW_MANY, TIME_UNIT).build()));
        caches.add(new CaffeineCache("researchers", Caffeine.newBuilder().expireAfterAccess(HOW_MANY, TIME_UNIT).build()));
        caches.add(new CaffeineCache("externalDatasets", Caffeine.newBuilder().expireAfterAccess(HOW_MANY, TIME_UNIT).build()));
        caches.add(new CaffeineCache("currencies", Caffeine.newBuilder().expireAfterAccess(HOW_MANY, TIME_UNIT).build()));
        caches.add(new CaffeineCache("licenses", Caffeine.newBuilder().expireAfterAccess(HOW_MANY, TIME_UNIT).build()));
        simpleCacheManager.setCaches(caches);
        logger.info("OK");
        return simpleCacheManager;
    }

    @Bean(name = "externalUrlsKeyGenerator")
    private KeyGenerator externalUrlsKeyGenerator() {
        return new ExternalUrlsKeyGenerator();
    }

}
