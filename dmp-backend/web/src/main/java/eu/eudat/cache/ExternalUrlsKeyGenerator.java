package eu.eudat.cache;

import eu.eudat.logic.proxy.config.ExternalUrlCriteria;
import org.springframework.cache.interceptor.KeyGenerator;

import java.lang.reflect.Method;

public class ExternalUrlsKeyGenerator implements KeyGenerator {
    @Override
    public Object generate(Object o, Method method, Object... params) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(o.getClass().getSimpleName()).append("_");
        stringBuffer.append(method.getName()).append("_");
        for (Object param: params) {
            if (param instanceof ExternalUrlCriteria) {
                ExternalUrlCriteria externalUrlCriteria = (ExternalUrlCriteria) param;
                stringBuffer.append(externalUrlCriteria);
            }
        }
        return stringBuffer.toString();
    }
}
