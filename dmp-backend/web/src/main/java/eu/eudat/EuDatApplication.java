package eu.eudat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication(scanBasePackages = {"eu.eudat", "eu.eudat.depositinterface"})
@EnableAsync
public class EuDatApplication extends SpringBootServletInitializer {
    private static final Logger logger = LoggerFactory.getLogger(EuDatApplication.class);

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(EuDatApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(EuDatApplication.class, args);
    }
}
