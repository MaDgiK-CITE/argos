package eu.eudat.logic.proxy.config;

import javax.xml.bind.annotation.XmlElement;

public class DataSearchConfiguration {
	private String type;
	private String queryParam;

	public String getType() {
		return type;
	}
	@XmlElement(name = "type")
	public void setType(String type) {
		this.type = type;
	}

	public String getQueryParam() {
		return queryParam;
	}
	@XmlElement(name = "queryparam")
	public void setQueryParam(String queryParam) {
		this.queryParam = queryParam;
	}
}
