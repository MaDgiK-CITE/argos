package eu.eudat.logic.utilities.interfaces;


import eu.eudat.models.data.entities.xmlmodels.datasetprofiledefinition.DatabaseViewStyleDefinition;

public interface ViewStyleDefinition<T extends DatabaseViewStyleDefinition> {
    T toDatabaseDefinition(T item);

    void fromDatabaseDefinition(T item);

}
