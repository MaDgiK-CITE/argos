package eu.eudat.logic.security.customproviders.B2Access;

import eu.eudat.logic.security.validators.b2access.helpers.B2AccessResponseToken;

/**
 * Created by ikalyvas on 2/22/2018.
 */
public interface B2AccessCustomProvider {
    B2AccessUser getUser(String accessToken);

    B2AccessResponseToken getAccessToken(String code, String redirectUri, String clientId, String clientSecret);
}
