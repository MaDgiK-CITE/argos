package eu.eudat.logic.builders.entity;

import eu.eudat.logic.builders.Builder;
import eu.eudat.data.entities.Credential;
import eu.eudat.data.entities.DMP;
import eu.eudat.data.entities.UserInfo;
import eu.eudat.data.entities.UserRole;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * Created by ikalyvas on 2/15/2018.
 */
public class UserInfoBuilder extends Builder<UserInfo> {

    private UUID id;

    private String email = null;

    private Short authorization_level; //0 admin, 1 user

    private Short usertype; // 0 internal, 1 external

    private Boolean verified_email = null;

    private String name = null;

    private Date created = null;

    private Date lastloggedin = null;

    private String additionalinfo;

    private Set<DMP> dmps;

    private Set<Credential> credentials = new HashSet<>();

    private Set<UserRole> userRoles = new HashSet<>();

    private Short userStatus;

    public UserInfoBuilder id(UUID id) {
        this.id = id;
        return this;
    }

    public UserInfoBuilder email(String email) {
        this.email = email;
        return this;
    }

    public UserInfoBuilder authorization_level(Short authorization_level) {
        this.authorization_level = authorization_level;
        return this;
    }

    public UserInfoBuilder usertype(Short usertype) {
        this.usertype = usertype;
        return this;
    }

    public UserInfoBuilder verified_email(Boolean verified_email) {
        this.verified_email = verified_email;
        return this;
    }

    public UserInfoBuilder name(String name) {
        this.name = name;
        return this;
    }

    public UserInfoBuilder created(Date created) {
        this.created = created;
        return this;
    }

    public UserInfoBuilder lastloggedin(Date lastloggedin) {
        this.lastloggedin = lastloggedin;
        return this;
    }

    public UserInfoBuilder additionalinfo(String additionalinfo) {
        this.additionalinfo = additionalinfo;
        return this;
    }

    public UserInfoBuilder dmps(Set<DMP> dmps) {
        this.dmps = dmps;
        return this;
    }

    public UserInfoBuilder credentials(Set<Credential> credentials) {
        this.credentials = credentials;
        return this;
    }

    public UserInfoBuilder userRoles(Set<UserRole> userRoles) {
        this.userRoles = userRoles;
        return this;
    }

    public UserInfoBuilder userStatus(Short userStatus) {
        this.userStatus = userStatus;
        return this;
    }

    @Override
    public UserInfo build() {
        UserInfo userInfo = new UserInfo();
        userInfo.setId(id);
        userInfo.setUsertype(usertype);
        userInfo.setAuthorization_level(authorization_level);
        userInfo.setLastloggedin(lastloggedin);
        userInfo.setCreated(created);
        userInfo.setEmail(email);
        userInfo.setName(name);
        userInfo.setAdditionalinfo(additionalinfo);
        userInfo.setUserRoles(userRoles);
        userInfo.setCredentials(credentials);
        userInfo.setDmps(dmps);
        userInfo.setVerified_email(verified_email);
        userInfo.setUserStatus(userStatus);
        return userInfo;
    }
}
