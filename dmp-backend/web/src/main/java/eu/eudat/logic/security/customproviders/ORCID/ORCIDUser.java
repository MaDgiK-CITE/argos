package eu.eudat.logic.security.customproviders.ORCID;

import java.util.Map;

public class ORCIDUser {
    private String orcidId;
    private String name;
    private String email;

    public String getOrcidId() {
        return orcidId;
    }
    public void setOrcidId(String orcidId) {
        this.orcidId = orcidId;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }


    public ORCIDUser getOrcidUser(Object data) {
        this.orcidId = (String) ((Map) data).get("orcidId");
        this.name = (String) ((Map) data).get("name");
        this.email = (String) ((Map) data).get("email");
        return this;
    }
}
