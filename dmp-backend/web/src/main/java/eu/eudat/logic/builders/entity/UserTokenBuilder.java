package eu.eudat.logic.builders.entity;

import eu.eudat.logic.builders.Builder;
import eu.eudat.data.entities.UserInfo;
import eu.eudat.data.entities.UserToken;

import java.util.Date;
import java.util.UUID;

/**
 * Created by ikalyvas on 2/15/2018.
 */
public class UserTokenBuilder extends Builder<UserToken> {

    private UUID token;

    private UserInfo user;

    private Date issuedAt;

    private Date expiresAt;

    public UserTokenBuilder token(UUID token) {
        this.token = token;
        return this;
    }

    public UserTokenBuilder user(UserInfo user) {
        this.user = user;
        return this;
    }

    public UserTokenBuilder issuedAt(Date issuedAt) {
        this.issuedAt = issuedAt;
        return this;
    }

    public UserTokenBuilder expiresAt(Date expiresAt) {
        this.expiresAt = expiresAt;
        return this;
    }

    @Override
    public UserToken build() {
        UserToken userToken = new UserToken();
        userToken.setExpiresAt(expiresAt);
        userToken.setToken(token);
        userToken.setUser(user);
        userToken.setIssuedAt(issuedAt);
        return userToken;
    }
}
