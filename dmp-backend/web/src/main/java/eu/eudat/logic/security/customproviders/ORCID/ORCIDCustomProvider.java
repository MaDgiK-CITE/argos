package eu.eudat.logic.security.customproviders.ORCID;

import eu.eudat.logic.security.validators.orcid.helpers.ORCIDResponseToken;

public interface ORCIDCustomProvider {
    ORCIDResponseToken getAccessToken(String code, String redirectUri, String clientId, String clientSecret);
}
