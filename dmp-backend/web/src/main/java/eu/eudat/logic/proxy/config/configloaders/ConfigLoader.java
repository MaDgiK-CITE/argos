package eu.eudat.logic.proxy.config.configloaders;

import eu.eudat.logic.proxy.config.ExternalUrls;
import eu.eudat.logic.proxy.config.Semantic;
import eu.eudat.logic.security.customproviders.ConfigurableProvider.entities.ConfigurableProviders;
import eu.eudat.models.data.pid.PidLinks;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import java.util.List;
import java.util.Map;

public interface ConfigLoader {
    ExternalUrls getExternalUrls();
    List<Semantic> getSemantics();
    XWPFDocument getDocument();
    XWPFDocument getDatasetDocument();
    ConfigurableProviders getConfigurableProviders();
    PidLinks getPidLinks();
    Map<String, String> getKeyToSourceMap();
}
