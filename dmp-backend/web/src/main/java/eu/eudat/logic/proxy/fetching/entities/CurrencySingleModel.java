package eu.eudat.logic.proxy.fetching.entities;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "CcyNtry")
public class CurrencySingleModel {
	private String country;
	private String currency;
	private String code;
	private Integer numericCode;
	private Integer unit;

	@XmlElement(name = "CtryNm")
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}

	@XmlElement(name = "CcyNm")
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	@XmlElement(name = "Ccy")
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}

	@XmlElement(name = "CcyNbr")
	public Integer getNumericCode() {
		return numericCode;
	}
	public void setNumericCode(Integer numericCode) {
		this.numericCode = numericCode;
	}

	@XmlElement(name = "CcyMnrUnts")
	public Integer getUnit() {
		return unit;
	}
	public void setUnit(Integer unit) {
		this.unit = unit;
	}
}
