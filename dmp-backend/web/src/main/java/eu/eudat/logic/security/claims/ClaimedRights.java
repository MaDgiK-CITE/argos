package eu.eudat.logic.security.claims;

import eu.eudat.types.Rights;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by ikalyvas on 2/8/2018.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface ClaimedRights {
    Rights[] claims() default {};
}
