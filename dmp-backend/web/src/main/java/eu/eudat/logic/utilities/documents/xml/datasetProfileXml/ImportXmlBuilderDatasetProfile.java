package eu.eudat.logic.utilities.documents.xml.datasetProfileXml;
import eu.eudat.logic.utilities.documents.xml.datasetProfileXml.datasetProfileModel.DatasetProfile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.*;

public class ImportXmlBuilderDatasetProfile {
    private static final Logger logger = LoggerFactory.getLogger(ImportXmlBuilderDatasetProfile.class);

    public DatasetProfile build(File xmlFile) throws IOException {
        DatasetProfile datasetProfile = new DatasetProfile();
        JAXBContext jaxbContext = null;
        try {
            jaxbContext = JAXBContext.newInstance(DatasetProfile.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            datasetProfile = (DatasetProfile) unmarshaller.unmarshal(xmlFile);
        } catch (JAXBException e) {
            logger.error(e.getMessage(), e);
        }

        return datasetProfile;
    }


}
