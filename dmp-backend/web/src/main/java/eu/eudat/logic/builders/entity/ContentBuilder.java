package eu.eudat.logic.builders.entity;

import eu.eudat.logic.builders.Builder;
import eu.eudat.data.entities.Content;

import java.util.UUID;

/**
 * Created by ikalyvas on 3/16/2018.
 */
public class ContentBuilder extends Builder<Content> {

    private UUID id;

    private String label;

    private String extension;

    private Integer parentType;

    private String uri;

    private Integer locationType;

    public UUID getId() {
        return id;
    }

    public ContentBuilder id(UUID id) {
        this.id = id;
        return this;
    }

    public String getLabel() {
        return label;
    }

    public ContentBuilder label(String label) {
        this.label = label;
        return this;
    }

    public String getExtension() {
        return extension;
    }

    public ContentBuilder extension(String extension) {
        this.extension = extension;
        return this;
    }

    public Integer getParentType() {
        return parentType;
    }

    public ContentBuilder parentType(Integer parentType) {
        this.parentType = parentType;
        return this;
    }

    public String getUri() {
        return uri;
    }

    public ContentBuilder uri(String uri) {
        this.uri = uri;
        return this;
    }

    public Integer getLocationType() {
        return locationType;
    }

    public ContentBuilder locationType(Integer locationType) {
        this.locationType = locationType;
        return this;
    }

    @Override
    public Content build() {
        Content content = new Content();
        content.setExtension(extension);
        content.setId(id);
        content.setLabel(label);
        content.setParentType(parentType);
        content.setLocationType(locationType);
        content.setUri(uri);
        return content;
    }
}
