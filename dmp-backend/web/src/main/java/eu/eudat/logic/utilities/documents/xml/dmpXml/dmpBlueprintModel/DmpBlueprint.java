package eu.eudat.logic.utilities.documents.xml.dmpXml.dmpBlueprintModel;

import eu.eudat.data.entities.DMPProfile;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@XmlRootElement(name = "root")
public class DmpBlueprint {

    private DmpBlueprintDefinition dmpBlueprintDefinition;

    @XmlElement(name = "definition")
    public DmpBlueprintDefinition getDmpBlueprintDefinition() {
        return dmpBlueprintDefinition;
    }

    public void setDmpBlueprintDefinition(DmpBlueprintDefinition dmpBlueprintDefinition) {
        this.dmpBlueprintDefinition = dmpBlueprintDefinition;
    }

    public eu.eudat.models.data.listingmodels.DataManagementPlanBlueprintListingModel toDmpProfileCompositeModel(String label) {
        eu.eudat.models.data.listingmodels.DataManagementPlanBlueprintListingModel dmpProfileModel = new eu.eudat.models.data.listingmodels.DataManagementPlanBlueprintListingModel();
        dmpProfileModel.setLabel(label);
        dmpProfileModel.setStatus(DMPProfile.Status.SAVED.getValue());
        dmpProfileModel.setCreated(new Date());
        dmpProfileModel.setModified(new Date());
        if (this.dmpBlueprintDefinition != null) {
            dmpProfileModel.setDefinition(this.dmpBlueprintDefinition.toDmpBlueprintCompositeModel());
        }
        return dmpProfileModel;
    }

}
