package eu.eudat.logic.managers;

import eu.eudat.logic.proxy.fetching.LocalFetcher;
import eu.eudat.logic.utilities.helpers.StreamDistinctBy;
import eu.eudat.models.data.local.LocalFetchModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class LocalFetchManager {
	private LocalFetcher localFetcher;

	@Autowired
	public LocalFetchManager(LocalFetcher localFetcher) {
		this.localFetcher = localFetcher;
	}

	public List<LocalFetchModel> getCurrency(String query) throws Exception {
		List<Map<String, String>> data = localFetcher.retrieveCurrency();
		List<LocalFetchModel> result = data.stream().map(entry -> new LocalFetchModel(entry.get("name"), entry.get("value"))).collect(Collectors.toList());
		result = result.stream().filter(localFetchModel -> localFetchModel.getValue() != null).filter(StreamDistinctBy.distinctByKey(LocalFetchModel::getValue)).filter(localFetchModel -> localFetchModel.getName().toLowerCase().contains(query.toLowerCase())).collect(Collectors.toList());
		return result;
	}
}
