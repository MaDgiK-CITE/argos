package eu.eudat.logic.utilities.documents.xml.dmpXml.dmpBlueprintModel;

import eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition.types.FieldCategory;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.UUID;

@XmlRootElement(name = "descriptionTemplate")
public class DescriptionTemplate {

    private String id;
    private String descriptionTemplateId;
    private String label;
    private Integer minMultiplicity;
    private Integer maxMultiplicity;

    @XmlAttribute(name = "id")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @XmlAttribute(name = "descriptionTemplateId")
    public String getDescriptionTemplateId() {
        return descriptionTemplateId;
    }

    public void setDescriptionTemplateId(String descriptionTemplateId) {
        this.descriptionTemplateId = descriptionTemplateId;
    }

    @XmlAttribute(name = "label")
    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @XmlAttribute(name = "minMultiplicity")
    public Integer getMinMultiplicity() {
        return minMultiplicity;
    }

    public void setMinMultiplicity(Integer minMultiplicity) {
        this.minMultiplicity = minMultiplicity;
    }

    @XmlAttribute(name = "maxMultiplicity")
    public Integer getMaxMultiplicity() {
        return maxMultiplicity;
    }

    public void setMaxMultiplicity(Integer maxMultiplicity) {
        this.maxMultiplicity = maxMultiplicity;
    }

    public eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition.DescriptionTemplate toDmpBlueprintCompositeModel() {
        eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition.DescriptionTemplate descriptionTemplate = new eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition.DescriptionTemplate();
        descriptionTemplate.setId(UUID.fromString(this.id));
        descriptionTemplate.setDescriptionTemplateId(UUID.fromString(this.descriptionTemplateId));
        descriptionTemplate.setLabel(this.label);
        descriptionTemplate.setMinMultiplicity(this.minMultiplicity);
        descriptionTemplate.setMaxMultiplicity(this.maxMultiplicity);
        return descriptionTemplate;
    }

}
