package eu.eudat.logic.services.operations;

import eu.eudat.elastic.repository.DatasetRepository;
import eu.eudat.elastic.repository.DmpRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("elasticRepository")
public class ElasticRepositoryImpl implements ElasticRepository {

	private final DatasetRepository datasetRepository;
	private final DmpRepository dmpRepository;

	@Autowired
	public ElasticRepositoryImpl(DatasetRepository datasetRepository, DmpRepository dmpRepository) {
		this.datasetRepository = datasetRepository;
		this.dmpRepository = dmpRepository;
	}

	@Override
	public DatasetRepository getDatasetRepository() {
		return datasetRepository;
	}

	@Override
	public DmpRepository getDmpRepository() {
		return dmpRepository;
	}
}
