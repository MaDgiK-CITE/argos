package eu.eudat.logic.services.operations.authentication;

import eu.eudat.exceptions.security.NullEmailException;
import eu.eudat.models.data.login.Credentials;
import eu.eudat.models.data.loginprovider.LoginProviderUser;
import eu.eudat.models.data.security.Principal;

import java.util.UUID;

/**
 * Created by ikalyvas on 3/1/2018.
 */
public interface AuthenticationService {

    Principal Touch(LoginProviderUser profile) throws NullEmailException;

    Principal Touch(Credentials credentials) throws NullEmailException;
    
    void Logout(UUID token);

    Principal Touch(UUID token) throws NullEmailException;
}
