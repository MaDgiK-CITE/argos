package eu.eudat.logic.services.forms;

import java.util.List;

/**
 * Created by ikalyvas on 3/5/2018.
 */
public class VisibilityRule {
    private String visibilityRuleTargetId;
    private List<VisibilityRuleSource> visibilityRuleSources;

    public String getVisibilityRuleTargetId() {
        return visibilityRuleTargetId;
    }

    public void setVisibilityRuleTargetId(String visibilityRuleTargetId) {
        this.visibilityRuleTargetId = visibilityRuleTargetId;
    }

    public List<VisibilityRuleSource> getVisibilityRuleSources() {
        return visibilityRuleSources;
    }

    public void setVisibilityRuleSources(List<VisibilityRuleSource> visibilityRuleSources) {
        this.visibilityRuleSources = visibilityRuleSources;
    }
}
