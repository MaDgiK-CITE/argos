package eu.eudat.logic.managers;

import eu.eudat.data.query.items.item.funder.FunderCriteriaRequest;
import eu.eudat.logic.builders.model.models.FunderBuilder;
import eu.eudat.logic.proxy.config.ExternalUrlCriteria;
import eu.eudat.logic.proxy.config.configloaders.ConfigLoader;
import eu.eudat.logic.proxy.config.exceptions.HugeResultSet;
import eu.eudat.logic.proxy.config.exceptions.NoURLFound;
import eu.eudat.logic.proxy.fetching.RemoteFetcher;
import eu.eudat.logic.services.ApiContext;
import eu.eudat.logic.utilities.helpers.ListHelper;
import eu.eudat.models.data.external.ExternalSourcesItemModel;
import eu.eudat.models.data.external.FundersExternalSourcesModel;
import eu.eudat.models.data.funder.Funder;
import eu.eudat.models.data.security.Principal;
import eu.eudat.queryable.QueryableList;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class FunderManager {

	private ApiContext apiContext;
	private RemoteFetcher remoteFetcher;
	private ListHelper listHelper;

	public FunderManager(ApiContext apiContext, RemoteFetcher remoteFetcher, ListHelper listHelper) {
		this.apiContext = apiContext;
		this.remoteFetcher = remoteFetcher;
		this.listHelper = listHelper;
	}

	public List<Funder> getCriteriaWithExternal(FunderCriteriaRequest funderCriteria, Principal principal) throws HugeResultSet, NoURLFound {
		eu.eudat.data.entities.UserInfo userInfo = new eu.eudat.data.entities.UserInfo();
		userInfo.setId(principal.getId());
		funderCriteria.getCriteria().setReference("dmp:");
		QueryableList<eu.eudat.data.entities.Funder> items = apiContext.getOperationsContext().getDatabaseRepository().getFunderDao().getWithCritetia(funderCriteria.getCriteria());
		QueryableList<eu.eudat.data.entities.Funder> authItems = apiContext.getOperationsContext().getDatabaseRepository().getFunderDao().getAuthenticated(items, userInfo);
		List<Funder> funders = authItems.select(item -> new eu.eudat.models.data.funder.Funder().fromDataModel(item));
		ExternalUrlCriteria externalUrlCriteria = new ExternalUrlCriteria(funderCriteria.getCriteria().getLike());
		List<Map<String, String>> remoteRepos = remoteFetcher.getFunders(externalUrlCriteria);
		FundersExternalSourcesModel fundersExternalSourcesModel = new FundersExternalSourcesModel().fromExternalItem(remoteRepos);
		for (ExternalSourcesItemModel externalListingItem : fundersExternalSourcesModel) {
			eu.eudat.models.data.funder.Funder funder = apiContext.getOperationsContext().getBuilderFactory().getBuilder(FunderBuilder.class)
					.reference(externalListingItem.getRemoteId()).label(externalListingItem.getName())
					.status(eu.eudat.data.entities.Funder.Status.fromInteger(0))
					.key(externalListingItem.getKey())
					.source(externalListingItem.getTag())
					.build();
			if (externalListingItem.getSource() != null) {
				funder.setSource(externalListingItem.getSource());
			} else {
				funder.setSource(externalListingItem.getTag());
			}

			funders.add(funder);
		}
		funders.sort(Comparator.comparing(Funder::getLabel));
		funders = funders.stream().filter(listHelper.distinctByKey(Funder::getLabel)).collect(Collectors.toList());
		return funders;
	}
}
