package eu.eudat.logic.utilities.documents.xml.datasetProfileXml.datasetProfileModel;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "multiplicity")
public class Multiplicity {
    private int max;
    private int min;
    private String placeholder;
    private boolean tableView;

    @XmlAttribute(name = "max")
    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    @XmlAttribute(name = "min")
    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    @XmlAttribute(name = "placeholder")
    public String getPlaceholder() {
        return placeholder;
    }

    public void setPlaceholder(String placeholder) {
        this.placeholder = placeholder;
    }

    @XmlAttribute(name = "tableView")
    public boolean getTableView() {
        return tableView;
    }

    public void setTableView(boolean tableView) {
        this.tableView = tableView;
    }

    public eu.eudat.models.data.components.commons.Multiplicity toAdminCompositeModelSection() {
        eu.eudat.models.data.components.commons.Multiplicity multiplicityEntity = new eu.eudat.models.data.components.commons.Multiplicity();
        multiplicityEntity.setMax(max);
        multiplicityEntity.setMin(min);
        multiplicityEntity.setPlaceholder(placeholder);
        multiplicityEntity.setTableView(tableView);
        return multiplicityEntity;
    }
}