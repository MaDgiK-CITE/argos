package eu.eudat.logic.services.utilities;


import eu.eudat.data.dao.entities.DMPDao;
import eu.eudat.data.dao.entities.InvitationDao;
import eu.eudat.data.entities.DMP;
import eu.eudat.data.entities.Invitation;

import javax.mail.MessagingException;
import java.util.List;
import java.util.concurrent.CompletableFuture;


public interface InvitationService {
    void assignToDmp(DMPDao dmpDao, List<eu.eudat.data.entities.UserDMP> users, DMP dmp);

    void assignToDmp(DMPDao dmpDao, eu.eudat.data.entities.UserDMP user, DMP dmp);

    void createInvitations(InvitationDao invitationDao, MailService mailService, List<eu.eudat.data.entities.UserInfo> users, DMP dmp, Integer role, eu.eudat.data.entities.UserInfo creator) throws MessagingException;

    CompletableFuture sendInvitationAsync(DMP dmp, Invitation invitation, String recipient, MailService mailService, Integer role) throws MessagingException;
}
