package eu.eudat.logic.managers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.eudat.logic.proxy.config.ExternalUrlCriteria;
import eu.eudat.logic.proxy.config.exceptions.HugeResultSet;
import eu.eudat.logic.proxy.config.exceptions.NoURLFound;
import eu.eudat.logic.proxy.fetching.RemoteFetcher;
import eu.eudat.models.data.security.Principal;

@Component
public class ValidationManager {
	
	private RemoteFetcher remoteFetcher;

	@Autowired
	public ValidationManager(RemoteFetcher remoteFetcher) {
		super();
		this.remoteFetcher = remoteFetcher;
	}
	
	public Boolean validateIdentifier(String identifier, String type, Principal principal) throws NoURLFound, HugeResultSet {
		ExternalUrlCriteria externalUrlCriteria = new ExternalUrlCriteria(identifier);
		Integer count = this.remoteFetcher.findEntries(externalUrlCriteria, type);
		return principal != null && count > 0;
	}
	
	

}
