package eu.eudat.logic.services;

import eu.eudat.logic.services.helpers.HelpersService;
import eu.eudat.logic.services.operations.OperationsContext;
import eu.eudat.logic.services.utilities.UtilitiesService;

public interface ApiContext {

    HelpersService getHelpersService();

    OperationsContext getOperationsContext();

    UtilitiesService getUtilitiesService();
}
