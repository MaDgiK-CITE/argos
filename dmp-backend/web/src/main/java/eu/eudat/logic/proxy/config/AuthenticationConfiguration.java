package eu.eudat.logic.proxy.config;

import javax.xml.bind.annotation.XmlElement;

public class AuthenticationConfiguration {

    private String authUrl;
    private String authMethod = "GET";
    private String authTokenPath;
    private String authRequestBody;
    private String type;

    public String getAuthUrl() {
        return authUrl;
    }

    @XmlElement(name = "authUrl")
    public void setAuthUrl(String authUrl) {
        this.authUrl = authUrl;
    }

    public String getAuthMethod() {
        return authMethod;
    }

    @XmlElement(name = "authUrlMethod")
    public void setAuthMethod(String authMethod) {
        this.authMethod = authMethod;
    }

    public String getAuthTokenPath() {
        return authTokenPath;
    }

    @XmlElement(name = "authTokenJpath")
    public void setAuthTokenPath(String authTokenPath) {
        this.authTokenPath = authTokenPath;
    }

    public String getAuthRequestBody() {
        return authRequestBody;
    }

    @XmlElement(name = "authUrlBody")
    public void setAuthRequestBody(String authRequestBody) {
        this.authRequestBody = authRequestBody;
    }

    public String getType() {
        return type;
    }

    @XmlElement(name = "authType")
    public void setType(String type) {
        this.type = type;
    }
}
