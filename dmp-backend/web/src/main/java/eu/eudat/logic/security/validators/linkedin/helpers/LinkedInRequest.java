package eu.eudat.logic.security.validators.linkedin.helpers;

public class LinkedInRequest {
	private String code;

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
}
