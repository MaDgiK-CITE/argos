package eu.eudat.logic.security.validators;


public interface TokenValidatorFactory {
    TokenValidator getProvider(TokenValidatorFactoryImpl.LoginProvider provider);
}
