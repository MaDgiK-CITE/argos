package eu.eudat.logic.proxy.config;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.List;

public class UrlConfiguration {

    private String key;
    private String label;
    private Integer ordinal;
    private String url;
    private boolean dataInListing;
    private DataUrlConfiguration data;
    private String type;
    private String paginationPath;
    private String contentType;
    private String funderQuery;
    private String firstpage;
    private String requestType = "GET";
    private String requestBody = "";
    private String filterType = "remote";
    private AuthenticationConfiguration auth;

    private List<QueryConfig> queries;

    public String getKey() {
        return key;
    }
    @XmlElement(name = "key")
    public void setKey(String key) {
        this.key = key;
    }

    public String getLabel() {
        return label;
    }
    @XmlElement(name = "label")
    public void setLabel(String label) {
        this.label = label;
    }

    public String getUrl() {
        return url;
    }
    @XmlElement(name = "url")
    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isDataInListing() {
        return dataInListing;
    }
    @XmlElement(name = "dataInListing")
    public void setDataInListing(boolean dataInListing) {
        this.dataInListing = dataInListing;
    }

    public Integer getOrdinal() {
        return ordinal;
    }
    @XmlElement(name = "ordinal")
    public void setOrdinal(Integer ordinal) {
        this.ordinal = ordinal;
    }

    public DataUrlConfiguration getData() {
        return data;
    }
    @XmlElement(name = "data")
    public void setData(DataUrlConfiguration data) {
        this.data = data;
    }

    public String getPaginationPath() {
        return paginationPath;
    }
    @XmlElement(name = "paginationpath")
    public void setPaginationPath(String paginationPath) {
        this.paginationPath = paginationPath;
    }

    public String getType() {
        return type;
    }
    @XmlElement(name = "type")
    public void setType(String type) {
        this.type = type;
    }

    public String getContentType() {
        return contentType;
    }
    @XmlElement(name = "contenttype")
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getFunderQuery() {
        return funderQuery;
    }
    @XmlElement(name = "funderQuery")
    public void setFunderQuery(String funderQuery) {
        this.funderQuery = funderQuery;
    }

    public String getFirstpage() {
        return firstpage;
    }
    @XmlElement(name = "firstPage")
    public void setFirstpage(String firstpage) {
        this.firstpage = firstpage;
    }
    
    public String getRequestType() {
        return requestType;
    }
    @XmlElement(name = "request")
    public void setRequestType(String requestType) {
        this.requestType = requestType != null ? requestType : "GET";
    }
	public String getRequestBody() {
		return requestBody;
	}
	@XmlElement(name = "requestBody")
	public void setRequestBody(String requestBody) {
		this.requestBody = requestBody != null ? requestBody : "";
	}
    public String getFilterType() {
        return filterType;
    }
    @XmlElement(name = "filterType")
    public void setFilterType(String filterType) {
        this.filterType = filterType;
    }

    public List<QueryConfig> getQueries() {
        return queries;
    }

    @XmlElementWrapper
    @XmlElement(name = "query")
    public void setQueries(List<QueryConfig> queries) {
        this.queries = queries;
    }

    public AuthenticationConfiguration getAuth() {
        return auth;
    }

    @XmlElement(name="authentication")
    public void setAuth(AuthenticationConfiguration auth) {
        this.auth = auth;
    }
}
