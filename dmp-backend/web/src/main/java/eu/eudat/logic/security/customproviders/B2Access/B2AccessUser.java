package eu.eudat.logic.security.customproviders.B2Access;

/**
 * Created by ikalyvas on 2/22/2018.
 */
public class B2AccessUser {
    private String id;
    private String name;
    private String email;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
