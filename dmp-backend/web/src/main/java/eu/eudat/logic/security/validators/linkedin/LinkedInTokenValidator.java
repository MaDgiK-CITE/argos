package eu.eudat.logic.security.validators.linkedin;

import eu.eudat.exceptions.security.UnauthorisedException;
import eu.eudat.logic.security.customproviders.LinkedIn.LinkedInCustomProvider;
import eu.eudat.logic.security.customproviders.LinkedIn.LinkedInUser;
import eu.eudat.logic.security.validators.TokenValidator;
import eu.eudat.logic.security.validators.linkedin.helpers.LinkedInRequest;
import eu.eudat.logic.security.validators.linkedin.helpers.LinkedInResponseToken;
import eu.eudat.logic.services.operations.authentication.AuthenticationService;
import eu.eudat.models.data.login.LoginInfo;
import eu.eudat.models.data.loginprovider.LoginProviderUser;
import eu.eudat.models.data.security.Principal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;


@Component("linkedInTokenValidator")
public class LinkedInTokenValidator implements TokenValidator {

    private Environment environment;
    private AuthenticationService nonVerifiedUserAuthenticationService;
    private LinkedInCustomProvider linkedInCustomProvider;

    @Autowired
    public LinkedInTokenValidator(Environment environment, AuthenticationService nonVerifiedUserAuthenticationService, LinkedInCustomProvider linkedInCustomProvider) {
        this.environment = environment;
        this.nonVerifiedUserAuthenticationService = nonVerifiedUserAuthenticationService;
        this.linkedInCustomProvider = linkedInCustomProvider;
    }

    @Override
    public Principal validateToken(LoginInfo credentials) {
        /*AccessGrant accessGrant = this.linkedInServiceProvider.getOAuthOperations().exchangeForAccess(credentials.getTicket(), this.environment.getProperty("linkedin.login.redirect_uri"), null);
        LinkedIn linkedInService = this.linkedInServiceProvider.getApi(accessGrant.getAccessToken());
        LinkedInProfile linkedInProfile = linkedInService.profileOperations().getUserProfile();
        LoginProviderUser user = new LoginProviderUser();

        if (linkedInProfile.getEmailAddress() == null)
            throw new UnauthorisedException("Cannot login user.LinkedIn account did not provide email");
        user.setEmail(linkedInProfile.getEmailAddress());
        user.setId(linkedInProfile.getId());
        user.setIsVerified(true); //TODO
        user.setAvatarUrl(linkedInProfile.getProfilePictureUrl());
        user.setName(linkedInProfile.getFirstName() + " " + linkedInProfile.getLastName());
        user.setProvider(TokenValidatorFactoryImpl.LoginProvider.LINKEDIN);
        user.setSecret(accessGrant.getAccessToken());*/

        LinkedInUser linkedInUser = this.linkedInCustomProvider.getUser(credentials.getTicket());
        if (linkedInUser.getEmail() == null)
            throw new UnauthorisedException("Cannot login user.LinkedIn account did not provide email");
        LoginProviderUser user = new LoginProviderUser();
        user.setId(linkedInUser.getId());
        user.setName(linkedInUser.getName());
        user.setEmail(linkedInUser.getEmail());
        user.setProvider(credentials.getProvider());
        user.setSecret(credentials.getTicket());

        return this.nonVerifiedUserAuthenticationService.Touch(user);
    }

    public LinkedInResponseToken getAccessToken(LinkedInRequest linkedInRequest) {
        return this.linkedInCustomProvider.getAccessToken(
                linkedInRequest.getCode(), this.environment.getProperty("linkedin.login.redirect_uri"),
                this.environment.getProperty("linkedin.login.clientId"), this.environment.getProperty("linkedin.login.clientSecret"));
    }
}
