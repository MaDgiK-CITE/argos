package eu.eudat.logic.utilities.documents.xml.datasetProfileXml.datasetProfileModel;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.LinkedList;
import java.util.List;

@XmlRootElement(name = "field-Sets")
public class FieldSets {

    List<FieldSet> fieldSet;

    @XmlElement(name = "field-Set")
    public List<FieldSet> getFieldSet() {
        return fieldSet;
    }

    public void setFieldSet(List<FieldSet> fieldSet) {
        this.fieldSet = fieldSet;
    }

    public  List<eu.eudat.models.data.admin.components.datasetprofile.FieldSet> toAdminCompositeModelSection(){
        List<eu.eudat.models.data.admin.components.datasetprofile.FieldSet> fieldSetEntity = new LinkedList<>();
        if(this.fieldSet!=null)
        for (FieldSet xmlFieldSet:this.fieldSet){
            fieldSetEntity.add(xmlFieldSet.toAdminCompositeModelSection());
        }
        return fieldSetEntity;
    }
}
