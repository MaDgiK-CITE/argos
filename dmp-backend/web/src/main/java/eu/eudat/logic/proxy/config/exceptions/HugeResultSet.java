package eu.eudat.logic.proxy.config.exceptions;

public class HugeResultSet extends Exception {

    private static final long serialVersionUID = -6961447213733280563L;


    public HugeResultSet(String message) {
        super(message);
    }


}
