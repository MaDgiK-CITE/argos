package eu.eudat.logic.security.validators.b2access.helpers;

/**
 * Created by ikalyvas on 2/22/2018.
 */
public class B2AccessResponseToken {
    private String accessToken;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
