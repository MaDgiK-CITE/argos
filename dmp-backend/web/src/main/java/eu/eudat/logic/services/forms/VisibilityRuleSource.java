package eu.eudat.logic.services.forms;

/**
 * Created by ikalyvas on 3/5/2018.
 */
public class VisibilityRuleSource {
    private String visibilityRuleSourceId;
    private String visibilityRuleSourceValue;

    public String getVisibilityRuleSourceId() {
        return visibilityRuleSourceId;
    }

    public void setVisibilityRuleSourceId(String visibilityRuleSourceId) {
        this.visibilityRuleSourceId = visibilityRuleSourceId;
    }

    public String getVisibilityRuleSourceValue() {
        return visibilityRuleSourceValue;
    }

    public void setVisibilityRuleSourceValue(String visibilityRuleSourceValue) {
        this.visibilityRuleSourceValue = visibilityRuleSourceValue;
    }
}
