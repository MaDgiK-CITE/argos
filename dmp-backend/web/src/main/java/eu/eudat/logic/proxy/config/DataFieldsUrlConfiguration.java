package eu.eudat.logic.proxy.config;

import javax.xml.bind.annotation.XmlElement;

/**
 * Created by ikalyvas on 6/29/2018.
 */
public class DataFieldsUrlConfiguration {
    private String id;
    private String name;
    private String pid;
    private String pidTypeField;
    private String uri;
    private String description;
    private String source;
    private String count;
    private String path;
    private String host;
    private String types;
    private String firstName;
    private String lastName;

    public String getId() {
        return id;
    }

    @XmlElement(name = "id")
    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    @XmlElement(name = "name")
    public void setName(String name) {
        this.name = name;
    }

    public String getPid() {
        return pid;
    }

    @XmlElement(name = "pid")
    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getPidTypeField() {
        return pidTypeField;
    }

    @XmlElement(name = "pidTypeField")
    public void setPidTypeField(String pidTypeField) {
        this.pidTypeField = pidTypeField;
    }

    public String getUri() {
        return uri;
    }

    @XmlElement(name = "uri")
    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getDescription() {
        return description;
    }

    @XmlElement(name = "description")
    public void setDescription(String description) {
        this.description = description;
    }

    public String getSource() {
        return source;
    }

    @XmlElement(name = "source")
    public void setSource(String source) {
        this.source = source;
    }

    public String getCount() {
        return count;
    }

    @XmlElement(name = "count")
    public void setCount(String count) {
        this.count = count;
    }

    public String getPath() {
        return path;
    }
    @XmlElement(name = "path")
    public void setPath(String path) {
        this.path = path;
    }

    public String getHost() {
        return host;
    }
    @XmlElement(name = "host")
    public void setHost(String host) {
        this.host = host;
    }

    @XmlElement(name = "types")
    public String getTypes() {
        return types;
    }
    public void setTypes(String types) {
        this.types = types;
    }

    @XmlElement(name = "firstName")
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @XmlElement(name = "lastName")
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
