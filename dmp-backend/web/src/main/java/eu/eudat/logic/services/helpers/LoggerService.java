package eu.eudat.logic.services.helpers;

import eu.eudat.types.WarningLevel;

/**
 * Created by ikalyvas on 3/1/2018.
 */
public interface LoggerService {
    void log(String message);

    void log(String message, WarningLevel level);
}
