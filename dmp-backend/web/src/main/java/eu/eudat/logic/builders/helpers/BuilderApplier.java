package eu.eudat.logic.builders.helpers;

/**
 * Created by ikalyvas on 3/15/2018.
 */
public interface BuilderApplier<T> {
    void apply(T builder);
}
