package eu.eudat.logic.security.validators.configurableProvider.helpers;

public class ConfigurableProviderRequest {
	private String code;
	private String configurableLoginId;

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}

	public String getConfigurableLoginId() {
		return configurableLoginId;
	}
	public void setConfigurableLoginId(String configurableLoginId) {
		this.configurableLoginId = configurableLoginId;
	}
}
