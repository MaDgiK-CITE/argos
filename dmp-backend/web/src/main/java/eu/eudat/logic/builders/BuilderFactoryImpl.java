package eu.eudat.logic.builders;

import eu.eudat.logic.builders.entity.*;
import eu.eudat.logic.builders.model.criteria.DataRepositoryCriteriaBuilder;
import eu.eudat.logic.builders.model.criteria.ExternalDatasetCriteriaBuilder;
import eu.eudat.logic.builders.model.criteria.RegistryCriteriaBuilder;
import eu.eudat.logic.builders.model.models.*;
import org.springframework.stereotype.Service;


@Service("builderFactory")
public class BuilderFactoryImpl implements BuilderFactory {

    public <T extends Builder> T getBuilder(Class<T> tClass) {
        if (tClass.equals(CredentialBuilder.class)) return (T) new CredentialBuilder();
        if (tClass.equals(DataRepositoryCriteriaBuilder.class)) return (T) new DataRepositoryCriteriaBuilder();
        if (tClass.equals(DatasetProfileBuilder.class)) return (T) new DatasetProfileBuilder();
        if (tClass.equals(DataTableDataBuilder.class)) return (T) new DataTableDataBuilder<>();
        if (tClass.equals(PrincipalBuilder.class)) return (T) new PrincipalBuilder();
        if (tClass.equals(GrantBuilder.class)) return (T) new GrantBuilder();
        if (tClass.equals(ProjectBuilder.class)) return (T) new ProjectBuilder();
        if (tClass.equals(FunderBuilder.class)) return (T) new FunderBuilder();
        if (tClass.equals(RegistryCriteriaBuilder.class)) return (T) new RegistryCriteriaBuilder();
        if (tClass.equals(UserInfoBuilder.class)) return (T) new UserInfoBuilder();
        if (tClass.equals(UserRoleBuilder.class)) return (T) new UserRoleBuilder();
        if (tClass.equals(UserTokenBuilder.class)) return (T) new UserTokenBuilder();
        if (tClass.equals(ResearcherBuilder.class)) return (T) new ResearcherBuilder();
        if (tClass.equals(ExternalDatasetCriteriaBuilder.class)) return (T) new ExternalDatasetCriteriaBuilder();
        if (tClass.equals(RecentActivityDataBuilder.class)) return (T) new RecentActivityDataBuilder();
        if (tClass.equals(OrganisationBuilder.class)) return (T) new OrganisationBuilder();

        return null;
    }
}
