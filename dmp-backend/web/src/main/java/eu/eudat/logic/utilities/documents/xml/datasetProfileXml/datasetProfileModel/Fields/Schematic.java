package eu.eudat.logic.utilities.documents.xml.datasetProfileXml.datasetProfileModel.Fields;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

@XmlRootElement(name = "schematic")
public class Schematic {

    private String schematic;

    @XmlValue
    public String getSchematic() {
        return schematic;
    }
    public void setSchematic(String schematic) {
        this.schematic = schematic;
    }

}
