package eu.eudat.logic.utilities.documents.xml.dmpXml.dmpBlueprintModel;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "descriptionTemplates")
public class DescriptionTemplates {

    private List<DescriptionTemplate> descriptionTemplates;

    @XmlElement(name = "descriptionTemplate")
    public List<DescriptionTemplate> getDescriptionTemplates() {
        return descriptionTemplates;
    }
    public void setDescriptionTemplates(List<DescriptionTemplate> descriptionTemplates) {
        this.descriptionTemplates = descriptionTemplates;
    }

}
