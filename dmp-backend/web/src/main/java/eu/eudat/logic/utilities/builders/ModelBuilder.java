package eu.eudat.logic.utilities.builders;

import eu.eudat.models.data.entities.xmlmodels.datasetprofiledefinition.DatabaseViewStyleDefinition;
import eu.eudat.models.data.components.commons.datafield.*;
import eu.eudat.models.data.entities.xmlmodels.modeldefinition.DatabaseModelDefinition;
import eu.eudat.logic.utilities.interfaces.ModelDefinition;
import eu.eudat.logic.utilities.interfaces.ViewStyleDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ModelBuilder {
    private static final Logger logger = LoggerFactory.getLogger(ModelBuilder.class);
    public <U extends ModelDefinition<T>, T extends DatabaseModelDefinition> List<T> toModelDefinition(List<U> items, Class<T> clazz) {
        List<T> list = new LinkedList<T>();
        for (U item : items) {
            try {
                list.add(item.toDatabaseDefinition(clazz.newInstance()));
            } catch (InstantiationException e) {
                logger.error(e.getMessage(), e);
            } catch (IllegalAccessException e) {
                logger.error(e.getMessage(), e);
            }
        }
        return list;
    }

    public <U extends ViewStyleDefinition<T>, T extends DatabaseViewStyleDefinition> List<T> toViewStyleDefinition(List<U> items, Class<T> clazz) {
        List<T> list = new LinkedList<T>();
        for (U item : items) {
            try {
                list.add(item.toDatabaseDefinition(clazz.newInstance()));
            } catch (InstantiationException e) {
                logger.error(e.getMessage(), e);
            } catch (IllegalAccessException e) {
                logger.error(e.getMessage(), e);
            }
        }
        return list;
    }

    public <U extends ViewStyleDefinition<T>, T extends DatabaseViewStyleDefinition> List<U> fromViewStyleDefinition(List<T> items, Class<U> clazz) {
        List<U> list = new LinkedList<U>();
        for (T item : items) {
            try {
                U modelItem = clazz.newInstance();
                modelItem.fromDatabaseDefinition(item);
                list.add(modelItem);
            } catch (InstantiationException e) {
                logger.error(e.getMessage(), e);
            } catch (IllegalAccessException e) {
                logger.error(e.getMessage(), e);
            }
        }
        return list;
    }

    public <U> FieldData<U> toFieldData(Object data, String type, Element dataElement) {
        if (type.equals("combobox")) {
            if (dataElement != null) {
                if (dataElement.getAttribute("type").equals("autocomplete")) {
                    return (FieldData<U>) new AutoCompleteData().fromData(data);
                } else if (dataElement.getAttribute("type").equals("wordlist"))
                    return (FieldData<U>) new WordListData().fromData(data);
            }
        }
        if (type.equals("internalDmpEntities")) {
            if (dataElement != null) {
                if (dataElement.getAttribute("type").equals("researchers")) {
                    return (FieldData<U>) new ResearchersAutoCompleteData().fromData(data);
                }
				else if (dataElement.getAttribute("type").equals("datasets"))
					return (FieldData<U>) new DatasetsAutoCompleteData().fromData(data);
				else if (dataElement.getAttribute("type").equals("dmps"))
					return (FieldData<U>) new DMPsAutoCompleteData().fromData(data);
            }
        }
        if (type.equals("booleanDecision")) return (FieldData<U>) new BooleanDecisionData().fromData(data);
        if (type.equals("radiobox")) return (FieldData<U>) new RadioBoxData().fromData(data);
        if (type.equals("checkBox")) return (FieldData<U>) new CheckBoxData().fromData(data);
        if (type.equals("freetext")) return (FieldData<U>) new FreeTextData().fromData(data);
        if (type.equals("textarea")) return (FieldData<U>) new TextAreaData().fromData(data);
        if (type.equals("richTextarea")) return (FieldData<U>) new RichTextAreaData().fromData(data);
        if (type.equals("upload")) return (FieldData<U>) new UploadData().fromData(data);
//        if (type.equals("table")) return (FieldData<U>) new TableData().fromData(data);
        if (type.equals("datePicker")) return (FieldData<U>) new DatePickerData().fromData(data);
        if (type.equals("externalDatasets")) return (FieldData<U>) new ExternalDatasetsData().fromData(data);
        if (type.equals("dataRepositories")) return (FieldData<U>) new DataRepositoriesData().fromData(data);
        if (type.equals("pubRepositories")) return (FieldData<U>) new DataRepositoriesData().fromData(data);
        if (type.equals("journalRepositories")) return (FieldData<U>) new DataRepositoriesData().fromData(data);
        if (type.equals("taxonomies")) return (FieldData<U>) new TaxonomiesData().fromData(data);
        if (type.equals("licenses")) return (FieldData<U>) new LicensesData().fromData(data);
        if (type.equals("publications")) return (FieldData<U>) new PublicationsData().fromData(data);
        if (type.equals("registries")) return (FieldData<U>) new RegistriesData().fromData(data);
        if (type.equals("services")) return (FieldData<U>) new ServicesData().fromData(data);
        if (type.equals("tags")) return (FieldData<U>) new TagsData().fromData(data);
        if (type.equals("researchers")) return (FieldData<U>) new ResearcherData().fromData(data);
        if (type.equals("organizations")) return (FieldData<U>) new OrganizationsData().fromData(data);
        if (type.equals("datasetIdentifier")) return (FieldData<U>) new DatasetIdentifierData().fromData(data);
        if (type.equals("currency")) return (FieldData<U>) new CurrencyData().fromData(data);
        if (type.equals("validation")) return (FieldData<U>) new ValidationData().fromData(data);
        return null;
    }

    public <U> FieldData<U> toFieldData(Object data, String type) {
        if (type.equals("combobox")) {
            String comboboxType = (String) ((Map<String, Object>) data).get("type");
            if (comboboxType.equals("autocomplete")) {
                return (FieldData<U>) new AutoCompleteData().fromData(data);
            } else if (comboboxType.equals("wordlist"))
                return (FieldData<U>) new WordListData().fromData(data);
        }
        if (type.equals("internalDmpEntities")) {
            String internalDmpEntitiesType = (String) ((Map<String, Object>) data).get("type");
            if (internalDmpEntitiesType.equals("researchers")) {
                return (FieldData<U>) new ResearchersAutoCompleteData().fromData(data);
            }
            else if (internalDmpEntitiesType.equals("datasets")) {
                return (FieldData<U>) new DatasetsAutoCompleteData().fromData(data);
            }
			else if (internalDmpEntitiesType.equals("dmps")) {
				return (FieldData<U>) new DMPsAutoCompleteData().fromData(data);
			}
        }
        if (type.equals("booleanDecision")) return (FieldData<U>) new BooleanDecisionData().fromData(data);
        if (type.equals("radiobox")) return (FieldData<U>) new RadioBoxData().fromData(data);
        if (type.equals("checkBox")) return (FieldData<U>) new CheckBoxData().fromData(data);
        if (type.equals("freetext")) return (FieldData<U>) new FreeTextData().fromData(data);
        if (type.equals("textarea")) return (FieldData<U>) new TextAreaData().fromData(data);
        if (type.equals("richTextarea")) return (FieldData<U>) new RichTextAreaData().fromData(data);
        if (type.equals("upload")) return (FieldData<U>) new UploadData().fromData(data);
//        if (type.equals("table")) return (FieldData<U>) new TableData().fromData(data);
        if (type.equals("datePicker")) return (FieldData<U>) new DatePickerData().fromData(data);
        if (type.equals("externalDatasets")) return (FieldData<U>) new ExternalDatasetsData().fromData(data);
        if (type.equals("dataRepositories")) return (FieldData<U>) new DataRepositoriesData().fromData(data);
        if (type.equals("pubRepositories")) return (FieldData<U>) new DataRepositoriesData().fromData(data);
        if (type.equals("journalRepositories")) return (FieldData<U>) new DataRepositoriesData().fromData(data);
        if (type.equals("taxonomies")) return (FieldData<U>) new TaxonomiesData().fromData(data);
        if (type.equals("licenses")) return (FieldData<U>) new LicensesData().fromData(data);
        if (type.equals("publications")) return (FieldData<U>) new PublicationsData().fromData(data);
        if (type.equals("registries")) return (FieldData<U>) new RegistriesData().fromData(data);
        if (type.equals("services")) return (FieldData<U>) new ServicesData().fromData(data);
        if (type.equals("tags")) return (FieldData<U>) new TagsData().fromData(data);
        if (type.equals("researchers")) return (FieldData<U>) new ResearcherData().fromData(data);
        if (type.equals("organizations")) return (FieldData<U>) new OrganizationsData().fromData(data);
        if (type.equals("datasetIdentifier")) return (FieldData<U>) new DatasetIdentifierData().fromData(data);
        if (type.equals("currency")) return (FieldData<U>) new CurrencyData().fromData(data);
        if (type.equals("validation")) return (FieldData<U>) new ValidationData().fromData(data);
        return null;
    }
}
