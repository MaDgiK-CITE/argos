package eu.eudat.logic.utilities.helpers;

import java.util.List;


public class LabelBuilder {
    private static <T extends LabelGenerator> String generateLabel(List<T> items) {
        String label = "";
        for (T item : items) {
            if (items.indexOf(item) == 3) {
                label += "...";
                break;
            }
            if (items.indexOf(item) > 1) {
                label += ", ";
            }
            label += item.generateLabel();
        }
        return label;
    }

    public static <T extends LabelGenerator> String getLabel(List<T> items) {
        return generateLabel(items);
    }
}
