package eu.eudat.logic.utilities.documents.xml.datasetProfileXml.datasetProfileModel;


import eu.eudat.data.entities.DescriptionTemplate;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.LinkedList;
import java.util.List;

@XmlRootElement(name = "pages")
public class DatasetProfile {

    private String description;
    private String language;
    private String type;
    private boolean enablePrefilling;

    private List<Page> page;

    @XmlElement(name = "page")
    public List<Page> getPage() {
        return page;
    }

    public void setPage(List<Page> page) {
        this.page = page;
    }

    @XmlAttribute(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlAttribute(name = "language")
    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @XmlAttribute(name = "type")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @XmlAttribute(name = "enablePrefilling")
    public boolean isEnablePrefilling() {
        return enablePrefilling;
    }

    public void setEnablePrefilling(boolean enablePrefilling) {
        this.enablePrefilling = enablePrefilling;
    }

    public eu.eudat.models.data.admin.composite.DatasetProfile toAdminCompositeModel(String label){
        eu.eudat.models.data.admin.composite.DatasetProfile newDatasetEntityProfile = new eu.eudat.models.data.admin.composite.DatasetProfile();
        newDatasetEntityProfile.setLabel(label);
        newDatasetEntityProfile.setStatus(DescriptionTemplate.Status.SAVED.getValue());
        newDatasetEntityProfile.setDescription(description);
        newDatasetEntityProfile.setLanguage(language);
        newDatasetEntityProfile.setType(type);
        newDatasetEntityProfile.setEnablePrefilling(enablePrefilling);
        List<eu.eudat.models.data.admin.components.datasetprofile.Page> pagesDatasetEntity = new LinkedList<>();
        List<eu.eudat.models.data.admin.components.datasetprofile.Section> sectionDatasetEntity = new LinkedList<>();
        for (Page xmlPage: page) {
            pagesDatasetEntity.add(xmlPage.toAdminCompositeModelPage());
            for (int i = 0; i < xmlPage.getSections().size(); i++) {
                sectionDatasetEntity.add(xmlPage.toAdminCompositeModelSection(i));
            }
        }
        newDatasetEntityProfile.setPages(pagesDatasetEntity);
        newDatasetEntityProfile.setSections(sectionDatasetEntity);

        return newDatasetEntityProfile;
    }
}
