package eu.eudat.logic.utilities.documents.xml.datasetProfileXml.datasetProfileModel.Fields;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "schematics")
public class Schematics {

    private List<Schematic> schematics;

    @XmlElement(name = "schematic")
    public List<Schematic> getSchematics() {
        return schematics;
    }

    public void setSchematics(List<Schematic> schematics) {
        this.schematics = schematics;
    }
}
