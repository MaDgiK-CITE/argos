package eu.eudat.logic.services.utilities;

import eu.eudat.logic.services.forms.VisibilityRuleService;

/**
 * Created by ikalyvas on 3/1/2018.
 */
public interface UtilitiesService {

    InvitationService getInvitationService();

    MailService getMailService();

    ConfirmationEmailService getConfirmationEmailService();
}
