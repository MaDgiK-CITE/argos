package eu.eudat.logic.security.customproviders.ConfigurableProvider.entities;

import java.util.ArrayList;
import java.util.List;

public class ConfigurableProviders {
	private List<ConfigurableProvider> providers = new ArrayList<>();

	public List<ConfigurableProvider> getProviders() {
		return providers;
	}
	public void setProviders(List<ConfigurableProvider> providers) {
		this.providers = providers;
	}
}
