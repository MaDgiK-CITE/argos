package eu.eudat.logic.utilities.documents.xml.dmpXml.dmpBlueprintModel;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "systemFields")
public class SystemFields {

    private List<SystemField> systemFields;

    @XmlElement(name = "systemField")
    public List<SystemField> getSystemFields() {
        return systemFields;
    }
    public void setSystemFields(List<SystemField> systemFields) {
        this.systemFields = systemFields;
    }

}
