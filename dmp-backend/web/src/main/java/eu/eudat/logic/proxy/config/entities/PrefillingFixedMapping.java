package eu.eudat.logic.proxy.config.entities;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "fixedMapping")
public class PrefillingFixedMapping implements PrefillingMapping{
    private String target;
    private String semanticTarget;
    private String value;

    public String getTarget() {
        return target;
    }

    @XmlAttribute(name = "target")
    public void setTarget(String target) {
        this.target = target;
    }

    public String getSemanticTarget() {
        return semanticTarget;
    }

    @XmlAttribute(name = "semanticTarget")
    public void setSemanticTarget(String semanticTarget) {
        this.semanticTarget = semanticTarget;
    }

    @Override
    public String getSubSource() {
        return "";
    }

    @Override
    public String getTrimRegex() {
        return "";
    }

    public String getValue() {
        return value;
    }

    @XmlAttribute(name = "value")
    public void setValue(String value) {
        this.value = value;
    }
}
