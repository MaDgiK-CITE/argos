package eu.eudat.logic.managers;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.eudat.logic.builders.model.criteria.ExternalDatasetCriteriaBuilder;
import eu.eudat.logic.builders.model.models.DataTableDataBuilder;
import eu.eudat.data.entities.ExternalDataset;
import eu.eudat.data.dao.criteria.ExternalDatasetCriteria;
import eu.eudat.logic.proxy.config.ExternalUrlCriteria;
import eu.eudat.logic.services.operations.DatabaseRepository;
import eu.eudat.models.data.external.ExternalDatasetSourcesModel;
import eu.eudat.models.data.external.ExternalSourcesItemModel;
import eu.eudat.models.data.externaldataset.ExternalDatasetListingModel;
import eu.eudat.data.query.items.table.externaldataset.ExternalDatasetTableRequest;
import eu.eudat.models.data.externaldataset.ExternalDatasetModel;
import eu.eudat.models.data.helpers.common.DataTableData;
import eu.eudat.logic.proxy.config.exceptions.HugeResultSet;
import eu.eudat.logic.proxy.config.exceptions.NoURLFound;
import eu.eudat.logic.proxy.fetching.RemoteFetcher;
import eu.eudat.models.data.security.Principal;
import eu.eudat.queryable.QueryableList;
import eu.eudat.logic.services.ApiContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class ExternalDatasetManager {

    private ApiContext apiContext;
    private DatabaseRepository databaseRepository;
    private RemoteFetcher remoteFetcher;
    @Autowired
    public ExternalDatasetManager(ApiContext apiContext) {
        this.apiContext = apiContext;
        this.databaseRepository = apiContext.getOperationsContext().getDatabaseRepository();
        this.remoteFetcher = apiContext.getOperationsContext().getRemoteFetcher();
    }

    public DataTableData<ExternalDatasetListingModel> getPaged(ExternalDatasetTableRequest externalDatasetTableRequest) throws Exception {
        QueryableList<ExternalDataset> items = apiContext.getOperationsContext().getDatabaseRepository().getExternalDatasetDao().getWithCriteria(externalDatasetTableRequest.getCriteria());
        QueryableList<ExternalDataset> pagedItems = PaginationManager.applyPaging(items, externalDatasetTableRequest);
        List<ExternalDatasetListingModel> externalDatasetListingmodels = pagedItems.select(item -> new ExternalDatasetListingModel().fromDataModel(item));
        return apiContext.getOperationsContext().getBuilderFactory().getBuilder(DataTableDataBuilder.class).data(externalDatasetListingmodels).totalCount(items.count()).build();
    }

    public List<ExternalDatasetListingModel> getWithExternal(String query, String type, Principal principal) throws HugeResultSet, NoURLFound {
        // Fetch the local saved external Datasets that belong to the user.
        ExternalDatasetCriteria criteria = apiContext.getOperationsContext().getBuilderFactory().getBuilder(ExternalDatasetCriteriaBuilder.class).like(query).build();
        criteria.setCreationUserId(principal.getId());
        QueryableList<eu.eudat.data.entities.ExternalDataset> items = apiContext.getOperationsContext().getDatabaseRepository().getExternalDatasetDao().getWithCriteria(criteria);

        // Fetch external Datasets from external sources.
        ExternalUrlCriteria externalUrlCriteria = new ExternalUrlCriteria(query);
        List<Map<String, String>> remoteRepos = remoteFetcher.getDatasets(externalUrlCriteria, type);

        // Parse items from external sources to listing models.
        ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        List<ExternalDatasetListingModel> externalDatasetModels = remoteRepos.stream()
                .map(item -> mapper.convertValue(item, ExternalDatasetListingModel.class))
                .collect(Collectors.toCollection(LinkedList::new));

        // Merge fetched and local.
        List<ExternalDatasetListingModel> externalDatasets = items.select(item -> new ExternalDatasetListingModel().fromDataModel(item));
        externalDatasets.addAll(externalDatasetModels);

        return externalDatasets;
    }

    public ExternalDatasetListingModel getSingle(UUID id) throws HugeResultSet, NoURLFound, InstantiationException, IllegalAccessException {
        ExternalDataset externalDataset = databaseRepository.getExternalDatasetDao().find(id);
        ExternalDatasetListingModel externalDatasetModel = new ExternalDatasetListingModel();
        externalDatasetModel.fromDataModel(externalDataset);
        return externalDatasetModel;
    }

    public ExternalDataset create(eu.eudat.models.data.externaldataset.ExternalDatasetModel externalDatasetModel, Principal principal) throws Exception {
        ExternalDataset externalDataset = externalDatasetModel.toDataModel();
        externalDataset.getCreationUser().setId(principal.getId());
        return apiContext.getOperationsContext().getDatabaseRepository().getExternalDatasetDao().createOrUpdate(externalDataset);
    }
}
