package eu.eudat.logic.builders.model.models;

import eu.eudat.logic.builders.Builder;
import eu.eudat.models.data.dmp.Organisation;

public class OrganisationBuilder extends Builder<Organisation> {
	private String label;
	private String name;
	private String id;
	private String reference;
	private int status;
	private String tag;
	private String key;

	public String getLabel() {
		return label;
	}

	public OrganisationBuilder label(String label) {
		this.label = label;
		return this;
	}

	public String getName() {
		return name;
	}

	public OrganisationBuilder name(String name) {
		this.name = name;
		return this;
	}

	public String getId() {
		return id;
	}

	public OrganisationBuilder id(String id) {
		this.id = id;
		return this;
	}

	public String getReference() { return reference; }

	public OrganisationBuilder reference(String reference) {
		this.reference = reference;
		return this;
	}

	public int getStatus() {
		return status;
	}

	public OrganisationBuilder status(int status) {
		this.status = status;
		return this;
	}

	public String getTag() {
		return tag;
	}

	public OrganisationBuilder tag(String tag) {
		this.tag = tag;
		return this;
	}

	public String getKey() {
		return key;
	}

	public OrganisationBuilder key(String key) {
		this.key = key;
		return this;
	}

	@Override
	public Organisation build() {
		Organisation Organisation = new Organisation();
		Organisation.setId(id);
		Organisation.setReference(reference);
		Organisation.setLabel(label);
		Organisation.setName(name);
		Organisation.setStatus(status);
		Organisation.setTag(tag);
		Organisation.setKey(key);
		return Organisation;
	}
}
