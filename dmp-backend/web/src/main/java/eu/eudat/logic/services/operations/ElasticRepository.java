package eu.eudat.logic.services.operations;

import eu.eudat.elastic.repository.DatasetRepository;
import eu.eudat.elastic.repository.DmpRepository;

public interface ElasticRepository {

	DatasetRepository getDatasetRepository();

	DmpRepository getDmpRepository();
}
