package eu.eudat.logic.proxy.fetching.entities;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "ISO_4217")
public class CurrencyModel {
	private List<CurrencySingleModel> currencies;

	@XmlElementWrapper(name = "CcyTbl")
	@XmlElement(name = "CcyNtry")
	public List<CurrencySingleModel> getCurrencies() {
		return currencies;
	}
	public void setCurrencies(List<CurrencySingleModel> currencies) {
		this.currencies = currencies;
	}

}
