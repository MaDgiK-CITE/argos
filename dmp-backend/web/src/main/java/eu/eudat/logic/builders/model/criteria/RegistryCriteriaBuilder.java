package eu.eudat.logic.builders.model.criteria;

import eu.eudat.logic.builders.Builder;
import eu.eudat.data.dao.criteria.RegistryCriteria;

/**
 * Created by ikalyvas on 2/15/2018.
 */
public class RegistryCriteriaBuilder extends Builder<RegistryCriteria> {
    private String like;

    public RegistryCriteriaBuilder like(String like) {
        this.like = like;
        return this;
    }

    @Override
    public RegistryCriteria build() {
        RegistryCriteria registryCriteria = new RegistryCriteria();
        registryCriteria.setLike(like);
        return registryCriteria;
    }
}
