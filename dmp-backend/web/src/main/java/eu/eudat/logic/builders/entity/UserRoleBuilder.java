package eu.eudat.logic.builders.entity;

import eu.eudat.logic.builders.Builder;
import eu.eudat.data.entities.UserInfo;
import eu.eudat.data.entities.UserRole;

import java.util.UUID;

/**
 * Created by ikalyvas on 2/15/2018.
 */
public class UserRoleBuilder extends Builder<UserRole> {

    private UUID id;

    private int role;

    private UserInfo userInfo;

    public UserRoleBuilder id(UUID id) {
        this.id = id;
        return this;
    }

    public UserRoleBuilder role(int role) {
        this.role = role;
        return this;
    }

    public UserRoleBuilder userInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
        return this;
    }

    @Override
    public UserRole build() {
        UserRole userRole = new UserRole();
        userRole.setUserInfo(userInfo);
        userRole.setRole(role);
        userRole.setId(id);
        return userRole;
    }
}
