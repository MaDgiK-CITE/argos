package eu.eudat.logic.managers;

import eu.eudat.data.dao.criteria.DatasetProfileCriteria;
import eu.eudat.data.dao.entities.DMPDao;
import eu.eudat.data.dao.entities.DatasetProfileDao;
import eu.eudat.data.entities.DMP;
import eu.eudat.data.entities.Dataset;
import eu.eudat.data.entities.DescriptionTemplate;
import eu.eudat.data.entities.UserInfo;
import eu.eudat.data.query.items.item.dataset.DatasetWizardAutocompleteRequest;
import eu.eudat.data.query.items.item.datasetprofile.DatasetProfileWizardAutocompleteRequest;
import eu.eudat.exceptions.datasetwizard.DatasetWizardCannotUnlockException;
import eu.eudat.logic.services.ApiContext;
import eu.eudat.models.data.datasetwizard.DataManagentPlanListingModel;
import eu.eudat.models.data.dmp.AssociatedProfile;
import eu.eudat.models.data.dmp.DataManagementPlan;
import eu.eudat.models.data.security.Principal;
import eu.eudat.queryable.QueryableList;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;


public class DatasetWizardManager {

    public static List<DataManagentPlanListingModel> getUserDmps(DMPDao dmpRepository, DatasetWizardAutocompleteRequest datasetWizardAutocompleteRequest, Principal principal) throws InstantiationException, IllegalAccessException {
        UserInfo userInfo = new UserInfo();
        userInfo.setId(principal.getId());
        QueryableList<DMP> items = dmpRepository.getUserDmps(datasetWizardAutocompleteRequest.getCriteria(), userInfo);
        List<DataManagentPlanListingModel> dataManagementPlans = items.select(item -> new DataManagentPlanListingModel().fromDataModel(item));
        return dataManagementPlans;
    }

    public static List<AssociatedProfile> getAvailableProfiles(DMPDao dmpRepository, DatasetProfileDao profileDao, DatasetProfileWizardAutocompleteRequest datasetProfileWizardAutocompleteRequest) throws InstantiationException, IllegalAccessException {
        DataManagementPlan dataManagementPlan = new DataManagementPlan().fromDataModel(dmpRepository.find(datasetProfileWizardAutocompleteRequest.getCriteria().getId()));
        if (dataManagementPlan.getProfiles() == null || dataManagementPlan.getProfiles().isEmpty()) {
            return new LinkedList<>();
        }
        DatasetProfileCriteria criteria = new DatasetProfileCriteria();
        criteria.setIds(dataManagementPlan.getProfiles().stream().map(AssociatedProfile::getDescriptionTemplateId).collect(Collectors.toList()));
        List<DescriptionTemplate> descriptionTemplates = profileDao.getWithCriteria(criteria).toList();
        criteria.setIds(null);
        criteria.setGroupIds(descriptionTemplates.stream().map(DescriptionTemplate::getGroupId).collect(Collectors.toList()));
        descriptionTemplates = profileDao.getWithCriteria(criteria).toList();
        List<AssociatedProfile> profiles = descriptionTemplates.stream().map(profile -> new AssociatedProfile().fromData(profile)).collect(Collectors.toList());
        return profiles;
    }

    public void unlock(ApiContext apiContext, UUID uuid) throws DatasetWizardCannotUnlockException {
        Dataset dataset = apiContext.getOperationsContext().getDatabaseRepository().getDatasetDao().find(uuid);
        if(dataset.getDmp().getStatus() == DMP.DMPStatus.FINALISED.getValue()) throw new DatasetWizardCannotUnlockException("To perform this action you will need to revert DMP's finalisation");
        dataset.setStatus(Dataset.Status.SAVED.getValue());
        apiContext.getOperationsContext().getDatabaseRepository().getDatasetDao().createOrUpdate(dataset);
        return;
    }

    public void delete(ApiContext apiContext, UUID uuid) throws IOException {
        Dataset oldDataset = apiContext.getOperationsContext().getDatabaseRepository().getDatasetDao().find(uuid);
        eu.eudat.elastic.entities.Dataset oldDatasetElasitc = apiContext.getOperationsContext().getElasticRepository().getDatasetRepository().findDocument(uuid.toString());
        oldDataset.setStatus(Dataset.Status.DELETED.getValue());
        apiContext.getOperationsContext().getDatabaseRepository().getDatasetDao().createOrUpdate(oldDataset);
        if (oldDatasetElasitc != null && uuid != null && oldDatasetElasitc.getId()!= null) {
            oldDatasetElasitc.setStatus(oldDataset.getStatus());
            apiContext.getOperationsContext().getElasticRepository().getDatasetRepository().createOrUpdate(oldDatasetElasitc);
        }
    }
}
