package eu.eudat.logic.security.validators.openaire.helpers;

public class OpenAIRERequest {
	private String code;

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
}
