package eu.eudat.logic.services.operations;

import eu.eudat.elastic.repository.DatasetRepository;
import eu.eudat.elastic.repository.DmpRepository;
import eu.eudat.logic.builders.BuilderFactory;
import eu.eudat.logic.proxy.fetching.RemoteFetcher;
import org.springframework.context.ApplicationContext;

/**
 * Created by ikalyvas on 3/1/2018.
 */
public interface OperationsContext {

    DatabaseRepository getDatabaseRepository();

    ApplicationContext getApplicationContext();

    BuilderFactory getBuilderFactory();

    RemoteFetcher getRemoteFetcher();

//    FileStorageService getFileStorageService();

    ElasticRepository getElasticRepository();
}
