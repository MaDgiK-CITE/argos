package eu.eudat.logic.utilities.documents.xml.dmpXml.dmpBlueprintModel;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "sections")
public class Sections {

    private List<Section> sections;

    @XmlElement(name = "section")
    public List<Section> getSections() {
        return sections;
    }
    public void setSections(List<Section> sections) {
        this.sections = sections;
    }
}
