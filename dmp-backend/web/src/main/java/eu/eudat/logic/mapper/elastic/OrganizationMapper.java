package eu.eudat.logic.mapper.elastic;

import eu.eudat.data.entities.Organisation;
import eu.eudat.elastic.entities.Organization;

public class OrganizationMapper {

	public static Organization toElastic(Organisation organisation) {
		Organization elastic = new Organization();
		elastic.setId(organisation.getId().toString());
		elastic.setName(organisation.getLabel());
		return elastic;
	}
}
