package eu.eudat.logic.utilities.helpers;


public interface LabelGenerator {
    String generateLabel();
}
