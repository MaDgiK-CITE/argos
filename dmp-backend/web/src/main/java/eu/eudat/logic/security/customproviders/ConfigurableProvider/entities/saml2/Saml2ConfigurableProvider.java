package eu.eudat.logic.security.customproviders.ConfigurableProvider.entities.saml2;

import com.fasterxml.jackson.annotation.JsonValue;
import eu.eudat.logic.security.customproviders.ConfigurableProvider.entities.ConfigurableProvider;

import java.util.Map;

public class Saml2ConfigurableProvider extends ConfigurableProvider {

    public enum SAML2UsingFormat {
        NAME("name"), FRIENDLY_NAME("friendly_name");

        private String name;
        SAML2UsingFormat(String name) {
            this.name = name;
        }
        @JsonValue
        public String getName() { return name; }

        public static SAML2UsingFormat fromName(String name) {
            for (SAML2UsingFormat type: SAML2UsingFormat.values()) {
                if (name.equals(type.getName())) {
                    return type;
                }
            }
            throw new IllegalArgumentException("Unsupported SAML2 Attribute " + name);
        }
    }

    public enum SAML2AttributeType {
        XSSTRING("XSString"), XSINTEGER("XSInteger"), XSDATETIME("XSDateTime"), XSBOOLEAN("XSBoolean"), XSBASE64BINARY("XSBase64Binary"), XSURI("XSURI"), XSQNAME("XSQName"), XSANY("XSAny");

        private String type;
        SAML2AttributeType(String type) {
            this.type = type;
        }
        @JsonValue
        public String getType() { return type; }

        public static SAML2AttributeType fromType(String type) {
            for (SAML2AttributeType t: SAML2AttributeType.values()) {
                if (type.equals(t.getType())) {
                    return t;
                }
            }
            throw new IllegalArgumentException("Unsupported SAML2 Attribute Type " + type);
        }
    }

    private String spEntityId;
    private String idpEntityId;
    private String idpUrl;
    private String idpArtifactUrl;
    private String idpMetadataUrl;
    private boolean assertionEncrypted;
    private CertificateInfo encryptionCert;
    private CertificateInfo signingCert;
    private boolean responseSigned;
    private boolean assertionSigned;
    private boolean signatureRequired;
    private SAML2UsingFormat usingFormat;
    private Map<String, SAML2AttributeType> attributeTypes;
    private Map<String, String> configurableUserFromAttributes;
    private String binding;
    private String assertionConsumerServiceUrl;
    private boolean wantAssertionsSigned;
    private boolean authnRequestsSigned;

    public String getSpEntityId() {
        return spEntityId;
    }
    public void setSpEntityId(String spEntityId) {
        this.spEntityId = spEntityId;
    }

    public String getIdpEntityId() {
        return idpEntityId;
    }
    public void setIdpEntityId(String idpEntityId) {
        this.idpEntityId = idpEntityId;
    }

    public String getIdpUrl() {
        return idpUrl;
    }
    public void setIdpUrl(String idpUrl) {
        this.idpUrl = idpUrl;
    }

    public String getIdpArtifactUrl() {
        return idpArtifactUrl;
    }
    public void setIdpArtifactUrl(String idpArtifactUrl) {
        this.idpArtifactUrl = idpArtifactUrl;
    }

    public String getIdpMetadataUrl() {
        return idpMetadataUrl;
    }
    public void setIdpMetadataUrl(String idpMetadataUrl) {
        this.idpMetadataUrl = idpMetadataUrl;
    }

    public boolean isAssertionEncrypted() {
        return assertionEncrypted;
    }
    public void setAssertionEncrypted(boolean assertionEncrypted) {
        this.assertionEncrypted = assertionEncrypted;
    }

    public CertificateInfo getEncryptionCert() {
        return encryptionCert;
    }
    public void setEncryptionCert(CertificateInfo encryptionCert) {
        this.encryptionCert = encryptionCert;
    }

    public CertificateInfo getSigningCert() {
        return signingCert;
    }
    public void setSigningCert(CertificateInfo signingCert) {
        this.signingCert = signingCert;
    }

    public boolean isResponseSigned() {
        return responseSigned;
    }
    public void setResponseSigned(boolean responseSigned) {
        this.responseSigned = responseSigned;
    }

    public boolean isAssertionSigned() {
        return assertionSigned;
    }
    public void setAssertionSigned(boolean assertionSigned) {
        this.assertionSigned = assertionSigned;
    }

    public boolean isSignatureRequired() {
        return signatureRequired;
    }
    public void setSignatureRequired(boolean signatureRequired) {
        this.signatureRequired = signatureRequired;
    }

    public SAML2UsingFormat getUsingFormat() {
        return usingFormat;
    }
    public void setUsingFormat(SAML2UsingFormat usingFormat) {
        this.usingFormat = usingFormat;
    }

    public Map<String, String> getConfigurableUserFromAttributes() {
        return configurableUserFromAttributes;
    }
    public void setConfigurableUserFromAttributes(Map<String, String> configurableUserFromAttributes) {
        this.configurableUserFromAttributes = configurableUserFromAttributes;
    }

    public Map<String, SAML2AttributeType> getAttributeTypes() {
        return attributeTypes;
    }
    public void setAttributeTypes(Map<String, SAML2AttributeType> attributeTypes) {
        this.attributeTypes = attributeTypes;
    }

    public String getBinding() {
        return binding;
    }
    public void setBinding(String binding) {
        this.binding = binding;
    }

    public String getAssertionConsumerServiceUrl() {
        return assertionConsumerServiceUrl;
    }
    public void setAssertionConsumerServiceUrl(String assertionConsumerServiceUrl) {
        this.assertionConsumerServiceUrl = assertionConsumerServiceUrl;
    }

    public boolean isWantAssertionsSigned() {
        return wantAssertionsSigned;
    }
    public void setWantAssertionsSigned(boolean wantAssertionsSigned) {
        this.wantAssertionsSigned = wantAssertionsSigned;
    }

    public boolean isAuthnRequestsSigned() {
        return authnRequestsSigned;
    }
    public void setAuthnRequestsSigned(boolean authnRequestsSigned) {
        this.authnRequestsSigned = authnRequestsSigned;
    }
}
