package eu.eudat.logic.managers;

import eu.eudat.depositinterface.repository.RepositoryDeposit;
import eu.eudat.depositinterface.repository.RepositoryDepositConfiguration;
import eu.eudat.models.data.doi.DepositRequest;
import eu.eudat.models.data.doi.Doi;
import eu.eudat.models.data.doi.RepositoryConfig;
import eu.eudat.models.data.security.Principal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class DepositManager {
    private static final Logger logger = LoggerFactory.getLogger(DepositManager.class);

    private List<RepositoryDeposit> repositories;
    private DataManagementPlanManager dataManagementPlanManager;

    @Autowired
    public DepositManager(List<RepositoryDeposit> repositories, DataManagementPlanManager dataManagementPlanManager){
        this.repositories = repositories;
        this.dataManagementPlanManager = dataManagementPlanManager;
    }

    public List<RepositoryConfig> getAvailableRepos() {
        List<RepositoryConfig> reposConfigModel = new ArrayList<>();
        for (RepositoryDeposit r: this.repositories) {
            List<RepositoryDepositConfiguration> repoConf = r.getConfiguration();
            if(repoConf != null) {
                for(RepositoryDepositConfiguration cf: repoConf){
                    RepositoryConfig repoModel = new RepositoryConfig();
                    reposConfigModel.add(repoModel.toModel(cf));
                }
            }
        }
        return reposConfigModel;
    }

    public String authenticate(String id, String code) {
        for(RepositoryDeposit r: this.repositories){
            if(r.getConfiguration().stream().anyMatch(x -> x.getRepositoryId().equals(id))){
                return r.authenticate(id, code);
            }
        }
        return null;
    }

    public Doi deposit(DepositRequest depositRequest, Principal principal) throws Exception {
        return this.dataManagementPlanManager.createDoi(depositRequest, principal);
    }

    public String getRepositoryLogo(String repositoryId){
        for(RepositoryDeposit r: this.repositories){
            Optional<RepositoryDepositConfiguration> cf = r.getConfiguration().stream().filter(x -> x.getRepositoryId().equals(repositoryId)).findFirst();
            if(cf.isPresent()){
                return cf.get().isHasLogo() ?  r.getLogo(repositoryId) : null;
            }
        }
        return null;
    }
}
