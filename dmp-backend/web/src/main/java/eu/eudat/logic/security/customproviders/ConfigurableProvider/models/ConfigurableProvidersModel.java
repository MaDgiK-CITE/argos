package eu.eudat.logic.security.customproviders.ConfigurableProvider.models;

import eu.eudat.logic.security.customproviders.ConfigurableProvider.entities.ConfigurableProvider;
import eu.eudat.logic.security.customproviders.ConfigurableProvider.entities.ConfigurableProviders;
import eu.eudat.logic.security.customproviders.ConfigurableProvider.entities.oauth2.Oauth2ConfigurableProvider;
import eu.eudat.logic.security.customproviders.ConfigurableProvider.entities.saml2.Saml2ConfigurableProvider;
import eu.eudat.logic.security.customproviders.ConfigurableProvider.models.oauth2.Oauth2ConfigurableProviderModel;
import eu.eudat.logic.security.customproviders.ConfigurableProvider.models.saml2.Saml2ConfigurableProviderModel;

import java.util.LinkedList;
import java.util.List;

public class ConfigurableProvidersModel {
	private List<ConfigurableProviderModel> providers;

	public List<ConfigurableProviderModel> getProviders() {
		return providers;
	}
	public void setProviders(List<ConfigurableProviderModel> providers) {
		this.providers = providers;
	}

	public ConfigurableProvidersModel fromDataModel(ConfigurableProviders entity) {
		ConfigurableProvidersModel model = new ConfigurableProvidersModel();
		List<ConfigurableProviderModel> providerModelList = new LinkedList<>();
		if (entity != null) {
			for (ConfigurableProvider entityProvider : entity.getProviders()) {
				if (entityProvider.isEnabled()){
					if(entityProvider instanceof Oauth2ConfigurableProvider)
						providerModelList.add(new Oauth2ConfigurableProviderModel().fromDataModel(entityProvider));
					else if(entityProvider instanceof Saml2ConfigurableProvider)
						providerModelList.add(new Saml2ConfigurableProviderModel().fromDataModel(entityProvider));
					else
						providerModelList.add(new ConfigurableProviderModel().fromDataModel(entityProvider));
				}
			}
		}
		model.setProviders(providerModelList);
		return model;
	}
}
