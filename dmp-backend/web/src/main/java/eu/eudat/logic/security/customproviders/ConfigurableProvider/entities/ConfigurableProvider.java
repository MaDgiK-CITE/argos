package eu.eudat.logic.security.customproviders.ConfigurableProvider.entities;

import com.fasterxml.jackson.annotation.*;
import eu.eudat.logic.security.customproviders.ConfigurableProvider.entities.oauth2.Oauth2ConfigurableProvider;
import eu.eudat.logic.security.customproviders.ConfigurableProvider.entities.saml2.Saml2ConfigurableProvider;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type", visible = true)
@JsonSubTypes({
		@JsonSubTypes.Type(value = Oauth2ConfigurableProvider.class, name = "oauth2"),
		@JsonSubTypes.Type(value = Saml2ConfigurableProvider.class, name = "saml2")
})
public class ConfigurableProvider {

	private boolean enabled;
	private String configurableLoginId;
	private String type;
	private String name;
	private String logoUrl;

	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getConfigurableLoginId() {
		return configurableLoginId;
	}
	public void setConfigurableLoginId(String configurableLoginId) {
		this.configurableLoginId = configurableLoginId;
	}

	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getLogoUrl() {
		return logoUrl;
	}
	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}

}
