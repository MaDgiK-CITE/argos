package eu.eudat.logic.utilities.interfaces;

/**
 * Created by ikalyvas on 2/5/2018.
 */
public interface Cloneable<T> {
    T clone();
}
