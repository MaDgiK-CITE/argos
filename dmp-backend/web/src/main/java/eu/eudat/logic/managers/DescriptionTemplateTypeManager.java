package eu.eudat.logic.managers;

import eu.eudat.data.entities.DescriptionTemplateType;
import eu.eudat.exceptions.descriptiontemplate.DescriptionTemplatesWithTypeException;
import eu.eudat.logic.services.ApiContext;
import eu.eudat.logic.services.operations.DatabaseRepository;
import eu.eudat.models.data.descriptiontemplatetype.DescriptionTemplateTypeModel;
import eu.eudat.models.data.helpers.common.DataTableData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class DescriptionTemplateTypeManager {
    private static final Logger logger = LoggerFactory.getLogger(DescriptionTemplateTypeManager.class);

    private ApiContext apiContext;
    private DatabaseRepository databaseRepository;


    @Autowired
    public DescriptionTemplateTypeManager(ApiContext apiContext, DatabaseRepository databaseRepository) {
        this.apiContext = apiContext;
        this.databaseRepository = databaseRepository;
    }

    public DataTableData<DescriptionTemplateTypeModel> get() {
        List<DescriptionTemplateType> types = this.databaseRepository.getDescriptionTemplateTypeDao().asQueryable().toList();
        List<DescriptionTemplateTypeModel> typesModelList = types.stream().map(type -> new DescriptionTemplateTypeModel().fromDataModel(type)).collect(Collectors.toList());
        DataTableData<DescriptionTemplateTypeModel> dataTableData = new DataTableData<>();
        dataTableData.setData(typesModelList);
        dataTableData.setTotalCount((long) typesModelList.size());
        return dataTableData;
    }

    public DescriptionTemplateTypeModel getSingle(UUID id) throws Exception {
        DescriptionTemplateType type = this.databaseRepository.getDescriptionTemplateTypeDao().find(id);
        if (type != null) {
            return new DescriptionTemplateTypeModel().fromDataModel(type);
        }
        else {
            throw new DescriptionTemplatesWithTypeException("No description template type found with this id");
        }
    }

    public void create(DescriptionTemplateTypeModel type) throws Exception {
        DescriptionTemplateType existed = this.databaseRepository.getDescriptionTemplateTypeDao().findFromName(type.getName());
        if (existed == null) {
            this.databaseRepository.getDescriptionTemplateTypeDao().createOrUpdate(type.toDataModel());
        }
        else {
            throw new DescriptionTemplatesWithTypeException("There is already a description template type with that name.");
        }
    }

    public void update(DescriptionTemplateTypeModel type) throws Exception {
        DescriptionTemplateType existed = this.databaseRepository.getDescriptionTemplateTypeDao().findFromName(type.getName());
        if (existed != null) {
            this.databaseRepository.getDescriptionTemplateTypeDao().createOrUpdate(type.toDataModel());
        }
        else {
            throw new DescriptionTemplatesWithTypeException("No description template type found.");
        }
    }

    public void delete(UUID id) throws DescriptionTemplatesWithTypeException {
        DescriptionTemplateType type = this.databaseRepository.getDescriptionTemplateTypeDao().find(id);
        if (type != null) {
            Long descriptionsWithType = this.databaseRepository.getDatasetProfileDao().countWithType(type);
            if(descriptionsWithType == 0) {
                type.setStatus(DescriptionTemplateType.Status.DELETED.getValue());
                this.databaseRepository.getDescriptionTemplateTypeDao().createOrUpdate(type);
            }
            else{
                throw new DescriptionTemplatesWithTypeException("This type can not deleted, because Descriptions are associated with it");
            }
        }
    }
}
