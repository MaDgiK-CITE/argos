package eu.eudat.logic.services.utilities;

import eu.eudat.models.data.mail.SimpleMail;

import javax.mail.MessagingException;


public interface MailService {
    void sendSimpleMail(SimpleMail mail) throws MessagingException;

    String getMailTemplateContent(String resourceTemplate);

    String getMailTemplateSubject();
}
