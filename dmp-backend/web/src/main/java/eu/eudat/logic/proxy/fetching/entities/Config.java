package eu.eudat.logic.proxy.fetching.entities;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "fetchConfig")
public class Config {
	private List<ConfigSingle> configs;

	@XmlElementWrapper
	@XmlElement(name = "config")
	public List<ConfigSingle> getConfigs() {
		return configs;
	}
	public void setConfigs(List<ConfigSingle> configs) {
		this.configs = configs;
	}

}
