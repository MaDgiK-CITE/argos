package eu.eudat.logic.utilities.documents.xml.dmpXml;

import eu.eudat.logic.utilities.builders.XmlBuilder;

import eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition.DataManagementPlanBlueprint;
import eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition.Section;
import eu.eudat.models.data.listingmodels.DataManagementPlanBlueprintListingModel;
import org.springframework.core.env.Environment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.UUID;

public class ExportXmlBuilderDmpBlueprint {


    public File build(DataManagementPlanBlueprintListingModel dmpProfile, Environment environment) throws IOException {

        File xmlFile = new File(environment.getProperty("temp.temp") + UUID.randomUUID() + ".xml");
        BufferedWriter writer = new BufferedWriter(new FileWriter(xmlFile, true));
        Document xmlDoc = XmlBuilder.getDocument();
        Element root = xmlDoc.createElement("root");
        Element definition = xmlDoc.createElement("definition");
//        Element root = xmlDoc.createElement(dmpProfile.getLabel());
        definition.appendChild(createDefinition(dmpProfile.getDefinition(), xmlDoc));
        root.appendChild(definition);
        xmlDoc.appendChild(root);
        String xml = XmlBuilder.generateXml(xmlDoc);
        writer.write(xml);
        writer.close();
        return xmlFile;
    }

    public Element createDefinition(DataManagementPlanBlueprint dmpDefinition, Document doc) {
        Element sections = doc.createElement("sections");
        for (Section section : dmpDefinition.getSections()) {
            sections.appendChild(section.toXml(doc));
        }
        return sections;
    }

}
