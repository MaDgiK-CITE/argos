package eu.eudat.logic.builders.model.models;

import eu.eudat.logic.builders.Builder;
import eu.eudat.models.data.security.Principal;
import eu.eudat.types.Authorities;

import java.time.Instant;
import java.util.Date;
import java.util.Set;
import java.util.UUID;

/**
 * Created by ikalyvas on 2/15/2018.
 */
public class PrincipalBuilder extends Builder<Principal> {

    private UUID id;
    private UUID token;
    private String name;
    private String email;
    private Date expiresAt;
    private String avatarUrl;
    private Set<Authorities> authorities;
    private String culture;
    private String language;
    private String timezone;
    private String zenodoToken;
    private Instant zenodoDuration;
    private String zenodoEmail;
    private String zenodoRefresh;

    public PrincipalBuilder id(UUID id) {
        this.id = id;
        return this;
    }

    public PrincipalBuilder token(UUID token) {
        this.token = token;
        return this;
    }

    public PrincipalBuilder name(String name) {
        this.name = name;
        return this;
    }

    public PrincipalBuilder email(String email) {
        this.email = email;
        return this;
    }

    public PrincipalBuilder expiresAt(Date expiresAt) {
        this.expiresAt = expiresAt;
        return this;
    }

    public PrincipalBuilder authorities(Set<Authorities> authorities) {
        this.authorities = authorities;
        return this;
    }

    public PrincipalBuilder avatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
        return this;
    }

    public PrincipalBuilder culture(String culture) {
        this.culture = culture;
        return this;
    }

    public PrincipalBuilder language(String language) {
        this.language = language;
        return this;
    }

    public PrincipalBuilder timezone(String timezone) {
        this.timezone = timezone;
        return this;
    }

    public PrincipalBuilder zenodoToken(String zenodoToken) {
        this.zenodoToken = zenodoToken;
        return this;
    }

    public PrincipalBuilder zenodoDuration(Instant zenodoDuration) {
        this.zenodoDuration = zenodoDuration;
        return this;
    }

    public PrincipalBuilder zenodoEmail(String zenodoEmail) {
        this.zenodoEmail = zenodoEmail;
        return this;
    }

    public PrincipalBuilder zenodoRefresh(String zenodoRefresh) {
        this.zenodoRefresh = zenodoRefresh;
        return this;
    }

    @Override
    public Principal build() {
        Principal principal = new Principal();
        principal.setAuthorities(authorities);
        principal.setName(name);
        principal.setEmail(email);
        principal.setExpiresAt(expiresAt);
        principal.setToken(token);
        principal.setId(id);
        principal.setAvatarUrl(avatarUrl);
        principal.setCulture(culture);
        principal.setLanguage(language);
        principal.setTimezone(timezone);
        principal.setZenodoToken(zenodoToken);
        principal.setZenodoDuration(zenodoDuration);
        principal.setZenodoEmail(zenodoEmail);
        principal.setZenodoRefresh(zenodoRefresh);
        return principal;
    }
}
