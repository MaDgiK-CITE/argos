package eu.eudat.logic.utilities.documents.xml.dmpXml;

import eu.eudat.logic.utilities.documents.xml.dmpXml.dmpBlueprintModel.DmpBlueprint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;

public class ImportXmlBuilderDmpBlueprint {
    private static final Logger logger = LoggerFactory.getLogger(ImportXmlBuilderDmpBlueprint.class);

    public DmpBlueprint build(File xmlFile) throws IOException {
        DmpBlueprint dmpProfile = new DmpBlueprint();
        JAXBContext jaxbContext = null;
        try {
            jaxbContext = JAXBContext.newInstance(DmpBlueprint.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            dmpProfile = (DmpBlueprint) unmarshaller.unmarshal(xmlFile);
        } catch (JAXBException e) {
            logger.error(e.getMessage(), e);
        }

        return dmpProfile;
    }

}
