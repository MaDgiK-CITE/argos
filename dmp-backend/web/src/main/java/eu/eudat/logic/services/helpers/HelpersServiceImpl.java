package eu.eudat.logic.services.helpers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

/**
 * Created by ikalyvas on 3/1/2018.
 */
@Service("helpersService")
public class HelpersServiceImpl implements HelpersService {

    private MessageSource messageSource;
//    private LoggerService loggerService;

    @Autowired
    public HelpersServiceImpl(MessageSource messageSource/*, LoggerService loggerService*/) {
        this.messageSource = messageSource;
//        this.loggerService = loggerService;
    }

    @Override
    public MessageSource getMessageSource() {
        return messageSource;
    }

    /*@Override
    public LoggerService getLoggerService() {
        return loggerService;
    }*/
}
