package eu.eudat.logic.security.customproviders.ConfigurableProvider.entities.saml2;

import com.fasterxml.jackson.annotation.JsonValue;

public class CertificateInfo {

    public enum KeyFormat {
        JKS("JKS"), PKCS12("PKCS12");

        private String type;
        KeyFormat(String type) {
            this.type = type;
        }
        @JsonValue
        public String getType() { return type; }

        public static KeyFormat fromType(String type) {
            for (KeyFormat t: KeyFormat.values()) {
                if (type.equals(t.getType())) {
                    return t;
                }
            }
            throw new IllegalArgumentException("Unsupported Keystore format " + type);
        }
    }

    private String alias;
    private String password;
    private String keystorePath;
    private String keystorePassword;
    private KeyFormat keyFormat;

    public String getAlias() {
        return alias;
    }
    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public String getKeystorePath() {
        return keystorePath;
    }
    public void setKeystorePath(String keystorePath) {
        this.keystorePath = keystorePath;
    }

    public String getKeystorePassword() {
        return keystorePassword;
    }
    public void setKeystorePassword(String keystorePassword) {
        this.keystorePassword = keystorePassword;
    }

    public KeyFormat getKeyFormat() {
        return keyFormat;
    }
    public void setKeyFormat(KeyFormat keyFormat) {
        this.keyFormat = keyFormat;
    }

}
