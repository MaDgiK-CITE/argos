package eu.eudat.logic.builders.model.models;

import eu.eudat.logic.builders.Builder;
import eu.eudat.data.entities.UserInfo;
import eu.eudat.models.data.dmp.DataManagementPlan;
import eu.eudat.models.data.grant.Grant;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public class GrantBuilder extends Builder<Grant> {

    private UUID id;

    private List<DataManagementPlan> dmps;

    private String label;

    private String abbreviation;

    private String reference;

    private String uri;

    private String definition;

    private Date startDate;

    private Date endDate;

    private eu.eudat.data.entities.Grant.Status status;

    private UserInfo creationUser;

    private Date created;

    private Date modified;

    private String description;

    private String source;

    private String key;

    public GrantBuilder id(UUID id) {
        this.id = id;
        return this;
    }

    public GrantBuilder dmps(List<DataManagementPlan> dmps) {
        this.dmps = dmps;
        return this;
    }

    public GrantBuilder label(String label) {
        this.label = label;
        return this;
    }

    public GrantBuilder abbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
        return this;
    }

    public GrantBuilder reference(String reference) {
        this.reference = reference;
        return this;
    }

    public GrantBuilder uri(String uri) {
        this.uri = uri;
        return this;
    }

    public GrantBuilder definition(String definition) {
        this.definition = definition;
        return this;
    }

    public GrantBuilder startDate(Date startDate) {
        this.startDate = startDate;
        return this;
    }

    public GrantBuilder endDate(Date endDate) {
        this.endDate = endDate;
        return this;
    }

    public GrantBuilder status(eu.eudat.data.entities.Grant.Status status) {
        this.status = status;
        return this;
    }

    public GrantBuilder creationUser(UserInfo creationUser) {
        this.creationUser = creationUser;
        return this;
    }

    public GrantBuilder created(Date created) {
        this.created = created;
        return this;
    }

    public GrantBuilder modified(Date modified) {
        this.modified = modified;
        return this;
    }

    public GrantBuilder description(String description) {
        this.description = description;
        return this;
    }

    public GrantBuilder source(String source) {
        this.source = source;
        return this;
    }

    public GrantBuilder key(String key) {
        this.key = key;
        return this;
    }

    @Override
    public Grant build() {
        Grant grant = new Grant();
        grant.setStatus(status.getValue());
        grant.setAbbreviation(abbreviation);
        grant.setCreated(created);
        grant.setCreationUser(creationUser);
        grant.setDefinition(definition);
        grant.setDescription(description);
        grant.setDmps(dmps);
        grant.setEndDate(endDate);
        grant.setId(id);
        grant.setLabel(label);
        grant.setModified(modified);
        grant.setReference(reference);
        grant.setCreationUser(creationUser);
        grant.setStartDate(startDate);
        grant.setSource(source);
        grant.setKey(key);
        return grant;
    }
}
