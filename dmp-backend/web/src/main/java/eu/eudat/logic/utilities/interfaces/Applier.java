package eu.eudat.logic.utilities.interfaces;

/**
 * Created by ikalyvas on 3/1/2018.
 */
public interface Applier<A, V> {
    void apply(A applier, V value);
}
