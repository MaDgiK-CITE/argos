package eu.eudat.logic.utilities.documents.xml.datasetProfileXml.datasetProfileModel.Fields;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.LinkedList;
import java.util.List;

@XmlRootElement(name = "fields")
public class Fields {

    private List<Field> field;

    @XmlElement(name = "field")
    public List<Field> getField() {
        return field;
    }

    public void setField(List<Field> field) {
        this.field = field;
    }

    public List<eu.eudat.models.data.admin.components.datasetprofile.Field> toAdminCompositeModelSection() {
        List<eu.eudat.models.data.admin.components.datasetprofile.Field> fieldsEntity = new LinkedList<>();
        if (this.field != null)
            for (Field xmlField : this.field) {
                fieldsEntity.add(xmlField.toAdminCompositeModelSection());
            }
        return fieldsEntity;
    }

}







