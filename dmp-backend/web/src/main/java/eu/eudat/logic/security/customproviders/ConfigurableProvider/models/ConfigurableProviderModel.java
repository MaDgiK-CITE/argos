package eu.eudat.logic.security.customproviders.ConfigurableProvider.models;

import eu.eudat.logic.security.customproviders.ConfigurableProvider.entities.ConfigurableProvider;

public class ConfigurableProviderModel {

	private String configurableLoginId;
	private String type;
	private String name;
	private String logoUrl;

	public String getConfigurableLoginId() {
		return configurableLoginId;
	}
	public void setConfigurableLoginId(String configurableLoginId) {
		this.configurableLoginId = configurableLoginId;
	}

	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getLogoUrl() {
		return logoUrl;
	}
	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}

	public ConfigurableProviderModel fromDataModel(ConfigurableProvider entity) {
		ConfigurableProviderModel model = new ConfigurableProviderModel();
		model.setConfigurableLoginId(entity.getConfigurableLoginId());
		model.setType(entity.getType());
		model.setName(entity.getName());
		model.setLogoUrl(entity.getLogoUrl());
		return model;
	}

}
