package eu.eudat.logic.builders;

/**
 * Created by ikalyvas on 2/15/2018.
 */
public interface BuilderFactory {
    <T extends Builder> T getBuilder(Class<T> tClass);
}
