package eu.eudat.logic.security.customproviders.ConfigurableProvider.models.oauth2;

import eu.eudat.logic.security.customproviders.ConfigurableProvider.entities.ConfigurableProvider;
import eu.eudat.logic.security.customproviders.ConfigurableProvider.entities.oauth2.Oauth2ConfigurableProvider;
import eu.eudat.logic.security.customproviders.ConfigurableProvider.models.ConfigurableProviderModel;

public class Oauth2ConfigurableProviderModel extends ConfigurableProviderModel {

	private String clientId;
	private String redirect_uri;
	private String oauthUrl;
	private String scope;
	private String state;

	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getRedirect_uri() {
		return redirect_uri;
	}
	public void setRedirect_uri(String redirect_uri) {
		this.redirect_uri = redirect_uri;
	}

	public String getOauthUrl() {
		return oauthUrl;
	}
	public void setOauthUrl(String oauthUrl) {
		this.oauthUrl = oauthUrl;
	}

	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}

	@Override
	public Oauth2ConfigurableProviderModel fromDataModel(ConfigurableProvider entity) {
		Oauth2ConfigurableProviderModel model = new Oauth2ConfigurableProviderModel();
		model.setConfigurableLoginId(entity.getConfigurableLoginId());
		model.setType(entity.getType());
		model.setName(entity.getName());
		model.setLogoUrl(entity.getLogoUrl());
		model.setClientId(((Oauth2ConfigurableProvider)entity).getClientId());
		model.setRedirect_uri(((Oauth2ConfigurableProvider)entity).getRedirect_uri());
		model.setOauthUrl(((Oauth2ConfigurableProvider)entity).getOauthUrl());
		model.setScope(((Oauth2ConfigurableProvider)entity).getScope());
		model.setState(((Oauth2ConfigurableProvider)entity).getState());
		return model;
	}

}
