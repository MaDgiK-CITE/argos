package eu.eudat.logic.proxy.config.entities;

import eu.eudat.logic.proxy.config.FetchStrategy;
import eu.eudat.logic.proxy.config.UrlConfiguration;

import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

public class PrefillingSearch extends GenericUrls{
    private UrlConfiguration urlConfig;

    public UrlConfiguration getUrlConfig() {
        return urlConfig;
    }

    @XmlElement(name = "urlConfig")
    public void setUrlConfig(UrlConfiguration urlConfig) {
        this.urlConfig = urlConfig;
    }

    @Override
    public List<UrlConfiguration> getUrls() {
        List<UrlConfiguration> urls = new ArrayList<>();
        urls.add(urlConfig);
        return urls;
    }

    @Override
    public FetchStrategy getFetchMode() {
        return null;
    }
}
