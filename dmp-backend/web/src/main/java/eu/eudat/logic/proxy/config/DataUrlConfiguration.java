package eu.eudat.logic.proxy.config;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.List;

/**
 * Created by ikalyvas on 6/29/2018.
 */
public class DataUrlConfiguration {
    private String path;
    private DataFieldsUrlConfiguration fieldsUrlConfiguration;
    private UrlConfiguration urlConfiguration;
    private String parseClass;
    private String parseField;
    private List<String> mergedFields;
    private String mergedFieldName;

    public String getPath() {
        return path;
    }

    @XmlElement(name = "path")
    public void setPath(String path) {
        this.path = path;
    }

    public DataFieldsUrlConfiguration getFieldsUrlConfiguration() {
        return fieldsUrlConfiguration;
    }

    @XmlElement(name = "fields")
    public void setFieldsUrlConfiguration(DataFieldsUrlConfiguration fieldsUrlConfiguration) {
        this.fieldsUrlConfiguration = fieldsUrlConfiguration;
    }

    public UrlConfiguration getUrlConfiguration() {
        return urlConfiguration;
    }

    @XmlElement(name = "urlConfig")
    public void setUrlConfiguration(UrlConfiguration urlConfiguration) {
        this.urlConfiguration = urlConfiguration;
    }

    public String getParseClass() {
        return parseClass;
    }

    @XmlElement(name = "parse-class")
    public void setParseClass(String parseClass) {
        this.parseClass = parseClass;
    }

    public String getParseField() {
        return parseField;
    }

    @XmlElement(name = "parse-field")
    public void setParseField(String parseField) {
        this.parseField = parseField;
    }

    public List<String> getMergedFields() {
        return mergedFields;
    }

    @XmlElementWrapper(name = "merge-fields")
    @XmlElement(name = "field")
    public void setMergedFields(List<String> mergedFields) {
        this.mergedFields = mergedFields;
    }

    public String getMergedFieldName() {
        return mergedFieldName;
    }

    @XmlElement(name = "merge-field-name")
    public void setMergedFieldName(String mergedFieldName) {
        this.mergedFieldName = mergedFieldName;
    }
}
