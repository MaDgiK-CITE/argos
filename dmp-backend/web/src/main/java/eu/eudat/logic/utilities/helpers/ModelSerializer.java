package eu.eudat.logic.utilities.helpers;


import eu.eudat.models.data.entities.xmlmodels.datasetprofiledefinition.DatabaseViewStyleDefinition;
import eu.eudat.models.data.entities.xmlmodels.modeldefinition.DatabaseModelDefinition;

public interface ModelSerializer<T extends DatabaseViewStyleDefinition, U extends DatabaseModelDefinition> {
    void fromDatabaseDefinition(T viewStyle, U model);
}
