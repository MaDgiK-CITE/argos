package eu.eudat.logic.utilities.documents.xml.datasetProfileXml.datasetProfileModel.Fields;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "validation")
public class Validation {

    private int type;

    @XmlAttribute(name = "type")
    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
