package eu.eudat.logic.proxy.config;

public enum AuthType {
    ;
    private final String name;

    AuthType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static AuthType fromName(String name) {
        for (AuthType authType : AuthType.values()) {
            if (authType.getName().equals(name)) {
                return authType;
            }
        }
        throw new IllegalArgumentException("AuthType [" + name + "] is not supported");
    }
}
