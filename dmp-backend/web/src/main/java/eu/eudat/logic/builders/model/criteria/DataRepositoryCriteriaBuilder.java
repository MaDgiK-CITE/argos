package eu.eudat.logic.builders.model.criteria;

import eu.eudat.logic.builders.Builder;
import eu.eudat.data.dao.criteria.DataRepositoryCriteria;

/**
 * Created by ikalyvas on 2/15/2018.
 */
public class DataRepositoryCriteriaBuilder extends Builder<DataRepositoryCriteria> {
    private String like;

    public DataRepositoryCriteriaBuilder like(String like) {
        this.like = like;
        return this;
    }

    @Override
    public DataRepositoryCriteria build() {
        DataRepositoryCriteria dataRepositoryCriteria = new DataRepositoryCriteria();
        dataRepositoryCriteria.setLike(like);
        return dataRepositoryCriteria;
    }
}
