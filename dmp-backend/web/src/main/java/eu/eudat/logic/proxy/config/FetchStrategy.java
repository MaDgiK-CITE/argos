package eu.eudat.logic.proxy.config;

public enum FetchStrategy {

    FIRST,
    ALL

}
