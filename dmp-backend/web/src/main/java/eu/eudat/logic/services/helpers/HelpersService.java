package eu.eudat.logic.services.helpers;

import org.springframework.context.MessageSource;

/**
 * Created by ikalyvas on 3/1/2018.
 */
public interface HelpersService {

    MessageSource getMessageSource();

//    LoggerService getLoggerService();
}
