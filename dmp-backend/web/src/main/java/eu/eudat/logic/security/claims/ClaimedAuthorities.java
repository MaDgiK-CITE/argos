package eu.eudat.logic.security.claims;

import eu.eudat.types.Authorities;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static eu.eudat.types.Authorities.USER;


@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface ClaimedAuthorities {
    Authorities[] claims() default {USER};
}
