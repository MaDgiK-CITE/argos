package eu.eudat.logic.managers;

import eu.eudat.models.data.externalurl.ExternalSourcesConfiguration;
import eu.eudat.logic.proxy.config.configloaders.ConfigLoader;

import java.util.stream.Collectors;

/**
 * Created by ikalyvas on 5/17/2018.
 */
public class CommonsManager {

    public static ExternalSourcesConfiguration getExternalSourcesConfiguration(ConfigLoader configLoader){
        ExternalSourcesConfiguration externalSourcesConfiguration = new ExternalSourcesConfiguration();
        externalSourcesConfiguration.setDataRepositories(configLoader.getExternalUrls().getRepositories().getUrls().stream()
                .map(item-> new ExternalSourcesConfiguration.ExternalSourcesUrlModel(item.getKey(),item.getLabel())).collect(Collectors.toList()));
        externalSourcesConfiguration.setExternalDatasets(configLoader.getExternalUrls().getDatasets().getUrls().stream()
                .map(item-> new ExternalSourcesConfiguration.ExternalSourcesUrlModel(item.getKey(),item.getLabel())).collect(Collectors.toList()));
        externalSourcesConfiguration.setRegistries(configLoader.getExternalUrls().getRegistries().getUrls().stream()
                .map(item-> new ExternalSourcesConfiguration.ExternalSourcesUrlModel(item.getKey(),item.getLabel())).collect(Collectors.toList()));
        externalSourcesConfiguration.setServices(configLoader.getExternalUrls().getServices().getUrls().stream()
                .map(item-> new ExternalSourcesConfiguration.ExternalSourcesUrlModel(item.getKey(),item.getLabel())).collect(Collectors.toList()));
        /*externalSourcesConfiguration.setTags(configLoader.getExternalUrls().getTags().getUrls().stream()
                .map(item-> new ExternalSourcesConfiguration.ExternalSourcesUrlModel(item.getKey(),item.getLabel())).collect(Collectors.toList()));*/
        return externalSourcesConfiguration;
    }
}
