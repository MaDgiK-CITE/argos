package eu.eudat.logic.proxy.config;


import eu.eudat.logic.proxy.config.entities.*;
import eu.eudat.logic.proxy.config.prefilling.PrefillingConfigMapAdapter;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Map;

@XmlRootElement
public class ExternalUrls implements Serializable {

    private static final long serialVersionUID = -5076364662014107275L;

    Long maxresults;

    GrantUrls grants;
    ProjectUrls projects;
    RegistryUrls registries;
    RepositoryUrls repositories;
    PubRepositoryUrls pubRepositories;
    JournalUrls journals;
    TaxonomiesUrls taxonomies;
    PublicationsUrls publications;
    ServiceUrls services;
    ResearcherUrls researchers;
    OrganisationUrls organisations;
    DatasetUrls datasets;
    /*TagUrls tags;*/
    FunderUrls funders;
    LicenseUrls licenses;
    ValidationUrls validations;
    Map<String, PrefillingConfig> prefillings;


    public RegistryUrls getRegistries() {
        return registries;
    }

    @XmlElement(name = "registries")
    public void setRegistries(RegistryUrls registries) {
        this.registries = registries;
    }


    public RepositoryUrls getRepositories() {
        return repositories;
    }

    @XmlElement(name = "repositories")
    public void setRepositories(RepositoryUrls repositories) {
        this.repositories = repositories;
    }

    public PubRepositoryUrls getPubRepositories() {
        return pubRepositories;
    }

    public void setPubRepositories(PubRepositoryUrls pubRepositories) {
        this.pubRepositories = pubRepositories;
    }

    public void setJournals(JournalUrls journals) {
        this.journals = journals;
    }

    public JournalUrls getJournals() {
        return journals;
    }

    public JournalUrls getJournal() {
        return journals;
    }
    @XmlElement(name = "journals")
    public void setJournal(JournalUrls journal) {
        this.journals = journal;
    }

    public TaxonomiesUrls getTaxonomies() {
        return taxonomies;
    }
    @XmlElement(name = "taxonomies")
    public void setTaxonomies(TaxonomiesUrls taxonomies) {
        this.taxonomies = taxonomies;
    }

    public PublicationsUrls getPublications() {
        return publications;
    }

    public void setPublications(PublicationsUrls publications) {
        this.publications = publications;
    }

    public ServiceUrls getServices() {
        return services;
    }

    @XmlElement(name = "services")
    public void setServices(ServiceUrls services) {
        this.services = services;
    }


    public ResearcherUrls getResearchers() {
        return researchers;
    }

    @XmlElement(name = "researchers")
    public void setResearchers(ResearcherUrls researchers) {
        this.researchers = researchers;
    }


    /*public TagUrls getTags() {
        return tags;
    }

    @XmlElement(name = "tags")
    public void setTags(TagUrls tags) {
        this.tags = tags;
    }*/


    public OrganisationUrls getOrganisations() {
        return organisations;
    }

    @XmlElement(name = "organisations")
    public void setOrganisations(OrganisationUrls organisations) {
        this.organisations = organisations;
    }


    public GrantUrls getGrants() {
        return grants;
    }

    @XmlElement(name = "grants")
    public void setGrants(GrantUrls grants) {
        this.grants = grants;
    }


    public ProjectUrls getProjects() {
        return projects;
    }

    @XmlElement(name = "projects")
    public void setProjects(ProjectUrls projects) {
        this.projects = projects;
    }


    public FunderUrls getFunders() {
        return funders;
    }

    public void setFunders(FunderUrls funders) {
        this.funders = funders;
    }


    public Long getMaxresults() {
        return maxresults;
    }

    @XmlElement(name = "maxresults")
    public void setMaxresults(Long maxresults) {
        this.maxresults = maxresults;
    }


    public DatasetUrls getDatasets() {
        return datasets;
    }

    @XmlElement(name = "datasets")
    public void setDatasets(DatasetUrls datasets) {
        this.datasets = datasets;
    }

    public LicenseUrls getLicenses() {
        return licenses;
    }

    @XmlElement(name = "licenses")
    public void setLicenses(LicenseUrls licenses) {
        this.licenses = licenses;
    }
    
    public ValidationUrls getValidations() {
        return validations;
    }

    @XmlElement(name = "validators")
    public void setValidations(ValidationUrls validations) {
        this.validations = validations;
    }

    public Map<String, PrefillingConfig> getPrefillings() {
        return prefillings;
    }

    @XmlJavaTypeAdapter(PrefillingConfigMapAdapter.class)
    @XmlElement(name = "prefillings")
    public void setPrefillings(Map<String, PrefillingConfig> prefillings) {
        this.prefillings = prefillings;
    }
}


