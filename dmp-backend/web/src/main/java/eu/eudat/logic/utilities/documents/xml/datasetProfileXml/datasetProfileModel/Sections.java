package eu.eudat.logic.utilities.documents.xml.datasetProfileXml.datasetProfileModel;


import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.LinkedList;
import java.util.List;

@XmlRootElement(name = "sections")
public class Sections {
    private String id;
    private int ordinal;
    private String page;
    private Boolean defaultVisibility;
    private String numbering;
    private String description;
    private String title;
    private List<Section> section;
    private FieldSets fieldSets;
    private Boolean multiplicity;

    @XmlAttribute(name = "id")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @XmlAttribute(name = "ordinal")
    public int getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(int ordinal) {
        this.ordinal = ordinal;
    }

    @XmlAttribute(name = "page")
    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    @XmlAttribute(name = "defaultVisibility")
    public Boolean getDefaultVisibility() {
        return defaultVisibility;
    }

    public void setDefaultVisibility(Boolean defaultVisibility) {
        this.defaultVisibility = defaultVisibility;
    }

    @XmlElement(name = "numbering")
    public String getNumbering() {
        return numbering;
    }

    public void setNumbering(String numbering) {
        this.numbering = numbering;
    }

    @XmlElement(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlElement(name = "title")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @XmlElement(name = "section")
    public List<Section> getSection() {
        return section;
    }

    public void setSection(List<Section> section) {
        this.section = section;
    }

    @XmlElement(name = "field-Sets")
    public FieldSets getFieldSets() {
        return fieldSets;
    }

    public void setFieldSets(FieldSets fieldSets) {
        this.fieldSets = fieldSets;
    }

    @XmlAttribute(name = "multiplicity")
    public Boolean getMultiplicity() {
        return multiplicity;
    }

    public void setMultiplicity(Boolean multiplicity) {
        this.multiplicity = multiplicity;
    }

    public eu.eudat.models.data.admin.components.datasetprofile.Section toAdminCompositeModelSection() {
        eu.eudat.models.data.admin.components.datasetprofile.Section sectionEntity = new eu.eudat.models.data.admin.components.datasetprofile.Section();
        List<eu.eudat.models.data.admin.components.datasetprofile.Section> sectionsListEntity = new LinkedList<>();

        if (this.section != null) {
            for (Section xmlsection : this.section) {
                sectionsListEntity.add(xmlsection.toAdminCompositeModelSection());
            }
        } /*else {
            sectionsListEntity.add(new eu.eudat.models.data.admin.components.datasetprofile.Section());
        }*/
        sectionEntity.setId(this.id);
        sectionEntity.setOrdinal(this.ordinal);
        sectionEntity.setTitle(this.title);
        sectionEntity.setDefaultVisibility(this.defaultVisibility);
        sectionEntity.setDescription(description);
        sectionEntity.setPage(this.page);
        sectionEntity.setFieldSets(toAdminCompositeModelSectionFieldSets());
        sectionEntity.setMultiplicity(this.multiplicity);


        sectionEntity.setSections(sectionsListEntity);
        return sectionEntity;
    }

    public List<eu.eudat.models.data.admin.components.datasetprofile.FieldSet> toAdminCompositeModelSectionFieldSets() {
        return fieldSets.toAdminCompositeModelSection();
    }
}

