package eu.eudat.logic.security.validators.zenodo.helpers;

public class ZenodoRequest {
    private String code;

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
}
