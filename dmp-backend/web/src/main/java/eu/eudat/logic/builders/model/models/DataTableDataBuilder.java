package eu.eudat.logic.builders.model.models;

import eu.eudat.logic.builders.Builder;
import eu.eudat.models.data.helpers.common.DataTableData;

import java.util.List;

/**
 * Created by ikalyvas on 2/15/2018.
 */
public class DataTableDataBuilder<T> extends Builder<DataTableData<T>> {

    private Long totalCount;
    private List<T> data;

    public DataTableDataBuilder<T> totalCount(Long totalCount) {
        this.totalCount = totalCount;
        return this;
    }

    public DataTableDataBuilder<T> data(List<T> data) {
        this.data = data;
        return this;
    }

    @Override
    public DataTableData<T> build() {
        DataTableData<T> dataTableData = new DataTableData<>();
        dataTableData.setTotalCount(totalCount);
        dataTableData.setData(data);
        return dataTableData;
    }
}
