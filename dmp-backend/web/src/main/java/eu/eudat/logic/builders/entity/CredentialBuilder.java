package eu.eudat.logic.builders.entity;

import eu.eudat.logic.builders.Builder;
import eu.eudat.data.entities.Credential;
import eu.eudat.data.entities.UserInfo;

import java.util.Date;
import java.util.UUID;

/**
 * Created by ikalyvas on 2/15/2018.
 */
public class CredentialBuilder extends Builder<Credential> {

    private UUID id;

    private UserInfo userInfo;

    private Integer status;

    private Integer provider;

    private String publicValue;

    private String secret;

    private Date creationTime;

    private Date lastUpdateTime;

    private String externalId;

    private String email;

    public CredentialBuilder id(UUID id) {
        this.id = id;
        return this;
    }

    public CredentialBuilder userInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
        return this;
    }

    public CredentialBuilder status(Integer status) {
        this.status = status;
        return this;
    }

    public CredentialBuilder provider(Integer provider) {
        this.provider = provider;
        return this;
    }

    public CredentialBuilder publicValue(String publicValue) {
        this.publicValue = publicValue;
        return this;
    }

    public CredentialBuilder secret(String secret) {
        this.secret = secret;
        return this;
    }

    public CredentialBuilder creationTime(Date creationTime) {
        this.creationTime = creationTime;
        return this;
    }

    public CredentialBuilder lastUpdateTime(Date lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
        return this;
    }

    public CredentialBuilder externalId(String externalId) {
        this.externalId = externalId;
        return this;
    }

    public CredentialBuilder email(String email) {
        this.email = email;
        return this;
    }

    public Credential build() {
        Credential credential = new Credential();
        credential.setStatus(status);
        credential.setLastUpdateTime(lastUpdateTime);
        credential.setCreationTime(creationTime);
        credential.setProvider(provider);
        credential.setSecret(secret);
        credential.setPublicValue(publicValue);
        credential.setUserInfo(userInfo);
        credential.setId(id);
        credential.setExternalId(externalId);
        credential.setEmail(email);
        return credential;
    }
}
