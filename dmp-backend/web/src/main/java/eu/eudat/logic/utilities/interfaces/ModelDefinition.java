package eu.eudat.logic.utilities.interfaces;

import eu.eudat.models.data.entities.xmlmodels.modeldefinition.DatabaseModelDefinition;

public interface ModelDefinition<T extends DatabaseModelDefinition> {
    T toDatabaseDefinition(T item);

    void fromDatabaseDefinition(T item);
}