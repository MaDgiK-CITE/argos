package eu.eudat.logic.builders.model.models;

import eu.eudat.logic.builders.Builder;
import eu.eudat.models.data.dashboard.recent.RecentActivityData;

import java.util.Date;

/**
 * Created by ikalyvas on 3/14/2018.
 */
public class RecentActivityDataBuilder extends Builder<RecentActivityData> {

    private String label;
    private String id;
    private Date timestamp;

    public String getLabel() {
        return label;
    }

    public RecentActivityDataBuilder label(String label) {
        this.label = label;
        return this;
    }

    public String getId() {
        return id;
    }

    public RecentActivityDataBuilder id(String id) {
        this.id = id;
        return this;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public RecentActivityDataBuilder timestamp(Date timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    @Override
    public RecentActivityData build() {
        RecentActivityData recentActivityData = new RecentActivityData();
        recentActivityData.setLabel(label);
        recentActivityData.setTimestamp(timestamp);
        recentActivityData.setId(id);
        return recentActivityData;
    }
}
