package eu.eudat.logic.services.operations;

import eu.eudat.data.dao.entities.*;
import eu.eudat.data.dao.entities.security.CredentialDao;
import eu.eudat.data.dao.entities.security.UserTokenDao;


public interface DatabaseRepository {
    DataRepositoryDao getDataRepositoryDao();

    DatasetDao getDatasetDao();

    DatasetProfileDao getDatasetProfileDao();

    DMPDao getDmpDao();

    DmpDatasetProfileDao getDmpDatasetProfileDao();

    OrganisationDao getOrganisationDao();

    GrantDao getGrantDao();

    RegistryDao getRegistryDao();

    ResearcherDao getResearcherDao();

    ServiceDao getServiceDao();

    UserInfoDao getUserInfoDao();

    UserRoleDao getUserRoleDao();

    InvitationDao getInvitationDao();

    CredentialDao getCredentialDao();

    UserTokenDao getUserTokenDao();

    ExternalDatasetDao getExternalDatasetDao();

    UserDatasetProfileDao getUserDatasetProfileDao();

    UserDmpDao getUserDmpDao();

    ContentDao getContentDao();

    DMPProfileDao getDmpProfileDao();

    DatasetExternalDatasetDao getDatasetExternalDatasetDao();

    DatasetServiceDao getDatasetServiceDao();

    EmailConfirmationDao getLoginConfirmationEmailDao();

    ProjectDao getProjectDao();

    FunderDao getFunderDao();

    LockDao getLockDao();

    NotificationDao getNotificationDao();

    FileUploadDao getFileUploadDao();

    EntityDoiDao getEntityDoiDao();

    DescriptionTemplateTypeDao getDescriptionTemplateTypeDao();

    <T> void detachEntity(T entity);
}
