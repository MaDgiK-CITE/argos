package eu.eudat.logic.proxy.config;

import javax.xml.bind.annotation.XmlElement;

public class QueryConfig {

    private String condition;
    private String separator;
    private String value;
    private Integer ordinal;


    public String getCondition() {
        return condition;
    }

    @XmlElement(name = "condition")
    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getSeparator() {
        return separator;
    }

    @XmlElement(name = "separator")
    public void setSeparator(String separator) {
        this.separator = separator;
    }

    public String getValue() {
        return value;
    }

    @XmlElement(name = "value")
    public void setValue(String value) {
        this.value = value;
    }

    public Integer getOrdinal() {
        return ordinal;
    }

    @XmlElement(name = "ordinal")
    public void setOrdinal(Integer ordinal) {
        this.ordinal = ordinal;
    }
}
