package eu.eudat.logic.proxy.config.entities;

import javax.xml.bind.annotation.XmlAttribute;

public interface PrefillingMapping {
    String getTarget();

    void setTarget(String target);

    String getSemanticTarget();

    void setSemanticTarget(String semanticTarget);

    String getSubSource();

    String getTrimRegex();
}
