package eu.eudat.logic.proxy.fetching;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.spi.json.JacksonJsonProvider;
import eu.eudat.logic.proxy.config.DataUrlConfiguration;
import eu.eudat.logic.proxy.config.ExternalUrlCriteria;
import eu.eudat.logic.proxy.fetching.entities.Results;
import io.swagger.models.auth.In;
import net.minidev.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

public class RemoteFetcherUtils {
    private final static Logger logger = LoggerFactory.getLogger(RemoteFetcherUtils.class);
    private static final ObjectMapper mapper = new ObjectMapper();

    public static Results getFromJson(DocumentContext jsonContext, DataUrlConfiguration jsonDataPath) {
        return new Results(parseData(jsonContext, jsonDataPath),
                new HashMap<>(1, 1));
    }

    public static Results getFromJsonWithRecursiveFetching(DocumentContext jsonContext, DataUrlConfiguration jsonDataPath, RemoteFetcher remoteFetcher, String requestBody, String requestType, String auth) {
        Results results = new Results(parseData(jsonContext, jsonDataPath),
                new HashMap<>(1, 1));

        List<Map<String, String>> multiResults = results.getResults().stream().map(result -> {
            ExternalUrlCriteria externalUrlCriteria = new ExternalUrlCriteria();
            externalUrlCriteria.setPath(result.get("path"));
            externalUrlCriteria.setHost(result.get("host"));
            String replacedPath = remoteFetcher.replaceCriteriaOnUrl(jsonDataPath.getUrlConfiguration().getUrl(), externalUrlCriteria, jsonDataPath.getUrlConfiguration().getFirstpage(), jsonDataPath.getUrlConfiguration().getQueries());
            return remoteFetcher.getResultsFromUrl(replacedPath, jsonDataPath.getUrlConfiguration().getData(), jsonDataPath.getUrlConfiguration().getData().getPath(), jsonDataPath.getUrlConfiguration().getContentType(), requestBody, requestType, auth);
        }).filter(Objects::nonNull).map(results1 -> results1.getResults().get(0)).collect(Collectors.toList());
        return new Results(multiResults, new HashMap<>(1, 1));
    }

    public static Results getFromJsonWithFirstAndLastName(DocumentContext jsonContext, DataUrlConfiguration jsonDataPath) {
        Results results = new Results(parseData(jsonContext, jsonDataPath),
                new HashMap<>(1, 1));
        results.getResults().stream().forEach(entry -> {
            String name = entry.get(jsonDataPath.getFieldsUrlConfiguration().getFirstName().replace("'", "")) + " " + entry.get(jsonDataPath.getFieldsUrlConfiguration().getLastName().replace("'", ""));
            entry.put("name", name);
            entry.remove(jsonDataPath.getFieldsUrlConfiguration().getFirstName().replace("'", ""));
            entry.remove(jsonDataPath.getFieldsUrlConfiguration().getLastName().replace("'", ""));
        });
        return results;
    }

    private static List<Map<String, String>> parseData (DocumentContext jsonContext, DataUrlConfiguration jsonDataPath) {
        List <Map<String, Object>> rawData = jsonContext.read(jsonDataPath.getPath());
        List<Map<String, String>> parsedData = new ArrayList<>();
        rawData.forEach(stringObjectMap -> {
            parsedData.add(new LinkedHashMap<>());
            Arrays.stream(jsonDataPath.getFieldsUrlConfiguration().getClass().getDeclaredFields()).forEach(field -> {
                String getterMethodName = "get" + field.getName().substring(0, 1).toUpperCase(Locale.ROOT) + field.getName().substring(1);
                Method getterMethod = Arrays.stream(jsonDataPath.getFieldsUrlConfiguration().getClass().getDeclaredMethods()).filter(method -> method.getName().equals(getterMethodName)).collect(Collectors.toList()).get(0);
                try {
                    String value = ((String) getterMethod.invoke(jsonDataPath.getFieldsUrlConfiguration()));
                    if (value != null) {
                        if (field.getName().equals("pid") || field.getName().equals("pidTypeField")) {
                            String pid = null;
                            Object pidObj = stringObjectMap.get(value.split("\\.")[0]);
                            if(pidObj != null){
                                if(pidObj instanceof Map){
                                    Object o = ((Map<String, Object>) pidObj).get(value.split("\\.")[1]);
                                    if(o instanceof String){
                                        pid = (String)o;
                                    }
                                    else if(o instanceof Integer){
                                        pid = String.valueOf(o);
                                    }
                                }
                                else if(pidObj instanceof List){
                                    Object o = ((List<Map<String,?>>) pidObj).get(0).get(value.split("\\.")[1]);
                                    if(o instanceof String){
                                        pid = (String)o;
                                    }
                                    else if(o instanceof Integer){
                                        pid = String.valueOf(o);
                                    }
                                }
                            }
                            if(pid != null) {
                                if ((field.getName().equals("pid"))){
                                    parsedData.get(parsedData.size() - 1).put("pid", pid);
                                }
                                else{
                                    parsedData.get(parsedData.size() - 1).put("pidTypeField", pid);
                                }
                            }
                        } else {
                            value = value.replace("'", "");
                            if (value.contains(".")) {
                                String[] parts = value.split("\\.");
                                Map<String, Object> tempMap = stringObjectMap;
                                for (int i = 0; i < parts.length; i++) {
                                    if (tempMap.containsKey(parts[i])) {
                                        if (i + 1 < parts.length) {
                                            tempMap = (Map<String, Object>) tempMap.get(parts[i]);
                                        } else {
                                            parsedData.get(parsedData.size() - 1).put(field.getName().equals("types") ? "tags" : value, normalizeValue(tempMap.get(parts[i]), (field.getName().equals("types") || field.getName().equals("uri"))));
                                        }
                                    }
                                }
                            } else {
                                if (stringObjectMap.containsKey(value)) {
                                    parsedData.get(parsedData.size() - 1).put(field.getName().equals("types") ? "tags" : value, normalizeValue(stringObjectMap.get(value), (field.getName().equals("types") || field.getName().equals("uri"))));
                                }
                            }
                        }
                    }
                } catch (IllegalAccessException | InvocationTargetException e) {
                    logger.error(e.getLocalizedMessage(), e);
                }
            });
        });
        return parsedData;
    }

    private static String normalizeValue(Object value, boolean jsonString) {
        if (value instanceof JSONArray) {
            if (jsonString) {
                return ((JSONArray)value).toJSONString();
            }
            JSONArray jarr = (JSONArray) value;
            if (jarr.get(0) instanceof String) {
                return jarr.get(0).toString();
            } else {
                for (Object o : jarr) {
                    if ((o instanceof Map) && ((Map) o).containsKey("content")) {
                        try {
                            return ((Map<String, String>) o).get("content");
                        }
                        catch (ClassCastException e){
                            if(((Map<?, ?>) o).get("content") instanceof Integer) {
                                return String.valueOf(((Map<?, ?>) o).get("content"));
                            }
                            return null;
                        }
                    }
                }
            }
        } else if (value instanceof Map) {
            String key = ((Map<String, String>)value).containsKey("$") ? "$" : "content";
            return ((Map<String, String>)value).get(key);
        }
        return value != null ? value.toString() : null;
    }
}
