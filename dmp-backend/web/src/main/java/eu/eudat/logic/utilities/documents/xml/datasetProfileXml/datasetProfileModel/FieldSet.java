package eu.eudat.logic.utilities.documents.xml.datasetProfileXml.datasetProfileModel;

import eu.eudat.logic.utilities.documents.xml.datasetProfileXml.datasetProfileModel.Fields.Fields;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "field-Set")
public class FieldSet {

    private String id;
    private int ordinal;
    private Fields fields;
    private String numbering;
    private Boolean commentField;
    private Multiplicity multiplicity;
    private String description;
    private String extendedDescription;
    private String additionalInformation;
    private String title;

    @XmlAttribute(name = "id")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @XmlAttribute(name = "ordinal")
    public int getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(int ordinal) {
        this.ordinal = ordinal;
    }

    @XmlElement(name = "fields")
    public Fields getFields() {
        return fields;
    }

    public void setFields(Fields fields) {
        this.fields = fields;
    }

    @XmlElement(name = "numbering")
    public String getNumbering() {
        return numbering;
    }

    public void setNumbering(String numbering) {
        this.numbering = numbering;
    }

    @XmlElement(name = "CommentField")
    public Boolean getCommentField() {
        return commentField;
    }

    public void setCommentField(Boolean commentField) {
        this.commentField = commentField;
    }

    @XmlElement(name = "multiplicity")
    public Multiplicity getMultiplicity() {
        return multiplicity;
    }

    public void setMultiplicity(Multiplicity multiplicity) {
        this.multiplicity = multiplicity;
    }

    @XmlElement(name = "title")
    public String getTitle() {
        return title;
    }

    @XmlElement(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlElement(name = "extendedDescription")
    public String getExtendedDescription() {
        return extendedDescription;
    }

    public void setExtendedDescription(String extendedDescription) {
        this.extendedDescription = extendedDescription;
    }

    @XmlElement(name = "additionalInformation")
    public String getAdditionalInformation() {
        return additionalInformation;
    }

    public void setAdditionalInformation(String additionalInformation) {
        this.additionalInformation = additionalInformation;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public eu.eudat.models.data.admin.components.datasetprofile.FieldSet toAdminCompositeModelSection() {
        eu.eudat.models.data.admin.components.datasetprofile.FieldSet fieldSet1Entity = new eu.eudat.models.data.admin.components.datasetprofile.FieldSet();
        fieldSet1Entity.setId(this.id);
        fieldSet1Entity.setOrdinal(this.ordinal);
        fieldSet1Entity.setHasCommentField(this.commentField != null ? this.commentField : false);
        fieldSet1Entity.setMultiplicity(this.multiplicity != null ? this.multiplicity.toAdminCompositeModelSection() : null);
        fieldSet1Entity.setTitle(this.title);
        fieldSet1Entity.setDescription(this.description);
        fieldSet1Entity.setExtendedDescription(this.extendedDescription);
        fieldSet1Entity.setAdditionalInformation(this.additionalInformation);

        fieldSet1Entity.setFields(this.fields.toAdminCompositeModelSection());
        return fieldSet1Entity;
    }
}
