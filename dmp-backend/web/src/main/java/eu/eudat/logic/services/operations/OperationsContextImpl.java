package eu.eudat.logic.services.operations;

import eu.eudat.elastic.repository.DatasetRepository;
import eu.eudat.elastic.repository.DmpRepository;
import eu.eudat.logic.builders.BuilderFactory;
import eu.eudat.logic.proxy.fetching.RemoteFetcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

/**
 * Created by ikalyvas on 3/1/2018.
 */
@Service("operationsContext")
public class OperationsContextImpl implements OperationsContext {

    private final DatabaseRepository databaseRepository;
    private final ApplicationContext applicationContext;
    private final RemoteFetcher remoteFetcher;
    private final BuilderFactory builderFactory;
//    private final FileStorageService fileStorageService;
    private final ElasticRepository elasticRepository;

    @Autowired
    public OperationsContextImpl(DatabaseRepository databaseRepository, ApplicationContext applicationContext, RemoteFetcher remoteFetcher
            , BuilderFactory builderFactory, /*FileStorageService fileStorageService,*/ ElasticRepository elasticRepository) {
        this.databaseRepository = databaseRepository;
        this.applicationContext = applicationContext;
        this.remoteFetcher = remoteFetcher;
        this.builderFactory = builderFactory;
//        this.fileStorageService = fileStorageService;
        this.elasticRepository = elasticRepository;
    }

    @Override
    public DatabaseRepository getDatabaseRepository() {
        return databaseRepository;
    }

    @Override
    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    @Override
    public RemoteFetcher getRemoteFetcher() {
        return remoteFetcher;
    }

    @Override
    public BuilderFactory getBuilderFactory() {
        return builderFactory;
    }

//    @Override
//    public FileStorageService getFileStorageService() {
//        return fileStorageService;
//    }

    @Override
    public ElasticRepository getElasticRepository() {
        return elasticRepository;
    }
}
