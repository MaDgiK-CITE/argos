package eu.eudat.logic.services;

import eu.eudat.logic.services.helpers.HelpersService;
import eu.eudat.logic.services.operations.OperationsContext;
import eu.eudat.logic.services.utilities.UtilitiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service("apiContext")
public class ApiContextImpl implements ApiContext {

    private OperationsContext operationsContext;
    private HelpersService helpersService;
    private UtilitiesService utilitiesService;

    @Autowired
    public ApiContextImpl(OperationsContext operationsContext, HelpersService helpersService, UtilitiesService utilitiesService) {
        this.operationsContext = operationsContext;
        this.helpersService = helpersService;
        this.utilitiesService = utilitiesService;
    }

    @Override
    public OperationsContext getOperationsContext() {
        return operationsContext;
    }

    @Override
    public HelpersService getHelpersService() {
        return helpersService;
    }

    @Override
    public UtilitiesService getUtilitiesService() {
        return utilitiesService;
    }
}
