package eu.eudat.logic.security.customproviders.ConfigurableProvider.entities.oauth2;

import eu.eudat.logic.security.customproviders.ConfigurableProvider.entities.ConfigurableProvider;

public class Oauth2ConfigurableProvider extends ConfigurableProvider {

	private String clientId;
	private String clientSecret;
	private String redirect_uri;
	private String access_token_url;
	private String grant_type;
	private Oauth2ConfigurableProviderToken token;
	private Oauth2ConfigurableProviderUserSettings user;
	private String oauthUrl;
	private String scope;
	private String state;

	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getClientSecret() {
		return clientSecret;
	}
	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	public String getRedirect_uri() {
		return redirect_uri;
	}
	public void setRedirect_uri(String redirect_uri) {
		this.redirect_uri = redirect_uri;
	}

	public String getAccess_token_url() {
		return access_token_url;
	}
	public void setAccess_token_url(String access_token_url) {
		this.access_token_url = access_token_url;
	}

	public String getGrant_type() {
		return grant_type;
	}
	public void setGrant_type(String grant_type) {
		this.grant_type = grant_type;
	}

	public Oauth2ConfigurableProviderToken getToken() {
		return token;
	}
	public void setToken(Oauth2ConfigurableProviderToken token) {
		this.token = token;
	}

	public Oauth2ConfigurableProviderUserSettings getUser() {
		return user;
	}
	public void setUser(Oauth2ConfigurableProviderUserSettings user) {
		this.user = user;
	}

	public String getOauthUrl() {
		return oauthUrl;
	}
	public void setOauthUrl(String oauthUrl) {
		this.oauthUrl = oauthUrl;
	}

	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}

}
