package eu.eudat.logic.proxy.config.entities;

import eu.eudat.logic.proxy.config.FetchStrategy;
import eu.eudat.logic.proxy.config.UrlConfiguration;

import java.util.ArrayList;
import java.util.List;

public class GeneralUrls extends GenericUrls{

    List<UrlConfiguration> urls;
    FetchStrategy fetchMode;

    public GeneralUrls() {
        this.urls = new ArrayList<>();
    }

    @Override
    public List<UrlConfiguration> getUrls() {
        return urls;
    }

    @Override
    public FetchStrategy getFetchMode() {
        return fetchMode;
    }

    public void setFetchMode(FetchStrategy fetchMode) {
        this.fetchMode = fetchMode;
    }
}
