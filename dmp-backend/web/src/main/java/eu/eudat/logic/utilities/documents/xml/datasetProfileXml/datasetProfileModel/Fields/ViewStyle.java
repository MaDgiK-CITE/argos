package eu.eudat.logic.utilities.documents.xml.datasetProfileXml.datasetProfileModel.Fields;


import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "viewStyle")
public class ViewStyle {

    private String cssClass;
    private String renderStyle;

    @XmlAttribute(name = "cssClass")
    public String getCssClass() {
        return cssClass;
    }

    public void setCssClass(String cssClass) {
        this.cssClass = cssClass;
    }
    @XmlAttribute(name = "renderStyle")
    public String getRenderStyle() {
        return renderStyle;
    }

    public void setRenderStyle(String renderStyle) {
        this.renderStyle = renderStyle;
    }

    public eu.eudat.models.data.components.commons.ViewStyle toAdminCompositeModelSection(){
        eu.eudat.models.data.components.commons.ViewStyle viewStyleEntity = new eu.eudat.models.data.components.commons.ViewStyle();
        viewStyleEntity.setCssClass(this.cssClass);
        viewStyleEntity.setRenderStyle(this.renderStyle);
        return viewStyleEntity;
    }
}