package eu.eudat.logic.services.utilities;

import eu.eudat.logic.services.forms.VisibilityRuleService;
import eu.eudat.logic.services.forms.VisibilityRuleServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by ikalyvas on 3/1/2018.
 */
@Service("utilitiesService")
public class UtilitiesServiceImpl implements UtilitiesService {

    private InvitationService invitationService;
    private MailService mailService;
    private ConfirmationEmailService confirmationEmailService;

    @Autowired
    public UtilitiesServiceImpl(InvitationService invitationService, MailService mailService, ConfirmationEmailService confirmationEmailService) {
        this.invitationService = invitationService;
        this.mailService = mailService;
        this.confirmationEmailService = confirmationEmailService;
    }

    @Override
    public ConfirmationEmailService getConfirmationEmailService() {
        return confirmationEmailService;
    }

    @Override
    public InvitationService getInvitationService() {
        return invitationService;
    }

    @Override
    public MailService getMailService() {
        return mailService;
    }
}
