package eu.eudat.logic.proxy.fetching.entities;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "config")
public class ConfigSingle {
	private String type;
	private String fileType;
	private String filePath;
	private String parseClass;
	private String parseField;
	private String name;
	private String value;

	@XmlElement(name = "type")
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	@XmlElement(name = "fileType")
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	@XmlElement(name = "filePath")
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	@XmlElement(name = "parseClass")
	public String getParseClass() {
		return parseClass;
	}
	public void setParseClass(String parseClass) {
		this.parseClass = parseClass;
	}

	@XmlElement(name = "parseField")
	public String getParseField() {
		return parseField;
	}
	public void setParseField(String parseField) {
		this.parseField = parseField;
	}

	@XmlElement(name = "name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name = "value")
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
