package eu.eudat.logic.security.validators.orcid.helpers;

public class ORCIDRequest {
    private String code;

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
}
