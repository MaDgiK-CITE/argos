package eu.eudat.logic.security.customproviders.OpenAIRE;

import java.util.Map;

public class OpenAIREUser {
	private String id;
	private String name;
	private String email;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public OpenAIREUser getOpenAIREUser(Object data) {
		this.id = (String) ((Map) data).get("sub");
		this.name = (String) ((Map) data).get("name");
		this.email = (String) ((Map) data).get("email");
		return this;
	}
}
