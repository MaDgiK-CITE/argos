package eu.eudat.logic.security.validators.b2access.helpers;

/**
 * Created by ikalyvas on 2/22/2018.
 */
public class B2AccessRequest {
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
