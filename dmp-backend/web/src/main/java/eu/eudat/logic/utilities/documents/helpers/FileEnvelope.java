package eu.eudat.logic.utilities.documents.helpers;

import java.io.File;

/**
 * Created by ikalyvas on 3/6/2018.
 */
public class FileEnvelope {
    private String filename;
    private File file;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
}
