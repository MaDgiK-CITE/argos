package eu.eudat.logic.utilities.documents.xml.dmpXml.dmpBlueprintModel;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "extraFields")
public class ExtraFields {

    private List<ExtraField> extraFields;

    @XmlElement(name = "extraField")
    public List<ExtraField> getExtraFields() {
        return extraFields;
    }
    public void setExtraFields(List<ExtraField> extraFields) {
        this.extraFields = extraFields;
    }

}
