package eu.eudat.logic.security.customproviders.ConfigurableProvider.models.saml2;

import eu.eudat.logic.security.customproviders.ConfigurableProvider.entities.ConfigurableProvider;
import eu.eudat.logic.security.customproviders.ConfigurableProvider.entities.saml2.Saml2ConfigurableProvider;
import eu.eudat.logic.security.customproviders.ConfigurableProvider.models.ConfigurableProviderModel;

public class Saml2ConfigurableProviderModel extends ConfigurableProviderModel {

	private String spEntityId;
	private String idpUrl;
	private String binding;
	private String assertionConsumerServiceUrl;

	public String getSpEntityId() {
		return spEntityId;
	}
	public void setSpEntityId(String spEntityId) {
		this.spEntityId = spEntityId;
	}

	public String getIdpUrl() {
		return idpUrl;
	}
	public void setIdpUrl(String idpUrl) {
		this.idpUrl = idpUrl;
	}

	public String getBinding() {
		return binding;
	}
	public void setBinding(String binding) {
		this.binding = binding;
	}

	public String getAssertionConsumerServiceUrl() {
		return assertionConsumerServiceUrl;
	}
	public void setAssertionConsumerServiceUrl(String assertionConsumerServiceUrl) {
		this.assertionConsumerServiceUrl = assertionConsumerServiceUrl;
	}

	@Override
	public Saml2ConfigurableProviderModel fromDataModel(ConfigurableProvider entity) {
		Saml2ConfigurableProviderModel model = new Saml2ConfigurableProviderModel();
		model.setConfigurableLoginId(entity.getConfigurableLoginId());
		model.setType(entity.getType());
		model.setName(entity.getName());
		model.setLogoUrl(entity.getLogoUrl());
		model.setSpEntityId(((Saml2ConfigurableProvider)entity).getSpEntityId());
		model.setIdpUrl(((Saml2ConfigurableProvider)entity).getIdpUrl());
		model.setBinding(((Saml2ConfigurableProvider)entity).getBinding());
		model.setAssertionConsumerServiceUrl(((Saml2ConfigurableProvider)entity).getAssertionConsumerServiceUrl());
		return model;
	}

}
