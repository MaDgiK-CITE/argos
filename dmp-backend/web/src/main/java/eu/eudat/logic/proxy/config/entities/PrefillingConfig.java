package eu.eudat.logic.proxy.config.entities;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "config")
public class PrefillingConfig {

    private PrefillingSearch prefillingSearch;
    private PrefillingGet prefillingGet;

    private String type;

    public PrefillingSearch getPrefillingSearch() {
        return prefillingSearch;
    }

    @XmlElement(name = "prefillingSearch")
    public void setPrefillingSearch(PrefillingSearch prefillingSearch) {
        this.prefillingSearch = prefillingSearch;
    }

    public PrefillingGet getPrefillingGet() {
        return prefillingGet;
    }

    @XmlElement(name = "prefillingGet")
    public void setPrefillingGet(PrefillingGet prefillingGet) {
        this.prefillingGet = prefillingGet;
    }

    @XmlAttribute(name = "type")
    public void setType(String type) {
        this.type = type;
    }

    public String getType(){
        return type;
    }
}
