package eu.eudat.logic.mapper.elastic;

import eu.eudat.data.entities.UserInfo;
import eu.eudat.elastic.entities.Collaborator;

public class CollaboratorMapper {

	public static Collaborator toElastic(UserInfo user, Integer role) {
		Collaborator elastic = new Collaborator();
		elastic.setId(user.getId().toString());
		elastic.setName(user.getName());
		elastic.setRole(role);
		return elastic;
	}
}
