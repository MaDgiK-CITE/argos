package eu.eudat.logic.services.forms;

import eu.eudat.models.data.user.components.commons.Rule;

import java.util.List;
import java.util.Map;

/**
 * Created by ikalyvas on 3/5/2018.
 */
public interface VisibilityRuleService {
    boolean isElementVisible(String id);

    void buildVisibilityContext(List<Rule> sources);

    void setProperties(Map<String, Object> properties);
}
