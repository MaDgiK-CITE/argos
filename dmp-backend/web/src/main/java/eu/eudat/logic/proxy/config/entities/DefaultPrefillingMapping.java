package eu.eudat.logic.proxy.config.entities;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "mapping")
public class DefaultPrefillingMapping implements PrefillingMapping{
    private String source;
    private String target;
    private String semanticTarget;
    private String subSource;
    private String trimRegex;

    public String getSource() {
        return source;
    }

    @XmlAttribute(name = "source")
    public void setSource(String source) {
        this.source = source;
    }

    public String getTarget() {
        return target;
    }

    @XmlAttribute(name = "target")
    public void setTarget(String target) {
        this.target = target;
    }

    public String getSemanticTarget() {
        return semanticTarget;
    }

    @XmlAttribute(name = "semanticTarget")
    public void setSemanticTarget(String semanticTarget) {
        this.semanticTarget = semanticTarget;
    }

    public String getSubSource() {
        return subSource;
    }

    @XmlAttribute(name = "subSource")
    public void setSubSource(String subSource) {
        this.subSource = subSource;
    }

    public String getTrimRegex() {
        return trimRegex;
    }

    @XmlAttribute(name = "trimRegex")
    public void setTrimRegex(String trimRegex) {
        this.trimRegex = trimRegex;
    }
}
