package eu.eudat.logic.builders;

/**
 * Created by ikalyvas on 2/15/2018.
 */
public abstract class Builder<T> {
    public abstract T build();
}
