package eu.eudat.logic.security.customproviders.ConfigurableProvider;

import eu.eudat.logic.security.customproviders.ConfigurableProvider.entities.oauth2.Oauth2ConfigurableProviderUserSettings;
import eu.eudat.logic.security.validators.configurableProvider.helpers.ConfigurableProviderResponseToken;

public interface ConfigurableProviderCustomProvider {

	ConfigurableProviderResponseToken getAccessToken(String code, String redirectUri, String clientId, String clientSecret, String accessTokenUrl, String grantType, String access_token, String expires_in);

	ConfigurableProviderUser getUser(String accessToken, Oauth2ConfigurableProviderUserSettings user);
}
