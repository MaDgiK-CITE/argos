package eu.eudat.logic.proxy.fetching.entities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Results {
    List<Map<String, String>> results;
    Map<String, Integer> pagination;

    public Results() {
        this.results = new ArrayList<>();
        this.pagination = new HashMap<>();
    }

    public Results(List<Map<String, String>> results, Map<String, Integer> pagination) {
        this.results = results;
        this.pagination = pagination;
    }

    public List<Map<String, String>> getResults() {
        return results;
    }

    public void setResults(List<Map<String, String>> results) {
        this.results = results;
    }

    public Map<String, Integer> getPagination() {
        return pagination;
    }

    public void setPagination(Map<String, Integer> pagination) {
        this.pagination = pagination;
    }
}
