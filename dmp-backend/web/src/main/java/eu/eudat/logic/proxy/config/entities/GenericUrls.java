package eu.eudat.logic.proxy.config.entities;

import eu.eudat.logic.proxy.config.FetchStrategy;
import eu.eudat.logic.proxy.config.UrlConfiguration;

import java.util.List;

public abstract class GenericUrls {

    public abstract List<UrlConfiguration> getUrls();
    public abstract FetchStrategy getFetchMode();
}
