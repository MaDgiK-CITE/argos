package eu.eudat.logic.proxy.config.entities;

import eu.eudat.logic.proxy.config.FetchStrategy;
import eu.eudat.logic.proxy.config.UrlConfiguration;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.List;

public class FunderUrls extends GenericUrls{

	List<UrlConfiguration> urls;
	FetchStrategy fetchMode;

	public List<UrlConfiguration> getUrls() {
		return urls;
	}

	@XmlElementWrapper
	@XmlElement(name = "urlConfig")
	public void setUrls(List<UrlConfiguration> urls) {
		this.urls = urls;
	}

	public FetchStrategy getFetchMode() {
		return fetchMode;
	}

	@XmlElement(name = "fetchMode")
	public void setFetchMode(FetchStrategy fetchMode) {
		this.fetchMode = fetchMode;
	}
}
