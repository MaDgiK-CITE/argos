package eu.eudat.logic.utilities.json;

public class JavaToJson {

	public static String objectStringToJson(String object) {
		String result = object.replaceAll("=", "\":\"")
				.replaceAll("\\{", "{\"")
				.replaceAll(", ", "\", \"")
				.replaceAll("}", "\"}" )
				.replaceAll("}\", \"\\{", "}, {");
		return result;
	}
}
