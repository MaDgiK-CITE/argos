package eu.eudat.logic.security.validators.orcid.helpers;

public class ORCIDResponseToken {
    private String orcidId;
    private String name;
    private String accessToken;

    public String getOrcidId() {
        return orcidId;
    }
    public void setOrcidId(String orcidId) {
        this.orcidId = orcidId;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getAccessToken() {
        return accessToken;
    }
    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
