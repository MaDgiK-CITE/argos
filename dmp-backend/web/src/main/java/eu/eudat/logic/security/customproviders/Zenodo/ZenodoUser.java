package eu.eudat.logic.security.customproviders.Zenodo;

import java.util.Map;

public class ZenodoUser {
    private String userId;
    private String email;
    private String accessToken;
    private Integer expiresIn;
    private String refreshToken;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public Integer getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(Integer expiresIn) {
        this.expiresIn = expiresIn;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public ZenodoUser getZenodoUser(Object data) {
        this.userId = (String) ((Map) data).get("userId");
        this.email = (String) ((Map) data).get("email");
        this.accessToken = (String) ((Map) data).get("accessToken");
        this.expiresIn = (Integer) ((Map) data).get("expiresIn");
        this.refreshToken = (String) ((Map) data).get("refreshToken");
        return this;
    }
}
