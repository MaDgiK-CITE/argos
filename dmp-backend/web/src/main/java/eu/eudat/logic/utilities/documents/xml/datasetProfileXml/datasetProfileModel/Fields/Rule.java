package eu.eudat.logic.utilities.documents.xml.datasetProfileXml.datasetProfileModel.Fields;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "rule")
public class Rule{

    private String ruleStyle;
    private String target;
    private String type;
    private Value value;

    @XmlAttribute(name = "ruleStyle")
    public String getRuleStyle() {
        return ruleStyle;
    }

    public void setRuleStyle(String ruleStyle) {
        this.ruleStyle = ruleStyle;
    }
    @XmlAttribute(name = "target")
    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }
    @XmlAttribute(name = "type")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    @XmlElement(name = "value")
    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }

    public eu.eudat.models.data.components.commons.Rule toAdminCompositeModelSection(){
        eu.eudat.models.data.components.commons.Rule ruleEntity = new eu.eudat.models.data.components.commons.Rule();
        ruleEntity.setRuleStyle(ruleStyle);
        ruleEntity.setTarget(target);
        ruleEntity.setRuleType(type);
        ruleEntity.setValueType(value.getType());
        ruleEntity.setValue(value.getValue());
    return ruleEntity;
    }
}