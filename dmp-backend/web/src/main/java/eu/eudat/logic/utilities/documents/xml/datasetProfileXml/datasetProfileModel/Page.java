package eu.eudat.logic.utilities.documents.xml.datasetProfileXml.datasetProfileModel;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "page")
public class Page {
    private String id;
    private int ordinal;
    private String title;
    private List<Sections> sections;

    @XmlAttribute(name = "id")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @XmlAttribute(name = "ordinal")
    public int getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(int ordinal) {
        this.ordinal = ordinal;
    }

    @XmlAttribute(name = "title")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @XmlElement(name = "sections")
    public List<Sections> getSections() {
        return sections;
    }

    public void setSections(List<Sections> sections) {
        this.sections = sections;
    }

    public eu.eudat.models.data.admin.components.datasetprofile.Page toAdminCompositeModelPage(){
        eu.eudat.models.data.admin.components.datasetprofile.Page pageEntity = new eu.eudat.models.data.admin.components.datasetprofile.Page();
        pageEntity.setId(this.id);
        pageEntity.setOrdinal(this.ordinal);
        pageEntity.setTitle(this.title);
        return pageEntity;
    }

    public eu.eudat.models.data.admin.components.datasetprofile.Section toAdminCompositeModelSection(int i){
     /*   eu.eudat.models.data.admin.components.datasetprofile.Section sectionEntity =new eu.eudat.models.data.admin.components.datasetprofile.Section();
//        List<eu.eudat.models.data.admin.components.datasetprofile.Section> sectionsListEntity = new LinkedList<>();
//        for (Section xmlsection:this.sections.section) {
//            sectionsListEntity.add(xmlsection.toAdminCompositeModelSection());
//        }
        if(this.sections.section!=null)
        sectionEntity.setSections(this.sections.toAdminCompositeModelSection());
        if(this.sections.fieldSets.fieldSet!=null)
        sectionEntity.setFieldSets(this.sections.toAdminCompositeModelSectionFieldSets());
        sectionEntity.setId(this.id);
        sectionEntity.setOrdinal(this.ordinal);
        sectionEntity.setTitle(this.title);*/
        return sections.get(i).toAdminCompositeModelSection();
    }
}
