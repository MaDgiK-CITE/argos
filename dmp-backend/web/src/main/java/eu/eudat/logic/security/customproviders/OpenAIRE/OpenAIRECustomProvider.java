package eu.eudat.logic.security.customproviders.OpenAIRE;

import eu.eudat.logic.security.validators.openaire.helpers.OpenAIREResponseToken;

public interface OpenAIRECustomProvider {

	OpenAIREResponseToken getAccessToken(String code, String redirectUri, String clientId, String clientSecret);

	OpenAIREUser getUser(String accessToken);
}
