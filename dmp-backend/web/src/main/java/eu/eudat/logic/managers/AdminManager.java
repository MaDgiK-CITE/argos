package eu.eudat.logic.managers;

import eu.eudat.data.dao.entities.DatasetDao;
import eu.eudat.data.dao.entities.DatasetProfileDao;
import eu.eudat.data.entities.DescriptionTemplate;
import eu.eudat.data.entities.DescriptionTemplateType;
import eu.eudat.exceptions.datasetprofile.DatasetProfileWithDatasetsExeption;
import eu.eudat.logic.builders.entity.DatasetProfileBuilder;
import eu.eudat.models.data.entities.xmlmodels.datasetprofiledefinition.ViewStyleModel;
import eu.eudat.models.data.admin.composite.DatasetProfile;
import eu.eudat.logic.services.ApiContext;
import eu.eudat.logic.utilities.builders.ModelBuilder;
import eu.eudat.logic.utilities.builders.XmlBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.util.Date;
import java.util.UUID;

public class AdminManager {

    public static DescriptionTemplate generateViewStyleDefinition(DatasetProfile profile, ApiContext apiContext) throws Exception {
        ViewStyleModel viewStyleModel = new ViewStyleModel();
        viewStyleModel.setEnablePrefilling(profile.isEnablePrefilling());
        viewStyleModel.setSections(new ModelBuilder().toViewStyleDefinition(profile.getSections(), eu.eudat.models.data.entities.xmlmodels.datasetprofiledefinition.Section.class));
        viewStyleModel.setPages(new ModelBuilder().toViewStyleDefinition(profile.getPages(), eu.eudat.models.data.entities.xmlmodels.datasetprofiledefinition.Page.class));
        Document viewStyleDoc = XmlBuilder.getDocument();
        Element elementViewStyle = viewStyleModel.toXml(viewStyleDoc);
        viewStyleDoc.appendChild(elementViewStyle);
        String xml = XmlBuilder.generateXml(viewStyleDoc);

        if (profile.getDescription() == null) {
            profile.setDescription("");
        }

        if (profile.getLanguage() == null) {
            profile.setLanguage("en");
        }

        DescriptionTemplateType type;
        try {
            type = apiContext.getOperationsContext().getDatabaseRepository().getDescriptionTemplateTypeDao().findFromName(profile.getType());
        }
        catch (Exception e) {
            throw new Exception("Description template type '" + profile.getType() + "' could not be found.");
        }

        DescriptionTemplate descriptionTemplate = apiContext.getOperationsContext().getBuilderFactory().getBuilder(DatasetProfileBuilder.class).definition(xml).label(profile.getLabel())
                .status(profile.getStatus()).created(new Date()).description(profile.getDescription()).language(profile.getLanguage())
                .type(type)
                .build();

        if (descriptionTemplate.getGroupId() == null) {
            descriptionTemplate.setGroupId(UUID.randomUUID());
        }

        if (descriptionTemplate.getVersion() == null) {
            descriptionTemplate.setVersion((short)1);
        }

        return descriptionTemplate;
    }

    public static eu.eudat.models.data.admin.composite.DatasetProfile generateDatasetProfileModel(DescriptionTemplate profile) {
        Document viewStyleDoc = XmlBuilder.fromXml(profile.getDefinition());
        Element root = viewStyleDoc.getDocumentElement();
        eu.eudat.models.data.entities.xmlmodels.datasetprofiledefinition.ViewStyleModel viewstyle = new eu.eudat.models.data.entities.xmlmodels.datasetprofiledefinition.ViewStyleModel().fromXml(root);

        eu.eudat.models.data.admin.composite.DatasetProfile datasetprofile = new eu.eudat.models.data.admin.composite.DatasetProfile();
        datasetprofile.buildProfile(viewstyle);
        return datasetprofile;
    }


    public static DescriptionTemplate inactivate(DatasetProfileDao datasetProfileRepository, DatasetDao datasetDao, String id) {
        eu.eudat.data.dao.criteria.DatasetCriteria datasetsForThatDatasetProfile = new eu.eudat.data.dao.criteria.DatasetCriteria();
        datasetsForThatDatasetProfile.setProfileDatasetId(UUID.fromString(id));
        if (datasetDao.getWithCriteria(datasetsForThatDatasetProfile).count() == 0) {
            DescriptionTemplate detasetProfile = datasetProfileRepository.find(UUID.fromString(id));
            detasetProfile.setStatus(DescriptionTemplate.Status.DELETED.getValue());
            detasetProfile = datasetProfileRepository.createOrUpdate(detasetProfile);
            return detasetProfile;
        } else {
            throw new DatasetProfileWithDatasetsExeption("This profile can not deleted, because Datasets are associated with it");
        }
    }
}