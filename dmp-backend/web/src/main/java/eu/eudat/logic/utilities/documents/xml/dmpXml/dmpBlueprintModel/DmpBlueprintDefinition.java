package eu.eudat.logic.utilities.documents.xml.dmpXml.dmpBlueprintModel;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.LinkedList;
import java.util.List;

@XmlRootElement(name = "definition")
public class DmpBlueprintDefinition {

    private Sections sections;

    @XmlElement(name = "sections")
    public Sections getSections() {
        return sections;
    }
    public void setSections(Sections sections) {
        this.sections = sections;
    }


    public eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition.DataManagementPlanBlueprint toDmpBlueprintCompositeModel() {
        eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition.DataManagementPlanBlueprint dmpBlueprint = new eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition.DataManagementPlanBlueprint();
        List<eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition.Section> dmpBlueprintSections = new LinkedList<>();
        if (this.sections != null && this.sections.getSections() != null) {
            for (Section section : this.sections.getSections()) {
                dmpBlueprintSections.add(section.toDmpBlueprintCompositeModel());
            }
        }
        dmpBlueprint.setSections(dmpBlueprintSections);
        return dmpBlueprint;
    }

}