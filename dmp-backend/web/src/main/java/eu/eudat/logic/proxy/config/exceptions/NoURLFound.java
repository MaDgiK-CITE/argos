package eu.eudat.logic.proxy.config.exceptions;

public class NoURLFound extends Exception {

    private static final long serialVersionUID = -6961447213733280563L;


    public NoURLFound(String message) {
        super(message);
    }


}
