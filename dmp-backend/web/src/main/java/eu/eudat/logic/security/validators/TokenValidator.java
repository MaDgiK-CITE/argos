package eu.eudat.logic.security.validators;

import eu.eudat.exceptions.security.NonValidTokenException;
import eu.eudat.exceptions.security.NullEmailException;
import eu.eudat.models.data.login.LoginInfo;
import eu.eudat.models.data.security.Principal;
import net.shibboleth.utilities.java.support.component.ComponentInitializationException;
import net.shibboleth.utilities.java.support.resolver.ResolverException;

import java.io.IOException;
import java.security.GeneralSecurityException;

public interface TokenValidator {

    Principal validateToken(LoginInfo credentials) throws NonValidTokenException, IOException, GeneralSecurityException, NullEmailException, ResolverException, ComponentInitializationException;

}
