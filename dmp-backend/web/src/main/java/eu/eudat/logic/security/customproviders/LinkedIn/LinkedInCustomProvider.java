package eu.eudat.logic.security.customproviders.LinkedIn;

import eu.eudat.logic.security.validators.linkedin.helpers.LinkedInResponseToken;

public interface LinkedInCustomProvider {

	LinkedInUser getUser(String accessToken);

	LinkedInResponseToken getAccessToken(String code, String redirectUri, String clientId, String clientSecret);
}
