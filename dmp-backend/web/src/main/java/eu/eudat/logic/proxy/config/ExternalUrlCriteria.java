package eu.eudat.logic.proxy.config;

public class ExternalUrlCriteria {
	private String like;
	private String page;
	private String pageSize;
	private String funderId;
	private String path;
	private String host;

	public String getLike() {
		return like;
	}
	public void setLike(String like) {
		this.like = like;
	}

	public String getPage() {
		return page;
	}
	public void setPage(String page) {
		this.page = page;
	}

	public String getPageSize() {
		return pageSize;
	}
	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
	}

	public String getFunderId() {
		return funderId;
	}
	public void setFunderId(String funderId) {
		this.funderId = funderId;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public ExternalUrlCriteria(String like) {
		this.like = like;
	}

	public ExternalUrlCriteria() {
	}

	@Override
	public String toString() {
		return "{" +
				"like='" + like + '\'' +
				", page='" + page + '\'' +
				", pageSize='" + pageSize + '\'' +
				", funderId='" + funderId + '\'' +
				", path='" + path + '\'' +
				", host='" + host + '\'' +
				'}';
	}
}
