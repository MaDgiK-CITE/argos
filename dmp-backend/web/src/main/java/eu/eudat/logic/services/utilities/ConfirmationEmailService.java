package eu.eudat.logic.services.utilities;

import eu.eudat.data.dao.entities.EmailConfirmationDao;
import eu.eudat.data.entities.EmailConfirmation;
import eu.eudat.models.data.security.Principal;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

public interface ConfirmationEmailService {
	public void createConfirmationEmail(EmailConfirmationDao loginConfirmationEmailDao, MailService mailService, String email, UUID userId);
	
	public void createMergeConfirmationEmail(EmailConfirmationDao loginConfirmationEmailDao, MailService mailService, String email, UUID userId, Principal principal, Integer provider);

	public void createUnlinkConfirmationEmail(EmailConfirmationDao loginConfirmationEmailDao, MailService mailService, String email, UUID userId, Principal principal, Integer provider);

	public CompletableFuture sentConfirmationEmail(EmailConfirmation confirmationEmail, MailService mailService);

	public CompletableFuture sentMergeConfirmationEmail(EmailConfirmation confirmationEmail, MailService mailService, String userName);

	public CompletableFuture sentUnlinkConfirmationEmail(EmailConfirmation confirmationEmail, MailService mailService);
}