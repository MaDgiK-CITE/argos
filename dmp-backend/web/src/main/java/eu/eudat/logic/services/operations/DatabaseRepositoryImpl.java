package eu.eudat.logic.services.operations;

import eu.eudat.data.dao.entities.*;
import eu.eudat.data.dao.entities.security.CredentialDao;
import eu.eudat.data.dao.entities.security.UserTokenDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;


@Service("databaseRepository")
public class DatabaseRepositoryImpl implements DatabaseRepository {

    private DataRepositoryDao dataRepositoryDao;
    private DatasetDao datasetDao;
    private DatasetProfileDao datasetProfileDao;
    private DMPDao dmpDao;
    private DmpDatasetProfileDao dmpDatasetProfileDao;
    private OrganisationDao organisationDao;
    private GrantDao GrantDao;
    private RegistryDao registryDao;
    private ResearcherDao researcherDao;
    private ServiceDao serviceDao;
    private UserInfoDao userInfoDao;
    private InvitationDao invitationDao;
    private CredentialDao credentialDao;
    private UserTokenDao userTokenDao;
    private ExternalDatasetDao externalDatasetDao;
    private UserRoleDao userRoleDao;
    private UserDatasetProfileDao userDatasetProfileDao;
    private UserDmpDao userDmpDao;
    private ContentDao contentDao;
    private DMPProfileDao dmpProfileDao;
    private DatasetExternalDatasetDao datasetExternalDatasetDao;
    private DatasetServiceDao datasetServiceDao;
    private EmailConfirmationDao loginConfirmationEmailDao;
    private ProjectDao projectDao;
    private FunderDao funderDao;
    private LockDao lockDao;
    private NotificationDao notificationDao;
    private FileUploadDao fileUploadDao;
    private EntityDoiDao entityDoiDao;
    private DescriptionTemplateTypeDao descriptionTemplateTypeDao;

    private EntityManager entityManager;

    @Autowired
    private void setDataRepositoryDao(DataRepositoryDao dataRepositoryDao) {
        this.dataRepositoryDao = dataRepositoryDao;
    }

    @Autowired
    private void setDatasetDao(DatasetDao datasetDao) {
        this.datasetDao = datasetDao;
    }

    @Autowired
    private void setDatasetProfileDao(DatasetProfileDao datasetProfileDao) {
        this.datasetProfileDao = datasetProfileDao;
    }

    @Autowired
    private void setDmpDao(DMPDao dmpDao) {
        this.dmpDao = dmpDao;
    }

    @Autowired
    private void setDmpDatasetProfileDao(DmpDatasetProfileDao dmpDatasetProfileDao) {
        this.dmpDatasetProfileDao = dmpDatasetProfileDao;
    }

    @Autowired
    private void setOrganisationDao(OrganisationDao organisationDao) {
        this.organisationDao = organisationDao;
    }

    @Autowired
    private void setGrantDao(GrantDao GrantDao) {
        this.GrantDao = GrantDao;
    }

    @Autowired
    private void setRegistryDao(RegistryDao registryDao) {
        this.registryDao = registryDao;
    }

    @Autowired
    private void setResearcherDao(ResearcherDao researcherDao) {
        this.researcherDao = researcherDao;
    }

    @Autowired
    public void setServiceDao(ServiceDao serviceDao) {
        this.serviceDao = serviceDao;
    }

    @Autowired
    private void setUserInfoDao(UserInfoDao userInfoDao) {
        this.userInfoDao = userInfoDao;
    }

    @Override
    public DataRepositoryDao getDataRepositoryDao() {
        return dataRepositoryDao;
    }

    @Override
    public DatasetDao getDatasetDao() {
        return datasetDao;
    }

    @Override
    public DatasetProfileDao getDatasetProfileDao() {
        return datasetProfileDao;
    }

    @Override
    public DMPDao getDmpDao() {
        return dmpDao;
    }

    @Override
    public DmpDatasetProfileDao getDmpDatasetProfileDao() {
        return dmpDatasetProfileDao;
    }

    @Override
    public OrganisationDao getOrganisationDao() {
        return organisationDao;
    }

    @Override
    public GrantDao getGrantDao() {
        return GrantDao;
    }

    @Override
    public RegistryDao getRegistryDao() {
        return registryDao;
    }

    @Override
    public ResearcherDao getResearcherDao() {
        return researcherDao;
    }

    @Override
    public ServiceDao getServiceDao() {
        return serviceDao;
    }

    @Override
    public UserInfoDao getUserInfoDao() {
        return userInfoDao;
    }

    @Override
    public InvitationDao getInvitationDao() {
        return invitationDao;
    }

    @Autowired
    public void setInvitationDao(InvitationDao invitationDao) {
        this.invitationDao = invitationDao;
    }

    @Override
    public CredentialDao getCredentialDao() {
        return credentialDao;
    }

    @Autowired
    public void setCredentialDao(CredentialDao credentialDao) {
        this.credentialDao = credentialDao;
    }

    @Override
    public UserTokenDao getUserTokenDao() {
        return userTokenDao;
    }

    @Autowired
    public void setUserTokenDao(UserTokenDao userTokenDao) {
        this.userTokenDao = userTokenDao;
    }

    @Override
    public ExternalDatasetDao getExternalDatasetDao() {
        return externalDatasetDao;
    }

    @Autowired
    public void setExternalDatasetDao(ExternalDatasetDao externalDatasetDao) {
        this.externalDatasetDao = externalDatasetDao;
    }

    @Override
    public UserRoleDao getUserRoleDao() {
        return userRoleDao;
    }

    @Autowired
    public void setUserRoleDao(UserRoleDao userRoleDao) {
        this.userRoleDao = userRoleDao;
    }

    @Autowired
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public UserDmpDao getUserDmpDao() {
        return userDmpDao;
    }

    @Autowired
    public void setUserDmpDao(UserDmpDao userDmpDao) {
        this.userDmpDao = userDmpDao;
    }

    @Override
    public ContentDao getContentDao() {
        return this.contentDao;
    }

    @Autowired
    public void setContentDao(ContentDao contentDao) {
        this.contentDao = contentDao;
    }

    @Override
    public DMPProfileDao getDmpProfileDao() {
        return dmpProfileDao;
    }

    @Autowired
    public void setDmpProfileDao(DMPProfileDao dmpProfileDao) {
        this.dmpProfileDao = dmpProfileDao;
    }

    @Override
    public DatasetExternalDatasetDao getDatasetExternalDatasetDao() {
        return datasetExternalDatasetDao;
    }

    @Autowired
    public void setDatasetExternalDatasetDao(DatasetExternalDatasetDao datasetExternalDatasetDao) {
        this.datasetExternalDatasetDao = datasetExternalDatasetDao;
    }

    @Override
    public DatasetServiceDao getDatasetServiceDao() {
        return datasetServiceDao;
    }

    @Autowired
    public void setDatasetServiceDao(DatasetServiceDao datasetServiceDao) {
        this.datasetServiceDao = datasetServiceDao;
    }

    @Override
    public EmailConfirmationDao getLoginConfirmationEmailDao() {
        return loginConfirmationEmailDao;
    }

    @Autowired
    public void setLoginConfirmationEmailDao(EmailConfirmationDao loginConfirmationEmailDao) {
        this.loginConfirmationEmailDao = loginConfirmationEmailDao;
    }

    @Override
    public ProjectDao getProjectDao() {
        return projectDao;
    }

    @Autowired
    public void setProjectDao(ProjectDao projectDao) {
        this.projectDao = projectDao;
    }

    @Override
    public FunderDao getFunderDao() {
        return funderDao;
    }

    @Autowired
    public void setFunderDao(FunderDao funderDao) {
        this.funderDao = funderDao;
    }

    @Autowired
    public void setLockDao(LockDao lockDao) {
        this.lockDao = lockDao;
    }

    @Override
    public LockDao getLockDao() {
        return lockDao;
    }

    @Override
    public NotificationDao getNotificationDao() {
        return notificationDao;
    }

    @Autowired
    public void setNotificationDao(NotificationDao notificationDao) {
        this.notificationDao = notificationDao;
    }

    @Override
    public UserDatasetProfileDao getUserDatasetProfileDao() {
        return userDatasetProfileDao;
    }

    @Autowired
    public void setUserDatasetProfileDao(UserDatasetProfileDao userDatasetProfileDao) {
        this.userDatasetProfileDao = userDatasetProfileDao;
    }

    @Override
    public FileUploadDao getFileUploadDao() {
        return fileUploadDao;
    }

    @Autowired
    public void setFileUploadDao(FileUploadDao fileUploadDao) {
        this.fileUploadDao = fileUploadDao;
    }

    @Override
    public EntityDoiDao getEntityDoiDao() {
        return entityDoiDao;
    }

    @Autowired
    public void setEntityDoiDao(EntityDoiDao entityDoiDao) {
        this.entityDoiDao = entityDoiDao;
    }

    @Override
    public DescriptionTemplateTypeDao getDescriptionTemplateTypeDao() {
        return descriptionTemplateTypeDao;
    }

    @Autowired
    public void setDescriptionTemplateTypeDao(DescriptionTemplateTypeDao descriptionTemplateTypeDao) {
        this.descriptionTemplateTypeDao = descriptionTemplateTypeDao;
    }

    public <T> void detachEntity(T entity) {
        this.entityManager.detach(entity);
    }
}
