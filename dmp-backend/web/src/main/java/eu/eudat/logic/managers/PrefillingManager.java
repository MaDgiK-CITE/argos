package eu.eudat.logic.managers;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.eudat.data.entities.DescriptionTemplate;
import eu.eudat.logic.mapper.prefilling.PrefillingMapper;
import eu.eudat.logic.proxy.config.ExternalUrlCriteria;
import eu.eudat.logic.proxy.config.configloaders.ConfigLoader;
import eu.eudat.logic.proxy.config.entities.PrefillingConfig;
import eu.eudat.logic.proxy.config.entities.PrefillingGet;
import eu.eudat.logic.services.ApiContext;
import eu.eudat.models.data.datasetwizard.DatasetWizardModel;
import eu.eudat.models.data.prefilling.Prefilling;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class PrefillingManager {

    private final ApiContext apiContext;
    private final ConfigLoader configLoader;
    private final ObjectMapper objectMapper;
    private final DatasetManager datasetManager;
    private final LicenseManager licenseManager;
    private final PrefillingMapper prefillingMapper;

    @Autowired
    public PrefillingManager(ApiContext apiContext, ConfigLoader configLoader, DatasetManager datasetManager, LicenseManager licenseManager, PrefillingMapper prefillingMapper) {
        this.apiContext = apiContext;
        this.configLoader = configLoader;
        this.prefillingMapper = prefillingMapper;
        this.objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        this.datasetManager = datasetManager;
        this.licenseManager = licenseManager;
    }

    public List<Prefilling> getPrefillings(String like) {
        ExternalUrlCriteria externalUrlCriteria = new ExternalUrlCriteria();
        externalUrlCriteria.setLike(like);
        List<Prefilling> prefillings = new ArrayList<>();
        List<Map<String, String>> map;
        Map<String, PrefillingConfig> prefillingConfigs = configLoader.getExternalUrls().getPrefillings();
        for (PrefillingConfig prefillingConfig: prefillingConfigs.values()) {
            map = apiContext.getOperationsContext().getRemoteFetcher().getExternalGeneric(externalUrlCriteria, prefillingConfig.getPrefillingSearch());
            prefillings.addAll(map.stream().map(submap -> objectMapper.convertValue(submap, Prefilling.class)).collect(Collectors.toList()));
            if (prefillingConfig.getPrefillingSearch().getUrlConfig().isDataInListing()) {
                List<Map<String, Object>> mapData = apiContext.getOperationsContext().getRemoteFetcher().getExternalGenericWithData(externalUrlCriteria, prefillingConfig.getPrefillingSearch());
                for (int i = 0; i < mapData.size(); i++) {
                    prefillings.get(i).setData(mapData.get(i));
                }
                prefillings = prefillings.stream().filter(prefilling -> prefilling.getData() != null).collect(Collectors.toList());
            }
        }
        return prefillings;
    }

    public DatasetWizardModel getPrefilledDataset(String prefillId, String configId, UUID profileId) throws Exception {
        PrefillingConfig prefillingConfig = configLoader.getExternalUrls().getPrefillings().get(configId);
        PrefillingGet prefillingGet = prefillingConfig.getPrefillingGet();
        Map<String, Object> prefillingEntity = getSingle(prefillingGet.getUrl(), prefillId);
        DescriptionTemplate descriptionTemplate = apiContext.getOperationsContext().getDatabaseRepository().getDatasetProfileDao().find(profileId);
        return prefillingMapper.mapPrefilledEntityToDatasetWizard(prefillingEntity, prefillingGet, prefillingConfig.getType(), descriptionTemplate, datasetManager, licenseManager);
    }

    public DatasetWizardModel getPrefilledDatasetUsingData(Map<String, Object> data, String configId, UUID profileId) throws Exception {
        PrefillingConfig prefillingConfig = configLoader.getExternalUrls().getPrefillings().get(configId);
        PrefillingGet prefillingGet = prefillingConfig.getPrefillingGet();
        DescriptionTemplate descriptionTemplate = apiContext.getOperationsContext().getDatabaseRepository().getDatasetProfileDao().find(profileId);
        return prefillingMapper.mapPrefilledEntityToDatasetWizard(data, prefillingGet, prefillingConfig.getType(), descriptionTemplate, datasetManager, licenseManager);
    }

    private Map<String, Object> getSingle(String url, String id) {
        RestTemplate restTemplate = new RestTemplate();
        String parsedUrl = url.replace("{id}", id);
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity("", headers);

        return restTemplate.exchange(parsedUrl, HttpMethod.GET, entity, LinkedHashMap.class).getBody();
    }
}
