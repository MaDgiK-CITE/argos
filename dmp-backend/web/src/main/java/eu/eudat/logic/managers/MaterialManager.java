package eu.eudat.logic.managers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class MaterialManager {

    @Autowired
    public MaterialManager(){}

    public ResponseEntity<byte[]> getResponseEntity(String lang, Stream<Path> paths) throws IOException {
        List<String> result = paths.filter(Files::isRegularFile)
                .map(Path::toString).collect(Collectors.toList());

        String fileName = result.stream().filter(about -> about.contains("_" + lang)).findFirst().orElse(null);
        if (fileName == null) {
            fileName = result.stream().filter(about -> about.contains("_en")).findFirst().get();
        }
        InputStream is = new FileInputStream(fileName);

        Path path = Paths.get(fileName);

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentLength(is.available());
        responseHeaders.setContentType(MediaType.TEXT_HTML);
        responseHeaders.set("Content-Disposition", "attachment;filename=" + path.getFileName().toString());
        responseHeaders.set("Access-Control-Expose-Headers", "Content-Disposition");
        responseHeaders.get("Access-Control-Expose-Headers").add("Content-Type");

        byte[] content = new byte[is.available()];
        is.read(content);
        is.close();

        return new ResponseEntity<>(content, responseHeaders, HttpStatus.OK);
    }
}
