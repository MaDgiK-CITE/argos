package eu.eudat.logic.utilities.documents.types;

/**
 * Created by ikalyvas on 2/26/2018.
 */
public enum ParagraphStyle {
    TEXT(0), HEADER1(1), HEADER2(2), HEADER3(3), HEADER4(4), TITLE(5), FOOTER(6), COMMENT(7), HEADER5(8), HEADER6(9), HTML(10), IMAGE(11);

    private Integer value;

    private ParagraphStyle(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }

    public static ParagraphStyle fromInteger(Integer value) {
        switch (value) {
            case 0:
                return TEXT;
            case 1:
                return HEADER1;
            case 2:
                return HEADER2;
            case 3:
                return HEADER3;
            case 4:
                return HEADER4;
            case 5:
                return TITLE;
            case 6:
                return FOOTER;
            case 7:
                return COMMENT;
            case 8:
                return HEADER5;
            case 9:
                return HEADER6;
            case 10:
                return HTML;
            case 11:
                return IMAGE;
            default:
                throw new RuntimeException("Unsupported ParagraphStyle Code");
        }
    }
}
