package eu.eudat.logic.managers;

import eu.eudat.data.dao.criteria.DatasetProfileCriteria;
import eu.eudat.data.entities.DescriptionTemplate;
import eu.eudat.data.entities.UserDatasetProfile;
import eu.eudat.data.entities.UserInfo;
import eu.eudat.data.query.items.item.datasetprofile.DatasetProfileAutocompleteRequest;
import eu.eudat.data.query.items.table.datasetprofile.DatasetProfileTableRequestItem;
import eu.eudat.exceptions.datasetprofile.DatasetProfileNewVersionException;
import eu.eudat.logic.builders.model.models.DataTableDataBuilder;
import eu.eudat.logic.proxy.config.*;
import eu.eudat.logic.proxy.config.configloaders.ConfigLoader;
import eu.eudat.logic.proxy.config.entities.GeneralUrls;
import eu.eudat.logic.proxy.fetching.RemoteFetcher;
import eu.eudat.logic.services.ApiContext;
import eu.eudat.logic.services.operations.DatabaseRepository;
import eu.eudat.logic.utilities.builders.XmlBuilder;
import eu.eudat.logic.utilities.documents.helpers.FileEnvelope;
import eu.eudat.logic.utilities.documents.xml.datasetProfileXml.ExportXmlBuilderDatasetProfile;
import eu.eudat.logic.utilities.documents.xml.datasetProfileXml.ImportXmlBuilderDatasetProfile;
import eu.eudat.models.data.admin.composite.DatasetProfile;
import eu.eudat.models.data.components.commons.datafield.AutoCompleteData;
import eu.eudat.models.data.datasetprofile.DatasetProfileAutocompleteItem;
import eu.eudat.models.data.datasetprofile.DatasetProfileListingModel;
import eu.eudat.models.data.datasetprofile.DatasetProfileWithPrefillingPropertyModel;
import eu.eudat.models.data.entities.xmlmodels.datasetprofiledefinition.Field;
import eu.eudat.models.data.externaldataset.ExternalAutocompleteFieldModel;
import eu.eudat.models.data.helpers.common.DataTableData;
import eu.eudat.models.data.listingmodels.UserInfoListingModel;
import eu.eudat.models.data.mail.SimpleMail;
import eu.eudat.models.data.security.Principal;
import eu.eudat.queryable.QueryableList;
import eu.eudat.types.Authorities;
import eu.eudat.types.MetricNames;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.activation.MimetypesFileTypeMap;
import javax.transaction.Transactional;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.*;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.Collectors;


@Component
public class DatasetProfileManager {
    private static final Logger logger = LoggerFactory.getLogger(DatasetProfileManager.class);
    private static final List<String> cache = new ArrayList<>();

    private final ApiContext apiContext;
    private final DatabaseRepository databaseRepository;
    private final Environment environment;
    private final ConfigLoader configLoader;
    private final MetricsManager metricsManager;
    private final RemoteFetcher remoteFetcher;
    @Autowired
    public DatasetProfileManager(ApiContext apiContext, Environment environment, ConfigLoader configLoader, MetricsManager metricsManager, RemoteFetcher remoteFetcher) {
        this.apiContext = apiContext;
        this.databaseRepository = apiContext.getOperationsContext().getDatabaseRepository();
        this.environment = environment;
        this.configLoader = configLoader;
        this.metricsManager = metricsManager;
        this.remoteFetcher = remoteFetcher;
    }

    @Transactional
    public eu.eudat.models.data.admin.composite.DatasetProfile getDatasetProfile(String id) {
        DescriptionTemplate profile = apiContext.getOperationsContext().getDatabaseRepository().getDatasetProfileDao().find(UUID.fromString(id));
        eu.eudat.models.data.admin.composite.DatasetProfile datasetprofile = AdminManager.generateDatasetProfileModel(profile);
        datasetprofile.setLabel(profile.getLabel());
        datasetprofile.setStatus(profile.getStatus());
        datasetprofile.setDescription(profile.getDescription());
        datasetprofile.setType(profile.getType().getName());
        datasetprofile.setLanguage(profile.getLanguage());
        datasetprofile.setUsers(new ArrayList<>());
        retrieveUsers(profile, datasetprofile);
        return datasetprofile;
    }

    public List<DatasetProfileAutocompleteItem> getWithCriteria(DatasetProfileAutocompleteRequest datasetProfileAutocompleteRequest) throws IllegalAccessException, InstantiationException {
        QueryableList<DescriptionTemplate> items = databaseRepository.getDatasetProfileDao().getWithCriteria(datasetProfileAutocompleteRequest.getCriteria());
        QueryableList<DescriptionTemplate> pagedItems = datasetProfileAutocompleteRequest.applyPaging(items);
        List<DatasetProfileAutocompleteItem> datasetProfiles = pagedItems.select(item -> new DatasetProfileAutocompleteItem().fromDataModel(item));
        return datasetProfiles;
    }

    public DescriptionTemplate clone(String id) {
        DescriptionTemplate profile = apiContext.getOperationsContext().getDatabaseRepository().getDatasetProfileDao().find(UUID.fromString(id));
        apiContext.getOperationsContext().getDatabaseRepository().detachEntity(profile);
        profile.setId(null);
        return profile;
    }

    public DataTableData<DatasetProfileListingModel> getPaged(DatasetProfileTableRequestItem datasetProfileTableRequestItem, Principal principal) throws Exception {
        QueryableList<DescriptionTemplate> items = apiContext.getOperationsContext().getDatabaseRepository().getDatasetProfileDao().getWithCriteria(datasetProfileTableRequestItem.getCriteria());
        QueryableList<DescriptionTemplate> authItems = null;
        if (principal.getAuthz().contains(Authorities.ADMIN)) {
            authItems = items;
        } else if (principal.getAuthz().contains(Authorities.DATASET_PROFILE_MANAGER)) {
            List<Integer> roles = Arrays.asList(0, 1);
            authItems = apiContext.getOperationsContext().getDatabaseRepository().getDatasetProfileDao().getAuthenticated(items, principal.getId(), roles);
        }
        QueryableList<DescriptionTemplate> pagedItems = PaginationManager.applyPaging(authItems, datasetProfileTableRequestItem);
        List<DatasetProfileListingModel> datasetProfiles = pagedItems.select(item -> new DatasetProfileListingModel().fromDataModel(item));
        return apiContext.getOperationsContext().getBuilderFactory().getBuilder(DataTableDataBuilder.class).data(datasetProfiles).totalCount(items.count()).build();
    }

    public List<DatasetProfileListingModel> getAll(DatasetProfileTableRequestItem tableRequestItem) throws IllegalAccessException, InstantiationException {
        QueryableList<DescriptionTemplate> items = databaseRepository.getDatasetProfileDao().getWithCriteria(tableRequestItem.getCriteria());
        List<DatasetProfileListingModel> datasetProfiles = items.select(item -> new DatasetProfileListingModel().fromDataModel(item));

        return datasetProfiles;
    }

    public List<DatasetProfileWithPrefillingPropertyModel> getAllWithPrefilling(DatasetProfileTableRequestItem tableRequestItem) {
        List<DatasetProfileWithPrefillingPropertyModel> datasetProfiles = new ArrayList<>();
        if (!tableRequestItem.getCriteria().getIds().isEmpty()) {
            tableRequestItem.getCriteria().getIds().forEach(id -> {
                DatasetProfile datasetProfile = this.getDatasetProfile(id.toString());
                DatasetProfileWithPrefillingPropertyModel profileModel = new DatasetProfileWithPrefillingPropertyModel();
                profileModel.setId(id);
                profileModel.setLabel(datasetProfile.getLabel());
                profileModel.setEnablePrefilling(datasetProfile.isEnablePrefilling());
                datasetProfiles.add(profileModel);
            });
        }
        return datasetProfiles;
    }

    public eu.eudat.models.data.entities.xmlmodels.datasetprofiledefinition.Field queryForField(String xml, String fieldId) throws XPathExpressionException {
        eu.eudat.models.data.entities.xmlmodels.datasetprofiledefinition.Field field = new Field();
        Document document = XmlBuilder.fromXml(xml);
        XPathFactory xpathFactory = XPathFactory.newInstance();
        XPath xpath = xpathFactory.newXPath();
        XPathExpression expr =
                xpath.compile("//field[@id='" + fieldId + "']");
        Element name = (Element) expr.evaluate(document, XPathConstants.NODE);
        field.fromXml(name);
        return field;
    }

    public List<ExternalAutocompleteFieldModel> getAutocomplete(AutoCompleteData data, String like) {
        /*List<ExternalAutocompleteFieldModel> result = new LinkedList<>();
        SimpleClientHttpRequestFactory simpleFactory = new SimpleClientHttpRequestFactory();

        RestTemplate restTemplate = new RestTemplate(simpleFactory);
        HttpHeaders headers = new HttpHeaders();
        DocumentContext jsonContext = null;
        HttpEntity<String> entity;
        ResponseEntity response;
        List<Map<String, String>> jsonItems;
        int i = 0;
        for (AutoCompleteData.AutoCompleteSingleData singleData: data.getAutoCompleteSingleDataList()) {
            switch (AutoCompleteData.AutocompleteType.fromValue(singleData.getAutocompleteType())) {
                case UNCACHED:

                    String url = singleData.getUrl();
                    String mediaType = "";
                    if (url.contains("openaire") || url.contains("zenodo")) {
                    	mediaType = "application/json; charset=utf-8";
                    	if (url.contains("zenodo")) {
                    		url = url.replace("?", "/?");
                    	}

                        url = url.replace("{like}", like.equals("") ? "*" : like);
                        url = url.replace("%20", " ");
                        url = url.replace("%22", "\"");
                        url = url.replace("&amp;", "&");
                    } else {
                        mediaType = "application/vnd.api+json; charset=utf-8";
                        url += "?search=" + like;
                    }

                    if (!url.contains("zenodo")) {
                    	headers.setAccept(Collections.singletonList(MediaType.valueOf(mediaType)));
                    }
                    headers.setContentType(MediaType.APPLICATION_JSON);
                    entity = new HttpEntity<>("parameters", headers);


                    response = restTemplate.exchange(url, HttpMethod.GET, entity, Object.class);
                    jsonContext = JsonPath.parse(response.getBody());
                    jsonItems = jsonContext.read(singleData.getOptionsRoot() + "['" + singleData.getAutoCompleteOptions().getLabel() + "','" + singleData.getAutoCompleteOptions().getValue() + "','" + singleData.getAutoCompleteOptions().getSource() + "','" + "uri" + "']");
                    jsonItems.forEach(item -> result.add(new ExternalAutocompleteFieldModel(parseItem(item.get(singleData.getAutoCompleteOptions().getValue())), parseItem(item.get(singleData.getAutoCompleteOptions().getLabel())), item.get(singleData.getAutoCompleteOptions().getSource())  != null ? parseItem(item.get(singleData.getAutoCompleteOptions().getSource())) : singleData.getAutoCompleteOptions().getSource(), parseItem(item.get("uri")))));
                    break;
                case CACHED:
                    headers.setAccept(Collections.singletonList(MediaType.valueOf("text/plain; charset=utf-8")));
                    headers.setContentType(MediaType.TEXT_PLAIN);
                    entity = new HttpEntity<>("parameters", headers);

                    if (cache.size() <= i) {
                        response = restTemplate.exchange(singleData.getUrl(), HttpMethod.GET, entity, String.class);
                        cache.add((String) response.getBody());
                    }
                    jsonContext = JsonPath.parse(cache.get(i));
                    jsonItems = jsonContext.read(singleData.getOptionsRoot() + "['" + singleData.getAutoCompleteOptions().getLabel() + "','" + singleData.getAutoCompleteOptions().getValue() + "','" + singleData.getAutoCompleteOptions().getSource() + "','" + "uri" + "']");
                    jsonItems.stream().filter(item -> item.get(singleData.getAutoCompleteOptions().getLabel()).toLowerCase().contains(like.toLowerCase()))
                             .forEach(item -> result.add(new ExternalAutocompleteFieldModel(item.get(singleData.getAutoCompleteOptions().getValue()), item.get(singleData.getAutoCompleteOptions().getLabel()), item.get(singleData.getAutoCompleteOptions().getSource())  != null ? item.get(singleData.getAutoCompleteOptions().getSource()) : singleData.getAutoCompleteOptions().getSource(), item.get("uri"))));
                    i++;
                    break;
            }

        }

        return result.stream().sorted(Comparator.comparing(ExternalAutocompleteFieldModel::getLabel)).collect(Collectors.toList());
*/
        List<ExternalAutocompleteFieldModel> result = new LinkedList<>();
        ExternalUrlCriteria urlCriteria = new ExternalUrlCriteria();
        GeneralUrls genericUrls = new GeneralUrls();
        int ordinal = 1;
        List<Map<String, String>> rawResults = new ArrayList<>();
        genericUrls.setFetchMode(FetchStrategy.FIRST);
        urlCriteria.setLike(like);
        for (AutoCompleteData.AutoCompleteSingleData singleData : data.getAutoCompleteSingleDataList()) {
            UrlConfiguration urlConfiguration = new UrlConfiguration();
            try {
                URI uri;
                if (singleData.getUrl().contains("?")) {
                    uri = new URI(singleData.getUrl().substring(0, singleData.getUrl().lastIndexOf("?")));
                } else {
                    uri = new URI(singleData.getUrl());
                }
                String source = singleData.getAutoCompleteOptions().getSource();
                source = source != null && !source.isEmpty() ? source : uri.getHost();
                String uriString = singleData.getAutoCompleteOptions().getUri();
                uriString = uriString != null && !uriString.isEmpty() ? uriString : "uri";
                String parsedUrl = singleData.getUrl();
                parsedUrl = parsedUrl.replace("%20", " ");
                parsedUrl = parsedUrl.replace("%22", "\"");
                while (parsedUrl.contains("&amp;")) {
                    parsedUrl = parsedUrl.replace("&amp;", "&");
                }
                urlConfiguration.setUrl(parsedUrl);
                urlConfiguration.setOrdinal(ordinal);
                urlConfiguration.setType("External");
                urlConfiguration.setContentType(MediaType.APPLICATION_JSON_VALUE);
                urlConfiguration.setFirstpage("1");
                urlConfiguration.setRequestType(singleData.getMethod() != null ? singleData.getMethod() : "GET");
                DataUrlConfiguration dataUrlConfiguration = new DataUrlConfiguration();
                dataUrlConfiguration.setPath(singleData.getOptionsRoot());
                DataFieldsUrlConfiguration fieldsUrlConfiguration = new DataFieldsUrlConfiguration();
                fieldsUrlConfiguration.setId(singleData.getAutoCompleteOptions().getValue());
                fieldsUrlConfiguration.setName(singleData.getAutoCompleteOptions().getLabel());
                fieldsUrlConfiguration.setSource(singleData.getAutoCompleteOptions().getSource().isEmpty()? null : singleData.getAutoCompleteOptions().getSource());
                fieldsUrlConfiguration.setUri(uriString);
                dataUrlConfiguration.setFieldsUrlConfiguration(fieldsUrlConfiguration);
                urlConfiguration.setKey(source);
                urlConfiguration.setLabel(source);
                urlConfiguration.setData(dataUrlConfiguration);
                if (singleData.getHasAuth()) {
                    AuthenticationConfiguration authenticationConfiguration = new AuthenticationConfiguration();
                    authenticationConfiguration.setAuthUrl(singleData.getAuth().getUrl());
                    authenticationConfiguration.setAuthMethod(singleData.getAuth().getMethod());
                    authenticationConfiguration.setAuthTokenPath(singleData.getAuth().getPath());
                    authenticationConfiguration.setAuthRequestBody(singleData.getAuth().getBody());
                    authenticationConfiguration.setType(singleData.getAuth().getType());
                    urlConfiguration.setAuth(authenticationConfiguration);
                }
                genericUrls.getUrls().add(urlConfiguration);
                List<Map<String, String>> singleResults =  this.remoteFetcher.getExternalGeneric(urlCriteria, genericUrls);
                if (!singleResults.isEmpty() && !singleResults.get(0).containsKey("source") && !singleData.getAutoCompleteOptions().getSource().isEmpty()) {
                    singleResults.forEach(singleResult -> singleResult.put("source", singleData.getAutoCompleteOptions().getSource()));
                }
                rawResults.addAll(singleResults);
                genericUrls.getUrls().clear();
            } catch (URISyntaxException e) {
                logger.error(e.getMessage(), e);
            }
        }
        rawResults.forEach(item -> result.add(new ExternalAutocompleteFieldModel(parseItem(item.get("pid")), parseItem(item.get("name")), parseItem(item.get("source")), parseItem(item.get("uri")))));
        return result;
    }

    private static String parseItem(Object item) {
        if (item instanceof String) {
            return (String) item;
        }
        if (item instanceof List) {
            List listedItems = (List) item;
            return parseItem(listedItems.get(0));
        }
        if (item instanceof Map) {
            return String.valueOf(((Map)item).get("$"));
        }
        return item != null ? item.toString() : null;
    }

    public ResponseEntity<byte[]> getDocument(eu.eudat.models.data.user.composite.DatasetProfile datasetProfile, String label) throws IllegalAccessException, IOException, InstantiationException {
        FileEnvelope envelope = getXmlDocument(datasetProfile, label);
        InputStream resource = new FileInputStream(envelope.getFile());
        logger.info("Mime Type of " + envelope.getFilename() + " is " +
                new MimetypesFileTypeMap().getContentType(envelope.getFile()));
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentLength(envelope.getFile().length());
        responseHeaders.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        String fileName = envelope.getFilename().replace(" ", "_").replace(",", "_");
        responseHeaders.set("Content-Disposition", "attachment;filename=" + fileName + ".xml");
        responseHeaders.set("Access-Control-Expose-Headers", "Content-Disposition");
        responseHeaders.get("Access-Control-Expose-Headers").add("Content-Type");

        byte[] content = org.apache.poi.util.IOUtils.toByteArray(resource);
        resource.close();
        Files.deleteIfExists(envelope.getFile().toPath());

        return new ResponseEntity<>(content,
                responseHeaders,
                HttpStatus.OK);
    }

    public FileEnvelope getXmlDocument(eu.eudat.models.data.user.composite.DatasetProfile datatasetProfile, String label) throws InstantiationException, IllegalAccessException, IOException {
        ExportXmlBuilderDatasetProfile xmlBuilder = new ExportXmlBuilderDatasetProfile();
        File file = xmlBuilder.build(datatasetProfile, environment);
        FileEnvelope fileEnvelope = new FileEnvelope();
        fileEnvelope.setFile(file);
        fileEnvelope.setFilename(label);
        return fileEnvelope;
    }

    public eu.eudat.logic.utilities.documents.xml.datasetProfileXml.datasetProfileModel.DatasetProfile createDatasetProfileFromXml(MultipartFile multiPartFile) {
        ImportXmlBuilderDatasetProfile xmlBuilder = new ImportXmlBuilderDatasetProfile();
        try {
            File localFile = convert(multiPartFile);
            eu.eudat.logic.utilities.documents.xml.datasetProfileXml.datasetProfileModel.DatasetProfile profile = xmlBuilder.build(localFile);
            Files.deleteIfExists(localFile.toPath());
            metricsManager.increaseValue(MetricNames.DATASET_TEMPLATE, 1, MetricNames.DRAFT);
            return profile;
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
        return null;
    }

    private File convert(MultipartFile file) throws IOException {
        File convFile = new File(this.environment.getProperty("temp.temp") + file.getOriginalFilename());
        convFile.createNewFile();
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(file.getBytes());
        fos.close();
        return convFile;
    }

    public DescriptionTemplate createNewVersionDatasetProfile(String id, eu.eudat.models.data.admin.composite.DatasetProfile profile) throws Exception {
        // Getting the DescriptionTemplate which we will create its new version.
        DescriptionTemplate oldDescriptionTemplate = apiContext.getOperationsContext().getDatabaseRepository().getDatasetProfileDao().find(UUID.fromString(id));

        // Getting the DescriptionTemplate with the latest Version.
        DatasetProfileCriteria criteria = new DatasetProfileCriteria();
        LinkedList<UUID> list = new LinkedList<>();
        list.push(oldDescriptionTemplate.getGroupId());
        criteria.setGroupIds(list);
        criteria.setAllVersions(false);
        QueryableList<DescriptionTemplate> datasetProfileQueryableList = apiContext.getOperationsContext().getDatabaseRepository().getDatasetProfileDao().getWithCriteria(criteria);
        DescriptionTemplate latestVersionDescriptionTemplate = datasetProfileQueryableList.getSingle();

        if (latestVersionDescriptionTemplate.getVersion().equals(oldDescriptionTemplate.getVersion())){
            eu.eudat.models.data.admin.composite.DatasetProfile sortedProfile = profile.toShort();
            DescriptionTemplate modelDefinition = AdminManager.generateViewStyleDefinition(sortedProfile, apiContext);
//            modelDefinition.setLabel(oldDescriptionTemplate.getLabel());
            modelDefinition.setVersion((short) (oldDescriptionTemplate.getVersion() + 1));
            modelDefinition.setGroupId(oldDescriptionTemplate.getGroupId());
//            modelDefinition.setLanguage(oldDescriptionTemplate.getLanguage());
            apiContext.getOperationsContext().getDatabaseRepository().getDatasetProfileDao().createOrUpdate(modelDefinition);
            DescriptionTemplate descriptionTemplate =  apiContext.getOperationsContext().getDatabaseRepository().getDatasetProfileDao().createOrUpdate(modelDefinition);
            this.storeDatasetProfileUsers(descriptionTemplate, profile);
            return modelDefinition;
        } else {
            throw new DatasetProfileNewVersionException("Version to update not the latest.");
        }
    }

    public void storeDatasetProfileUsers(DescriptionTemplate entity, eu.eudat.models.data.admin.composite.DatasetProfile model) {
        if (model.getUsers() != null && !model.getUsers().isEmpty()) {
            if (entity.getUsers() == null) {
                entity.setUsers(new HashSet<>());
            }
            model.getUsers().stream().filter(userInfoListingModel -> entity.getUsers().stream()
                    .filter(userDatasetProfile -> userDatasetProfile.getUser().getId().equals(userInfoListingModel.getId())).count() == 0)
                    .forEach(userInfoListingModel -> {
                UserDatasetProfile userDatasetProfile1 = new UserDatasetProfile();
                userDatasetProfile1.setDatasetProfile(entity);
                UserInfo userInfo1 = apiContext.getOperationsContext().getDatabaseRepository().getUserInfoDao().find(userInfoListingModel.getId());
                userDatasetProfile1.setUser(userInfo1);
                userDatasetProfile1.setRole(1);
                apiContext.getOperationsContext().getDatabaseRepository().getUserDatasetProfileDao().createOrUpdate(userDatasetProfile1);
                sendJoinMail(userDatasetProfile1);
            });
            entity.getUsers().stream().filter(userDatasetProfile -> model.getUsers().stream()
                    .filter(userInfoListingModel -> userDatasetProfile.getUser().getId().equals(userInfoListingModel.getId())).count() > 0
                    && userDatasetProfile.getRole() == 2).forEach(userDatasetProfile -> {
                userDatasetProfile.setRole(1);
                apiContext.getOperationsContext().getDatabaseRepository().getUserDatasetProfileDao().createOrUpdate(userDatasetProfile);
                sendJoinMail(userDatasetProfile);
            });
        }
        if (entity.getUsers() != null && !entity.getUsers().isEmpty()) {
            entity.getUsers().stream().filter(userDatasetProfile -> model.getUsers().stream()
                    .filter(userInfoListingModel -> userDatasetProfile.getUser().getId().equals(userInfoListingModel.getId())).count() == 0)
                    .forEach(userDatasetProfile -> {
                userDatasetProfile.setRole(2);
                apiContext.getOperationsContext().getDatabaseRepository().getUserDatasetProfileDao().createOrUpdate(userDatasetProfile);
            });
        }
    }

    @Transactional
    public void retrieveUsers(DescriptionTemplate entity, eu.eudat.models.data.admin.composite.DatasetProfile model) {
        if (entity.getUsers() != null && !entity.getUsers().isEmpty()) {
            model.setUsers(entity.getUsers().stream().filter(userDatasetProfile -> userDatasetProfile.getRole() < 2).map(userDatasetProfile -> {
                UserInfoListingModel userInfoListingModel = new UserInfoListingModel();
                userInfoListingModel.setId(userDatasetProfile.getUser().getId());
                userInfoListingModel.setName(userDatasetProfile.getUser().getName());
                userInfoListingModel.setEmail(userDatasetProfile.getUser().getEmail());
                userInfoListingModel.setRole(userDatasetProfile.getRole());
                return userInfoListingModel;
            }).collect(Collectors.toList()));
        }
    }

    private void sendJoinMail(UserDatasetProfile userDatasetProfile) {
            SimpleMail mail = new SimpleMail();
            mail.setSubject(environment.getProperty("admin.mail.subject").replace( "{templateName}", userDatasetProfile.getDatasetProfile().getLabel()));
            String content = apiContext.getUtilitiesService().getMailService().getMailTemplateContent(environment.getProperty("email.dataset.template"));
            content = content.replace("{recipient}", userDatasetProfile.getUser().getName());
            content = content.replace("{templateName}", userDatasetProfile.getDatasetProfile().getLabel());
            content = content.replace("{host}", this.environment.getProperty("dmp.domain"));
            content = content.replace("{templateID}", userDatasetProfile.getDatasetProfile().getId().toString());
            mail.setContent(content);
            mail.setTo(userDatasetProfile.getUser().getEmail());
            try {
                apiContext.getUtilitiesService().getMailService().sendSimpleMail(mail);
            } catch (Exception ex) {
                logger.error(ex.getMessage(), ex);
            }

    }

    public List<String> getSemantics(String query) {
        List<Semantic> semantics = configLoader.getSemantics();
        List<String> filteredSemantics = semantics.stream().map(Semantic::getName).collect(Collectors.toList());
        if(query != null && !query.isEmpty()){
            filteredSemantics = semantics.stream().filter(x -> x.getCategory().contains(query) || x.getName().contains(query)).map(Semantic::getName).collect(Collectors.toList());
        }
        return filteredSemantics;
    }

    public void addSemanticsInDatasetProfiles() throws XPathExpressionException {
        List<DescriptionTemplate> ids = this.databaseRepository.getDatasetProfileDao().getAllIds();
        for(DescriptionTemplate dp: ids){
            DescriptionTemplate descriptionTemplate = this.databaseRepository.getDatasetProfileDao().find(dp.getId());
            Document document = XmlBuilder.fromXml(descriptionTemplate.getDefinition());
            XPathFactory xpathFactory = XPathFactory.newInstance();
            XPath xpath = xpathFactory.newXPath();
            XPathExpression expr = xpath.compile("//rdaCommonStandard");
            NodeList rdaProperties = (NodeList) expr.evaluate(document, XPathConstants.NODESET);
            for(int i = 0; i < rdaProperties.getLength(); i++){
                Node rdaPropertyNode = rdaProperties.item(i);
                String rdaProperty = rdaPropertyNode.getTextContent();
                Element schematics = document.createElement("schematics");
                Node fieldParent = rdaPropertyNode.getParentNode();
                if(rdaProperty != null && !rdaProperty.isEmpty()){
                    Element schematic = document.createElement("schematic");
                    schematic.setTextContent("rda." + rdaProperty);
                    schematics.appendChild(schematic);
                }
                fieldParent.insertBefore(schematics, rdaPropertyNode);
                fieldParent.removeChild(rdaPropertyNode);
            }
            this.updateDatasetProfileXml(document, descriptionTemplate);
        }
    }

    public void addRdaInSemanticsInDatasetProfiles() throws XPathExpressionException {
        List<DescriptionTemplate> ids = this.databaseRepository.getDatasetProfileDao().getAllIds();
        for(DescriptionTemplate dp: ids){
            DescriptionTemplate descriptionTemplate = this.databaseRepository.getDatasetProfileDao().find(dp.getId());
            Document document = XmlBuilder.fromXml(descriptionTemplate.getDefinition());
            XPathFactory xpathFactory = XPathFactory.newInstance();
            XPath xpath = xpathFactory.newXPath();
            XPathExpression expr = xpath.compile("//schematic");
            NodeList schematics = (NodeList) expr.evaluate(document, XPathConstants.NODESET);
            for (int i = 0; i < schematics.getLength(); i++) {
                Node schematicNode = schematics.item(i);
                String schematicRda = schematicNode.getTextContent();
                if (schematicRda != null && !schematicRda.isEmpty() && !schematicRda.startsWith("rda.")) {
                    schematicNode.setTextContent("rda." + schematicRda);
                }
            }
            this.updateDatasetProfileXml(document, descriptionTemplate);
        }
    }

    private void updateDatasetProfileXml(Document document, DescriptionTemplate descriptionTemplate) {
        try {
            DOMSource domSource = new DOMSource(document);
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.transform(domSource, result);
            String newDefinition = writer.toString();
            if(newDefinition != null){
                descriptionTemplate.setDefinition(newDefinition);
                this.databaseRepository.getDatasetProfileDao().createOrUpdate(descriptionTemplate);
            }
        }
        catch(TransformerException ex) {
            logger.error(ex.getMessage(), ex);
        }
    }
}
