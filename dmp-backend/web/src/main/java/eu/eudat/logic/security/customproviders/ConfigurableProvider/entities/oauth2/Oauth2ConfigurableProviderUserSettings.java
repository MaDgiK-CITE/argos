package eu.eudat.logic.security.customproviders.ConfigurableProvider.entities.oauth2;

public class Oauth2ConfigurableProviderUserSettings {
	private String id;
	private String name;
	private String email;
	private String user_info_url;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public String getUser_info_url() {
		return user_info_url;
	}
	public void setUser_info_url(String user_info_url) {
		this.user_info_url = user_info_url;
	}
}
