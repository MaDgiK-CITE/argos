package eu.eudat.logic.utilities.documents.xml.datasetProfileXml.datasetProfileModel.Fields;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "defaultValue")
public class DefaultValue {

    private String type;
    private String value;

    @XmlAttribute(name = "type")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @XmlAttribute(name = "value")
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public eu.eudat.models.data.components.commons.DefaultValue toAdminCompositeModelSection(){
        eu.eudat.models.data.components.commons.DefaultValue defaultValueEntity =new eu.eudat.models.data.components.commons.DefaultValue();
        defaultValueEntity.setValue(value);
        defaultValueEntity.setType(type);
        return defaultValueEntity;
    }
}
