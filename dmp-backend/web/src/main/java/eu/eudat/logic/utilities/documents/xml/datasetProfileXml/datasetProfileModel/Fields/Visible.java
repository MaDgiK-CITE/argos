package eu.eudat.logic.utilities.documents.xml.datasetProfileXml.datasetProfileModel.Fields;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.LinkedList;
import java.util.List;

@XmlRootElement(name = "visible")
public class Visible {

    private String style;
    private List<Rule> rule;

    @XmlAttribute(name = "style")
    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }
    @XmlElement(name = "rule")
    public List<Rule> getRule() {
        return rule;
    }

    public void setRule(List<Rule> rule) {
        this.rule = rule;
    }

    public eu.eudat.models.data.components.commons.Visibility toAdminCompositeModelSection(){
        eu.eudat.models.data.components.commons.Visibility visibilityEntity = new eu.eudat.models.data.components.commons.Visibility();
        visibilityEntity.setStyle(this.style);
        if(this.rule!=null) {
            List<eu.eudat.models.data.components.commons.Rule> ruleListEntity = new LinkedList<>();
            for (Rule xmlRule : this.rule) {
                ruleListEntity.add(xmlRule.toAdminCompositeModelSection());
            }
            visibilityEntity.setRules(ruleListEntity);
        }
        return visibilityEntity;
    }
}