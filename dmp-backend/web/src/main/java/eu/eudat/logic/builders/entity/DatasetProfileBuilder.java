package eu.eudat.logic.builders.entity;

import eu.eudat.data.entities.DescriptionTemplate;
import eu.eudat.data.entities.DescriptionTemplateType;
import eu.eudat.logic.builders.Builder;
import eu.eudat.data.entities.Dataset;

import java.util.Date;
import java.util.Set;
import java.util.UUID;

/**
 * Created by ikalyvas on 2/15/2018.
 */
public class DatasetProfileBuilder extends Builder<DescriptionTemplate> {

    private UUID id;

    private String label;

    private DescriptionTemplateType type;

    private Set<Dataset> dataset;

    private String definition;

    private Short status;

    private Date created;

    private Date modified = new Date();

    private String description;

    private String language;

    public DatasetProfileBuilder id(UUID id) {
        this.id = id;
        return this;
    }

    public DatasetProfileBuilder label(String label) {
        this.label = label;
        return this;
    }

    public DatasetProfileBuilder type(DescriptionTemplateType type) {
        this.type = type;
        return this;
    }

    public DatasetProfileBuilder dataset(Set<Dataset> dataset) {
        this.dataset = dataset;
        return this;
    }

    public DatasetProfileBuilder definition(String definition) {
        this.definition = definition;
        return this;
    }

    public DatasetProfileBuilder status(Short status) {
        this.status = status;
        return this;
    }

    public DatasetProfileBuilder created(Date created) {
        this.created = created;
        return this;
    }

    public DatasetProfileBuilder modified(Date modified) {
        this.modified = modified;
        return this;
    }

    public DatasetProfileBuilder description(String description) {
        this.description = description;
        return this;
    }

    public DatasetProfileBuilder language(String language) {
        this.language = language;
        return this;
    }

    @Override
    public DescriptionTemplate build() {
        DescriptionTemplate descriptionTemplate = new DescriptionTemplate();
        descriptionTemplate.setCreated(created);
        descriptionTemplate.setStatus(status);
        descriptionTemplate.setId(id);
        descriptionTemplate.setDataset(dataset);
        descriptionTemplate.setDefinition(definition);
        descriptionTemplate.setDescription(description);
        descriptionTemplate.setModified(modified);
        descriptionTemplate.setLabel(label);
        descriptionTemplate.setLanguage(language);
        descriptionTemplate.setType(type);
        return descriptionTemplate;
    }
}
