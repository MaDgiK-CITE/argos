package eu.eudat.logic.managers;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.eudat.data.dao.criteria.DataRepositoryCriteria;
import eu.eudat.data.entities.DataRepository;
import eu.eudat.logic.proxy.config.ExternalUrlCriteria;
import eu.eudat.logic.proxy.config.exceptions.HugeResultSet;
import eu.eudat.logic.proxy.config.exceptions.NoURLFound;
import eu.eudat.logic.services.ApiContext;
import eu.eudat.models.data.datarepository.DataRepositoryModel;
import eu.eudat.models.data.security.Principal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by ikalyvas on 9/3/2018.
 */
@Component
public class DataRepositoryManager {
    private ApiContext apiContext;

    @Autowired
    public DataRepositoryManager(ApiContext apiContext) {
        this.apiContext = apiContext;
    }

    public DataRepository create(eu.eudat.models.data.datarepository.DataRepositoryModel dataRepositoryModel, Principal principal) throws Exception {
        DataRepository dataRepository = dataRepositoryModel.toDataModel();
        dataRepository.getCreationUser().setId(principal.getId());
        return apiContext.getOperationsContext().getDatabaseRepository().getDataRepositoryDao().createOrUpdate(dataRepository);
    }

    public List<DataRepositoryModel> getDataRepositories(String query, String type, Principal principal) throws HugeResultSet, NoURLFound {
        ExternalUrlCriteria externalUrlCriteria = new ExternalUrlCriteria(query);
        List<Map<String, String>> remoteRepos = this.apiContext.getOperationsContext().getRemoteFetcher().getRepositories(externalUrlCriteria, type);

        DataRepositoryCriteria criteria = new DataRepositoryCriteria();
        if (!query.isEmpty()) criteria.setLike(query);
        criteria.setCreationUserId(principal.getId());

        List<DataRepositoryModel> dataRepositoryModels = new LinkedList<>();
         if (type.equals("")) {
             List<DataRepository> dataRepositoryList = (this.apiContext.getOperationsContext().getDatabaseRepository().getDataRepositoryDao().getWithCriteria(criteria)).toList();
             dataRepositoryModels = dataRepositoryList.stream().map(item -> new DataRepositoryModel().fromDataModel(item)).collect(Collectors.toList());
         }

        ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        dataRepositoryModels.addAll(remoteRepos.stream().map(item -> mapper.convertValue(item, DataRepositoryModel.class)).collect(Collectors.toList()));

        return dataRepositoryModels;
    }
    public List<DataRepositoryModel> getPubRepositories(String query, String type, Principal principal) throws HugeResultSet, NoURLFound {
        ExternalUrlCriteria externalUrlCriteria = new ExternalUrlCriteria(query);
        List<Map<String, String>> remoteRepos = this.apiContext.getOperationsContext().getRemoteFetcher().getPubRepositories(externalUrlCriteria, type);

        DataRepositoryCriteria criteria = new DataRepositoryCriteria();
        if (!query.isEmpty()) criteria.setLike(query);
        criteria.setCreationUserId(principal.getId());

        List<DataRepositoryModel> dataRepositoryModels = new LinkedList<>();
        if (type.equals("")) {
            List<DataRepository> dataRepositoryList = (this.apiContext.getOperationsContext().getDatabaseRepository().getDataRepositoryDao().getWithCriteria(criteria)).toList();
            dataRepositoryModels = dataRepositoryList.stream().map(item -> new DataRepositoryModel().fromDataModel(item)).collect(Collectors.toList());
        }

        ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        dataRepositoryModels.addAll(remoteRepos.stream().map(item -> mapper.convertValue(item, DataRepositoryModel.class)).collect(Collectors.toList()));

        return dataRepositoryModels;
    }
    public List<DataRepositoryModel> getJournals(String query, String type, Principal principal) throws HugeResultSet, NoURLFound {
        ExternalUrlCriteria externalUrlCriteria = new ExternalUrlCriteria(query);
        List<Map<String, String>> remoteRepos = this.apiContext.getOperationsContext().getRemoteFetcher().getJournals(externalUrlCriteria, type);

        DataRepositoryCriteria criteria = new DataRepositoryCriteria();
        if (!query.isEmpty()) criteria.setLike(query);
        criteria.setCreationUserId(principal.getId());

        List<DataRepositoryModel> dataRepositoryModels = new LinkedList<>();
        if (type.equals("")) {
            List<DataRepository> dataRepositoryList = (this.apiContext.getOperationsContext().getDatabaseRepository().getDataRepositoryDao().getWithCriteria(criteria)).toList();
            dataRepositoryModels = dataRepositoryList.stream().map(item -> new DataRepositoryModel().fromDataModel(item)).collect(Collectors.toList());
        }

        ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        dataRepositoryModels.addAll(remoteRepos.stream().map(item -> mapper.convertValue(item, DataRepositoryModel.class)).collect(Collectors.toList()));

        return dataRepositoryModels;
    }
}
