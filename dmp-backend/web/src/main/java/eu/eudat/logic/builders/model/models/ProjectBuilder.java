package eu.eudat.logic.builders.model.models;


import eu.eudat.data.entities.UserInfo;
import eu.eudat.logic.builders.Builder;
import eu.eudat.models.data.dmp.DataManagementPlan;
import eu.eudat.models.data.project.Project;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public class ProjectBuilder extends Builder<Project> {

	private UUID id;
	private List<DataManagementPlan> dmps;
	private String label;
	private int type;
	private String abbreviation;
	private String reference;
	private String uri;
	private String definition;
	private Date startDate;
	private Date endDate;
	private eu.eudat.data.entities.Project.Status status;
	private UserInfo creationUser;
	private Date created;
	private Date modified;
	private String description;
	private String source;
	private String key;

	public ProjectBuilder id(UUID id) {
		this.id = id;
		return this;
	}

	public ProjectBuilder dmps(List<DataManagementPlan> dmps) {
		this.dmps = dmps;
		return this;
	}

	public ProjectBuilder label(String label) {
		this.label = label;
		return this;
	}

	public ProjectBuilder type(int type) {
		this.type = type;
		return this;
	}

	public ProjectBuilder abbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
		return this;
	}

	public ProjectBuilder reference(String reference) {
		this.reference = reference;
		return this;
	}

	public ProjectBuilder uri(String uri) {
		this.uri = uri;
		return this;
	}

	public ProjectBuilder definition(String definition) {
		this.definition = definition;
		return this;
	}

	public ProjectBuilder startDate(Date startDate) {
		this.startDate = startDate;
		return this;
	}

	public ProjectBuilder endDate(Date endDate) {
		this.endDate = endDate;
		return this;
	}

	public ProjectBuilder status(eu.eudat.data.entities.Project.Status status) {
		this.status = status;
		return this;
	}

	public ProjectBuilder creationUser(UserInfo creationUser) {
		this.creationUser = creationUser;
		return this;
	}

	public ProjectBuilder created(Date created) {
		this.created = created;
		return this;
	}

	public ProjectBuilder modified(Date modified) {
		this.modified = modified;
		return this;
	}

	public ProjectBuilder description(String description) {
		this.description = description;
		return this;
	}

	public ProjectBuilder source(String source) {
		this.source = source;
		return this;
	}

	public ProjectBuilder key(String key) {
		this.key = key;
		return this;
	}

	@Override
	public Project build() {
		Project project = new Project();
		project.setStatus(status.getValue());
		project.setAbbreviation(abbreviation);
		project.setCreated(created);
		project.setCreationUser(creationUser);
		project.setDefinition(definition);
		project.setDescription(description);
		project.setDmps(dmps);
		project.setEndDate(endDate);
		project.setId(id);
		project.setLabel(label);
		project.setModified(modified);
		project.setReference(reference);
		project.setCreationUser(creationUser);
		project.setStartDate(startDate);
		project.setSource(source);
		project.setKey(key);
		return project;
	}
}
