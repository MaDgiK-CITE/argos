package eu.eudat.logic.managers;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.eudat.data.dao.criteria.DataManagementPlanCriteria;
import eu.eudat.data.dao.entities.UserInfoDao;
import eu.eudat.data.entities.*;
import eu.eudat.data.query.items.table.userinfo.UserInfoTableRequestItem;
import eu.eudat.exceptions.security.NullEmailException;
import eu.eudat.exceptions.security.UnauthorisedException;
import eu.eudat.logic.builders.entity.UserRoleBuilder;
import eu.eudat.logic.builders.model.models.DataTableDataBuilder;
import eu.eudat.logic.security.customproviders.Zenodo.ZenodoAccessType;
import eu.eudat.logic.security.customproviders.Zenodo.ZenodoCustomProvider;
import eu.eudat.logic.security.validators.zenodo.helpers.ZenodoResponseToken;
import eu.eudat.logic.services.ApiContext;
import eu.eudat.logic.services.operations.authentication.AuthenticationService;
import eu.eudat.logic.utilities.builders.XmlBuilder;
import eu.eudat.models.HintedModelFactory;
import eu.eudat.models.data.dmp.DataManagementPlan;
import eu.eudat.models.data.doi.DOIRequest;
import eu.eudat.models.data.helpers.common.DataTableData;
import eu.eudat.models.data.login.Credentials;
import eu.eudat.models.data.principal.PrincipalModel;
import eu.eudat.models.data.security.Principal;
import eu.eudat.models.data.userinfo.UserCredential;
import eu.eudat.models.data.userinfo.UserListingModel;
import eu.eudat.models.data.userinfo.UserProfile;
import eu.eudat.queryable.QueryableList;
import eu.eudat.types.Authorities;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class UserManager {
	private static final Logger logger = LoggerFactory.getLogger(UserManager.class);

	private ApiContext apiContext;
	private ZenodoCustomProvider zenodoCustomProvider;
	private Environment environment;

	@Autowired
	public UserManager(ApiContext apiContext, ZenodoCustomProvider zenodoCustomProvider, Environment environment) {
		this.apiContext = apiContext;
		this.zenodoCustomProvider = zenodoCustomProvider;
		this.environment = environment;
	}

	public eu.eudat.models.data.user.composite.DatasetProfile generateDatasetProfileModel(DescriptionTemplate profile) {
		Document viewStyleDoc = XmlBuilder.fromXml(profile.getDefinition());
		Element root = (Element) viewStyleDoc.getDocumentElement();
		eu.eudat.models.data.entities.xmlmodels.datasetprofiledefinition.ViewStyleModel viewstyle = new eu.eudat.models.data.entities.xmlmodels.datasetprofiledefinition.ViewStyleModel().fromXml(root);

		eu.eudat.models.data.user.composite.DatasetProfile datasetprofile = new eu.eudat.models.data.user.composite.DatasetProfile();
		datasetprofile.buildProfile(viewstyle);

		return datasetprofile;
	}

	public DataTableData<UserListingModel> getPaged(UserInfoTableRequestItem userInfoTableRequestItem) throws Exception {
		QueryableList<eu.eudat.data.entities.UserInfo> users = apiContext.getOperationsContext().getDatabaseRepository().getUserInfoDao().getWithCriteria(userInfoTableRequestItem.getCriteria()).withHint(HintedModelFactory.getHint(UserListingModel.class));
		QueryableList<eu.eudat.data.entities.UserInfo> pagedUsers = PaginationManager.applyPaging(users, userInfoTableRequestItem);

		List<UserListingModel> modelUsers = pagedUsers.select(item -> new UserListingModel().fromDataModel(item));
		return apiContext.getOperationsContext().getBuilderFactory().getBuilder(DataTableDataBuilder.class).totalCount(users.count()).data(modelUsers).build();
	}
	
	public List<UserCredential> getCredentials(UUID userId) {
		List<UserCredential> results = new ArrayList<>();
		eu.eudat.data.entities.UserInfo user = apiContext.getOperationsContext().getDatabaseRepository().getUserInfoDao().find(userId);
		List<Credential> credentials = apiContext.getOperationsContext().getDatabaseRepository().getCredentialDao().asQueryable().where((builder, root) -> builder.equal(root.get("userInfo"), user)).toList();
		credentials.forEach(credential -> {
			UserCredential userCredential = new UserCredential();
			userCredential.setEmail(credential.getEmail());
			userCredential.setProvider(credential.getProvider());
			results.add(userCredential);
		});
		return results;
	}

	public UserProfile getSingle(UUID userId) throws Exception {
		eu.eudat.data.entities.UserInfo user = apiContext.getOperationsContext().getDatabaseRepository().getUserInfoDao().find(userId);
		UserProfile profile = new UserProfile().fromDataModel(user);
		List<Integer> roles = new LinkedList<>();
		DataManagementPlanCriteria criteria = new DataManagementPlanCriteria();
		criteria.setAllVersions(false);
		QueryableList<DMP> items = apiContext.getOperationsContext().getDatabaseRepository().getDmpDao().getWithCriteria(criteria);
		List<DMP> dmps = apiContext.getOperationsContext().getDatabaseRepository().getDmpDao().getAuthenticated(items, userId, roles).take(5).toList();
		profile.setAssociatedDmps(dmps.stream().map(x -> new DataManagementPlan().fromDataModel(x)).collect(Collectors.toList()));
		return profile;
	}

	public void editRoles(UserListingModel user) {
		eu.eudat.data.entities.UserInfo userInfo = apiContext.getOperationsContext().getDatabaseRepository().getUserInfoDao().find(user.getId());
		userInfo.getUserRoles().stream().forEach(item -> apiContext.getOperationsContext().getDatabaseRepository().getUserRoleDao().delete(item));
		for (Integer role : user.getAppRoles()) {
			UserRole userRole = apiContext.getOperationsContext().getBuilderFactory().getBuilder(UserRoleBuilder.class).role(role).userInfo(userInfo).build();
			apiContext.getOperationsContext().getDatabaseRepository().getUserRoleDao().createOrUpdate(userRole);
		}
	}

	public void updateSettings(Map<String, Object> settings, Principal principal) throws IOException {
		eu.eudat.data.entities.UserInfo userInfo = apiContext.getOperationsContext().getDatabaseRepository().getUserInfoDao().find(principal.getId());
		apiContext.getOperationsContext().getDatabaseRepository().detachEntity(userInfo);
		HashMap<String, Object> result =
				new ObjectMapper().readValue(userInfo.getAdditionalinfo(), HashMap.class);
		userInfo.setName(settings.entrySet().stream().filter(entry -> entry.getKey().equals("name")).filter(Objects::nonNull).map(entry -> entry.getValue().toString()).findFirst().orElse(userInfo.getName()));
		settings.remove("name");
		result.putAll(settings);
		userInfo.setAdditionalinfo(new JSONObject(result).toString());
		apiContext.getOperationsContext().getDatabaseRepository().getUserInfoDao()
				.createOrUpdate(userInfo);
	}

	public PrincipalModel authenticate(AuthenticationService authenticationServiceImpl, Credentials credentials) throws NullEmailException {
		Principal principal = authenticationServiceImpl.Touch(credentials);
		if (principal == null) throw new UnauthorisedException("Could not Sign In User");
		PrincipalModel principalModel = PrincipalModel.fromEntity(principal);
		return principalModel;
	}

	public DataTableData<UserListingModel> getCollaboratorsPaged(UserInfoTableRequestItem userInfoTableRequestItem, Principal principal) throws Exception {
		UserInfoDao userInfoDao = apiContext.getOperationsContext().getDatabaseRepository().getUserInfoDao();
		QueryableList<UserInfo> users = userInfoDao.getWithCriteria(userInfoTableRequestItem.getCriteria());

		List<UserListingModel> colaborators = userInfoDao.getAuthenticated(users, principal.getId())
				.withHint(HintedModelFactory.getHint(UserListingModel.class))
				.select(colaborator -> new UserListingModel().fromDataModel(colaborator));

		DataTableData<UserListingModel> dataTableData = new DataTableData<>();
		dataTableData.setData(colaborators);
		dataTableData.setTotalCount((long) colaborators.size());
		return dataTableData;
	}

	public void registerDOIToken(DOIRequest doiRequest, Principal principal) throws IOException {
		ZenodoResponseToken responseToken = this.zenodoCustomProvider.getAccessToken(ZenodoAccessType.AUTHORIZATION_CODE,
				doiRequest.getZenodoRequest().getCode(), this.environment.getProperty("zenodo.login.client_id"),
				this.environment.getProperty("zenodo.login.client_secret"), doiRequest.getRedirectUri());
		Map<String, Object> settings = new HashMap<>();
		settings.put("zenodoEmail", responseToken.getEmail());
		settings.put("zenodoRefresh", responseToken.getRefreshToken());
		settings.put("zenodoToken", responseToken.getAccessToken());
		settings.put("expirationDate",  Instant.now().plusSeconds(responseToken.getExpiresIn()).toEpochMilli());
		this.updateSettings(settings, principal);
	}

	public void deleteDOIToken(Principal principal) throws IOException {
		Map<String, Object> settings = new HashMap<>();
		settings.put("zenodoEmail", "");
		settings.put("zenodoRefresh", "");
		settings.put("zenodoToken", "");
		settings.put("expirationDate",  0);
		this.updateSettings(settings, principal);
	}
	
	public ResponseEntity<byte[]> exportToCsv(Principal principal) throws IOException {
        if (!principal.getAuthz().contains(Authorities.ADMIN))
            throw new UnauthorisedException();
        
        List<UserInfo> users = this.apiContext.getOperationsContext().getDatabaseRepository().getUserInfoDao().asQueryable().toList();
        StringBuilder resultBuilder = new StringBuilder();
        resultBuilder.append("User Id").append(",").append("User Name").append(",").append("User Email").append("\n");
        users.stream().forEach(user -> resultBuilder.append(user.getId().toString()).append(",")
        		.append(user.getName()).append(",")
        		.append(user.getEmail()).append("\n"));
        String result = resultBuilder.toString();
        String fileName = "Users_dump";//dmp.getLabel();
        fileName = fileName.replaceAll("[^a-zA-Z0-9+ ]", "");
        String uuid = UUID.randomUUID().toString();
        File file = new File(this.environment.getProperty("temp.temp") + uuid + ".csv");
        OutputStream output = new FileOutputStream(file);
        try {
//            mapper.writeValue(file, rdaExportModel);
            output.write(result.getBytes());
            output.flush();
            output.close();
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }

        InputStream resource = new FileInputStream(file);
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentLength(file.length());
        responseHeaders.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        responseHeaders.set("Content-Disposition", "attachment;filename=" + fileName + ".csv");
        responseHeaders.set("Access-Control-Expose-Headers", "Content-Disposition");
        responseHeaders.get("Access-Control-Expose-Headers").add("Content-Type");

        byte[] content = org.apache.poi.util.IOUtils.toByteArray(resource);
        resource.close();
        Files.deleteIfExists(file.toPath());
        return new ResponseEntity<>(content, responseHeaders, HttpStatus.OK);
    }

    public UserProfile getFromEmail(String email) {
		UserInfo user = apiContext.getOperationsContext().getDatabaseRepository().getUserInfoDao().asQueryable().where((builder, root) -> builder.equal(root.get("email"), email)).getSingle();
		return new UserProfile().fromDataModel(user);
	}

	public Long countActiveUsers(){
		return apiContext.getOperationsContext().getDatabaseRepository().getUserTokenDao().asQueryable().where(((builder, root) -> builder.greaterThan(root.get("expiresAt"), new Date()))).count();
	}

	public Long countAllUsers(){
		return apiContext.getOperationsContext().getDatabaseRepository().getUserInfoDao().asQueryable().count();
	}
	
}
