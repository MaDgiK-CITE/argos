package eu.eudat.logic.security.repositorydeposit.mapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.eudat.data.entities.*;
import eu.eudat.depositinterface.models.*;
import eu.eudat.logic.utilities.builders.XmlBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.xpath.*;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DMPToDepositMapper {

    private static final Logger logger = LoggerFactory.getLogger(DMPToDepositMapper.class);
    private static final ObjectMapper mapper = new ObjectMapper();

    public static DMPDepositModel fromDMP(DMP entity, FileEnvelope pdfFile, FileEnvelope jsonFile, File supportingFilesZip, String previousDOI) {
        DMPDepositModel deposit = new DMPDepositModel();
        deposit.setId(entity.getId());
        deposit.setVersion(entity.getVersion());
        deposit.setLabel(entity.getLabel());
        deposit.setDescription(entity.getDescription());
        deposit.setPublic(entity.isPublic());
        deposit.setDatasets(entity.getDataset().stream().map(DMPToDepositMapper::fromDataset).collect(Collectors.toList()));
        deposit.setUsers(entity.getUsers().stream().map(DMPToDepositMapper::fromUserDMP).collect(Collectors.toSet()));
        deposit.setOrganisations(entity.getOrganisations().stream().map(DMPToDepositMapper::fromOrganisation).collect(Collectors.toSet()));
        deposit.setResearchers(entity.getResearchers().stream().map(DMPToDepositMapper::fromResearcher).collect(Collectors.toSet()));
        if (entity.getGrant() != null) {
            deposit.setGrant(fromGrant(entity.getGrant()));
        }

        deposit.setPdfFile(pdfFile);
        deposit.setRdaJsonFile(jsonFile);
        deposit.setSupportingFilesZip(supportingFilesZip);
        deposit.setPreviousDOI(previousDOI);

        deposit.setExtraProperties(entity.getExtraProperties());
        return deposit;
    }

    private static DatasetDepositModel fromDataset(Dataset entity){
        DatasetDepositModel deposit = new DatasetDepositModel();
        deposit.setLabel(entity.getLabel());
        deposit.setDescription(entity.getDescription());
        deposit.setProfileDefinition(entity.getProfile().getDefinition());
        deposit.setProperties(entity.getProperties());
        deposit.setFields(fromDefinitionAndProperties(deposit.getProfileDefinition(), deposit.getProperties()));
        return deposit;
    }

    private static List<DatasetFieldsDepositModel> fromDefinitionAndProperties(String definition, String properties){
        List<DatasetFieldsDepositModel> deposit = new ArrayList<>();
        try {

            Map<String, Object> datasetAnswers = mapper.readValue(properties, HashMap.class);

            Document document = XmlBuilder.fromXml(definition);
            XPathFactory xpathFactory = XPathFactory.newInstance();
            XPath xpath = xpathFactory.newXPath();
            XPathExpression expr = xpath.compile("//schematics");
            NodeList schematics = (NodeList) expr.evaluate(document, XPathConstants.NODESET);
            for (int i = 0; i < schematics.getLength(); i++) {
                Node schematicsNode = schematics.item(i);
                NodeList schematicsList = schematicsNode.getChildNodes();
                DatasetFieldsDepositModel fieldDeposit = new DatasetFieldsDepositModel();
                List<String> schematicsDeposit = new ArrayList<>();
                if(schematicsList != null){
                    for(int j = 0; j < schematicsList.getLength(); j++){
                        Node schematic = schematicsList.item(j);
                        if(schematic.getTextContent().matches(".*\\w+.*")) {
                            schematicsDeposit.add(schematic.getTextContent());
                        }
                    }
                }
                fieldDeposit.setSchematics(schematicsDeposit);
                String fieldId = schematicsNode.getParentNode().getAttributes().getNamedItem("id").getNodeValue();
                Object value = datasetAnswers.get(fieldId);
                fieldDeposit.setValue(value);
                Element field = (Element) schematicsNode.getParentNode();
                Element viewStyle = (Element) field.getElementsByTagName("viewStyle").item(0);
                String renderStyle = viewStyle.getAttribute("renderstyle");
                fieldDeposit.setRenderStyleType(renderStyle);
                Element data = (Element) field.getElementsByTagName("data").item(0);
                String multipleSelection = data.getAttribute("multiList");
                String multipleAutoComplete = data.getAttribute("multiAutoComplete");
                if(!multipleSelection.isEmpty()){
                    fieldDeposit.setMultiple(Boolean.parseBoolean(multipleSelection));
                }
                else if(!multipleAutoComplete.isEmpty()){
                    fieldDeposit.setMultiple(Boolean.parseBoolean(multipleAutoComplete));
                }
                else{
                    fieldDeposit.setMultiple(false);
                }
                deposit.add(fieldDeposit);
            }
        }
        catch (XPathExpressionException | JsonProcessingException ex){
            logger.error(ex.getMessage(), ex);
            return null;
        }
        return deposit;
    }

    private static UserDMPDepositModel fromUserDMP(UserDMP entity){
        UserDMPDepositModel deposit = new UserDMPDepositModel();
        deposit.setUser(fromUserInfo(entity.getUser()));
        deposit.setRole(entity.getRole());
        return deposit;
    }

    private static UserInfoDepositModel fromUserInfo(UserInfo entity){
        UserInfoDepositModel deposit = new UserInfoDepositModel();
        deposit.setName(entity.getName());
        deposit.setEmail(entity.getEmail());
        return deposit;
    }

    private static OrganisationDepositModel fromOrganisation(Organisation entity){
        OrganisationDepositModel deposit = new OrganisationDepositModel();
        deposit.setLabel(entity.getLabel());
        return deposit;
    }

    private static ResearcherDepositModel fromResearcher(Researcher entity){
        ResearcherDepositModel deposit = new ResearcherDepositModel();
        deposit.setLabel(entity.getLabel());
        deposit.setReference(entity.getReference());
        return deposit;
    }

    private static GrantDepositModel fromGrant(Grant entity){
        GrantDepositModel deposit = new GrantDepositModel();
        deposit.setId(entity.getId());
        deposit.setReference(entity.getReference());
        deposit.setFunder(fromFunder(entity.getFunder()));
        return deposit;
    }

    private static FunderDepositModel fromFunder(Funder entity){
        FunderDepositModel deposit = new FunderDepositModel();
        deposit.setLabel(entity.getLabel());
        return deposit;
    }
}
