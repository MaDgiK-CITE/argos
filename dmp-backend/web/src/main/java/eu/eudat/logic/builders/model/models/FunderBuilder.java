package eu.eudat.logic.builders.model.models;

import eu.eudat.logic.builders.Builder;
import eu.eudat.models.data.funder.Funder;

import java.util.Date;
import java.util.UUID;

public class FunderBuilder extends Builder<Funder> {

	private UUID id;
	private String label;
	private String reference;
	private String definition;
	private eu.eudat.data.entities.Funder.Status status;
	private Date created;
	private Date modified;
	private Integer type;
	private String source;
	private String key;

	public FunderBuilder id(UUID id) {
		this.id = id;
		return this;
	}

	public FunderBuilder label(String label) {
		this.label = label;
		return this;
	}

	public FunderBuilder reference(String reference) {
		this.reference = reference;
		return this;
	}

	public FunderBuilder definition(String definition) {
		this.definition = definition;
		return this;
	}

	public FunderBuilder status(eu.eudat.data.entities.Funder.Status status) {
		this.status = status;
		return this;
	}

	public FunderBuilder created(Date created) {
		this.created = created;
		return this;
	}

	public FunderBuilder modified(Date modified) {
		this.modified = modified;
		return this;
	}

	public FunderBuilder type(int type) {
		this.type = type;
		return this;
	}

	public FunderBuilder source(String source) {
		this.source = source;
		return this;
	}

	public FunderBuilder key(String key) {
		this.key = key;
		return this;
	}

	@Override
	public Funder build() {
		Funder funder = new Funder();
		funder.setId(id);
		funder.setLabel(label);
		funder.setReference(reference);
		funder.setDefinition(definition);
		if (status != null) funder.setStatus(status.getValue());
		funder.setCreated(created);
		funder.setModified(modified);
		funder.setType(type);
		funder.setSource(source);
		funder.setKey(key);
		return funder;
	}
}
