package eu.eudat.logic.utilities.documents.xml.dmpXml.dmpBlueprintModel;

import eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition.types.FieldCategory;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.UUID;

@XmlRootElement(name = "extraField")
public class ExtraField {

    private String id;
    private int type;
    private String label;
    private String placeholder;
    private String description;
    private int ordinal;
    private boolean required;

    @XmlAttribute(name = "id")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @XmlAttribute(name = "type")
    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @XmlAttribute(name = "label")
    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @XmlAttribute(name = "placeholder")
    public String getPlaceholder() {
        return placeholder;
    }

    public void setPlaceholder(String placeholder) {
        this.placeholder = placeholder;
    }

    @XmlAttribute(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlAttribute(name = "ordinal")
    public int getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(int ordinal) {
        this.ordinal = ordinal;
    }

    @XmlAttribute(name = "required")
    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition.FieldModel toDmpBlueprintCompositeModel() {
        eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition.FieldModel systemField = new eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition.FieldModel();
        systemField.setId(UUID.fromString(this.id));
        systemField.setCategory(FieldCategory.EXTRA);
        systemField.setType(this.type);
        systemField.setLabel(this.label);
        systemField.setPlaceholder(this.placeholder);
        systemField.setDescription(this.description);
        systemField.setOrdinal(this.ordinal);
        systemField.setRequired(this.required);
        return systemField;
    }

}
