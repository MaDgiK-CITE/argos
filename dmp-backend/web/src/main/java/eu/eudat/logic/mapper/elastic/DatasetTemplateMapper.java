package eu.eudat.logic.mapper.elastic;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.eudat.data.entities.DMPDatasetProfile;
import eu.eudat.data.entities.DescriptionTemplate;
import eu.eudat.elastic.entities.DatasetTempalate;

import java.util.HashMap;
import java.util.Map;

public class DatasetTemplateMapper {

	public static DatasetTempalate toElastic(DMPDatasetProfile profile) {
		DatasetTempalate elastic = new DatasetTempalate();
		elastic.setId(profile.getDatasetprofile().getId());
		elastic.setName(profile.getDatasetprofile().getLabel());
		try {
			elastic.setData(new ObjectMapper().readValue(profile.getData(), new TypeReference<Map<String, Object>>() {}));
		}
		catch (Exception e){
			elastic.setData(new HashMap<>());
		}
		return elastic;
	}
}
