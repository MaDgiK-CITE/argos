package eu.eudat.logic.builders.model.criteria;

import eu.eudat.logic.builders.Builder;
import eu.eudat.data.dao.criteria.ServiceCriteria;

/**
 * Created by ikalyvas on 2/15/2018.
 */
public class ServiceCriteriaBuilder extends Builder<ServiceCriteria> {
    private String like;

    public ServiceCriteriaBuilder setLike(String like) {
        this.like = like;
        return this;
    }

    @Override
    public ServiceCriteria build() {
        ServiceCriteria serviceCriteria = new ServiceCriteria();
        serviceCriteria.setLike(like);
        return serviceCriteria;
    }
}
