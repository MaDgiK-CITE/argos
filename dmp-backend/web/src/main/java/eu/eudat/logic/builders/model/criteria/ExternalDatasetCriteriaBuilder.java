package eu.eudat.logic.builders.model.criteria;

import eu.eudat.logic.builders.Builder;
import eu.eudat.data.dao.criteria.ExternalDatasetCriteria;

/**
 * Created by ikalyvas on 2/15/2018.
 */
public class ExternalDatasetCriteriaBuilder extends Builder<ExternalDatasetCriteria> {
    private String like;

    public ExternalDatasetCriteriaBuilder like(String like) {
        this.like = like;
        return this;
    }

    @Override
    public ExternalDatasetCriteria build() {
        ExternalDatasetCriteria externalDatasetCriteria = new ExternalDatasetCriteria();
        externalDatasetCriteria.setLike(like);
        return externalDatasetCriteria;
    }
}
