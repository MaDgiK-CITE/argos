package eu.eudat.logic.utilities.documents.types;

/**
 * Created by ikalyvas on 2/27/2018.
 */
public enum TextStyle {
    ITALIC(0), BOLD(1), CAPS(2);

    private Integer value;

    private TextStyle(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }

    public static TextStyle fromInteger(Integer value) {
        switch (value) {
            case 0:
                return ITALIC;
            case 1:
                return BOLD;
            case 2:
                return CAPS;
            default:
                throw new RuntimeException("Unsupported TextStyle Code");
        }
    }
}
