package eu.eudat.logic.proxy.config.entities;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.List;

public class PrefillingGet{
    private String url;
    private List<DefaultPrefillingMapping> mappings;
    private List<PrefillingFixedMapping> fixedMappings;

    public String getUrl() {
        return url;
    }

    @XmlElement(name = "url")
    public void setUrl(String url) {
        this.url = url;
    }

    public List<DefaultPrefillingMapping> getMappings() {
        return mappings;
    }

    @XmlElement(name = "mapping")
    @XmlElementWrapper
    public void setMappings(List<DefaultPrefillingMapping> mappings) {
        this.mappings = mappings;
    }

    public List<PrefillingFixedMapping> getFixedMappings() {
        return fixedMappings;
    }

    @XmlElement(name = "fixedMapping")
    @XmlElementWrapper
    public void setFixedMappings(List<PrefillingFixedMapping> fixedMappings) {
        this.fixedMappings = fixedMappings;
    }
}
