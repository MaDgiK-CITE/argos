package eu.eudat.logic.utilities.documents.xml.datasetProfileXml.datasetProfileModel.Fields;

import eu.eudat.logic.utilities.builders.ModelBuilder;
import eu.eudat.models.data.components.commons.datafield.FieldData;
import org.w3c.dom.Element;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.LinkedList;
import java.util.List;

@XmlRootElement(name = "field")
public class Field {

    private String id;

    private int ordinal;

    private String numbering;

    private List<validations> validations;

    private DefaultValue defaultValue;

    private Visible visible;

    private ViewStyle viewStyle;

    private Object data;

    private Schematics schematics;

    @XmlAttribute(name = "id")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @XmlAttribute(name = "ordinal")
    public int getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(int ordinal) {
        this.ordinal = ordinal;
    }

    @XmlElement(name = "numbering")
    public String getNumbering() {
        return numbering;
    }

    public void setNumbering(String numbering) {
        this.numbering = numbering;
    }

    @XmlElement(name = "validations")
    public List<eu.eudat.logic.utilities.documents.xml.datasetProfileXml.datasetProfileModel.Fields.validations> getValidations() {
        return validations;
    }

    public void setValidations(List<eu.eudat.logic.utilities.documents.xml.datasetProfileXml.datasetProfileModel.Fields.validations> validations) {
        this.validations = validations;
    }

    @XmlElement(name = "defaultValue")
    public DefaultValue getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(DefaultValue defaultValue) {
        this.defaultValue = defaultValue;
    }

    @XmlElement(name = "visible")
    public Visible getVisible() {
        return visible;
    }

    public void setVisible(Visible visible) {
        this.visible = visible;
    }

    @XmlElement(name = "viewStyle")
    public ViewStyle getViewStyle() {
        return viewStyle;
    }

    public void setViewStyle(ViewStyle viewStyle) {
        this.viewStyle = viewStyle;
    }

    @XmlElement(name = "data")
    public Object getData() {
        return this.data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @XmlElement(name = "schematics")
    public Schematics getSchematics() {
        return schematics;
    }

    public void setSchematics(Schematics schematics) {
        this.schematics = schematics;
    }

    public  eu.eudat.models.data.admin.components.datasetprofile.Field toAdminCompositeModelSection() {
        eu.eudat.models.data.admin.components.datasetprofile.Field fieldEntity =new  eu.eudat.models.data.admin.components.datasetprofile.Field();
        fieldEntity.setId(this.id);
        fieldEntity.setOrdinal(this.ordinal);
        List<Integer> validationList = new LinkedList<>();
        for(validations validation:this.validations){
            if(validation.getValidation()!=null)
            validationList.add(validation.toAdminCompositeModelSection());
        }
        fieldEntity.setValidations(validationList);
        fieldEntity.setDefaultValue(this.defaultValue.toAdminCompositeModelSection());
        fieldEntity.setVisible(this.visible.toAdminCompositeModelSection());
        fieldEntity.setViewStyle(this.viewStyle.toAdminCompositeModelSection());
        FieldData data = new ModelBuilder().toFieldData(null, this.viewStyle.getRenderStyle(), (Element) this.data);
//        fieldEntity.setData( data.fromXml((Element) this.data));
        if (data != null) {
            fieldEntity.setData(data.toMap((Element) this.data));
        }
        List<String> schematicsList = new LinkedList<>();
        if (this.schematics != null && this.schematics.getSchematics() != null) {
            for (Schematic schematic : this.schematics.getSchematics()) {
                if (schematic != null && schematic.getSchematic() != null && !schematic.getSchematic().isEmpty())
                    schematicsList.add(schematic.getSchematic());
            }
        }
        fieldEntity.setSchematics(schematicsList);
        return fieldEntity;
    }
}
