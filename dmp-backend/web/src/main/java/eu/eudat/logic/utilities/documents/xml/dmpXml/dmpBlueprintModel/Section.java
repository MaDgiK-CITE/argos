package eu.eudat.logic.utilities.documents.xml.dmpXml.dmpBlueprintModel;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

@XmlRootElement(name = "section")
public class Section {

    private String id;
    private String label;
    private String description;
    private int ordinal;
    private SystemFields systemFields;
    private ExtraFields extraFields;
    private boolean hasTemplates;
    private DescriptionTemplates descriptionTemplates;

    @XmlAttribute(name = "id")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @XmlAttribute(name = "label")
    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @XmlAttribute(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlAttribute(name = "ordinal")
    public int getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(int ordinal) {
        this.ordinal = ordinal;
    }

    @XmlElement(name = "systemFields")
    public SystemFields getSystemFields() {
        return systemFields;
    }

    public void setSystemFields(SystemFields systemFields) {
        this.systemFields = systemFields;
    }

    @XmlElement(name = "extraFields")
    public ExtraFields getExtraFields() {
        return extraFields;
    }

    public void setExtraFields(ExtraFields extraFields) {
        this.extraFields = extraFields;
    }

    @XmlAttribute(name = "hasTemplates")
    public boolean isHasTemplates() {
        return hasTemplates;
    }

    public void setHasTemplates(boolean hasTemplates) {
        this.hasTemplates = hasTemplates;
    }

    @XmlElement(name = "descriptionTemplates")
    public DescriptionTemplates getDescriptionTemplates() {
        return descriptionTemplates;
    }

    public void setDescriptionTemplates(DescriptionTemplates descriptionTemplates) {
        this.descriptionTemplates = descriptionTemplates;
    }

    public eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition.Section toDmpBlueprintCompositeModel() {
        eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition.Section section = new eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition.Section();
        section.setId(UUID.fromString(this.id));
        section.setLabel(this.label);
        section.setDescription(this.description);
        section.setOrdinal(this.ordinal);
        section.setHasTemplates(this.hasTemplates);
        List<eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition.FieldModel> dmpBlueprintFieldModels = new LinkedList<>();
        if (this.systemFields != null && this.systemFields.getSystemFields() != null) {
            for (SystemField systemField : this.systemFields.getSystemFields()) {
                dmpBlueprintFieldModels.add(systemField.toDmpBlueprintCompositeModel());
            }
        }
        if (this.extraFields != null&& this.extraFields.getExtraFields() != null) {
            for (ExtraField extraField : this.extraFields.getExtraFields()) {
                dmpBlueprintFieldModels.add(extraField.toDmpBlueprintCompositeModel());
            }
        }
        section.setFields(dmpBlueprintFieldModels);
        List<eu.eudat.models.data.entities.xmlmodels.dmpprofiledefinition.DescriptionTemplate> dmpBlueprintDescriptionTemplates = new LinkedList<>();
        if (this.descriptionTemplates != null && this.descriptionTemplates.getDescriptionTemplates() != null) {
            for (DescriptionTemplate descriptionTemplate : this.descriptionTemplates.getDescriptionTemplates()) {
                dmpBlueprintDescriptionTemplates.add(descriptionTemplate.toDmpBlueprintCompositeModel());
            }
        }
        section.setDescriptionTemplates(dmpBlueprintDescriptionTemplates);
        return section;
    }

}
