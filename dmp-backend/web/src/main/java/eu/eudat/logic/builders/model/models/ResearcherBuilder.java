package eu.eudat.logic.builders.model.models;

import eu.eudat.logic.builders.Builder;
import eu.eudat.models.data.dmp.Researcher;

public class ResearcherBuilder extends Builder<Researcher> {
    private String label;
    private String name;
    private String id;
    private String reference;
    private int status;
    private String tag;
    private String key;

    public String getLabel() {
        return label;
    }

    public ResearcherBuilder label(String label) {
        this.label = label;
        return this;
    }

    public String getName() {
        return name;
    }

    public ResearcherBuilder name(String name) {
        this.name = name;
        return this;
    }

    public String getId() {
        return id;
    }

    public ResearcherBuilder id(String id) {
        this.id = id;
        return this;
    }

    public String getReference() { return reference; }

    public ResearcherBuilder reference(String reference) {
        this.reference = reference;
        return this;
    }

    public int getStatus() {
        return status;
    }

    public ResearcherBuilder status(int status) {
        this.status = status;
        return this;
    }

    public String getTag() {
        return tag;
    }

    public ResearcherBuilder tag(String tag) {
        this.tag = tag;
        return this;
    }

    public String getKey() {
        return key;
    }

    public ResearcherBuilder key(String key) {
        this.key = key;
        return this;
    }

    @Override
    public Researcher build() {
        Researcher researcher = new Researcher();
        researcher.setId(id);
        researcher.setReference(reference);
        researcher.setLabel(label);
        researcher.setName(name);
        researcher.setStatus(status);
        researcher.setTag(tag);
        researcher.setKey(key);
        return researcher;
    }
}
