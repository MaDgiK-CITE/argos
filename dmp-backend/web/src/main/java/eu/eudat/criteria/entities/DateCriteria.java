package eu.eudat.criteria.entities;

import java.util.Date;

enum DateCriteriaType implements CriteriaType {
	EQUALS,
	NOT_EQUALS,
	BEFORE,
	BETWEEN,
	AFTER
}

public class DateCriteria extends Criteria<Date> {
	private Date values;
	private CriteriaType type;

	public Date getValues() {
		return values;
	}

	public void setValues(Date values) {
		this.values = values;
	}

	public CriteriaType getType() {
		return type;
	}

	public void setType(DateCriteriaType type) {
		this.type = type;
	}
}
