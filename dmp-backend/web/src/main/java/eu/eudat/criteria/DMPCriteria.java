package eu.eudat.criteria;

import eu.eudat.criteria.entities.Criteria;
import eu.eudat.criteria.entities.DateCriteria;
import eu.eudat.logic.services.operations.DatabaseRepository;
import eu.eudat.query.DMPQuery;

import java.util.*;

public class DMPCriteria {
	private Criteria<UUID> id;
	private Criteria<UUID> groupId;
	private Criteria<String> label;
	private Criteria<Integer> version;
	private GrantCriteria grant;
	private DateCriteria created;
	private DateCriteria modified;
	private UserCriteria creator;
	private DatasetCriteria dataset;

	public Criteria<UUID> getId() {
		return id;
	}

	public void setId(Criteria<UUID> id) {
		this.id = id;
	}

	public void setId(String id) {
		Criteria<UUID> criteria = new Criteria<>();
		criteria.setAs(id);
		this.id = criteria;
	}

	public Criteria<UUID> getGroupId() {
		return groupId;
	}

	public void setGroupId(Criteria<UUID> groupId) {
		this.groupId = groupId;
	}

	public void setGroupId(String groupId) {
		Criteria<UUID> criteria = new Criteria<>();
		criteria.setAs(groupId);
		this.groupId = criteria;
	}

	public Criteria<String> getLabel() {
		return label;
	}

	public void setLabel(Criteria<String> label) {
		this.label = label;
	}

	public void setLabel(String label) {
		Criteria<String> criteria = new Criteria<>();
		criteria.setAs(label);
		this.label = criteria;
	}

	public Criteria<Integer> getVersion() {
		return version;
	}

	public void setVersion(Criteria<Integer> version) {
		this.version = version;
	}

	public void setVersion(String version) {
		Criteria<Integer> criteria = new Criteria<>();
		criteria.setAs(version);
		this.version = criteria;
	}

	public GrantCriteria getGrant() {
		return grant;
	}

	public void setGrant(GrantCriteria grant) {
		this.grant = grant;
	}

	public DateCriteria getCreated() {
		return created;
	}

	public void setCreated(DateCriteria created) {
		this.created = created;
	}

	public void setCreated(String created) {
		DateCriteria criteria = new DateCriteria();
		criteria.setAs(created);
		this.created = criteria;
	}

	public DateCriteria getModified() {
		return modified;
	}

	public void setModified(DateCriteria modified) {
		this.modified = modified;
	}

	public void setModified(String modified) {
		DateCriteria criteria = new DateCriteria();
		criteria.setAs(modified);
		this.modified = criteria;
	}

	public UserCriteria getCreator() {
		return creator;
	}

	public void setCreator(UserCriteria creator) {
		this.creator = creator;
	}

	public DatasetCriteria getDataset() {
		return dataset;
	}

	public void setDataset(DatasetCriteria dataset) {
		this.dataset = dataset;
	}

	protected List<String> buildFields(String path) {
		Set<String> fields = new LinkedHashSet<>();
		path = path != null && !path.isEmpty() ? path + "." : "";
		if (this.id != null) fields.add(path + this.id.getAs());
		if (this.label != null) fields.add(path + this.label.getAs());
		if (this.grant != null) fields.addAll(this.grant.buildFields(path + "grant"));
		if (this.creator != null) fields.addAll(this.creator.buildFields(path + "creator"));
		if (this.dataset != null) fields.addAll(this.dataset.buildFields(path + "dataset"));
		if (!fields.contains(path + "id")) fields.add(path + "id");
		return new LinkedList<>(fields);
	}

	public DMPQuery buildQuery(DatabaseRepository dao) {
		List<String> fields = this.buildFields("");
		DMPQuery dmpQuery = new DMPQuery(dao.getDmpDao(), fields);
		if (this.id != null) dmpQuery.setId(this.id.getValue());
		if (this.grant != null) dmpQuery.setGrantQuery(this.grant.buildQuery(dao));
		if (this.creator != null) dmpQuery.setUserQuery(this.creator.buildQuery(dao));
		if (this.dataset != null) dmpQuery.setDatasetQuery(this.dataset.buildQuery(dao));
		return dmpQuery;
	}
}
