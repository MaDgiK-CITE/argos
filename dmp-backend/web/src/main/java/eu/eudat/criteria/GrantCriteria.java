package eu.eudat.criteria;

import eu.eudat.criteria.entities.Criteria;
import eu.eudat.criteria.entities.DateCriteria;
import eu.eudat.logic.services.operations.DatabaseRepository;
import eu.eudat.query.GrantQuery;
import java.util.*;

public class GrantCriteria {
	private Criteria<UUID> id;
	private List<UUID> ids;
	private Criteria<String> label;
	private List<Integer> statuses;
	private DateCriteria created;
	private DateCriteria modified;
	private UserCriteria creator;

	public Criteria<UUID> getId() {
		return id;
	}

	public void setId(Criteria<UUID> id) {
		this.id = id;
	}

	public List<UUID> getIds() {
		return ids;
	}

	public void setIds(List<UUID> ids) {
		this.ids = ids;
	}

	public Criteria<String> getLabel() {
		return label;
	}

	public void setLabel(Criteria<String> label) {
		this.label = label;
	}

	public void setLabel(String label) {
		Criteria<String> criteria = new Criteria<>();
		criteria.setAs(label);
		this.label = criteria;
	}

	public List<Integer> getStatuses() {
		return statuses;
	}

	public void setStatuses(List<Integer> statuses) {
		this.statuses = statuses;
	}

	public DateCriteria getCreated() {
		return created;
	}

	public void setCreated(DateCriteria created) {
		this.created = created;
	}

	public void setCreated(String created) {
		DateCriteria criteria = new DateCriteria();
		criteria.setAs(created);
		this.created = criteria;
	}

	public DateCriteria getModified() {
		return modified;
	}

	public void setModified(DateCriteria modified) {
		this.modified = modified;
	}

	public void setModified(String modified) {
		DateCriteria criteria = new DateCriteria();
		criteria.setAs(modified);
		this.modified = criteria;
	}

	public UserCriteria getCreator() {
		return creator;
	}

	public void setCreator(UserCriteria creator) {
		this.creator = creator;
	}

	protected List<String> buildFields(String path) {
		Set<String> fields = new LinkedHashSet<>();
		path = path != null && !path.isEmpty() ? path + "." : "";
		if (this.id != null) fields.add(path + this.id.getAs());
		if (this.label != null) fields.add(path + this.label.getAs());
		if (!fields.contains(path + "id")) fields.add(path + "id");
		if (this.creator != null) fields.addAll(this.creator.buildFields(path + "creationUser"));
		return new LinkedList<>(fields);
	}

	public GrantQuery buildQuery(DatabaseRepository dao) {
		List<String> fields = this.buildFields("");
		GrantQuery query = new GrantQuery(dao.getGrantDao(), fields);
		query.setId(this.id.getValue());
		if (this.creator != null) query.setUserQuery(this.creator.buildQuery(dao));
		return query;
	}
}
