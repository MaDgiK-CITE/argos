package eu.eudat.criteria.entities;

enum BaseCriteriaType implements CriteriaType {
	EQUALS,
	NOT_EQUALS
}

public class Criteria<T> {
	private String as;
	private BaseCriteriaType type;
	private T value;

	public String getAs() {
		return as;
	}

	public void setAs(String as) {
		this.as = as;
	}

	public CriteriaType getType() {
		return type;
	}

	public void setType(BaseCriteriaType type) {
		this.type = type;
	}

	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}
}
