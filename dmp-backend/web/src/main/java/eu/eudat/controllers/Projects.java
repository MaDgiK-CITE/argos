package eu.eudat.controllers;

import eu.eudat.data.query.items.item.project.ProjectCriteriaRequest;
import eu.eudat.logic.managers.ProjectManager;
import eu.eudat.logic.proxy.config.exceptions.HugeResultSet;
import eu.eudat.logic.proxy.config.exceptions.NoURLFound;
import eu.eudat.logic.services.ApiContext;
import eu.eudat.models.data.helpers.responses.ResponseItem;
import eu.eudat.models.data.project.Project;
import eu.eudat.models.data.security.Principal;
import eu.eudat.types.ApiMessageCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = {"/api/projects/"})
public class Projects extends BaseController {
	private ProjectManager projectManager;

	@Autowired
	public Projects(ApiContext apiContext, ProjectManager projectManager) {
		super(apiContext);
		this.projectManager = projectManager;
	}

	@RequestMapping(method = RequestMethod.POST, value = {"/external"}, consumes = "application/json", produces = "application/json")
	public @ResponseBody
	ResponseEntity<ResponseItem<List<Project>>> getWithExternal(@RequestBody ProjectCriteriaRequest projectCriteria, Principal principal) throws NoURLFound, InstantiationException, HugeResultSet, IllegalAccessException {
		List<Project> dataTable = this.projectManager.getCriteriaWithExternal(projectCriteria, principal);
		return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<List<eu.eudat.models.data.project.Project>>().payload(dataTable).status(ApiMessageCode.NO_MESSAGE));
	}
}
