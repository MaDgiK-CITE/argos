package eu.eudat.controllers;

import eu.eudat.logic.services.ApiContext;
import eu.eudat.models.validators.*;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;


public abstract class BaseController {

    private ApiContext apiContext;

    public ApiContext getApiContext() {
        return apiContext;
    }

    public BaseController(ApiContext apiContext) {

        this.apiContext = apiContext;
    }

    @InitBinder()
    protected void initBinder(WebDataBinder binder) {
        if (binder.getTarget() != null && DataManagementPlanTableRequestValidator.supportsType((binder.getTarget().getClass())))
            binder.addValidators(this.apiContext.getOperationsContext().getApplicationContext().getBean("dataManagementPlanTableRequestValidator", DataManagementPlanTableRequestValidator.class));
        if (binder.getTarget() != null && GrantTableRequestValidator.supportsType((binder.getTarget().getClass())))
            binder.addValidators(this.apiContext.getOperationsContext().getApplicationContext().getBean("grantTableRequestValidator", GrantTableRequestValidator.class));
        if (binder.getTarget() != null && DatasetProfileValidator.supportsType((binder.getTarget().getClass())))
            binder.addValidators(this.apiContext.getOperationsContext().getApplicationContext().getBean("datasetProfileValidator", DatasetProfileValidator.class));
        if (binder.getTarget() != null && GrantModelValidator.supportsType((binder.getTarget().getClass())))
            binder.addValidators(this.apiContext.getOperationsContext().getApplicationContext().getBean("grantModelValidator", GrantModelValidator.class));
        if (binder.getTarget() != null && DataManagementPlanNewVersionValidator.supportsType((binder.getTarget().getClass())))
            binder.addValidators(this.apiContext.getOperationsContext().getApplicationContext().getBean("dataManagementPlanNewVersionValidator", DataManagementPlanNewVersionValidator.class));
    }
}
