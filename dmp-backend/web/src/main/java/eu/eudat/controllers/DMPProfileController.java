package eu.eudat.controllers;

import eu.eudat.data.dao.criteria.RequestItem;
import eu.eudat.data.entities.DMPProfile;
import eu.eudat.data.entities.DescriptionTemplate;
import eu.eudat.data.query.items.dmpblueprint.DataManagementPlanBlueprintTableRequest;
import eu.eudat.data.query.items.table.dmpprofile.DataManagementPlanProfileTableRequest;
import eu.eudat.exceptions.dmpblueprint.DmpBlueprintUsedException;
import eu.eudat.logic.managers.DataManagementProfileManager;
import eu.eudat.logic.security.claims.ClaimedAuthorities;
import eu.eudat.logic.services.ApiContext;
import eu.eudat.models.data.helpermodels.Tuple;
import eu.eudat.models.data.helpers.common.AutoCompleteLookupItem;
import eu.eudat.models.data.helpers.common.DataTableData;
import eu.eudat.models.data.helpers.responses.ResponseItem;
import eu.eudat.models.data.listingmodels.DataManagementPlanBlueprintListingModel;
import eu.eudat.models.data.listingmodels.DataManagementPlanProfileListingModel;
import eu.eudat.models.data.security.Principal;
import eu.eudat.types.ApiMessageCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.util.List;

import static eu.eudat.types.Authorities.ADMIN;
import static eu.eudat.types.Authorities.DATASET_PROFILE_MANAGER;

/**
 * Created by ikalyvas on 3/21/2018.
 */
@RestController
@CrossOrigin
@RequestMapping(value = {"/api/dmpprofile"})
public class DMPProfileController extends BaseController {

    private DataManagementProfileManager dataManagementProfileManager;

    @Autowired
    public DMPProfileController(ApiContext apiContext, DataManagementProfileManager dataManagementProfileManager) {
        super(apiContext);
        this.dataManagementProfileManager = dataManagementProfileManager;
    }

    @Transactional
    @RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public @ResponseBody
    ResponseEntity<ResponseItem<DMPProfile>> createOrUpdate(@RequestBody DataManagementPlanProfileListingModel dataManagementPlan, @ClaimedAuthorities(claims = {ADMIN}) Principal principal) throws Exception {
        this.dataManagementProfileManager.createOrUpdate(dataManagementPlan, principal);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<DMPProfile>().status(ApiMessageCode.SUCCESS_MESSAGE).message("Created"));
    }

    @Transactional
    @RequestMapping(method = RequestMethod.POST, value = {"/blueprint"}, consumes = "application/json", produces = "application/json")
    public @ResponseBody
    ResponseEntity<ResponseItem<DMPProfile>> createOrUpdateBlueprint(@RequestBody DataManagementPlanBlueprintListingModel dataManagementPlan, @ClaimedAuthorities(claims = {ADMIN}) Principal principal) throws Exception {
        this.dataManagementProfileManager.createOrUpdateBlueprint(dataManagementPlan, principal);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<DMPProfile>().status(ApiMessageCode.SUCCESS_MESSAGE).message("Created"));
    }

    @RequestMapping(method = RequestMethod.GET, value = {"/getSingle/{id}"}, produces = "application/json")
    public @ResponseBody
    ResponseEntity<ResponseItem<DataManagementPlanProfileListingModel>> getSingle(@PathVariable String id, Principal principal) throws IllegalAccessException, InstantiationException {
        DataManagementPlanProfileListingModel dataManagementPlanProfileListingModel = this.dataManagementProfileManager.getSingle(id, principal);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<DataManagementPlanProfileListingModel>().status(ApiMessageCode.NO_MESSAGE).payload(dataManagementPlanProfileListingModel));
    }

    @RequestMapping(method = RequestMethod.GET, value = {"/getSingleBlueprint/{id}"}, produces = "application/json")
    public @ResponseBody
    ResponseEntity<ResponseItem<DataManagementPlanBlueprintListingModel>> getSingleBlueprint(@PathVariable String id, Principal principal) {
        DataManagementPlanBlueprintListingModel dataManagementPlanBlueprintListingModel = this.dataManagementProfileManager.getSingleBlueprint(id, principal);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<DataManagementPlanBlueprintListingModel>().status(ApiMessageCode.NO_MESSAGE).payload(dataManagementPlanBlueprintListingModel));
    }

    @RequestMapping(method = RequestMethod.POST, value = {"/getPaged"}, consumes = "application/json", produces = "application/json")
    public @ResponseBody
    ResponseEntity<ResponseItem<DataTableData<DataManagementPlanProfileListingModel>>> getPaged(@Valid @RequestBody DataManagementPlanProfileTableRequest dataManagementPlanProfileTableRequest, Principal principal) throws Exception {
        DataTableData<DataManagementPlanProfileListingModel> dataTable = this.dataManagementProfileManager.getPaged(dataManagementPlanProfileTableRequest, principal);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<DataTableData<DataManagementPlanProfileListingModel>>().status(ApiMessageCode.NO_MESSAGE).payload(dataTable));
    }

    @RequestMapping(method = RequestMethod.POST, value = {"/getPagedBlueprint"}, consumes = "application/json", produces = "application/json")
    public @ResponseBody
    ResponseEntity<ResponseItem<DataTableData<DataManagementPlanBlueprintListingModel>>> getPagedBlueprint(@Valid @RequestBody DataManagementPlanBlueprintTableRequest dataManagementPlanBlueprintTableRequest, Principal principal) throws Exception {
        DataTableData<DataManagementPlanBlueprintListingModel> dataTable = this.dataManagementProfileManager.getPagedBlueprint(dataManagementPlanBlueprintTableRequest, principal);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<DataTableData<DataManagementPlanBlueprintListingModel>>().status(ApiMessageCode.NO_MESSAGE).payload(dataTable));
    }

    @Transactional
    @RequestMapping(method = RequestMethod.POST, value = {"/clone/{id}"}, consumes = "application/json", produces = "application/json")
    public ResponseEntity<ResponseItem<DataManagementPlanBlueprintListingModel>> clone(@PathVariable String id, @ClaimedAuthorities(claims = {ADMIN}) Principal principal) {
        DataManagementPlanBlueprintListingModel dmpBlueprint = this.dataManagementProfileManager.getSingleBlueprint(id, principal);
        dmpBlueprint.setLabel(dmpBlueprint.getLabel() + " new ");
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<DataManagementPlanBlueprintListingModel>().payload(dmpBlueprint));
    }

    @Transactional
    @RequestMapping(method = RequestMethod.DELETE, value = {"{id}"}, consumes = "application/json", produces = "application/json")
    public @ResponseBody
    ResponseEntity<ResponseItem<Void>> inactivate(@PathVariable String id, @ClaimedAuthorities(claims = {ADMIN}) Principal principal) {
        try {
            this.dataManagementProfileManager.inactivate(id);
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<Void>().status(ApiMessageCode.SUCCESS_MESSAGE));
        } catch (DmpBlueprintUsedException exception) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseItem<Void>().status(ApiMessageCode.UNSUCCESS_DELETE).message(exception.getMessage()));
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = {"/getXml/{id}"}, produces = "application/json")
    public @ResponseBody
    ResponseEntity  getXml( @RequestHeader("Content-Type") String contentType, @PathVariable String id, Principal principal) throws IOException {
        if (contentType.equals("application/xml")) {
            DataManagementPlanBlueprintListingModel dataManagementPlanBlueprintListingModel = this.dataManagementProfileManager.getSingleBlueprint(id, principal);
            return this.dataManagementProfileManager.getDocument(dataManagementPlanBlueprintListingModel);
        }else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseItem<DataManagementPlanBlueprintListingModel>().status(ApiMessageCode.ERROR_MESSAGE).message("NOT AUTHORIZE"));
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = {"/upload"})
    public ResponseEntity<Object> setDatasetProfileXml(@RequestParam("file") MultipartFile file,
                                                       @ClaimedAuthorities(claims = {ADMIN}) Principal principal) throws Exception {
        eu.eudat.logic.utilities.documents.xml.dmpXml.dmpBlueprintModel.DmpBlueprint dmpBlueprintModel = this.dataManagementProfileManager.createDmpProfileFromXml(file);
        DataManagementPlanBlueprintListingModel dmpBlueprint = dmpBlueprintModel.toDmpProfileCompositeModel(file.getOriginalFilename());
        this.dataManagementProfileManager.createOrUpdateBlueprint(dmpBlueprint, principal);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<List<DescriptionTemplate>>()
                .status(ApiMessageCode.SUCCESS_MESSAGE).message(""));
    }

    @RequestMapping(method = RequestMethod.POST, value = {"/search/autocomplete"})
    public ResponseEntity<Object> getExternalAutocomplete(@RequestBody RequestItem<AutoCompleteLookupItem> lookupItem) throws XPathExpressionException {
        List<Tuple<String, String>> items = this.dataManagementProfileManager.getExternalAutocomplete(lookupItem);
        return ResponseEntity.status(HttpStatus.OK).body(items);
    }
}
