package eu.eudat.controllers;


import eu.eudat.data.entities.DescriptionTemplate;
import eu.eudat.data.entities.Funder;
import eu.eudat.data.entities.Project;
import eu.eudat.logic.managers.DatasetManager;
import eu.eudat.logic.managers.QuickWizardManager;
import eu.eudat.logic.services.ApiContext;
import eu.eudat.models.data.datasetwizard.DatasetWizardModel;
import eu.eudat.models.data.dmp.DataManagementPlan;
import eu.eudat.models.data.helpers.responses.ResponseItem;
import eu.eudat.models.data.quickwizard.DatasetCreateWizardModel;
import eu.eudat.models.data.quickwizard.DatasetDescriptionQuickWizardModel;
import eu.eudat.models.data.quickwizard.QuickWizardModel;
import eu.eudat.models.data.security.Principal;
import eu.eudat.types.ApiMessageCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import javax.validation.Valid;

@RestController
@CrossOrigin
@RequestMapping(value = {"/api/quick-wizard/"})
public class QuickWizardController extends BaseController {

    private QuickWizardManager quickWizardManager;
    private DatasetManager datasetManager;

    @Autowired
    public QuickWizardController(ApiContext apiContext, QuickWizardManager quickWizardManager, DatasetManager datasetManager) {
        super(apiContext);
        this.quickWizardManager = quickWizardManager;
        this.datasetManager = datasetManager;
    }

    @Transactional
    @RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public @ResponseBody
    ResponseEntity<ResponseItem<QuickWizardModel>> addQuickWizardModel(@Valid @RequestBody QuickWizardModel quickWizard, Principal principal) throws Exception {

        Funder funderEntity;
        //Create Funder
        if (quickWizard.getFunder() == null) {
            funderEntity = null;
        } else if (quickWizard.getFunder().getExistFunder() == null && quickWizard.getFunder().getLabel() == null) {
            funderEntity = null;
        } else if (quickWizard.getFunder().getExistFunder() == null && quickWizard.getFunder().getLabel() != null) {
            funderEntity = this.quickWizardManager.createOrUpdate(quickWizard.getFunder().toDataFunder(), principal);
        } else {
            funderEntity = quickWizard.getFunder().getExistFunder().toDataModel();
        }

        eu.eudat.data.entities.Grant grantEntity;
        //Create Grant
        if (quickWizard.getGrant() == null) {
            grantEntity = null;
        } else if (quickWizard.getGrant().getExistGrant() == null && quickWizard.getGrant().getLabel() == null) {
            grantEntity = null;
        } else if (quickWizard.getGrant().getExistGrant() == null) {
            grantEntity = this.quickWizardManager.createOrUpdate(quickWizard.getGrant().toDataGrant(), principal);
        } else {
            grantEntity = quickWizard.getGrant().getExistGrant().toDataModel();
        }

        Project projectEntity;
        //Create Project
        if (quickWizard.getProject().getExistProject() == null
                && quickWizard.getProject().getLabel() == null) {
            projectEntity = null;
        } else if (quickWizard.getProject().getExistProject() == null && quickWizard.getProject().getLabel() != null) {
            projectEntity = this.quickWizardManager.createOrUpdate(quickWizard.getProject().toDataProject(), principal);
        } else {
            projectEntity = quickWizard.getProject().getExistProject().toDataModel();
        }

        //Create Dmp
        DataManagementPlan dataManagementPlan = quickWizard.getDmp().toDataDmp(grantEntity, projectEntity, principal);
        eu.eudat.data.entities.DMP dmpEntity = this.quickWizardManager.createOrUpdate(dataManagementPlan, funderEntity, principal);

        //Create Datasets
        quickWizard.getDmp().setId(dmpEntity.getId());
        for (DatasetDescriptionQuickWizardModel dataset : quickWizard.getDatasets().getDatasetsList()) {
            DataManagementPlan dmp = quickWizard.getDmp().toDataDmp(grantEntity, projectEntity, principal);
            DescriptionTemplate profile = quickWizard.getDmp().getDatasetProfile();
            DatasetWizardModel datasetWizardModel = dataset.toDataModel(dmp, profile);
            this.datasetManager.createOrUpdate(datasetWizardModel, principal);
        }

        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<QuickWizardModel>().status(ApiMessageCode.SUCCESS_MESSAGE).message("Created"));
    }

    @RequestMapping(method = RequestMethod.POST, value = {"/datasetcreate"}, consumes = "application/json", produces = "application/json")
    public @ResponseBody
    ResponseEntity<ResponseItem<DatasetCreateWizardModel>> addDatasetWizard(@RequestBody DatasetCreateWizardModel datasetCreateWizardModel, Principal principal) throws Exception{
        for(DatasetDescriptionQuickWizardModel dataset : datasetCreateWizardModel.getDatasets().getDatasetsList()){
            DescriptionTemplate profile = new DescriptionTemplate();
            profile.setId(datasetCreateWizardModel.getDmpMeta().getDatasetProfile().getId());
            profile.setLabel(datasetCreateWizardModel.getDmpMeta().getDatasetProfile().getLabel());
            this.datasetManager.createOrUpdate(dataset.toDataModel(datasetCreateWizardModel.getDmpMeta().getDmp(), profile), principal);
        }

        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<DatasetCreateWizardModel>().status(ApiMessageCode.SUCCESS_MESSAGE).message("Dataset added!"));
    }
}
