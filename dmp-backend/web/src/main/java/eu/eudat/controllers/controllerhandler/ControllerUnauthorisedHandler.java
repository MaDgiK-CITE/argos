package eu.eudat.controllers.controllerhandler;

import eu.eudat.exceptions.security.UnauthorisedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.annotation.Priority;

/**
 * Created by ikalyvas on 6/12/2018.
 */
@ControllerAdvice
@Priority(4)
public class ControllerUnauthorisedHandler {
    private static final Logger logger = LoggerFactory.getLogger(ControllerUnauthorisedHandler.class);

    @Autowired
    public ControllerUnauthorisedHandler() {
    }

    @ExceptionHandler(UnauthorisedException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public void processValidationError(UnauthorisedException ex) {
        logger.error(ex.getMessage(), ex);
        return;
    }
}
