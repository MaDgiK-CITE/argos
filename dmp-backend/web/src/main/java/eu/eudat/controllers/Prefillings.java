package eu.eudat.controllers;

import eu.eudat.logic.managers.PrefillingManager;
import eu.eudat.models.data.datasetwizard.DatasetWizardModel;
import eu.eudat.models.data.helpers.responses.ResponseItem;
import eu.eudat.models.data.prefilling.Prefilling;
import eu.eudat.types.ApiMessageCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = {"/api"})
public class Prefillings {

    private final PrefillingManager prefillingManager;

    @Autowired
    public Prefillings(PrefillingManager prefillingManager) {
        this.prefillingManager = prefillingManager;
    }

    @RequestMapping(method = RequestMethod.GET, value = {"/prefilling/list"}, produces = "application/json")
    public ResponseEntity<ResponseItem<List<Prefilling>>> getPrefillingList(@RequestParam String like) {
        List<Prefilling> prefillingList = prefillingManager.getPrefillings(like);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<List<Prefilling>>().payload(prefillingList).status(ApiMessageCode.NO_MESSAGE));
    }

    @RequestMapping(method = RequestMethod.GET, value = {"/prefilling/generate/{id}"}, produces = "application/json")
    public ResponseEntity<ResponseItem<DatasetWizardModel>> getPrefillingDataset(@PathVariable String id, @RequestParam String configId, @RequestParam UUID profileId) throws Exception {
        DatasetWizardModel datasetWizardModel = prefillingManager.getPrefilledDataset(id, configId, profileId);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<DatasetWizardModel>().payload(datasetWizardModel).status(ApiMessageCode.NO_MESSAGE));
    }

    @RequestMapping(method = RequestMethod.POST, value = {"/prefilling/generateUsingData"}, produces = "application/json")
    public ResponseEntity<ResponseItem<DatasetWizardModel>> getPrefillingDataset(@RequestBody Map<String, Object> data, @RequestParam String configId, @RequestParam UUID profileId) throws Exception {
        DatasetWizardModel datasetWizardModel = prefillingManager.getPrefilledDatasetUsingData(data, configId, profileId);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<DatasetWizardModel>().payload(datasetWizardModel).status(ApiMessageCode.NO_MESSAGE));
    }
}
