package eu.eudat.controllers;

import eu.eudat.elastic.criteria.DatasetCriteria;
import eu.eudat.elastic.entities.Dataset;
import eu.eudat.elastic.entities.Tag;
import eu.eudat.logic.proxy.config.exceptions.HugeResultSet;
import eu.eudat.logic.proxy.config.exceptions.NoURLFound;
import eu.eudat.logic.services.ApiContext;
import eu.eudat.logic.utilities.helpers.StreamDistinctBy;
import eu.eudat.models.data.helpers.responses.ResponseItem;
import eu.eudat.types.ApiMessageCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

/**
 * Created by ikalyvas on 7/5/2018.
 */
@RestController
@CrossOrigin
@RequestMapping(value = {"/api"})
public class TagController extends BaseController {

//    private Repository<Dataset, TagCriteria> datasetRepository;
    private Environment environment;

    @Autowired
    public TagController(ApiContext apiContext, /*Repository tagRepository, */Environment environment) {
        super(apiContext);
//        this.datasetRepository = tagRepository;
        this.environment = environment;
    }

    @RequestMapping(method = RequestMethod.GET, value = {"/external/tags"}, produces = "application/json")
    public @ResponseBody
    ResponseEntity<ResponseItem<List<Tag>>> listExternalTagModel(
            @RequestParam(value = "query", required = false) String query, @RequestParam(value = "type", required = false) String type) throws HugeResultSet, NoURLFound, IOException, ExecutionException, InterruptedException {
        //ExternalUrlCriteria externalUrlCriteria = new ExternalUrlCriteria(query);
        /*List<Map<String, String>> remoteRepos = this.getApiContext().getOperationsContext().getRemoteFetcher().getTags(externalUrlCriteria, type);
        TagExternalSourcesModel researchersExternalSourcesModel = new TagExternalSourcesModel().fromExternalItem(remoteRepos);*/
        if (this.getApiContext().getOperationsContext().getElasticRepository().getDatasetRepository().exists()) {
            DatasetCriteria criteria = new DatasetCriteria();
            criteria.setHasTags(true);
            List<Tag> tags = this.getApiContext().getOperationsContext().getElasticRepository().getDatasetRepository().query(criteria).stream().map(Dataset::getTags).flatMap(Collection::stream).filter(StreamDistinctBy.distinctByKey(Tag::getId)).filter(tag -> tag.getName().toLowerCase().startsWith(query.toLowerCase())).collect(Collectors.toList());

            return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<List<Tag>>().payload(tags).status(ApiMessageCode.NO_MESSAGE));
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ResponseItem<List<Tag>>().status(ApiMessageCode.ERROR_MESSAGE).message("Elastic Services are not available"));
        }
    }
}
