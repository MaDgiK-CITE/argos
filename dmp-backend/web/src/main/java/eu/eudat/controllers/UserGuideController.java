package eu.eudat.controllers;

import eu.eudat.logic.managers.MaterialManager;
import eu.eudat.logic.managers.MetricsManager;
import eu.eudat.logic.security.claims.ClaimedAuthorities;
import eu.eudat.models.data.helpers.responses.ResponseItem;
import eu.eudat.models.data.security.Principal;
import eu.eudat.models.data.userguide.UserGuide;
import eu.eudat.types.ApiMessageCode;
import eu.eudat.types.MetricNames;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static eu.eudat.types.Authorities.ADMIN;

@RestController
@CrossOrigin
@RequestMapping(value = {"/api/userguide/"})
public class UserGuideController {

	private Environment environment;
	private MaterialManager materialManager;

	@Autowired
	public UserGuideController(Environment environment, MaterialManager materialManager, MetricsManager metricsManager) {
		this.environment = environment;
		this.materialManager = materialManager;
	}

	@RequestMapping(path = "{lang}", method = RequestMethod.GET )
	public ResponseEntity<byte[]> getUserGuide(@PathVariable(name = "lang") String lang) throws IOException {
		try (Stream<Path> paths = Files.walk(Paths.get(Objects.requireNonNull(this.environment.getProperty("userguide.path"))))) {
			return this.materialManager.getResponseEntity(lang, paths);
		}
	}

	@RequestMapping(value = "current", method = RequestMethod.POST)
	public @ResponseBody
	ResponseEntity<ResponseItem<String>> updateGuide(@RequestBody UserGuide guide, @ClaimedAuthorities(claims = {ADMIN}) Principal principal) throws Exception {
		String fileName =  this.environment.getProperty("userguide.path") + guide.getName();
		OutputStream os = new FileOutputStream(fileName);
		os.write(guide.getHtml().getBytes());
		os.close();
		return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<String>().status(ApiMessageCode.SUCCESS_MESSAGE).message("Updated").payload("Updated"));
	}
}
