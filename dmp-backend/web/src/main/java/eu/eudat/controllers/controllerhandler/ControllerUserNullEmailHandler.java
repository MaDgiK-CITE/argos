package eu.eudat.controllers.controllerhandler;

import eu.eudat.exceptions.security.NullEmailException;
import eu.eudat.models.data.helpers.responses.ResponseItem;
import eu.eudat.types.ApiMessageCode;
import org.springframework.core.annotation.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Order(2)
public class ControllerUserNullEmailHandler {

	@ExceptionHandler(NullEmailException.class)
	public ResponseEntity nullEmailException(Exception ex) throws Exception {
		return ResponseEntity.status(ApiMessageCode.NULL_EMAIL.getValue()).body("");
	}
}
