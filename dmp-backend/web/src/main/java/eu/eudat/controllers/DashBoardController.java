package eu.eudat.controllers;

import eu.eudat.logic.managers.DashBoardManager;
import eu.eudat.logic.security.claims.ClaimedAuthorities;
import eu.eudat.logic.services.ApiContext;
import eu.eudat.models.data.dashboard.recent.RecentActivity;
import eu.eudat.models.data.dashboard.recent.model.RecentActivityModel;
import eu.eudat.models.data.dashboard.recent.tablerequest.RecentActivityTableRequest;
import eu.eudat.models.data.dashboard.searchbar.SearchBarItem;
import eu.eudat.models.data.dashboard.statistics.DashBoardStatistics;
import eu.eudat.models.data.helpers.responses.ResponseItem;
import eu.eudat.models.data.security.Principal;
import eu.eudat.types.ApiMessageCode;
import eu.eudat.types.Authorities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = {"/api"})
public class DashBoardController extends BaseController {

    private DashBoardManager dashBoardManager;
    @Autowired
    public DashBoardController(ApiContext apiContext, DashBoardManager dashBoardManager) {
        super(apiContext);
        this.dashBoardManager = dashBoardManager;
    }

    @RequestMapping(method = RequestMethod.GET, value = {"/dashboard/getStatistics"}, produces = "application/json")
    public ResponseEntity<ResponseItem<DashBoardStatistics>> getStatistics() {
        DashBoardStatistics statistics = dashBoardManager.getStatistics();
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<DashBoardStatistics>().status(ApiMessageCode.NO_MESSAGE).payload(statistics));
    }

    @RequestMapping(method = RequestMethod.GET, value = {"/dashboard/me/getStatistics"}, produces = "application/json")
    public ResponseEntity<ResponseItem<DashBoardStatistics>> getStatistics(Principal principal) throws IOException {
        DashBoardStatistics statistics = dashBoardManager.getMeStatistics(principal);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<DashBoardStatistics>().status(ApiMessageCode.NO_MESSAGE).payload(statistics));
    }

    @RequestMapping(method = RequestMethod.POST, value = {"/dashboard/recentActivity"}, produces = "application/json")
    @Transactional
    public ResponseEntity<ResponseItem<List<RecentActivityModel>>> getNewRecentActivity(@RequestBody RecentActivityTableRequest tableRequest,
                                                                                        @ClaimedAuthorities(claims = {Authorities.ADMIN, Authorities.MANAGER, Authorities.USER, Authorities.ANONYMOUS}) Principal principal) throws Exception {
        List<RecentActivityModel> statistics = dashBoardManager.getNewRecentActivity(tableRequest, principal);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<List<RecentActivityModel>>().status(ApiMessageCode.NO_MESSAGE).payload(statistics));
    }

    @Deprecated
    @RequestMapping(method = RequestMethod.GET, value = {"/user/recentActivity"}, produces = "application/json")
    public ResponseEntity<ResponseItem<RecentActivity>> getRecentActivity(@RequestParam(name = "numOfActivities", required = false, defaultValue = "5") Integer numberOfActivities, Principal principal) {
        RecentActivity statistics = dashBoardManager.getRecentActivity(principal, numberOfActivities);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<RecentActivity>().status(ApiMessageCode.NO_MESSAGE).payload(statistics));
    }

    @RequestMapping(method = RequestMethod.GET, value = {"/dashboard/search"}, produces = "application/json")
    public ResponseEntity<ResponseItem<List<SearchBarItem>>> search(@RequestParam(name = "like") String like,
                                                                    @ClaimedAuthorities(claims = {Authorities.ADMIN, Authorities.MANAGER, Authorities.USER, Authorities.ANONYMOUS}) Principal principal) {
        List<SearchBarItem> searchBarItemList = dashBoardManager.searchUserData(like, principal);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<List<SearchBarItem>>().status(ApiMessageCode.NO_MESSAGE).payload(searchBarItemList));
    }
}
