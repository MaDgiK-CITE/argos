package eu.eudat.controllers.controllerhandler;


import eu.eudat.logic.security.claims.ClaimedAuthorities;
import eu.eudat.models.data.helpers.responses.ResponseItem;
import eu.eudat.models.data.security.Principal;
import eu.eudat.types.ApiMessageCode;
import eu.eudat.types.Authorities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.annotation.Priority;

/**
 * Created by ikalyvas on 6/12/2018.
 */
@ControllerAdvice
@Priority(5)
public class ControllerErrorHandler {
    private static final Logger logger = LoggerFactory.getLogger(ControllerErrorHandler.class);

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ResponseItem<Exception> processValidationError( Exception ex) throws Exception {
        logger.error(ex.getMessage(), ex);
        return new ResponseItem<Exception>().message(ex.getMessage()).status(ApiMessageCode.DEFAULT_ERROR_MESSAGE);
    }
}
