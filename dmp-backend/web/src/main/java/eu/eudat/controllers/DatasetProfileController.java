package eu.eudat.controllers;

import eu.eudat.data.dao.criteria.RequestItem;
import eu.eudat.data.entities.DescriptionTemplate;
import eu.eudat.logic.managers.AdminManager;
import eu.eudat.logic.managers.DatasetProfileManager;
import eu.eudat.logic.security.claims.ClaimedAuthorities;
import eu.eudat.logic.services.ApiContext;
import eu.eudat.models.data.components.commons.datafield.AutoCompleteData;
import eu.eudat.models.data.externaldataset.ExternalAutocompleteFieldModel;
import eu.eudat.models.data.helpers.common.AutoCompleteLookupItem;
import eu.eudat.models.data.helpers.common.AutoCompleteOptionsLookupItem;
import eu.eudat.models.data.helpers.responses.ResponseItem;
import eu.eudat.models.data.properties.PropertiesModel;
import eu.eudat.models.data.security.Principal;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.xml.xpath.XPathExpressionException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static eu.eudat.types.Authorities.ADMIN;

@RestController
@CrossOrigin
@RequestMapping(value = {"/api"})
public class DatasetProfileController extends BaseController {

    private DatasetProfileManager datasetProfileManager;

    @Autowired
    public DatasetProfileController(ApiContext apiContext, DatasetProfileManager datasetProfileManager) {
        super(apiContext);
        this.datasetProfileManager = datasetProfileManager;
    }

/*    @Transactional
    @RequestMapping(method = RequestMethod.POST, value = {"/datasetprofile/save/{id}"}, consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> updateDataset(@PathVariable String id, @RequestBody PropertiesModel properties) {
        eu.eudat.data.entities.Dataset dataset = this.getApiContext().getOperationsContext().getDatabaseRepository().getDatasetDao().find(UUID.fromString(id));
        Map<String, Object> values = new HashMap<>();
        properties.toMap(values);
        JSONObject jobject = new JSONObject(values);
        dataset.setProperties(jobject.toString());
        dataset.setStatus((short) properties.getStatus());
        this.getApiContext().getOperationsContext().getDatabaseRepository().getDatasetDao().createOrUpdate(dataset);  //TODO
        return ResponseEntity.status(HttpStatus.OK).body(properties);
    }*/

    @Transactional
    @RequestMapping(method = RequestMethod.POST, value = {"/datasetprofile/clone/{id}"}, consumes = "application/json", produces = "application/json")
    public ResponseEntity<ResponseItem<eu.eudat.models.data.admin.composite.DatasetProfile>> clone(@PathVariable String id, @ClaimedAuthorities(claims = {ADMIN})Principal principal) {
        DescriptionTemplate profile = this.datasetProfileManager.clone(id);
        eu.eudat.models.data.admin.composite.DatasetProfile datasetprofile = AdminManager.generateDatasetProfileModel(profile);
        datasetprofile.setLabel(profile.getLabel() + " new ");
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<eu.eudat.models.data.admin.composite.DatasetProfile>().payload(datasetprofile));
    }

    @RequestMapping(method = RequestMethod.POST, value = {"/search/autocomplete"}, consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> getDataForAutocomplete(@RequestBody RequestItem<AutoCompleteLookupItem> lookupItem) throws XPathExpressionException {
        DescriptionTemplate descriptionTemplate = this.getApiContext().getOperationsContext().getDatabaseRepository().getDatasetProfileDao().find(UUID.fromString(lookupItem.getCriteria().getProfileID()));
        eu.eudat.models.data.entities.xmlmodels.datasetprofiledefinition.Field modelfield = this.datasetProfileManager.queryForField(descriptionTemplate.getDefinition(), lookupItem.getCriteria().getFieldID());
        AutoCompleteData data = (AutoCompleteData) modelfield.getData();
        List<ExternalAutocompleteFieldModel> items = this.datasetProfileManager.getAutocomplete(data, lookupItem.getCriteria().getLike());
        return ResponseEntity.status(HttpStatus.OK).body(items);
    }

    @RequestMapping(method = RequestMethod.POST, value = {"/search/autocompleteOptions"}, consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> getDataForAutocompleteOptions(@RequestBody RequestItem<AutoCompleteOptionsLookupItem> lookupItem) {
        AutoCompleteData data = new AutoCompleteData();
        data.setAutoCompleteSingleDataList(lookupItem.getCriteria().getAutoCompleteSingleDataList());
        List<ExternalAutocompleteFieldModel> items = this.datasetProfileManager.getAutocomplete(data, lookupItem.getCriteria().getLike());
        return ResponseEntity.status(HttpStatus.OK).body(items);
    }

}
