package eu.eudat.controllers;

import eu.eudat.data.query.items.item.datasetprofile.DatasetProfileAutocompleteRequest;
import eu.eudat.data.query.items.table.datasetprofile.DatasetProfileTableRequestItem;
import eu.eudat.logic.managers.DatasetProfileManager;
import eu.eudat.logic.services.ApiContext;
import eu.eudat.models.data.datasetprofile.DatasetProfileAutocompleteItem;
import eu.eudat.models.data.datasetprofile.DatasetProfileListingModel;
import eu.eudat.models.data.datasetprofile.DatasetProfileWithPrefillingPropertyModel;
import eu.eudat.models.data.helpers.responses.ResponseItem;
import eu.eudat.types.ApiMessageCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@CrossOrigin
@RequestMapping(value = {"/api"})
public class DatasetProfiles extends BaseController {

    private DatasetProfileManager datasetProfileManager;

    @Autowired
    public DatasetProfiles(ApiContext apiContext, DatasetProfileManager datasetProfileManager) {
        super(apiContext);
        this.datasetProfileManager = datasetProfileManager;
    }

    @RequestMapping(method = RequestMethod.POST, value = {"/dmps/datasetprofiles/get"}, produces = "application/json")
    public @ResponseBody
    ResponseEntity<ResponseItem<List<DatasetProfileAutocompleteItem>>> get(@RequestBody DatasetProfileAutocompleteRequest datasetProfileAutocompleteRequest) throws InstantiationException, IllegalAccessException {
        List<DatasetProfileAutocompleteItem> datasetProfileAutocompleteItems = this.datasetProfileManager.getWithCriteria(datasetProfileAutocompleteRequest);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<List<DatasetProfileAutocompleteItem>>().status(ApiMessageCode.NO_MESSAGE).payload(datasetProfileAutocompleteItems));
    }

    @RequestMapping(method = RequestMethod.POST, value = {"/datasetprofiles/getAll"}, produces = "application/json")
    public @ResponseBody
    ResponseEntity<ResponseItem<List<DatasetProfileListingModel>>> getAll(@RequestBody DatasetProfileTableRequestItem tableRequestItem) throws InstantiationException, IllegalAccessException {
        List<DatasetProfileListingModel> datasetProfileTableData = this.datasetProfileManager.getAll(tableRequestItem);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<List<DatasetProfileListingModel>>().status(ApiMessageCode.NO_MESSAGE).payload(datasetProfileTableData));
    }

    @RequestMapping(method = RequestMethod.POST, value = {"/datasetprofiles/getAllWithPrefilling"}, produces = "application/json")
    public @ResponseBody
    ResponseEntity<ResponseItem<List<DatasetProfileWithPrefillingPropertyModel>>> getAllWithPrefilling(@RequestBody DatasetProfileTableRequestItem tableRequestItem) {
        List<DatasetProfileWithPrefillingPropertyModel> datasetProfileTableData = this.datasetProfileManager.getAllWithPrefilling(tableRequestItem);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<List<DatasetProfileWithPrefillingPropertyModel>>().status(ApiMessageCode.NO_MESSAGE).payload(datasetProfileTableData));
    }
}

