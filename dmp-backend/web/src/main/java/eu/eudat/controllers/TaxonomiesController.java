package eu.eudat.controllers;

import eu.eudat.logic.managers.TaxonomyManager;
import eu.eudat.logic.proxy.config.exceptions.HugeResultSet;
import eu.eudat.logic.proxy.config.exceptions.NoURLFound;
import eu.eudat.logic.services.ApiContext;
import eu.eudat.models.data.helpers.responses.ResponseItem;
import eu.eudat.models.data.security.Principal;
import eu.eudat.models.data.taxonomy.TaxonomyModel;
import eu.eudat.types.ApiMessageCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@CrossOrigin
@RequestMapping(value = {"/api/external/taxonomies"})
public class TaxonomiesController extends BaseController {

    private TaxonomyManager taxonomyManager;

    @Autowired
    public TaxonomiesController(ApiContext apiContext, TaxonomyManager taxonomyManager) {
        super(apiContext);
        this.taxonomyManager = taxonomyManager;
    }

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    ResponseEntity<ResponseItem<List<TaxonomyModel>>> listExternalPublications(
            @RequestParam(value = "query", required = false) String query, @RequestParam(value = "type", required = false) String type, Principal principal
    ) throws HugeResultSet, NoURLFound {
        List<TaxonomyModel> taxonomyModels = this.taxonomyManager.getTaxonomies(query, type);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<List<TaxonomyModel>>().status(ApiMessageCode.NO_MESSAGE).payload(taxonomyModels));
    }
}

