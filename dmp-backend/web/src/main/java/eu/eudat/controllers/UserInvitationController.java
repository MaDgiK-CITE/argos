package eu.eudat.controllers;

import eu.eudat.data.query.items.item.userinfo.UserInfoRequestItem;
import eu.eudat.logic.managers.InvitationsManager;
import eu.eudat.logic.services.ApiContext;
import eu.eudat.models.data.helpers.responses.ResponseItem;
import eu.eudat.models.data.invitation.Invitation;
import eu.eudat.models.data.security.Principal;
import eu.eudat.models.data.userinfo.UserInfoInvitationModel;
import eu.eudat.types.ApiMessageCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.List;
import java.util.UUID;


@RequestMapping("api/invite/")
@RestController
@CrossOrigin
public class UserInvitationController extends BaseController {

    private InvitationsManager invitationsManager;
    @Autowired
    public UserInvitationController(ApiContext apiContext, InvitationsManager invitationsManager) {
        super(apiContext);
        this.invitationsManager = invitationsManager;
    }

    @Transactional
    @RequestMapping(method = RequestMethod.POST, value = {"/users"}, consumes = "application/json", produces = "application/json")
    public @ResponseBody
    ResponseEntity<ResponseItem<Invitation>> users(@RequestBody Invitation invitation, Principal principal) throws Exception {
        this.invitationsManager.inviteUsers(invitation, principal);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<Invitation>().status(ApiMessageCode.SUCCESS_MESSAGE).message("Users have beeen invited"));
    }

    @Transactional
    @RequestMapping(method = RequestMethod.GET, value = {"/exchange/{invitationID}"}, produces = "application/json")
    public @ResponseBody
    ResponseEntity<ResponseItem<UUID>> exchange(@PathVariable UUID invitationID, Principal principal) throws JAXBException, IOException {
        UUID dmpId = invitationsManager.assignUserAcceptedInvitation(invitationID, principal);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<UUID>().status(ApiMessageCode.SUCCESS_MESSAGE).payload(dmpId));
    }

    @RequestMapping(method = RequestMethod.POST, value = {"/getUsers"}, consumes = "application/json", produces = "application/json")
    public @ResponseBody
//    ResponseEntity<ResponseItem<List<UserInfoInvitationModel>>> getUsers(Principal principal) throws IllegalAccessException, InstantiationException {
    ResponseEntity<ResponseItem<List<UserInfoInvitationModel>>> getUsers(Principal principal, @RequestBody UserInfoRequestItem userInfoRequestItem) throws IllegalAccessException, InstantiationException {
//        List<UserInfoInvitationModel> users = invitationsManager.getUsers(principal);
        List<UserInfoInvitationModel> users = invitationsManager.getUsersWithCriteria(principal, userInfoRequestItem);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<List<UserInfoInvitationModel>>().status(ApiMessageCode.SUCCESS_MESSAGE).payload(users));
    }
}
