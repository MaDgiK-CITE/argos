package eu.eudat.controllers;

import eu.eudat.exceptions.datasetprofile.DatasetProfileNewVersionException;
import eu.eudat.logic.managers.DatasetProfileManager;
import eu.eudat.logic.security.claims.ClaimedAuthorities;
import eu.eudat.logic.services.ApiContext;
import eu.eudat.models.data.admin.composite.DatasetProfile;
import eu.eudat.models.data.helpers.responses.ResponseItem;
import eu.eudat.models.data.security.Principal;
import eu.eudat.types.ApiMessageCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;

import static eu.eudat.types.Authorities.ADMIN;

@RestController
@CrossOrigin
@RequestMapping(value = {"/api/management/"})
public class ManagementController extends BaseController {

    private DatasetProfileManager datasetProfileManager;

    @Autowired
    public ManagementController(ApiContext apiContext, DatasetProfileManager datasetProfileManager){
        super(apiContext);
        this.datasetProfileManager = datasetProfileManager;
    }

    @Transactional
    @RequestMapping(method = RequestMethod.POST, value = {"/addSemantics"})
    public ResponseEntity addSemanticsInDatasetProfiles(@ClaimedAuthorities(claims = {ADMIN}) Principal principal) throws Exception {
        try {
            this.datasetProfileManager.addSemanticsInDatasetProfiles();
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<Void>().status(ApiMessageCode.SUCCESS_MESSAGE));
        } catch (Exception exception) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseItem<Void>().status(ApiMessageCode.ERROR_MESSAGE).message(exception.getMessage()));
        }
    }

    @Transactional
    @RequestMapping(method = RequestMethod.POST, value = {"/addRdaInSemantics"})
    public ResponseEntity addRdaInSemanticsInDatasetProfiles(@ClaimedAuthorities(claims = {ADMIN}) Principal principal) throws Exception {
        try {
            this.datasetProfileManager.addRdaInSemanticsInDatasetProfiles();
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<Void>().status(ApiMessageCode.SUCCESS_MESSAGE));
        } catch (Exception exception) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseItem<Void>().status(ApiMessageCode.ERROR_MESSAGE).message(exception.getMessage()));
        }
    }
}
