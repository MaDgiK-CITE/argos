package eu.eudat.controllers;

import eu.eudat.exceptions.descriptiontemplate.DescriptionTemplatesWithTypeException;
import eu.eudat.logic.managers.DescriptionTemplateTypeManager;
import eu.eudat.logic.security.claims.ClaimedAuthorities;
import eu.eudat.logic.services.ApiContext;
import eu.eudat.models.data.descriptiontemplatetype.DescriptionTemplateTypeModel;
import eu.eudat.models.data.helpers.common.DataTableData;
import eu.eudat.models.data.helpers.responses.ResponseItem;
import eu.eudat.models.data.security.Principal;
import eu.eudat.types.ApiMessageCode;
import eu.eudat.types.Authorities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.UUID;

import static eu.eudat.types.Authorities.ADMIN;

@RestController
@CrossOrigin
@RequestMapping(value = {"/api/descriptionTemplateType/"})
public class DescriptionTemplateTypeController extends BaseController {

    private DescriptionTemplateTypeManager descriptionTemplateTypeManager;

    @Autowired
    public DescriptionTemplateTypeController(ApiContext apiContext, DescriptionTemplateTypeManager descriptionTemplateTypeManager){
        super(apiContext);
        this.descriptionTemplateTypeManager = descriptionTemplateTypeManager;
    }

    @Transactional
    @RequestMapping(method = RequestMethod.POST, value = {"create"}, produces = "application/json")
    public @ResponseBody
    ResponseEntity<ResponseItem<DescriptionTemplateTypeModel>> create(@RequestBody DescriptionTemplateTypeModel type, @ClaimedAuthorities(claims = {ADMIN}) Principal principal) throws Exception {
        try {
            this.descriptionTemplateTypeManager.create(type);
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<DescriptionTemplateTypeModel>().status(ApiMessageCode.SUCCESS_MESSAGE).message("Created"));
        }
        catch(DescriptionTemplatesWithTypeException e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseItem<DescriptionTemplateTypeModel>().status(ApiMessageCode.ERROR_MESSAGE).message(e.getMessage()));
        }
    }

    @Transactional
    @RequestMapping(method = RequestMethod.POST, value = {"update"}, produces = "application/json")
    public @ResponseBody
    ResponseEntity<ResponseItem<DescriptionTemplateTypeModel>> update(@RequestBody DescriptionTemplateTypeModel type, @ClaimedAuthorities(claims = {ADMIN}) Principal principal) throws Exception {
        try {
            this.descriptionTemplateTypeManager.update(type);
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<DescriptionTemplateTypeModel>().status(ApiMessageCode.SUCCESS_MESSAGE).message("Updated"));
        }
        catch(DescriptionTemplatesWithTypeException e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseItem<DescriptionTemplateTypeModel>().status(ApiMessageCode.ERROR_MESSAGE).message(e.getMessage()));
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = {"get"}, produces = "application/json")
    public @ResponseBody
    ResponseEntity<ResponseItem<DataTableData<DescriptionTemplateTypeModel>>> get(@ClaimedAuthorities(claims = {Authorities.ADMIN, Authorities.MANAGER, Authorities.USER, Authorities.ANONYMOUS}) Principal principal) throws Exception {
        DataTableData<DescriptionTemplateTypeModel> dataTable = this.descriptionTemplateTypeManager.get();
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<DataTableData<DescriptionTemplateTypeModel>>().status(ApiMessageCode.NO_MESSAGE).payload(dataTable));
    }

    @RequestMapping(method = RequestMethod.GET, value = {"get/{id}"}, produces = "application/json")
    public @ResponseBody
    ResponseEntity<ResponseItem<DescriptionTemplateTypeModel>> getSingle(@PathVariable(value = "id") UUID id, @ClaimedAuthorities(claims = {Authorities.ADMIN, Authorities.MANAGER, Authorities.USER, Authorities.ANONYMOUS}) Principal principal) throws Exception {
        try {
            DescriptionTemplateTypeModel typeModel = this.descriptionTemplateTypeManager.getSingle(id);
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<DescriptionTemplateTypeModel>().status(ApiMessageCode.NO_MESSAGE).payload(typeModel));
        }
        catch(DescriptionTemplatesWithTypeException e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseItem<DescriptionTemplateTypeModel>().status(ApiMessageCode.ERROR_MESSAGE).message(e.getMessage()));
        }
    }

    @Transactional
    @RequestMapping(method = RequestMethod.DELETE, value = {"/delete/{id}"}, produces = "application/json")
    public @ResponseBody
    ResponseEntity<ResponseItem<DescriptionTemplateTypeModel>> delete(@PathVariable(value = "id") UUID id, @ClaimedAuthorities(claims = {ADMIN}) Principal principal) throws Exception {
        try{
            this.descriptionTemplateTypeManager.delete(id);
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<DescriptionTemplateTypeModel>().status(ApiMessageCode.SUCCESS_MESSAGE).message("Deleted"));
        }
        catch(DescriptionTemplatesWithTypeException e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseItem<DescriptionTemplateTypeModel>().status(ApiMessageCode.UNSUCCESS_DELETE).message(e.getMessage()));
        }
    }

}
