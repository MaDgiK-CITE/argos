package eu.eudat.controllers;

import eu.eudat.logic.managers.LocalFetchManager;
import eu.eudat.models.data.helpers.responses.ResponseItem;
import eu.eudat.models.data.local.LocalFetchModel;
import eu.eudat.types.ApiMessageCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "api/currency")
public class CurrencyController {

	private LocalFetchManager localFetchManager;

	@Autowired
	public CurrencyController(LocalFetchManager localFetchManager) {
		this.localFetchManager = localFetchManager;
	}

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<ResponseItem<List<LocalFetchModel>>> getCurrencies( @RequestParam(value = "query", required = false) String query) throws Exception {
		List<LocalFetchModel> currencies = localFetchManager.getCurrency(query);
		return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<List<LocalFetchModel>>().status(ApiMessageCode.NO_MESSAGE).payload(currencies));
	}
}
