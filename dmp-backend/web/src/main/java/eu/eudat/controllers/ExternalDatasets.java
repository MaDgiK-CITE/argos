package eu.eudat.controllers;

import eu.eudat.data.entities.ExternalDataset;
import eu.eudat.data.query.items.table.externaldataset.ExternalDatasetTableRequest;
import eu.eudat.logic.managers.DataRepositoryManager;
import eu.eudat.logic.managers.ExternalDatasetManager;
import eu.eudat.logic.proxy.config.exceptions.HugeResultSet;
import eu.eudat.logic.proxy.config.exceptions.NoURLFound;
import eu.eudat.logic.services.ApiContext;
import eu.eudat.models.data.datarepository.DataRepositoryModel;
import eu.eudat.models.data.externaldataset.ExternalDatasetListingModel;
import eu.eudat.models.data.helpers.common.DataTableData;
import eu.eudat.models.data.helpers.responses.ResponseItem;
import eu.eudat.models.data.security.Principal;
import eu.eudat.types.ApiMessageCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;


@RestController
@CrossOrigin
@RequestMapping(value = {"/api"})
public class ExternalDatasets extends BaseController {

    private ExternalDatasetManager externalDatasetManager;

    @Autowired
    public ExternalDatasets(ApiContext apiContext, ExternalDatasetManager externalDatasetManager) {
        super(apiContext);
        this.externalDatasetManager = externalDatasetManager;
    }

    @RequestMapping(method = RequestMethod.POST, value = {"/externaldatasets/getPaged"}, consumes = "application/json", produces = "application/json")
    public @ResponseBody
    ResponseEntity<ResponseItem<DataTableData<ExternalDatasetListingModel>>> getPaged(@RequestBody ExternalDatasetTableRequest datasetTableRequest, Principal principal) throws Exception {
        DataTableData<ExternalDatasetListingModel> dataTable = externalDatasetManager.getPaged(datasetTableRequest);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<DataTableData<ExternalDatasetListingModel>>().status(ApiMessageCode.NO_MESSAGE).payload(dataTable));
    }

    @RequestMapping(method = RequestMethod.GET, value = {"/external/datasets"}, produces = "application/json")
    public @ResponseBody
    ResponseEntity<ResponseItem<List<ExternalDatasetListingModel>>> getWithExternal(
            @RequestParam(value = "query", required = false) String query, @RequestParam(value = "type", required = false) String type, Principal principal
    ) throws NoURLFound, InstantiationException, HugeResultSet, IllegalAccessException {
        List<ExternalDatasetListingModel> dataTable = externalDatasetManager.getWithExternal(query, type, principal);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<List<ExternalDatasetListingModel>>().payload(dataTable).status(ApiMessageCode.NO_MESSAGE));
    }

    @RequestMapping(method = RequestMethod.POST, value = {"/externaldatasets/getSingle/{id}"}, consumes = "application/json", produces = "application/json")
    public @ResponseBody
    ResponseItem<ExternalDatasetListingModel> getWithExternal(@PathVariable UUID id, Principal principal) throws NoURLFound, InstantiationException, HugeResultSet, IllegalAccessException {
        ExternalDatasetListingModel externalDatasetModel = externalDatasetManager.getSingle(id);
        return new ResponseItem<ExternalDatasetListingModel>().payload(externalDatasetModel).status(ApiMessageCode.NO_MESSAGE);
    }

    @Transactional
    @RequestMapping(method = RequestMethod.POST, value = {"/externaldatasets"}, consumes = "application/json", produces = "application/json")
    public @ResponseBody
    ResponseEntity<ResponseItem<ExternalDatasetListingModel>> create(@RequestBody eu.eudat.models.data.externaldataset.ExternalDatasetModel externalDatasetModel, Principal principal) throws Exception {
        ExternalDataset externalDataset = this.externalDatasetManager.create(externalDatasetModel, principal);
        ExternalDatasetListingModel externalDatasetListingModel = new ExternalDatasetListingModel().fromDataModel(externalDataset);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<ExternalDatasetListingModel>().payload(externalDatasetListingModel).status(ApiMessageCode.SUCCESS_MESSAGE));
    }
}
