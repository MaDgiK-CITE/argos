package eu.eudat.controllers;

import eu.eudat.logic.managers.LicenseManager;
import eu.eudat.logic.proxy.config.exceptions.HugeResultSet;
import eu.eudat.logic.proxy.config.exceptions.NoURLFound;
import eu.eudat.logic.services.ApiContext;
import eu.eudat.models.data.helpers.responses.ResponseItem;
import eu.eudat.models.data.license.LicenseModel;
import eu.eudat.models.data.security.Principal;
import eu.eudat.types.ApiMessageCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@CrossOrigin
@RequestMapping(value = {"/api/external/licenses"})
public class Licenses extends BaseController {

    private LicenseManager licenseManager;

    @Autowired
    public Licenses(ApiContext apiContext, LicenseManager licenseManager) {
        super(apiContext);
        this.licenseManager = licenseManager;
    }

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    ResponseEntity<ResponseItem<List<LicenseModel>>> listExternalLicenses(
            @RequestParam(value = "query", required = false) String query, @RequestParam(value = "type", required = false) String type, Principal principal
    ) throws HugeResultSet, NoURLFound {
        List<LicenseModel> licenseModels = this.licenseManager.getLicenses(query, type);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<List<LicenseModel>>().status(ApiMessageCode.NO_MESSAGE).payload(licenseModels));
    }
}

