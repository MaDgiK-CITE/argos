package eu.eudat.controllers;

import eu.eudat.logic.managers.PublicationManager;
import eu.eudat.logic.proxy.config.exceptions.HugeResultSet;
import eu.eudat.logic.proxy.config.exceptions.NoURLFound;
import eu.eudat.logic.services.ApiContext;
import eu.eudat.models.data.helpers.responses.ResponseItem;
import eu.eudat.models.data.publication.PublicationModel;
import eu.eudat.models.data.security.Principal;
import eu.eudat.types.ApiMessageCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@CrossOrigin
@RequestMapping(value = {"/api/external/publications"})
public class PublicationsController extends BaseController {

    private PublicationManager publicationManager;

    @Autowired
    public PublicationsController(ApiContext apiContext, PublicationManager publicationManager) {
        super(apiContext);
        this.publicationManager = publicationManager;
    }

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    ResponseEntity<ResponseItem<List<PublicationModel>>> listExternalPublications(
            @RequestParam(value = "query", required = false) String query, @RequestParam(value = "type", required = false) String type, Principal principal
    ) throws HugeResultSet, NoURLFound {
        List<PublicationModel> publicationModels = this.publicationManager.getPublications(query, type);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<List<PublicationModel>>().status(ApiMessageCode.NO_MESSAGE).payload(publicationModels));
    }
}

