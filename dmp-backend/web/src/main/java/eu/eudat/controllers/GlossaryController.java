package eu.eudat.controllers;

import eu.eudat.logic.managers.MaterialManager;
import eu.eudat.logic.managers.MetricsManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.stream.Stream;

@RestController
@CrossOrigin
@RequestMapping(value = {"/api/material/glossary/"})
public class GlossaryController {

    private Environment environment;
    private MaterialManager materialManager;

    @Autowired
    public GlossaryController(Environment environment, MaterialManager materialManager, MetricsManager metricsManager) {
        this.environment = environment;
        this.materialManager = materialManager;
    }

    @RequestMapping(path = "{lang}", method = RequestMethod.GET )
    public ResponseEntity<byte[]> getGlossary(@PathVariable(name = "lang") String lang) throws IOException {
        try (Stream<Path> paths = Files.walk(Paths.get(Objects.requireNonNull(this.environment.getProperty("glossary.path"))))) {
            return this.materialManager.getResponseEntity(lang, paths);
        }
    }

}
