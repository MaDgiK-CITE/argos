package eu.eudat.controllers;

import eu.eudat.data.entities.Service;
import eu.eudat.logic.managers.ServiceManager;
import eu.eudat.logic.managers.ValidationManager;
import eu.eudat.logic.proxy.config.exceptions.HugeResultSet;
import eu.eudat.logic.proxy.config.exceptions.NoURLFound;
import eu.eudat.logic.services.ApiContext;
import eu.eudat.models.data.helpers.responses.ResponseItem;
import eu.eudat.models.data.security.Principal;
import eu.eudat.models.data.services.ServiceModel;
import eu.eudat.types.ApiMessageCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@CrossOrigin
@RequestMapping(value = {"/api"})
public class Validation extends BaseController {

	private ValidationManager validationManager;

	@Autowired
	public Validation(ApiContext apiContext, ValidationManager validationManager) {
		super(apiContext);
		this.validationManager = validationManager;
	}

	@RequestMapping(method = RequestMethod.GET, value = {"/external/validation"}, produces = "application/json")
	public @ResponseBody
	ResponseEntity<ResponseItem<Boolean>> validate(
			@RequestParam(value = "query", required = false) String query, @RequestParam(value = "type", required = false) String type, Principal principal
	) throws HugeResultSet, NoURLFound {
		Boolean isValid = this.validationManager.validateIdentifier(query, type, principal);
		return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<Boolean>().payload(isValid).status(ApiMessageCode.NO_MESSAGE));
	}
}

