package eu.eudat.controllers;

import eu.eudat.logic.proxy.config.configloaders.ConfigLoader;
import eu.eudat.logic.security.customproviders.ConfigurableProvider.entities.saml2.Saml2ConfigurableProvider;
import eu.eudat.logic.security.validators.configurableProvider.Saml2SSOUtils;
import eu.eudat.logic.services.ApiContext;
import eu.eudat.models.data.helpers.responses.ResponseItem;
import eu.eudat.models.data.saml2.AuthnRequestModel;
import eu.eudat.types.ApiMessageCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.nio.charset.StandardCharsets;

@RestController
@CrossOrigin
@RequestMapping(value = {"/api/saml2/"})
public class Saml2MetadataController extends BaseController {

    private final ConfigLoader configLoader;

    @Autowired
    public Saml2MetadataController(ApiContext apiContext, ConfigLoader configLoader) {
        super(apiContext);
        this.configLoader = configLoader;
    }

    @RequestMapping(method = RequestMethod.GET, value = {"metadata/{configurableProviderId}"})
    public @ResponseBody
    ResponseEntity getMetadata(@PathVariable String configurableProviderId) {
        Saml2ConfigurableProvider saml2ConfigurableProvider = (Saml2ConfigurableProvider) this.configLoader.getConfigurableProviders().getProviders().stream()
                .filter(prov -> prov.getConfigurableLoginId().equals(configurableProviderId))
                .findFirst().orElse(null);
        if (saml2ConfigurableProvider != null) {
            try {
                String metadataXml = Saml2SSOUtils.getMetadata(saml2ConfigurableProvider);
                HttpHeaders responseHeaders = new HttpHeaders();
                responseHeaders.setContentLength(metadataXml.length());
                responseHeaders.setContentType(MediaType.APPLICATION_OCTET_STREAM);
                responseHeaders.set("Content-Disposition", "attachment;filename=" + configurableProviderId + ".xml");
                responseHeaders.set("Access-Control-Expose-Headers", "Content-Disposition");
                responseHeaders.get("Access-Control-Expose-Headers").add("Content-Type");
                return new ResponseEntity<>(metadataXml.getBytes(StandardCharsets.UTF_8),
                        responseHeaders,
                        HttpStatus.OK);
            } catch (Exception e) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseItem<String>().status(ApiMessageCode.ERROR_MESSAGE).message("Failed to fetch metadata."));
            }
        }
        else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseItem<String>().status(ApiMessageCode.ERROR_MESSAGE).message("Failed to fetch metadata."));
        }
    }
    @RequestMapping(method = RequestMethod.GET, value = {"authnRequest/{configurableProviderId}"})
    public @ResponseBody
    ResponseEntity getAuthnRequest(@PathVariable String configurableProviderId) {
        Saml2ConfigurableProvider saml2ConfigurableProvider = (Saml2ConfigurableProvider) this.configLoader.getConfigurableProviders().getProviders().stream()
                .filter(prov -> prov.getConfigurableLoginId().equals(configurableProviderId))
                .findFirst().orElse(null);
        if (saml2ConfigurableProvider != null) {
            try {
                AuthnRequestModel authnRequest = Saml2SSOUtils.getAuthnRequest(saml2ConfigurableProvider);
                return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<AuthnRequestModel>().status(ApiMessageCode.SUCCESS_MESSAGE).message("Created").payload(authnRequest));
            }
            catch (Exception e) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseItem<String>().status(ApiMessageCode.ERROR_MESSAGE).message("Failed to create authentication request."));
            }

        }
        else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseItem<String>().status(ApiMessageCode.ERROR_MESSAGE).message("Unknown provider."));
        }
    }

}
