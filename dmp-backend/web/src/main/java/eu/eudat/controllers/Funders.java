package eu.eudat.controllers;

import eu.eudat.data.query.items.item.funder.FunderCriteriaRequest;
import eu.eudat.logic.managers.FunderManager;
import eu.eudat.logic.proxy.config.exceptions.HugeResultSet;
import eu.eudat.logic.proxy.config.exceptions.NoURLFound;
import eu.eudat.logic.services.ApiContext;
import eu.eudat.models.data.funder.Funder;
import eu.eudat.models.data.helpers.responses.ResponseItem;
import eu.eudat.models.data.security.Principal;
import eu.eudat.types.ApiMessageCode;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = {"/api/funders/"})
public class Funders extends BaseController {
	private FunderManager funderManager;

	public Funders(ApiContext apiContext, FunderManager funderManager) {
		super(apiContext);
		this.funderManager = funderManager;
	}

	@RequestMapping(method = RequestMethod.POST, value = {"/external"}, consumes = "application/json", produces = "application/json")
	public @ResponseBody
	ResponseEntity<ResponseItem<List<Funder>>> getWithExternal(@RequestBody FunderCriteriaRequest funderCriteria, Principal principal) throws NoURLFound, InstantiationException, HugeResultSet, IllegalAccessException {
		List<Funder> dataTable = this.funderManager.getCriteriaWithExternal(funderCriteria, principal);
		return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<List<eu.eudat.models.data.funder.Funder>>().payload(dataTable).status(ApiMessageCode.NO_MESSAGE));
	}
}
