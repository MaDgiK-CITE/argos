package eu.eudat.controllers;

import eu.eudat.logic.managers.MaterialManager;
import eu.eudat.logic.managers.MetricsManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.stream.Stream;

@RestController
@CrossOrigin
@RequestMapping(value = {"/api/material/termsofservice/"})
public class TermsOfServiceController {

    private Environment environment;
    private MaterialManager materialManager;

    @Autowired
    public TermsOfServiceController(Environment environment, MaterialManager materialManager, MetricsManager metricsManager) {
        this.environment = environment;
        this.materialManager = materialManager;
    }

    @RequestMapping(path = "{lang}", method = RequestMethod.GET )
    public ResponseEntity<byte[]> getTermsOfService(@PathVariable(name = "lang") String lang) throws IOException {
        try (Stream<Path> paths = Files.walk(Paths.get(Objects.requireNonNull(this.environment.getProperty("termsofservice.path"))))) {
            return this.materialManager.getResponseEntity(lang, paths);
        }
    }

}
