package eu.eudat.controllers;

import eu.eudat.data.query.items.table.organisations.OrganisationsTableRequest;
import eu.eudat.logic.managers.OrganisationsManager;
import eu.eudat.logic.proxy.config.exceptions.HugeResultSet;
import eu.eudat.logic.proxy.config.exceptions.NoURLFound;
import eu.eudat.logic.services.ApiContext;
import eu.eudat.models.data.dmp.Organisation;
import eu.eudat.models.data.helpers.common.DataTableData;
import eu.eudat.models.data.helpers.responses.ResponseItem;
import eu.eudat.models.data.security.Principal;
import eu.eudat.types.ApiMessageCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@CrossOrigin
@RequestMapping(value = {"/api"})
public class Organisations extends BaseController {

    private OrganisationsManager organisationsManager;
    private ApiContext apiContext;

    @Autowired
    public Organisations(ApiContext apiContext, OrganisationsManager organisationsManager) {
        super(apiContext);
        this.organisationsManager = organisationsManager;
    }

    @RequestMapping(method = RequestMethod.GET, value = {"/external/organisations"}, produces = "application/json")
    public @ResponseBody
    ResponseEntity<ResponseItem<List<Organisation>>> listExternalOrganisations(
            @RequestParam(value = "query", required = false) String query, @RequestParam(value = "type", required = false) String type
    ) throws HugeResultSet, NoURLFound {
        List<Organisation> organisations = organisationsManager.getCriteriaWithExternal(query, type);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<List<Organisation>>().payload(organisations).status(ApiMessageCode.NO_MESSAGE));
    }

    @RequestMapping(method = RequestMethod.POST, value = {"/general/organisations"}, produces = "application/json")
    public @ResponseBody
    ResponseEntity<ResponseItem<List<Organisation>>> listGeneralOrganisations(@RequestBody OrganisationsTableRequest organisationsTableRequest, Principal principal) throws Exception {
        List<Organisation> organisations = organisationsManager.getWithExternal(organisationsTableRequest, principal);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<List<Organisation>>().payload(organisations).status(ApiMessageCode.NO_MESSAGE));
    }

    @RequestMapping(method = RequestMethod.POST, value = {"/internal/organisations"}, produces = "application/json")
    public @ResponseBody
    ResponseEntity<ResponseItem<DataTableData<Organisation>>> getPaged(@Valid @RequestBody OrganisationsTableRequest organisationsTableRequest, Principal principal) throws Exception{
        DataTableData<Organisation> organisationDataTableData = this.organisationsManager.getPagedOrganisations(organisationsTableRequest, principal);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<DataTableData<Organisation>>().payload(organisationDataTableData).status(ApiMessageCode.NO_MESSAGE));
    }

    @RequestMapping(method = RequestMethod.POST, value = {"/public/organisations"}, produces = "application/json")
    public @ResponseBody
    ResponseEntity<ResponseItem<DataTableData<Organisation>>> getPublicPaged(@Valid @RequestBody OrganisationsTableRequest organisationsTableRequest) throws Exception{
        DataTableData<Organisation> organisationDataTableData = this.organisationsManager.getPublicPagedOrganisations(organisationsTableRequest);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<DataTableData<Organisation>>().payload(organisationDataTableData).status(ApiMessageCode.NO_MESSAGE));
    }
}

