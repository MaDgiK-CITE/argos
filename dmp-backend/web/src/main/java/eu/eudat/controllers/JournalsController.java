package eu.eudat.controllers;

import eu.eudat.data.entities.DataRepository;
import eu.eudat.logic.managers.DataRepositoryManager;
import eu.eudat.logic.proxy.config.exceptions.HugeResultSet;
import eu.eudat.logic.proxy.config.exceptions.NoURLFound;
import eu.eudat.logic.services.ApiContext;
import eu.eudat.models.data.datarepository.DataRepositoryModel;
import eu.eudat.models.data.helpers.responses.ResponseItem;
import eu.eudat.models.data.security.Principal;
import eu.eudat.types.ApiMessageCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@CrossOrigin
@RequestMapping(value = {"/api/external/journals"})
public class JournalsController extends BaseController {

    private DataRepositoryManager dataRepositoryManager;

    @Autowired
    public JournalsController(ApiContext apiContext, DataRepositoryManager dataRepositoryManager) {
        super(apiContext);
        this.dataRepositoryManager = dataRepositoryManager;
    }

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    ResponseEntity<ResponseItem<List<DataRepositoryModel>>> listExternalDataRepositories(
            @RequestParam(value = "query", required = false) String query, @RequestParam(value = "type", required = false) String type, Principal principal
    ) throws HugeResultSet, NoURLFound {
        List<DataRepositoryModel> dataRepositoryModels = this.dataRepositoryManager.getJournals(query, type, principal);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<List<DataRepositoryModel>>().status(ApiMessageCode.NO_MESSAGE).payload(dataRepositoryModels));
    }


}

