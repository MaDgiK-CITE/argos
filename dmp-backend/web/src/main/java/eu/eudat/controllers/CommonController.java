package eu.eudat.controllers;

import eu.eudat.configurations.dynamicgrant.DynamicGrantConfiguration;
import eu.eudat.configurations.dynamicgrant.entities.Language;
import eu.eudat.logic.managers.CommonsManager;
import eu.eudat.logic.proxy.config.configloaders.ConfigLoader;
import eu.eudat.models.data.externalurl.ExternalSourcesConfiguration;
import eu.eudat.models.data.helpers.responses.ResponseItem;
import eu.eudat.types.ApiMessageCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by ikalyvas on 3/28/2018.
 */
@RestController
@CrossOrigin
@RequestMapping(value = {"/api/common"})
public class CommonController {

    private ConfigLoader configLoader;

    @Autowired
    public CommonController(ConfigLoader configLoader) {
        this.configLoader = configLoader;
    }

    @RequestMapping(method = RequestMethod.GET, value = {"/externalSourcesConfiguration"}, produces = "application/json")
    public @ResponseBody
    ResponseEntity<ResponseItem<ExternalSourcesConfiguration>> getExternalSourcesConfiguration() {
        ExternalSourcesConfiguration configuration = CommonsManager.getExternalSourcesConfiguration(configLoader);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<ExternalSourcesConfiguration>().status(ApiMessageCode.NO_MESSAGE).payload(configuration));
    }
}
