package eu.eudat.controllers;

import eu.eudat.data.entities.Grant;
import eu.eudat.data.query.items.item.grant.GrantCriteriaRequest;
import eu.eudat.data.query.items.table.grant.GrantTableRequest;
import eu.eudat.logic.managers.GrantManager;
import eu.eudat.logic.proxy.config.exceptions.HugeResultSet;
import eu.eudat.logic.proxy.config.exceptions.NoURLFound;
import eu.eudat.logic.security.claims.ClaimedAuthorities;
import eu.eudat.logic.services.ApiContext;
import eu.eudat.models.data.helpers.common.DataTableData;
import eu.eudat.models.data.helpers.responses.ResponseItem;
import eu.eudat.models.data.grant.GrantListingModel;
import eu.eudat.models.data.security.Principal;
import eu.eudat.types.ApiMessageCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import static eu.eudat.types.Authorities.ANONYMOUS;


@RestController
@CrossOrigin
@RequestMapping(value = {"/api/grants/"})
public class Grants extends BaseController {
    private GrantManager grantManager;

    @Autowired
    public Grants(ApiContext apiContext, GrantManager grantManager) {
        super(apiContext);
        this.grantManager = grantManager;
    }

    @RequestMapping(method = RequestMethod.POST, value = {"/paged"}, consumes = "application/json", produces = "application/json")
    public @ResponseBody
    ResponseEntity<ResponseItem<DataTableData<GrantListingModel>>> getPaged(@Valid @RequestBody GrantTableRequest grantTableRequest, @RequestParam String fieldsGroup, Principal principal) throws Exception {
        DataTableData<eu.eudat.models.data.grant.GrantListingModel> dataTable = this.grantManager.getPaged(grantTableRequest, principal, fieldsGroup);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<DataTableData<eu.eudat.models.data.grant.GrantListingModel>>().payload(dataTable).status(ApiMessageCode.NO_MESSAGE));
    }

    @RequestMapping(method = RequestMethod.POST, value = {"public/paged"}, consumes = "application/json", produces = "application/json")
    public @ResponseBody
    ResponseEntity<ResponseItem<DataTableData<GrantListingModel>>> getPaged(@Valid @RequestBody GrantTableRequest grantTableRequest) throws Exception {
        DataTableData<eu.eudat.models.data.grant.GrantListingModel> dataTable = this.grantManager.getPublicPaged(grantTableRequest);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<DataTableData<eu.eudat.models.data.grant.GrantListingModel>>().payload(dataTable).status(ApiMessageCode.NO_MESSAGE));
    }

    @RequestMapping(method = RequestMethod.GET, value = {"{id}"}, produces = "application/json")
    public @ResponseBody
    ResponseEntity<ResponseItem<eu.eudat.models.data.grant.Grant>> getSingle(@PathVariable String id, Principal principal) throws IllegalAccessException, InstantiationException {
        eu.eudat.models.data.grant.Grant grant = this.grantManager.getSingle(id);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<eu.eudat.models.data.grant.Grant>().payload(grant).status(ApiMessageCode.NO_MESSAGE));
    }

    /*@Transactional
    @RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public @ResponseBody
    ResponseEntity<ResponseItem<Grant>> addGrant(@Valid @RequestBody eu.eudat.models.data.grant.Grant grant, Principal principal) throws IOException, ParseException {
        this.grantManager.createOrUpdate(grant, principal);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<eu.eudat.data.entities.Grant>().status(ApiMessageCode.SUCCESS_MESSAGE).message("Created"));
    }*/

    /*@Transactional
    @RequestMapping(method = RequestMethod.DELETE, value = {"{id}"}, consumes = "application/json", produces = "application/json")
    public @ResponseBody
    ResponseEntity<ResponseItem<Grant>> inactivate(@PathVariable String id, Principal principal) throws IllegalAccessException, InstantiationException {
        this.grantManager.inactivate(id);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<eu.eudat.data.entities.Grant>().status(ApiMessageCode.SUCCESS_MESSAGE));
    }*/

    @RequestMapping(method = RequestMethod.POST, value = {"/external"}, consumes = "application/json", produces = "application/json")
    public @ResponseBody
    ResponseEntity<ResponseItem<List<eu.eudat.models.data.grant.Grant>>> getWithExternal(@RequestBody GrantCriteriaRequest grantCriteria, Principal principal) throws NoURLFound, InstantiationException, HugeResultSet, IllegalAccessException {
        List<eu.eudat.models.data.grant.Grant> dataTable = this.grantManager.getCriteriaWithExternal(grantCriteria, principal);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<List<eu.eudat.models.data.grant.Grant>>().payload(dataTable).status(ApiMessageCode.NO_MESSAGE));
    }

    @RequestMapping(method = RequestMethod.POST, value = {"get"}, consumes = "application/json", produces = "application/json")
    public @ResponseBody
    ResponseEntity<ResponseItem<List<eu.eudat.models.data.grant.Grant>>> get(@RequestBody GrantCriteriaRequest grantCriteria, @ClaimedAuthorities(claims = {ANONYMOUS}) Principal principal) throws NoURLFound, InstantiationException, HugeResultSet, IllegalAccessException {
        List<eu.eudat.models.data.grant.Grant> dataTable = this.grantManager.getCriteria(grantCriteria);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<List<eu.eudat.models.data.grant.Grant>>().payload(dataTable).status(ApiMessageCode.NO_MESSAGE));
    }

}

