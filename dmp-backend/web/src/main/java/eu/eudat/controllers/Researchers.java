package eu.eudat.controllers;

import eu.eudat.data.query.items.item.researcher.ResearcherCriteriaRequest;
import eu.eudat.logic.managers.ResearcherManager;
import eu.eudat.logic.proxy.config.exceptions.HugeResultSet;
import eu.eudat.logic.proxy.config.exceptions.NoURLFound;
import eu.eudat.logic.services.ApiContext;
import eu.eudat.models.data.dmp.Researcher;
import eu.eudat.models.data.external.ResearchersExternalSourcesModel;
import eu.eudat.models.data.helpers.responses.ResponseItem;
import eu.eudat.models.data.security.Principal;
import eu.eudat.types.ApiMessageCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


@RestController
@CrossOrigin
@RequestMapping(value = {"/api/researchers"})

public class Researchers extends BaseController {

    private ResearcherManager researcherManager;
    @Autowired
    public Researchers(ApiContext apiContext, ResearcherManager researcherManager) {
        super(apiContext);
        this.researcherManager = researcherManager;
    }

    @RequestMapping(method = RequestMethod.POST, value = {"/getWithExternal"}, consumes = "application/json", produces = "application/json")
    public @ResponseBody
    ResponseEntity<ResponseItem<List<eu.eudat.models.data.dmp.Researcher>>> getWithExternal(@RequestBody ResearcherCriteriaRequest researcherCriteriaRequest, Principal principal) throws HugeResultSet, NoURLFound {
        List<eu.eudat.models.data.dmp.Researcher> dataTable = this.researcherManager.getCriteriaWithExternal(researcherCriteriaRequest, principal);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<List<eu.eudat.models.data.dmp.Researcher>>().payload(dataTable).status(ApiMessageCode.NO_MESSAGE));
    }

    @Transactional
    @RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public @ResponseBody
    ResponseEntity<ResponseItem<Researcher>> create(@RequestBody eu.eudat.models.data.researcher.Researcher researcher, Principal principal) throws Exception {
        this.researcherManager.create(researcher, principal);
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseItem<Researcher>().status(ApiMessageCode.SUCCESS_MESSAGE));
    }

}

