package eu.eudat.elastic.entities;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.elasticsearch.common.xcontent.XContentBuilder;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class DatasetTempalate implements ElasticEntity<DatasetTempalate> {
	private UUID id;
	private String name;
	private Map<String, Object> data;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Map<String, Object> getData() {
		return data;
	}

	public void setData(Map<String, Object> data) {
		this.data = data;
	}

	@Override
	public XContentBuilder toElasticEntity(XContentBuilder builder) throws IOException {
		builder.startObject();
		builder.field("id", this.id.toString());
		builder.field("name", this.name);
		if(this.data != null) {
			builder.field("data", new ObjectMapper().writeValueAsString(this.data));
		}
		else{
			builder.field("data", "");
		}
		builder.endObject();
		return builder;
	}

	@Override
	public DatasetTempalate fromElasticEntity(Map<String, Object> fields) {
		this.id = UUID.fromString((String) fields.get("id"));
		this.name = (String) fields.get("name");
		try {
			this.data = new ObjectMapper().readValue((String) fields.get("data"), new TypeReference<Map<String, Object>>() {});
		}
		catch (Exception e){
			this.data = new HashMap<>();
		}
		return this;
	}
}
