package eu.eudat.elastic.entities;

import org.elasticsearch.common.xcontent.XContentBuilder;

import java.io.IOException;
import java.util.Map;
import java.util.UUID;

public class Doi implements ElasticEntity<Doi>{
    private UUID id;
    private String repositoryId;
    private String doi;
    private UUID dmp;

    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }

    public String getRepositoryId() {
        return repositoryId;
    }
    public void setRepositoryId(String repositoryId) {
        this.repositoryId = repositoryId;
    }

    public String getDoi() {
        return doi;
    }
    public void setDoi(String doi) {
        this.doi = doi;
    }

    public UUID getDmp() {
        return dmp;
    }
    public void setDmp(UUID dmp) {
        this.dmp = dmp;
    }

    @Override
    public XContentBuilder toElasticEntity(XContentBuilder builder) throws IOException {
        builder.startObject();
        builder.field("id", this.id.toString());
        builder.field("repositoryId", this.repositoryId);
        builder.field("doi", this.doi);
        builder.field("dmp", this.dmp.toString());
        builder.endObject();
        return builder;
    }

    @Override
    public Doi fromElasticEntity(Map<String, Object> fields) {
        if (fields == null || fields.isEmpty()) {
            return null;
        }
        this.id = UUID.fromString((String) fields.get("id"));
        this.repositoryId = (String) fields.get("repositoryId");
        this.doi = (String) fields.get("doi");
        this.dmp = UUID.fromString((String) fields.get("dmp"));
        return this;
    }
}
