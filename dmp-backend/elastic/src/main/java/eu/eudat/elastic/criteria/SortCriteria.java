package eu.eudat.elastic.criteria;

public class SortCriteria {
    public enum OrderByType {
        ASC, DESC
    }

    public enum ColumnType {
        COUNT, COLUMN, JOIN_COLUMN
    }

    private String fieldName;
    private OrderByType orderByType;
    private ColumnType columnType;

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public OrderByType getOrderByType() {
        return orderByType;
    }

    public void setOrderByType(OrderByType orderByType) {
        this.orderByType = orderByType;
    }

    public ColumnType getColumnType() {
        return columnType;
    }

    public void setColumnType(ColumnType columnType) {
        this.columnType = columnType;
    }
}
