package eu.eudat.elastic.criteria;

import eu.eudat.elastic.entities.Tag;

import java.util.List;
import java.util.UUID;

/**
 * Created by ikalyvas on 7/5/2018.
 */
public class DatasetCriteria extends Criteria {
    private String like;
    private List<UUID> datasetTemplates;
    private Short status;
    private List<UUID> dmps;
    private List<UUID> groupIds;
    private List<UUID> grants;
    private List<UUID> collaborators;
    private Boolean allowAllVersions;
    private List<String> organiztions;
    private Boolean hasTags;
    private List<Tag> tags;
    private boolean isPublic;
    private Short grantStatus;
    private int offset;
    private int size;
    private List<SortCriteria> sortCriteria;

    public String getLike() {
        return like;
    }

    public void setLike(String like) {
        this.like = like;
    }

    public List<UUID> getDatasetTemplates() {
        return datasetTemplates;
    }

    public void setDatasetTemplates(List<UUID> datasetTemplates) {
        this.datasetTemplates = datasetTemplates;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    public List<UUID> getDmps() {
        return dmps;
    }

    public void setDmps(List<UUID> dmps) {
        this.dmps = dmps;
    }

    public List<UUID> getGroupIds() {
        return groupIds;
    }

    public void setGroupIds(List<UUID> groupIds) {
        this.groupIds = groupIds;
    }

    public List<UUID> getGrants() {
        return grants;
    }

    public void setGrants(List<UUID> grants) {
        this.grants = grants;
    }

    public List<UUID> getCollaborators() {
        return collaborators;
    }

    public void setCollaborators(List<UUID> collaborators) {
        this.collaborators = collaborators;
    }

    public Boolean getAllowAllVersions() {
        return allowAllVersions;
    }

    public void setAllowAllVersions(Boolean allowAllVersions) {
        this.allowAllVersions = allowAllVersions;
    }

    public List<String> getOrganiztions() {
        return organiztions;
    }

    public void setOrganiztions(List<String> organiztions) {
        this.organiztions = organiztions;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setPublic(boolean aPublic) {
        isPublic = aPublic;
    }

    public Short getGrantStatus() {
        return grantStatus;
    }

    public void setGrantStatus(Short grantStatus) {
        this.grantStatus = grantStatus;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public List<SortCriteria> getSortCriteria() {
        return sortCriteria;
    }

    public void setSortCriteria(List<SortCriteria> sortCriteria) {
        this.sortCriteria = sortCriteria;
    }

    public Boolean getHasTags() {
        return hasTags;
    }

    public void setHasTags(Boolean hasTags) {
        this.hasTags = hasTags;
    }
}
