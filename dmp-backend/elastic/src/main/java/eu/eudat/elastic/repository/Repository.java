package eu.eudat.elastic.repository;

import eu.eudat.elastic.criteria.Criteria;
import eu.eudat.elastic.entities.ElasticEntity;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by ikalyvas on 7/5/2018.
 */
public interface Repository<ET extends ElasticEntity, C extends Criteria> {

    ET createOrUpdate(ET entity) throws IOException;

    ET findDocument(String id) throws IOException;

    List<ET> query(C criteria) throws ExecutionException, InterruptedException, IOException;

    Long count(C criteria) throws ExecutionException, InterruptedException, IOException;

    boolean exists() throws IOException;

    void clear() throws IOException;
}
