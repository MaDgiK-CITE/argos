package eu.eudat.elastic.entities;

import org.elasticsearch.common.xcontent.XContentBuilder;

import java.io.IOException;
import java.util.Map;

public class Collaborator implements ElasticEntity<Collaborator> {
	private String id;
	private String name;
	private int role;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getRole() {
		return role;
	}

	public void setRole(int role) {
		this.role = role;
	}

	@Override
	public XContentBuilder toElasticEntity(XContentBuilder builder) throws IOException {
		builder.startObject();
		builder.field("id", this.id);
		builder.field("name", this.name);
		builder.field("role", this.role);
		builder.endObject();
		return builder;
	}

	@Override
	public Collaborator fromElasticEntity(Map fields) {
		this.id = (String) fields.get("id");
		this.name = (String) fields.get("name");
		this.role = (int) fields.get("role");
		return this;
	}
}
