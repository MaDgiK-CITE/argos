package eu.eudat.elastic.entities;

import org.elasticsearch.common.document.DocumentField;
import org.elasticsearch.common.xcontent.XContentBuilder;

import java.io.IOException;
import java.util.Map;

/**
 * Created by ikalyvas on 7/5/2018.
 */
public interface ElasticEntity<T> {
    XContentBuilder toElasticEntity(XContentBuilder builder) throws IOException;
    T fromElasticEntity(Map<String, Object> fields);
}
