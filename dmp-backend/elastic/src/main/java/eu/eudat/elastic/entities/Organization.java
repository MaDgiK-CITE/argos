package eu.eudat.elastic.entities;

import org.elasticsearch.common.xcontent.XContentBuilder;

import java.io.IOException;
import java.util.Map;

public class Organization implements ElasticEntity<Organization> {
	private String id;
	private String name;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public XContentBuilder toElasticEntity(XContentBuilder builder) throws IOException {
		builder.startObject();
		builder.field("id", this.id);
		builder.field("name", this.name);
		builder.endObject();
		return builder;
	}

	@Override
	public Organization fromElasticEntity(Map fields) {
		this.id = (String) fields.get("id");
		this.name = (String) fields.get("name");
		return this;
	}
}
