package eu.eudat.elastic.criteria;

import java.util.List;
import java.util.UUID;

public class DmpCriteria extends Criteria {
	private String like;
	private Short status;
	private List<UUID> templates;
	private List<UUID> grants;
	private List<UUID> collaborators;
	private List<Integer> roles;
	private List<UUID> organizations;
	private boolean isPublic;
	private List<UUID> groupIds;
	private boolean allowAllVersions;
	private Short grantStatus;
	private int offset;
	private Integer size;
	private List<SortCriteria> sortCriteria;


	public String getLike() {
		return like;
	}

	public void setLike(String like) {
		this.like = like;
	}

	public Short getStatus() {
		return status;
	}

	public void setStatus(Short status) {
		this.status = status;
	}

	public List<UUID> getTemplates() {
		return templates;
	}

	public void setTemplates(List<UUID> templates) {
		this.templates = templates;
	}

	public List<UUID> getGrants() {
		return grants;
	}

	public void setGrants(List<UUID> grants) {
		this.grants = grants;
	}

	public List<UUID> getCollaborators() {
		return collaborators;
	}

	public void setCollaborators(List<UUID> collaborators) {
		this.collaborators = collaborators;
	}

	public List<Integer> getRoles() {
		return roles;
	}

	public void setRoles(List<Integer> roles) {
		this.roles = roles;
	}

	public List<UUID> getOrganizations() {
		return organizations;
	}

	public void setOrganizations(List<UUID> organizations) {
		this.organizations = organizations;
	}

	public boolean isPublic() {
		return isPublic;
	}

	public void setPublic(boolean aPublic) {
		isPublic = aPublic;
	}

	public List<UUID> getGroupIds() {
		return groupIds;
	}

	public void setGroupIds(List<UUID> groupIds) {
		this.groupIds = groupIds;
	}

	public boolean isAllowAllVersions() {
		return allowAllVersions;
	}

	public void setAllowAllVersions(boolean allowAllVersions) {
		this.allowAllVersions = allowAllVersions;
	}

	public Short getGrantStatus() {
		return grantStatus;
	}

	public void setGrantStatus(Short grantStatus) {
		this.grantStatus = grantStatus;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public List<SortCriteria> getSortCriteria() {
		return sortCriteria;
	}

	public void setSortCriteria(List<SortCriteria> sortCriteria) {
		this.sortCriteria = sortCriteria;
	}
}
