package eu.eudat.types.grant;

/**
 * Created by ikalyvas on 8/24/2018.
 */
public enum GrantStateType {
    ONGOING(0), FINISHED(1);

    private Integer value;

    private GrantStateType(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }

    public static GrantStateType fromInteger(Integer value) {
        switch (value) {
            case 0:
                return ONGOING;
            case 1:
                return FINISHED;
            default:
                throw new RuntimeException("Unsupported Grant State Type");
        }
    }
}
