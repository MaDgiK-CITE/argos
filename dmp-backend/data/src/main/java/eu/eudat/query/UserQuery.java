package eu.eudat.query;

import eu.eudat.data.dao.DatabaseAccessLayer;
import eu.eudat.data.entities.UserInfo;
import eu.eudat.queryable.QueryableList;

import java.util.List;
import java.util.UUID;

public class UserQuery extends Query<UserInfo, UUID> {

	private UUID id;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public UserQuery(DatabaseAccessLayer<UserInfo, UUID> databaseAccessLayer) {
		super(databaseAccessLayer);
	}

	public UserQuery(DatabaseAccessLayer<UserInfo, UUID> databaseAccessLayer, List<String> selectionFields) {
		super(databaseAccessLayer, selectionFields);
	}

	@Override
	public QueryableList<UserInfo> getQuery() {
		QueryableList<UserInfo> query = this.databaseAccessLayer.asQueryable();
		if (this.id != null)
			query.where((builder, root) -> builder.equal(root.get("id"), this.id));
		return query;
	}
}
