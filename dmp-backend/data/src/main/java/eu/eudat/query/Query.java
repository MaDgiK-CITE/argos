package eu.eudat.query;

import eu.eudat.data.dao.DatabaseAccessLayer;
import eu.eudat.queryable.QueryableList;
import eu.eudat.queryable.queryableentity.DataEntity;

import java.util.LinkedList;
import java.util.List;

public abstract class Query<T extends DataEntity, K> {
	protected DatabaseAccessLayer<T, K> databaseAccessLayer;

	private List<String> selectionFields = new LinkedList<>();

	public Query(DatabaseAccessLayer<T, K> databaseAccessLayer, List<String> selectionFields) {
		this.databaseAccessLayer = databaseAccessLayer;
		this.selectionFields = selectionFields;
	}

	public Query(DatabaseAccessLayer<T, K> databaseAccessLayer) {
		this.databaseAccessLayer = databaseAccessLayer;
	}

	public abstract QueryableList<T> getQuery();

	protected List<String> getSelectionFields() {
		return selectionFields;
	}
}
