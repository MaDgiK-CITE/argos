package eu.eudat.query;

import java.util.UUID;

public class DatasetProfileQuery {

    private UserQuery userQuery;

    public UserQuery getUserQuery() {
        return userQuery;
    }

    public void setUserQuery(UserQuery userQuery) {
        this.userQuery = userQuery;
    }
}
