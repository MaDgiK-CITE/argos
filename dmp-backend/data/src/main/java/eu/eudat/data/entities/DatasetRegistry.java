package eu.eudat.data.entities;


import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;


@Entity
@Table(name = "\"DatasetRegistry\"")
public class DatasetRegistry {

    @Id
    @GeneratedValue
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "\"ID\"", updatable = false, nullable = false, columnDefinition = "BINARY(16)")
    private UUID id;


    @Type(type = "org.hibernate.type.PostgresUUIDType") //DEPWARN dependency to Hibernate and PostgreSQL
    @Column(name = "\"Dataset\"", nullable = false)
    private UUID dataset;

    @Type(type = "org.hibernate.type.PostgresUUIDType") //DEPWARN dependency to Hibernate and PostgreSQL
    @Column(name = "\"Registry\"", nullable = false)
    private UUID registry;

    @Column(name = "\"Role\"")
    private Integer role;

    @Column(name = "\"Data\"")
    private String data;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getDataset() {
        return dataset;
    }

    public void setDataset(UUID dataset) {
        this.dataset = dataset;
    }

    public UUID getRegistry() {
        return registry;
    }

    public void setRegistry(UUID registry) {
        this.registry = registry;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
