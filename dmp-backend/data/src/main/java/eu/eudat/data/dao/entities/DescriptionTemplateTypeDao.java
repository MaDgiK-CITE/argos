package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccessLayer;
import eu.eudat.data.entities.DescriptionTemplateType;

import java.util.UUID;

public interface DescriptionTemplateTypeDao extends DatabaseAccessLayer<DescriptionTemplateType, UUID> {
    DescriptionTemplateType findFromName(String name);
}
