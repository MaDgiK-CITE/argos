package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccess;
import eu.eudat.data.dao.databaselayer.service.DatabaseService;
import eu.eudat.data.entities.FileUpload;
import eu.eudat.queryable.QueryableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Component("FileUploadDao")
public class FileUploadDaoImpl extends DatabaseAccess<FileUpload> implements FileUploadDao {

    @Autowired
    public FileUploadDaoImpl(DatabaseService<FileUpload> databaseService) {
        super(databaseService);
    }

    @Override
    public FileUpload createOrUpdate(FileUpload item)  {
        return getDatabaseService().createOrUpdate(item, FileUpload.class);
    }
//
    @Override
    public CompletableFuture<FileUpload> createOrUpdateAsync(FileUpload item) {
        return CompletableFuture.supplyAsync(() -> this.createOrUpdate(item));
    }

    @Override
    public FileUpload find(UUID id) {
        return getDatabaseService().getQueryable(FileUpload.class).where((builder, root) -> builder.equal(root.get("id"), id)).getSingle();
    }

    @Override
    public List<FileUpload> getFileUploads(UUID entityId) {
        return this.getDatabaseService().getQueryable(FileUpload.class).where((builder, root) -> builder.equal(root.get("entityId"), entityId)).toList();
    }

    @Override
    public FileUpload find(UUID id, String hint) {
        return null;
    }

    @Override
    public void delete(FileUpload item) {
        this.getDatabaseService().delete(item);
    }

    @Override
    public QueryableList<FileUpload> asQueryable() {
        return this.getDatabaseService().getQueryable(FileUpload.class);
    }
}