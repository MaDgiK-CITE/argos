package eu.eudat.data.entities;

import eu.eudat.data.entities.helpers.EntityBinder;
import eu.eudat.queryable.queryableentity.DataEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "\"UserDatasetProfile\"")
public class UserDatasetProfile implements DataEntity<UserDatasetProfile, UUID> {
    @Id
    @GeneratedValue
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "id", updatable = false, nullable = false, columnDefinition = "BINARY(16)")
    private UUID id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usr")
    private UserInfo user;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "\"descriptionTemplate\"")
    private DescriptionTemplate descriptionTemplate;

    @Column(name = "role")
    private Integer role;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UserInfo getUser() {
        return user;
    }

    public void setUser(UserInfo user) {
        this.user = user;
    }

    public DescriptionTemplate getDatasetProfile() {
        return descriptionTemplate;
    }

    public void setDatasetProfile(DescriptionTemplate descriptionTemplate) {
        this.descriptionTemplate = descriptionTemplate;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    @Override
    public void update(UserDatasetProfile entity) {
        this.role = entity.getRole();
    }

    @Override
    public UUID getKeys() {
        return this.id;
    }

    @Override
    public UserDatasetProfile buildFromTuple(List<Tuple> tuple, List<String> fields, String base) {
        String currentBase = base.isEmpty() ? "" : base + ".";
        if (fields.contains(currentBase + "id")) this.id = EntityBinder.fromTuple(tuple, currentBase + "id");
        return this;
    }
}
