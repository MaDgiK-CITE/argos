package eu.eudat.data.entities;


import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;


@Entity
@Table(name = "\"DMPOrganisation\"")
public class DMPOrganisation {

    @Id
    @GeneratedValue
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "\"ID\"", updatable = false, nullable = false, columnDefinition = "BINARY(16)")
    private UUID id;

    @Column(name = "\"DMP\"")
    private UUID dmp;

    @Column(name = "\"Organisation\"")
    private UUID organisation;

    @Column(name = "\"Role\"")
    private Integer role;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getDmp() {
        return dmp;
    }

    public void setDmp(UUID dmp) {
        this.dmp = dmp;
    }

    public UUID getOrganisation() {
        return organisation;
    }

    public void setOrganisation(UUID organisation) {
        this.organisation = organisation;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }


}
