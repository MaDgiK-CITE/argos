package eu.eudat.data.entities;

import eu.eudat.data.converters.DateToUTCConverter;
import eu.eudat.queryable.queryableentity.DataEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "\"EmailConfirmation\"")
public class EmailConfirmation implements DataEntity<EmailConfirmation, UUID> {

	@Id
	@GeneratedValue
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(name = "\"ID\"", updatable = false, nullable = false)
	private UUID id;

	@Column(name = "\"email\"", nullable = false)
	private String email;

	@Column(name = "\"isConfirmed\"", nullable = false)
	private boolean isConfirmed;

	@Column(name = "\"token\"", updatable = false, nullable = false, columnDefinition = "BINARY(16)")
	private UUID token;

	@Column(name = "\"userId\"", nullable = false)
	private UUID userId;
	
	@Column(name = "\"data\"")
	private String data;

	@Column(name = "\"expiresAt\"", nullable = false)
	@Convert(converter = DateToUTCConverter.class)
	private Date expiresAt;

	public UUID getId() {
		return id;
	}
	public void setId(UUID id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public boolean getIsConfirmed() {
		return isConfirmed;
	}
	public void setIsConfirmed(boolean confirmed) {
		isConfirmed = confirmed;
	}

	public UUID getToken() {
		return token;
	}
	public void setToken(UUID token) {
		this.token = token;
	}

	public UUID getUserId() {
		return userId;
	}
	public void setUserId(UUID userId) {
		this.userId = userId;
	}

	public Date getExpiresAt() {
		return expiresAt;
	}
	public void setExpiresAt(Date expiresAt) {
		this.expiresAt = expiresAt;
	}

	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	@Override
	public void update(EmailConfirmation entity) {
	}

	@Override
	public UUID getKeys() {
		return null;
	}

	@Override
	public EmailConfirmation buildFromTuple(List<Tuple> tuple, List<String> fields, String base) {
		return null;
	}
}
