package eu.eudat.data.entities;


import eu.eudat.data.converters.DateToUTCConverter;
import eu.eudat.data.entities.helpers.EntityBinder;
import eu.eudat.queryable.queryableentity.DataEntity;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;


@Entity
@Table(name = "\"Researcher\"")
public class Researcher implements DataEntity<Researcher, UUID> {

	@Id
	/*@GeneratedValue
	@GenericGenerator(name = "uuid2", strategy = "uuid2")*/
	@Column(name = "\"ID\"", updatable = false, nullable = false, columnDefinition = "BINARY(16)")
	private UUID id;

	@Column(name = "\"Label\"", nullable = false, length = 250)
	private String label;

	@Column(name = "\"Uri\"")
	private String uri;

	@Column(name = "\"PrimaryEmail\"")
	private String primaryEmail;

	@Type(type = "eu.eudat.configurations.typedefinition.XMLType")
	@Column(name = "\"Definition\"", columnDefinition = "xml", nullable = true)
	private String definition;

	@Type(type = "eu.eudat.configurations.typedefinition.XMLType")
	@Column(name = "\"Reference\"", columnDefinition = "xml", nullable = true)
	private String reference;


	@OneToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "\"DMPResearcher\"",
			joinColumns = {@JoinColumn(name = "\"Researcher\"", referencedColumnName = "\"ID\"")},
			inverseJoinColumns = {@JoinColumn(name = "\"DMP\"", referencedColumnName = "\"ID\"")}
	)
	private Set<DMP> dMPs;


	@Column(name = "\"Status\"", nullable = false)
	private Short status;


	@Column(name = "\"Created\"")
	@Convert(converter = DateToUTCConverter.class)
	private Date created = null;

	@Column(name = "\"Modified\"")
	@Convert(converter = DateToUTCConverter.class)
	private Date modified = new Date();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "\"CreationUser\"", nullable = true)
	private UserInfo creationUser;

	public Short getStatus() {
		return status;
	}
	public void setStatus(Short status) {
		this.status = status;
	}

	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getModified() {
		return modified;
	}
	public void setModified(Date modified) {
		this.modified = modified;
	}

	public UUID getId() {
		return id;
	}
	public void setId(UUID id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}

	public String getPrimaryEmail() {
		return primaryEmail;
	}
	public void setPrimaryEmail(String primaryEmail) {
		this.primaryEmail = primaryEmail;
	}

	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getDefinition() {
		return definition;
	}
	public void setDefinition(String definition) {
		this.definition = definition;
	}

	public Set<DMP> getdMPs() {
		return dMPs;
	}
	public void setdMPs(Set<DMP> dMPs) {
		this.dMPs = dMPs;
	}

	public UserInfo getCreationUser() {
		return creationUser;
	}
	public void setCreationUser(UserInfo creationUser) {
		this.creationUser = creationUser;
	}

	@Override
	public void update(Researcher entity) {

	}

	@Override
	public UUID getKeys() {
		return this.id;
	}

	@Override
	public Researcher buildFromTuple(List<Tuple> tuple, List<String> fields, String base) {
		String currentBase = base.isEmpty() ? "" : base + ".";
		if (fields.contains(currentBase + "id")) this.id = EntityBinder.fromTuple(tuple, currentBase + "id");
		return this;
	}
}
