package eu.eudat.data.entities;


import eu.eudat.data.converters.DateToUTCConverter;
import eu.eudat.data.entities.helpers.EntityBinder;
import eu.eudat.queryable.queryableentity.DataEntity;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;


@Entity
@Table(name = "\"DMPProfile\"")
public class DMPProfile implements DataEntity<DMPProfile, UUID> {

    public enum Status {
        SAVED((short) 0), FINALIZED((short) 1), DELETED((short) 99);

        private short value;

        private Status(short value) {
            this.value = value;
        }

        public short getValue() {
            return value;
        }

        public static Status fromInteger(int value) {
            switch (value) {
                case 0:
                    return SAVED;
                case 1:
                    return FINALIZED;
                case 99:
                    return DELETED;
                default:
                    throw new RuntimeException("Unsupported Dmp Profile Status");
            }
        }
    }

    @Id
    @GeneratedValue
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "\"ID\"", updatable = false, nullable = false, columnDefinition = "BINARY(16)")
    private UUID id;


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "profile")
    private Set<DMP> dmps;


    @Column(name = "\"Label\"")
    private String label;

    @Type(type = "eu.eudat.configurations.typedefinition.XMLType")
    @Column(name = "\"Definition\"", columnDefinition = "xml", nullable = true)
    private String definition;


    @Column(name = "\"Status\"", nullable = false)
    private int status;


    @Column(name = "\"Created\"")
    @Convert(converter = DateToUTCConverter.class)
    private Date created = null;

    @Column(name = "\"Modified\"")
    @Convert(converter = DateToUTCConverter.class)
    private Date modified = new Date();

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    public Set<DMP> getDmps() {
        return dmps;
    }

    public void setDmps(Set<DMP> dmps) {
        this.dmps = dmps;
    }

    @Override
    public void update(DMPProfile entity) {
        this.modified = new Date();
        this.definition = entity.getDefinition();
        this.label = entity.getLabel();
        this.status= entity.getStatus();
    }

    @Override
    public UUID getKeys() {
        return this.id;
    }

    @Override
    public DMPProfile buildFromTuple(List<Tuple> tuple,List<String> fields, String base) {
        String currentBase = base.isEmpty() ? "" : base + ".";
        if (fields.contains(currentBase + "id")) this.id = EntityBinder.fromTuple(tuple, currentBase + "id");
        return this;
    }
}
