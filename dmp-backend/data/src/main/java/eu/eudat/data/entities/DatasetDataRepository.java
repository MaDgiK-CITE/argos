package eu.eudat.data.entities;

import eu.eudat.data.entities.helpers.EntityBinder;
import eu.eudat.queryable.queryableentity.DataEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

/**
 * Created by ikalyvas on 5/22/2018.
 */

@Entity
@Table(name = "\"DatasetDataRepository\"")
public class DatasetDataRepository implements DataEntity<DatasetDataRepository, UUID> {

	@Id
	@GeneratedValue
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(name = "\"ID\"", updatable = false, nullable = false, columnDefinition = "BINARY(16)")
	private UUID id;

	@ManyToOne
	@JoinColumn(name = "\"Dataset\"", nullable = false)
	private Dataset dataset;

	@ManyToOne
	@JoinColumn(name = "\"DataRepository\"", nullable = false)
	private DataRepository dataRepository;

	@Column(name = "\"Role\"")
	private Integer role;

	@Column(name = "\"Data\"")
	private String data;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public Dataset getDataset() {
		return dataset;
	}

	public void setDataset(Dataset dataset) {
		this.dataset = dataset;
	}

	public DataRepository getDataRepository() {
		return dataRepository;
	}

	public void setDataRepository(DataRepository dataRepository) {
		this.dataRepository = dataRepository;
	}

	public Integer getRole() {
		return role;
	}

	public void setRole(Integer role) {
		this.role = role;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	@Override
	public void update(DatasetDataRepository entity) {
		this.dataset = entity.getDataset();
		this.dataRepository = entity.getDataRepository();
		this.role = entity.getRole();
	}

	@Override
	public UUID getKeys() {
		return this.id;
	}

	@Override
	public DatasetDataRepository buildFromTuple(List<Tuple> tuple, List<String> fields, String base) {
		String currentBase = base.isEmpty() ? "" : base + ".";
		if (fields.contains(currentBase + "id")) this.id = EntityBinder.fromTuple(tuple, currentBase + "id");
		return this;
	}
}
