package eu.eudat.data.dao.criteria;

import eu.eudat.data.entities.Grant;
import eu.eudat.types.grant.GrantStateType;

import java.util.Date;

public class GrantCriteria extends Criteria<Grant> {
    private Date periodStart;
    private Date periodEnd;
    private String reference;
    private Integer grantStateType;
    private boolean isPublic;
    private String funderId;
    private String funderReference;
    private String exactReference;
    private boolean isActive;

    public Date getPeriodStart() {
        return periodStart;
    }
    public void setPeriodStart(Date periodStart) {
        this.periodStart = periodStart;
    }

    public Date getPeriodEnd() {
        return periodEnd;
    }
    public void setPeriodEnd(Date periodEnd) {
        this.periodEnd = periodEnd;
    }

    public String getReference() {
        return reference;
    }
    public void setReference(String reference) {
        this.reference = reference;
    }

    public Integer getGrantStateType() {
        return grantStateType;
    }
    public void setGrantStateType(Integer grantStateType) {
        this.grantStateType = grantStateType;
    }

    public boolean isPublic() {
        return isPublic;
    }
    public void setPublic(boolean aPublic) {
        isPublic = aPublic;
    }

    public String getFunderId() {
        return funderId;
    }
    public void setFunderId(String funderId) {
        this.funderId = funderId;
    }

    public String getFunderReference() {
        return funderReference;
    }
    public void setFunderReference(String funderReference) {
        this.funderReference = funderReference;
    }

    public String getExactReference() {
        return exactReference;
    }

    public void setExactReference(String exactReference) {
        this.exactReference = exactReference;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
