package eu.eudat.data.query.items.item.userinfo;

import eu.eudat.data.dao.criteria.UserInfoCriteria;
import eu.eudat.data.entities.UserInfo;
import eu.eudat.data.query.definition.Query;
import eu.eudat.queryable.QueryableList;


public class UserInfoRequestItem extends Query<UserInfoCriteria, UserInfo> {
    @Override
    public QueryableList<UserInfo> applyCriteria() {
        QueryableList<UserInfo> users = this.getQuery();
        if (this.getCriteria().getAppRoles() != null && !this.getCriteria().getAppRoles().isEmpty())
            users.where((builder, root) -> root.join("userRoles").get("role").in(this.getCriteria().getAppRoles()));
        if (this.getCriteria().getLike() != null)
            users.where((builder, root) -> builder.or(builder.like(builder.upper(root.get("name")), "%" + this.getCriteria().getLike().toUpperCase() + "%"), builder.like(root.get("email"), "%" + this.getCriteria().getLike() + "%")));
        if (this.getCriteria().getEmail() != null)
            users.where((builder, root) -> builder.equal(root.get("email"), this.getCriteria().getEmail()));
        return users;
    }
}
