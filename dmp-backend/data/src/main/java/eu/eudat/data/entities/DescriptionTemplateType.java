package eu.eudat.data.entities;

import eu.eudat.queryable.queryableentity.DataEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "\"DescriptionTemplateType\"")
public class DescriptionTemplateType implements DataEntity<DescriptionTemplateType, UUID> {

    public enum Status {
        SAVED((short) 0), FINALIZED((short) 1), DELETED((short) 99);

        private short value;

        private Status(short value) {
            this.value = value;
        }
        public short getValue() {
            return value;
        }

        public static Status fromInteger(int value) {
            switch (value) {
                case 0:
                    return SAVED;
                case 1:
                    return FINALIZED;
                case 99:
                    return DELETED;
                default:
                    throw new RuntimeException("Unsupported Description Template Type Status");
            }
        }
    }

    @Id
    @GeneratedValue
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "\"ID\"", updatable = false, nullable = false, columnDefinition = "BINARY(16)")
    private UUID id;

    @Column(name = "\"Name\"", nullable = false)
    private String name;

    @Column(name = "\"Status\"", nullable = false)
    private Short status;

    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public Short getStatus() {
        return status;
    }
    public void setStatus(Short status) {
        this.status = status;
    }

    @Override
    public void update(DescriptionTemplateType entity) {
        this.name = entity.name;
        this.status = entity.status;
    }

    @Override
    public UUID getKeys() {
        return this.id;
    }

    @Override
    public DescriptionTemplateType buildFromTuple(List<Tuple> tuple, List<String> fields, String base) {
        this.id = UUID.fromString((String) tuple.get(0).get(!base.isEmpty() ? base + "." + "id" : "id"));
        return this;
    }
}
