package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccess;
import eu.eudat.data.dao.databaselayer.service.DatabaseService;
import eu.eudat.data.entities.UserDMP;
import eu.eudat.data.entities.UserDatasetProfile;
import eu.eudat.queryable.QueryableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Component("userDatasetProfileDao")
public class UserDatasetProfileDaoImpl extends DatabaseAccess<UserDatasetProfile> implements UserDatasetProfileDao {

    @Autowired
    public UserDatasetProfileDaoImpl(DatabaseService<UserDatasetProfile> databaseService) {
        super(databaseService);
    }

    @Override
    public UserDatasetProfile createOrUpdate(UserDatasetProfile item) {
        return this.getDatabaseService().createOrUpdate(item, UserDatasetProfile.class);
    }

    @Override
    public UserDatasetProfile find(UUID id) {
        return this.getDatabaseService().getQueryable(UserDatasetProfile.class).where((builder, root) -> builder.equal(root.get("id"), id)).getSingleOrDefault();
    }

    @Override
    public void delete(UserDatasetProfile item) {
        this.getDatabaseService().delete(item);
    }

    @Override
    public QueryableList<UserDatasetProfile> asQueryable() {
        return this.getDatabaseService().getQueryable(UserDatasetProfile.class);
    }

    @Async
    @Override
    public CompletableFuture<UserDatasetProfile> createOrUpdateAsync(UserDatasetProfile item) {
        return CompletableFuture.supplyAsync(() -> this.createOrUpdate(item));
    }

    @Override
    public UserDatasetProfile find(UUID id, String hint) {
        throw new UnsupportedOperationException();
    }
}
