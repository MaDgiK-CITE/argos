package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccess;
import eu.eudat.data.dao.databaselayer.service.DatabaseService;
import eu.eudat.data.entities.DatasetService;
import eu.eudat.queryable.QueryableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

/**
 * Created by ikalyvas on 5/22/2018.
 */
@Component("datasetServiceDao")
public class DatasetServiceDaoImpl extends DatabaseAccess<DatasetService> implements DatasetServiceDao {

    @Autowired
    public DatasetServiceDaoImpl(DatabaseService<DatasetService> databaseService) {
        super(databaseService);
    }

    @Override
    public DatasetService createOrUpdate(DatasetService item) {
        return this.getDatabaseService().createOrUpdate(item, DatasetService.class);
    }

    @Async
    @Override
    public CompletableFuture<DatasetService> createOrUpdateAsync(DatasetService item) {
        return CompletableFuture.supplyAsync(() -> this.createOrUpdate(item));
    }

    @Override
    public DatasetService find(UUID id) {
        return getDatabaseService().getQueryable(DatasetService.class).where((builder, root) -> builder.equal(root.get("id"), id)).getSingle();
    }

    @Override
    public DatasetService find(UUID id, String hint) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(DatasetService item) {
        this.getDatabaseService().delete(item);
    }

    @Override
    public QueryableList<DatasetService> asQueryable() {
        return this.getDatabaseService().getQueryable(DatasetService.class);
    }
}
