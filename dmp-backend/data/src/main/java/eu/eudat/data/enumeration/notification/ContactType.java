package eu.eudat.data.enumeration.notification;

public enum ContactType {
	EMAIL(0);

	private int type;

	ContactType(int type) {
		this.type = type;
	}

	public int getType() {
		return type;
	}

	public ContactType fromInteger(int type) {
		switch (type) {
			case 0:
				return EMAIL;
			default:
				throw new RuntimeException("Unsupported Contact Type");
		}
	}
}
