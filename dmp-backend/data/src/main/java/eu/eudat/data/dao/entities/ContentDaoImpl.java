package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccess;
import eu.eudat.data.dao.databaselayer.service.DatabaseService;
import eu.eudat.data.entities.Content;
import eu.eudat.queryable.QueryableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

/**
 * Created by ikalyvas on 3/16/2018.
 */
@Service("contentDao")
public class ContentDaoImpl extends DatabaseAccess<Content> implements ContentDao {

    @Autowired
    public ContentDaoImpl(DatabaseService<Content> databaseService) {
        super(databaseService);
    }

    @Override
    public Content createOrUpdate(Content item) {
        return this.getDatabaseService().createOrUpdate(item, Content.class);
    }

    @Override
    @Async
    public CompletableFuture<Content> createOrUpdateAsync(Content item) {
        return CompletableFuture.supplyAsync(() -> this.createOrUpdate(item));
    }

    @Override
    public Content find(UUID id) {
        return this.getDatabaseService().getQueryable(Content.class).where((builder, root) -> builder.equal(root.get("id"), id)).getSingle();
    }

    @Override
    public Content find(UUID id, String hint) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(Content item) {
        this.getDatabaseService().delete(item);
    }

    @Override
    public QueryableList<Content> asQueryable() {
        return this.getDatabaseService().getQueryable(Content.class);
    }
}
