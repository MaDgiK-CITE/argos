package eu.eudat.data.dao;


import eu.eudat.queryable.QueryableList;
import eu.eudat.queryable.queryableentity.DataEntity;

import java.util.concurrent.CompletableFuture;

public interface DatabaseAccessLayer<T extends DataEntity, I> {
    T createOrUpdate(T item);

    CompletableFuture<T> createOrUpdateAsync(T item);
    
    T find(I id);

    T find(I id, String hint);

    void delete(T item);

    QueryableList<T> asQueryable();
}
