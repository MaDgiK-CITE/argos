package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccessLayer;
import eu.eudat.data.entities.UserDMP;

import java.util.UUID;

/**
 * Created by ikalyvas on 2/8/2018.
 */
public interface UserDmpDao extends DatabaseAccessLayer<UserDMP, UUID> {
}
