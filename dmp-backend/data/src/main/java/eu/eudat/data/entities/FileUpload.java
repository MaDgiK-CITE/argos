package eu.eudat.data.entities;

import eu.eudat.data.converters.DateToUTCConverter;
import eu.eudat.data.entities.helpers.EntityBinder;
import eu.eudat.queryable.queryableentity.DataEntity;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Entity
@Table(name = "\"FileUpload\"")
public class FileUpload implements DataEntity<FileUpload, UUID> {
    public enum EntityType {
        DATASET, DMP
    }

    @Id
    @Column(name = "\"ID\"", updatable = false, nullable = false, columnDefinition = "BINARY(16)")
    private UUID id;

    @Column(name = "\"Name\"", nullable = false)
    private String name;

    @Column(name = "\"FileType\"", nullable = false)
    private String fileType;

    @Column(name = "\"EntityId\"", nullable = false)
    private UUID entityId;

    @Enumerated(EnumType.STRING)
    @Type(type = "eu.eudat.configurations.typedefinition.PostgreSQLEnumType")
    @Column(name = "\"EntityType\"", nullable = false)
    private EntityType entityType;

    @Column(name = "\"CreatedAt\"", nullable = false)
    @Convert(converter = DateToUTCConverter.class)
    private Date createdAt;

    @Column(name = "\"IsDeleted\"", nullable = false)
    private Boolean isDeleted;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "\"Creator\"")
    private UserInfo creator;

    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getFileType() {
        return fileType;
    }
    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public UUID getEntityId() {
        return entityId;
    }
    public void setEntityId(UUID entityId) {
        this.entityId = entityId;
    }

    public EntityType getEntityType() {
        return entityType;
    }
    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public Date getCreatedAt() {
        return createdAt;
    }
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }
    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public UserInfo getCreator() {
        return creator;
    }
    public void setCreator(UserInfo creator) {
        this.creator = creator;
    }

    @Override
    public void update(FileUpload file) {
        this.name = file.getName();
        this.fileType = file.getFileType();
        this.entityId = file.getEntityId();
        this.entityType = file.getEntityType();
        this.createdAt = file.getCreatedAt();
        this.isDeleted = file.getIsDeleted();
        this.creator = file.getCreator();
    }

    @Override
    public UUID getKeys() {
        return this.id;
    }

    @Override
    public FileUpload buildFromTuple(List<Tuple> tuple, List<String> fields, String base) {
        String currentBase = base.isEmpty() ? "" : base + ".";
        if (fields.contains(currentBase + "id")) this.id = EntityBinder.fromTuple(tuple, currentBase + "id");
        this.creator = tuple.stream().map(x -> new UserInfo().buildFromTuple(tuple, fields , base.isEmpty() ? "creator" : base + "." + "creator")).collect(Collectors.toList()).get(0);
        return this;
    }
}
