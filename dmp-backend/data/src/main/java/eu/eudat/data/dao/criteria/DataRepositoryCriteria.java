package eu.eudat.data.dao.criteria;

import eu.eudat.data.entities.DataRepository;

import java.util.UUID;

public class DataRepositoryCriteria extends Criteria<DataRepository> {

	private UUID creationUserId;

	public UUID getCreationUserId() {
		return creationUserId;
	}
	public void setCreationUserId(UUID creationUserId) {
		this.creationUserId = creationUserId;
	}
}
