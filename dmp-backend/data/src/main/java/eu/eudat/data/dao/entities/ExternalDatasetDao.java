package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccessLayer;
import eu.eudat.data.dao.criteria.ExternalDatasetCriteria;
import eu.eudat.data.entities.ExternalDataset;
import eu.eudat.queryable.QueryableList;

import java.util.UUID;


public interface ExternalDatasetDao extends DatabaseAccessLayer<ExternalDataset, UUID> {

    QueryableList<ExternalDataset> getWithCriteria(ExternalDatasetCriteria criteria);

}
