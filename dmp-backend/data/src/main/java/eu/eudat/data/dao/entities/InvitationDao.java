package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccessLayer;
import eu.eudat.data.dao.criteria.InvitationCriteria;
import eu.eudat.data.entities.Invitation;
import eu.eudat.queryable.QueryableList;

import java.util.UUID;


public interface InvitationDao extends DatabaseAccessLayer<Invitation, UUID> {

    QueryableList<Invitation> getWithCriteria(InvitationCriteria criteria);

}
