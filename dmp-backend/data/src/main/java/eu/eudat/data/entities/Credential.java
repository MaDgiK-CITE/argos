package eu.eudat.data.entities;

import eu.eudat.data.converters.DateToUTCConverter;
import eu.eudat.data.entities.helpers.EntityBinder;
import eu.eudat.queryable.queryableentity.DataEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.UUID;


@Entity
@Table(name = "\"Credential\"")
@NamedEntityGraphs({
		@NamedEntityGraph(
				name = "credentialUserInfo",
				attributeNodes = {@NamedAttributeNode("userInfo")})
})
public class Credential implements DataEntity<Credential, UUID> {

	@Id
	@Column(name = "\"Id\"", updatable = false, nullable = false, columnDefinition = "BINARY(16)")
	private UUID id;

	@ManyToOne
	@JoinColumn(name = "\"UserId\"", nullable = false)
	private UserInfo userInfo;

	@Column(name = "\"Status\"", nullable = false)
	private Integer status;

	@Column(name = "\"Provider\"", nullable = false)
	private Integer provider;
	@Column(name = "\"Public\"", nullable = false)
	private String publicValue;
	@Column(name = "\"Email\"")
	private String email;
	@Column(name = "\"Secret\"", nullable = false)
	private String secret;

	@Column(name = "\"CreationTime\"", nullable = false)
	@Convert(converter = DateToUTCConverter.class)
	private Date creationTime;

	@Column(name = "\"LastUpdateTime\"", nullable = false)
	@Convert(converter = DateToUTCConverter.class)
	private Date lastUpdateTime;

	@Column(name = "\"ExternalId\"", nullable = false)
	private String externalId;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getProvider() {
		return provider;
	}

	public void setProvider(Integer provider) {
		this.provider = provider;
	}

	public String getPublicValue() {
		return publicValue;
	}

	public void setPublicValue(String publicValue) {
		this.publicValue = publicValue;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public Date getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}

	public Date getLastUpdateTime() {
		return lastUpdateTime;
	}

	public void setLastUpdateTime(Date lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Credential that = (Credential) o;

		return provider.intValue() == that.provider.intValue();
	}

	@Override
	public int hashCode() {
		return provider.intValue();
	}

	@Override
	public void update(Credential entity) {
		this.status = entity.status;
		this.publicValue = entity.getPublicValue();
		this.email = entity.getEmail();
		this.secret = entity.getSecret();
		this.lastUpdateTime = new Date();
	}

	@Override
	public UUID getKeys() {
		return this.id;
	}

	@Override
	public Credential buildFromTuple(List<Tuple> tuple, List<String> fields, String base) {
		String currentBase = base.isEmpty() ? "" : base + ".";
		if (fields.contains(currentBase + "id")) this.id = EntityBinder.fromTuple(tuple, currentBase + "id");
		return this;
	}
}
