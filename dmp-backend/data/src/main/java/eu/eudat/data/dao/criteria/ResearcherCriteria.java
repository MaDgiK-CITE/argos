package eu.eudat.data.dao.criteria;

import eu.eudat.data.entities.Researcher;

import java.util.Date;

public class ResearcherCriteria extends Criteria<Researcher> {
    private String name;
    private String reference;
    private Date periodStart;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Date getPeriodStart() {
        return periodStart;
    }

    public void setPeriodStart(Date periodStart) {
        this.periodStart = periodStart;
    }
}
