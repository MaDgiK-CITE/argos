package eu.eudat.data.dao.entities.security;

import eu.eudat.data.dao.DatabaseAccessLayer;
import eu.eudat.data.entities.UserToken;

import java.util.UUID;


public interface UserTokenDao extends DatabaseAccessLayer<UserToken, UUID> {

    UserToken createOrUpdate(UserToken item);

    UserToken find(UUID id);

    void delete(UserToken token);
}
