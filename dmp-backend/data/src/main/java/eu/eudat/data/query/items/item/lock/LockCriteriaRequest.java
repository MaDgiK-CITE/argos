package eu.eudat.data.query.items.item.lock;

import eu.eudat.data.dao.criteria.LockCriteria;
import eu.eudat.data.entities.Lock;
import eu.eudat.data.query.definition.Query;
import eu.eudat.queryable.QueryableList;

public class LockCriteriaRequest extends Query<LockCriteria, Lock> {
	@Override
	public QueryableList<Lock> applyCriteria() {
		QueryableList<Lock> query = this.getQuery();
		if (this.getCriteria().getTouchedAt() != null)
			query.where((builder, root) -> builder.equal(root.get("touchedAt"), this.getCriteria().getTouchedAt()));
		if (this.getCriteria().getLockedBy() != null)
			query.where(((builder, root) -> builder.equal(root.get("lockedBy"), this.getCriteria().getLockedBy())));
		if (this.getCriteria().getTarget() != null)
			query.where(((builder, root) -> builder.equal(root.get("target"), this.getCriteria().getTarget())));
		return query;
	}
}
