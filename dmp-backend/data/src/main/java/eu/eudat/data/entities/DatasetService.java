package eu.eudat.data.entities;


import eu.eudat.queryable.queryableentity.DataEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;


@Entity
@Table(name = "\"DatasetService\"")
public class DatasetService implements DataEntity<DatasetService, UUID> {

	@Id
	@GeneratedValue
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(name = "\"ID\"", updatable = false, nullable = false, columnDefinition = "BINARY(16)")
	private UUID id;


	@ManyToOne
	@JoinColumn(name = "\"Dataset\"", nullable = false)
	private Dataset dataset;

	@ManyToOne
	@JoinColumn(name = "\"Service\"", nullable = false)
	private Service service;

	@Column(name = "\"Role\"")
	private Integer role;

	@Column(name = "\"Data\"")
	private String data;


	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public Dataset getDataset() {
		return dataset;
	}

	public void setDataset(Dataset dataset) {
		this.dataset = dataset;
	}

	public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}

	public Integer getRole() {
		return role;
	}

	public void setRole(Integer role) {
		this.role = role;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	@Override
	public void update(DatasetService entity) {
		this.dataset = entity.getDataset();
		this.service = entity.getService();
		this.role = entity.getRole();
	}

	@Override
	public UUID getKeys() {
		return this.id;
	}

	@Override
	public DatasetService buildFromTuple(List<Tuple> tuple, List<String> fields, String base) {
		String currentBase = base.isEmpty() ? "" : base + ".";
		if(fields.contains(currentBase + "id")) this.id = UUID.fromString((String) tuple.get(0).get(currentBase + "id"));
		return this;
	}
}
