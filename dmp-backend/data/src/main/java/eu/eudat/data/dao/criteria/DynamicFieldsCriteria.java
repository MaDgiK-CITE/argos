package eu.eudat.data.dao.criteria;

import java.util.List;

/**
 * Created by ikalyvas on 3/26/2018.
 */
public class DynamicFieldsCriteria extends Criteria {
    public static class DynamicFieldDependencyCriteria {
        private String property;
        private String value;

        public DynamicFieldDependencyCriteria() {
        }

        public String getProperty() {
            return property;
        }

        public void setProperty(String property) {
            this.property = property;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    private String id;
    private List<DynamicFieldDependencyCriteria> dynamicFields;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<DynamicFieldDependencyCriteria> getDynamicFields() {
        return dynamicFields;
    }

    public void setDynamicFields(List<DynamicFieldDependencyCriteria> dynamicFields) {
        this.dynamicFields = dynamicFields;
    }
}
