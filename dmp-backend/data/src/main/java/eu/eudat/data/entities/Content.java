package eu.eudat.data.entities;

import eu.eudat.data.entities.helpers.EntityBinder;
import eu.eudat.queryable.queryableentity.DataEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

/**
 * Created by ikalyvas on 3/15/2018.
 */
@Entity
@Table(name = "\"Content\"")
public class Content implements DataEntity<Content, UUID> { //IGNORE ME

	public enum ParentType {
		GRANT(0);

		private int value;

		private ParentType(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}

		public static ParentType fromInteger(int value) {
			switch (value) {
				case 0:
					return GRANT;
				default:
					throw new RuntimeException("Unsupported Content Parent Type Status");
			}
		}
	}

	public enum LocationType {
		EXTERNAL(0), INTERNAL(1);

		private Integer value;

		private LocationType(Integer value) {
			this.value = value;
		}

		public Integer getValue() {
			return value;
		}

		public static LocationType fromInteger(int value) {
			switch (value) {
				case 0:
					return EXTERNAL;
				case 1:
					return INTERNAL;
				default:
					throw new RuntimeException("Unsupported Content Location Type");
			}
		}
	}

	@Id
	@GeneratedValue
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(name = "\"Id\"", updatable = false, nullable = false, columnDefinition = "BINARY(16)")
	private UUID id;

	@Column(name = "\"Filename\"", nullable = false)
	private String label;

	@Column(name = "\"Extension\"", nullable = false)
	private String extension;

	@Column(name = "\"ParentType\"", nullable = false)
	private Integer parentType;

	@Column(name = "\"Uri\"", nullable = false)
	private String uri;

	@Column(name = "\"LocationType\"", nullable = false)
	private Integer locationType;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public Integer getParentType() {
		return parentType;
	}

	public void setParentType(Integer parentType) {
		this.parentType = parentType;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public Integer getLocationType() {
		return locationType;
	}

	public void setLocationType(Integer locationType) {
		this.locationType = locationType;
	}

	@Override
	public void update(Content entity) {
		this.extension = entity.getExtension();
		this.label = entity.getLabel();
		this.locationType = entity.getLocationType();
		this.parentType = entity.getParentType();
		this.uri = entity.getUri();
	}

	@Override
	public UUID getKeys() {
		return this.id;
	}

	@Override
	public Content buildFromTuple(List<Tuple> tuple, List<String> fields, String base) {
		String currentBase = base.isEmpty() ? "" : base + ".";
		if (fields.contains(currentBase + "id")) this.id = EntityBinder.fromTuple(tuple, currentBase + "id");
		return this;
	}
}
