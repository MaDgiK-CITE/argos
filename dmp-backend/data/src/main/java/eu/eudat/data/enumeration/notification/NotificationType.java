package eu.eudat.data.enumeration.notification;

public enum NotificationType {
	DMP_MODIFIED(0),
	DATASET_MODIFIED(1),
	DMP_PUBLISH(2),
	DMP_FINALISED(3),
	DMP_MODIFIED_FINALISED(4),
	DATASET_MODIFIED_FINALISED(5);

	private int type;

	NotificationType(int type) {
		this.type = type;
	}

	public int getType() {
		return type;
	}

	public NotificationType fromInteger(int type) {
		switch (type) {
			case 0:
				return DMP_MODIFIED;
			case 1:
				return DATASET_MODIFIED;
			case 2:
				return DMP_PUBLISH;
			case 3:
				return DMP_FINALISED;
			case 4:
				return DMP_MODIFIED_FINALISED;
			case 5:
				return DATASET_MODIFIED_FINALISED;
			default:
				throw new RuntimeException("Unsupported Notification Type");
		}
	}
}
