package eu.eudat.data.entities;

import eu.eudat.data.entities.helpers.EntityBinder;
import eu.eudat.queryable.queryableentity.DataEntity;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;


@Entity
@Table(name = "\"Invitation\"")
public class Invitation implements DataEntity<Invitation, UUID> {

	@Id
	@GeneratedValue
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(name = "\"Id\"", updatable = false, nullable = false)
	private UUID id;

	@Column(name = "\"InvitationEmail\"", nullable = false)
	private String invitationEmail;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "\"CreationUser\"", nullable = false)
	private UserInfo user;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "\"Dmp\"", nullable = false)
	private DMP dmp;

	@Column(name = "\"Token\"", updatable = false, nullable = false, columnDefinition = "BINARY(16)")
	private UUID token;

	@Column(name = "\"AcceptedInvitation\"", nullable = false)
	private boolean acceptedInvitation;

	@Type(type = "eu.eudat.configurations.typedefinition.XMLType")
	@Column(name = "\"Properties\"", columnDefinition = "xml", nullable = true)
	private String properties;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getInvitationEmail() {
		return invitationEmail;
	}

	public void setInvitationEmail(String invitationEmail) {
		this.invitationEmail = invitationEmail;
	}

	public UserInfo getUser() {
		return user;
	}

	public void setUser(UserInfo user) {
		this.user = user;
	}

	public DMP getDmp() {
		return dmp;
	}

	public void setDmp(DMP dmp) {
		this.dmp = dmp;
	}

	public UUID getToken() {
		return token;
	}

	public void setToken(UUID token) {
		this.token = token;
	}

	public String getProperties() {
		return properties;
	}

	public void setProperties(String properties) {
		this.properties = properties;
	}

	public boolean getAcceptedInvitation() {
		return acceptedInvitation;
	}

	public void setAcceptedInvitation(boolean acceptedInvitation) {
		this.acceptedInvitation = acceptedInvitation;
	}

	@Override
	public void update(Invitation entity) {

	}

	@Override
	public UUID getKeys() {
		return this.id;
	}

	@Override
	public Invitation buildFromTuple(List<Tuple> tuple, List<String> fields, String base) {
		String currentBase = base.isEmpty() ? "" : base + ".";
		if (fields.contains(currentBase + "id")) this.id = EntityBinder.fromTuple(tuple, currentBase + "id");
		return this;
	}
}
