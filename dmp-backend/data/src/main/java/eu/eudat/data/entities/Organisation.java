package eu.eudat.data.entities;

import eu.eudat.data.converters.DateToUTCConverter;
import eu.eudat.data.entities.helpers.EntityBinder;
import eu.eudat.queryable.queryableentity.DataEntity;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "\"Organisation\"")
@NamedEntityGraphs({
        @NamedEntityGraph(
                name = "organisationRecentActivity",
                attributeNodes = {@NamedAttributeNode(value = "dmps")}
        )
})
public class Organisation implements Serializable, DataEntity<Organisation,UUID> {

    @Id
    @GeneratedValue
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "\"ID\"", updatable = false, nullable = false, columnDefinition = "BINARY(16)")
    private UUID id;

    @Column(name = "\"Label\"")
    private String label;

    @Column(name = "\"Abbreviation\"")
    private String abbreviation;

    @Type(type = "eu.eudat.configurations.typedefinition.XMLType")
    @Column(name = "\"Reference\"", columnDefinition = "xml", nullable = true)
    private String reference;

    @Column(name = "\"Uri\"")
    private String uri;

    @Type(type = "eu.eudat.configurations.typedefinition.XMLType")
    @Column(name = "\"Definition\"", columnDefinition = "xml", nullable = true)
    private String definition;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "\"DMPOrganisation\"",
            joinColumns = {@JoinColumn(name = "\"Organisation\"", referencedColumnName = "\"ID\"")},
            inverseJoinColumns = {@JoinColumn(name = "\"DMP\"", referencedColumnName = "\"ID\"")}
    )
    private Set<DMP> dmps;

    @Column(name = "\"Status\"", nullable = false)
    private Short status;

    @Column(name = "\"Created\"")
    @Convert(converter = DateToUTCConverter.class)
    private Date created = null;

    @Column(name = "\"Modified\"")
    @Convert(converter = DateToUTCConverter.class)
    private Date modified = new Date();


    public Short getStatus() {
        return status;
    }
    public void setStatus(Short status) {
        this.status = status;
    }

    public Date getCreated() {
        return created;
    }
    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }
    public void setModified(Date modified) {
        this.modified = modified;
    }

    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }

    public String getAbbreviation() {
        return abbreviation;
    }
    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getReference() {
        return reference;
    }
    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getUri() {
        return uri;
    }
    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getDefinition() {
        return definition;
    }
    public void setDefinition(String definition) {
        this.definition = definition;
    }

    public Set<DMP> getDmps() {
        return dmps;
    }
    public void setDmps(Set<DMP> dmps) {
        this.dmps = dmps;
    }

    @Override
    public void update(Organisation entity) {

    }

    @Override
    public UUID getKeys() {
        return this.id;
    }

    @Override
    public Organisation buildFromTuple(List<Tuple> tuple,List<String> fields, String base) {
        String currentBase = base.isEmpty() ? "" : base + ".";
        if (fields.contains(currentBase + "id")) this.id = EntityBinder.fromTuple(tuple, currentBase + "id");
        return this;
    }
}
