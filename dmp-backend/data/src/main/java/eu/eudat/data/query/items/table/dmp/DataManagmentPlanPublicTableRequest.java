package eu.eudat.data.query.items.table.dmp;

import eu.eudat.data.dao.criteria.DataManagementPlanPublicCriteria;
import eu.eudat.data.entities.DMP;
import eu.eudat.data.query.PaginationService;
import eu.eudat.data.query.definition.TableQuery;
import eu.eudat.queryable.QueryableList;
import eu.eudat.queryable.types.FieldSelectionType;
import eu.eudat.queryable.types.SelectionField;
import eu.eudat.types.grant.GrantStateType;

import java.util.Arrays;
import java.util.Date;
import java.util.UUID;

public class DataManagmentPlanPublicTableRequest extends TableQuery<DataManagementPlanPublicCriteria, DMP, UUID> {

    public QueryableList<DMP> applyCriteria() {
        QueryableList<DMP> query = this.getQuery();
        query.where((builder, root) -> builder.equal(root.get("isPublic"), true));
        if (this.getCriteria().getLike() != null && !this.getCriteria().getLike().isEmpty())
            query.where((builder, root) -> builder.or(
                    builder.like(builder.upper(root.get("label")), "%" + this.getCriteria().getLike().toUpperCase() + "%"),
                    builder.like(builder.upper(root.get("description")), "%" + this.getCriteria().getLike().toUpperCase() + "%")));
        if (this.getCriteria().getGrants() != null && !this.getCriteria().getGrants().isEmpty())
            query.where(((builder, root) -> root.get("grant").get("id").in(this.getCriteria().getGrants())));
        if (this.getCriteria().getGrantStatus() != null) {
            if (this.getCriteria().getGrantStatus().getValue().equals(GrantStateType.FINISHED.getValue()))
                query.where((builder, root) -> builder.lessThan(root.get("grant").get("enddate"), new Date()));
            if (this.getCriteria().getGrantStatus().getValue().equals(GrantStateType.ONGOING.getValue()))
                query.where((builder, root) ->
                        builder.or(builder.greaterThan(root.get("grant").get("enddate"), new Date())
                                , builder.isNull(root.get("grant").get("enddate"))));
        }
        if (this.getCriteria().datasetProfile != null && !this.getCriteria().datasetProfile.isEmpty())
            query.where((builder, root) -> root.join("associatedDmps").get("id").in(this.getCriteria().datasetProfile));
        if (this.getCriteria().getDmpOrganisations() != null && !this.getCriteria().getDmpOrganisations().isEmpty())
            query.where(((builder, root) -> root.join("organisations").get("reference").in(this.getCriteria().getDmpOrganisations())));
        if (!this.getCriteria().getAllVersions()) {
            query.initSubQuery(String.class).where((builder, root) -> builder.equal(root.get("version"),
                    query.<String>subQueryMax((builder1, externalRoot, nestedRoot) -> builder1.and(builder1.equal(externalRoot.get("groupId"),
                            nestedRoot.get("groupId")), builder1.equal(nestedRoot.get("isPublic"), true)), Arrays.asList(new SelectionField(FieldSelectionType.FIELD, "version")), String.class)));
        }
        if (this.getCriteria().getGroupIds() != null && !this.getCriteria().getGroupIds().isEmpty()) {
            query.where((builder, root) -> root.get("groupId").in(this.getCriteria().getGroupIds()));
        }
        return query;
    }

    @Override
    public QueryableList<DMP> applyPaging(QueryableList<DMP> items) {
        return PaginationService.applyPaging(items, this);
    }
}
