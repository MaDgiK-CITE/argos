package eu.eudat.data.dao.criteria;

import eu.eudat.data.entities.DMP;
import eu.eudat.types.grant.GrantStateType;

import java.util.List;
import java.util.UUID;

public class DataManagementPlanPublicCriteria extends Criteria<DMP> {
    private GrantStateType grantStatus;
    private List<UUID> grants;
    public List<UUID> datasetProfile;
    private List<String> dmpOrganisations;
    private Integer role;
    private boolean allVersions;
    private List<UUID> groupIds;

    public GrantStateType getGrantStatus() {
        return grantStatus;
    }
    public void setGrantStatus(GrantStateType grantStatus) {
        this.grantStatus = grantStatus;
    }

    public List<UUID> getGrants() {
        return grants;
    }
    public void setGrants(List<UUID> grants) {
        this.grants = grants;
    }

    public List<UUID> getDatasetProfile() {
        return datasetProfile;
    }
    public void setDatasetProfile(List<UUID> datasetProfile) {
        this.datasetProfile = datasetProfile;
    }

    public List<String> getDmpOrganisations() {
        return dmpOrganisations;
    }
    public void setDmpOrganisations(List<String> dmpOrganisations) {
        this.dmpOrganisations = dmpOrganisations;
    }

    public Integer getRole() {
        return role;
    }
    public void setRole(Integer role) {
        this.role = role;
    }

    public boolean getAllVersions() {
        return allVersions;
    }
    public void setAllVersions(boolean allVersions) {
        this.allVersions = allVersions;
    }

    public List<UUID> getGroupIds() {
        return groupIds;
    }
    public void setGroupIds(List<UUID> groupIds) {
        this.groupIds = groupIds;
    }
}
