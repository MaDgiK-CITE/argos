package eu.eudat.data.dao.criteria;

import eu.eudat.data.entities.Dataset;
import eu.eudat.elastic.entities.Tag;
import eu.eudat.types.grant.GrantStateType;

import java.util.Date;
import java.util.List;
import java.util.UUID;


public class DatasetCriteria extends Criteria<Dataset> {
    private Integer status;
    private Date periodStart;
    private Date periodEnd;
    private List<UUID> dmpIds;
    private List<Tag> tags;
    private boolean allVersions;
    private UUID profileDatasetId;
    private List<String> organisations;
    private Integer role;
    private List<UUID> grants;
    private List<UUID> collaborators;
    private List<UUID> datasetTemplates;
    private List<UUID> groupIds;
    private Boolean isPublic;
    private Short grantStatus;
    private boolean hasDoi;

    public boolean getAllVersions() {
        return allVersions;
    }
    public void setAllVersions(boolean allVersions) {
        this.allVersions = allVersions;
    }

    public Integer getStatus() {
        return status;
    }
    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getPeriodStart() {
        return periodStart;
    }
    public void setPeriodStart(Date periodStart) {
        this.periodStart = periodStart;
    }

    public Date getPeriodEnd() {
        return periodEnd;
    }
    public void setPeriodEnd(Date periodEnd) {
        this.periodEnd = periodEnd;
    }

    public List<UUID> getDmpIds() {
        return dmpIds;
    }
    public void setDmpIds(List<UUID> dmpIds) {
        this.dmpIds = dmpIds;
    }

    public List<Tag> getTags() {
        return tags;
    }
    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public UUID getProfileDatasetId() {
        return profileDatasetId;
    }
    public void setProfileDatasetId(UUID profileDatasetId) {
        this.profileDatasetId = profileDatasetId;
    }

    public List<String> getOrganisations() {
        return organisations;
    }
    public void setOrganisations(List<String> organisations) {
        this.organisations = organisations;
    }

    public Integer getRole() {
        return role;
    }
    public void setRole(Integer role) {
        this.role = role;
    }

    public List<UUID> getGrants() {
        return grants;
    }
    public void setGrants(List<UUID> grants) {
        this.grants = grants;
    }

    public List<UUID> getCollaborators() {
        return collaborators;
    }
    public void setCollaborators(List<UUID> collaborators) {
        this.collaborators = collaborators;
    }

    public List<UUID> getDatasetTemplates() {
        return datasetTemplates;
    }
    public void setDatasetTemplates(List<UUID> datasetTemplates) {
        this.datasetTemplates = datasetTemplates;
    }

    public List<UUID> getGroupIds() {
        return groupIds;
    }
    public void setGroupIds(List<UUID> groupIds) {
        this.groupIds = groupIds;
    }

    public Boolean getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(Boolean isPublic) {
        this.isPublic = isPublic;
    }

    public Short getGrantStatus() {
        return grantStatus;
    }

    public void setGrantStatus(Short grantStatus) {
        this.grantStatus = grantStatus;
    }

    public boolean hasDoi() {
        return hasDoi;
    }

    public void setHasDoi(boolean hasDoi) {
        this.hasDoi = hasDoi;
    }
}
