package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccessLayer;
import eu.eudat.data.dao.criteria.GrantCriteria;
import eu.eudat.data.entities.Grant;
import eu.eudat.data.entities.UserInfo;
import eu.eudat.queryable.QueryableList;

import java.util.UUID;

public interface GrantDao extends DatabaseAccessLayer<Grant, UUID> {

    QueryableList<Grant> getWithCriteria(GrantCriteria criteria);

    QueryableList<Grant> getAuthenticated(QueryableList<Grant> query, UserInfo principal);

}