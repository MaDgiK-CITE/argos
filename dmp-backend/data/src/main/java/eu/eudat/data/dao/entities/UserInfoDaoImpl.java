package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccess;
import eu.eudat.data.dao.criteria.UserInfoCriteria;
import eu.eudat.data.dao.databaselayer.service.DatabaseService;
import eu.eudat.data.entities.UserInfo;
import eu.eudat.queryable.QueryableList;
import eu.eudat.queryable.types.FieldSelectionType;
import eu.eudat.queryable.types.SelectionField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Component("userInfoDao")
public class UserInfoDaoImpl extends DatabaseAccess<UserInfo> implements UserInfoDao {

	@Autowired
	public UserInfoDaoImpl(DatabaseService<UserInfo> databaseService) {
		super(databaseService);
	}

	@Override
	public QueryableList<UserInfo> getWithCriteria(UserInfoCriteria criteria) {
		QueryableList<UserInfo> users = this.getDatabaseService().getQueryable(UserInfo.class);
		users.where(((builder, root) -> builder.equal(root.get("userStatus"), 0)));
		if (criteria.getAppRoles() != null && !criteria.getAppRoles().isEmpty())
			users.where((builder, root) -> root.join("userRoles").get("role").in(criteria.getAppRoles()));
		if (criteria.getLike() != null)
			users.where((builder, root) -> builder.or(builder.like(builder.upper(root.get("name")), "%" + criteria.getLike().toUpperCase() + "%"), builder.like(root.get("email"), "%" + criteria.getLike() + "%")));
		if (criteria.getEmail() != null)
			users.where((builder, root) -> builder.equal(root.get("email"), criteria.getEmail()));
		if (criteria.getCollaboratorLike() != null)
			users.where((builder, root) -> builder.or(builder.like(builder.upper(root.get("name")), "%" + criteria.getCollaboratorLike().toUpperCase() + "%"), builder.like(root.get("email"), "%" + criteria.getCollaboratorLike() + "%")));
		return users;
	}

	@Override
	public QueryableList<UserInfo> getAuthenticated(QueryableList<UserInfo> users, UUID principalId) {
		users.initSubQuery(UUID.class).where((builder, root) ->
				builder.and(root.join("dmps").get("id").in(
						users.subQuery((builder1, root1) -> builder1.equal(root1.get("id"), principalId),
								Arrays.asList(new SelectionField(FieldSelectionType.COMPOSITE_FIELD, "dmps:id")))),
						builder.notEqual(root.get("id"), principalId)));
		return users;
	}

	@Override
	public UserInfo createOrUpdate(UserInfo item) {
		return this.getDatabaseService().createOrUpdate(item, UserInfo.class);
	}

	@Override
	public UserInfo find(UUID id) {
		return this.getDatabaseService().getQueryable(UserInfo.class).where((builder, root) -> builder.equal(root.get("id"), id)).getSingle();
	}

	@Override
	public void delete(UserInfo item) {
		this.getDatabaseService().delete(item);
	}

	@Override
	public QueryableList<UserInfo> asQueryable() {
		return this.getDatabaseService().getQueryable(UserInfo.class);
	}

	@Async
	@Override
	public CompletableFuture<UserInfo> createOrUpdateAsync(UserInfo item) {
		return CompletableFuture.supplyAsync(() -> this.createOrUpdate(item));
	}

	@Override
	public UserInfo find(UUID id, String hint) {
		throw new UnsupportedOperationException();
	}
}