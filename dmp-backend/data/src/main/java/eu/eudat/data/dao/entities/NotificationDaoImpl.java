package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccess;
import eu.eudat.data.dao.criteria.NotificationCriteria;
import eu.eudat.data.dao.databaselayer.service.DatabaseService;
import eu.eudat.data.entities.Lock;
import eu.eudat.data.entities.Notification;
import eu.eudat.queryable.QueryableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Service("NotificationDao")
public class NotificationDaoImpl extends DatabaseAccess<Notification> implements NotificationDao {
	@Autowired
	public NotificationDaoImpl(DatabaseService<Notification> databaseService) {
		super(databaseService);
	}

	@Override
	public QueryableList<Notification> getWithCriteria(NotificationCriteria criteria) {
		QueryableList<Notification> query = this.getDatabaseService().getQueryable(Notification.class);
		if (criteria.getIsActive() != null)
			query.where((builder, root) -> builder.equal(root.get("isActive"), criteria.getIsActive()));
		if (criteria.getNotifyState() != null)
			query.where(((builder, root) -> builder.equal(root.get("notifyState"), criteria.getNotifyState())));
		return query;
	}

	@Override
	public Notification createOrUpdate(Notification item) {
		return this.getDatabaseService().createOrUpdate(item, Notification.class);
	}

	@Override
	public CompletableFuture<Notification> createOrUpdateAsync(Notification item) {
		return CompletableFuture.supplyAsync(() -> this.getDatabaseService().createOrUpdate(item, Notification.class));
	}

	@Override
	public Notification find(UUID id) {
		return this.getDatabaseService().getQueryable(Notification.class).where(((builder, root) -> builder.equal(root.get("id"), id))).getSingle();
	}

	@Override
	public Notification find(UUID id, String hint) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void delete(Notification item) {
		this.getDatabaseService().delete(item);
	}

	@Override
	public QueryableList<Notification> asQueryable() {
		return this.getDatabaseService().getQueryable(Notification.class);
	}
}
