package eu.eudat.data.dao.criteria;

/**
 * Created by ikalyvas on 3/26/2018.
 */
public class RequestItem<T> {
    T criteria;

    public T getCriteria() {
        return criteria;
    }

    public void setCriteria(T criteria) {
        this.criteria = criteria;
    }
}
