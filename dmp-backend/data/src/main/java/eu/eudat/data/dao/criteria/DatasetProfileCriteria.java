package eu.eudat.data.dao.criteria;


import eu.eudat.data.entities.DescriptionTemplate;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public class DatasetProfileCriteria extends Criteria<DescriptionTemplate> {

    public enum DatasetProfileFilter {
        DMPs((short) 0), Datasets((short) 1);

        private short value;
        private DatasetProfileFilter(short value) {
            this.value = value;
        }
        public short getValue() { return value; }

        public static DatasetProfileFilter fromInteger(short value) {
            switch (value) {
                case 0:
                    return DMPs;
                case 1:
                    return Datasets;
                default:
                    throw new RuntimeException("Unsupported DescriptionTemplate filter");
            }
        }
    }

    private boolean allVersions;
    private List<UUID> groupIds;
    private Short filter;
    private UUID userId;
    private boolean finalized;
    private Integer status;
    private Integer role;
    private List<UUID> ids;
    private Date periodStart;

    public boolean getAllVersions() { return allVersions; }
    public void setAllVersions(boolean allVersions) { this.allVersions = allVersions; }

    public List<UUID> getGroupIds() { return groupIds; }
    public void setGroupIds(List<UUID> groupIds) { this.groupIds = groupIds; }

    public Short getFilter() {
        return filter;
    }
    public void setFilter(Short filter) {
        this.filter = filter;
    }

    public UUID getUserId() {
        return userId;
    }
    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public boolean getFinalized() {
        return finalized;
    }
    public void setFinalized(boolean finalized) {
        this.finalized = finalized;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    public List<UUID> getIds() {
        return ids;
    }

    public void setIds(List<UUID> ids) {
        this.ids = ids;
    }

    public Date getPeriodStart() {
        return periodStart;
    }

    public void setPeriodStart(Date periodStart) {
        this.periodStart = periodStart;
    }
}
