package eu.eudat.data.query.items.item.dataset;

import eu.eudat.data.dao.criteria.DatasetWizardUserDmpCriteria;
import eu.eudat.data.entities.DMP;
import eu.eudat.data.query.definition.Query;
import eu.eudat.queryable.QueryableList;

import java.util.UUID;


public class DatasetWizardAutocompleteRequest extends Query<DatasetWizardUserDmpCriteria, DMP> {
    @Override
    public QueryableList<DMP> applyCriteria() {
        QueryableList<DMP> query = this.getQuery().where((builder, root) -> builder.or(builder.equal(root.get("creator"), this.getCriteria().getUserInfo()), builder.isMember(this.getCriteria().getUserInfo(), root.get("users"))));
        if (this.getCriteria().getLike() != null && !this.getCriteria().getLike().isEmpty()) {
            query.where((builder, root) -> builder.like(builder.upper(root.get("label")), "%" + this.getCriteria().getLike().toUpperCase() + "%"));
        }
        return query;
    }
}
