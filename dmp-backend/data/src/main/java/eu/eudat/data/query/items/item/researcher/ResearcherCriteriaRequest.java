package eu.eudat.data.query.items.item.researcher;

import eu.eudat.data.dao.criteria.ResearcherCriteria;
import eu.eudat.data.entities.Researcher;
import eu.eudat.data.query.definition.Query;
import eu.eudat.queryable.QueryableList;

import java.util.UUID;

/**
 * Created by ikalyvas on 3/6/2018.
 */
public class ResearcherCriteriaRequest extends Query<ResearcherCriteria,Researcher> {
    @Override
    public QueryableList<Researcher> applyCriteria() {
        QueryableList<Researcher> query = this.getQuery();
        if (this.getCriteria().getLike() != null)
            query.where((builder, root) -> builder.equal(root.get("reference"), this.getCriteria().getLike()));
        if (this.getCriteria().getName() != null)
            query.where((builder, root) -> builder.equal(root.get("label"), this.getCriteria().getName()));
        return query;
    }
}
