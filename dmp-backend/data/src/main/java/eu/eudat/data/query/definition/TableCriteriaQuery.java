package eu.eudat.data.query.definition;

import eu.eudat.data.dao.criteria.Criteria;
import eu.eudat.queryable.QueryableList;
import eu.eudat.queryable.queryableentity.DataEntity;

/**
 * Created by ikalyvas on 3/21/2018.
 */
public interface TableCriteriaQuery<C extends Criteria<T>, T extends DataEntity> extends CriteriaQuery<C, T> {
    QueryableList<T> applyPaging(QueryableList<T> items) ;
}
