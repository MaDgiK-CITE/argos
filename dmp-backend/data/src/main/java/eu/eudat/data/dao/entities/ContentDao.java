package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccessLayer;
import eu.eudat.data.entities.Content;

import java.util.UUID;

/**
 * Created by ikalyvas on 3/16/2018.
 */
public interface ContentDao extends DatabaseAccessLayer<Content, UUID> {
}
