package eu.eudat.data.entities.helpers;

import javax.persistence.Tuple;
import java.util.List;

public class EntityBinder {
	public static <T> T fromTuple(List<Tuple> tuple, String path) {
		try {
			return (T) tuple.get(0).get(path);
		}catch (IllegalArgumentException illegalArgument){
			return null;
		}
	}
}
