package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccessLayer;
import eu.eudat.data.dao.criteria.DataRepositoryCriteria;
import eu.eudat.data.entities.DataRepository;
import eu.eudat.queryable.QueryableList;

import java.util.UUID;

public interface DataRepositoryDao extends DatabaseAccessLayer<DataRepository, UUID> {
    QueryableList<DataRepository> getWithCriteria(DataRepositoryCriteria criteria);
}