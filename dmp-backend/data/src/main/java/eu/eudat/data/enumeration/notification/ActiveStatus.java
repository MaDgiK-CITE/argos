package eu.eudat.data.enumeration.notification;

public enum ActiveStatus {
	ACTIVE(0),
	INACTIVE(1);

	private int status;

	ActiveStatus(int status) {
		this.status = status;
	}

	public int getStatus() {
		return status;
	}

	public ActiveStatus fromInteger(int status) {
		switch (status) {
			case 0:
				return ACTIVE;
			case 1:
				return INACTIVE;
			default:
				throw new RuntimeException("Unsupported Active Status");
		}
	}
}
