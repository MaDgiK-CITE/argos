package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccessLayer;
import eu.eudat.data.dao.criteria.RegistryCriteria;
import eu.eudat.data.entities.Registry;
import eu.eudat.queryable.QueryableList;

import java.util.UUID;

public interface RegistryDao extends DatabaseAccessLayer<Registry, UUID> {

    QueryableList<Registry> getWithCriteria(RegistryCriteria criteria);

}