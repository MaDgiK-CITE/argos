package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccess;
import eu.eudat.data.dao.criteria.InvitationCriteria;
import eu.eudat.data.dao.databaselayer.service.DatabaseService;
import eu.eudat.data.entities.Invitation;
import eu.eudat.queryable.QueryableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;


@Service("invitationDao")
public class InvitationDaoImpl extends DatabaseAccess<Invitation> implements InvitationDao {

    @Autowired
    public InvitationDaoImpl(DatabaseService<Invitation> databaseService) {
        super(databaseService);
    }

    @Override
    public QueryableList<Invitation> getWithCriteria(InvitationCriteria criteria) {
        return null;
    }

    @Override
    public Invitation createOrUpdate(Invitation item) {
        return this.getDatabaseService().createOrUpdate(item, Invitation.class);
    }

    @Override
    public Invitation find(UUID id) {
        return this.getDatabaseService().getQueryable(Invitation.class).where((builder, root) -> builder.equal(root.get("id"), id)).getSingle();
    }

    @Override
    public void delete(Invitation item) {
        this.getDatabaseService().delete(item);
    }

    @Override
    public QueryableList<Invitation> asQueryable() {
        return this.getDatabaseService().getQueryable(Invitation.class);
    }

    @Async
    @Override
    public CompletableFuture<Invitation> createOrUpdateAsync(Invitation item) {
        return CompletableFuture.supplyAsync(() -> this.createOrUpdate(item));
    }

    @Override
    public Invitation find(UUID id, String hint) {
        throw new UnsupportedOperationException();
    }
}
