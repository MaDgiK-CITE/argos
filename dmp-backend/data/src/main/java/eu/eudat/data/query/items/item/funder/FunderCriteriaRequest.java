package eu.eudat.data.query.items.item.funder;

import eu.eudat.data.dao.criteria.FunderCriteria;
import eu.eudat.data.entities.Funder;
import eu.eudat.data.query.definition.Query;
import eu.eudat.queryable.QueryableList;

public class FunderCriteriaRequest extends Query<FunderCriteria, Funder> {
	@Override
	public QueryableList<Funder> applyCriteria() {
		QueryableList<Funder> query = this.getQuery();
		if (this.getCriteria().getLike() != null && !this.getCriteria().getLike().isEmpty())
			query.where((builder, root) -> builder.like(builder.upper(root.get("label")), "%" + this.getCriteria().getLike().toUpperCase() + "%"));
		query.where((builder, root) -> builder.notEqual(root.get("status"), Funder.Status.DELETED.getValue()));
		return query;
	}
}
