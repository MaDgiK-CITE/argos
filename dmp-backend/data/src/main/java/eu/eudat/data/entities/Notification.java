package eu.eudat.data.entities;

import eu.eudat.data.enumeration.notification.ActiveStatus;
import eu.eudat.data.enumeration.notification.ContactType;
import eu.eudat.data.enumeration.notification.NotificationType;
import eu.eudat.data.enumeration.notification.NotifyState;
import eu.eudat.queryable.queryableentity.DataEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "\"Notification\"")
public class Notification implements DataEntity<Notification, UUID> {

	@Id
	@GeneratedValue
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(name = "id", updatable = false, nullable = false, columnDefinition = "BINARY(16)")
	private UUID id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "\"UserId\"")
	private UserInfo userId;

	@Enumerated
	@Column(name = "\"IsActive\"", nullable = false)
	private ActiveStatus isActive;

	@Enumerated
	@Column(name = "\"Type\"", nullable = false)
	private NotificationType type;

	@Enumerated
	@Column(name = "\"ContactTypeHint\"")
	private ContactType contactTypeHint;

	@Column(name = "\"ContactHint\"")
	private String contactHint;

	@Column(name = "\"Data\"")
	private String data;

	@Enumerated
	@Column(name = "\"NotifyState\"")
	private NotifyState notifyState;

	@Column(name = "\"NotifiedAt\"")
	private Date notifiedAt;

	@Column(name = "\"RetryCount\"")
	private Integer retryCount;

	@Column(name = "\"CreatedAt\"")
	private Date createdAt;

	@Column(name = "\"UpdatedAt\"")
	private Date updatedAt;


	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public UserInfo getUserId() {
		return userId;
	}

	public void setUserId(UserInfo userId) {
		this.userId = userId;
	}

	public ActiveStatus getIsActive() {
		return isActive;
	}

	public void setIsActive(ActiveStatus isActive) {
		this.isActive = isActive;
	}

	public NotificationType getType() {
		return type;
	}

	public void setType(NotificationType type) {
		this.type = type;
	}

	public ContactType getContactTypeHint() {
		return contactTypeHint;
	}

	public void setContactTypeHint(ContactType contactTypeHint) {
		this.contactTypeHint = contactTypeHint;
	}

	public String getContactHint() {
		return contactHint;
	}

	public void setContactHint(String contactHint) {
		this.contactHint = contactHint;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public NotifyState getNotifyState() {
		return notifyState;
	}

	public void setNotifyState(NotifyState notifyState) {
		this.notifyState = notifyState;
	}

	public Date getNotifiedAt() {
		return notifiedAt;
	}

	public void setNotifiedAt(Date notifiedAt) {
		this.notifiedAt = notifiedAt;
	}

	public Integer getRetryCount() {
		return retryCount;
	}

	public void setRetryCount(Integer retryCount) {
		this.retryCount = retryCount;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Override
	public void update(Notification entity) {
	}

	@Override
	public UUID getKeys() {
		return null;
	}

	@Override
	public Notification buildFromTuple(List<Tuple> tuple, List<String> fields, String base) {
		return null;
	}


}
