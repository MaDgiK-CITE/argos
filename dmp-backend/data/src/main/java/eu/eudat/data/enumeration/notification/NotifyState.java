package eu.eudat.data.enumeration.notification;

public enum NotifyState {
	PENDING(0),
	PROCESSING(1),
	SENDING(2),
	SUCCEEDED(3),
	ERROR(4);

	private int state;

	NotifyState(int state) {
		this.state = state;
	}

	public int getState() {
		return state;
	}

	public NotifyState fromInteger(int state) {
		switch (state) {
			case 0:
				return PENDING;
			case 1:
				return PROCESSING;
			case 2:
				return SENDING;
			case 3:
				return SUCCEEDED;
			case 4:
				return ERROR;
			default:
				throw new RuntimeException("Unsupported Notify State");
		}
	}
}
