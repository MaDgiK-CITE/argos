package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccessLayer;
import eu.eudat.data.entities.DatasetService;

import java.util.UUID;

/**
 * Created by ikalyvas on 5/22/2018.
 */
public interface DatasetServiceDao extends DatabaseAccessLayer<DatasetService, UUID> {
}
