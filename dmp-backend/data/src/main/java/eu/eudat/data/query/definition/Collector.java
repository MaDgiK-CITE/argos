package eu.eudat.data.query.definition;

import eu.eudat.queryable.QueryableList;
import eu.eudat.queryable.queryableentity.DataEntity;

/**
 * Created by ikalyvas on 3/21/2018.
 */
public interface Collector<T extends DataEntity> {
    QueryableList<T> collect() throws Exception;

    QueryableList<T> collect(QueryableList<T> repo) throws Exception;

}
