package eu.eudat.data.entities;


import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;


@Entity
@Table(name = "\"DatasetProfileRuleset\"")
public class DatasetProfileRuleset {

    @Id
    @GeneratedValue
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "\"ID\"", updatable = false, nullable = false, columnDefinition = "BINARY(16)")
    private UUID id;


    @Column(name = "\"Label\"")
    private String label;

    @Type(type = "eu.eudat.configurations.typedefinition.XMLType")
    @Column(name = "\"Definition\"", columnDefinition = "xml", nullable = false)
    private String definition;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDefinition() {
//		return XML.toJSONObject(definition).toString(); //return definition as json
        return definition;
    }

    public void setDefinition(String definition) {
//		this.definition = XML.toString(definition); //if definition is in json
        this.definition = definition;
    }


}
