package eu.eudat.data.dao.criteria;

import eu.eudat.data.entities.Lock;
import eu.eudat.data.entities.UserInfo;

import java.util.Date;
import java.util.UUID;

public class LockCriteria extends Criteria<Lock> {

	private UUID target;
	private UserInfo lockedBy;
	private Date touchedAt;

	public UUID getTarget() {
		return target;
	}

	public void setTarget(UUID target) {
		this.target = target;
	}

	public UserInfo getLockedBy() {
		return lockedBy;
	}

	public void setLockedBy(UserInfo lockedBy) {
		this.lockedBy = lockedBy;
	}

	public Date getTouchedAt() {
		return touchedAt;
	}

	public void setTouchedAt(Date touchedAt) {
		this.touchedAt = touchedAt;
	}
}
