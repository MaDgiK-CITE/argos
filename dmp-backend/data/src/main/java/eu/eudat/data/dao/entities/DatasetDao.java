package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccessLayer;
import eu.eudat.data.dao.criteria.DatasetCriteria;
import eu.eudat.data.entities.Dataset;
import eu.eudat.data.entities.UserInfo;
import eu.eudat.queryable.QueryableList;

import java.util.List;
import java.util.UUID;

public interface DatasetDao extends DatabaseAccessLayer<Dataset, UUID> {

    QueryableList<Dataset> getWithCriteria(DatasetCriteria criteria);

    QueryableList<Dataset> filterFromElastic(DatasetCriteria criteria, List<UUID> ids);

    QueryableList<Dataset> getAuthenticated(QueryableList<Dataset> query, UserInfo principal, List<Integer> roles);

    Dataset isPublicDataset(UUID id);

}