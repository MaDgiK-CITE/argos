package eu.eudat.data.dao.criteria;

import eu.eudat.data.enumeration.notification.ActiveStatus;
import eu.eudat.data.enumeration.notification.NotifyState;

public class NotificationCriteria {

	private ActiveStatus isActive;
	private NotifyState notifyState;

	public ActiveStatus getIsActive() {
		return isActive;
	}

	public void setIsActive(ActiveStatus isActive) {
		this.isActive = isActive;
	}

	public NotifyState getNotifyState() {
		return notifyState;
	}

	public void setNotifyState(NotifyState notifyState) {
		this.notifyState = notifyState;
	}
}
