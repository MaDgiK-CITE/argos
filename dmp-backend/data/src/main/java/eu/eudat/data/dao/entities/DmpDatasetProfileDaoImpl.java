package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccess;
import eu.eudat.data.dao.databaselayer.service.DatabaseService;
import eu.eudat.data.entities.Content;
import eu.eudat.data.entities.DMPDatasetProfile;
import eu.eudat.queryable.QueryableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Service("dmpDatasetProfileDao")
public class DmpDatasetProfileDaoImpl extends DatabaseAccess<DMPDatasetProfile> implements DmpDatasetProfileDao {
    @Autowired
    public DmpDatasetProfileDaoImpl(DatabaseService<DMPDatasetProfile> databaseService) {
        super(databaseService);
    }

    @Override
    public DMPDatasetProfile createOrUpdate(DMPDatasetProfile item) {
        return this.getDatabaseService().createOrUpdate(item, DMPDatasetProfile.class);
    }

    @Override
    @Async
    public CompletableFuture<DMPDatasetProfile> createOrUpdateAsync(DMPDatasetProfile item) {
        return CompletableFuture.supplyAsync(() -> this.createOrUpdate(item));
    }

    @Override
    public DMPDatasetProfile find(UUID id) {
        return this.getDatabaseService().getQueryable(DMPDatasetProfile.class).where((builder, root) -> builder.equal(root.get("id"), id)).getSingle();
    }

    @Override
    public DMPDatasetProfile find(UUID id, String hint) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(DMPDatasetProfile item) {
        this.getDatabaseService().delete(item);
    }

    @Override
    public QueryableList<DMPDatasetProfile> asQueryable() {
        return this.getDatabaseService().getQueryable(DMPDatasetProfile.class);
    }
}
