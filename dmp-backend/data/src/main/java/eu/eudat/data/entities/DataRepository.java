package eu.eudat.data.entities;


import eu.eudat.data.entities.helpers.EntityBinder;
import eu.eudat.queryable.queryableentity.DataEntity;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "\"DataRepository\"")
public class DataRepository implements Serializable, DataEntity<DataRepository, UUID> {

	@Id
	@Column(name = "\"ID\"", updatable = false, nullable = false, columnDefinition = "BINARY(16)")
	private UUID id;

	@Column(name = "\"Label\"")
	private String label;

	@Column(name = "\"Abbreviation\"")
	private String abbreviation;

	@Column(name = "\"Reference\"")
	private String reference;

	@Column(name = "\"Uri\"")
	private String uri;

	@Type(type = "eu.eudat.configurations.typedefinition.XMLType")
	@Column(name = "\"Definition\"", columnDefinition = "xml")
	private String definition;

	@OneToMany(mappedBy = "dataRepository", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<DatasetDataRepository> datasetDataRepositories;

	@Column(name = "\"Status\"", nullable = false)
	private Short status;

	@Column(name = "\"Created\"", nullable = false)
	private Date created = null;

	@Column(name = "\"Modified\"", nullable = false)
	private Date modified = new Date();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "\"CreationUser\"")
	private UserInfo creationUser;


	public Short getStatus() {
		return status;
	}
	public void setStatus(Short status) {
		this.status = status;
	}

	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getModified() {
		return modified;
	}
	public void setModified(Date modified) {
		this.modified = modified;
	}

	public UUID getId() {
		return id;
	}
	public void setId(UUID id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}

	public String getAbbreviation() {
		return abbreviation;
	}
	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getDefinition() {
		return definition;
	}
	public void setDefinition(String definition) {
		this.definition = definition;
	}

	public Set<DatasetDataRepository> getDatasetDataRepositories() {
		return datasetDataRepositories;
	}
	public void setDatasetDataRepositories(Set<DatasetDataRepository> datasetDataRepositories) {
		this.datasetDataRepositories = datasetDataRepositories;
	}

	public UserInfo getCreationUser() {
		return creationUser;
	}
	public void setCreationUser(UserInfo creationUser) {
		this.creationUser = creationUser;
	}

	@Override
	public void update(DataRepository entity) {
		this.label = entity.getLabel();
		this.abbreviation = entity.getAbbreviation();
		this.uri = entity.getUri();

	}

	@Override
	public UUID getKeys() {
		return this.id;
	}

	@Override
	public DataRepository buildFromTuple(List<Tuple> tuple, List<String> fields, String base) {
		String currentBase = base.isEmpty() ? "" : base + ".";
		if (fields.contains(currentBase + "id")) this.id = EntityBinder.fromTuple(tuple, currentBase + "id");
		return this;
	}
}
