package eu.eudat.data.dao.criteria;

import eu.eudat.data.entities.DescriptionTemplate;

import java.util.UUID;


public class DatasetProfileWizardCriteria extends Criteria<DescriptionTemplate> {
    private UUID id;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}
