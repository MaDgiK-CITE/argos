package eu.eudat.data.query.items.dmpblueprint;

import eu.eudat.data.dao.criteria.DataManagementPlanBlueprintCriteria;
import eu.eudat.data.entities.DMPProfile;
import eu.eudat.data.query.PaginationService;
import eu.eudat.data.query.definition.TableQuery;
import eu.eudat.queryable.QueryableList;

import java.util.UUID;

public class DataManagementPlanBlueprintTableRequest extends TableQuery<DataManagementPlanBlueprintCriteria, DMPProfile, UUID> {

    @Override
    public QueryableList<DMPProfile> applyCriteria() {
        QueryableList<DMPProfile> query = this.getQuery();
        if (this.getCriteria().getLike() != null && !this.getCriteria().getLike().isEmpty())
            query.where((builder, root) -> builder.like(root.get("label"), "%" + this.getCriteria().getLike() + "%"));
        if (this.getCriteria().getStatus() != null)
            query.where((builder, root) -> builder.equal(root.get("status"), this.getCriteria().getStatus()));
        return query;
    }

    @Override
    public QueryableList<DMPProfile> applyPaging(QueryableList<DMPProfile> items) {
        return PaginationService.applyPaging(items, this);
    }
}
