package eu.eudat.data.query.items.table.dmpprofile;

import eu.eudat.data.dao.criteria.DataManagementPlanProfileCriteria;
import eu.eudat.data.entities.DMPProfile;
import eu.eudat.data.query.PaginationService;
import eu.eudat.data.query.definition.TableQuery;
import eu.eudat.queryable.QueryableList;

import java.util.UUID;

/**
 * Created by ikalyvas on 3/21/2018.
 */
public class DataManagementPlanProfileTableRequest extends TableQuery<DataManagementPlanProfileCriteria, DMPProfile, UUID> {

    @Override
    public QueryableList<DMPProfile> applyCriteria() {
        QueryableList<DMPProfile> query = this.getQuery();
        if (this.getCriteria().getLike() != null && !this.getCriteria().getLike().isEmpty())
            query.where((builder, root) -> builder.like(root.get("label"), "%" + this.getCriteria().getLike() + "%"));
        return query;
    }

    @Override
    public QueryableList<DMPProfile> applyPaging(QueryableList<DMPProfile> items) {
        return PaginationService.applyPaging(items, this);
    }
}
