package eu.eudat.data.dao.databaselayer.service;


import eu.eudat.data.dao.databaselayer.context.DatabaseContext;
import eu.eudat.queryable.QueryableList;
import eu.eudat.queryable.queryableentity.DataEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Set;


@Service("databaseService")
public class DatabaseService<T extends DataEntity> {

    private DatabaseContext<T> databaseCtx;

    @Autowired
    public DatabaseService(DatabaseContext<T> databaseCtx) {
        this.databaseCtx = databaseCtx;
    }

    public QueryableList<T> getQueryable(Class<T> tClass) {
        return this.databaseCtx.getQueryable(tClass);
    }

    public QueryableList<T> getQueryable(Set<String> hints, Class<T> tClass) {
        return this.databaseCtx.getQueryable(tClass);
    }

    @Transactional
    public T createOrUpdate(T item, Class<T> tClass) {
        return this.databaseCtx.createOrUpdate(item, tClass);
    }

    public void delete(T item) {
        this.databaseCtx.delete(item);
    }

}
