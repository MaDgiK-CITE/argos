package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccess;
import eu.eudat.data.dao.databaselayer.service.DatabaseService;
import eu.eudat.data.entities.DatasetExternalDataset;
import eu.eudat.queryable.QueryableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

/**
 * Created by ikalyvas on 5/22/2018.
 */
@Component("datasetExternalDatasetDao")
public class DatasetExternalDatasetDaoImpl extends DatabaseAccess<DatasetExternalDataset> implements DatasetExternalDatasetDao {

    @Autowired
    public DatasetExternalDatasetDaoImpl(DatabaseService<DatasetExternalDataset> databaseService) {
        super(databaseService);
    }

    @Override
    public DatasetExternalDataset createOrUpdate(DatasetExternalDataset item) {
        return this.getDatabaseService().createOrUpdate(item,DatasetExternalDataset.class);
    }

    @Override
    public CompletableFuture<DatasetExternalDataset> createOrUpdateAsync(DatasetExternalDataset item) {
        return CompletableFuture.supplyAsync(() -> this.createOrUpdate(item));
    }

    @Override
    @Async
    public DatasetExternalDataset find(UUID id) {
        return getDatabaseService().getQueryable(DatasetExternalDataset.class).where((builder, root) -> builder.equal(root.get("id"), id)).getSingle();
    }

    @Override
    public DatasetExternalDataset find(UUID id, String hint) {
        throw new UnsupportedOperationException();
    }


    @Override
    public void delete(DatasetExternalDataset item) {
        this.getDatabaseService().delete(item);
    }

    @Override
    public QueryableList<DatasetExternalDataset> asQueryable() {
        return this.getDatabaseService().getQueryable(DatasetExternalDataset.class);
    }
}
