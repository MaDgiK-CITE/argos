package eu.eudat.data.dao;

import eu.eudat.data.dao.databaselayer.service.DatabaseService;
import eu.eudat.queryable.queryableentity.DataEntity;


public class DatabaseAccess<T extends DataEntity> {

    public DatabaseAccess(DatabaseService<T> databaseService) {
        this.databaseService = databaseService;
    }

    private DatabaseService<T> databaseService;

    public DatabaseService<T> getDatabaseService() {
        return databaseService;
    }

    public void setDatabaseService(DatabaseService<T> databaseService) {
        this.databaseService = databaseService;
    }
}
