package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccessLayer;
import eu.eudat.data.dao.criteria.DataManagementPlanCriteria;
import eu.eudat.data.dao.criteria.DatasetWizardUserDmpCriteria;
import eu.eudat.data.entities.DMP;
import eu.eudat.data.entities.UserDMP;
import eu.eudat.data.entities.UserInfo;
import eu.eudat.queryable.QueryableList;

import java.util.List;
import java.util.UUID;

public interface DMPDao extends DatabaseAccessLayer<DMP, UUID> {

    QueryableList<DMP> getWithCriteria(DataManagementPlanCriteria criteria);

    QueryableList<DMP> getUserDmps(DatasetWizardUserDmpCriteria datasetWizardAutocompleteRequest, UserInfo userInfo);

    QueryableList<DMP> getAuthenticated(QueryableList<DMP> query, UUID principalId, List<Integer> roles);

}