package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccessLayer;
import eu.eudat.data.dao.criteria.ResearcherCriteria;
import eu.eudat.data.entities.Researcher;
import eu.eudat.queryable.QueryableList;

import java.util.UUID;

public interface ResearcherDao extends DatabaseAccessLayer<Researcher, UUID> {

    QueryableList<Researcher> getWithCriteria(ResearcherCriteria criteria);

}