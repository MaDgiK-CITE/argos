package eu.eudat.data.entities;

import eu.eudat.data.entities.helpers.EntityBinder;
import eu.eudat.queryable.queryableentity.DataEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "\"UserDMP\"")
public class UserDMP implements DataEntity<UserDMP, UUID> {

	public enum UserDMPRoles {
		OWNER(0), USER(1);

		private Integer value;

		private UserDMPRoles(Integer value) {
			this.value = value;
		}

		public Integer getValue() {
			return value;
		}

		public static List<Integer> getAllValues() {
			List<Integer> list = new LinkedList<>();
			for (Enum en : UserDMP.UserDMPRoles.values()) {
				list.add(((UserDMPRoles) en).value);
			}
			return list;
		}

		public static UserDMPRoles fromInteger(Integer value) {
			switch (value) {
				case 0:
					return OWNER;
				case 1:
					return USER;
				default:
					throw new RuntimeException("Unsupported User Dmp Role Message Code");
			}
		}
	}


	@Id
	@GeneratedValue
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(name = "id", updatable = false, nullable = false, columnDefinition = "BINARY(16)")
	private UUID id;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "usr")
	private UserInfo user;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "dmp")
	private DMP dmp;

	@Column(name = "role")
	private Integer role;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public UserInfo getUser() {
		return user;
	}

	public void setUser(UserInfo user) {
		this.user = user;
	}

	public DMP getDmp() {
		return dmp;
	}

	public void setDmp(DMP dmp) {
		this.dmp = dmp;
	}

	public Integer getRole() {
		return role;
	}

	public void setRole(Integer role) {
		this.role = role;
	}

	@Override
	public void update(UserDMP entity) {
		this.role = entity.getRole();
	}

	@Override
	public UUID getKeys() {
		return this.id;
	}

	@Override
	public UserDMP buildFromTuple(List<Tuple> tuple, List<String> fields, String base) {
		String currentBase = base.isEmpty() ? "" : base + ".";
		if (fields.contains(currentBase + "id")) this.id = EntityBinder.fromTuple(tuple, currentBase + "id");
		return this;
	}
}
