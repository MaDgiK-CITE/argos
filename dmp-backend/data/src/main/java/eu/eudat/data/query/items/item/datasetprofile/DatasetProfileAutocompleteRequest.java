package eu.eudat.data.query.items.item.datasetprofile;

import eu.eudat.data.dao.criteria.DatasetProfileCriteria;
import eu.eudat.data.entities.DescriptionTemplate;
import eu.eudat.data.query.PaginationService;
import eu.eudat.data.query.definition.TableQuery;
import eu.eudat.queryable.QueryableList;

import java.util.UUID;

public class DatasetProfileAutocompleteRequest extends TableQuery<DatasetProfileCriteria, DescriptionTemplate, UUID> {
    @Override
    public QueryableList<DescriptionTemplate> applyCriteria() {
        QueryableList<DescriptionTemplate> query = this.getQuery();
        if (this.getCriteria().getLike() != null && !this.getCriteria().getLike().isEmpty())
            query.where((builder, root) -> builder.like(builder.upper(root.get("label")), "%" + this.getCriteria().getLike().toUpperCase() + "%"));
        return query;
    }

    @Override
    public QueryableList<DescriptionTemplate> applyPaging(QueryableList<DescriptionTemplate> items) {
        return PaginationService.applyPaging(items, this);
    }
}
