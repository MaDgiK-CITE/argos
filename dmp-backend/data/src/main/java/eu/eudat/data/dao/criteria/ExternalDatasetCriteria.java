package eu.eudat.data.dao.criteria;

import eu.eudat.data.entities.ExternalDataset;
import java.util.UUID;

public class ExternalDatasetCriteria extends Criteria<ExternalDataset> {
	private UUID creationUserId;

	public UUID getCreationUserId() {
		return creationUserId;
	}
	public void setCreationUserId(UUID creationUserId) {
		this.creationUserId = creationUserId;
	}
}
