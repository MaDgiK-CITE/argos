package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccessLayer;
import eu.eudat.data.entities.EntityDoi;

import java.util.UUID;

public interface EntityDoiDao extends DatabaseAccessLayer<EntityDoi, UUID> {
    EntityDoi findFromDoi(String doi);
}
