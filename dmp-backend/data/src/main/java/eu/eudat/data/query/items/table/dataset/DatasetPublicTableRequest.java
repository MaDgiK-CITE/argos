package eu.eudat.data.query.items.table.dataset;

import eu.eudat.data.dao.criteria.DatasetPublicCriteria;
import eu.eudat.data.entities.Dataset;
import eu.eudat.data.query.definition.TableQuery;
import eu.eudat.queryable.QueryableList;
import eu.eudat.queryable.types.FieldSelectionType;
import eu.eudat.queryable.types.SelectionField;
import eu.eudat.types.grant.GrantStateType;

import java.util.Arrays;
import java.util.Date;
import java.util.UUID;

/**
 * Created by ikalyvas on 10/2/2018.
 */
public class DatasetPublicTableRequest extends TableQuery<DatasetPublicCriteria, Dataset, UUID> {
    @Override
    public QueryableList<Dataset> applyCriteria() {
        QueryableList<Dataset> query = this.getQuery();
        query.where((builder, root) -> builder.equal(root.get("dmp").get("isPublic"), true));
        query.where((builder, root) -> builder.equal(root.get("status"), Dataset.Status.FINALISED.getValue()));
        query.initSubQuery(String.class).where((builder, root) -> builder.equal(root.get("dmp").get("version"),
                query.<String>subQueryMax((builder1, externalRoot, nestedRoot) -> builder1.equal(externalRoot.get("dmp").get("groupId"), nestedRoot.get("dmp").get("groupId")),
                        Arrays.asList(new SelectionField(FieldSelectionType.COMPOSITE_FIELD, "dmp:version")), String.class)));
        if (this.getCriteria().getLike() != null && !this.getCriteria().getLike().isEmpty())
            query.where((builder, root) -> builder.or(
                    builder.like(builder.upper(root.get("label")), "%" + this.getCriteria().getLike().toUpperCase() + "%"),
                    builder.like(builder.upper(root.get("description")), "%" + this.getCriteria().getLike().toUpperCase() + "%")));
        if (this.getCriteria().getGrants() != null && !this.getCriteria().getGrants().isEmpty())
            query.where(((builder, root) -> root.get("dmp").get("grant").get("id").in(this.getCriteria().getGrants())));
        if (this.getCriteria().getGrantStatus() != null) {
            if (this.getCriteria().getGrantStatus().getValue().equals(GrantStateType.FINISHED.getValue()))
                query.where((builder, root) -> builder.lessThan(root.get("dmp").get("grant").get("enddate"), new Date()));
            if (this.getCriteria().getGrantStatus().getValue().equals(GrantStateType.ONGOING.getValue()))
                query.where((builder, root) ->
                        builder.or(builder.greaterThan(root.get("dmp").get("grant").get("enddate"), new Date())
                                , builder.isNull(root.get("dmp").get("grant").get("enddate"))));
        }
        if (this.getCriteria().getDmpIds() != null && !this.getCriteria().getDmpIds().isEmpty()) {
            query.where(((builder, root) -> root.get("dmp").get("id").in(this.getCriteria().getDmpIds())));
        }
        if (this.getCriteria().getDatasetProfile() != null && !this.getCriteria().getDatasetProfile().isEmpty()) query
                .where(((builder, root) -> root.get("profile").get("id").in(this.getCriteria().getDatasetProfile())));
        if (this.getCriteria().getDmpOrganisations() != null && !this.getCriteria().getDmpOrganisations().isEmpty()) query
                .where(((builder, root) -> root.join("dmp").join("organisations").get("reference").in(this.getCriteria().getDmpOrganisations())));
        query.where((builder, root) -> builder.notEqual(root.get("status"), Dataset.Status.DELETED.getValue()));
        return query;
    }

    @Override
    public QueryableList<Dataset> applyPaging(QueryableList<Dataset> items) {
        return null;
    }
}
