package eu.eudat.data.dao.criteria;

import eu.eudat.data.entities.Dataset;
import eu.eudat.elastic.entities.Tag;
import eu.eudat.types.grant.GrantStateType;

import java.util.List;
import java.util.UUID;

/**
 * Created by ikalyvas on 10/2/2018.
 */
public class DatasetPublicCriteria extends Criteria<Dataset>{
    private GrantStateType grantStatus;
    private List<UUID> grants;
    private List<UUID> datasetProfile;
    private List<String> dmpOrganisations;
    private List<Tag> tags;
    private List<UUID> dmpIds;
    private Integer role;

    public GrantStateType getGrantStatus() {
        return grantStatus;
    }
    public void setGrantStatus(GrantStateType grantStatus) {
        this.grantStatus = grantStatus;
    }

    public List<UUID> getGrants() {
        return grants;
    }
    public void setGrants(List<UUID> grants) {
        this.grants = grants;
    }

    public List<UUID> getDatasetProfile() {
        return datasetProfile;
    }
    public void setDatasetProfile(List<UUID> datasetProfile) {
        this.datasetProfile = datasetProfile;
    }

    public List<String> getDmpOrganisations() {
        return dmpOrganisations;
    }
    public void setDmpOrganisations(List<String> dmpOrganisations) {
        this.dmpOrganisations = dmpOrganisations;
    }

    public List<Tag> getTags() {
        return tags;
    }
    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public List<UUID> getDmpIds() {
        return dmpIds;
    }
    public void setDmpIds(List<UUID> dmpIds) {
        this.dmpIds = dmpIds;
    }

    public Integer getRole() {
        return role;
    }
    public void setRole(Integer role) {
        this.role = role;
    }
}
