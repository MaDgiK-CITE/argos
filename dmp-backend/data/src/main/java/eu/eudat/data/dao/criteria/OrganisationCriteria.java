package eu.eudat.data.dao.criteria;

import eu.eudat.data.entities.Organisation;

public class OrganisationCriteria extends Criteria<Organisation> {
    private String labelLike;
    private Boolean isPublic;
    private boolean isActive;

    public String getLabelLike() {
        return labelLike;
    }
    public void setLabelLike(String labelLike) {
        this.labelLike = labelLike;
    }

    public Boolean getPublic() {
        return isPublic;
    }
    public void setPublic(Boolean aPublic) {
        isPublic = aPublic;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
