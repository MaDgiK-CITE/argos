package eu.eudat.data.query.items.table.lock;

import eu.eudat.data.dao.criteria.LockCriteria;
import eu.eudat.data.entities.Lock;
import eu.eudat.data.query.PaginationService;
import eu.eudat.data.query.definition.Query;
import eu.eudat.data.query.definition.TableQuery;
import eu.eudat.queryable.QueryableList;

import java.util.UUID;

public class LockTableRequest extends TableQuery<LockCriteria, Lock, UUID> {
	@Override
	public QueryableList<Lock> applyCriteria() {
		QueryableList<Lock> query = this.getQuery();
		if (this.getCriteria().getTouchedAt() != null)
			query.where((builder, root) -> builder.equal(root.get("touchedAt"), this.getCriteria().getTouchedAt()));
		if (this.getCriteria().getLockedBy() != null)
			query.where(((builder, root) -> builder.equal(root.get("lockedBy"), this.getCriteria().getLockedBy())));
		if (this.getCriteria().getTarget() != null)
			query.where(((builder, root) -> builder.equal(root.get("target"), this.getCriteria().getTarget())));
		return query;
	}

	@Override
	public QueryableList<Lock> applyPaging(QueryableList<Lock> items) {
		return PaginationService.applyPaging(items, this);
	}
}
