package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccessLayer;
import eu.eudat.data.dao.criteria.NotificationCriteria;
import eu.eudat.data.entities.Notification;
import eu.eudat.queryable.QueryableList;

import java.util.UUID;

public interface NotificationDao extends DatabaseAccessLayer<Notification, UUID> {

	QueryableList<Notification> getWithCriteria(NotificationCriteria criteria);
}
