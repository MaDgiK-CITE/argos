package eu.eudat.data.dao.entities.security;

import eu.eudat.data.dao.DatabaseAccess;
import eu.eudat.data.dao.databaselayer.service.DatabaseService;
import eu.eudat.data.entities.UserToken;
import eu.eudat.queryable.QueryableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;


@Component("userTokenDao")
public class UserTokenDaoImpl extends DatabaseAccess<UserToken> implements UserTokenDao {

    @Autowired
    public UserTokenDaoImpl(DatabaseService<UserToken> databaseService) {
        super(databaseService);
    }

    @Override
    public UserToken createOrUpdate(UserToken item) {
        return this.getDatabaseService().createOrUpdate(item, UserToken.class);
    }

    @Override
    public UserToken find(UUID id) {
        return this.getDatabaseService().getQueryable(UserToken.class).where((builder, root) -> builder.equal(root.get("token"), id)).getSingleOrDefault();
    }

    @Override
    public void delete(UserToken userToken) {
        this.getDatabaseService().delete(userToken);
    }

    @Override
    public QueryableList<UserToken> asQueryable() {
        return this.getDatabaseService().getQueryable(UserToken.class);
    }

    @Override
    public CompletableFuture<UserToken> createOrUpdateAsync(UserToken item) {
        return CompletableFuture.supplyAsync(() -> this.createOrUpdate(item));
    }

    @Override
    public UserToken find(UUID id, String hint) {
        throw new UnsupportedOperationException();
    }
}
