package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccessLayer;
import eu.eudat.data.entities.DMPDatasetProfile;

import java.util.UUID;

public interface DmpDatasetProfileDao extends DatabaseAccessLayer<DMPDatasetProfile, UUID> {
}
