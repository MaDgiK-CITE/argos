package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccessLayer;
import eu.eudat.data.dao.criteria.UserInfoCriteria;
import eu.eudat.data.entities.UserInfo;
import eu.eudat.queryable.QueryableList;

import java.util.UUID;

public interface UserInfoDao extends DatabaseAccessLayer<UserInfo, UUID> {

    QueryableList<UserInfo> getWithCriteria(UserInfoCriteria criteria);

    QueryableList<UserInfo> getAuthenticated(QueryableList<UserInfo> users, UUID principalId);
}