package eu.eudat.data.query.items.item.datasetprofile;

import eu.eudat.data.dao.criteria.DatasetProfileWizardCriteria;
import eu.eudat.data.entities.DescriptionTemplate;
import eu.eudat.data.query.definition.Query;
import eu.eudat.queryable.QueryableList;

public class DatasetProfileWizardAutocompleteRequest extends Query<DatasetProfileWizardCriteria, DescriptionTemplate> {
    @Override
    public QueryableList<DescriptionTemplate> applyCriteria() {
        return null;
    }
}
