package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccessLayer;
import eu.eudat.data.dao.criteria.DataManagementPlanBlueprintCriteria;
import eu.eudat.data.dao.criteria.DataManagementPlanProfileCriteria;
import eu.eudat.data.entities.DMP;
import eu.eudat.data.entities.DMPProfile;
import eu.eudat.queryable.QueryableList;

import java.util.UUID;

/**
 * Created by ikalyvas on 3/21/2018.
 */
public interface DMPProfileDao extends DatabaseAccessLayer<DMPProfile, UUID> {

    QueryableList<DMPProfile> getWithCriteria(DataManagementPlanProfileCriteria criteria);

    QueryableList<DMPProfile> getWithCriteriaBlueprint(DataManagementPlanBlueprintCriteria criteria);

}
