package eu.eudat.data.dao.criteria;

import eu.eudat.data.entities.Funder;

import java.util.Date;

public class FunderCriteria extends Criteria<Funder> {
	private String reference;
	private String exactReference;
	private Date periodStart;

	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getExactReference() {
		return exactReference;
	}

	public void setExactReference(String exactReference) {
		this.exactReference = exactReference;
	}

	public Date getPeriodStart() {
		return periodStart;
	}

	public void setPeriodStart(Date periodStart) {
		this.periodStart = periodStart;
	}
}
