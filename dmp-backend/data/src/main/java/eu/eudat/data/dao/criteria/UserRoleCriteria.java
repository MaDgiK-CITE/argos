package eu.eudat.data.dao.criteria;

import eu.eudat.data.entities.UserRole;

import java.util.List;

/**
 * Created by ikalyvas on 2/1/2018.
 */
public class UserRoleCriteria extends Criteria<UserRole> {
    private List<Integer> appRoles;

    public List<Integer> getAppRoles() {
        return appRoles;
    }

    public void setAppRoles(List<Integer> appRoles) {
        this.appRoles = appRoles;
    }
}
