package eu.eudat.data.dao.criteria;

import eu.eudat.data.entities.Service;

import java.util.UUID;

public class ServiceCriteria extends Criteria<Service> {

	private UUID creationUserId;

	public UUID getCreationUserId() {
		return creationUserId;
	}
	public void setCreationUserId(UUID creationUserId) {
		this.creationUserId = creationUserId;
	}
}
