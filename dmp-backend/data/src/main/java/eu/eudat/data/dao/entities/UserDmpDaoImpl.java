package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccess;
import eu.eudat.data.dao.databaselayer.service.DatabaseService;
import eu.eudat.data.entities.UserDMP;
import eu.eudat.queryable.QueryableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

/**
 * Created by ikalyvas on 2/8/2018.
 */
@Component("userDmpDao")
public class UserDmpDaoImpl extends DatabaseAccess<UserDMP> implements UserDmpDao {

    @Autowired
    public UserDmpDaoImpl(DatabaseService<UserDMP> databaseService) {
        super(databaseService);
    }

    @Override
    public UserDMP createOrUpdate(UserDMP item) {
        return this.getDatabaseService().createOrUpdate(item, UserDMP.class);
    }

    @Override
    public UserDMP find(UUID id) {
        return this.getDatabaseService().getQueryable(UserDMP.class).where((builder, root) -> builder.equal(root.get("id"), id)).getSingleOrDefault();
    }

    @Override
    public void delete(UserDMP item) {
        this.getDatabaseService().delete(item);
    }

    @Override
    public QueryableList<UserDMP> asQueryable() {
        return this.getDatabaseService().getQueryable(UserDMP.class);
    }

    @Async
    @Override
    public CompletableFuture<UserDMP> createOrUpdateAsync(UserDMP item) {
        return CompletableFuture.supplyAsync(() -> this.createOrUpdate(item));
    }

    @Override
    public UserDMP find(UUID id, String hint) {
        throw new UnsupportedOperationException();
    }
}
