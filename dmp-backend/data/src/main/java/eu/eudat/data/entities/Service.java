package eu.eudat.data.entities;


import eu.eudat.data.converters.DateToUTCConverter;
import eu.eudat.queryable.queryableentity.DataEntity;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;


@Entity
@Table(name = "\"Service\"")
public class Service implements DataEntity<Service, UUID> {

	@Id
	@Column(name = "\"ID\"", updatable = false, nullable = false, columnDefinition = "BINARY(16)")
	private UUID id;

	@Column(name = "\"Label\"")
	private String label;

	@Column(name = "\"Abbreviation\"")
	private String abbreviation;

	@Column(name = "\"Reference\"", nullable = true)
	private String reference;

	@Column(name = "\"Uri\"")
	private String uri;

	@Type(type = "eu.eudat.configurations.typedefinition.XMLType")
	@Column(name = "\"Definition\"", columnDefinition = "xml", nullable = false)
	private String definition;

	@OneToMany(mappedBy = "service", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<DatasetService> services;

	@Column(name = "\"Status\"", nullable = false)
	private Short status;

	@Column(name = "\"Created\"")
	@Convert(converter = DateToUTCConverter.class)
	private Date created = null;

	@Column(name = "\"Modified\"")
	@Convert(converter = DateToUTCConverter.class)
	private Date modified = new Date();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "\"CreationUser\"", nullable = true)
	private UserInfo creationUser;


	public Short getStatus() {
		return status;
	}
	public void setStatus(Short status) {
		this.status = status;
	}

	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getModified() {
		return modified;
	}
	public void setModified(Date modified) {
		this.modified = modified;
	}

	public UUID getId() {
		return id;
	}
	public void setId(UUID id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}

	public String getAbbreviation() {
		return abbreviation;
	}
	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getDefinition() {
		return definition;
	}
	public void setDefinition(String definition) {
		this.definition = definition;
	}

	public Set<DatasetService> getServices() {
		return services;
	}
	public void setServices(Set<DatasetService> services) {
		this.services = services;
	}

	public UserInfo getCreationUser() {
		return creationUser;
	}
	public void setCreationUser(UserInfo creationUser) {
		this.creationUser = creationUser;
	}

	@Override
	public void update(Service entity) {
		this.label = entity.getLabel();
		this.abbreviation = entity.getAbbreviation();
		this.uri = entity.getUri();
	}

	@Override
	public UUID getKeys() {
		return this.id;
	}

	@Override
	public Service buildFromTuple(List<Tuple> tuple, List<String> fields, String base) {
		String currentBase = base.isEmpty() ? "" : base + ".";
		if (fields.contains(currentBase + "id")) this.id = UUID.fromString((String) tuple.get(0).get(currentBase + "id"));
		return this;
	}
}
