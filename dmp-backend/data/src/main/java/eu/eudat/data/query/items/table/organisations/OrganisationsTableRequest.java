package eu.eudat.data.query.items.table.organisations;

import eu.eudat.data.dao.criteria.OrganisationCriteria;
import eu.eudat.data.entities.Organisation;
import eu.eudat.data.query.PaginationService;
import eu.eudat.data.query.definition.TableQuery;
import eu.eudat.queryable.QueryableList;

import java.util.UUID;

public class OrganisationsTableRequest extends TableQuery<OrganisationCriteria, Organisation, UUID> {
    @Override
    public QueryableList<Organisation> applyCriteria() {
        QueryableList<Organisation> query = this.getQuery();
        if (this.getCriteria().getLike() != null && !this.getCriteria().getLike().isEmpty()) {
            query.where((builder, root) -> builder.like(builder.upper(root.get("label")), "%" + this.getCriteria().getLike().toUpperCase() + "%"));
        }
        return query;
    }

    @Override
    public QueryableList<Organisation> applyPaging(QueryableList<Organisation> items) {
        return PaginationService.applyPaging(items, this);
    }
}
