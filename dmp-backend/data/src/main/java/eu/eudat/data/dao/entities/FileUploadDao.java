package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccessLayer;
import eu.eudat.data.entities.FileUpload;

import java.util.List;
import java.util.UUID;

public interface FileUploadDao extends DatabaseAccessLayer<FileUpload, UUID> {
    List<FileUpload> getFileUploads(UUID entityId);
}
