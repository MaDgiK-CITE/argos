package eu.eudat.data.entities;

import eu.eudat.queryable.queryableentity.DataEntity;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "\"Funder\"")
public class Funder implements DataEntity<Funder, UUID> {

	public enum Status {
		ACTIVE((short) 1), INACTIVE((short) 0), DELETED((short) 99);

		private short value;

		Status(short value) {
			this.value = value;
		}

		public short getValue() {
			return value;
		}

		public static Status fromInteger(int value) {
			switch (value) {
				case 0:
					return INACTIVE;
				case 1:
					return ACTIVE;
				case 99:
					return DELETED;
				default:
					throw new RuntimeException("Unsupported Funder Status");
			}
		}
	}

	public enum FunderType {
		EXTERNAL(0), INTERNAL(1);

		private Integer value;

		FunderType(Integer value) {
			this.value = value;
		}

		public Integer getValue() {
			return value;
		}

		public static FunderType fromInteger(int value) {
			switch (value) {
				case 0:
					return EXTERNAL;
				case 1:
					return INTERNAL;
				default:
					throw new RuntimeException("Unsupported Grant Type");
			}
		}
	}

	@Id
	//@GeneratedValue
	//@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(name = "\"ID\"", updatable = false, nullable = false, columnDefinition = "BINARY(16)")
	private UUID id;

	@Column(name = "\"Label\"")
	private String label;

	@Type(type = "eu.eudat.configurations.typedefinition.XMLType")
	@Column(name = "\"Reference\"", columnDefinition = "xml", nullable = true)
	private String reference;

	@Type(type = "eu.eudat.configurations.typedefinition.XMLType")
	@Column(name = "\"Definition\"", columnDefinition = "xml", nullable = true)
	private String definition;

	@Column(name = "\"Status\"", nullable = false)
	private Short status;

	@Column(name = "\"Created\"")
	private Date created = null;

	@Column(name = "\"Modified\"")
	private Date modified = new Date();

	@Column(name = "\"Type\"")
	private Integer type;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "\"CreationUser\"", nullable = true)
	private UserInfo creationUser;


	public UUID getId() {
		return id;
	}
	public void setId(UUID id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}

	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getDefinition() {
		return definition;
	}
	public void setDefinition(String definition) {
		this.definition = definition;
	}

	public Short getStatus() {
		return status;
	}
	public void setStatus(Short status) {
		this.status = status;
	}

	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getModified() {
		return modified;
	}
	public void setModified(Date modified) {
		this.modified = modified;
	}

	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}

	public UserInfo getCreationUser() {
		return creationUser;
	}
	public void setCreationUser(UserInfo creationUser) {
		this.creationUser = creationUser;
	}

	@Override
	public void update(Funder entity) {
		this.label = entity.getLabel();
		this.reference = entity.getReference();
		this.definition = entity.getDefinition();
		this.status = entity.getStatus();
		this.created = entity.getCreated();
		this.modified = new Date();
		this.type = entity.getType();
		this.creationUser = entity.getCreationUser();
	}

	@Override
	public UUID getKeys() {
		return this.id;
	}

	@Override
	public Funder buildFromTuple(List<Tuple> tuple, List<String> fields, String base) {
		return null;
	}
}
