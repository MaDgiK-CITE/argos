package eu.eudat.data.entities;


import eu.eudat.data.converters.DateToUTCConverter;
import eu.eudat.queryable.queryableentity.DataEntity;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;


@Entity
@Table(name = "\"DescriptionTemplate\"")
public class DescriptionTemplate implements DataEntity<DescriptionTemplate,UUID>{

    public enum Status {
        SAVED((short) 0), FINALIZED((short) 1), DELETED((short) 99);

        private short value;

        private Status(short value) {
            this.value = value;
        }
        public short getValue() {
            return value;
        }

        public static Status fromInteger(int value) {
            switch (value) {
                case 0:
                    return SAVED;
                case 1:
                    return FINALIZED;
                case 99:
                    return DELETED;
                default:
                    throw new RuntimeException("Unsupported Dataset Profile Status");
            }
        }
    }

    @Id
    @GeneratedValue
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "\"ID\"", updatable = false, nullable = false, columnDefinition = "BINARY(16)")
    private UUID id;

    @Column(name = "\"Label\"", nullable = false)
    private String label;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "profile")
    private Set<Dataset> dataset;

    @Type(type = "eu.eudat.configurations.typedefinition.XMLType")
    @Column(name = "\"Definition\"", columnDefinition = "xml", nullable = false)
    private String definition;

    @Column(name = "\"Status\"", nullable = false)
    private Short status;

    @Column(name = "\"Created\"")
    @Convert(converter = DateToUTCConverter.class)
    private Date created;

    @Column(name = "\"Modified\"")
    @Convert(converter = DateToUTCConverter.class)
    private Date modified = new Date();

    @Column(name = "\"Description\"")
    private String description;

    @Column(name = "\"GroupId\"", updatable = false, nullable = false, columnDefinition = "BINARY(16)")
    private UUID groupId;

    @Column(name = "\"Version\"", nullable = false)
    private Short version;

    @OneToMany(fetch = FetchType.LAZY)
    private Set<DMP> dmps;

    @Column(name = "\"Language\"", nullable = false)
    private String language;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "\"Type\"", nullable = false)
    private DescriptionTemplateType type;

    @OneToMany(mappedBy = "descriptionTemplate", fetch = FetchType.LAZY)
    private Set<UserDatasetProfile> users;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "datasetprofile")
    private Set<DMPDatasetProfile> associatedDmps;

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public Short getStatus() {
        return status;
    }
    public void setStatus(Short status) {
        this.status = status;
    }

    public Date getCreated() {
        return created;
    }
    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }
    public void setModified(Date modified) {
        this.modified = modified;
    }

    public UUID getId() {
        return id;
    }
    public void setId(UUID id) { this.id = id;}

    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }

    public String getDefinition() {
        return definition;
    }
    public void setDefinition(String definition) {
        this.definition = definition;
    }

    public Set<Dataset> getDataset() {
        return dataset;
    }
    public void setDataset(Set<Dataset> dataset) {
        this.dataset = dataset;
    }

    public UUID getGroupId() { return groupId; }
    public void setGroupId(UUID groupId) { this.groupId = groupId;}

    public Short getVersion() { return version; }
    public void setVersion(Short version) { this.version = version; }

    public String getLanguage() {
        return language;
    }
    public void setLanguage(String language) {
        this.language = language;
    }

    public DescriptionTemplateType getType() {
        return type;
    }
    public void setType(DescriptionTemplateType type) {
        this.type = type;
    }

    public Set<UserDatasetProfile> getUsers() {
        return users;
    }
    public void setUsers(Set<UserDatasetProfile> users) {
        this.users = users;
    }

    @Override
    public String toString() {
        return "DatasetProfileListingModel [id=" + id + ", label=" + label + ", dataset=" + dataset + ", definition=" + definition + ", version=" + version + ", language=" + language + ", type=" + type +"]";
    }

    @Override
    public void update(DescriptionTemplate entity) {
    }

    @Override
    public UUID getKeys() {
        return this.id;
    }

    @Override
    public DescriptionTemplate buildFromTuple(List<Tuple> tuple, List<String> fields, String base) {
        this.id = UUID.fromString((String) tuple.get(0).get(base.isEmpty() ? base + "." + "id" : "id"));
        return this;
    }
}
