package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccessLayer;
import eu.eudat.data.dao.criteria.OrganisationCriteria;
import eu.eudat.data.entities.Organisation;
import eu.eudat.data.entities.UserInfo;
import eu.eudat.queryable.QueryableList;

import java.util.UUID;

public interface OrganisationDao extends DatabaseAccessLayer<Organisation, UUID> {

    QueryableList<Organisation> getWithCriteria(OrganisationCriteria criteria);
    QueryableList<Organisation> getAuthenticated(QueryableList<Organisation> query, UserInfo principal);

}