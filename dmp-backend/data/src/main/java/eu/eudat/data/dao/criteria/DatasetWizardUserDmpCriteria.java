package eu.eudat.data.dao.criteria;


import eu.eudat.data.entities.DMP;
import eu.eudat.data.entities.UserInfo;

public class DatasetWizardUserDmpCriteria extends Criteria<DMP> {
    private UserInfo userInfo;

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }
}
