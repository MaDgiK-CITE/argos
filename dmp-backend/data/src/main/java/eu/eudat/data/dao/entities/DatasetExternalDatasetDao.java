package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccessLayer;
import eu.eudat.data.entities.DatasetExternalDataset;

import java.util.UUID;

/**
 * Created by ikalyvas on 5/22/2018.
 */
public interface DatasetExternalDatasetDao extends DatabaseAccessLayer<DatasetExternalDataset, UUID> {

}
