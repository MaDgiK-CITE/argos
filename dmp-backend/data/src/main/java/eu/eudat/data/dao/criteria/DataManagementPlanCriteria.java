package eu.eudat.data.dao.criteria;

import eu.eudat.data.entities.DMP;
import eu.eudat.data.entities.DMPProfile;
import eu.eudat.data.entities.Grant;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public class DataManagementPlanCriteria extends Criteria<DMP> {
    private Date periodStart;
    private Date periodEnd;
    private DMPProfile profile;
    private List<eu.eudat.data.entities.Grant> grants;
    private boolean allVersions;
    private List<UUID> groupIds;
    private Integer status;
    private List<String> organisations;
    private Integer role;
    private List<UUID> collaborators;
    private List<UUID> datasetTemplates;
    private boolean isPublic;
    private boolean onlyPublic;
    private Short grantStatus;
    private boolean hasDoi;

    public Date getPeriodStart() {
        return periodStart;
    }
    public void setPeriodStart(Date periodStart) {
        this.periodStart = periodStart;
    }

    public Date getPeriodEnd() {
        return periodEnd;
    }
    public void setPeriodEnd(Date periodEnd) {
        this.periodEnd = periodEnd;
    }

    public DMPProfile getProfile() {
        return profile;
    }
    public void setProfile(DMPProfile profile) {
        this.profile = profile;
    }

    public List<Grant> getGrants() {
        return grants;
    }
    public void setGrants(List<Grant> grants) {
        this.grants = grants;
    }

    public boolean getAllVersions() {
        return allVersions;
    }
    public void setAllVersions(boolean allVersions) {
        this.allVersions = allVersions;
    }

    public List<UUID> getGroupIds() {
        return groupIds;
    }
    public void setGroupIds(List<UUID> groupIds) {
        this.groupIds = groupIds;
    }

    public Integer getStatus() {
        return status;
    }
    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<String> getOrganisations() {
        return organisations;
    }
    public void setOrganisations(List<String> organisations) {
        this.organisations = organisations;
    }

    public Integer getRole() {
        return role;
    }
    public void setRole(Integer role) {
        this.role = role;
    }

    public List<UUID> getCollaborators() {
        return collaborators;
    }
    public void setCollaborators(List<UUID> collaborators) {
        this.collaborators = collaborators;
    }

    public List<UUID> getDatasetTemplates() {
        return datasetTemplates;
    }
    public void setDatasetTemplates(List<UUID> datasetTemplates) {
        this.datasetTemplates = datasetTemplates;
    }

    public boolean getIsPublic() {
        return isPublic;
    }
    public void setIsPublic(boolean isPublic) {
        this.isPublic = isPublic;
    }

    public boolean isOnlyPublic() {
        return onlyPublic;
    }

    public void setOnlyPublic(boolean onlyPublic) {
        this.onlyPublic = onlyPublic;
    }

    public Short getGrantStatus() {
        return grantStatus;
    }

    public void setGrantStatus(Short grantStatus) {
        this.grantStatus = grantStatus;
    }

    public boolean hasDoi() {
        return hasDoi;
    }

    public void setHasDoi(boolean hasDoi) {
        this.hasDoi = hasDoi;
    }
}
