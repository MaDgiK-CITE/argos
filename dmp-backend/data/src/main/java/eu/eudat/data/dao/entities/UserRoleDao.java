package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccessLayer;
import eu.eudat.data.dao.criteria.UserRoleCriteria;
import eu.eudat.data.entities.UserInfo;
import eu.eudat.data.entities.UserRole;
import eu.eudat.queryable.QueryableList;

import java.util.List;
import java.util.UUID;


public interface UserRoleDao extends DatabaseAccessLayer<UserRole, UUID> {

    QueryableList<UserRole> getWithCriteria(UserRoleCriteria criteria);

    List<UserRole> getUserRoles(UserInfo userInfo);
}
