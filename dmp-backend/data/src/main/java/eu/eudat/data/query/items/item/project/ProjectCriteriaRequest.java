package eu.eudat.data.query.items.item.project;

import eu.eudat.data.dao.criteria.ProjectCriteria;
import eu.eudat.data.entities.Project;
import eu.eudat.data.query.definition.Query;
import eu.eudat.queryable.QueryableList;

public class ProjectCriteriaRequest extends Query<ProjectCriteria, Project> {
	@Override
	public QueryableList<Project> applyCriteria() {
		QueryableList<Project> query = this.getQuery();
		if (this.getCriteria().getLike() != null && !this.getCriteria().getLike().isEmpty())
			query.where((builder, root) -> builder.like(builder.upper(root.get("label")), "%" + this.getCriteria().getLike().toUpperCase() + "%"));
		query.where((builder, root) -> builder.notEqual(root.get("status"), Project.Status.DELETED.getValue()));
		return query;
	}
}
