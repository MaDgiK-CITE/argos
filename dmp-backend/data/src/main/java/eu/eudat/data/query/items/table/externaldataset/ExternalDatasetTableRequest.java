package eu.eudat.data.query.items.table.externaldataset;

import eu.eudat.data.dao.criteria.ExternalDatasetCriteria;
import eu.eudat.data.entities.ExternalDataset;
import eu.eudat.data.query.definition.Query;
import eu.eudat.data.query.definition.TableQuery;
import eu.eudat.queryable.QueryableList;

import java.util.UUID;

public class ExternalDatasetTableRequest extends TableQuery<ExternalDatasetCriteria,ExternalDataset,UUID> {
    @Override
    public QueryableList<ExternalDataset> applyCriteria() {
        QueryableList<ExternalDataset> query = this.getQuery();
        if (this.getCriteria().getLike() != null && !this.getCriteria().getLike().isEmpty())
            query.where((builder, root) -> builder.like(builder.upper(root.get("label")), "%" + this.getCriteria().getLike().toUpperCase() + "%"));
        return query;
    }

    @Override
    public QueryableList<ExternalDataset> applyPaging(QueryableList<ExternalDataset> items) {
        return null;
    }
}
