package eu.eudat.data.entities;

import eu.eudat.queryable.queryableentity.DataEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "\"DMPDatasetProfile\"")
public class DMPDatasetProfile implements DataEntity<DMPDatasetProfile, UUID> {

    @Id
    @GeneratedValue
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "\"ID\"", updatable = false, nullable = false, columnDefinition = "BINARY(16)")
    private UUID id;

    @ManyToOne
    @JoinColumn(name = "\"dmp\"")
    private DMP dmp;

    @ManyToOne
    @JoinColumn(name = "\"datasetprofile\"")
    private DescriptionTemplate datasetprofile;

    @Column(name = "\"data\"")
    private String data;

    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }

    public DMP getDmp() {
        return dmp;
    }
    public void setDmp(DMP dmp) {
        this.dmp = dmp;
    }

    public DescriptionTemplate getDatasetprofile() {
        return datasetprofile;
    }
    public void setDatasetprofile(DescriptionTemplate datasetprofile) {
        this.datasetprofile = datasetprofile;
    }

    public String getData() {
        return data;
    }
    public void setData(String data) {
        this.data = data;
    }

    @Override
    public void update(DMPDatasetProfile entity) {
        this.dmp = entity.getDmp();
        this.datasetprofile = entity.getDatasetprofile();
        this.data = entity.getData();
    }

    @Override
    public UUID getKeys() {
        return this.id;
    }

    @Override
    public DMPDatasetProfile buildFromTuple(List<Tuple> tuple, List<String> fields, String base) {
        this.id = UUID.fromString((String) tuple.get(0).get(base.isEmpty() ? base + "." + "id" : "id"));
        return this;
    }
}
