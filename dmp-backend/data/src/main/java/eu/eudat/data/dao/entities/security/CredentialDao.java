package eu.eudat.data.dao.entities.security;

import eu.eudat.data.dao.DatabaseAccessLayer;
import eu.eudat.data.entities.Credential;

import java.util.UUID;


public interface CredentialDao extends DatabaseAccessLayer<Credential, UUID> {

    Credential getLoggedInCredentials(String username, String secret, Integer provider);
}
