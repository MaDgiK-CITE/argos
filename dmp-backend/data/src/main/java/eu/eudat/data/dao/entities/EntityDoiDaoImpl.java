package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccess;
import eu.eudat.data.dao.databaselayer.service.DatabaseService;
import eu.eudat.data.entities.EntityDoi;
import eu.eudat.queryable.QueryableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Component("EntityDoiDao")
public class EntityDoiDaoImpl extends DatabaseAccess<EntityDoi> implements EntityDoiDao {

    @Autowired
    public EntityDoiDaoImpl(DatabaseService<EntityDoi> databaseService){
        super(databaseService);
    }


    @Override
    public EntityDoi createOrUpdate(EntityDoi item) {
        return this.getDatabaseService().createOrUpdate(item, EntityDoi.class);
    }

    @Override
    public CompletableFuture<EntityDoi> createOrUpdateAsync(EntityDoi item) {
        return CompletableFuture.supplyAsync(() -> this.createOrUpdate(item));
    }

    @Override
    public EntityDoi find(UUID id) {
        return this.getDatabaseService().getQueryable(EntityDoi.class).where((builder, root) -> builder.equal(root.get("id"), id)).getSingle();
    }

    @Override
    public EntityDoi findFromDoi(String doi) {
        return this.getDatabaseService().getQueryable(EntityDoi.class).where((builder, root) -> builder.equal(root.get("doi"), doi)).getSingle();
    }

    @Override
    public EntityDoi find(UUID id, String hint) {
        return null;
    }

    @Override
    public void delete(EntityDoi item) {
        this.getDatabaseService().delete(item);
    }

    @Override
    public QueryableList<EntityDoi> asQueryable() {
        return this.getDatabaseService().getQueryable(EntityDoi.class);
    }
}
