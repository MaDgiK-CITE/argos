package eu.eudat.data.dao.criteria;

import eu.eudat.data.entities.Registry;

import java.util.UUID;

public class RegistryCriteria extends Criteria<Registry> {

	private UUID creationUserId;

	public UUID getCreationUserId() {
		return creationUserId;
	}
	public void setCreationUserId(UUID creationUserId) {
		this.creationUserId = creationUserId;
	}
}
