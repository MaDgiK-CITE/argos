package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccessLayer;
import eu.eudat.data.dao.criteria.EmailConfirmationCriteria;
import eu.eudat.data.entities.EmailConfirmation;
import eu.eudat.queryable.QueryableList;

import java.util.UUID;

public interface EmailConfirmationDao extends DatabaseAccessLayer<EmailConfirmation, UUID> {

	QueryableList<EmailConfirmation> getWithCriteria(EmailConfirmationCriteria criteria);
}
