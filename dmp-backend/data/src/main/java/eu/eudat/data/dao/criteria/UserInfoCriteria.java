package eu.eudat.data.dao.criteria;

import eu.eudat.data.entities.UserInfo;

import java.util.List;


public class UserInfoCriteria extends Criteria<UserInfo> {
    private String email;
    private List<Integer> appRoles;
    private String collaboratorLike;

    public List<Integer> getAppRoles() {
        return appRoles;
    }
    public void setAppRoles(List<Integer> appRoles) {
        this.appRoles = appRoles;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getCollaboratorLike() {
        return collaboratorLike;
    }
    public void setCollaboratorLike(String collaboratorLike) {
        this.collaboratorLike = collaboratorLike;
    }
}
