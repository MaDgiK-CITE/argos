package eu.eudat.data.entities;

import eu.eudat.data.entities.helpers.EntityBinder;
import eu.eudat.queryable.queryableentity.DataEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;


@Entity
@Table(name = "\"DatasetExternalDataset\"")
public class DatasetExternalDataset implements DataEntity<DatasetExternalDataset, UUID> {

	@Id
	@GeneratedValue
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(name = "\"Id\"", updatable = false, nullable = false, columnDefinition = "BINARY(16)")
	private UUID id;

	@ManyToOne
	@JoinColumn(name = "\"Dataset\"", nullable = false)
	private Dataset dataset;

	@ManyToOne
	@JoinColumn(name = "\"ExternalDataset\"", nullable = false)
	private ExternalDataset externalDataset;

	@Column(name = "\"Role\"")
	private Integer role;

	@Column(name = "\"Data\"")
	private String data;


	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public Dataset getDataset() {
		return dataset;
	}

	public void setDataset(Dataset dataset) {
		this.dataset = dataset;
	}

	public ExternalDataset getExternalDataset() {
		return externalDataset;
	}

	public void setExternalDataset(ExternalDataset externalDataset) {
		this.externalDataset = externalDataset;
	}

	public Integer getRole() {
		return role;
	}

	public void setRole(Integer role) {
		this.role = role;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	@Override
	public void update(DatasetExternalDataset entity) {
		this.dataset = entity.getDataset();
		this.externalDataset = entity.getExternalDataset();
		this.role = entity.getRole();
	}

	@Override
	public UUID getKeys() {
		return this.id;
	}

	@Override
	public DatasetExternalDataset buildFromTuple(List<Tuple> tuple, List<String> fields, String base) {
		String currentBase = base.isEmpty() ? "" : base + ".";
		if (fields.contains(currentBase + "id")) this.id = EntityBinder.fromTuple(tuple, currentBase + "id");
		return this;
	}
}
