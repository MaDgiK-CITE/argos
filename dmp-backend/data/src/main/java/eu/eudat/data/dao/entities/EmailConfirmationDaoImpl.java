package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccess;
import eu.eudat.data.dao.criteria.EmailConfirmationCriteria;
import eu.eudat.data.dao.databaselayer.service.DatabaseService;
import eu.eudat.data.entities.EmailConfirmation;
import eu.eudat.queryable.QueryableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Service("LoginConfirmationEmailDao")
public class EmailConfirmationDaoImpl extends DatabaseAccess<EmailConfirmation> implements EmailConfirmationDao {

	@Autowired
	public EmailConfirmationDaoImpl(DatabaseService<EmailConfirmation> databaseService) {
		super(databaseService);
	}

	@Override
	public QueryableList<EmailConfirmation> getWithCriteria(EmailConfirmationCriteria criteria) {
		return null;
	}

	@Override
	public EmailConfirmation createOrUpdate(EmailConfirmation item) {
		return this.getDatabaseService().createOrUpdate(item, EmailConfirmation.class);
	}

	@Override
	public CompletableFuture<EmailConfirmation> createOrUpdateAsync(EmailConfirmation item) {
		return null;
	}

	@Override
	public EmailConfirmation find(UUID id) {
		return this.getDatabaseService().getQueryable(EmailConfirmation.class).where((builder, root) -> builder.equal(root.get("id"), id)).getSingle();
	}

	@Override
	public EmailConfirmation find(UUID id, String hint) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void delete(EmailConfirmation item) {
		throw new UnsupportedOperationException();
	}

	@Override
	public QueryableList<EmailConfirmation> asQueryable() {
		return this.getDatabaseService().getQueryable(EmailConfirmation.class);
	}
}
