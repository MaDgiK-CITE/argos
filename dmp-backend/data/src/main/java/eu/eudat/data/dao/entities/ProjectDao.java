package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccessLayer;
import eu.eudat.data.dao.criteria.ProjectCriteria;
import eu.eudat.data.entities.Project;
import eu.eudat.data.entities.UserInfo;
import eu.eudat.queryable.QueryableList;

import java.util.UUID;

public interface ProjectDao extends DatabaseAccessLayer<Project, UUID> {

	QueryableList<Project> getWithCritetia(ProjectCriteria criteria);

	QueryableList<Project> getAuthenticated(QueryableList<Project> query, UserInfo principal);
}
