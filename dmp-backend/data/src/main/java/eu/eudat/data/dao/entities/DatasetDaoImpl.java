package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccess;
import eu.eudat.data.dao.criteria.DatasetCriteria;
import eu.eudat.data.dao.databaselayer.service.DatabaseService;
import eu.eudat.data.entities.DMP;
import eu.eudat.data.entities.Dataset;
import eu.eudat.data.entities.UserDMP;
import eu.eudat.data.entities.UserInfo;
import eu.eudat.queryable.QueryableList;
import eu.eudat.queryable.types.FieldSelectionType;
import eu.eudat.queryable.types.SelectionField;
import eu.eudat.types.grant.GrantStateType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Component("datasetDao")
public class DatasetDaoImpl extends DatabaseAccess<Dataset> implements DatasetDao {

    @Autowired
    public DatasetDaoImpl(DatabaseService<Dataset> databaseService) { super(databaseService); }

    @Override
    public QueryableList<Dataset> getWithCriteria(DatasetCriteria criteria) {
        QueryableList<Dataset> query = getDatabaseService().getQueryable(Dataset.getHints(), Dataset.class);
        if (criteria.getIsPublic() != null && criteria.getIsPublic()) {
            query.where((builder, root) -> builder.equal(root.get("dmp").get("isPublic"), true));
            query.where((builder, root) -> builder.equal(root.get("status"), Dataset.Status.FINALISED.getValue()));
            /*query.initSubQuery(String.class).where((builder, root) -> builder.equal(root.get("dmp").get("version"),
                    query.<String>subQueryMax((builder1, externalRoot, nestedRoot) -> builder1.equal(externalRoot.get("dmp").get("groupId"), nestedRoot.get("dmp").get("groupId")),
                            Arrays.asList(new SelectionField(FieldSelectionType.COMPOSITE_FIELD, "dmp:version")), String.class)));*/
        }
        if (criteria.getLike() != null && !criteria.getLike().isEmpty())
            query.where((builder, root) -> builder.or(
                    builder.like(builder.upper(root.get("label")), "%" + criteria.getLike().toUpperCase() + "%"),
                    builder.like(builder.upper(root.get("description")), "%" + criteria.getLike().toUpperCase() + "%")));
        if (criteria.getStatus() != null)
            query.where((builder, root) -> builder.equal(root.get("status"), criteria.getStatus()));
        if (criteria.getProfileDatasetId() != null)
            query.where((builder, root) -> builder.equal(root.get("profile").get("id"), criteria.getProfileDatasetId()));
        if (criteria.getPeriodEnd() != null)
            query.where((builder, root) -> builder.lessThan(root.get("created"), criteria.getPeriodEnd()));
        if (criteria.getPeriodStart() != null)
            query.where((builder, root) -> builder.greaterThan(root.get("created"), criteria.getPeriodStart()));
        if (!criteria.getAllVersions())
            query.initSubQuery(String.class).where((builder, root) -> builder.equal(root.get("dmp").get("version"), query.<String>subQueryMax((builder1, externalRoot, nestedRoot) -> builder1.and(builder1.equal(externalRoot.get("dmp").get("groupId"), nestedRoot.get("dmp").get("groupId")), builder1.notEqual(nestedRoot.get("dmp").get("status"), DMP.DMPStatus.DELETED.getValue())), Arrays.asList(new SelectionField(FieldSelectionType.COMPOSITE_FIELD, "dmp:version")), String.class)));
        if (criteria.getGroupIds() != null && !criteria.getGroupIds().isEmpty())
            query.where((builder, root) -> root.get("dmp").get("groupId").in(criteria.getGroupIds()));
        if (criteria.getDmpIds() != null && !criteria.getDmpIds().isEmpty())
            query.where((builder, root) -> root.get("dmp").get("id").in(criteria.getDmpIds()));
        /*if (criteria.getRole() != null) {
            query.where((builder, root) -> builder.equal(root.join("dmp").join("users").get("role"), criteria.getRole()));
        } else {
            query.where((builder, root) -> root.join("dmp").join("users").get("role").in(UserDMP.UserDMPRoles.getAllValues()));
        }*/
        if (criteria.getOrganisations() != null && !criteria.getOrganisations().isEmpty())
            query.where((builder, root) -> root.join("dmp").join("organisations").get("reference").in(criteria.getOrganisations()));
        if (criteria.getGrants() != null && !criteria.getGrants().isEmpty())
            query.where((builder, root) -> root.join("dmp").join("grant").get("id").in(criteria.getGrants()));
        if (criteria.getGrantStatus() != null) {
            if (criteria.getGrantStatus().equals(GrantStateType.FINISHED.getValue().shortValue()))
                query.where((builder, root) -> builder.lessThan(root.get("dmp").get("grant").get("enddate"), new Date()));
            if (criteria.getGrantStatus().equals(GrantStateType.ONGOING.getValue().shortValue()))
                query.where((builder, root) ->
                        builder.or(builder.greaterThan(root.get("dmp").get("grant").get("enddate"), new Date())
                                , builder.isNull(root.get("dmp").get("grant").get("enddate"))));
        }
        if (criteria.getCollaborators() != null && !criteria.getCollaborators().isEmpty())
            query.where((builder, root) -> root.join("dmp", JoinType.LEFT).join("users", JoinType.LEFT).join("user", JoinType.LEFT).get("id").in(criteria.getCollaborators()));
        if (criteria.getDatasetTemplates() != null && !criteria.getDatasetTemplates().isEmpty())
            query.where((builder, root) -> root.get("profile").get("id").in(criteria.getDatasetTemplates()));

        if (criteria.hasDoi()) {
            query.where((builder, root) -> builder.not(builder.isNull(root.join("dmp").join("dois").get("id"))));
        }
        query.where((builder, root) -> builder.notEqual(root.get("status"), Dataset.Status.DELETED.getValue()));
        query.where((builder, root) -> builder.notEqual(root.get("status"), Dataset.Status.CANCELED.getValue()));
        return query;
    }

    public QueryableList<Dataset> filterFromElastic(DatasetCriteria criteria, List<UUID> ids) {
        QueryableList<Dataset> query = getDatabaseService().getQueryable(Dataset.getHints(), Dataset.class);

        query.where(((builder, root) -> root.get("id").in(ids)));
        if (!criteria.getAllVersions())
            query.initSubQuery(String.class).where((builder, root) -> builder.equal(root.get("dmp").get("version"), query.<String>subQueryMax((builder1, externalRoot, nestedRoot) -> builder1.and(builder1.equal(externalRoot.get("dmp").get("groupId"), nestedRoot.get("dmp").get("groupId")), builder1.notEqual(nestedRoot.get("dmp").get("status"), DMP.DMPStatus.DELETED.getValue())), Arrays.asList(new SelectionField(FieldSelectionType.COMPOSITE_FIELD, "dmp:version")), String.class)));

        return query;
    }

    @Override
    public Dataset createOrUpdate(Dataset item) {
        return getDatabaseService().createOrUpdate(item, Dataset.class);
    }

    @Override
    public Dataset find(UUID id) {
        return getDatabaseService().getQueryable(Dataset.class)
                .where((builder, root) -> builder.and(builder.notEqual(root.get("status"),Dataset.Status.DELETED.getValue()), builder.notEqual(root.get("status"),Dataset.Status.CANCELED.getValue()), builder.equal((root.get("id")), id))).getSingle();
    }

    @Override
    public Dataset find(UUID id, String hint) {
        return getDatabaseService().getQueryable(Dataset.getHints(), Dataset.class).withHint(hint)
                .where((builder, root) -> builder.and(builder.notEqual(root.get("status"),Dataset.Status.DELETED.getValue()), builder.notEqual(root.get("status"),Dataset.Status.CANCELED.getValue()), builder.equal((root.get("id")), id))).getSingle();
    }

    @Override
    public Dataset isPublicDataset(UUID id) {
        QueryableList<Dataset> query = getDatabaseService().getQueryable(Dataset.getHints(), Dataset.class);
        query.where(((builder, root) -> builder.equal(root.get("id"), id)));

        return query.withHint("datasetListingModel").getSingle();
    }

    @Override
    public QueryableList<Dataset> getAuthenticated(QueryableList<Dataset> query, UserInfo principal, List<Integer> roles) {
        if (roles != null && !roles.isEmpty()) {
            query.where((builder, root) -> {
                Join userJoin = root.join("dmp", JoinType.LEFT).join("users", JoinType.LEFT);
                return builder.and(builder.equal(userJoin.join("user", JoinType.LEFT).get("id"), principal.getId()), userJoin.get("role").in(roles));
            });
        } else {
            query.where((builder, root) -> builder.equal(root.join("dmp", JoinType.LEFT).join("users", JoinType.LEFT).join("user", JoinType.LEFT).get("id"), principal.getId()));

        }
        return query;
    }

    @Override
    public void delete(Dataset item) {
        this.getDatabaseService().delete(item);
    }

    @Override
    public QueryableList<Dataset> asQueryable() {
        return this.getDatabaseService().getQueryable(Dataset.class);
    }

    @Async
    public CompletableFuture<Dataset> createOrUpdateAsync(Dataset item) {
        return CompletableFuture.supplyAsync(() -> this.createOrUpdate(item));
    }
}
