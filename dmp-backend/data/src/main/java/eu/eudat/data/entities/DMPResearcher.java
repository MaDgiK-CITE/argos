package eu.eudat.data.entities;


import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.UUID;


@Entity
@Table(name = "\"DMPResearcher\"")
public class DMPResearcher {

    @Id
    @GeneratedValue
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "\"ID\"", updatable = false, nullable = false, columnDefinition = "BINARY(16)")
    private UUID id;

    @Type(type = "org.hibernate.type.PostgresUUIDType")
    @Column(name = "\"DMP\"")
    private UUID dmp;

    @Type(type = "org.hibernate.type.PostgresUUIDType")
    @Column(name = "\"Researcher\"")
    private UUID researcher;

    @Column(name = "\"Role\"")
    private Integer role;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getDmp() {
        return dmp;
    }

    public void setDmp(UUID dmp) {
        this.dmp = dmp;
    }

    public UUID getResearcher() {
        return researcher;
    }

    public void setResearcher(UUID researcher) {
        this.researcher = researcher;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }


}
