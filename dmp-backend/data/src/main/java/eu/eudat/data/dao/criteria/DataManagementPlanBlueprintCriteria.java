package eu.eudat.data.dao.criteria;

import eu.eudat.data.entities.DMPProfile;

public class DataManagementPlanBlueprintCriteria extends Criteria<DMPProfile> {

    private Integer status;

    public Integer getStatus() {
        return status;
    }
    public void setStatus(Integer status) {
        this.status = status;
    }

}
