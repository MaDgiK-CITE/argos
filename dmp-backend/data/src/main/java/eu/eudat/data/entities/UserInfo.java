package eu.eudat.data.entities;

import eu.eudat.data.converters.DateToUTCConverter;
import eu.eudat.data.entities.helpers.EntityBinder;
import eu.eudat.queryable.queryableentity.DataEntity;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.*;


@Entity
@Table(name = "\"UserInfo\"")
@NamedEntityGraphs({
		@NamedEntityGraph(
				name = "userInfo",
				attributeNodes = {@NamedAttributeNode("userRoles"), @NamedAttributeNode("credentials"), @NamedAttributeNode("additionalinfo")}),
})
public class UserInfo implements DataEntity<UserInfo, UUID> {

	@Id
	@GeneratedValue
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(name = "id", updatable = false, nullable = false, columnDefinition = "BINARY(16)")
	private UUID id;


	@Column(name = "email")
	private String email = null;

	@Column(name = "authorization_level", nullable = false)
	private Short authorization_level; //0 admin, 1 user

	@Column(name = "usertype", nullable = false)
	private Short usertype; // 0 internal, 1 external

	@Column(name = "userstatus", nullable = false)
	private Short userStatus; // 0 active, 1 inactive

	@Column(name = "verified_email", nullable = true)
	private Boolean verified_email = null;

	@Column(name = "name", nullable = true)
	private String name = null;


	@Column(name = "created", nullable = false)
	@Convert(converter = DateToUTCConverter.class)
	private Date created = null;


	@Column(name = "lastloggedin", nullable = true)
	@Convert(converter = DateToUTCConverter.class)
	private Date lastloggedin = null;


	@Type(type = "eu.eudat.configurations.typedefinition.XMLType")
	@Column(name = "additionalinfo", nullable = true)
	private String additionalinfo;

	@OneToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "\"UserDMP\"",
			joinColumns = {@JoinColumn(name = "usr", referencedColumnName = "id")},
			inverseJoinColumns = {@JoinColumn(name = "dmp", referencedColumnName = "\"ID\"")}
	)
	private Set<DMP> dmps;

	@OneToMany(mappedBy = "userInfo", fetch = FetchType.LAZY)
	private Set<Credential> credentials = new HashSet<>();

	@OneToMany(mappedBy = "userInfo", fetch = FetchType.LAZY)
	private Set<UserRole> userRoles = new HashSet<>();

	@OneToMany(mappedBy = "lockedBy", fetch = FetchType.LAZY)
	private Set<Lock> locks = new HashSet<>();

	@OneToMany(mappedBy = "userId", fetch = FetchType.LAZY)
	private Set<Notification> notifications = new HashSet<>();

	public Set<DMP> getDmps() {
		return dmps;
	}

	public void setDmps(Set<DMP> dmps) {
		this.dmps = dmps;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getLastloggedin() {
		return lastloggedin;
	}

	public void setLastloggedin(Date lastloggedin) {
		this.lastloggedin = lastloggedin;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Short getAuthorization_level() {
		return authorization_level;
	}

	public void setAuthorization_level(Short authorization_level) {
		this.authorization_level = authorization_level;
	}

	public Short getUsertype() {
		return usertype;
	}

	public void setUsertype(Short usertype) {
		this.usertype = usertype;
	}

	public Boolean getVerified_email() {
		return verified_email;
	}

	public void setVerified_email(Boolean verified_email) {
		this.verified_email = verified_email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAdditionalinfo() {
		return additionalinfo;
	}

	public void setAdditionalinfo(String additionalinfo) {
		this.additionalinfo = additionalinfo;
	}

	public Set<Credential> getCredentials() {
		return credentials;
	}

	public void setCredentials(Set<Credential> credentials) {
		this.credentials = credentials;
	}

	public Set<UserRole> getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(Set<UserRole> userRoles) {
		this.userRoles = userRoles;
	}

	public Set<Lock> getLocks() {
		return locks;
	}

	public void setLocks(Set<Lock> locks) {
		this.locks = locks;
	}

	public Set<Notification> getNotifications() {
		return notifications;
	}

	public void setNotifications(Set<Notification> notifications) {
		this.notifications = notifications;
	}

	public Short getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(Short userStatus) {
		this.userStatus = userStatus;
	}

	@Override
	public void update(UserInfo entity) {
		this.name = entity.getName();
		this.email = entity.getEmail();
		this.additionalinfo = entity.getAdditionalinfo();
		this.lastloggedin = entity.getLastloggedin();
		this.userRoles = entity.getUserRoles();
		this.userStatus = entity.getUserStatus();
	}

	@Override
	public UUID getKeys() {
		return this.id;
	}

	@Override
	public UserInfo buildFromTuple(List<Tuple> tuple, List<String> fields, String base) {
		String currentBase = base.isEmpty() ? "" : base + ".";
		if (fields.contains(currentBase + "id")) this.id = EntityBinder.fromTuple(tuple, currentBase + "id");
		return this;
	}
}
