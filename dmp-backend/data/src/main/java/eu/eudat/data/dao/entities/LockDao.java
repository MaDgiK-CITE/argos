package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccessLayer;
import eu.eudat.data.dao.criteria.LockCriteria;
import eu.eudat.data.entities.Lock;
import eu.eudat.queryable.QueryableList;

import java.util.UUID;

public interface LockDao extends DatabaseAccessLayer<Lock, UUID> {

	QueryableList<Lock> getWithCriteria(LockCriteria criteria);
}
