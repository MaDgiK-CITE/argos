package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccess;
import eu.eudat.data.dao.criteria.DataManagementPlanCriteria;
import eu.eudat.data.dao.criteria.DatasetWizardUserDmpCriteria;
import eu.eudat.data.dao.databaselayer.service.DatabaseService;
import eu.eudat.data.entities.DMP;
import eu.eudat.data.entities.UserInfo;
import eu.eudat.queryable.QueryableList;
import eu.eudat.queryable.types.FieldSelectionType;
import eu.eudat.queryable.types.SelectionField;
import eu.eudat.types.grant.GrantStateType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Component("dMPDao")
public class DMPDaoImpl extends DatabaseAccess<DMP> implements DMPDao {

	@Autowired
	public DMPDaoImpl(DatabaseService<DMP> databaseService) {
		super(databaseService);
	}

	@Override
	public QueryableList<DMP> getWithCriteria(DataManagementPlanCriteria criteria) {
		QueryableList<DMP> query = getDatabaseService().getQueryable(DMP.getHints(), DMP.class);
		if (criteria.getLike() != null && !criteria.getLike().isEmpty())
			query.where((builder, root) -> builder.or(
					builder.like(builder.upper(root.get("label")), "%" + criteria.getLike().toUpperCase() + "%"),
					builder.like(builder.upper(root.get("description")), "%" + criteria.getLike().toUpperCase() + "%")));
		if (criteria.getPeriodEnd() != null)
			query.where((builder, root) -> builder.lessThan(root.get("created"), criteria.getPeriodEnd()));
		if (criteria.getPeriodStart() != null)
			query.where((builder, root) -> builder.greaterThan(root.get("created"), criteria.getPeriodStart()));
		if (criteria.getProfile() != null)
			query.where((builder, root) -> builder.equal(root.get("profile"), criteria.getProfile()));
		if (criteria.getGrants() != null && !criteria.getGrants().isEmpty())
			query.where(((builder, root) -> root.get("grant").in(criteria.getGrants())));
		if (!criteria.getAllVersions())
			query.initSubQuery(String.class).where((builder, root) -> builder.equal(root.get("version"),
					query.<String>subQueryMax((builder1, externalRoot, nestedRoot) -> builder1.and(
							builder1.equal(externalRoot.get("groupId"), nestedRoot.get("groupId")),
							builder1.notEqual(nestedRoot.get("status"), DMP.DMPStatus.DELETED.getValue())), Arrays.asList(new SelectionField(FieldSelectionType.FIELD, "version")), String.class)));
		if (criteria.getGroupIds() != null && !criteria.getGroupIds().isEmpty())
			query.where((builder, root) -> root.get("groupId").in(criteria.getGroupIds()));
		if (criteria.getStatus() != null) {
			if (criteria.getStatus() == DMP.DMPStatus.FINALISED.getValue()) {
				query.where((builder, root) -> builder.equal(root.get("status"), DMP.DMPStatus.FINALISED.getValue()));
			} else if (criteria.getStatus() == DMP.DMPStatus.ACTIVE.getValue()) {
				query.where((builder, root) -> builder.equal(root.get("status"), DMP.DMPStatus.ACTIVE.getValue()));
			}
		}
		if (criteria.getIsPublic()) {
			query.where(((builder, root) -> builder.equal(root.get("isPublic"), criteria.getIsPublic())));
		}
        /*if (criteria.getRole() != null) {
            if (criteria.getRole().equals(UserDMP.UserDMPRoles.OWNER.getValue())) {
                query.where((builder, root) -> builder.equal(root.join("users", JoinType.LEFT).get("role"), UserDMP.UserDMPRoles.OWNER.getValue()));
            } else if (criteria.getRole().equals(UserDMP.UserDMPRoles.USER.getValue())) {
                query.where((builder, root) -> builder.equal(root.join("users", JoinType.LEFT).get("role"), UserDMP.UserDMPRoles.USER.getValue()));
            }
        }*/
		if (criteria.getOrganisations() != null && !criteria.getOrganisations().isEmpty()) {
			query.where((builder, root) -> root.join("organisations").get("reference").in(criteria.getOrganisations()));
		}
		if (criteria.getCollaborators() != null && !criteria.getCollaborators().isEmpty()) {
			query.where((builder, root) -> root.join("users", JoinType.LEFT).join("user", JoinType.LEFT).get("id").in(criteria.getCollaborators()));
		}
		if (criteria.getDatasetTemplates() != null && !criteria.getDatasetTemplates().isEmpty()) {
			query.where((builder, root) -> root.join("associatedDmps", JoinType.LEFT).get("datasetprofile").get("id").in(criteria.getDatasetTemplates()));
		}
		if (criteria.getGrantStatus() != null) {
			if (criteria.getGrantStatus().equals(GrantStateType.FINISHED.getValue().shortValue()))
				query.where((builder, root) -> builder.lessThan(root.get("grant").get("enddate"), new Date()));
			if (criteria.getGrantStatus().equals(GrantStateType.ONGOING.getValue().shortValue()))
				query.where((builder, root) ->
						builder.or(builder.greaterThan(root.get("grant").get("enddate"), new Date())
								, builder.isNull(root.get("grant").get("enddate"))));
		}

		if (criteria.hasDoi()) {
			query.where((builder, root) -> builder.not(builder.isNull(root.join("dois").get("id"))));
		}
		query.where((builder, root) -> builder.notEqual(root.get("status"), DMP.DMPStatus.DELETED.getValue()));
		return query;
	}

	public QueryableList<DMP> getAuthenticated(QueryableList<DMP> query, UUID principal, List<Integer> roles) {
		if (roles != null && !roles.isEmpty()) {
			query.where((builder, root) -> {
				Join userJoin = root.join("users", JoinType.LEFT);
				return builder.and(builder.equal(userJoin.join("user", JoinType.LEFT).get("id"), principal), userJoin.get("role").in(roles));
			});
		} else {
			query.where((builder, root) -> builder.equal(root.join("users", JoinType.LEFT).join("user", JoinType.LEFT).get("id"), principal));
		}

		return query;
	}

	@Override
	@Transactional
	public DMP createOrUpdate(DMP item) {
		return this.getDatabaseService().createOrUpdate(item, DMP.class);
	}

	@Override
	public DMP find(UUID id) {
		return getDatabaseService().getQueryable(DMP.class).where((builder, root) -> builder.equal((root.get("id")), id)).getSingle();
	}

	@Override
	public QueryableList<DMP> getUserDmps(DatasetWizardUserDmpCriteria datasetWizardUserDmpCriteria, UserInfo userInfo) {
		QueryableList<DMP> query = getDatabaseService().getQueryable(DMP.class).where((builder, root) -> builder.or(builder.equal(root.get("creator"), userInfo), builder.isMember(userInfo, root.get("users"))));
		if (datasetWizardUserDmpCriteria.getLike() != null && !datasetWizardUserDmpCriteria.getLike().isEmpty()) {
			query.where((builder, root) -> builder.like(root.get("label"), "%" + datasetWizardUserDmpCriteria.getLike() + "%"));
		}
		return query;
	}

	@Override
	public void delete(DMP item) {
		this.getDatabaseService().delete(item);
	}

	@Override
	public QueryableList<DMP> asQueryable() {
		return this.getDatabaseService().getQueryable(DMP.class);
	}

	@Async
	@Override
	public CompletableFuture<DMP> createOrUpdateAsync(DMP item) {
		return CompletableFuture.supplyAsync(() -> this.createOrUpdate(item));
	}

	@Override
	public DMP find(UUID id, String hint) {
		throw new UnsupportedOperationException();
	}
}
