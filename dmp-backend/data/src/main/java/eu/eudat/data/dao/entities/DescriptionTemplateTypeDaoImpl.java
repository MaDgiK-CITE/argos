package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccess;
import eu.eudat.data.dao.databaselayer.service.DatabaseService;
import eu.eudat.data.entities.DescriptionTemplateType;
import eu.eudat.queryable.QueryableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Component("descriptionTemplateTypeDao")
public class DescriptionTemplateTypeDaoImpl extends DatabaseAccess<DescriptionTemplateType> implements DescriptionTemplateTypeDao {

    @Autowired
    public DescriptionTemplateTypeDaoImpl(DatabaseService<DescriptionTemplateType> databaseService) {
        super(databaseService);
    }

    @Override
    public DescriptionTemplateType findFromName(String name){
        try {
            return this.getDatabaseService().getQueryable(DescriptionTemplateType.class).where((builder, root) -> builder.and(builder.equal(root.get("name"), name), builder.notEqual(root.get("status"), DescriptionTemplateType.Status.DELETED.getValue()))).getSingle();
        }
        catch(Exception e){
            return null;
        }
    }

    @Override
    @Transactional
    public DescriptionTemplateType createOrUpdate(DescriptionTemplateType item) {
        return this.getDatabaseService().createOrUpdate(item, DescriptionTemplateType.class);
    }

    @Override
    public DescriptionTemplateType find(UUID id) {
        return getDatabaseService().getQueryable(DescriptionTemplateType.class).where((builder, root) -> builder.equal((root.get("id")), id)).getSingle();
    }

    @Override
    public void delete(DescriptionTemplateType item) {
        this.getDatabaseService().delete(item);
    }

    @Override
    public QueryableList<DescriptionTemplateType> asQueryable() {
        return this.getDatabaseService().getQueryable(DescriptionTemplateType.class).where((builder, root) -> builder.notEqual((root.get("status")), DescriptionTemplateType.Status.DELETED.getValue()));
    }

    @Async
    @Override
    public CompletableFuture<DescriptionTemplateType> createOrUpdateAsync(DescriptionTemplateType item) {
        return CompletableFuture.supplyAsync(() -> this.createOrUpdate(item));
    }

    @Override
    public DescriptionTemplateType find(UUID id, String hint) {
        throw new UnsupportedOperationException();
    }

}
