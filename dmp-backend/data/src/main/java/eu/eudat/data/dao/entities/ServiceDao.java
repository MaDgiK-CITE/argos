package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccessLayer;
import eu.eudat.data.dao.criteria.ServiceCriteria;
import eu.eudat.data.entities.Service;
import eu.eudat.queryable.QueryableList;

import java.util.UUID;

public interface ServiceDao extends DatabaseAccessLayer<Service, UUID> {

    QueryableList<Service> getWithCriteria(ServiceCriteria criteria);

}