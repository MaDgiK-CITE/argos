package eu.eudat.data.query.items.item.dmpprofile;

import eu.eudat.data.dao.criteria.DataManagementPlanProfileCriteria;
import eu.eudat.data.entities.DMPProfile;
import eu.eudat.data.query.definition.Query;
import eu.eudat.queryable.QueryableList;

/**
 * Created by ikalyvas on 3/21/2018.
 */
public class DataManagementPlanProfileCriteriaRequest extends Query<DataManagementPlanProfileCriteria, DMPProfile> {
    @Override
    public QueryableList<DMPProfile> applyCriteria() {
        QueryableList<DMPProfile> query = this.getQuery();
        if (this.getCriteria().getLike() != null && !this.getCriteria().getLike().isEmpty())
            query.where((builder, root) -> builder.like(builder.upper(root.get("label")), "%" + this.getCriteria().getLike().toUpperCase() + "%"));
        return query;
    }
}
