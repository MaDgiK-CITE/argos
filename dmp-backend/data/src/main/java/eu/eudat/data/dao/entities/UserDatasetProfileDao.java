package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccessLayer;
import eu.eudat.data.entities.UserDMP;
import eu.eudat.data.entities.UserDatasetProfile;

import java.util.UUID;

/**
 * Created by ikalyvas on 2/8/2018.
 */
public interface UserDatasetProfileDao extends DatabaseAccessLayer<UserDatasetProfile, UUID> {
}
