package eu.eudat.data.entities;


import eu.eudat.data.converters.DateToUTCConverter;
import eu.eudat.data.entities.helpers.EntityBinder;
import eu.eudat.queryable.queryableentity.DataEntity;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.*;
import java.util.stream.Collectors;


@Entity
@Table(name = "\"Grant\"")
@NamedEntityGraphs({
		@NamedEntityGraph(
				name = "grantRecentActivity",
				attributeNodes = {@NamedAttributeNode(value = "dmps", subgraph = "dmps")},
				subgraphs = @NamedSubgraph(name = "dmps", attributeNodes = {@NamedAttributeNode("users")})
		),
		@NamedEntityGraph(
				name = "grantListingItem",
				attributeNodes = {@NamedAttributeNode(value = "dmps", subgraph = "dmps"), @NamedAttributeNode(value = "content")},
				subgraphs = @NamedSubgraph(name = "dmps", attributeNodes = {@NamedAttributeNode("creator"), @NamedAttributeNode("grant"), @NamedAttributeNode("users")})
		)
})
public class Grant implements DataEntity<Grant, UUID> {

	public enum Status {
		ACTIVE((short) 1), INACTIVE((short) 0), DELETED((short) 99);

		private short value;

		private Status(short value) {
			this.value = value;
		}

		public short getValue() {
			return value;
		}

		public static Status fromInteger(int value) {
			switch (value) {
				case 0:
					return INACTIVE;
				case 1:
					return ACTIVE;
				case 99:
					return DELETED;
				default:
					throw new RuntimeException("Unsupported Grant Status");
			}
		}
	}

	public enum GrantType {
		EXTERNAL(0), INTERNAL(1);

		private Integer value;

		private GrantType(Integer value) {
			this.value = value;
		}

		public Integer getValue() {
			return value;
		}

		public static GrantType fromInteger(int value) {
			switch (value) {
				case 0:
					return EXTERNAL;
				case 1:
					return INTERNAL;
				default:
					throw new RuntimeException("Unsupported Grant Type");
			}
		}
	}

	@Id
	//@GeneratedValue
	//@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(name = "\"ID\"", updatable = false, nullable = false, columnDefinition = "BINARY(16)")
	private UUID id;


	@OneToMany(mappedBy = "grant")
	private Set<DMP> dmps;

	@Column(name = "\"Label\"")
	private String label;

	@Column(name = "\"Abbreviation\"")
	private String abbreviation;

	@Column(name = "\"Reference\"", columnDefinition = "xml", nullable = true)
	private String reference;

	@Column(name = "\"Uri\"")
	private String uri;

	@Type(type = "eu.eudat.configurations.typedefinition.XMLType")
	@Column(name = "\"Definition\"", columnDefinition = "xml", nullable = true)
	private String definition;

	@Column(name = "\"StartDate\"", nullable = true)
	@Convert(converter = DateToUTCConverter.class)
	private Date startdate = null;

	@Column(name = "\"EndDate\"", nullable = true)
	@Convert(converter = DateToUTCConverter.class)
	private Date enddate = null;

	@Column(name = "\"Status\"", nullable = false)
	private Short status;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "\"CreationUser\"", nullable = true)
	private UserInfo creationUser;

	@Column(name = "\"Created\"")
	private Date created = null;

	@Column(name = "\"Modified\"")
	private Date modified = new Date();

	@Column(name = "\"Description\"")
	private String description;

	@Column(name = "\"Type\"")
	private Integer type;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "\"Content\"")
	private Content content;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "\"Funder\"")
	private Funder funder;

	public Grant() {
	}

	public Grant(Grant grant) {
		this.id = grant.getId();
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public Short getStatus() {
		return status;
	}
	public void setStatus(Short status) {
		this.status = status;
	}

	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getModified() {
		return modified;
	}
	public void setModified(Date modified) {
		this.modified = modified;
	}

	public Date getStartdate() {
		return startdate;
	}
	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	public Date getEnddate() {
		return enddate;
	}
	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}

	public UUID getId() {
		return id;
	}
	public void setId(UUID id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}

	public String getAbbreviation() {
		return abbreviation;
	}
	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getDefinition() {
		return definition;
	}
	public void setDefinition(String definition) {
		this.definition = definition;
	}

	public Set<DMP> getDmps() {
		return dmps;
	}
    public void setDmps(Set<DMP> dmps) {
        this.dmps = dmps;
    }

    public UserInfo getCreationUser() {
        return creationUser;
    }
    public void setCreationUser(UserInfo creationUser) {
        this.creationUser = creationUser;
    }

    public Integer getType() {
        return type;
    }
    public void setType(Integer type) {
        this.type = type;
    }

    public Content getContent() {
        return content;
    }
    public void setContent(Content content) {
        this.content = content;
    }

	public Funder getFunder() {
		return funder;
	}
	public void setFunder(Funder funder) {
		this.funder = funder;
	}

	@Override
    public void update(Grant entity) {
        this.description = entity.getDescription();
        this.label = entity.getLabel();
        this.abbreviation = entity.getAbbreviation();
        this.created = entity.getCreated();
        this.definition = entity.getDefinition();
        this.dmps = entity.getDmps();
        this.startdate = entity.getStartdate();
        this.enddate = entity.getEnddate();
        this.modified = new Date();
        this.uri = entity.getUri();
        this.funder = entity.getFunder();
        if (entity.getContent() != null) this.content = entity.getContent();
    }

    @Override
    public UUID getKeys() {
        return this.id;
    }

    @Override
	public Grant buildFromTuple(List<Tuple> tuple, List<String> fields, String base) {
		String currentBase = base.isEmpty() ? "" : base + ".";
		if (fields.contains(currentBase + "id")) this.id = EntityBinder.fromTuple(tuple, currentBase + "id");
		if (fields.contains(currentBase + "dmps"))
			this.dmps = tuple.stream().map(x -> new DMP().buildFromTuple(Arrays.asList(x), fields, currentBase + "dmps")).collect(Collectors.toSet());
		if (fields.contains(currentBase + "creationUser"))
			this.creationUser = tuple.stream().map(x -> new UserInfo().buildFromTuple(Arrays.asList(x), fields, currentBase + "creationUser")).collect(Collectors.toList()).get(0);
		return this;
	}
}
