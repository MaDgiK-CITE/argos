package eu.eudat.data.entities;

import eu.eudat.data.converters.DateToUTCConverter;
import eu.eudat.data.entities.helpers.EntityBinder;
import eu.eudat.queryable.queryableentity.DataEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;


@Entity
@Table(name = "\"ExternalDataset\"")
public class ExternalDataset implements DataEntity<ExternalDataset,UUID> {

    @Id
    @Column(name = "\"Id\"", updatable = false, nullable = false, columnDefinition = "BINARY(16)")
    private UUID id;

    @Column(name = "\"Label\"", nullable = false)
    private String label;

    @Column(name = "\"Abbreviation\"", nullable = false)
    private String abbreviation;

    @Column(name = "\"Reference\"", nullable = false)
    private String reference;

    @Column(name = "\"Created\"", nullable = false)
    @Convert(converter = DateToUTCConverter.class)
    private Date created;

    @Column(name = "\"Modified\"", nullable = false)
    @Convert(converter = DateToUTCConverter.class)
    private Date modified;

    @OneToMany(mappedBy = "externalDataset", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<DatasetExternalDataset> datasets;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "\"CreationUser\"", nullable = true)
    private UserInfo creationUser;


    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }

    public String getAbbreviation() {
        return abbreviation;
    }
    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getReference() {
        return reference;
    }
    public void setReference(String reference) {
        this.reference = reference;
    }

    public Date getCreated() {
        return created;
    }
    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }
    public void setModified(Date modified) {
        this.modified = modified;
    }

    public Set<DatasetExternalDataset> getDatasets() {
        return datasets;
    }
    public void setDatasets(Set<DatasetExternalDataset> datasets) {
        this.datasets = datasets;
    }

    public UserInfo getCreationUser() {
        return creationUser;
    }
    public void setCreationUser(UserInfo creationUser) {
        this.creationUser = creationUser;
    }

    @Override
    public void update(ExternalDataset entity) {
        this.label = entity.getLabel();
        this.abbreviation = entity.getAbbreviation();
        this.modified = new Date();
        this.creationUser = entity.getCreationUser();
    }

    @Override
    public UUID getKeys() {
        return this.id;
    }

    @Override
    public ExternalDataset buildFromTuple(List<Tuple> tuple, List<String> fields, String base) {
        String currentBase = base.isEmpty() ? "" : base + ".";
        if (fields.contains(currentBase + "id")) this.id = EntityBinder.fromTuple(tuple, currentBase + "id");
        return this;
    }
}
