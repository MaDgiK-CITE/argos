package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccessLayer;
import eu.eudat.data.dao.criteria.FunderCriteria;
import eu.eudat.data.entities.Funder;
import eu.eudat.data.entities.UserInfo;
import eu.eudat.queryable.QueryableList;

import java.util.UUID;

public interface FunderDao extends DatabaseAccessLayer<Funder, UUID> {

	QueryableList<Funder> getWithCritetia(FunderCriteria criteria);

	QueryableList<Funder> getAuthenticated(QueryableList<Funder> query, UserInfo principal);
}
