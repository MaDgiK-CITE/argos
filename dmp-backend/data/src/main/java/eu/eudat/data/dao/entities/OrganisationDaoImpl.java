package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccess;
import eu.eudat.data.dao.criteria.OrganisationCriteria;
import eu.eudat.data.dao.databaselayer.service.DatabaseService;
import eu.eudat.data.entities.DMP;
import eu.eudat.data.entities.Organisation;
import eu.eudat.data.entities.UserInfo;
import eu.eudat.queryable.QueryableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.JoinType;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Component("organisationDao")
public class OrganisationDaoImpl extends DatabaseAccess<Organisation> implements OrganisationDao {

    @Autowired
    public OrganisationDaoImpl(DatabaseService<Organisation> databaseService) {
        super(databaseService);
    }

    @Override
    public QueryableList<Organisation> getWithCriteria(OrganisationCriteria criteria) {
        QueryableList<Organisation> query = this.getDatabaseService().getQueryable(Organisation.class);
        if (criteria.getLabelLike() != null && criteria.getLike() != null) {
            query.where((builder, root) -> builder.or(builder.equal(root.get("reference"), criteria.getLike()), builder.like(builder.upper(root.get("label")), "%" + criteria.getLabelLike().toUpperCase() + "%")));
        } else {
            if (criteria.getLike() != null)
                query.where((builder, root) -> builder.equal(root.get("reference"), criteria.getLike()));
            if (criteria.getLabelLike() != null) {
                query.where((builder, root) -> builder.like(builder.upper(root.get("label")), "%" + criteria.getLabelLike().toUpperCase() + "%"));
            }
            if (criteria.getPublic() != null && criteria.getPublic()) {
                query.where((builder, root) -> builder.equal(root.join("dmps", JoinType.LEFT).get("status"), DMP.DMPStatus.FINALISED.getValue()));
            }
        }

        if (criteria.isActive()) {
            query.where((builder, root) -> builder.notEqual(root.join("dmps").get("status"), DMP.DMPStatus.DELETED.getValue())).distinct();
        }

        return query;
    }

    @Override
    public Organisation createOrUpdate(Organisation item) {
        return this.getDatabaseService().createOrUpdate(item, Organisation.class);
    }

    @Override
    public Organisation find(UUID id) {
        return this.getDatabaseService().getQueryable(Organisation.class).where((builder, root) -> builder.equal(root.get("id"), id)).getSingle();
    }

    @Override
    public void delete(Organisation item) {
        this.getDatabaseService().delete(item);
    }

    @Override
    public QueryableList<Organisation> asQueryable() {
        return this.getDatabaseService().getQueryable(Organisation.class);
    }

    @Async
    @Override
    public CompletableFuture<Organisation> createOrUpdateAsync(Organisation item) {
        return CompletableFuture.supplyAsync(() -> this.createOrUpdate(item));
    }

    public QueryableList<Organisation> getAuthenticated(QueryableList<Organisation> query, UserInfo principal) {
        query.where((builder, root) -> builder.equal(root.join("dmps").join("users").get("user"), principal));
        return query;
    }

    @Override
    public Organisation find(UUID id, String hint) {
        throw new UnsupportedOperationException();
    }
}
