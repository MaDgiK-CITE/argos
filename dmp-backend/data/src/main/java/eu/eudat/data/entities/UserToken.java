package eu.eudat.data.entities;

import eu.eudat.data.converters.DateToUTCConverter;
import eu.eudat.data.entities.helpers.EntityBinder;
import eu.eudat.queryable.queryableentity.DataEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.UUID;


@Entity
@Table(name = "\"UserToken\"")
public class UserToken implements DataEntity<UserToken, UUID> {

	@Id
	@Column(name = "\"Token\"", updatable = false, nullable = false, columnDefinition = "BINARY(16)")
	private UUID token;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "\"UserId\"", nullable = false)
	private UserInfo user;

	@Column(name = "\"IssuedAt\"", nullable = false)
	@Convert(converter = DateToUTCConverter.class)
	private Date issuedAt = null;


	@Column(name = "\"ExpiresAt\"", nullable = false)
	@Convert(converter = DateToUTCConverter.class)
	private Date expiresAt = null;

	public UUID getToken() {
		return token;
	}

	public void setToken(UUID token) {
		this.token = token;
	}

	public UserInfo getUser() {
		return user;
	}

	public void setUser(UserInfo user) {
		this.user = user;
	}

	public Date getIssuedAt() {
		return issuedAt;
	}

	public void setIssuedAt(Date issuedAt) {
		this.issuedAt = issuedAt;
	}

	public Date getExpiresAt() {
		return expiresAt;
	}

	public void setExpiresAt(Date expiresAt) {
		this.expiresAt = expiresAt;
	}

	@Override
	public void update(UserToken entity) {

	}

	@Override
	public UUID getKeys() {
		return this.token;
	}

	@Override
	public UserToken buildFromTuple(List<Tuple> tuple, List<String> fields, String base) {
		String currentBase = base.isEmpty() ? "" : base + ".";
		if (fields.contains(currentBase + "token")) this.token = EntityBinder.fromTuple(tuple, currentBase + "token");
		return this;
	}
}
