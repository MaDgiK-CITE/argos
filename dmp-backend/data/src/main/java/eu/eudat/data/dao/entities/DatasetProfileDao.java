package eu.eudat.data.dao.entities;

import eu.eudat.data.dao.DatabaseAccessLayer;
import eu.eudat.data.dao.criteria.DatasetProfileCriteria;
import eu.eudat.data.entities.DescriptionTemplate;
import eu.eudat.data.entities.DescriptionTemplateType;
import eu.eudat.queryable.QueryableList;

import java.util.List;
import java.util.UUID;

public interface DatasetProfileDao extends DatabaseAccessLayer<DescriptionTemplate, UUID> {

    QueryableList<DescriptionTemplate> getWithCriteria(DatasetProfileCriteria criteria);

    QueryableList<DescriptionTemplate> getAll();

    QueryableList<DescriptionTemplate> getAuthenticated(QueryableList<DescriptionTemplate> query, UUID principal, List<Integer> roles);

    List<DescriptionTemplate> getAllIds();

    Long countWithType(DescriptionTemplateType type);

}