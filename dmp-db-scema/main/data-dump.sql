INSERT INTO public."UserInfo"(email, authorization_level, usertype, userstatus, name, created, additionalinfo) VALUES ('fake@email.org', 1, 1, 0, :'ADMIN_USERNAME', now(), '{}');

INSERT INTO public."Credential" VALUES (uuid_generate_v4(), 0, 5, :'ADMIN_USERNAME', 'fake@email.org', :'ADMIN_PASSWORD', now(), now(), (SELECT public."UserInfo"."id" FROM public."UserInfo" WHERE name = :'ADMIN_USERNAME'), 'dmp');

INSERT INTO public."UserRole"("Role", "UserId") VALUES (2, (SELECT public."UserInfo"."id" FROM public."UserInfo" WHERE name = :'ADMIN_USERNAME'));

INSERT INTO public."DoiFunder"(name, doi) VALUES ('Australian Research Council',	'10.13039/501100000923');
INSERT INTO public."DoiFunder"(name, doi) VALUES ('European Commission', '10.13039/501100000780');
INSERT INTO public."DoiFunder"(name, doi) VALUES ('Fundação para a Ciência e a Tecnologia', '10.13039/501100001871');
INSERT INTO public."DoiFunder"(name, doi) VALUES ('Ministarstvo Prosvete, Nauke i Tehnološkog Razvoja', '10.13039/501100004564');
INSERT INTO public."DoiFunder"(name, doi) VALUES ('Ministarstvo Znanosti, Obrazovanja i Sporta',	'10.13039/501100006588');
INSERT INTO public."DoiFunder"(name, doi) VALUES ('National Health and Medical Research Council', '10.13039/501100000925');
INSERT INTO public."DoiFunder"(name, doi) VALUES ('National Science Foundation','10.13039/100000001');
INSERT INTO public."DoiFunder"(name, doi) VALUES ('Nederlandse Organisatie voor Wetenschappelijk Onderzoek',	'10.13039/501100003246');
INSERT INTO public."DoiFunder"(name, doi) VALUES ('Wellcome Trust', '10.13039/100004440');

UPDATE public."DMP"
	SET "extraProperties"='{"language": "en"}';
	
UPDATE public."DescriptionTemplate"
	SET  "Language"='en';

INSERT INTO public."DescriptionTemplateType"("ID", "Name", "Status") VALUES (uuid_generate_v4(), 'Dataset', 1);

INSERT INTO public."DBVersion" VALUES ('DMPDB', '00.00.007', '2020-10-27 13:40:00.000000+03', now(), 'Add userstatus on UserInfo table');