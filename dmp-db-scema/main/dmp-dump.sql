--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.26
-- Dumped by pg_dump version 11.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: Content; Type: TABLE; Schema: public; Owner: :POSTGRES_USER
--

CREATE TABLE public."Content" (
    "Id" uuid NOT NULL,
    "Filename" character varying NOT NULL,
    "Extension" character varying NOT NULL,
    "ParentType" numeric NOT NULL,
    "Uri" character varying NOT NULL,
    "LocationType" numeric NOT NULL
);


ALTER TABLE public."Content" OWNER TO :POSTGRES_USER;

--
-- Name: Credential; Type: TABLE; Schema: public; Owner: :POSTGRES_USER
--

CREATE TABLE public."Credential" (
    "Id" uuid NOT NULL,
    "Status" numeric NOT NULL,
    "Provider" numeric NOT NULL,
    "Public" character varying NOT NULL,
	"Email" character varying,
    "Secret" character varying NOT NULL,
    "CreationTime" timestamp(4) with time zone NOT NULL,
    "LastUpdateTime" timestamp(4) with time zone NOT NULL,
    "UserId" uuid NOT NULL,
    "ExternalId" character varying NOT NULL
);


ALTER TABLE public."Credential" OWNER TO :POSTGRES_USER;

--
-- Name: DBVersion; Type: TABLE; Schema: public; Owner: :POSTGRES_USER
--

CREATE TABLE public."DBVersion" (
    key character varying NOT NULL,
    version character varying NOT NULL,
    "releasedAt" timestamp with time zone NOT NULL,
    "deployedAt" timestamp with time zone,
    description text
);


ALTER TABLE public."DBVersion" OWNER TO :POSTGRES_USER;

--
-- Name: DMP; Type: TABLE; Schema: public; Owner: :POSTGRES_USER
--

CREATE TABLE public."DMP" (
    "ID" uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    "GroupId" uuid,
    "Label" character varying(250) NOT NULL,
    "Version" integer NOT NULL,
    "Grant" uuid,
    "AssociatedDmps" xml,
    "Profile" uuid,
    "Status" smallint DEFAULT 0 NOT NULL,
    "Created" timestamp(6) with time zone DEFAULT now() NOT NULL,
    "Modified" timestamp(6) with time zone DEFAULT now() NOT NULL,
    "Description" text,
    "Creator" uuid,
    "Properties" text,
    "DmpProperties" text,
    "FinalizedAt" timestamp(6) with time zone,
    "isPublic" boolean DEFAULT false NOT NULL,
    "PublishedAt" timestamp(6) with time zone,
    "DOI" text,
    "Project" uuid,
    "extraProperties" text
);


ALTER TABLE public."DMP" OWNER TO :POSTGRES_USER;

--
-- Name: COLUMN "DMP"."AssociatedDmps"; Type: COMMENT; Schema: public; Owner: :POSTGRES_USER
--

COMMENT ON COLUMN public."DMP"."AssociatedDmps" IS 'More data about the DMP as defined by the profile';


--
-- Name: DMPDatasetProfile; Type: TABLE; Schema: public; Owner: :POSTGRES_USER
--

CREATE TABLE public."DMPDatasetProfile" (
    "ID" uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    dmp uuid NOT NULL,
    datasetprofile uuid NOT NULL,
    "data" text NOT NULL
);


ALTER TABLE public."DMPDatasetProfile" OWNER TO :POSTGRES_USER;

--
-- Name: DMPOrganisation; Type: TABLE; Schema: public; Owner: :POSTGRES_USER
--

CREATE TABLE public."DMPOrganisation" (
    "DMP" uuid NOT NULL,
    "Organisation" uuid NOT NULL,
    "Role" integer,
    "ID" uuid DEFAULT public.uuid_generate_v4() NOT NULL
);


ALTER TABLE public."DMPOrganisation" OWNER TO :POSTGRES_USER;

--
-- Name: TABLE "DMPOrganisation"; Type: COMMENT; Schema: public; Owner: :POSTGRES_USER
--

COMMENT ON TABLE public."DMPOrganisation" IS 'Linking of DMPs to Organisations';


--
-- Name: COLUMN "DMPOrganisation"."Role"; Type: COMMENT; Schema: public; Owner: :POSTGRES_USER
--

COMMENT ON COLUMN public."DMPOrganisation"."Role" IS 'Enumerator of roles';


--
-- Name: DMPProfile; Type: TABLE; Schema: public; Owner: :POSTGRES_USER
--

CREATE TABLE public."DMPProfile" (
    "ID" uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    "Label" character varying(250) NOT NULL,
    "Definition" xml,
    "Status" smallint DEFAULT 0 NOT NULL,
    "Created" timestamp(6) with time zone DEFAULT now() NOT NULL,
    "Modified" timestamp(6) with time zone DEFAULT now() NOT NULL
);


ALTER TABLE public."DMPProfile" OWNER TO :POSTGRES_USER;

--
-- Name: DMPResearcher; Type: TABLE; Schema: public; Owner: :POSTGRES_USER
--

CREATE TABLE public."DMPResearcher" (
    "DMP" uuid NOT NULL,
    "Researcher" uuid NOT NULL,
    "Role" integer,
    "ID" uuid DEFAULT public.uuid_generate_v4() NOT NULL
);


ALTER TABLE public."DMPResearcher" OWNER TO :POSTGRES_USER;

--
-- Name: TABLE "DMPResearcher"; Type: COMMENT; Schema: public; Owner: :POSTGRES_USER
--

COMMENT ON TABLE public."DMPResearcher" IS 'Linking of DMPs to researchers';


--
-- Name: COLUMN "DMPResearcher"."Role"; Type: COMMENT; Schema: public; Owner: :POSTGRES_USER
--

COMMENT ON COLUMN public."DMPResearcher"."Role" IS 'Enumerator of roles';


--
-- Name: DataRepository; Type: TABLE; Schema: public; Owner: :POSTGRES_USER
--

CREATE TABLE public."DataRepository" (
    "ID" uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    "Label" character varying(250),
    "Abbreviation" character varying(50),
    "Reference" character varying,
    "Uri" character varying(250),
    "Definition" xml,
    "Status" smallint DEFAULT 0 NOT NULL,
    "Created" timestamp without time zone DEFAULT now() NOT NULL,
    "Modified" timestamp without time zone DEFAULT now() NOT NULL,
    "CreationUser" uuid
);


ALTER TABLE public."DataRepository" OWNER TO :POSTGRES_USER;

--
-- Name: Dataset; Type: TABLE; Schema: public; Owner: :POSTGRES_USER
--

CREATE TABLE public."Dataset" (
    "ID" uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    "Label" character varying(250) NOT NULL,
    "DMP" uuid,
    "DmpSectionIndex" integer NOT NULL,
    "Uri" character varying(250),
    "Properties" text,
    "Profile" uuid,
    "Reference" text,
    "Status" smallint DEFAULT 0 NOT NULL,
    "Created" timestamp(6) with time zone DEFAULT now() NOT NULL,
    "Modified" timestamp(6) with time zone DEFAULT now() NOT NULL,
    "Description" text,
    "Creator" uuid,
    "FinalizedAt" timestamp(6) with time zone
);


ALTER TABLE public."Dataset" OWNER TO :POSTGRES_USER;

--
-- Name: COLUMN "Dataset"."Uri"; Type: COMMENT; Schema: public; Owner: :POSTGRES_USER
--

COMMENT ON COLUMN public."Dataset"."Uri" IS 'URI of item';


--
-- Name: COLUMN "Dataset"."Properties"; Type: COMMENT; Schema: public; Owner: :POSTGRES_USER
--

COMMENT ON COLUMN public."Dataset"."Properties" IS 'More data about the dataset such as Uri, data types etc as defined by the profile';


--
-- Name: DatasetDataRepository; Type: TABLE; Schema: public; Owner: :POSTGRES_USER
--

CREATE TABLE public."DatasetDataRepository" (
    "Dataset" uuid NOT NULL,
    "DataRepository" uuid NOT NULL,
    "Role" integer,
    "ID" uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    "Data" character varying
);


ALTER TABLE public."DatasetDataRepository" OWNER TO :POSTGRES_USER;

--
-- Name: TABLE "DatasetDataRepository"; Type: COMMENT; Schema: public; Owner: :POSTGRES_USER
--

COMMENT ON TABLE public."DatasetDataRepository" IS 'Linking Dataset to DataRepository';


--
-- Name: DatasetExternalDataset; Type: TABLE; Schema: public; Owner: :POSTGRES_USER
--

CREATE TABLE public."DatasetExternalDataset" (
    "Id" uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    "Dataset" uuid NOT NULL,
    "ExternalDataset" uuid NOT NULL,
    "Role" numeric,
    "Data" character varying
);


ALTER TABLE public."DatasetExternalDataset" OWNER TO :POSTGRES_USER;

--
-- Name: DescriptionTemplateType; Type: TABLE; Schema: public; Owner: :POSTGRES_USER
--

CREATE TABLE public."DescriptionTemplateType" (
    "ID" uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    "Name" character varying(250) NOT NULL,
    "Status" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE public."DescriptionTemplateType" OWNER TO :POSTGRES_USER;

--
-- Name: DescriptionTemplate; Type: TABLE; Schema: public; Owner: :POSTGRES_USER
--

CREATE TABLE public."DescriptionTemplate" (
    "ID" uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    "Label" character varying(250) NOT NULL,
    "Definition" xml NOT NULL,
    "Status" smallint DEFAULT 0 NOT NULL,
    "Created" timestamp(6) with time zone DEFAULT now() NOT NULL,
    "Modified" timestamp(6) with time zone DEFAULT now() NOT NULL,
    "Description" text NOT NULL,
    "GroupId" uuid NOT NULL,
    "Version" integer DEFAULT 0 NOT NULL,
    "Language" character varying NOT NULL,
    "Type" uuid NOT NULL
);


ALTER TABLE public."DescriptionTemplate" OWNER TO :POSTGRES_USER;

--
-- Name: TABLE "DescriptionTemplate"; Type: COMMENT; Schema: public; Owner: :POSTGRES_USER
--

COMMENT ON TABLE public."DescriptionTemplate" IS 'Profiles for dmp descriptions';


--
-- Name: DatasetProfileRuleset; Type: TABLE; Schema: public; Owner: :POSTGRES_USER
--

CREATE TABLE public."DatasetProfileRuleset" (
    "ID" uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    "Label" character varying(250) NOT NULL,
    "Definition" xml NOT NULL
);


ALTER TABLE public."DatasetProfileRuleset" OWNER TO :POSTGRES_USER;

--
-- Name: TABLE "DatasetProfileRuleset"; Type: COMMENT; Schema: public; Owner: :POSTGRES_USER
--

COMMENT ON TABLE public."DatasetProfileRuleset" IS 'Sets of Rules for dmp dataset profiles';


--
-- Name: DatasetProfileViewstyle; Type: TABLE; Schema: public; Owner: :POSTGRES_USER
--

CREATE TABLE public."DatasetProfileViewstyle" (
    "ID" uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    "Label" character varying(250) NOT NULL,
    "Definition" xml NOT NULL
);


ALTER TABLE public."DatasetProfileViewstyle" OWNER TO :POSTGRES_USER;

--
-- Name: TABLE "DatasetProfileViewstyle"; Type: COMMENT; Schema: public; Owner: :POSTGRES_USER
--

COMMENT ON TABLE public."DatasetProfileViewstyle" IS 'Style sets for dmp dataset profiles';


--
-- Name: DatasetRegistry; Type: TABLE; Schema: public; Owner: :POSTGRES_USER
--

CREATE TABLE public."DatasetRegistry" (
    "Dataset" uuid NOT NULL,
    "Registry" uuid NOT NULL,
    "Role" integer,
    "ID" uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    "Data" character varying
);


ALTER TABLE public."DatasetRegistry" OWNER TO :POSTGRES_USER;

--
-- Name: TABLE "DatasetRegistry"; Type: COMMENT; Schema: public; Owner: :POSTGRES_USER
--

COMMENT ON TABLE public."DatasetRegistry" IS 'Linking Dataset to Registry';


--
-- Name: DatasetService; Type: TABLE; Schema: public; Owner: :POSTGRES_USER
--

CREATE TABLE public."DatasetService" (
    "Dataset" uuid NOT NULL,
    "Service" uuid NOT NULL,
    "Role" integer,
    "ID" uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    "Data" character varying
);


ALTER TABLE public."DatasetService" OWNER TO :POSTGRES_USER;

--
-- Name: TABLE "DatasetService"; Type: COMMENT; Schema: public; Owner: :POSTGRES_USER
--

COMMENT ON TABLE public."DatasetService" IS 'Linking Dataset to Service';


--
-- Name: DoiFunder; Type: TABLE; Schema: public; Owner: :POSTGRES_USER
--

CREATE TABLE public."DoiFunder" (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name character varying,
    doi character varying
);


ALTER TABLE public."DoiFunder" OWNER TO :POSTGRES_USER;

--
-- Name: ExternalDataset; Type: TABLE; Schema: public; Owner: :POSTGRES_USER
--

CREATE TABLE public."ExternalDataset" (
    "Id" uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    "Label" character varying NOT NULL,
    "Abbreviation" character varying,
    "Reference" character varying NOT NULL,
    "Created" timestamp(4) with time zone NOT NULL,
    "Modified" timestamp(4) with time zone NOT NULL,
    "CreationUser" uuid
);


ALTER TABLE public."ExternalDataset" OWNER TO :POSTGRES_USER;

--
-- Name: Funder; Type: TABLE; Schema: public; Owner: :POSTGRES_USER
--

CREATE TABLE public."Funder" (
    "ID" uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    "Label" character varying(250) NOT NULL,
    "Reference" character varying,
    "Definition" character varying,
    "Status" smallint DEFAULT 0 NOT NULL,
    "Created" timestamp(4) with time zone DEFAULT now() NOT NULL,
    "Modified" timestamp(4) with time zone DEFAULT now() NOT NULL,
    "Type" numeric DEFAULT 0 NOT NULL,
    "CreationUser" uuid
);


ALTER TABLE public."Funder" OWNER TO :POSTGRES_USER;

--
-- Name: Grant; Type: TABLE; Schema: public; Owner: :POSTGRES_USER
--

CREATE TABLE public."Grant" (
    "ID" uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    "Label" character varying(250) NOT NULL,
    "Abbreviation" character varying(50),
    "Reference" character varying,
    "Uri" character varying(250),
    "Definition" character varying,
    "Status" smallint DEFAULT 0 NOT NULL,
    "Created" timestamp(4) with time zone DEFAULT now() NOT NULL,
    "Modified" timestamp(4) with time zone DEFAULT now() NOT NULL,
    "StartDate" timestamp(4) with time zone,
    "EndDate" timestamp(4) with time zone,
    "Description" text,
    "CreationUser" uuid DEFAULT '332ffc36-bd51-4d4e-bf9a-ffb01fdee05a'::uuid,
    "Type" numeric DEFAULT 0 NOT NULL,
    "Content" uuid,
    "Funder" uuid
);


ALTER TABLE public."Grant" OWNER TO :POSTGRES_USER;

--
-- Name: TABLE "Grant"; Type: COMMENT; Schema: public; Owner: :POSTGRES_USER
--

COMMENT ON TABLE public."Grant" IS 'Table of project managed in the system';


--
-- Name: COLUMN "Grant"."ID"; Type: COMMENT; Schema: public; Owner: :POSTGRES_USER
--

COMMENT ON COLUMN public."Grant"."ID" IS 'Unique identifier and primary key of item';


--
-- Name: COLUMN "Grant"."Label"; Type: COMMENT; Schema: public; Owner: :POSTGRES_USER
--

COMMENT ON COLUMN public."Grant"."Label" IS 'A human readable long label of the item';


--
-- Name: COLUMN "Grant"."Abbreviation"; Type: COMMENT; Schema: public; Owner: :POSTGRES_USER
--

COMMENT ON COLUMN public."Grant"."Abbreviation" IS 'A human readable abbreviation of the item';


--
-- Name: COLUMN "Grant"."Reference"; Type: COMMENT; Schema: public; Owner: :POSTGRES_USER
--

COMMENT ON COLUMN public."Grant"."Reference" IS 'Additional reference data for the item along with information to allow how the item reached the system (e.g. via an external vocabulary)';


--
-- Name: COLUMN "Grant"."Uri"; Type: COMMENT; Schema: public; Owner: :POSTGRES_USER
--

COMMENT ON COLUMN public."Grant"."Uri" IS 'URI of item';


--
-- Name: COLUMN "Grant"."Definition"; Type: COMMENT; Schema: public; Owner: :POSTGRES_USER
--

COMMENT ON COLUMN public."Grant"."Definition" IS 'More data about the project such as web site, start/stop, etc';


--
-- Name: Invitation; Type: TABLE; Schema: public; Owner: :POSTGRES_USER
--

CREATE TABLE public."Invitation" (
    "Id" uuid NOT NULL,
    "InvitationEmail" character varying NOT NULL,
    "Token" uuid NOT NULL,
    "CreationUser" uuid NOT NULL,
    "Dmp" uuid NOT NULL,
    "Properties" xml,
    "AcceptedInvitation" boolean
);


ALTER TABLE public."Invitation" OWNER TO :POSTGRES_USER;

--
-- Name: Lock; Type: TABLE; Schema: public; Owner: :POSTGRES_USER
--

CREATE TABLE public."Lock" (
    id uuid NOT NULL,
    "Target" uuid NOT NULL,
    "LockedBy" uuid NOT NULL,
    "LockedAt" timestamp without time zone NOT NULL,
    "TouchedAt" timestamp without time zone
);


ALTER TABLE public."Lock" OWNER TO :POSTGRES_USER;

--
-- Name: LoginConfirmationEmail; Type: TABLE; Schema: public; Owner: :POSTGRES_USER
--

CREATE TABLE public."EmailConfirmation" (
    "ID" uuid NOT NULL,
    email character varying NOT NULL,
    "isConfirmed" boolean NOT NULL,
    token uuid NOT NULL,
    "userId" uuid NOT NULL,
    "expiresAt" timestamp(4) with time zone NOT NULL,
	data text
);


ALTER TABLE public."EmailConfirmation" OWNER TO :POSTGRES_USER;

--
-- Name: Notification; Type: TABLE; Schema: public; Owner: :POSTGRES_USER
--

CREATE TABLE public."Notification" (
    id uuid NOT NULL,
    "UserId" uuid,
    "IsActive" integer NOT NULL,
    "Type" integer NOT NULL,
    "ContactTypeHint" integer,
    "ContactHint" character varying,
    "Data" character varying,
    "NotifyState" integer NOT NULL,
    "NotifiedAt" timestamp without time zone,
    "RetryCount" integer,
    "CreatedAt" timestamp without time zone,
    "UpdatedAt" timestamp without time zone
);


ALTER TABLE public."Notification" OWNER TO :POSTGRES_USER;

--
-- Name: Organisation; Type: TABLE; Schema: public; Owner: :POSTGRES_USER
--

CREATE TABLE public."Organisation" (
    "ID" uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    "Label" character varying(250) NOT NULL,
    "Abbreviation" character varying(50),
    "Reference" character varying,
    "Uri" character varying(250),
    "Definition" xml,
    "Status" smallint DEFAULT 0 NOT NULL,
    "Created" timestamp(6) with time zone DEFAULT now() NOT NULL,
    "Modified" timestamp(6) with time zone DEFAULT now() NOT NULL
);


ALTER TABLE public."Organisation" OWNER TO :POSTGRES_USER;

--
-- Name: TABLE "Organisation"; Type: COMMENT; Schema: public; Owner: :POSTGRES_USER
--

COMMENT ON TABLE public."Organisation" IS 'Table of organizations utilized in the project';


--
-- Name: COLUMN "Organisation"."ID"; Type: COMMENT; Schema: public; Owner: :POSTGRES_USER
--

COMMENT ON COLUMN public."Organisation"."ID" IS 'Unique identifier and primary key of item';


--
-- Name: COLUMN "Organisation"."Label"; Type: COMMENT; Schema: public; Owner: :POSTGRES_USER
--

COMMENT ON COLUMN public."Organisation"."Label" IS 'A human readable long label of the item';


--
-- Name: COLUMN "Organisation"."Abbreviation"; Type: COMMENT; Schema: public; Owner: :POSTGRES_USER
--

COMMENT ON COLUMN public."Organisation"."Abbreviation" IS 'A human readable abbreviation of the item';


--
-- Name: COLUMN "Organisation"."Reference"; Type: COMMENT; Schema: public; Owner: :POSTGRES_USER
--

COMMENT ON COLUMN public."Organisation"."Reference" IS 'Reference to the URI of the item along with information to allow how the item reached the system (e.g. via an external vocabulary)';


--
-- Name: COLUMN "Organisation"."Uri"; Type: COMMENT; Schema: public; Owner: :POSTGRES_USER
--

COMMENT ON COLUMN public."Organisation"."Uri" IS 'URI of item';


--
-- Name: COLUMN "Organisation"."Definition"; Type: COMMENT; Schema: public; Owner: :POSTGRES_USER
--

COMMENT ON COLUMN public."Organisation"."Definition" IS 'More data about the Organisation such as web site, type etc';


--
-- Name: Project; Type: TABLE; Schema: public; Owner: :POSTGRES_USER
--

CREATE TABLE public."Project" (
    "ID" uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    "Label" character varying(250) NOT NULL,
    "Abbreviation" character varying(50),
    "Reference" character varying,
    "Uri" character varying(250),
    "Definition" character varying,
    "Status" smallint DEFAULT 0 NOT NULL,
    "Created" timestamp(4) with time zone DEFAULT now() NOT NULL,
    "Modified" timestamp(4) with time zone DEFAULT now() NOT NULL,
    "StartDate" timestamp(4) with time zone,
    "EndDate" timestamp(4) with time zone,
    "Description" text,
    "CreationUser" uuid,
    "Type" numeric DEFAULT 0 NOT NULL,
    "Content" uuid
);


ALTER TABLE public."Project" OWNER TO :POSTGRES_USER;

--
-- Name: Registry; Type: TABLE; Schema: public; Owner: :POSTGRES_USER
--

CREATE TABLE public."Registry" (
    "ID" uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    "Label" character varying(250),
    "Abbreviation" character varying(50),
    "Reference" character varying,
    "Uri" character varying(250),
    "Definition" xml,
    "Status" smallint DEFAULT 0 NOT NULL,
    "Created" timestamp(6) with time zone DEFAULT now() NOT NULL,
    "Modified" timestamp(6) with time zone DEFAULT now() NOT NULL,
    "CreationUser" uuid
);


ALTER TABLE public."Registry" OWNER TO :POSTGRES_USER;

--
-- Name: Researcher; Type: TABLE; Schema: public; Owner: :POSTGRES_USER
--

CREATE TABLE public."Researcher" (
    "ID" uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    "Label" character varying(250) NOT NULL,
    "Uri" character varying(250),
    "PrimaryEmail" character varying(250),
    "Definition" xml,
    "Reference" character varying,
    "Status" smallint DEFAULT 0 NOT NULL,
    "Created" timestamp(6) with time zone DEFAULT now() NOT NULL,
    "Modified" timestamp(6) with time zone DEFAULT now() NOT NULL,
    "CreationUser" uuid
);


ALTER TABLE public."Researcher" OWNER TO :POSTGRES_USER;

--
-- Name: TABLE "Researcher"; Type: COMMENT; Schema: public; Owner: :POSTGRES_USER
--

COMMENT ON TABLE public."Researcher" IS 'Table of Researcher managed in the system';


--
-- Name: COLUMN "Researcher"."ID"; Type: COMMENT; Schema: public; Owner: :POSTGRES_USER
--

COMMENT ON COLUMN public."Researcher"."ID" IS 'Unique identifier and primary key of item';


--
-- Name: COLUMN "Researcher"."Label"; Type: COMMENT; Schema: public; Owner: :POSTGRES_USER
--

COMMENT ON COLUMN public."Researcher"."Label" IS 'Full name of the researcher (as presented by the system, and composed automatically by data or provided by the reference service)';


--
-- Name: COLUMN "Researcher"."Uri"; Type: COMMENT; Schema: public; Owner: :POSTGRES_USER
--

COMMENT ON COLUMN public."Researcher"."Uri" IS 'URI of item';


--
-- Name: COLUMN "Researcher"."Definition"; Type: COMMENT; Schema: public; Owner: :POSTGRES_USER
--

COMMENT ON COLUMN public."Researcher"."Definition" IS 'More data about the researcher such as: email addresses, affiliations etc';


--
-- Name: COLUMN "Researcher"."Reference"; Type: COMMENT; Schema: public; Owner: :POSTGRES_USER
--

COMMENT ON COLUMN public."Researcher"."Reference" IS 'Additional reference data for the item along with information to allow how the item reached the system (e.g. via an external vocabulary)';


--
-- Name: Service; Type: TABLE; Schema: public; Owner: :POSTGRES_USER
--

CREATE TABLE public."Service" (
    "ID" uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    "Label" character varying(250),
    "Abbreviation" character varying(50),
    "Reference" character varying,
    "Uri" character varying(250),
    "Definition" xml,
    "Status" smallint DEFAULT 0 NOT NULL,
    "Created" timestamp(6) with time zone DEFAULT now() NOT NULL,
    "Modified" timestamp(6) with time zone DEFAULT now() NOT NULL,
    "CreationUser" uuid
);


ALTER TABLE public."Service" OWNER TO :POSTGRES_USER;

--
-- Name: UserDatasetProfile; Type: TABLE; Schema: public; Owner: :POSTGRES_USER
--

CREATE TABLE public."UserDatasetProfile" (
    id uuid NOT NULL,
    "usr" uuid NOT NULL,
    "datasetProfile" uuid NOT NULL,
    role integer
);

ALTER TABLE public."UserDatasetProfile" OWNER TO :POSTGRES_USER;

--
-- Name: UserDMP; Type: TABLE; Schema: public; Owner: :POSTGRES_USER
--

CREATE TABLE public."UserDMP" (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    usr uuid NOT NULL,
    dmp uuid NOT NULL,
    role integer
);


ALTER TABLE public."UserDMP" OWNER TO :POSTGRES_USER;

--
-- Name: UserInfo; Type: TABLE; Schema: public; Owner: :POSTGRES_USER
--

CREATE TABLE public."UserInfo" (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    email character varying(250),
    authorization_level smallint NOT NULL,
    usertype smallint NOT NULL,
	userstatus smallint NOT NULL,
    verified_email boolean,
    name character varying(250),
    created timestamp(6) with time zone,
    lastloggedin timestamp(6) with time zone,
    additionalinfo json
);


ALTER TABLE public."UserInfo" OWNER TO :POSTGRES_USER;

--
-- Name: COLUMN "UserInfo".authorization_level; Type: COMMENT; Schema: public; Owner: :POSTGRES_USER
--

COMMENT ON COLUMN public."UserInfo".authorization_level IS 'This stores the authorization level of the user: 0 admin, 1 user, being able to be extended furthermore';


--
-- Name: COLUMN "UserInfo".usertype; Type: COMMENT; Schema: public; Owner: :POSTGRES_USER
--

COMMENT ON COLUMN public."UserInfo".usertype IS 'This stores the type of user: 0 -> internal, 1 external';


--
-- Name: UserPreference; Type: TABLE; Schema: public; Owner: :POSTGRES_USER
--

CREATE TABLE public."UserPreference" (
    "Id" uuid NOT NULL,
    "UserId" uuid NOT NULL,
    "Data" json NOT NULL,
    "PreferenceType" smallint NOT NULL
);


ALTER TABLE public."UserPreference" OWNER TO :POSTGRES_USER;

--
-- Name: UserRole; Type: TABLE; Schema: public; Owner: :POSTGRES_USER
--

CREATE TABLE public."UserRole" (
    "Id" uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    "Role" numeric DEFAULT 0 NOT NULL,
    "UserId" uuid NOT NULL
);


ALTER TABLE public."UserRole" OWNER TO :POSTGRES_USER;

--
-- Name: UserToken; Type: TABLE; Schema: public; Owner: :POSTGRES_USER
--

CREATE TABLE public."UserToken" (
    "Token" uuid NOT NULL,
    "UserId" uuid NOT NULL,
    "IssuedAt" timestamp(4) with time zone NOT NULL,
    "ExpiresAt" timestamp(4) with time zone NOT NULL
);


ALTER TABLE public."UserToken" OWNER TO :POSTGRES_USER;

--
-- Name: FileUpload; Type: TABLE; Schema: public; Owner: :POSTGRES_USER
--

CREATE TYPE EntityType AS ENUM ('DATASET', 'DMP');

CREATE TABLE public."FileUpload"
(
    "ID" uuid NOT NULL,
    "Name" character varying(250) NOT NULL,
    "FileType" character varying(50) NOt NULL,
    "EntityId" uuid NOT NULL,
    "EntityType" EntityType NOT NULL,
    "CreatedAt" timestamp(6) with time zone DEFAULT now() NOT NULL,
    "IsDeleted" boolean DEFAULT false NOT NULL,
    "Creator" uuid NOT NULL,
    CONSTRAINT "File_pkey" PRIMARY KEY ("ID"),
    CONSTRAINT fk_fileupload_creator FOREIGN KEY ("Creator") REFERENCES public."UserInfo"(id)
);

ALTER TABLE public."FileUpload" OWNER TO :POSTGRES_USER;

--
-- Name: EntityDoi; Type: TABLE; Schema: public; Owner: :POSTGRES_USER
--

CREATE TYPE DoiEntityType AS ENUM ('DMP');

CREATE TABLE public."EntityDoi"
(
    "ID" uuid NOT NULL,
    "EntityType" DoiEntityType NOT NULL,
    "RepositoryId" character varying(150) NOT NULL,
    "Doi" character varying(50) NOt NULL,
    "CreatedAt" timestamp(6) with time zone DEFAULT now() NOT NULL,
    "UpdatedAt" timestamp(6) with time zone DEFAULT now() NOT NULL,
    "EntityId" uuid NOT NULL,
    CONSTRAINT "Doi_pkey" PRIMARY KEY ("ID"),
    CONSTRAINT fk_doi_entityId FOREIGN KEY ("EntityId") REFERENCES public."DMP"("ID")
);

ALTER TABLE public."EntityDoi" OWNER TO :POSTGRES_USER;

--
-- Name: Content Content_pkey; Type: CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."Content"
    ADD CONSTRAINT "Content_pkey" PRIMARY KEY ("Id");


--
-- Name: Credential Credential_pkey; Type: CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."Credential"
    ADD CONSTRAINT "Credential_pkey" PRIMARY KEY ("Id");


--
-- Name: DMPDatasetProfile DMPDatasetProfile_pkey; Type: CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."DMPDatasetProfile"
    ADD CONSTRAINT "DMPDatasetProfile_pkey" PRIMARY KEY ("ID");


--
-- Name: DMPProfile DMPPRofile_pkey; Type: CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."DMPProfile"
    ADD CONSTRAINT "DMPPRofile_pkey" PRIMARY KEY ("ID");


--
-- Name: DMP DMP_pkey; Type: CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."DMP"
    ADD CONSTRAINT "DMP_pkey" PRIMARY KEY ("ID");


--
-- Name: DatasetDataRepository DatasetDataRepository_pkey; Type: CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."DatasetDataRepository"
    ADD CONSTRAINT "DatasetDataRepository_pkey" PRIMARY KEY ("ID");


--
-- Name: DatasetExternalDataset DatasetExternalDataset_pkey; Type: CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."DatasetExternalDataset"
    ADD CONSTRAINT "DatasetExternalDataset_pkey" PRIMARY KEY ("Id");


--
-- Name: DatasetProfileRuleset DatasetProfileRuleset_pkey; Type: CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."DatasetProfileRuleset"
    ADD CONSTRAINT "DatasetProfileRuleset_pkey" PRIMARY KEY ("ID");


--
-- Name: DatasetProfileViewstyle DatasetProfileViewstyle_pkey; Type: CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."DatasetProfileViewstyle"
    ADD CONSTRAINT "DatasetProfileViewstyle_pkey" PRIMARY KEY ("ID");


--
-- Name: DescriptionTemplateType DescriptionTemplateType_pkey; Type: CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."DescriptionTemplateType"
    ADD CONSTRAINT "DescriptionTemplateType_pkey" PRIMARY KEY ("ID");


--
-- Name: DescriptionTemplate DescriptionTemplate_pkey; Type: CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."DescriptionTemplate"
    ADD CONSTRAINT "DescriptionTemplate_pkey" PRIMARY KEY ("ID");


--
-- Name: DatasetRegistry DatasetRegistry_pkey; Type: CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."DatasetRegistry"
    ADD CONSTRAINT "DatasetRegistry_pkey" PRIMARY KEY ("ID");


--
-- Name: DatasetService DatasetService_pkey; Type: CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."DatasetService"
    ADD CONSTRAINT "DatasetService_pkey" PRIMARY KEY ("ID");


--
-- Name: Dataset Dataset_pkey; Type: CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."Dataset"
    ADD CONSTRAINT "Dataset_pkey" PRIMARY KEY ("ID");


--
-- Name: DoiFunder DoiFunder_pkey; Type: CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."DoiFunder"
    ADD CONSTRAINT "DoiFunder_pkey" PRIMARY KEY (id);


--
-- Name: ExternalDataset ExternalDataset_pkey; Type: CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."ExternalDataset"
    ADD CONSTRAINT "ExternalDataset_pkey" PRIMARY KEY ("Id");


--
-- Name: Funder Funder_pkey; Type: CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."Funder"
    ADD CONSTRAINT "Funder_pkey" PRIMARY KEY ("ID");


--
-- Name: Grant Grant_pkey; Type: CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."Grant"
    ADD CONSTRAINT "Grant_pkey" PRIMARY KEY ("ID");


--
-- Name: Invitation Invitation_pkey; Type: CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."Invitation"
    ADD CONSTRAINT "Invitation_pkey" PRIMARY KEY ("Id");


--
-- Name: Lock Lock_pkey; Type: CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."Lock"
    ADD CONSTRAINT "Lock_pkey" PRIMARY KEY (id);


--
-- Name: LoginConfirmationEmail LoginConfirmationEmail_pkey; Type: CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."EmailConfirmation"
    ADD CONSTRAINT "EmailConfirmation_pkey" PRIMARY KEY ("ID");


--
-- Name: Notification Notification_pkey; Type: CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."Notification"
    ADD CONSTRAINT "Notification_pkey" PRIMARY KEY (id);


--
-- Name: Organisation Organisation_pkey; Type: CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."Organisation"
    ADD CONSTRAINT "Organisation_pkey" PRIMARY KEY ("ID");


--
-- Name: DMPOrganisation PKey_DMPOrganisation; Type: CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."DMPOrganisation"
    ADD CONSTRAINT "PKey_DMPOrganisation" PRIMARY KEY ("ID");


--
-- Name: DMPResearcher PKey_DMPResearcher; Type: CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."DMPResearcher"
    ADD CONSTRAINT "PKey_DMPResearcher" PRIMARY KEY ("ID");


--
-- Name: DataRepository PKey_DataRepository; Type: CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."DataRepository"
    ADD CONSTRAINT "PKey_DataRepository" PRIMARY KEY ("ID");


--
-- Name: Registry PKey_Registry; Type: CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."Registry"
    ADD CONSTRAINT "PKey_Registry" PRIMARY KEY ("ID");


--
-- Name: Service PKey_Service; Type: CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."Service"
    ADD CONSTRAINT "PKey_Service" PRIMARY KEY ("ID");


--
-- Name: Project Project_pkey; Type: CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."Project"
    ADD CONSTRAINT "Project_pkey" PRIMARY KEY ("ID");


--
-- Name: Researcher Researcher_pkey; Type: CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."Researcher"
    ADD CONSTRAINT "Researcher_pkey" PRIMARY KEY ("ID");

--
-- Name: UserDatasetProfile UserDatasetProfile_pkey; Type: CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."UserDatasetProfile"
	ADD CONSTRAINT "UserDatasetProfile_pkey" PRIMARY KEY (id);

--
-- Name: UserDMP UserDMP_pkey; Type: CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."UserDMP"
    ADD CONSTRAINT "UserDMP_pkey" PRIMARY KEY (id);


--
-- Name: UserInfo UserInfo_email_key; Type: CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."UserInfo"
    ADD CONSTRAINT "UserInfo_email_key" UNIQUE (email);


--
-- Name: UserInfo UserInfo_pkey; Type: CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."UserInfo"
    ADD CONSTRAINT "UserInfo_pkey" PRIMARY KEY (id);


--
-- Name: UserPreference UserPreference_pkey; Type: CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."UserPreference"
    ADD CONSTRAINT "UserPreference_pkey" PRIMARY KEY ("Id");


--
-- Name: UserRole UserRole_pkey; Type: CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."UserRole"
    ADD CONSTRAINT "UserRole_pkey" PRIMARY KEY ("Id");


--
-- Name: UserToken UserToken_pkey; Type: CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."UserToken"
    ADD CONSTRAINT "UserToken_pkey" PRIMARY KEY ("Token");


--
-- Name: fki_DMPDMPProfileReference; Type: INDEX; Schema: public; Owner: :POSTGRES_USER
--

CREATE INDEX "fki_DMPDMPProfileReference" ON public."DMP" USING btree ("Profile");


--
-- Name: fki_DatasetDatasetProfileReference; Type: INDEX; Schema: public; Owner: :POSTGRES_USER
--

CREATE INDEX "fki_DatasetDatasetProfileReference" ON public."Dataset" USING btree ("Profile");


--
-- Name: DMP DMPDMPProfileReference; Type: FK CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."DMP"
    ADD CONSTRAINT "DMPDMPProfileReference" FOREIGN KEY ("Profile") REFERENCES public."DMPProfile"("ID");


--
-- Name: DMPDatasetProfile DMPDatasetProfile_datasetprofile_fkey; Type: FK CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."DMPDatasetProfile"
    ADD CONSTRAINT "DMPDatasetProfile_datasetprofile_fkey" FOREIGN KEY (datasetprofile) REFERENCES public."DescriptionTemplate"("ID");


--
-- Name: DMPDatasetProfile DMPDatasetProfile_dmp_fkey; Type: FK CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."DMPDatasetProfile"
    ADD CONSTRAINT "DMPDatasetProfile_dmp_fkey" FOREIGN KEY (dmp) REFERENCES public."DMP"("ID");


--
-- Name: DMPOrganisation DMPOrganisationDMPReference; Type: FK CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."DMPOrganisation"
    ADD CONSTRAINT "DMPOrganisationDMPReference" FOREIGN KEY ("Organisation") REFERENCES public."Organisation"("ID");


--
-- Name: DMPOrganisation DMPOrganisationOrganisationReference; Type: FK CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."DMPOrganisation"
    ADD CONSTRAINT "DMPOrganisationOrganisationReference" FOREIGN KEY ("DMP") REFERENCES public."DMP"("ID");


--
-- Name: DMP DMPProjectReference; Type: FK CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."DMP"
    ADD CONSTRAINT "DMPProjectReference" FOREIGN KEY ("Grant") REFERENCES public."Grant"("ID");


--
-- Name: DMPResearcher DMPResearcherDMPReference; Type: FK CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."DMPResearcher"
    ADD CONSTRAINT "DMPResearcherDMPReference" FOREIGN KEY ("Researcher") REFERENCES public."Researcher"("ID");


--
-- Name: DMPResearcher DMPResearcherResearcherReference; Type: FK CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."DMPResearcher"
    ADD CONSTRAINT "DMPResearcherResearcherReference" FOREIGN KEY ("DMP") REFERENCES public."DMP"("ID");


--
-- Name: Dataset DatasetDMPReference; Type: FK CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."Dataset"
    ADD CONSTRAINT "DatasetDMPReference" FOREIGN KEY ("DMP") REFERENCES public."DMP"("ID");


--
-- Name: DatasetDataRepository DatasetDataRepositoryDataRepositoryReference; Type: FK CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."DatasetDataRepository"
    ADD CONSTRAINT "DatasetDataRepositoryDataRepositoryReference" FOREIGN KEY ("DataRepository") REFERENCES public."DataRepository"("ID");


--
-- Name: DatasetDataRepository DatasetDataRepositoryDatasetReference; Type: FK CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."DatasetDataRepository"
    ADD CONSTRAINT "DatasetDataRepositoryDatasetReference" FOREIGN KEY ("Dataset") REFERENCES public."Dataset"("ID");


--
-- Name: Dataset DatasetDatasetProfileReference; Type: FK CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."Dataset"
    ADD CONSTRAINT "DatasetDatasetProfileReference" FOREIGN KEY ("Profile") REFERENCES public."DescriptionTemplate"("ID");


--
-- Name: DatasetRegistry DatasetRegistryDatasetReference; Type: FK CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."DatasetRegistry"
    ADD CONSTRAINT "DatasetRegistryDatasetReference" FOREIGN KEY ("Dataset") REFERENCES public."Dataset"("ID");


--
-- Name: DatasetRegistry DatasetRegistryRegistryReference; Type: FK CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."DatasetRegistry"
    ADD CONSTRAINT "DatasetRegistryRegistryReference" FOREIGN KEY ("Registry") REFERENCES public."Registry"("ID");


--
-- Name: DatasetService DatasetServiceDatasetReference; Type: FK CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."DatasetService"
    ADD CONSTRAINT "DatasetServiceDatasetReference" FOREIGN KEY ("Dataset") REFERENCES public."Dataset"("ID");


--
-- Name: DatasetService DatasetServiceServiceReference; Type: FK CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."DatasetService"
    ADD CONSTRAINT "DatasetServiceServiceReference" FOREIGN KEY ("Service") REFERENCES public."Service"("ID");


--
-- Name: DescriptionTemplate DescriptionTemplateTypeReference; Type: FK CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."DescriptionTemplate"
    ADD CONSTRAINT "DescriptionTemplateTypeReference" FOREIGN KEY ("Type") REFERENCES public."DescriptionTemplateType"("ID");


--
-- Name: Lock LockUserReference; Type: FK CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."Lock"
    ADD CONSTRAINT "LockUserReference" FOREIGN KEY ("LockedBy") REFERENCES public."UserInfo"(id);


--
-- Name: Notification NotificationUserReference; Type: FK CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."Notification"
    ADD CONSTRAINT "NotificationUserReference" FOREIGN KEY ("UserId") REFERENCES public."UserInfo"(id);

--
-- Name: UserDatasetProfile UserDatasetProfile_datasetProfile_key; Type: FK CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."UserDatasetProfile"
	ADD CONSTRAINT "UserDatasetProfile_datasetProfile_fkey" FOREIGN KEY ("datasetProfile") REFERENCES public."DescriptionTemplate" ("ID");

--
-- Name: UserDatasetProfile UserDatasetProfile_user_key; Type: FK CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--


ALTER TABLE ONLY public."UserDatasetProfile"
	ADD CONSTRAINT "UserDatasetProfile_usr_fkey" FOREIGN KEY ("usr") REFERENCES public."UserInfo" (id);

--
-- Name: UserDMP UserDMP_dmp_fkey; Type: FK CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."UserDMP"
    ADD CONSTRAINT "UserDMP_dmp_fkey" FOREIGN KEY (dmp) REFERENCES public."DMP"("ID");


--
-- Name: UserDMP UserDMP_usr_fkey; Type: FK CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."UserDMP"
    ADD CONSTRAINT "UserDMP_usr_fkey" FOREIGN KEY (usr) REFERENCES public."UserInfo"(id);


--
-- Name: UserRole UserRole_userId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."UserRole"
    ADD CONSTRAINT "UserRole_userId_fkey" FOREIGN KEY ("UserId") REFERENCES public."UserInfo"(id);


--
-- Name: Dataset fk_dataset_creator; Type: FK CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."Dataset"
    ADD CONSTRAINT fk_dataset_creator FOREIGN KEY ("Creator") REFERENCES public."UserInfo"(id);


--
-- Name: DMP fk_dmp_creator; Type: FK CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."DMP"
    ADD CONSTRAINT fk_dmp_creator FOREIGN KEY ("Creator") REFERENCES public."UserInfo"(id);


--
-- Name: Grant fk_grant_content; Type: FK CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."Grant"
    ADD CONSTRAINT fk_grant_content FOREIGN KEY ("Content") REFERENCES public."Content"("Id");


--
-- Name: Grant fk_grant_creator; Type: FK CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."Grant"
    ADD CONSTRAINT fk_grant_creator FOREIGN KEY ("CreationUser") REFERENCES public."UserInfo"(id);


--
-- Name: Invitation fk_invitation_creator; Type: FK CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."Invitation"
    ADD CONSTRAINT fk_invitation_creator FOREIGN KEY ("CreationUser") REFERENCES public."UserInfo"(id);


--
-- Name: Invitation fk_invitation_dmp; Type: FK CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."Invitation"
    ADD CONSTRAINT fk_invitation_dmp FOREIGN KEY ("Dmp") REFERENCES public."DMP"("ID");


--
-- Name: Project fk_project_content; Type: FK CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."Project"
    ADD CONSTRAINT fk_project_content FOREIGN KEY ("Content") REFERENCES public."Content"("Id");


--
-- Name: Project fk_project_creator; Type: FK CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."Project"
    ADD CONSTRAINT fk_project_creator FOREIGN KEY ("CreationUser") REFERENCES public."UserInfo"(id);


--
-- Name: Credential fkey_credential_user; Type: FK CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."Credential"
    ADD CONSTRAINT fkey_credential_user FOREIGN KEY ("UserId") REFERENCES public."UserInfo"(id);


--
-- Name: DatasetExternalDataset fkey_datasetexternaldataset_dataset; Type: FK CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."DatasetExternalDataset"
    ADD CONSTRAINT fkey_datasetexternaldataset_dataset FOREIGN KEY ("Dataset") REFERENCES public."Dataset"("ID");


--
-- Name: DatasetExternalDataset fkey_datasetexternaldataset_externaldataset; Type: FK CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."DatasetExternalDataset"
    ADD CONSTRAINT fkey_datasetexternaldataset_externaldataset FOREIGN KEY ("ExternalDataset") REFERENCES public."ExternalDataset"("Id");


--
-- Name: UserToken fkey_usetoken_user; Type: FK CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."UserToken"
    ADD CONSTRAINT fkey_usetoken_user FOREIGN KEY ("UserId") REFERENCES public."UserInfo"(id);


--
-- Name: UserPreference userpreference_user_fk; Type: FK CONSTRAINT; Schema: public; Owner: :POSTGRES_USER
--

ALTER TABLE ONLY public."UserPreference"
    ADD CONSTRAINT userpreference_user_fk FOREIGN KEY ("UserId") REFERENCES public."UserInfo"(id);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

--
-- TOC entry 1827 (class 826 OID 16387)
-- Name: DEFAULT PRIVILEGES FOR TYPES; Type: DEFAULT ACL; Schema: -; Owner: :POSTGRES_USER
--

ALTER DEFAULT PRIVILEGES FOR ROLE :POSTGRES_USER REVOKE ALL ON TYPES  FROM :POSTGRES_USER;
ALTER DEFAULT PRIVILEGES FOR ROLE :POSTGRES_USER GRANT ALL ON TYPES  TO :POSTGRES_USER WITH GRANT OPTION;


--
-- TOC entry 1826 (class 826 OID 16386)
-- Name: DEFAULT PRIVILEGES FOR FUNCTIONS; Type: DEFAULT ACL; Schema: -; Owner: :POSTGRES_USER
--

ALTER DEFAULT PRIVILEGES FOR ROLE :POSTGRES_USER REVOKE ALL ON FUNCTIONS  FROM :POSTGRES_USER;
ALTER DEFAULT PRIVILEGES FOR ROLE :POSTGRES_USER GRANT ALL ON FUNCTIONS  TO :POSTGRES_USER WITH GRANT OPTION;


--
-- PostgreSQL database dump complete
--

