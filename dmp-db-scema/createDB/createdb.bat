@echo off
setlocal
cd ..
for /F "tokens=*" %%p in ('type Docker\dmp-db.env') do SET %%p
psql -d postgres -U postgres -w --set=POSTGRES_USER=%POSTGRES_USER% --set=POSTGRES_PASSWORD=%POSTGRES_PASSWORD% --set=POSTGRES_DB=%POSTGRES_DB% -f main/createDatabase.sql
psql -d %POSTGRES_DB% -U %POSTGRES_USER% --set=POSTGRES_USER=%POSTGRES_USER% -w -f main/dmp-dump.sql
psql --set=ADMIN_USERNAME=%ADMIN_USERNAME% --set=ADMIN_PASSWORD=%ADMIN_PASSWORD% --set=POSTGRES_USER=%POSTGRES_USER% -d %POSTGRES_DB% -U %POSTGRES_USER% -w -f main/data-dump.sql
endlocal