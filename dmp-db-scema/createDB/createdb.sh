cd ..
source Docker/dmp-db.env
export $(cut -d= -f1 Docker/dmp-db.env)
psql -d postgres -U postgres -w --set=POSTGRES_USER="$POSTGRES_USER" --set=POSTGRES_PASSWORD="$POSTGRES_PASSWORD" --set=POSTGRES_DB="$POSTGRES_DB" -f main/createDatabase.sql
PGPASSWORD=$POSTGRES_PASSWORD psql -d $POSTGRES_DB -U $POSTGRES_USER --set=POSTGRES_USER="$POSTGRES_USER" -f main/dmp-dump.sql; 
PGPASSWORD=$POSTGRES_PASSWORD psql --set=ADMIN_USERNAME="$ADMIN_USERNAME" --set=ADMIN_PASSWORD="$ADMIN_PASSWORD" -d $POSTGRES_DB -U $POSTGRES_USER -f main/data-dump.sql; 
