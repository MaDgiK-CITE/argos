CREATE TABLE public."DoiFunder" (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name character varying,
    doi character varying
);


ALTER TABLE ONLY public."DoiFunder"
    ADD CONSTRAINT "DoiFunder_pkey" PRIMARY KEY (id);

ALTER TABLE public."DoiFunder" OWNER TO :POSTGRES_USER;

INSERT INTO public."DoiFunder"(name, doi) VALUES ('Australian Research Council',	'10.13039/501100000923');
INSERT INTO public."DoiFunder"(name, doi) VALUES ('European Commission', '10.13039/501100000780');
INSERT INTO public."DoiFunder"(name, doi) VALUES ('Fundação para a Ciência e a Tecnologia', '10.13039/501100001871');
INSERT INTO public."DoiFunder"(name, doi) VALUES ('Ministarstvo Prosvete, Nauke i Tehnološkog Razvoja', '10.13039/501100004564');
INSERT INTO public."DoiFunder"(name, doi) VALUES ('Ministarstvo Znanosti, Obrazovanja i Sporta',	'10.13039/501100006588');
INSERT INTO public."DoiFunder"(name, doi) VALUES ('National Health and Medical Research Council', '10.13039/501100000925');
INSERT INTO public."DoiFunder"(name, doi) VALUES ('National Science Foundation','10.13039/100000001');
INSERT INTO public."DoiFunder"(name, doi) VALUES ('Nederlandse Organisatie voor Wetenschappelijk Onderzoek',	'10.13039/501100003246');
INSERT INTO public."DoiFunder"(name, doi) VALUES ('Wellcome Trust', '10.13039/100004440');

INSERT INTO public."DBVersion" VALUES ('DMPDB', '00.00.003', '2020-05-06 18:11:00.000000+03', now(), 'Add Doi Funder');



