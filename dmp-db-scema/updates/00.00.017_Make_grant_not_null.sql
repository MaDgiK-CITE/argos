DO $$DECLARE
   this_version CONSTANT varchar := '00.00.017';
BEGIN
   PERFORM * FROM "DBVersion" WHERE version = this_version;
   IF FOUND THEN RETURN; END IF;

ALTER TABLE public."DMP" 
    ALTER COLUMN "Grant" DROP NOT NULL;

INSERT INTO public."DBVersion" VALUES ('DMPDB', '00.00.017', '2023-09-18 12:00:00.000000+02', now(), 'Make grant column of dmp table not null.');

END$$;