DO $$DECLARE
   this_version CONSTANT varchar := '00.00.015';
BEGIN
   PERFORM * FROM "DBVersion" WHERE version = this_version;
   IF FOUND THEN RETURN; END IF;

ALTER TABLE public."Dataset" 
ADD COLUMN "DmpSectionIndex" integer;

ALTER TABLE public."DMPDatasetProfile" 
ADD COLUMN "data" text;

INSERT INTO public."DBVersion" VALUES ('DMPDB', '00.00.015', '2023-09-01 12:00:00.000000+02', now(), 'Add column DmpSectionIndex to Dataset table and data to DMPDatasetProfile.');

END$$;