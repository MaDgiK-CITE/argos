DO $$DECLARE
   this_version CONSTANT varchar := '00.00.014';
BEGIN
   PERFORM * FROM "DBVersion" WHERE version = this_version;
   IF FOUND THEN RETURN; END IF;

ALTER TABLE public."DatasetProfile"
RENAME TO "DescriptionTemplate";

ALTER TABLE public."DescriptionTemplate" 
ADD COLUMN "Type" uuid;

INSERT INTO public."DescriptionTemplateType" ("ID", "Name", "Status")
VALUES  ('709a8400-10ca-11ee-be56-0242ac120002', 'Dataset', 1);

UPDATE public."DescriptionTemplate" SET "Type" = '709a8400-10ca-11ee-be56-0242ac120002';

ALTER TABLE public."DescriptionTemplate" 
ALTER COLUMN "Type" SET NOT NULL;

ALTER TABLE ONLY public."DescriptionTemplate"
    ADD CONSTRAINT "DescriptionTemplate_type_fkey" FOREIGN KEY ("Type") REFERENCES public."DescriptionTemplateType"("ID");

ALTER TABLE "UserDatasetProfile"
RENAME COLUMN "datasetProfile" TO "descriptionTemplate";
	
INSERT INTO public."DBVersion" VALUES ('DMPDB', '00.00.014', '2023-06-21 12:00:00.000000+02', now(), 'Rename DatasetProfile Table to DescriptionTemplate and add column Type referencing the DescriptionTemplateType.');

END$$;
