DO $$DECLARE
   this_version CONSTANT varchar := '00.00.007';
BEGIN
   PERFORM * FROM "DBVersion" WHERE version = this_version;
   IF FOUND THEN RETURN; END IF;

   ALTER TABLE public."UserInfo" ADD COLUMN userstatus smallint;
   
   UPDATE public."UserInfo" SET userstatus=0;
   
   ALTER TABLE public."UserInfo" ALTER COLUMN userstatus SET NOT NULL;
   
   INSERT INTO public."DBVersion" VALUES ('DMPDB', '00.00.007', '2020-10-27 13:40:00.000000+03', now(), 'Add userstatus on UserInfo table');
END$$;