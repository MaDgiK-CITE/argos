DO $$DECLARE
   this_version CONSTANT varchar := '00.00.012';
BEGIN
   PERFORM * FROM "DBVersion" WHERE version = this_version;
   IF FOUND THEN RETURN; END IF;

ALTER TABLE public."DMP" DROP COLUMN "DOI";

INSERT INTO public."DBVersion" VALUES ('DMPDB', '00.00.012', '2022-10-27 10:50:00.000000+02', now(), 'Delete doi column from dmp table');

END$$;