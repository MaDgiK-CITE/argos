DO $$DECLARE
   this_version CONSTANT varchar := '00.00.010';
BEGIN
   PERFORM * FROM "DBVersion" WHERE version = this_version;
   IF FOUND THEN RETURN; END IF;

CREATE TYPE DoiEntityType AS ENUM ('DMP');

CREATE TABLE public."EntityDoi"
(
    "ID" uuid NOT NULL,
    "EntityType" DoiEntityType NOT NULL,
    "RepositoryId" character varying(150) NOT NULL,
    "Doi" character varying(50) NOt NULL,
    "CreatedAt" timestamp(6) with time zone DEFAULT now() NOT NULL,
    "UpdatedAt" timestamp(6) with time zone DEFAULT now() NOT NULL,
    "EntityId" uuid NOT NULL,
    CONSTRAINT "Doi_pkey" PRIMARY KEY ("ID"),
    CONSTRAINT fk_doi_entityId FOREIGN KEY ("EntityId") REFERENCES public."DMP"("ID")
);

/*ALTER TABLE public."EntityDoi" OWNER TO :POSTGRES_USER;*/
	
INSERT INTO public."DBVersion" VALUES ('DMPDB', '00.00.010', '2022-10-27 10:50:00.000000+02', now(), 'Add Doi Table for depositions');

END$$;