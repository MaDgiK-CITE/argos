CREATE TABLE public."UserDatasetProfile"
(
    id uuid NOT NULL,
    "usr" uuid NOT NULL,
    "datasetProfile" uuid NOT NULL,
    role integer,
    CONSTRAINT "UserDatasetProfile_pkey" PRIMARY KEY (id),
    CONSTRAINT "UserDatasetProfile_datasetProfile_fkey" FOREIGN KEY ("datasetProfile")
        REFERENCES public."DatasetProfile" ("ID") MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT "UserDatasetProfile_usr_fkey" FOREIGN KEY ("usr")
        REFERENCES public."UserInfo" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)
	
INSERT INTO public."DBVersion" VALUES ('DMPDB', '00.00.008', '2021-04-05 17:48:00.000000+03', now(), 'Add Dataset Profile User Table');