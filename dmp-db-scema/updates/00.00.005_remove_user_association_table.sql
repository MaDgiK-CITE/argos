DO $$DECLARE
   this_version CONSTANT varchar := '00.00.005';
BEGIN
   PERFORM * FROM "DBVersion" WHERE version = this_version;
   IF FOUND THEN RETURN; END IF;

   DROP TABLE IF EXISTS "UserAssociation";

   INSERT INTO public."DBVersion" VALUES ('DMPDB', '00.00.005', '2020-06-03 12:00:00.000000+03', now(), 'Remove user association table');
END$$;