DO $$DECLARE
   this_version CONSTANT varchar := '00.00.009';
BEGIN
   PERFORM * FROM "DBVersion" WHERE version = this_version;
   IF FOUND THEN RETURN; END IF;

CREATE TYPE EntityType AS ENUM ('DATASET', 'DMP');

CREATE TABLE public."FileUpload"
(
    "ID" uuid NOT NULL,
    "Name" character varying(250) NOT NULL,
    "FileType" character varying(50) NOt NULL,
    "EntityId" uuid NOT NULL,
    "EntityType" EntityType NOT NULL,
    "CreatedAt" timestamp(6) with time zone DEFAULT now() NOT NULL,
    "IsDeleted" boolean DEFAULT false NOT NULL,
    "Creator" uuid NOT NULL,
    CONSTRAINT "File_pkey" PRIMARY KEY ("ID"),
    CONSTRAINT fk_fileupload_creator FOREIGN KEY ("Creator") REFERENCES public."UserInfo"(id)
);

/*ALTER TABLE public."UploadFile" OWNER TO :POSTGRES_USER;*/
	
INSERT INTO public."DBVersion" VALUES ('DMPDB', '00.00.009', '2022-03-03 15:50:00.000000+02', now(), 'Add File Table for uploaded files');

END$$;
