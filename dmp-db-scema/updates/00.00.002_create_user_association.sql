CREATE TABLE public."UserAssociation" (
    id uuid NOT NULL,
    "firstUser" uuid NOT NULL,
    "secondUser" uuid NOT NULL
);


ALTER TABLE public."UserAssociation" OWNER TO :POSTGRES_USER;

ALTER TABLE ONLY public."UserAssociation"
    ADD CONSTRAINT pk_user_association PRIMARY KEY (id);

ALTER TABLE ONLY public."UserAssociation"
    ADD CONSTRAINT fk_userinfo_user_association_1 FOREIGN KEY ("firstUser") REFERENCES public."UserInfo"(id);

ALTER TABLE ONLY public."UserAssociation"
    ADD CONSTRAINT fk_userinfo_user_association_2 FOREIGN KEY ("secondUser") REFERENCES public."UserInfo"(id);

INSERT INTO public."DBVersion" VALUES ('DMPDB', '00.00.002', '2020-05-04 13:42:00.000000+03', now(), 'Add User Association');


