DO $$DECLARE
   this_version CONSTANT varchar := '00.00.006';
BEGIN
   PERFORM * FROM "DBVersion" WHERE version = this_version;
   IF FOUND THEN RETURN; END IF;

   ALTER TABLE public."Credential" ADD COLUMN Email character varying;
   
   ALTER TABLE public."LoginConfirmationEmail" RENAME TO "EmailConfirmation";
   
   ALTER TABLE public."EmailConfirmation" ADD COLUMN data text;
   
   UPDATE public."Credential" cred SET "Email"= u.email FROM public."UserInfo" u WHERE cred."UserId" = u.id;
   
   INSERT INTO public."DBVersion" VALUES ('DMPDB', '00.00.006', '2020-10-26 10:10:00.000000+03', now(), 'Update Credential and LoginConfirmationEmail tables');
END$$;