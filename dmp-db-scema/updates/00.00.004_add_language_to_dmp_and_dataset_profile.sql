ALTER TABLE public."DMP"
	ADD COLUMN "extraProperties" text;

ALTER TABLE public."DatasetProfile"
	ADD COLUMN "Language" character varying;
	
UPDATE public."DMP"
	SET "extraProperties"='{"language": "en"}';
	
UPDATE public."DatasetProfile"
	SET  "Language"='en';

ALTER TABLE public."DatasetProfile"
	ALTER COLUMN "Language" SET NOT NULL;
	
INSERT INTO public."DBVersion" VALUES ('DMPDB', '00.00.004', '2020-06-03 12:00:00.000000+03', now(), 'Add language to DMP and Dataset Template');
	