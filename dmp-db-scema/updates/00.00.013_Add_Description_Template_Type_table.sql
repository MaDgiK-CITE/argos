DO $$DECLARE
   this_version CONSTANT varchar := '00.00.013';
BEGIN
   PERFORM * FROM "DBVersion" WHERE version = this_version;
   IF FOUND THEN RETURN; END IF;


CREATE TABLE public."DescriptionTemplateType"
(
    "ID" uuid NOT NULL,
    "Name" character varying(250) NOT NULL,
    "Status" smallint DEFAULT 0 NOT NULL,
    CONSTRAINT "DescriptionTemplateType_pkey" PRIMARY KEY ("ID")
);

/*ALTER TABLE public."DescriptionTemplateType" OWNER TO :POSTGRES_USER;*/
	
INSERT INTO public."DBVersion" VALUES ('DMPDB', '00.00.013', '2023-06-21 12:00:00.000000+02', now(), 'Add Description Template Type table.');

END$$;
