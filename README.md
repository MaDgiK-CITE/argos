
**Important note: The deployment, troubleshooting, maintenance and operation of on-premises / self-served OpenDMP instances for development, testing or production use, shall be the sole responsibility of the adopter. No support is guaranteed by OpenDMP implentation team for issues that may be encountered during deployment, extension or operation of such installations.**

**Documentation is provided on a best-effort basis for the code and processes around the development, deployment and operation of OpenDMP. If you find any misalignment of the actual processes with the related documentation, please let us know so that the misalignment is addressed for the benefit of future adopters.**


# Using Docker Compose with Argos

ARGOS is an open extensible service that simplifies the management, validation, monitoring and maintenance and of Data Management Plans. It allows actors (researchers, managers, supervisors etc) to create actionable DMPs that may be freely exchanged among infrastructures for carrying out specific aspects of the Data management process in accordance with the intentions and commitment of Data owners.

## Before running the docker compose commands, configurations must be set

### Database

First of all, database must be configured

The only file that has to be changed is **/dmp-db-scema/Docker/dmp-db.env**

```bash
ADMIN_USER: Admin username (app)
ADMIN_PASSWORD: Admin password (app)

POSTGRES_DB: database name
POSTGRES_USER: Admin username (database)
POSTGRES_PASSWORD: Admin password (database)
```

### Backend

Secondly, a few more options should be asigned

The file **/dmp-backend/web/src/main/resources/config/application-docker.properties** contains all the necessary properties

Values to be modified:
```bash
database.url: the url that is used to connect to database (JDBC based)
database.username: database admin username
database.password: database admin password

elasticsearch.*(optional): setup elastic, check Elasticsearch(optional) section below

google.login.clientId(optional): google as login provider
```
**NOTE:** if you want to configure and integrate other providers, check this reference [Setup configurable login](https://code-repo.d4science.org/MaDgiK-CITE/argos/wiki/Page-2A:-Setup-configurable-login)

If you provide google.login.clientId, then the same value should be set in the field named **loginProviders.googleConfiguration.clientId** which belongs to **/dmp-frontend/src/assets/config/config.json**

## You are ready to build and run the entire application using Docker-compose

1. Go to the project's root directory
2. Type in the **Terminal** `docker volume create --name=dmpdata`
3. Type in the **Terminal** `docker-compose up -d --build`
4. After it's complete your application is running on [http://localhost:8080](http://localhost:8080)

### Elasticsearch(optional)
If you want to set up elasticsearch, you will need the password for the **elastic** user

After your application is running, type in the **Terminal** `docker exec -it elasticsearch /bin/sh`

Run the command `cat data/passwords.txt` in the shell and save its output

Finally, run `exit` to get back to your terminal

The elastic's password that you get has to be set in the **elasticsearch.password** property in the backend configuration

Rerun the application

1. Type in the **Terminal** `docker-compose down`
2. Type in the **Terminal** `docker-compose up -d --build`