export const environment = {
	production: true,
	Server: 'https://devel.opendmp.eu/srv/api/',
	App: 'https://devel.opendmp.eu/',
	HelpService: {
		Enabled: false,
		Url: 'https://devel.opendmp.eu/content-service/',
	},
	defaultCulture: 'en-US',
	loginProviders: {
		enabled: [1, 2, 3, 4, 5, 6],
		facebookConfiguration: { clientId: '' },
		googleConfiguration: { clientId: '596924546661-83nhl986pnrpug5h624i5kptuao03dcd.apps.googleusercontent.com' },
		linkedInConfiguration: {
			clientId: '',
			oauthUrl: 'https://www.linkedin.com/oauth/v2/authorization',
			redirectUri: 'https://devel.opendmp.eu/login/linkedin',
			state: ''
		},
		twitterConfiguration: {
			clientId: '',
			oauthUrl: 'https://api.twitter.com/oauth/authenticate'
		},
		b2accessConfiguration: {
			clientId: '',
			oauthUrl: 'https://b2access-integration.fz-juelich.de:443/oauth2-as/oauth2-authz',
			redirectUri: 'http://devel.opendmp.eu/api/oauth/authorized/b2access',
			state: ''
		},
		orcidConfiguration: {
			clientId: '',
			oauthUrl: 'https://sandbox.orcid.org/oauth/authorize',
			redirectUri: 'http://opendmp.eu/api/oauth/authorized/orcid'
		}
	},
	logging: {
		enabled: false,
		logLevels: ["debug", "info", "warning", "error"]
	},
};
