export const environment = {
	production: false,
	Server: 'http://localhost:8080/api/',
	App: 'http://localhost:4200/',
	HelpService: {
		Enabled: false,
		Url: 'localhost:5000/',
	},
	defaultCulture: 'en-US',
	loginProviders: {
		enabled: [1, 2, 3, 4, 5, 6, 7, 8],
		facebookConfiguration: { clientId: '' },
		googleConfiguration: { clientId: '' },
		linkedInConfiguration: {
			clientId: '',
			oauthUrl: 'https://www.linkedin.com/oauth/v2/authorization',
			redirectUri: 'http://localhost:4200/login/linkedin',
			state: '987654321'
		},
		twitterConfiguration: {
			clientId: '',
			oauthUrl: 'https://api.twitter.com/oauth/authenticate'
		},
		b2accessConfiguration: {
			clientId: '',
			oauthUrl: 'https://b2access-integration.fz-juelich.de:443/oauth2-as/oauth2-authz',
			redirectUri: 'http://localhost:4200/api/oauth/authorized/b2access',
			state: ''
		},
		orcidConfiguration: {
			clientId: 'APP-766DI5LP8T75FC4R',
			oauthUrl: 'https://orcid.org/oauth/authorize',
			redirectUri: 'http://localhost:4200/login/external/orcid'
		},
		openAireConfiguration: {
			clientId: '',
			oauthUrl: '',
			redirectUri: '',
			state: '987654321'
		}
	},
	logging: {
		enabled: true,
		logLevels: ["debug", "info", "warning", "error"]
	},
	lockInterval: 60000,
	guideAssets: "assets/images/guide"
};
