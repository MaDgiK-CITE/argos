export const environment = {
	production: true,
	Server: 'https://devel.opendmp.eu/srv/api/',
	App: 'https://devel.opendmp.eu/',
	HelpService: {
		Enabled: false,
		Url: 'https://devel.opendmp.eu/content-service/',
	},
	defaultCulture: 'en-US',
	loginProviders: {
		enabled: [1, 2, 3, 4, 5, 6],
		facebookConfiguration: { clientId: '' },
		googleConfiguration: { clientId: '' },
		linkedInConfiguration: {
			clientId: '',
			oauthUrl: 'https://www.linkedin.com/oauth/v2/authorization',
			redirectUri: 'http://localhost:4200/login/linkedin',
			state: ''
		},
		twitterConfiguration: {
			clientId: '',
			oauthUrl: 'https://api.twitter.com/oauth/authenticate'
		},
		b2accessConfiguration: {
			clientId: '',
			oauthUrl: 'https://b2access-integration.fz-juelich.de:443/oauth2-as/oauth2-authz',
			redirectUri: 'http://opendmp.eu/api/oauth/authorized/b2access',
			state: ''
		},
		orcidConfiguration: {
			clientId: 'APP-766DI5LP8T75FC4R',
			oauthUrl: 'https://sandbox.orcid.org/oauth/authorize',
			redirectUri: 'http://opendmp.eu/api/oauth/authorized/orcid'
		}
	},
	logging: {
		enabled: false,
		logLevels: ["debug", "info", "warning", "error"]
	},
};
