function toggleClass(ids, option) {
	ids.forEach(id => {
		var element = document.getElementById(id);
		var className = element.getAttribute("class").replace(/ .*/, '');
		if (id === option) {
			element.classList.replace(className, "selected");
		} else {
			element.classList.replace(className, "not-selected");
		}
	})
}

function navbarCollapse() {
	var element = document.getElementById('nav');
	if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
		element.classList.add("navbar-shrink");
		element.classList.add("fixed-top");
	} else {
		element.classList.remove("navbar-shrink");
		element.classList.remove("fixed-top");
	}
};
navbarCollapse();
window.onscroll = function () { navbarCollapse() };

function dropdownToggle(dropdownId) {
	this.closeOpenDropdowns();
	document.getElementById(dropdownId).classList.toggle("show");
}

function closeOpenDropdowns() {
	var dropdowns = document.getElementsByClassName("dropdown-content");
	var i;
	for (i = 0; i < dropdowns.length; i++) {
		var openDropdown = dropdowns[i];
		if (openDropdown.classList.contains('show')) {
			openDropdown.classList.remove('show');
		}
	}
}

// Close the dropdown if the user clicks outside of it
window.onclick = function (event) {
	if (!event.target.matches('.dropbtn')) {
		this.closeOpenDropdowns();
	}
}

// Slideshow per 3 sec, stops on option click
window.onload = function slideImages() {
	var x = -1;
	slides = setInterval(function displayNextImage() {
		x = (x === images.length - 1) ? 0 : x + 1;
		if (document.getElementById('template-preview')) {
			document.getElementById('template-preview').src = images[x];
			toggleClass(['id1', 'id2', 'id3'], 'id'.concat((x + 1).toString()));
		}
	}, 3000);
}

var slides;
var images = [];
images[0] = 'assets/img/template-preview-1.svg';
images[1] = 'assets/img/template-preview-2.svg';
images[2] = 'assets/img/template-preview-3.svg';


function emailSend() {
	alert("The form was submitted");
}
