
jQuery(function(){
    const $img_coloured = $('#img-coloured');
    const $img_preface = $('.preface-img');
    const $action_dots = $('[data-clip-mode]');
    const $lifecycle_img_container = $('#lifecycle-img-container');
    let current_mode = null;

    const mutationObserver = new MutationObserver(function(mutations) {
        mutations.forEach(mutation=>{
            if(mutation.attributeName === 'class'){//class changed
                const isActive = mutation.target.className.includes('active');
                const target_image_id = mutation.target.attributes['data-img-fragment'].value;

                if(target_image_id && target_image_id.length){


                    //dummy fix clean all opacities
                    $('.img-fragment-step').each(function(){
                        $(this).removeClass('opacity-1');
                    });


                    if(isActive){
                        $("#"+target_image_id).addClass('opacity-1');
                    }
                }
                
                
                if(mutation.target.id == 'multiple-navigation-1' && isActive){ //search children
                    
                    const $target_nav_links = $('.sub-navigation-button');
                    $target_nav_links.removeClass('active');   
                    $target_nav_links.first().addClass('active');
                }
            }
        })
    });


    $(function () {
        $('[data-toggle="tooltip"]').tooltip({html:true})
      })
    
    $action_dots.on('mouseenter', function(){
            const mode = $(this).attr('data-clip-mode');
            current_mode = mode;

            $img_coloured.addClass(mode);
            // $img_preface.addClass('opacity-0');
            $img_coloured.addClass('opacity-1');
            
            $action_dots.each(function(){
               if($(this).attr('data-clip-mode') != mode){
                   $(this).addClass('d-none');
               };
            });
    });
    $action_dots.on('mouseleave', function(){
            
    });


    $lifecycle_img_container.on('mouseleave', function(){
        if(current_mode){
            const mode = current_mode;


            //give animation time to complete
            setTimeout(() => {
                $img_coloured.removeClass(mode);
                $action_dots.each(function(){
                    $(this).removeClass('d-none');
                });
            }, 300);
            $img_coloured.removeClass('opacity-1');
            // $img_preface.removeClass('opacity-0');
    
        }

        current_mode = null;
    });


    $("[data-img-fragment]").each(function(){
        const nodeElement = $(this)[0];

        mutationObserver.observe(nodeElement ,{attributes:true});
    });

    
    $('.navigate-dot').on('click', function(){

        const navigate_to = $(this).attr('data-navigate-to');
        
        if(navigate_to){
            const $navigate_to = $('#'+navigate_to);
            $navigate_to.click();
            const offset = $('#options-menu').offset().top - 120;
            window.scrollTo({top:offset, behavior:'smooth'})
        }
    });
    

});
