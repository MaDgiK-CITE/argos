import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';
import { HttpResponse, HttpClient } from '@angular/common/http';
import { BaseHttpService } from '../http/base-http.service';
import { ConfigurationService } from '../configuration/configuration.service';

@Injectable()
export class UserGuideService {
	private userGuideUrl : string;

	constructor(
		private translate: TranslateService,
		private http: HttpClient,
		private baseHttp: BaseHttpService,
		private configurationService: ConfigurationService
	) {
		this.userGuideUrl = `${configurationService.server}userguide`;
	}

	public getUserGuide(lang: string): Observable<HttpResponse<Blob>> {
		return this.http.get(`${this.userGuideUrl}/${lang}`, { responseType: 'blob', observe: 'response', headers: {'Content-type': 'text/html',
		 'Accept': 'text/html',
		  'Access-Control-Allow-Origin': this.configurationService.app,
		  'Access-Control-Allow-Credentials': 'true'} });
	}

	public updateUserGuide(data: any): Observable<String> {
		return this.http.post<string>(`${this.userGuideUrl}/current`, data);
	}

}
