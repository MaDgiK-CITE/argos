import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {BaseHttpService} from "@app/core/services/http/base-http.service";
import {ConfigurationService} from "@app/core/services/configuration/configuration.service";
import {Observable} from "rxjs";
import {Prefilling} from "@app/core/model/dataset/prefilling";
import {DatasetWizardModel} from "@app/core/model/dataset/dataset-wizard";

@Injectable()
export class PrefillingService {
	private readonly actionUrl: string;
	private headers = new HttpHeaders();

	constructor(private http: BaseHttpService, private httpClient: HttpClient, private configurationService: ConfigurationService) {
		this.actionUrl = configurationService.server + 'prefilling/';
	}

	public getPrefillingList(like: string): Observable<Prefilling[]> {
		return this.http.get<Prefilling[]>(this.actionUrl + 'list?like=' + encodeURIComponent(like), { headers: this.headers });
	}

	public getPrefillingDataset(pid: string, profileId: string, configId: string): Observable<DatasetWizardModel> {
		return this.http.get<DatasetWizardModel>(this.actionUrl + 'generate/' + encodeURIComponent(pid) + '?configId=' + encodeURIComponent(configId) + '&profileId=' + encodeURIComponent(profileId), { headers: this.headers });
	}

	public getPrefillingDatasetUsingData(data: any, profileId: string, configId: string): Observable<DatasetWizardModel> {
		return this.http.post<DatasetWizardModel>(this.actionUrl + 'generateUsingData' + '?configId=' + encodeURIComponent(configId) + '&profileId=' + encodeURIComponent(profileId), data, { headers: this.headers });
	}
}
