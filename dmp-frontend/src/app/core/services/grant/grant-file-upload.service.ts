import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { ContentFile } from '../../model/grant/grant-listing';
import { BaseHttpService } from '../http/base-http.service';
import { BaseHttpParams } from '../../../../common/http/base-http-params';
import { InterceptorType } from '../../../../common/http/interceptors/interceptor-type';
import { ConfigurationService } from '../configuration/configuration.service';

@Injectable()
export class GrantFileUploadService {
	private actionUrl: string;
	private headers: HttpHeaders;

	constructor(private http: BaseHttpService, private configurationService: ConfigurationService) {
		this.actionUrl = configurationService.server + 'files/';
	}

	uploadFile(formData: FormData): Observable<ContentFile> {
		const params = new BaseHttpParams();
		params.interceptorContext = {
			excludedInterceptors: [InterceptorType.JSONContentType]
		};
		return this.http.post(this.actionUrl + 'upload', formData, { params: params });
	}
}
