import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { DataTableData } from '../../model/data-table/data-table-data';
import { DataTableRequest } from '../../model/data-table/data-table-request';
import { GrantListingModel } from '../../model/grant/grant-listing';
import { GrantCriteria } from '../../query/grant/grant-criteria';
import { RequestItem } from '../../query/request-item';
import { BaseHttpService } from '../http/base-http.service';
import { ConfigurationService } from '../configuration/configuration.service';


@Injectable()
export class GrantService {

	private actionUrl: string;
	private headers: HttpHeaders;

	constructor(private http: BaseHttpService, private configurationService: ConfigurationService) {
		this.actionUrl = configurationService.server + 'grants/';
	}

	getPaged(dataTableRequest: DataTableRequest<GrantCriteria>, fieldsGroup?: string): Observable<DataTableData<GrantListingModel>> {
		if (fieldsGroup) {
			return this.http.post<DataTableData<GrantListingModel>>(this.actionUrl + 'paged?fieldsGroup=' + fieldsGroup, dataTableRequest, { headers: this.headers });
		}
		else {
			return this.http.post<DataTableData<GrantListingModel>>(this.actionUrl + 'paged?fieldsGroup=' + 'autocomplete', dataTableRequest, { headers: this.headers });
		}
	}

	getPublicPaged(dataTableRequest: DataTableRequest<GrantCriteria>): Observable<DataTableData<GrantListingModel>> {
		return this.http.post<DataTableData<GrantListingModel>>(this.actionUrl + 'public/paged', dataTableRequest, { headers: this.headers });
	}

	getWithExternal(requestItem: RequestItem<GrantCriteria>): Observable<GrantListingModel[]> {
		return this.http.post<GrantListingModel[]>(this.actionUrl + 'external', requestItem, { headers: this.headers });
	}

	getSingle(id: string): Observable<GrantListingModel> {
		return this.http.get<GrantListingModel>(this.actionUrl + id, { headers: this.headers });
	}

	createGrant(grantModel: GrantListingModel): Observable<GrantListingModel> {
		return this.http.post<GrantListingModel>(this.actionUrl, grantModel, { headers: this.headers });
	}

	// Actually sets it inactive.
	delete(id: String): Observable<GrantListingModel> {
		return this.http.delete<GrantListingModel>(this.actionUrl + id, { headers: this.headers });
	}
}
