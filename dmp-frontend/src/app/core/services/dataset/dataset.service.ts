import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DataTableData } from '../../model/data-table/data-table-data';
import { DataTableRequest } from '../../model/data-table/data-table-request';
import { DatasetListingModel } from '../../model/dataset/dataset-listing';
import { DatasetProfileModel, DatasetProfileWithPrefillingModel } from '../../model/dataset/dataset-profile';
import { DatasetCriteria } from '../../query/dataset/dataset-criteria';
import { ExploreDatasetCriteriaModel } from '../../query/explore-dataset/explore-dataset-criteria';
import { BaseHttpService } from '../http/base-http.service';
import { DatasetProfileCriteria } from '../../query/dataset-profile/dataset-profile-criteria';
import { ConfigurationService } from '../configuration/configuration.service';
import { DatasetOverviewModel } from '@app/core/model/dataset/dataset-overview';
import { HttpHeaders, HttpResponse, HttpClient } from '@angular/common/http';
import { DatasetModel } from '@app/core/model/dataset/dataset';

@Injectable()
export class DatasetService {

	private actionUrl: string;
	private headers = new HttpHeaders();

	constructor(
		private http: BaseHttpService,
		private configurationSevice: ConfigurationService,
		private httpClient: HttpClient) {
		this.actionUrl = configurationSevice.server + 'datasets/';
	}

	getPaged(dataTableRequest: DataTableRequest<DatasetCriteria>): Observable<DataTableData<DatasetListingModel>> {
		return this.http.post<DataTableData<DatasetListingModel>>(this.actionUrl + 'paged', dataTableRequest);
	}

	getPublicPaged(dataTableRequest: DataTableRequest<ExploreDatasetCriteriaModel>): Observable<DataTableData<DatasetListingModel>> {
		return this.http.post<DataTableData<DatasetListingModel>>(this.actionUrl + 'public/paged', dataTableRequest);
	}

	makeDatasetPublic(id: String) {
		return this.http.get(this.actionUrl + 'makepublic/' + id);
	}

	getDatasetProfiles(dataTableRequest: DataTableRequest<DatasetProfileCriteria>): Observable<DatasetProfileModel[]> {
		return this.http.post<DatasetProfileModel[]>(this.configurationSevice.server + 'datasetprofiles/getAll', dataTableRequest);
	}

	getDatasetProfilesWithPrefilling(dataTableRequest: DataTableRequest<DatasetProfileCriteria>): Observable<DatasetProfileWithPrefillingModel[]> {
		return this.http.post<DatasetProfileWithPrefillingModel[]>(this.configurationSevice.server + 'datasetprofiles/getAllWithPrefilling', dataTableRequest);
	}

	getDatasetProfilesUsedPaged(dataTableRequest: DataTableRequest<DatasetProfileCriteria>) {
		return this.http.post<DataTableData<DatasetListingModel>>(this.actionUrl + 'datasetProfilesUsedByDatasets/paged', dataTableRequest);
	}

	generateIndex() {
		return this.http.post(this.actionUrl + 'index', {});
	}

	clearIndex() {
		return this.http.delete(this.actionUrl + 'index');
	}

	getOverviewSingle(id: string): Observable<DatasetOverviewModel> {
		return this.http.get<DatasetOverviewModel>(this.actionUrl + 'overview/' + id, { headers: this.headers });
	}

	getOverviewSinglePublic(id: string): Observable<DatasetOverviewModel> {
		return this.http.get<DatasetOverviewModel>(this.actionUrl + 'publicOverview/' + id, { headers: this.headers })
	}

	clone(datasetModel: DatasetModel, id: String): Observable<DatasetModel> {
		return this.http.post<DatasetModel>(this.actionUrl + 'clone/' + id, datasetModel, { headers: this.headers });
	}

	delete(id: String): Observable<DatasetModel> {
		return this.http.delete<DatasetModel>(this.actionUrl + 'delete/' + id, { headers: this.headers }); // + 'delete/'
	}

	publish(id: String): Observable<DatasetModel> {
		return this.http.get<DatasetModel>(this.actionUrl + 'makepublic/' + id, { headers: this.headers });
	}

	public downloadXML(id: string): Observable<HttpResponse<Blob>> {
		let headerXml: HttpHeaders = this.headers.set('Content-Type', 'application/xml')
		return this.httpClient.get(this.actionUrl + id, { responseType: 'blob', observe: 'response', headers: headerXml }); //+ "/getXml/"
	}

	public downloadDocx(id: string): Observable<HttpResponse<Blob>> {
		let headerDoc: HttpHeaders = this.headers.set('Content-Type', 'application/msword')
		return this.httpClient.get(this.actionUrl + id, { responseType: 'blob', observe: 'response', headers: headerDoc });
	}

	public downloadPDF(id: string): Observable<HttpResponse<Blob>> {
		let headerPdf: HttpHeaders = this.headers.set('Content-Type', 'application/pdf')
		return this.httpClient.get(this.actionUrl + 'getPDF/' + id, { responseType: 'blob', observe: 'response', headers: headerPdf });
	}

	public validateDataset(id: string) : Observable<any>{
		return this.http.get<any>(this.actionUrl+id+"/validate");
	}

	//GK: NO
	// public downloadJson(id: string): Observable<HttpResponse<Blob>> {
	// 	return this.httpClient.get(this.actionUrl + 'rda/' + id, { responseType: 'blob', observe: 'response' });
	// }
}
