import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { DatasetExternalAutocompleteCriteria, DatasetExternalAutocompleteOptionsCriteria } from '../../query/dataset/daatset-external-autocomplete-criteria';
import { RequestItem } from '../../query/request-item';
import { DatasetProfileService } from '../dataset-profile/dataset-profile.service';
import { ConfigurationService } from '../configuration/configuration.service';
import { map } from 'rxjs/operators';

@Injectable()
export class DatasetExternalAutocompleteService {

	private actionUrl: string;

	constructor(
		private httpClient: HttpClient,
		private datasetProfileService: DatasetProfileService,
		private configurationService: ConfigurationService) {
		this.actionUrl = configurationService.server + '/';
	}

	getDatasetProfileById(datasetProfileID) {
		return this.datasetProfileService.getDatasetProfileById(datasetProfileID);
	}

	queryAutocomplete(lookUpItem: RequestItem<DatasetExternalAutocompleteCriteria>): Observable<any> {
		return this.httpClient.post(this.configurationService.server + 'search/autocomplete', lookUpItem);
	}

	queryApi(lookUpItem: RequestItem<DatasetExternalAutocompleteOptionsCriteria>): Observable<any>{ 
		return this.httpClient.post(this.configurationService.server + 'search/autocompleteOptions', lookUpItem);
	}

}