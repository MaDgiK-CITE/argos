import { Observable } from "rxjs";
import { Injectable } from "@angular/core";
import { HttpHeaders } from "@angular/common/http";
import { RequestItem } from "../../query/request-item";
import { BaseCriteria } from "../../query/base-criteria";
import { BaseHttpService } from "../http/base-http.service";
import { environment } from '../../../../environments/environment';
import { ProjectCriteria } from "../../query/project/project-criteria";
import { ProjectModel } from "../../model/project/project";
import { ConfigurationService } from '../configuration/configuration.service';

@Injectable()
export class ProjectService {
	private actionUrl: string;
	private headers: HttpHeaders;

	constructor(private http: BaseHttpService, private configurationService: ConfigurationService) {
		this.actionUrl = configurationService.server + 'projects/';
	}

	getWithExternal(requestItem: RequestItem<ProjectCriteria>): Observable<ProjectModel[]> {
		return this.http.post<ProjectModel[]>(this.actionUrl + 'external', requestItem, { headers: this.headers });
	}
}
