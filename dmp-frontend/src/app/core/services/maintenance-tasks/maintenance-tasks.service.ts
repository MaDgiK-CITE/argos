import { Injectable } from "@angular/core";
import { BaseService } from "@common/base/base.service";
import { Observable } from "rxjs";
import { ConfigurationService } from "../configuration/configuration.service";
import { BaseHttpService } from "../http/base-http.service";

@Injectable()
export class MaintenanceTasksService extends BaseService {

    private actionUrl: string;

	constructor(private http: BaseHttpService, configurationService: ConfigurationService) {
		super();
		this.actionUrl = configurationService.server + 'management/';
	}

	migrateSemantics(): Observable<void> {
		return this.http.post<null>(this.actionUrl + 'addSemantics/', null);
	}

	addRdaInSemantics(): Observable<void> {
		return this.http.post<null>(this.actionUrl + 'addRdaInSemantics/', null);
	}
}