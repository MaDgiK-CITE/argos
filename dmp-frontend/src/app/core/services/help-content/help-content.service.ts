import { HttpClient } from '@angular/common/http';
/**
 * Created by stefania on 7/17/17.
 */
import { Injectable } from '@angular/core';
import { Observable, throwError as observableThrowError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';
import { PageHelpContent } from '../../model/help-content/page-help-content';
import { CachedContentItem } from './cached-content-item';
import { ConfigurationService } from '../configuration/configuration.service';


@Injectable()
export class HelpContentService {
	private _helpServiceUrl: string;
	cache = new Map<String, CachedContentItem>();

	constructor(private http: HttpClient, private configurationService: ConfigurationService) {
		this._helpServiceUrl = configurationService.helpService.url;
	}

	// getActivePageContent(route: string) {
	// 	if (!this.cache.get(route) || !this.isValidCachedItem(route)) {
	// 		return this.http.get(this._helpServiceUrl + '/page/route?q=' + route).pipe(
	// 			map((res: Response) => {
	// 				this.cache.set(route, { timestamp: Date.now(), content: res.json() as PageHelpContent });
	// 				return res.json();
	// 			}),
	// 			catchError(this.handleError));
	// 	}
	// 	return Observable.create(observer => observer.next(this.cache.get(route).content));
	// }
	// private extractData(res: Response) {
	// 	const body = res.json();
	// 	return body. || {};
	// }
	// private handleError(error: Response | any) {
	// 	// In a real world app, we might use a remote logging infrastructure
	// 	// We'd also dig deeper into the error to get a better message
	// 	let errMsg = '';
	// 	if (error instanceof Response) {
	// 		const body = error.text() || '';
	// 		//const err = body.error || JSON.stringify(body);
	// 		errMsg = `${error.status} - ${error.statusText || ''} ${body}`;
	// 	} else {
	// 		errMsg = (error.message) ? error.message :
	// 			error.status ? `${error.status} - ${error.statusText}` : 'Server error';
	// 		console.error(errMsg); // log to console instead
	// 	}
	// 	return observableThrowError(errMsg);
	// }

	// isValidCachedItem(route) {
	// 	const cachedTimestamp = this.cache.get(route).timestamp;
	// 	const currentTimestamp = Date.now();
	// 	if (currentTimestamp - cachedTimestamp > 30000) { return false; } else { return true; }
	// }
}
