import { PageHelpContent } from "../../model/help-content/page-help-content";

export class CachedContentItem {
	public timestamp: number;
	public content: PageHelpContent;
}
