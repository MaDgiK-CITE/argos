import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigurationService } from '../configuration/configuration.service';
import { BaseHttpService } from '../http/base-http.service';
import { Observable } from 'rxjs';
import { DataTableData } from '@app/core/model/data-table/data-table-data';
import { DescriptionTemplateType } from '@app/core/model/description-template-type/description-template-type';

@Injectable()
export class DescriptionTemplateTypeService {

  private actionUrl: string;
	private headers = new HttpHeaders();
  
  constructor(private http: BaseHttpService, private httpClient: HttpClient, private configurationService: ConfigurationService) {
		this.actionUrl = configurationService.server + 'descriptionTemplateType/';
	}

  getTypes(): Observable<DataTableData<DescriptionTemplateType>> {
    return this.http.get<DataTableData<DescriptionTemplateType>>(this.actionUrl + 'get', { headers: this.headers });
  }

  getSingle(id: string): Observable<DescriptionTemplateType> {
    return this.http.get<DescriptionTemplateType>(this.actionUrl + 'get/' + id, { headers: this.headers });
  }

  createType(type: DescriptionTemplateType): Observable<DescriptionTemplateType> {
    return this.http.post<DescriptionTemplateType>(this.actionUrl + 'create', type, { headers: this.headers });
  }

  updateType(type: DescriptionTemplateType): Observable<DescriptionTemplateType> {
    return this.http.post<DescriptionTemplateType>(this.actionUrl + 'update', type, { headers: this.headers });
  }

  deleteType(id: string): Observable<DescriptionTemplateType> {
    return this.http.delete<DescriptionTemplateType>(this.actionUrl + 'delete/' + id, { headers: this.headers });
  }
}
