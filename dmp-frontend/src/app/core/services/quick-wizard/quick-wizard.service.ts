import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { QuickWizardEditorWizardModel } from '../../../ui/quick-wizard/quick-wizard-editor/quick-wizard-editor.model';
import { BaseHttpService } from '../http/base-http.service';
import { DatasetCreateWizardModel } from '../../../ui/dataset-create-wizard/dataset-create-wizard.model';
import { ConfigurationService } from '../configuration/configuration.service';

@Injectable()
export class QuickWizardService {
    private actionUrl: string;
    private headers: HttpHeaders;

    constructor(private http: BaseHttpService, private configurationService: ConfigurationService) {
        this.actionUrl = configurationService.server + 'quick-wizard/';
    }

    createQuickWizard(quickWizard: QuickWizardEditorWizardModel): Observable<QuickWizardEditorWizardModel> {
        return this.http.post<QuickWizardEditorWizardModel>(this.actionUrl, quickWizard, { headers: this.headers });
	}

	createQuickDatasetWizard(datasetCreateWizardModel: DatasetCreateWizardModel): Observable<DatasetCreateWizardModel> {
		return this.http.post<DatasetCreateWizardModel>(this.actionUrl + 'datasetcreate', datasetCreateWizardModel, { headers: this.headers });
	}
}
