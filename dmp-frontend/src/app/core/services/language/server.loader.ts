import { TranslateLoader } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { environment } from 'environments/environment';
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../configuration/configuration.service';

export class TranslateServerLoader implements TranslateLoader{
	private languageUrl: string;

	constructor(
		private http: HttpClient,
		private configurationService: ConfigurationService
	) {

	}
	getTranslation(lang: string): Observable<any> {
		this.languageUrl = `${this.configurationService.server}language`;
		return this.http.get(`${this.languageUrl}/${lang}`);
	}
}
