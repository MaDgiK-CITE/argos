import { HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ExternalSourceItemModel } from "@app/core/model/external-sources/external-source-item";
import { RequestItem } from "@app/core/query/request-item";
import { Observable } from "rxjs";
import { DataTableData } from "../../model/data-table/data-table-data";
import { DataTableRequest } from "../../model/data-table/data-table-request";
import { OrganizationModel } from "../../model/organisation/organization";
import { OrganisationCriteria } from "../../query/organisation/organisation-criteria";
import { ConfigurationService } from '../configuration/configuration.service';
import { BaseHttpService } from "../http/base-http.service";

@Injectable()
export class OrganisationService {

	private actionUrl: string;
	private headers: HttpHeaders;

	constructor(private http: BaseHttpService, private configurationService: ConfigurationService) {

		this.actionUrl = configurationService.server;

		this.headers = new HttpHeaders();
		this.headers = this.headers.set('Content-Type', 'application/json');
		this.headers = this.headers.set('Accept', 'application/json');
	}

	public searchInternalOrganisations(dataTableRequest: DataTableRequest<OrganisationCriteria>): Observable<DataTableData<OrganizationModel>> {
		return this.http.post<DataTableData<OrganizationModel>>(this.actionUrl + 'internal/organisations', dataTableRequest, { headers: this.headers });
	}

	public searchGeneralOrganisations(dataTableRequest: RequestItem<OrganisationCriteria>): Observable<ExternalSourceItemModel[]> {
		return this.http.post<ExternalSourceItemModel[]>(this.actionUrl + 'general/organisations', dataTableRequest, { headers: this.headers });
	}

	public searchPublicOrganisations(dataTableRequest: DataTableRequest<OrganisationCriteria>): Observable<DataTableData<OrganizationModel>> {
		return this.http.post<DataTableData<OrganizationModel>>(this.actionUrl + 'public/organisations', dataTableRequest, { headers: this.headers });
	}
}
