import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AppRole } from '../../common/enum/app-role';
import { DatasetProfileComboBoxType } from '../../common/enum/dataset-profile-combo-box-type';
import { DatasetProfileFieldViewStyle } from '../../common/enum/dataset-profile-field-view-style';
import { DatasetStatus } from '../../common/enum/dataset-status';
import { DmpProfileFieldDataType } from '../../common/enum/dmp-profile-field-type';
import { DmpProfileType } from '../../common/enum/dmp-profile-type';
import { DmpStatus } from '../../common/enum/dmp-status';
import { ValidationType } from '../../common/enum/validation-type';
import { DatasetProfileInternalDmpEntitiesType } from '../../common/enum/dataset-profile-internal-dmp-entities-type';
import { RecentActivityOrder } from '@app/core/common/enum/recent-activity-order';
import { Role } from '@app/core/common/enum/role';
import { RoleOrganizationType } from '@app/core/common/enum/role-organization-type';
import { ViewStyleType } from '@app/ui/admin/dataset-profile/editor/components/field/view-style-enum';

@Injectable()
export class EnumUtils {
	constructor(private language: TranslateService) { }

	public getEnumValues(T): Array<any> {
		let keys = Object.keys(T);
		keys = keys.slice(0, keys.length / 2);
		const values: Array<Number> = keys.map(Number);
		return values;
	}

	convertFromPrincipalAppRole(status: AppRole): string {
		switch (status) {
			case AppRole.Admin: return this.language.instant('TYPES.APP-ROLE.ADMIN');
			case AppRole.User: return this.language.instant('TYPES.APP-ROLE.USER');
			case AppRole.Manager: return this.language.instant('TYPES.APP-ROLE.MANAGER');
			case AppRole.DatasetTemplateEditor: return this.language.instant('TYPES.APP-ROLE.DESCRIPTION-TEMPLATE-EDITOR');
		}
	}

	toDmpProfileFieldDataTypeString(type: DmpProfileFieldDataType): string {
		switch (type) {
			case DmpProfileFieldDataType.Date: return this.language.instant('TYPES.DMP-PROFILE-FIELD.DATA-TYPE.DATE');
			case DmpProfileFieldDataType.Number: return this.language.instant('TYPES.DMP-PROFILE-FIELD.DATA-TYPE.NUMBER');
			case DmpProfileFieldDataType.Text: return this.language.instant('TYPES.DMP-PROFILE-FIELD.DATA-TYPE.TEXT');
			case DmpProfileFieldDataType.ExternalAutocomplete: return this.language.instant('TYPES.DMP-PROFILE-FIELD.DATA-TYPE.EXTERNAL-AUTOCOMPLETE');
		}
	}

	toDmpProfileTypeString(type: DmpProfileType): string {
		switch (type) {
			case DmpProfileType.Input: return this.language.instant('TYPES.DMP-PROFILE-FIELD.TYPE.INPUT');
		}
	}

	toDatasetStatusString(status: DatasetStatus): string {
		switch (status) {
			case DatasetStatus.Draft: return this.language.instant('TYPES.DATASET-STATUS.DRAFT');
			case DatasetStatus.Finalized: return this.language.instant('TYPES.DATASET-STATUS.FINALISED');
		}
	}

	toDmpStatusString(status: DmpStatus): string {
		switch (status) {
			case DmpStatus.Draft: return this.language.instant('TYPES.DMP.DRAFT');
			case DmpStatus.Finalized: return this.language.instant('TYPES.DMP.FINALISED');
		}
	}

	toDatasetProfileFieldValidationTypeString(status: ValidationType): string {
		switch (status) {
			case ValidationType.None: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VALIDATION-TYPE.NONE');
			case ValidationType.Required: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VALIDATION-TYPE.REQUIRED');
		}
	}

	toDatasetProfileFieldViewStyleString(status: DatasetProfileFieldViewStyle): string {
		switch (status) {
			case DatasetProfileFieldViewStyle.BooleanDecision: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.BOOLEAN-DECISION');
			case DatasetProfileFieldViewStyle.CheckBox: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.CHECKBOX');
			case DatasetProfileFieldViewStyle.ComboBox: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.COMBO-BOX');
			case DatasetProfileFieldViewStyle.InternalDmpEntities: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.INTERNAL-DMP-ENTITIES');
			case DatasetProfileFieldViewStyle.FreeText: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.FREE-TEXT');
			case DatasetProfileFieldViewStyle.RadioBox: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.RADIO-BOX');
			case DatasetProfileFieldViewStyle.TextArea: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.TEXT-AREA');
			case DatasetProfileFieldViewStyle.RichTextArea: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.RICH-TEXT-AREA');
			case DatasetProfileFieldViewStyle.Upload: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.UPLOAD');
			case DatasetProfileFieldViewStyle.Table: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.TABLE');
			case DatasetProfileFieldViewStyle.DatePicker: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.DATE-PICKER');
			case DatasetProfileFieldViewStyle.ExternalDatasets: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.EXTERNAL-DATASETS');
			case DatasetProfileFieldViewStyle.DataRepositories: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.DATA-REPOSITORIES');
			case DatasetProfileFieldViewStyle.PubRepositories: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.PUB-REPOSITORIES');
			case DatasetProfileFieldViewStyle.JournalRepositories: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.JOURNALS-REPOSITORIES');
			case DatasetProfileFieldViewStyle.Taxonomies: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.TAXONOMIES');
			case DatasetProfileFieldViewStyle.Licenses: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.LICENSES');
			case DatasetProfileFieldViewStyle.Publications: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.PUBLICATIONS');
			case DatasetProfileFieldViewStyle.Registries: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.REGISTRIES');
			case DatasetProfileFieldViewStyle.Services: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.SERVICES');
			case DatasetProfileFieldViewStyle.Tags: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.TAGS');
			case DatasetProfileFieldViewStyle.Researchers: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.RESEARCHERS');
			case DatasetProfileFieldViewStyle.Organizations: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.ORGANIZATIONS');
			case DatasetProfileFieldViewStyle.DatasetIdentifier: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.DATASET-IDENTIFIER');
			case DatasetProfileFieldViewStyle.Currency: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.CURRENCY');
			case DatasetProfileFieldViewStyle.Validation: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.VALIDATION');
		}
	}
	toDatasetProfileViewTypeString(status: ViewStyleType) :string{
		switch (status) {
			case ViewStyleType.BooleanDecision: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.BOOLEAN-DECISION');
			case ViewStyleType.CheckBox: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.CHECKBOX');
			case ViewStyleType.Select: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.SELECT');
			case ViewStyleType.InternalDmpEntities: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.INTERNAL-DMP-ENTITIES');
			case ViewStyleType.FreeText: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.FREE-TEXT');
			case ViewStyleType.RadioBox: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.RADIO-BOX');
			case ViewStyleType.TextArea: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.TEXT-AREA');
			case ViewStyleType.RichTextArea: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.RICH-TEXT-AREA');
			case ViewStyleType.Table: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.TABLE');
			case ViewStyleType.Upload: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.UPLOAD');
			case ViewStyleType.DatePicker: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.DATE-PICKER');
			case ViewStyleType.ExternalDatasets: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.EXTERNAL-DATASETS');
			case ViewStyleType.DataRepositories: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.DATA-REPOSITORIES');
			case ViewStyleType.PubRepositories: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.PUB-REPOSITORIES');
			case ViewStyleType.JournalRepositories: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.JOURNALS-REPOSITORIES');
			case ViewStyleType.Taxonomies: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.TAXONOMIES');
			case ViewStyleType.Licenses: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.LICENSES');
			case ViewStyleType.Publications: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.PUBLICATIONS');
			case ViewStyleType.Registries: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.REGISTRIES');
			case ViewStyleType.Services: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.SERVICES');
			case ViewStyleType.Tags: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.TAGS');
			case ViewStyleType.Researchers: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.RESEARCHERS');
			case ViewStyleType.Organizations: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.ORGANIZATIONS');
			case ViewStyleType.DatasetIdentifier: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.DATASET-IDENTIFIER');
			case ViewStyleType.Currency: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.CURRENCY');
			case ViewStyleType.Validation: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.VALIDATION');
			case ViewStyleType.Other: return this.language.instant('TYPES.DATASET-PROFILE-FIELD-VIEW-STYLE.OTHER');
		}

	}

	toDatasetProfileComboBoxTypeString(status: DatasetProfileComboBoxType): string {
		switch (status) {
			case DatasetProfileComboBoxType.WordList: return this.language.instant('TYPES.DATASET-PROFILE-COMBO-BOX-TYPE.WORD-LIST');
			case DatasetProfileComboBoxType.Autocomplete: return this.language.instant('TYPES.DATASET-PROFILE-COMBO-BOX-TYPE.AUTOCOMPLETE');
		}
	}

	toDatasetProfileInternalDmpEntitiesTypeString(status: DatasetProfileInternalDmpEntitiesType): string {
		switch (status) {
			case DatasetProfileInternalDmpEntitiesType.Researchers: return this.language.instant('TYPES.DATASET-PROFILE-INTERNAL-DMP-ENTITIES-TYPE.RESEARCHERS');
			case DatasetProfileInternalDmpEntitiesType.Datasets: return this.language.instant('TYPES.DATASET-PROFILE-INTERNAL-DMP-ENTITIES-TYPE.DATASETS');
			case DatasetProfileInternalDmpEntitiesType.Dmps: return this.language.instant('TYPES.DATASET-PROFILE-INTERNAL-DMP-ENTITIES-TYPE.DMPS');
		}
	}

	toRecentActivityOrderString(status: RecentActivityOrder): string {
		switch (status) {
			case RecentActivityOrder.CREATED: return this.language.instant('TYPES.RECENT-ACTIVITY-ORDER.CREATED');
			case RecentActivityOrder.LABEL: return this.language.instant('TYPES.RECENT-ACTIVITY-ORDER.LABEL');
			case RecentActivityOrder.MODIFIED: return this.language.instant('TYPES.RECENT-ACTIVITY-ORDER.MODIFIED');
			case RecentActivityOrder.FINALIZED: return this.language.instant('TYPES.RECENT-ACTIVITY-ORDER.FINALIZED');
			case RecentActivityOrder.PUBLISHED: return this.language.instant('TYPES.RECENT-ACTIVITY-ORDER.PUBLISHED');
			case RecentActivityOrder.DATASETPUBLISHED: return this.language.instant('TYPES.RECENT-ACTIVITY-ORDER.PUBLISHED');
			case RecentActivityOrder.STATUS: return this.language.instant('TYPES.RECENT-ACTIVITY-ORDER.STATUS');
		}
	}

	toRoleString(status: Role): string {
		switch (status) {
			case Role.Owner: return this.language.instant('FACET-SEARCH.ROLE.OWNER');
			case Role.Member: return this.language.instant('FACET-SEARCH.ROLE.MEMBER');
		}
	}

	toRoleOrganizationString(status: RoleOrganizationType): string {
		switch (status) {
			case RoleOrganizationType.Faculty: return this.language.instant('USER-PROFILE.ROLE-ORGANIZATION.FACULTY');
			case RoleOrganizationType.Librarian: return this.language.instant('USER-PROFILE.ROLE-ORGANIZATION.LIBRARIAN');
			case RoleOrganizationType.Researcher: return this.language.instant('USER-PROFILE.ROLE-ORGANIZATION.RESEARCHER');
			case RoleOrganizationType.Student: return this.language.instant('USER-PROFILE.ROLE-ORGANIZATION.STUDENT');
			case RoleOrganizationType.EarlyCareerResearcher: return this.language.instant('USER-PROFILE.ROLE-ORGANIZATION.EARLY-CAREER-RESEARCHER');
			case RoleOrganizationType.ResearchAdministrator: return this.language.instant('USER-PROFILE.ROLE-ORGANIZATION.RESEARCH-ADMINISTRATOR');
			case RoleOrganizationType.RepositoryManager: return this.language.instant('USER-PROFILE.ROLE-ORGANIZATION.REPOSITORY-MANAGER');
			case RoleOrganizationType.ResearchInfrastructure: return this.language.instant('USER-PROFILE.ROLE-ORGANIZATION.RESEARCH-INFRASTRUCTURE');
			case RoleOrganizationType.ServiceProvider: return this.language.instant('USER-PROFILE.ROLE-ORGANIZATION.SERVICE-PROVIDER');
			case RoleOrganizationType.Publisher: return this.language.instant('USER-PROFILE.ROLE-ORGANIZATION.PUBLISHER');
			case RoleOrganizationType.ResearchFunder: return this.language.instant('USER-PROFILE.ROLE-ORGANIZATION.RESEARCH-FUNDER');
			case RoleOrganizationType.Policymaker: return this.language.instant('USER-PROFILE.ROLE-ORGANIZATION.POLICY-MAKER');
			case RoleOrganizationType.SMEIndustry: return this.language.instant('USER-PROFILE.ROLE-ORGANIZATION.SME-INDUSTRY');
			case RoleOrganizationType.Other: return this.language.instant('USER-PROFILE.ROLE-ORGANIZATION.OTHER');
		}
	}
}
