import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { RequestItem } from '../../query/request-item';
import { DmpInvitation } from '../../model/dmp/invitation/dmp-invitation';
import { DmpInvitationUser } from '../../model/dmp/invitation/dmp-invitation-user';
import { DmpInvitationUserCriteria } from '../../query/dmp/dmp-invitation-user-criteria';
import { BaseHttpService } from '../http/base-http.service';
import { ConfigurationService } from '../configuration/configuration.service';

@Injectable()
export class DmpInvitationService {

	private actionUrl: string;
	private headers: HttpHeaders;

	constructor(private http: BaseHttpService, private configurationService: ConfigurationService) {
		this.actionUrl = configurationService.server + 'invite/';
	}

	public inviteDmpInvitationUsers(invitation: DmpInvitation): Observable<DmpInvitationUser[]> {
		return this.http.post<DmpInvitationUser[]>(this.actionUrl + 'users', invitation, { headers: this.headers });
	}

	public getDmpInvitationUsers(usersInvitationRequestItem: RequestItem<DmpInvitationUserCriteria>): Observable<DmpInvitationUser[]> {
		return this.http.post<DmpInvitationUser[]>(this.actionUrl + 'getUsers', usersInvitationRequestItem, { headers: this.headers });
	}

	public exchange(id: String): Observable<String> {
		return this.http.get<String>(this.actionUrl + 'exchange/' + id, { headers: this.headers });
	}
}
