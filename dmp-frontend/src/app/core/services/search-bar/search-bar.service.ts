import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { SearchBarItem } from '../../model/dashboard/search-bar-item';
import { BaseHttpService } from '../http/base-http.service';
import { ConfigurationService } from '../configuration/configuration.service';

@Injectable()
export class SearchBarService {

	private actionUrl: string;
	private headers: HttpHeaders;

	constructor(private http: BaseHttpService, private configurationService: ConfigurationService) {

		this.actionUrl = configurationService.server + 'dashboard/';
	}

	search(like: string): Observable<SearchBarItem[]> {
		return this.http.get<SearchBarItem[]>(this.actionUrl + 'search?like=' + like, { headers: this.headers });
	}
}
