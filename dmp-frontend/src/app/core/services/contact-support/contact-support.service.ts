import { BaseHttpService } from "../../../core/services/http/base-http.service";
import { HttpHeaders } from "@angular/common/http";
import { environment } from "../../../../environments/environment";
import { Injectable } from "@angular/core";
import { ContactEmail } from "../../model/contact/contact-email-form-model";
import { Observable } from "rxjs";
import { ConfigurationService } from '../configuration/configuration.service';

@Injectable()
export class ContactSupportService {

	private actionUrl: string;

	constructor(private http: BaseHttpService,
		private configurationService: ConfigurationService) {
		this.actionUrl = configurationService.server + 'contactEmail/';
	}

	postEmail(contentEmail: ContactEmail): Observable<ContactEmail> {
		return this.http.post<ContactEmail>(this.actionUrl, contentEmail);
	}
}
