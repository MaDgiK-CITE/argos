import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigurationService } from './configuration/configuration.service';
import { BaseHttpService } from './http/base-http.service';
import { Observable } from 'rxjs';
import { BaseComponent } from '@common/base/base.component';
import { AuthnRequestModel } from '../model/saml2/AuthnRequestModel';

@Injectable()
export class SamlLoginService extends BaseComponent {
    
    private actionUrl: string;
	private headers = new HttpHeaders();

	constructor(private http: BaseHttpService, private httpClient: HttpClient, private configurationService: ConfigurationService) {
		super();
        this.actionUrl = configurationService.server + 'saml2/';
	}

    resolveConfigurableLoginId(relayState: string): string {
        const decoded = decodeURIComponent(relayState);
        const routeParams = new URLSearchParams(decoded);
        return routeParams.has('configurableLoginId') ? routeParams.get('configurableLoginId') : undefined;
    }
    resolveSpId(relayState: string): string {
        const decoded = decodeURIComponent(relayState);
        const routeParams = new URLSearchParams(decoded);
        return routeParams.has('spId') ? routeParams.get('spId') : '';
    }

    getAuthnRequest(configurableLoginId: string): Observable<AuthnRequestModel> {
        return this.http.get<AuthnRequestModel>(this.actionUrl + 'authnRequest/' + configurableLoginId, { headers: this.headers });
    }

}