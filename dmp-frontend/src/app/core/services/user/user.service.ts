import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { DataTableData } from '../../model/data-table/data-table-data';
import { DataTableRequest } from '../../model/data-table/data-table-request';
import { UserListingModel } from '../../model/user/user-listing';
import { UserCriteria } from '../../query/user/user-criteria';
import { BaseHttpService } from '../http/base-http.service';
import { ConfigurationService } from '../configuration/configuration.service';
import { UserCredentialModel } from '@app/core/model/user/user-credential';

@Injectable()
export class UserService {

	private actionUrl: string;
	private headers = new HttpHeaders();

	constructor(private http: BaseHttpService, private httpClient: HttpClient, private configurationService: ConfigurationService) {

		this.actionUrl = configurationService.server + 'user/';
	}

	getPaged(dataTableRequest: DataTableRequest<UserCriteria>): Observable<DataTableData<UserListingModel>> {
		return this.http.post<DataTableData<UserListingModel>>(this.actionUrl + 'getPaged', JSON.stringify(dataTableRequest), { headers: this.headers });
	}

	getUser(id: String): Observable<UserListingModel> {
		return this.http.get<UserListingModel>(this.actionUrl + id, { headers: this.headers });
	}

	getEmails(id: String): Observable<UserCredentialModel[]> {
		return this.http.get<UserCredentialModel[]>(`${this.actionUrl}${id}/emails`, { headers: this.headers });
	}

	updateRoles(itemToUpdate: UserListingModel): Observable<UserListingModel> {
		return this.http.post<UserListingModel>(this.actionUrl + 'updateRoles', JSON.stringify(itemToUpdate), { headers: this.headers });
	}

	delete(id: String): Observable<any> {
		return this.http.delete<any>(this.actionUrl + id, { headers: this.headers });
	}

	getRecentActivity(): Observable<any[]> {
		return this.http.get<any[]>(this.actionUrl + 'recentActivity', { headers: this.headers });
	}

	updateUserSettings(value: any): Observable<any[]> {
		return this.http.post<any[]>(this.actionUrl + 'settings', value, { headers: this.headers });
	}

	getCollaboratorsPaged(dataTableRequest: DataTableRequest<UserCriteria>): Observable<DataTableData<UserListingModel>> {
		return this.http.post<DataTableData<UserListingModel>>(this.actionUrl + 'getCollaboratorsPaged', JSON.stringify(dataTableRequest), { headers: this.headers });
	}

	getFromEmail(email: string): Observable<UserListingModel> {
		return this.http.post<UserListingModel>(this.actionUrl + 'find', email, {headers: this.headers});
	}

	public registerDOIToken(code: string, redirectUri: string): Observable<any> {
		const url = this.actionUrl + 'registerDOIToken';
		return this.http.post(url, {zenodoRequest: {code: code}, redirectUri: redirectUri}, { headers: this.headers });
	}

	public deleteDOIToken(): Observable<any> {
		const url = this.actionUrl + 'deleteDOIToken';
		return this.http.delete(url, { headers: this.headers });
	}

	downloadCSV(): Observable<HttpResponse<Blob>> {
        let headerCsv: HttpHeaders = this.headers.set('Content-Type', 'application/csv')
        return this.httpClient.get(this.actionUrl + 'getCsv/', { responseType: 'blob', observe: 'response', headers: headerCsv });
    }
}
