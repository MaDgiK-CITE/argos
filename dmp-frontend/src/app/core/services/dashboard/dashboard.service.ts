import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { DashboardStatisticsModel } from '../../model/dashboard/dashboard-statistics-model';
import { BaseHttpService } from '../http/base-http.service';
import { ConfigurationService } from '../configuration/configuration.service';
import { RecentActivityModel } from '@app/core/model/recent-activity/recent-activity.model';
import { RecentActivityCriteria } from '@app/core/query/recent-activity/recent-activity-criteria';
import { DataTableRequest, DataTableMultiTypeRequest } from '@app/core/model/data-table/data-table-request';

@Injectable()
export class DashboardService {

	private actionUrl: string;
	private headers: HttpHeaders;

	constructor(private http: BaseHttpService,
		private configurationService: ConfigurationService) {
		this.actionUrl = configurationService.server + 'dashboard/';
	}

	getStatistics(): Observable<DashboardStatisticsModel> {
		return this.http.get<DashboardStatisticsModel>(this.actionUrl + 'getStatistics', { headers: this.headers });
	}

	getUserStatistics(): Observable<DashboardStatisticsModel> {
		return this.http.get<DashboardStatisticsModel>(this.actionUrl + 'me/getStatistics', { headers: this.headers });
	}

	getRecentActivity(request: DataTableMultiTypeRequest<RecentActivityCriteria>): Observable<RecentActivityModel[]> {
		return this.http.post<RecentActivityModel[]>(this.actionUrl + 'recentActivity', request, {headers: this.headers});
	}

	// getRecentActivity(request: DataTableRequest<RecentActivityCriteria>): Observable<DataTableData<RecentActivityModel>> {
	// 	return this.http.post<DataTableData<RecentActivityModel>>(this.actionUrl + 'recentActivity', request, {headers: this.headers});
	// }
}
