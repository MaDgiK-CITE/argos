import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DepositConfigurationModel } from '@app/core/model/deposit/deposit-configuration';
import { DepositCode, DepositRequest } from '@app/core/model/deposit/deposit-request';
import { DoiModel } from '@app/core/model/doi/doi';
import { Observable } from 'rxjs';
import { ConfigurationService } from '../configuration/configuration.service';
import { BaseHttpService } from '../http/base-http.service';

@Injectable()
export class DepositRepositoriesService {

  private actionUrl: string;
	private headers = new HttpHeaders();

	constructor(private http: BaseHttpService, private httpClient: HttpClient, private configurationService: ConfigurationService) {
		this.actionUrl = configurationService.server + 'deposit/';
	}

  getAvailableRepos(): Observable<DepositConfigurationModel[]> {
		return this.http.get<DepositConfigurationModel[]>(this.actionUrl + 'repos', { headers: this.headers });
  }

  getAccessToken(repositoryId: string, code: string): Observable<string> {
		const depositCode = new DepositCode();
		depositCode.repositoryId = repositoryId;
		depositCode.code = code;
		return this.http.post<string>(this.actionUrl + 'getAccessToken', depositCode, { headers: this.headers });
  }

  createDoi(repositoryId: string, dmpId: string, accessToken: string | null): Observable<DoiModel> {
		const depositRequest = new DepositRequest();
		depositRequest.repositoryId = repositoryId;
		depositRequest.dmpId = dmpId;
		depositRequest.accessToken = accessToken;
		return this.http.post<DoiModel>(this.actionUrl + 'createDoi', depositRequest, { headers: this.headers });
  }

  getLogo(repositoryId: string): Observable<string> {
	return this.http.get<string>(this.actionUrl + 'logo/' + repositoryId, { headers: this.headers });
  }

}
