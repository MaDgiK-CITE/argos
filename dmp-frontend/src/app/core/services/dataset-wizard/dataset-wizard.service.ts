import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { BaseHttpParams } from '../../../../common/http/base-http-params';
import { InterceptorType } from '../../../../common/http/interceptors/interceptor-type';
import { DatasetProfileDefinitionModel } from '../../model/dataset-profile-definition/dataset-profile-definition';
import { DatasetProfileModel } from '../../model/dataset/dataset-profile';
import { DatasetWizardModel } from '../../model/dataset/dataset-wizard';
import { DatasetProfileCriteria } from '../../query/dataset-profile/dataset-profile-criteria';
import { RequestItem } from '../../query/request-item';
import { BaseHttpService } from '../http/base-http.service';
import { ConfigurationService } from '../configuration/configuration.service';

@Injectable()
export class DatasetWizardService {

	private actionUrl: string;
	private headers = new HttpHeaders();

	constructor(private http: BaseHttpService, private httpClient: HttpClient, private configurationService: ConfigurationService) {
		this.actionUrl = configurationService.server + 'datasets/';
	}

	// public userDmps(criteria: RequestItem<DmpCriteria>): Observable<DmpModel[]> {
	// 	return this.http.post<DmpModel[]>(this.actionUrl + 'userDmps', criteria, { headers: this.headers });
	// }

	public getAvailableProfiles(criteria: RequestItem<DatasetProfileCriteria>): Observable<DatasetProfileModel[]> {
		return this.http.post<DatasetProfileModel[]>(this.actionUrl + 'getAvailableProfiles/', criteria, { headers: this.headers });
	}

	public getSingle(id: String): Observable<DatasetWizardModel> {
		return this.http.get<DatasetWizardModel>(this.actionUrl + id, { headers: this.headers });  // + 'getSingle/'
	}

	public getSinglePublic(id: String): Observable<DatasetWizardModel> {
		return this.http.get<DatasetWizardModel>(this.actionUrl + 'public/' + id, { headers: this.headers });  // + 'getSingle/'
	}

	public delete(id: string): Observable<DatasetWizardModel> {
		return this.http.delete<DatasetWizardModel>(this.actionUrl + 'delete/' + id, { headers: this.headers });
	}

	createDataset(datasetModel: DatasetWizardModel): Observable<DatasetWizardModel> {
		return this.http.post<DatasetWizardModel>(this.actionUrl, datasetModel, { headers: this.headers });
	}

	public downloadPDF(id: string): Observable<HttpResponse<Blob>> {
		return this.httpClient.get(this.actionUrl + 'getPDF/' + id, { responseType: 'blob', observe: 'response', headers: this.headers });
	}

	public downloadDOCX(id: string): Observable<HttpResponse<Blob>> {
		let headerDocx: HttpHeaders = this.headers.set('Content-Type', 'application/msword')
		return this.httpClient.get(this.actionUrl + id, { responseType: 'blob', observe: 'response', headers: headerDocx });
	}

	public downloadXML(id: string): Observable<HttpResponse<Blob>> {
		let headerXml: HttpHeaders = this.headers.set('Content-Type', 'application/xml')
		return this.httpClient.get(this.actionUrl + id, { responseType: 'blob', observe: 'response', headers: headerXml }); // + 'getXml/'
	}

	public getDefinition(id: String): Observable<DatasetProfileDefinitionModel> {
		return this.http.get<DatasetProfileDefinitionModel>(this.actionUrl + 'get/' + id, { headers: this.headers });
	}

	unlock(id: String): Observable<DatasetWizardModel> {
		return this.http.get<DatasetWizardModel>(this.actionUrl + id + '/unlock', { headers: this.headers });
	}

	public uploadXml(fileList: FileList, datasetTitle: string, dmpId: string, datasetProfileId: string): Observable<any> {
		const formData: FormData = new FormData();
		if (fileList instanceof FileList) {
			for (let i = 0; i < fileList.length; i++) {
				formData.append('file', fileList[i], datasetTitle);
			}
		} else if (Array.isArray(fileList)) {
			formData.append('files', fileList);
		} else {
			formData.append('file', fileList);
		}
		formData.append('dmpId', dmpId);
		formData.append('datasetProfileId', datasetProfileId);

		const params = new BaseHttpParams();
		params.interceptorContext = {
			excludedInterceptors: [InterceptorType.JSONContentType]
		};
		return this.http.post(this.actionUrl + 'upload', formData, { params: params });
	}

	public updateDatasetProfile(id: String): Observable<DatasetWizardModel> {
		return this.http.get<DatasetWizardModel>(this.actionUrl + "profile/" + id, { headers: this.headers });
	}
}
