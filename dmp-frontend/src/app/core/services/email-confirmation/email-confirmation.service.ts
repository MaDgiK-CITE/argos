import { HttpHeaders } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { environment } from "../../../../environments/environment";
import { BaseHttpService } from "../http/base-http.service";
import { ConfigurationService } from '../configuration/configuration.service';

@Injectable()
export class EmailConfirmationService {
	private actioUrl: string;
	private headers: HttpHeaders

	constructor(private http: BaseHttpService, private configurationService: ConfigurationService) {
		this.actioUrl = configurationService.server + 'emailConfirmation/';
	}

	public emailConfirmation(token: string) {
		return this.http.get<String>(this.actioUrl + token, { headers: this.headers });
	}

	public sendConfirmationEmail(email: string) {
		return this.http.post<String>(this.actioUrl, email, { headers: this.headers });
	}
}
