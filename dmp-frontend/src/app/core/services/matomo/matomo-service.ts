import { Injectable } from '@angular/core';
import { MatomoInjector, MatomoTracker } from 'ngx-matomo';
import { AuthService } from '../auth/auth.service';
import { ConfigurationService } from '../configuration/configuration.service';

@Injectable()
export class MatomoService {

	constructor(
		private configurationService: ConfigurationService,
		private matomoInjector: MatomoInjector,
		private matomoTracker: MatomoTracker,
		private authService: AuthService
	) {

	}

	init() {
		if (this.configurationService.matomoEnabled) {
			this.matomoInjector.init(this.configurationService.matomoSiteUrl, this.configurationService.matomoSiteId);
		}
	}

	trackPageView(customTitle?: string): void {
		if (this.configurationService.matomoEnabled) {
			var principal = this.authService.current();
			if (principal != null) { this.matomoTracker.setUserId(principal.id); }
			this.matomoTracker.trackPageView(customTitle);
		}
	}

	trackSiteSearch(keyword: string, category?: string, resultsCount?: number): void {
		if (this.configurationService.matomoEnabled) {
			var principal = this.authService.current();
			if (principal != null) { this.matomoTracker.setUserId(principal.id); }
			this.matomoTracker.trackSiteSearch(keyword, category, resultsCount);
		}
	}

	trackDownload(category: "dmps"|"datasets", type: "docx"|"pdf"|"xml"|"json", id: string): void {
		if (this.configurationService.matomoEnabled) {
			var principal = this.authService.current();
			if (principal != null) { this.matomoTracker.setUserId(principal.id); }
			this.matomoTracker.trackLink(this.configurationService.server + category + "/" + type + "/" + id, "download");

			// this.matomoTracker.trackLink(url, "download");
			// this.matomoTracker.trackEvent(category, "Downloaded", type);
		}
	}
}
