import { HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { BaseHttpService } from "../http/base-http.service";
import { ConfigurationService } from "../configuration/configuration.service";
import { UnlinkAccountRequestModel } from "@app/core/model/unlink-account/unlink-account";

@Injectable()
export class UnlinkAccountEmailConfirmationService {
	private actioUrl: string;
	private headers: HttpHeaders;

	constructor(private http: BaseHttpService, private configurationService: ConfigurationService) {
		this.actioUrl = configurationService.server + 'emailUnlinkConfirmation/';
	}

	public emailConfirmation(token: string) {
		return this.http.get<String>(this.actioUrl + token, { headers: this.headers });
	}

	public sendConfirmationEmail(unlinkRequest: UnlinkAccountRequestModel) {
		return this.http.post<String>(this.actioUrl, unlinkRequest, { headers: this.headers });
	}
}