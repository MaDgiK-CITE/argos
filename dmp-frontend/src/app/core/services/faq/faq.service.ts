import { Injectable } from "@angular/core";
import { ConfigurationService } from "../configuration/configuration.service";
import { HttpClient, HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable()
export class FaqService {
    private faqUrl : string;

	constructor(
		private http: HttpClient,
		private configurationService: ConfigurationService
	) {
		this.faqUrl = `${configurationService.server}material/faq`;
	}

	public getFaq(lang: string): Observable<HttpResponse<Blob>> {
		return this.http.get(`${this.faqUrl}/${lang}`, { responseType: 'blob', observe: 'response', headers: {'Content-type': 'text/html',
		 'Accept': 'text/html',
		  'Access-Control-Allow-Origin': this.configurationService.app,
		  'Access-Control-Allow-Credentials': 'true'} });
	}
}