import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DatasetProfile } from '@app/core/model/admin/dataset-profile/dataset-profile';
import { DataTableData } from '@app/core/model/data-table/data-table-data';
import { DataTableRequest } from '@app/core/model/data-table/data-table-request';
import { DatasetProfileDefinitionModel } from '@app/core/model/dataset-profile-definition/dataset-profile-definition';
import { DatasetListingModel } from '@app/core/model/dataset/dataset-listing';
import { DatasetProfileCriteria } from '@app/core/query/dataset-profile/dataset-profile-criteria';
import { BaseHttpService } from '@app/core/services/http/base-http.service';
import { DatasetProfileEditorModel } from '@app/ui/admin/dataset-profile/editor/dataset-profile-editor-model';
import { BaseService } from '@common/base/base.service';
import { BaseHttpParams } from '@common/http/base-http-params';
import { InterceptorType } from '@common/http/interceptors/interceptor-type';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';
import { takeUntil } from "rxjs/operators";
import { ConfigurationService } from '../configuration/configuration.service';

@Injectable()
export class DatasetProfileService extends BaseService {

	private actionUrl: string;
	private headers = new HttpHeaders();

	constructor(private http: BaseHttpService, private httpClient: HttpClient, private configurationService: ConfigurationService) {
		super();
		this.actionUrl = configurationService.server + 'admin/';
	}

	createForm(data) {
		return this.httpClient.post<string>(this.actionUrl + 'addDmp', data);
	}

	updateForm(id, data): Observable<DatasetProfileEditorModel> {
		return this.http.post<DatasetProfileEditorModel>(this.actionUrl + 'addDmp/' + id, data);
	}

	getDatasetProfileById(datasetProfileID): Observable<DatasetProfile> {
		return this.http.get<DatasetProfile>(this.actionUrl + 'get/' + datasetProfileID);
	}

	getPaged(dataTableRequest: DataTableRequest<DatasetProfileCriteria>): Observable<DataTableData<DatasetListingModel>> {
		return this.http.post<DataTableData<DatasetListingModel>>(this.actionUrl + 'datasetprofiles/getPaged', dataTableRequest);
	}

	preview(data: DatasetProfile): Observable<DatasetProfileDefinitionModel> {
		return this.http.post<DatasetProfileDefinitionModel>(this.actionUrl + 'preview', data);
	}

	clone(id: string): Observable<DatasetProfile> {
		return this.http.post<DatasetProfile>(this.actionUrl + 'datasetprofile/clone/' + id, {});
	}

	newVersion(id, data) {
		return this.httpClient.post<string>(this.actionUrl + 'newVersion/' + id, data);
	}

	delete(id: string, data): Observable<DatasetProfile> {
		//return this.http.post<DatasetProfile>(this.actionUrl + 'addDmp/' + id, data);
		return this.http.delete<DatasetProfile>(this.actionUrl + id, {});
	}

	downloadXML(id: string): Observable<HttpResponse<Blob>> {
		let headerXml: HttpHeaders = this.headers.set('Content-Type', 'application/xml')
		return this.httpClient.get(this.actionUrl + 'getXml/' + id, { responseType: 'blob', observe: 'response', headers: headerXml });
	}

	uploadFile(file: FileList, labelSent: string, datasetProfileId?: string): Observable<DataTableData<DatasetListingModel>> {
		const params = new BaseHttpParams();
		params.interceptorContext = {
			excludedInterceptors: [InterceptorType.JSONContentType]
		};
		const formData = new FormData();
		formData.append('file', file[0], labelSent);
		return (datasetProfileId === undefined || datasetProfileId == null)
							? this.http.post(this.actionUrl + "upload", formData, { params: params })
							: this.http.post(this.actionUrl + "upload/" + datasetProfileId, formData, { params: params });

	}

	searchSemantics(like: string): Observable<String[]> {
		return this.http.get<String[]>(this.actionUrl + "getSemantics?query=" + like);
	}
}
