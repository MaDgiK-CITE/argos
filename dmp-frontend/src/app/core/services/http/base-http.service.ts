
import {map} from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiMessageCode } from '../../common/enum/api-message-code';

@Injectable()
export class BaseHttpService {
	constructor(
		protected http: HttpClient
	) {
	}

	get<T>(url: string, options?: Object): Observable<T> {
		return this.interceptRepsonse<T>(this.http.get<T>(url, options));
	}

	post<T>(url: string, body: any, options?: Object): Observable<T> {
		return this.interceptRepsonse<T>(this.http.post<T>(url, body, options));
	}

	put<T>(url: string, body: any, options?: Object): Observable<T> {
		return this.interceptRepsonse<T>(this.http.put<T>(url, body, options));
	}

	delete<T>(url: string, options?: Object): Observable<T> {
		return this.interceptRepsonse<T>(this.http.delete<T>(url, options));
	}

	patch<T>(url: string, body: any, options?: Object): Observable<T> {
		return this.interceptRepsonse<T>(this.http.patch<T>(url, body, options));
	}

	head<T>(url: string, options?: Object): Observable<T> {
		return this.interceptRepsonse<T>(this.http.head<T>(url, options));
	}

	options<T>(url: string, options?: Object): Observable<T> {
		return this.interceptRepsonse<T>(this.http.options<T>(url, options));
	}

	private interceptRepsonse<T>(observable: Observable<Object>): Observable<T> {
		return observable.pipe(
			// .catch((errorResponse) => {
			// 	if (errorResponse.status === 401) {
			// 		this.snackBar.openFromComponent(SnackBarNotificationComponent, {
			// 			data: { message: 'GENERAL.SNACK-BAR.SUCCESSFUL-LOGOUT', language: this.language },
			// 			duration: 3000,
			// 		});
			// 		const currentPage = this.router.url;
			// 		this.router.navigate(['/unauthorized'], { queryParams: { returnUrl: currentPage } });
			// 		//this.notification.httpError(error);
			// 		return Observable.of<T>();
			// 	} else {
			// 		const error: any = errorResponse.error;
			// 		if (error.statusCode === ApiMessageCode.ERROR_MESSAGE) {
			// 			this.snackBar.openFromComponent(SnackBarNotificationComponent, {
			// 				data: { message: error.message, language: null },
			// 				duration: 3000,
			// 			});
			// 			return Observable.throw(errorResponse);
			// 		} else if (error.statusCode === ApiMessageCode.VALIDATION_MESSAGE) {
			// 			return Observable.throw(errorResponse);
			// 		} else {
			// 			this.snackBar.openFromComponent(SnackBarNotificationComponent, {
			// 				data: { message: 'GENERAL.ERRORS.HTTP-REQUEST-ERROR', language: this.language },
			// 				duration: 3000,
			// 			});
			// 			return Observable.throw(errorResponse);
			// 		}
			// 	}
			// })
			map(response => {
				if (response instanceof Blob) { return response; }
				if (response['statusCode'] === ApiMessageCode.SUCCESS_MESSAGE) {
					//throw new Error('Request failed');
					// this.snackBar.openFromComponent(SnackBarNotificationComponent, {
					// 	data: { message: response['message'], language: null },
					// 	duration: 3000,
					// });
					return response['payload'];
				} else if (response['statusCode'] === ApiMessageCode.NO_MESSAGE) {
					return response['payload'];
				} else {
					return response['payload'];
				}
			}));
	}
}
