import { Observable } from "rxjs";
import { Injectable } from "@angular/core";
import { HttpHeaders } from "@angular/common/http";
import { RequestItem } from "../../query/request-item";
import { BaseHttpService } from "../http/base-http.service";
import { environment } from '../../../../environments/environment';
import { FunderCriteria } from "../../query/funder/funder-criteria";
import { FunderModel } from "../../model/funder/funder";
import { ConfigurationService } from '../configuration/configuration.service';

@Injectable()
export class FunderService {
	private actionUrl: string;
	private headers: HttpHeaders;

	constructor(private http: BaseHttpService, private configurationService: ConfigurationService) {
		this.actionUrl = configurationService.server + 'funders/';
	}

	getWithExternal(requestItem: RequestItem<FunderCriteria>): Observable<FunderModel[]> {
		return this.http.post<FunderModel[]>(this.actionUrl + 'external', requestItem, { headers: this.headers });
	}
}
