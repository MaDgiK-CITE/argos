import { HttpHeaders } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { BaseHttpService } from "../http/base-http.service";
import { ConfigurationService } from '../configuration/configuration.service';
import { UserMergeRequestModel } from '@app/core/model/merge/user-merge-request';

@Injectable()
export class MergeEmailConfirmationService {
	private actioUrl: string;
	private headers: HttpHeaders

	constructor(private http: BaseHttpService, private configurationService: ConfigurationService) {
		this.actioUrl = configurationService.server + 'emailMergeConfirmation/';
	}

	public emailConfirmation(token: string) {
		return this.http.get<String>(this.actioUrl + token, { headers: this.headers });
	}

	public sendConfirmationEmail(mergeRequest: UserMergeRequestModel) {
		return this.http.post<String>(this.actioUrl, mergeRequest, { headers: this.headers });
	}
}
