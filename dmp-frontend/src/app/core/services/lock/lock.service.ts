import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { BaseHttpService } from '../http/base-http.service';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';
import { LockModel } from '@app/core/model/lock/lock.model';
import { ConfigurationService } from '../configuration/configuration.service';

@Injectable()
export class LockService {

	private actionUrl: string;
	private headers = new HttpHeaders();

	constructor(private http: BaseHttpService, private httpClient: HttpClient, private configurationService: ConfigurationService) {
		this.actionUrl = configurationService.server + 'lock/';
	}

	checkLockStatus(id: string): Observable<boolean> {
		return this.http.get(`${this.actionUrl}target/status/${id}`, { headers: this.headers });
	}

	unlockTarget(id: string): Observable<any> {
		return this.http.delete(`${this.actionUrl}target/unlock/${id}`, { headers: this.headers });
	}

	getSingle(id: string): Observable<LockModel> {
		return this.http.get(`${this.actionUrl}target/${id}`, { headers: this.headers });
	}

	createOrUpdate(lock: LockModel): Observable<string> {
		return this.http.post(`${this.actionUrl}`, lock, { headers: this.headers });
	}
}
