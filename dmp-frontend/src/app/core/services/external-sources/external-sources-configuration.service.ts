import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { ExternalSourcesConfiguration } from '../../model/external-sources/external-sources-configuration';
import { BaseHttpService } from '../http/base-http.service';
import { ConfigurationService } from '../configuration/configuration.service';

@Injectable()
export class ExternalSourcesConfigurationService {

	private actionUrl: string;
	private headers: HttpHeaders;

	constructor(private http: BaseHttpService, private configurationService: ConfigurationService) {
		this.actionUrl = configurationService.server + 'common/';
	}

	public getExternalSourcesConfiguration(): Observable<ExternalSourcesConfiguration> {
		return this.http.get<ExternalSourcesConfiguration>(this.actionUrl + 'externalSourcesConfiguration', { headers: this.headers });
	}

}
