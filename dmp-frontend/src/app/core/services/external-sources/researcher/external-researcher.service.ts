import { Injectable } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { ResearcherModel } from '../../../model/researcher/researcher';
import { BaseHttpService } from '../../http/base-http.service';
import { ConfigurationService } from '../../configuration/configuration.service';

@Injectable()
export class ExternalResearcherService {

	private actionUrl: string;

	constructor(private http: BaseHttpService, private configurationService: ConfigurationService) {
		this.actionUrl = configurationService.server + 'researchers/';
	}

	public createResearcher(researcher: ResearcherModel) {
		return this.http.post(this.actionUrl, researcher);
	}

}
