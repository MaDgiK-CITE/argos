import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../../../environments/environment';
import { DataRepositoryModel } from '../../../model/data-repository/data-repository';
import { BaseHttpService } from '../../http/base-http.service';
import { ConfigurationService } from '../../configuration/configuration.service';

@Injectable()
export class ExternalDataRepositoryService {

	private actionUrl: string;
	constructor(private http: BaseHttpService, private configurationService: ConfigurationService) {
		this.actionUrl = configurationService.server + 'external/datarepos';
	}

	create(dataRepoModel: DataRepositoryModel): Observable<DataRepositoryModel> {
		return this.http.post<DataRepositoryModel>(this.actionUrl , dataRepoModel);
	}

}
