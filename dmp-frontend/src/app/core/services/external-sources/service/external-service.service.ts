import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../../../environments/environment';
import { ServiceModel } from '../../../model/service/service';
import { BaseHttpService } from '../../http/base-http.service';
import { ConfigurationService } from '../../configuration/configuration.service';

@Injectable()
export class ExternalServiceService {

	private actionUrl: string;
	constructor(private http: BaseHttpService, private configurationService: ConfigurationService) {
		this.actionUrl = configurationService.server + 'services/';
	}

	create(serviceModel: ServiceModel): Observable<ServiceModel> {
		return this.http.post<ServiceModel>(this.actionUrl, serviceModel);
	}

}
