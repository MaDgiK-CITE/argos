import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../../../environments/environment';
import { RequestItem } from '../../../query/request-item';
import { DataTableData } from '../../../model/data-table/data-table-data';
import { DataTableRequest } from '../../../model/data-table/data-table-request';
import { ExternalDatasetModel } from '../../../model/external-dataset/external-dataset';
import { ExternalDatasetCriteria } from '../../../query/external-dataset/external-dataset-criteria';
import { BaseHttpService } from '../../http/base-http.service';
import { ConfigurationService } from '../../configuration/configuration.service';

@Injectable()
export class ExternalDatasetService {

	private actionUrl: string;
	constructor(private http: BaseHttpService, private configurationService: ConfigurationService) {
		this.actionUrl = configurationService.server + 'externaldatasets/';
	}

	create(externalDatasetModel: ExternalDatasetModel): Observable<ExternalDatasetModel> {
		return this.http.post<ExternalDatasetModel>(this.actionUrl, externalDatasetModel);
	}
}
