import { ConfigurationService } from '../../configuration/configuration.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LocalFetchModel } from '@app/core/model/local-fetch/local-fetch.model';
import { BaseHttpService } from '../../http/base-http.service';

@Injectable()
export class CurrencyService {

	private actionUrl: string;
	constructor(private http: BaseHttpService, private configurationService: ConfigurationService) {
		this.actionUrl = configurationService.server + 'currency';
	}

	get(): Observable<LocalFetchModel[]> {
		return this.http.get<LocalFetchModel[]>(this.actionUrl);
	}

}
