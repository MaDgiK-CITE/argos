import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../../../environments/environment';
import { RegistryModel } from '../../../model/registry/registry';
import { BaseHttpService } from '../../http/base-http.service';
import { ConfigurationService } from '../../configuration/configuration.service';

@Injectable()
export class ExternalRegistryService {

	private actionUrl: string;
	constructor(private http: BaseHttpService, private configurationService: ConfigurationService) {
		this.actionUrl = configurationService.server + 'registries/';
	}

	create(registerModel: RegistryModel): Observable<RegistryModel> {
		return this.http.post<RegistryModel>(this.actionUrl, registerModel);
	}
}
