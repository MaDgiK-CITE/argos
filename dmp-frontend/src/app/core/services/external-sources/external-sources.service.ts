import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { RequestItem } from '../../query/request-item';
import { ExternalSourceItemModel } from '../../model/external-sources/external-source-item';
import { DataRepositoryCriteria } from '../../query/data-repository/data-repository-criteria';
import { ExternalDatasetCriteria } from '../../query/external-dataset/external-dataset-criteria';
import { RegistryCriteria } from '../../query/registry/registry-criteria';
import { ResearcherCriteria } from '../../query/researcher/researcher-criteria';
import { ServiceCriteria } from '../../query/service/service-criteria';
import { TagCriteria } from '../../query/tag/tag-criteria';
import { BaseHttpService } from '../http/base-http.service';
import { ConfigurationService } from '../configuration/configuration.service';
import { LicenseCriteria } from '@app/core/query/license/license-criteria';
import {PublicationCriteria} from "@app/core/query/publication/publication-criteria";

@Injectable()
export class ExternalSourcesService {

	private actionUrl: string;
	private headers: HttpHeaders;

	constructor(private http: BaseHttpService, private configurationService: ConfigurationService) {

		this.actionUrl = configurationService.server + 'external/';

		this.headers = new HttpHeaders();
		this.headers = this.headers.set('Content-Type', 'application/json');
		this.headers = this.headers.set('Accept', 'application/json');
	}

	public searchDatasetRegistry(requestItem: RequestItem<RegistryCriteria>): Observable<ExternalSourceItemModel[]> {
		return this.http.get<ExternalSourceItemModel[]>(this.actionUrl + 'registries' + '?query=' + requestItem.criteria.like + '&type=' + requestItem.criteria.type, { headers: this.headers });
	}

	public searchDatasetRepository(requestItem: RequestItem<DataRepositoryCriteria>): Observable<ExternalSourceItemModel[]> {
		return this.http.get<ExternalSourceItemModel[]>(this.actionUrl + 'datarepos' + '?query=' + requestItem.criteria.like + '&type=' + requestItem.criteria.type, { headers: this.headers });
	}
	public searchPublicationRepository(requestItem: RequestItem<DataRepositoryCriteria>): Observable<ExternalSourceItemModel[]> {
		return this.http.get<ExternalSourceItemModel[]>(this.actionUrl + 'pubrepos' + '?query=' + requestItem.criteria.like + '&type=' + requestItem.criteria.type, { headers: this.headers });
	}
	public searchJournals(requestItem: RequestItem<DataRepositoryCriteria>): Observable<ExternalSourceItemModel[]> {
		return this.http.get<ExternalSourceItemModel[]>(this.actionUrl + 'journals' + '?query=' + requestItem.criteria.like + '&type=' + requestItem.criteria.type, { headers: this.headers });
	}
	public searchTaxonomies(requestItem: RequestItem<TagCriteria>): Observable<ExternalSourceItemModel[]> {
		return this.http.get<ExternalSourceItemModel[]>(this.actionUrl + 'taxonomies' + '?query=' + requestItem.criteria.like + '&type=' + requestItem.criteria.type, { headers: this.headers });
	}
	public searchPublications(requestItem: RequestItem<PublicationCriteria>): Observable<ExternalSourceItemModel[]> {
		return this.http.get<ExternalSourceItemModel[]>(this.actionUrl + 'publications' + '?query=' + requestItem.criteria.like + '&type=' + requestItem.criteria.type, { headers: this.headers });
	}

	public searchDatasetService(requestItem: RequestItem<ServiceCriteria>): Observable<ExternalSourceItemModel[]> {
		return this.http.get<ExternalSourceItemModel[]>(this.actionUrl + 'services' + '?query=' + requestItem.criteria.like + '&type=' + requestItem.criteria.type, { headers: this.headers });
	}

	public searchLicense(requestItem: RequestItem<LicenseCriteria>): Observable<ExternalSourceItemModel[]> {
		return this.http.get<ExternalSourceItemModel[]>(this.actionUrl + 'licenses' + '?query=' + requestItem.criteria.like + '&type=' + requestItem.criteria.type, { headers: this.headers });
	}

	public searchDatasetTags(requestItem: RequestItem<TagCriteria>): Observable<ExternalSourceItemModel[]> {
		// return Observable.of([
		// 	{ id: '1', name: 'Tag 1', description: '' },
		// 	{ id: '2', name: 'Tag 2', description: '' },
		// 	{ id: '3', name: 'Tag 3', description: '' },
		// 	{ id: '4', name: 'Tag 4', description: '' },
		// 	{ id: '5', name: 'Tag 5', description: '' },
		// 	{ id: '6', name: 'Tag 6', description: '' },
		// 	{ id: '7', name: 'Tag 7', description: '' },
		// 	{ id: '8', name: 'Tag 8', description: '' },
		// ]);
		return this.http.get<ExternalSourceItemModel[]>(this.actionUrl + "tags" + "?query=" + requestItem.criteria.like + "&type=" + requestItem.criteria.type, { headers: this.headers });
	}

	public searchDatasetSExternalDatasetservice(requestItem: RequestItem<ExternalDatasetCriteria>): Observable<ExternalSourceItemModel[]> {
		return this.http.get<ExternalSourceItemModel[]>(this.actionUrl + 'datasets' + '?query=' + requestItem.criteria.like + '&type=' + requestItem.criteria.type, { headers: this.headers });
	}

	public searchDMPResearchers(requestItem: RequestItem<ResearcherCriteria>): Observable<ExternalSourceItemModel[]> {
		return this.http.post<ExternalSourceItemModel[]>(this.configurationService.server + 'researchers/getWithExternal', requestItem, { headers: this.headers });
	}

	public searchDMPOrganizations(like: string, reference?: string): Observable<ExternalSourceItemModel[]> {
		const params = {
			query:like
		}
		if(reference){
			params['reference'] = reference;
		}
		return this.http.get<ExternalSourceItemModel[]>(this.actionUrl + 'organisations', { headers: this.headers, params: params });
	}

	// TODO: currently not used.
	public searchDMPProfiles(like: string): Observable<ExternalSourceItemModel[]> {
		return this.http.get<ExternalSourceItemModel[]>(this.actionUrl + 'datasetprofiles/get' + '?query=' + like, { headers: this.headers });
	}

	public validateIdentifier(like: string, type: string): Observable<boolean> {
		return this.http.get<boolean>(this.actionUrl + 'validation?query=' + like + '&type=' + type, { headers: this.headers });
	}

}
