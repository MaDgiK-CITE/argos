import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanLoad, Route, Router, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './services/auth/auth.service';

@Injectable()
export class AuthGuard implements CanActivate, CanLoad {
	constructor(private auth: AuthService, private router: Router) {
	}

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
		const url: string = state.url;
		if (!this.auth.current()) {
			this.router.navigate(['/unauthorized'], { queryParams: { returnUrl: url } });
			return false;
		}
		return true;
	}

	canLoad(route: Route): boolean {
		const url = `/${route.path}`;
		if (!this.auth.current()) {
			this.router.navigate(['/unauthorized'], { queryParams: { returnUrl: url } });
			return false;
		}
		return true;
	}
}
