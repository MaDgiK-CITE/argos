export interface ResearcherModel {
	id: string;
	name: String;
	reference: String;
	lastName: String;
	uri: String;
	email: String;
}
