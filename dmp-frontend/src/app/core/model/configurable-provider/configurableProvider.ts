export class ConfigurableProvider {
	configurableLoginId: string;
	type: string;
	name: string;
	logoUrl: string;
}
