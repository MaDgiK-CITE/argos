import { ConfigurableProvider } from "./configurableProvider";

export class Saml2ConfigurableProvider extends ConfigurableProvider{
    spEntityId: string;
	idpUrl: string;
	binding: string;
	assertionConsumerServiceUrl: string;
}