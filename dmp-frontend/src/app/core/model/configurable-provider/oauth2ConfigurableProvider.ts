import { ConfigurableProvider } from "./configurableProvider";

export class Oauth2ConfigurableProvider extends ConfigurableProvider{
    clientId: string;
	redirect_uri: string;
	oauthUrl: string;
	scope: string;
	state: string;
}