export interface ServiceModel {
	id: String;
	abbreviation: String;
	definition: String;
	uri: String;
	label: String;
	reference: String;
	source: String;
}

