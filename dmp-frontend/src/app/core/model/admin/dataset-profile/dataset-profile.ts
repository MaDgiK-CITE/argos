import { ValidationType } from "../../../common/enum/validation-type";
import { UserInfoListingModel } from "../../user/user-info-listing";

export interface DatasetProfile {
	label: string;
	type: string;
	enablePrefilling: boolean;
	sections: Section[];
	pages: Page[];
	status: number;
	version: number;
	description: string;
	language: string;
	users: UserInfoListingModel[];
}

export interface Page {
	id: string;
	ordinal: number;
	title: string;
}

export interface Section {
	sections: Section[];
	fieldSets: FieldSet[];
	defaultVisibility: boolean;
	page: string;
	ordinal: number;
	id: string;
	title: string;
	description: string;
}

export interface FieldSet {
	id: string;
	ordinal: number;
	multiplicity: Multiplicity;
	title: string;
	description: string;
	extendedDescription: string;
	additionalInformation:string;
	hasCommentField: boolean;
	fields: Field[];
}

export interface Multiplicity {
	min: number;
	max: number;
	placeholder: string;
	tableView: boolean;
}

export interface Field {
	id: string;
	ordinal: number;
	value: string;
	viewStyle: ViewStyle;
	datatype: string;
	page: number;
	defaultValue: DefaultValue;
	data: any;
	visible: Visibility;
	validations: ValidationType[];
	schematics: string[];
	export: boolean;
}

export interface ViewStyle {
	renderStyle: string;
	cssClass: string;
}

export interface DefaultValue {
	type: string;
	value: string;
}


export interface Visibility {
	rules: Rule[];
	style: string;
}

export interface Rule {
	ruleType: string;
	target: string;
	ruleStyle: string;
	value: string;
	valueType: string;
}
