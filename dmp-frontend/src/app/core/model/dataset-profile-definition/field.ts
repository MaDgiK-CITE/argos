import { ValidationType } from "../../common/enum/validation-type";
import { DefaultValue } from "./default-value";
import { Multiplicity } from "./multiplicity";
import { ViewStyle } from "./view-style";

export interface Field {

	id: string;
	title: string;
	value: any;
	defaultValue: DefaultValue;
	description: string;
	numbering: string;
	extendedDescription: string;
	additionalInformation: string;
	viewStyle: ViewStyle;
	defaultVisibility: boolean;
	page: number;
	multiplicity: Multiplicity;
	multiplicityItems: Array<Field>;
	data: any;
	validations: Array<ValidationType>;
	validationRequired;
	validationURL;
	ordinal: number;
}
