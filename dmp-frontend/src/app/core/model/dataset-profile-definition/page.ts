import { Section } from "./section";

export interface Page {
	ordinal: number;
	title: string;
	sections: Array<Section>;
}
