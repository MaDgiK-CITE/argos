import { Page } from "./page";
import { Rule } from "./rule";

export interface DatasetProfileDefinitionModel {
	status: number;
	pages: Page[];
	rules: Rule[];
}