// import { FormGroup } from "@angular/forms";
// import { JsonSerializer } from "../../../common/types/json/json-serializer";
// import { Serializable } from "../../../common/types/json/serializable";
// import { BaseModel } from "../../../models/BaseModel";
// import { CompositeField } from "./composite-field";

// export class FieldGroup extends BaseModel implements Serializable<FieldGroup> {
// 	public id: string;
// 	public title: string;
// 	public section: string;
// 	public value: string;
// 	public description: string;
// 	public extendedDescription: string;
// 	public defaultVisibility: boolean;
// 	public page: number;
// 	public compositeFields: Array<CompositeField> = new Array<CompositeField>();

// 	fromJSONObject(item: any): FieldGroup {
// 		this.id = item.id;
// 		this.title = item.title;
// 		this.value = item.value;
// 		this.description = item.description;
// 		this.extendedDescription = item.extendedDescription;
// 		this.defaultVisibility = item.defaultVisibility;
// 		this.page = item.page;
// 		this.compositeFields = new JsonSerializer<CompositeField>(CompositeField).fromJSONArray(item.compositeFields);
// 		return this;
// 	}

// 	buildForm(): FormGroup {
// 		const formGroup: FormGroup = this.formBuilder.group({
// 			/* id: [this.id],
//             title: [this.title],
//             value: [this.value],
//             description: [this.description],
//             extendedDescription: [this.extendedDescription],
//             defaultVisibility: [this.defaultVisibility],
//             page: [this.page] */
// 		});
// 		const compositeFieldsFormArray = new Array<FormGroup>();
// 		if (this.compositeFields) {
// 			this.compositeFields.forEach(item => {
// 				const form: FormGroup = item.buildForm();
// 				compositeFieldsFormArray.push(form);
// 			});
// 		}
// 		formGroup.addControl('compositeFields', this.formBuilder.array(compositeFieldsFormArray));

// 		return formGroup;
// 	}
// }
