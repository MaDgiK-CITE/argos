﻿export interface ViewStyle {
	cssClass: string;
	renderStyle: string;
}
