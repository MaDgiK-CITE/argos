import { ExternalDatasetTypeEnum } from "@app/core/common/enum/external-dataset-type-enum";
import { DatasetProfileComboBoxType } from "../../../common/enum/dataset-profile-combo-box-type";
import { DatasetProfileInternalDmpEntitiesType } from "../../../common/enum/dataset-profile-internal-dmp-entities-type";

export interface FieldData {
	label: string;
}

export interface AutoCompleteFieldData extends FieldData {
	type: DatasetProfileComboBoxType;
	autoCompleteSingleDataList: AutoCompleteSingleData[];
	multiAutoComplete: boolean;
}

export interface AuthAutoCompleteData extends FieldData {
	url: string;
	method: string;
	body: string;
	path: string;
	type: string;
}

export interface AutoCompleteSingleData extends FieldData {
	url: string;
	optionsRoot: string;
	autoCompleteOptions: FieldDataOption;
	autocompleteType: number;
	hasAuth: boolean;
	method: string;
	auth: AuthAutoCompleteData;
}

export interface CheckBoxFieldData extends FieldData {

}

export interface BooleanDecisionFieldData extends FieldData {

}

export interface FreeTextFieldData extends FieldData {

}

export interface RadioBoxFieldData extends FieldData {
	options: Array<FieldDataOption>;
}

export interface TextAreaFieldData extends FieldData {

}

export interface RichTextAreaFieldData extends FieldData {

}

// export interface TableFieldData extends FieldData {
// 	headers: string[];
// 	rows: Array<string[]>;
// }

export interface UploadFieldData extends FieldData {
	types: Array<FieldDataOption>;
	maxFileSizeInMB: number;
}

export interface WordListFieldData extends FieldData {
	type: DatasetProfileComboBoxType;
	options: Array<FieldDataOption>;
	multiList: boolean;
}

export interface FieldDataOption extends FieldData {
	label: string;
	value: string;
	source: string;
}

export interface DatePickerFieldData extends FieldData {

}

export interface ResearchersAutoCompleteFieldData extends FieldData {
	type: DatasetProfileInternalDmpEntitiesType;
	multiAutoComplete: boolean;
}

export interface DatasetsAutoCompleteFieldData extends FieldData {
	type: DatasetProfileInternalDmpEntitiesType;
	multiAutoComplete: boolean;
	autoCompleteType: number;
}

export interface DmpsAutoCompleteFieldData extends FieldData {
	type: DatasetProfileInternalDmpEntitiesType;
	multiAutoComplete: boolean;
}

export interface ExternalDatasetsFieldData extends FieldData {
	multiAutoComplete: boolean;
	type?: ExternalDatasetTypeEnum;
}

export interface DataRepositoriesFieldData extends FieldData {
	multiAutoComplete: boolean;
}

export interface TaxonomiesFieldData extends FieldData {
	multiAutoComplete: boolean;
}

export interface LicensesFieldData extends FieldData {
	multiAutoComplete: boolean;
}

export interface PublicationsFieldData extends FieldData {
	multiAutoComplete: boolean;
}

export interface RegistriesFieldData extends FieldData {
	multiAutoComplete: boolean;
}

export interface ServicesFieldData extends FieldData {
	multiAutoComplete: boolean;
}

export interface TagsFieldData extends FieldData {

}

export interface ResearchersFieldData extends FieldData {

}

export interface OrganizationsFieldData extends AutoCompleteFieldData {

}

export interface DatasetIdentifierFieldData extends FieldData {

}

export interface CurrencyFieldData extends FieldData {

}

export interface ValidationFieldData extends FieldData {

}
