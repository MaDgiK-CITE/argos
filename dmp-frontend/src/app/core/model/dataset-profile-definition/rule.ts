export class Rule {
	sourceField: string;
	targetField: string;
	requiredValue: any;
	type: string;
}