

export interface Multiplicity {
	min: number;
	max: number;
	placeholder: string;
	tableView: boolean;
}

