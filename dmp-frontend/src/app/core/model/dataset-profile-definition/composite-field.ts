import { Field } from './field';
import { Multiplicity } from './multiplicity';

export interface CompositeField {
	fields: Array<Field>;
	ordinal: number;
	id: string;
	numbering: string;
	multiplicity: Multiplicity;
	multiplicityItems: Array<CompositeField>;
	title: string;
	description: string;
	extendedDescription: string;
	additionalInformation:string;
	hasCommentField: boolean;
	commentFieldValue: string;
}