import { CompositeField } from "./composite-field";

export interface Section {
	sections: Array<Section>;
	defaultVisibility: boolean;
	page: number;
	numbering: string;
	ordinal: number;
	id: string;
	title: string;
	description: string;
	compositeFields: Array<CompositeField>;
}