export interface DefaultValue {
	type: string;
	value: string;
}
