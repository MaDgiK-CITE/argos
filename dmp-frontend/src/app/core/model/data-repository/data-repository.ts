export interface DataRepositoryModel {
	id: string;
	name: string;
	abbreviation: string;
	uri: string;
	pid: string;
	info: string;
	source: string;
}
