import { RequestItem } from '../../query/request-item';
import { ColumnOrdering } from './column-ordering';

export class DataTableRequest<T> extends RequestItem<T> {
	offset = 0;
	length = 0;
	public orderings: ColumnOrdering;
	constructor(offset: number, length: number, orderings: ColumnOrdering) {
		super();
		this.length = length;
		this.offset = offset;
		this.orderings = orderings;
	}
}

export class DataTableMultiTypeRequest<T> extends RequestItem<T> {
	dmpOffset = 0;
	datasetOffset = 0;
	length = 0;
	public orderings: ColumnOrdering;
	constructor(dmpOffset: number, datasetOffset: number, length: number, orderings: ColumnOrdering) {
		super();
		this.length = length;
		this.dmpOffset = dmpOffset;
		this.datasetOffset = datasetOffset;
		this.orderings = orderings;
	}
}
