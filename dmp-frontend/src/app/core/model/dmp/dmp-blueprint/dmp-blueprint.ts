export interface DmpBlueprint {
	id: string;
	label: string;
	definition: DmpBlueprintDefinition;
	status: number;
	created: Date;
	modified: Date;
	description: string;
}

export interface DmpBlueprintDefinition {
    sections: SectionDmpBlueprint[];
}

export interface SectionDmpBlueprint {
    id: string;
    label: string;
    description: string;
    ordinal: number;
    fields: FieldInSection[];
    hasTemplates: boolean;
    descriptionTemplates?: DescriptionTemplatesInSection[];
}

export interface FieldInSection {
    id: string;
    category: FieldCategory;
    type: number;
    label: string;
    placeholder: string;
    description: string;
    required: boolean;
    ordinal: number;
}

export enum FieldCategory {
    SYSTEM = 0,
    EXTRA = 1
}

export enum SystemFieldType {
    TEXT = 0,
    HTML_TEXT = 1,
    RESEARCHERS=  2,
    ORGANIZATIONS = 3,
    LANGUAGE = 4,
    CONTACT = 5,
    FUNDER = 6,
    GRANT = 7,
    PROJECT = 8,
    LICENSE = 9,
    ACCESS_RIGHTS = 10
}

export interface DescriptionTemplatesInSection {
    id: string;
    descriptionTemplateId: string;
    label: string;
    minMultiplicity: number;
    maxMultiplicity: number;
}

export enum ExtraFieldType {
    TEXT = 0,
    RICH_TEXT = 1,
    DATE =  2,
    NUMBER = 3
}