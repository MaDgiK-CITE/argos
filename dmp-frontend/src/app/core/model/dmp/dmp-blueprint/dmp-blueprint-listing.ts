import { DmpBlueprintDefinition } from "./dmp-blueprint";

export interface DmpBlueprintListing {
	id: string;
	label: string;
	definition: DmpBlueprintDefinition;
	status: number;
	created: Date;
	modified: Date;
}