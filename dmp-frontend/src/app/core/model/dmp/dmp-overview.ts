import { OrganizationModel } from "../organisation/organization";
import { UserInfoListingModel } from "../user/user-info-listing";
import { DmpAssociatedProfileModel } from "../dmp-profile/dmp-associated-profile";
import { ResearcherModel } from "../researcher/researcher";
import { GrantOverviewModel } from "../grant/grant-overview";
import { DatasetOverviewModel } from "../dataset/dataset-overview";
import { DoiModel } from "../doi/doi";

export interface DmpOverviewModel {
	id: string;
	label: string;
	profile: string;
	creationTime: string;
	modifiedTime: string;
	version: number;
	status: number;
	isPublic: boolean;
	groupId: string;
	description: string;
	grant: GrantOverviewModel;
	associatedProfiles: DmpAssociatedProfileModel[];
	users: UserInfoListingModel[];
	organisations: OrganizationModel[];
	datasets: DatasetOverviewModel[];
	datasetsToBeFinalized: string[];
	researchers: ResearcherModel[];
	finalizedAt: Date;
	publishedAt: Date;
	doi: string;
	dois: DoiModel[];
}
