import { FormGroup, FormBuilder } from "@angular/forms";
import { BackendErrorValidator } from "@common/forms/validation/custom-validator";
import { ValidationErrorModel } from "@common/forms/validation/error-model/validation-error-model";
import { ValidationContext } from "@common/forms/validation/validation-context";

export class DmpDatasetProfileSectionsFormModel {
    public  dmpSectionIndex: number[] = [];
    public validationErrorModel: ValidationErrorModel = new ValidationErrorModel();

    fromModel(item: any): DmpDatasetProfileSectionsFormModel {
        this.dmpSectionIndex = item.dmpSectionIndex;
        return this;
    }

    buildForm(context: ValidationContext = null, disabled: boolean = false): FormGroup {
		if (context == null) { context = this.createValidationContext(); }
        const formGroup = new FormBuilder().group({
			language: [{ value: this.dmpSectionIndex, disabled: disabled }, context.getValidation('dmpSectionIndex').validators],
		});
        return formGroup;
    }

    createValidationContext(): ValidationContext {
		const baseContext: ValidationContext = new ValidationContext();
		baseContext.validation.push({ key: 'dmpSectionIndex', validators: [BackendErrorValidator(this.validationErrorModel, 'dmpSectionIndex')] });
		return baseContext;
	}
}