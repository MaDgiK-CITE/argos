import { DmpDatasetProfileSectionsFormModel } from "./dmp-dataset-profile-sections-form.model";

export interface DmpDatasetProfile {
	id: string;
	descriptionTemplateId: string;
	label: string;
	data: DmpDatasetProfileSectionsFormModel;
}