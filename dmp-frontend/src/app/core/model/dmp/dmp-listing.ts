import { DmpStatus } from "../../common/enum/dmp-status";
import { DmpAssociatedProfileModel } from '../dmp-profile/dmp-associated-profile';

export interface DmpListingModel {
	id: string;
	label: string;
	description: String;
	status: DmpStatus;
	grant: String;
	grantId: String;
	grantAbbreviation: String;
	profile: String;
	creationTime: string;
	modifiedTime: string;
	publishedAt: Date;
	finalizedAt: Date;
	organisations: String;
	groupId: string;
	version: number;
	datasets: any[];
	associatedProfiles: DmpAssociatedProfileModel[];
	users: any[];
	public: boolean;
}
