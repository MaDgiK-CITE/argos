import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ValidationContext } from '@common/forms/validation/validation-context';

export interface CostModel {
	code: string;
	description: string;
	title: string;
	value: number;
}
