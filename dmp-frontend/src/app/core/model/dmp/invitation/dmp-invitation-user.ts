import { FormBuilder, FormGroup } from '@angular/forms';
import { Serializable } from '@common/types/json/serializable';

export class DmpInvitationUser implements Serializable<DmpInvitationUser> {

	public id: string;
	public email: string;
	public name: string;

	fromJSONObject(item: any): DmpInvitationUser {
		this.id = item.id;
		this.email = item.email;
		this.name = item.name;
		return this;
	}

	buildForm(): FormGroup {
		return new FormBuilder().group({
			id: [this.id],
			email: [this.email],
			name: [this.name]
		});
	}

}
