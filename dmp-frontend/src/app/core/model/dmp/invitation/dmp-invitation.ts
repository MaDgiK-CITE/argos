import { FormBuilder, FormGroup } from '@angular/forms';
import { DmpInvitationUser } from './dmp-invitation-user';

export class DmpInvitation {

	public dataManagementPlan: string;
	public users = new Array<DmpInvitationUser>();
	public role: number;

	buildForm(): FormGroup {
		const formGroup = new FormBuilder().group({
			dataManagementPlan: [this.dataManagementPlan],
			users: [this.users],
			role: [this.role]
		});

		return formGroup;
	}
}
