import { DmpDynamicFieldDependency } from "./dmp-dynamic-field-dependency";

export interface DmpDynamicField {
	id: string;
	name: string;
	required: boolean;
	queryProperty;
	value: string;
	dependencies: Array<DmpDynamicFieldDependency>;
}
