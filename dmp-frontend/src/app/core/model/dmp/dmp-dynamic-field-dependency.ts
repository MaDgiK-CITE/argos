export interface DmpDynamicFieldDependency {
	id: string;
	queryProperty: string;
}
