export interface DmpExtraField {
	id: string;
	value: string;
}