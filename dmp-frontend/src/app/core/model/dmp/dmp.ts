import { DmpProfileDefinition } from "../dmp-profile/dmp-profile";
import { OrganizationModel } from "../organisation/organization";
import { GrantListingModel } from "../grant/grant-listing";
import { ResearcherModel } from "../researcher/researcher";
import { UserModel } from "../user/user";
import { DmpDynamicField } from "./dmp-dynamic-field";
import { UserInfoListingModel } from "../user/user-info-listing";
import { ProjectModel } from "../project/project";
import { FunderModel } from "../funder/funder";
import { DmpStatus } from '@app/core/common/enum/dmp-status';
import { DatasetWizardModel } from '../dataset/dataset-wizard';
import { DmpDatasetProfile } from "./dmp-dataset-profile/dmp-dataset-profile";
import { DmpExtraField } from "./dmp-extra-field";

export interface DmpModel {
	id: string;
	label: string;
	groupId: String;
	profile: DmpProfile;
	version: number;
	status: DmpStatus;
	lockable: boolean;
	description: String;
	grant: GrantListingModel;
	project: ProjectModel;
	funder: FunderModel;
	datasets: DatasetWizardModel[];
	datasetsToBeFinalized: string[];
	profiles: DmpDatasetProfile[];
	organisations: OrganizationModel[];
	researchers: ResearcherModel[];
	associatedUsers: UserModel[];
	users: UserInfoListingModel[];
	creator: UserModel;
	extraFields: Array<DmpExtraField>;
	dynamicFields: Array<DmpDynamicField>;
	modified: Date;
	extraProperties: Map<String, any>;
	language: String;
}


export interface DmpProfile {
	id: string;
	label: string;
}