import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { DatasetProfileModel } from '@app/core/model/dataset/dataset-profile';
import { DmpModel } from '@app/core/model/dmp/dmp';
import { BackendErrorValidator } from '@common/forms/validation/custom-validator';
import { ValidationErrorModel } from '@common/forms/validation/error-model/validation-error-model';
import { ValidationContext } from '@common/forms/validation/validation-context';

export class DmpCreateWizardFormModel {
	dmp: DmpModel;
	datasetProfile: DatasetProfileModel;
	public validationErrorModel: ValidationErrorModel = new ValidationErrorModel();

	fromModel(item: DmpCreateWizardFormModel): DmpCreateWizardFormModel {
		this.dmp = item.dmp;
		this.datasetProfile = item.datasetProfile;

		return this;
	}

	buildForm(context: ValidationContext = null): FormGroup {
		if (context == null) { context = this.createValidationContext(); }
		const formBuilder = new FormBuilder();
		const formGroup = formBuilder.group({
			dmp: [this.dmp, context.getValidation('dmp').validators],
			datasetProfile: [this.datasetProfile, context.getValidation('datasetProfile').validators],
		});

		return formGroup;
	}

	createValidationContext(): ValidationContext {
		const baseContext: ValidationContext = new ValidationContext();
		baseContext.validation.push({ key: 'datasetProfile', validators: [Validators.required, BackendErrorValidator(this.validationErrorModel, 'datasetProfile')] });
		baseContext.validation.push({ key: 'dmp', validators: [BackendErrorValidator(this.validationErrorModel, 'dmp')] });

		return baseContext;
	}
}
