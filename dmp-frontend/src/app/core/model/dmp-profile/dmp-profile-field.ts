import { DmpProfileFieldDataType } from '../../common/enum/dmp-profile-field-type';
import { DmpProfileType } from '../../common/enum/dmp-profile-type';
import { DmpProfileExternalAutoCompleteFieldDataEditorModel } from '../../../ui/admin/dmp-profile/editor/external-autocomplete/dmp-profile-external-autocomplete-field-editor.model';

export interface DmpProfileField {
	id: string;
	type: DmpProfileType;
	dataType: DmpProfileFieldDataType;
	required: boolean;
	label: string;
	value: any;
	externalAutocomplete?: DmpProfileExternalAutoCompleteFieldDataEditorModel;
}
