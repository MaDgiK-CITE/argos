import { DmpProfileField } from "./dmp-profile-field";


export interface DmpProfile {
	id: string;
	label: string;
	definition: DmpProfileDefinition;
	status: number;
	created: Date;
	modified: Date;
	description: string;
}

export interface DmpProfileDefinition {
	fields: DmpProfileField[];
}
