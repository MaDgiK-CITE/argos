import { DmpProfileDefinition } from "./dmp-profile";

export interface DmpProfileListing {
	id: string;
	label: string;
	definition: DmpProfileDefinition;
	status: number;
	created: Date;
	modified: Date;
}
