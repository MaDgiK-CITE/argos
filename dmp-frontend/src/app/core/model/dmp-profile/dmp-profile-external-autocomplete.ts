export interface DmpProfileExternalAutoCompleteField {
	url: string;
	optionsRoot: string;
	multiAutoComplete: boolean;
	label: string;
	value: string;
}
