import { RecentActivityModel } from "./recent-activity.model";

export class RecentDatasetModel extends RecentActivityModel {
	dmp: String;
	dmpId: String;
	dataRepositories: String;
	registries: String;
	services: String;
}
