import { RecentActivityModel } from './recent-activity.model';
import { RecentDatasetModel } from './recent-dataset-activity.model';
import { DmpAssociatedProfileModel } from '../dmp-profile/dmp-associated-profile';
import { UserInfoListingModel } from '../user/user-info-listing';
import { DatasetUrlListing } from '../dataset/dataset-url-listing';

export class RecentDmpModel extends RecentActivityModel {
	doi: String;
	extraProperties: Map<String, any>;
	datasets: DatasetUrlListing[];
	associatedProfiles: DmpAssociatedProfileModel[];
	organisations: String;
	groupId: string;
}
