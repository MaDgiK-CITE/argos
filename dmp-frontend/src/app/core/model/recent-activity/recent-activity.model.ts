import { RecentActivityType } from '@app/core/common/enum/recent-activity-type';
import { UserInfoListingModel } from '../user/user-info-listing';
import { DatasetProfileModel } from '../dataset/dataset-profile';

export class RecentActivityModel {
	id: string;
	title: string;
	description: String;
	created: Date;
	modified: Date;
	status: number;
	version: number;
	grant: String;
	grantAbbreviation: String;
	grantId: String;
	finalizedAt: Date;
	publishedAt: Date;
	profile: DatasetProfileModel;
	type: RecentActivityType;
	users: UserInfoListingModel[];
	public: boolean;
}

