import { AuthProvider } from "../../common/enum/auth-provider";

export interface LoginInfo {
	ticket: string;
	provider: AuthProvider;
	data?: any;
}