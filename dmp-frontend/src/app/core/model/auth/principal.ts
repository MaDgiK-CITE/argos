import { AppRole } from "../../common/enum/app-role";

export interface Principal {
	id: string;
	token: string;
	name: string;
	email: string;
	expiresAt: Date;
	authorities: AppRole[];
	avatarUrl: string;
	timezone: string;
	language: string;
	culture: string;
	zenodoEmail: string;
}
