export interface Credential {
	username: string;
	secret: string;
}
