export interface ExternalSourceItemModel {
	id: String;
	name: String;
	description: String;
}
