import { ExternalSourceUrlModel } from "./external-source-url";

export class ExternalSourcesConfiguration {
	public registries: Array<ExternalSourceUrlModel>;
	public dataRepositories: Array<ExternalSourceUrlModel>;
	public services: Array<ExternalSourceUrlModel>;
	public externalDatasets: Array<ExternalSourceUrlModel>;
	public tags: Array<ExternalSourceUrlModel>;
}
