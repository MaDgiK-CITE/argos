export interface DescriptionTemplateType {
	id: string;
	name: string;
	status: number;
}