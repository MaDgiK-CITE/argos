
export interface DatasetProfileModel {
	id: string;
	label: string;
	description: string;
}

export interface DatasetProfileWithPrefillingModel {
	id: string;
	label: string;
	enablePrefilling: boolean;
}

// export class DatasetProfileModel implements Serializable<DatasetProfileModel> {
// 	public id: String;
// 	public label: String;

// 	fromJSONObject(item: any): DatasetProfileModel {
// 		this.id = item.id;
// 		this.label = item.label;

// 		return this;
// 	}

// 	buildForm(context: ValidationContext = null, disabled: boolean = false): FormGroup {
// 		if (context == null) { context = this.createValidationContext(); }

// 		const formGroup = new FormBuilder().group({
// 			id: [{ value: this.id, disabled: disabled }],
// 		});

// 		return formGroup;
// 	}

// 	createValidationContext(): ValidationContext {
// 		const baseContext: ValidationContext = new ValidationContext();
// 		//baseContext.validation.push({ key: 'id', validators: [Validators.required, BackendErrorValidator(this.validationErrorModel, 'id')] });
// 		return baseContext;
// 	}
// }
