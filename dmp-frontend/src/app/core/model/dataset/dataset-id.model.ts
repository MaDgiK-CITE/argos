import { FormGroup, FormBuilder } from '@angular/forms';
import { isNullOrUndefined } from '@app/utilities/enhancers/utils';

export class DatasetIdModel {
	identifier: string;
	type: string;

	constructor(data: any) {
		try{
		const parsed = JSON.parse(data);
		if (!isNullOrUndefined(parsed)) {
			if(typeof parsed !== 'string'){
				this.identifier = parsed.identifier;
				this.type = parsed.type;
			}
			else{
				const parsedObjectFromString = JSON.parse(parsed);
				this.identifier = parsedObjectFromString.identifier;
				this.type = parsedObjectFromString.type;
			}
		}
		}
		catch(error){
			console.warn('Could not parse DatasetIdModel');
		}
	}

	buildForm(): FormGroup {
		return new FormBuilder().group({
			identifier: [this.identifier],
			type: [this.type]
		});
	}
}
