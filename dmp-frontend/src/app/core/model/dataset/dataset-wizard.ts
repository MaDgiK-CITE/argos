import { DataRepositoryModel } from "../data-repository/data-repository";
import { DatasetProfileDefinitionModel } from "../dataset-profile-definition/dataset-profile-definition";
import { DmpModel } from "../dmp/dmp";
import { ExternalDatasetModel } from "../external-dataset/external-dataset";
import { RegistryModel } from "../registry/registry";
import { ServiceModel } from "../service/service";
import { TagModel } from "../tag/tag";
import { DatasetProfileModel } from "./dataset-profile";

export interface DatasetWizardModel {
	id?: string;
	label?: string;
	uri?: String;
	description?: String;
	status?: number;
	dmp?: DmpModel;
	dmpSectionIndex?: number;
	datasetProfileDefinition?: DatasetProfileDefinitionModel;
	registries?: RegistryModel[];
	services?: ServiceModel[];
	dataRepositories?: DataRepositoryModel[];
	tags?: TagModel[];
	externalDatasets?: ExternalDatasetModel[];
	profile?: DatasetProfileModel;
	isProfileLatestVersion?: Boolean;
	modified?: Date;
}
