export interface DatasetUrlListing {
	label: String;
	url: String;
}
