import { DatasetProfileModel } from './dataset-profile';

export interface DatasetListingModel {
	id: string;
	label: string;
	dmp: String;
	dmpId: String;
	grant: String;
	grantId: String;
	grantAbbreviation: String;
	profile: DatasetProfileModel;
	dataRepositories: String;
	registries: String;
	services: String;
	description: String;
	status: number;
	created: Date;
	modified: Date;
	finalizedAt: Date;
	dmpPublishedAt?: Date;
	version: number;
	users: any[];
	public: boolean;
	isProfileLatestVersion: boolean;
}
