import { DatasetProfileModel } from "./dataset-profile";
import { GrantOverviewModel } from '../grant/grant-overview';
import { DmpOverviewModel } from '../dmp/dmp-overview';

export interface DatasetOverviewModel {
	id: string;
	label: string;
	status: number;
	datasetTemplate: DatasetProfileModel;

	users: any[];
	dmp: DmpOverviewModel;
	grant: GrantOverviewModel;
	description: String;
	public: boolean;
	modified: Date;
}
