import { DataRepositoryModel } from '../data-repository/data-repository';
import { RegistryModel } from '../registry/registry';
import { ServiceModel } from '../service/service';

export interface DatasetModel {
	id: String;
	label: String;
	profile: String;
	uri: String;
	status: String;
	description: String;
	services: ServiceModel[];
	registries: RegistryModel[];
	dataRepositories: DataRepositoryModel[];
}
// export class DatasetModel implements Serializable<DatasetModel> {
// 	public id: String;
// 	public label: String;
// 	public profile: String;
// 	public uri: String;
// 	public status: String;
// 	public description: String;
// 	public services: ServiceModel[] = [];
// 	public registries: RegistryModel[] = [];
// 	public dataRepositories: DataRepositoryModel[] = [];

// 	public validationErrorModel: ValidationErrorModel = new ValidationErrorModel();

// 	fromJSONObject(item: any): DatasetModel {
// 		this.id = item.id;
// 		this.label = item.label;
// 		this.profile = item.profile;
// 		this.uri = item.uri;
// 		this.status = item.status;
// 		this.description = item.description;
// 		this.services = new JsonSerializer<ServiceModel>(ServiceModel).fromJSONArray(item.services);
// 		this.registries = new JsonSerializer<RegistryModel>(RegistryModel).fromJSONArray(item.registries);
// 		this.dataRepositories = new JsonSerializer<DataRepositoryModel>(DataRepositoryModel).fromJSONArray(item.dataRepositories);

// 		return this;
// 	}

// 	buildForm(context: ValidationContext = null, disabled: boolean = false): FormGroup {
// 		if (context == null) { context = this.createValidationContext(); }

// 		const formGroup = new FormBuilder().group({
// 			label: [{ value: this.label, disabled: disabled }, context.getValidation('label').validators],
// 			profile: [{ value: this.profile, disabled: disabled }, context.getValidation('profile').validators],
// 			uri: [{ value: this.uri, disabled: disabled }, context.getValidation('uri').validators],
// 			status: [{ value: this.status, disabled: disabled }, context.getValidation('status').validators],
// 			description: [{ value: this.description, disabled: disabled }, context.getValidation('description').validators],
// 			services: [{ value: this.services, disabled: disabled }, context.getValidation('services').validators],
// 			registries: [{ value: this.registries, disabled: disabled }, context.getValidation('registries').validators],
// 			dataRepositories: [{ value: this.dataRepositories, disabled: disabled }, context.getValidation('dataRepositories').validators]
// 		});

// 		return formGroup;
// 	}

// 	createValidationContext(): ValidationContext {
// 		const baseContext: ValidationContext = new ValidationContext();
// 		baseContext.validation.push({ key: 'label', validators: [Validators.required, BackendErrorValidator(this.validationErrorModel, 'label')] });
// 		baseContext.validation.push({ key: 'profile', validators: [Validators.required, BackendErrorValidator(this.validationErrorModel, 'profile')] });
// 		baseContext.validation.push({ key: 'uri', validators: [BackendErrorValidator(this.validationErrorModel, 'uri')] });
// 		baseContext.validation.push({ key: 'status', validators: [Validators.required, BackendErrorValidator(this.validationErrorModel, 'status')] });
// 		baseContext.validation.push({ key: 'description', validators: [BackendErrorValidator(this.validationErrorModel, 'description')] });
// 		baseContext.validation.push({ key: 'services', validators: [Validators.required, BackendErrorValidator(this.validationErrorModel, 'services')] });
// 		baseContext.validation.push({ key: 'registries', validators: [Validators.required, BackendErrorValidator(this.validationErrorModel, 'registries')] });
// 		baseContext.validation.push({ key: 'dataRepositories', validators: [Validators.required, BackendErrorValidator(this.validationErrorModel, 'dataRepositories')] });

// 		return baseContext;
// 	}
// }
