export interface AuthnRequestModel {
	authnRequestXml: string;
	relayState: string;
	algorithm: string;
	signature: string;
}