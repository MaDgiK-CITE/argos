export interface TaxonomyModel {
	id: string;
	name: string;
	abbreviation: string;
	uri: string;
	pid: string;
	info: string;
	source: string;
}
