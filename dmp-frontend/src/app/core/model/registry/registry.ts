
export interface RegistryModel {
	abbreviation: String;
	definition: String;
	id: String;
	pid: String;
	label: String;
	reference: String;
	uri: String;
	source: String;
}
