import { Status } from "../../common/enum/status";

export interface OrganizationModel {
	id: string;
	name: string;
	label: string;
	status: Status;
	tag: string;
	reference?: string;
}
