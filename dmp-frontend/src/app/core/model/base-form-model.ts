import { FormBuilder } from '@angular/forms';
export abstract class BaseFormModel {
	public formBuilder: FormBuilder = new FormBuilder();
}
