export interface DashboardStatisticsModel {
	totalDataManagementPlanCount: number;
	totalGrantCount: number;
	totalDataSetCount: number;
	totalOrganisationCount: number;
}
