import { SearchBarType } from "../../../ui/misc/navigation/navigation.component";

export interface SearchBarItem {
	id: string;
	label: string;
	type: SearchBarType;
	isPublished: boolean;
}
