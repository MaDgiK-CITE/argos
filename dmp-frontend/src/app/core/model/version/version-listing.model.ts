export interface VersionListingModel {
	id: string;
	groupId: string;
	version: number;
}
