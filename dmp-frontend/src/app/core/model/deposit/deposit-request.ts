export class DepositRequest {
    repositoryId: string;
    dmpId: string;
    accessToken: string;
}

export class DepositCode {
    repositoryId: string;
    code: string;
}