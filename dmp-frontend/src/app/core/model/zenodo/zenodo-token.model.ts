export class ZenodoToken {
	userId: string;
	expiresIn: number;
	accessToken:string;
	email: string;
	refreshToken: string;
}
