import { UrlListingItem } from "../../../library/url-listing/url-listing-item";
import { GrantType } from '../../common/enum/grant-type';
import { Status } from '../../common/enum/status';

export interface GrantListingModel {
	id?: string;
	label?: string;
	abbreviation?: string;
	reference?: string;
	type?: GrantType;
	uri?: String;
	status?: Status;
	startDate?: Date;
	endDate?: Date;
	description?: String;
	contentUrl?: string;
	funderId?: string;
	files?: ContentFile[];
	dmps?: UrlListingItem[];
	source?: string;
}

export interface ContentFile {
	filename?: string;
	id?: string;
	location?: string;
	type?: string;
}
