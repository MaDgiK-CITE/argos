export interface GrantOverviewModel {
	id: String;
	label: String;
	uri: String;
	abbreviation: String;
	description: String;
	startDate: Date;
	endDate: Date;
}
