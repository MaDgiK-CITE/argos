export interface CultureInfo {
	name: string;
	displayName: string;
	nativeName: string;
}
