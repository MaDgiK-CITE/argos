import { Status } from "../../common/enum/status";

export class FunderModel {
	id: string;
	label: string;
	reference: string;
	definition: string;
	status: Status;
	created: Date;
	modified: Date;
	type: number;
}
