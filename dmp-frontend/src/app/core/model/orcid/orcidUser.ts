export class OrcidUser {
	orcidId: string;
	name: string;
	email: string;
}
