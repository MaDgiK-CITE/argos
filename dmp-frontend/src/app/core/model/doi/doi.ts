export interface DoiModel {
    id: string;
    repositoryId: string;
    doi: string;
    createdAt: Date;
    updatedAt: Date;
}