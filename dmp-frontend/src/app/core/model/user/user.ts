import { AppRole } from "../../common/enum/app-role";

export interface UserModel {
	id: String;
	name: String;
	appRoles: AppRole[];
	email: String;
}
