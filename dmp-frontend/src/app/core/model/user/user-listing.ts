import { RoleOrganizationType } from '@app/core/common/enum/role-organization-type';
import { AppRole } from "../../common/enum/app-role";
import { OrganizationModel } from '../organisation/organization';

export interface UserListingModel {
	id: String;
	name: String;
	email: string;
	appRoles: AppRole[];
	associatedDmps: any[];
	language: any;
	culture: any;
	timezone: String;
	zenodoEmail: String;
	avatarUrl: String;
	organization: OrganizationModel;
	roleOrganization: RoleOrganizationType;
}
