export interface UserInfoListingModel {
	id: String;
	name: String;
	role: number;
	email: String;
}
