import { AuthProvider } from '@app/core/common/enum/auth-provider';

export class UserCredentialModel {
	email: string;
	provider: AuthProvider;
}
