import { LoginConfiguration } from './login-configuration.model';

export class LoginProviders {

	private _enabled: number[] = [];
	get enabled(): number[] {
		return this._enabled;
	}

	private _facebookConfiguration: LoginConfiguration;
	get facebookConfiguration(): LoginConfiguration {
		return this._facebookConfiguration;
	}

	private _googleConfiguration: LoginConfiguration;
	get googleConfiguration(): LoginConfiguration {
		return this._googleConfiguration;
	}

	private _linkedInConfiguration: LoginConfiguration;
	get linkedInConfiguration(): LoginConfiguration {
		return this._linkedInConfiguration;
	}

	private _twitterConfiguration: LoginConfiguration;
	get twitterConfiguration(): LoginConfiguration {
		return this._twitterConfiguration;
	}

	private _b2accessConfiguration: LoginConfiguration;
	get b2accessConfiguration(): LoginConfiguration {
		return this._b2accessConfiguration;
	}

	private _orcidConfiguration: LoginConfiguration;
	get orcidConfiguration(): LoginConfiguration {
		return this._orcidConfiguration;
	}

	private _openAireConfiguration: LoginConfiguration;
	get openAireConfiguration(): LoginConfiguration {
		return this._openAireConfiguration;
	}

	private _zenodoConfiguration: LoginConfiguration;
	get zenodoConfiguration(): LoginConfiguration {
		return this._zenodoConfiguration;
	}

	public static parseValue(value: any): LoginProviders {
		const obj: LoginProviders = new LoginProviders();
		obj._enabled = value.enabled;
		obj._facebookConfiguration = value.facebookConfiguration;
		obj._googleConfiguration = value.googleConfiguration;
		obj._linkedInConfiguration = value.linkedInConfiguration;
		obj._twitterConfiguration = value.twitterConfiguration;
		obj._b2accessConfiguration = value.b2accessConfiguration;
		obj._orcidConfiguration = value.orcidConfiguration;
		obj._openAireConfiguration = value.openAireConfiguration;
		obj._zenodoConfiguration = value.zenodoConfiguration;
		return obj;
	}
}
