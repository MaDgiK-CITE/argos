import { Guid } from '@common/types/guid';
import { UserInfoListingModel } from '../user/user-info-listing';

export class LockModel {
	id: Guid;
	target: Guid;
	lockedBy: UserInfoListingModel;
	lockedAt: Date;
	touchedAt: Date;

	constructor(targetId: string, lockedBy: any) {
		this.lockedAt = new Date();
		this.touchedAt = new Date();
		this.target = Guid.parse(targetId);
		this.lockedBy = lockedBy;

	}
}
