import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ValidationErrorModel } from '@common/forms/validation/error-model/validation-error-model';

export interface ContactEmail {
	subject: string;
	description: string;
}

export class ContactEmailFormModel {
	subject: string;
	description: string;
	public validationErrorModel: ValidationErrorModel = new ValidationErrorModel();

	fromModel(item: ContactEmail): ContactEmailFormModel {
		this.subject = item.subject;
		this.description = item.description;
		return this;
	}

	buildForm(): FormGroup {
		const formGroup = new FormBuilder().group({
			subject: [this.subject, [Validators.required]],
			description: [this.description, [Validators.required]]
		});
		return formGroup;
	}
}
