import { Status } from "../../common/enum/status";
import { UrlListingItem } from "../../../library/url-listing/url-listing-item";
import { ProjectType } from "../../common/enum/project-type";

export class ProjectModel {
	id?: string;
	label?: string;
	abbreviation?: string;
	reference?: string;
	type?: ProjectType;
	uri?: String;
	status?: Status;
	startDate?: Date;
	endDate?: Date;
	description?: String;
	contentUrl?: string;
	files?: ContentFile[];
	dmps?: UrlListingItem[];
}

export interface ContentFile {
	filename?: string;
	id?: string;
	location?: string;
	type?: string;
}
