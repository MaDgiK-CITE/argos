import { ExternalDatasetType } from '../../common/enum/external-dataset-type';

export interface ExternalDatasetModel {
	abbreviation: String;
	id: String;
	name: String;
	reference: String;
	type: ExternalDatasetType;
	info: String;
	source: String;
}
