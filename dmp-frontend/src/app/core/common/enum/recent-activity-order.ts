export enum RecentActivityOrder {
	LABEL = "label",
	CREATED = "created",
	MODIFIED = "modified",
	FINALIZED = "finalizedAt",
	PUBLISHED = "publishedAt",
	DATASETPUBLISHED = "dmp:publishedAt|join|",
	STATUS = "status"
}
