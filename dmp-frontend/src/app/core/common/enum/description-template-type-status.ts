export enum DescriptionTemplateTypeStatus {
	Draft = 0,
	Finalized = 1,
	Deleted = 99
}