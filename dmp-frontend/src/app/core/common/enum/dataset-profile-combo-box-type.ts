export enum DatasetProfileComboBoxType {
	Autocomplete = "autocomplete",
	WordList = "wordlist"
}