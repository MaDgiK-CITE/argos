export enum AuthProvider {
	Google = 1,
	Facebook = 2,
	Twitter = 3,
	LinkedIn = 4,
	//NativeLogin=5,
	B2Access = 6,
	ORCID = 7,
	OpenAire = 8,
	Configurable = 9,
	Zenodo = 10
}
