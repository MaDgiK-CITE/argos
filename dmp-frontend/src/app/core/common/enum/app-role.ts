export enum AppRole {
	Admin = 2,
	Manager = 1,
	User = 0,
	DatasetTemplateEditor = 3
}
