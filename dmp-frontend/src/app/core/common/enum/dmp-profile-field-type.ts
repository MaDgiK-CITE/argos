export enum DmpProfileFieldDataType {
	Date = 0,
	Number = 1,
	Text = 2,
	ExternalAutocomplete = 3
}
