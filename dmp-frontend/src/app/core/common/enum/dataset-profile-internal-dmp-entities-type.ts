export enum DatasetProfileInternalDmpEntitiesType {
	Researchers = "researchers",
	Dmps = "dmps",
	Datasets = "datasets"
}
