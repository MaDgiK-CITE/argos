export enum ConfigurableProviderType {
	Oauth2 = "oauth2",
	Saml2 = "saml2"
}