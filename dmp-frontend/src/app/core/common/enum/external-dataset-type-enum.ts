export enum ExternalDatasetTypeEnum{
    ReusedDataset = "reused_dataset",
    ProducedDataset = "produced_dataset",
    Other = "other"
}