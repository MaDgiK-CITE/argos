export enum DatasetStatus {
	Draft = 0,
	Finalized = 1,
	Canceled = 2,
	Deleted = 99
}
