export enum DmpProfileStatus {
	Draft = 0,
	Finalized = 1,
	Deleted = 99
}