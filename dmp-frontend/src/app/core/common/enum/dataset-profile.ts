export enum DatasetProfileEnum {
	SAVED = 0,
	FINALIZED = 1,
	DELETED = 99
}
