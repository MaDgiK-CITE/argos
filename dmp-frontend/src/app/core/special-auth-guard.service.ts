import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanLoad, Route, Router, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './services/auth/auth.service';
import { AppRole } from './common/enum/app-role';

@Injectable()
export class SpecialAuthGuard implements CanActivate, CanLoad {
	constructor(private auth: AuthService, private router: Router) {
	}

	hasPermission(permission: AppRole): boolean {
		if (!this.auth.current()) { return false; }
		const principalRoles = this.auth.current().authorities;
		for (let i = 0; i < principalRoles.length; i++) {
			if (principalRoles[i] === permission) {
				return true;
			}
		}
		return false;
	}

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
		const url: string = state.url;
		const permissions = route.data['authContext']['permissions'];
		let count = permissions.length;
		if (count < 0 || count === undefined) {
			return false;
		}
		for (let i = 0; i < permissions.length; i++) {
			if (!this.hasPermission(permissions[i])) {
				count--;
			}
		}
		if (count === 0) {
		this.router.navigate(['/unauthorized'], { queryParams: { returnUrl: url } });
		return false;
		} else {
			return true;
		}
	}

	canLoad(route: Route): boolean {
		const url = `/${route.path}`;
		const permissions = route.data['authContext']['permissions'];
		let count = permissions.length;
		if (count < 0 || count === undefined) {
			return false;
		}
		for (let i = 0; i < permissions.length; i++) {
			if (!this.hasPermission(permissions[i])) {
				count--;
			}
		}
		if (count === 0) {
		this.router.navigate(['/unauthorized'], { queryParams: { returnUrl: url } });
		return false;
		} else {
			return true;
		}
	}
}
