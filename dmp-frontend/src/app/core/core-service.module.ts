import { HttpClient } from '@angular/common/http';
import { APP_INITIALIZER, ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { AdminAuthGuard } from './admin-auth-guard.service';
import { AuthGuard } from './auth-guard.service';
import { AuthService } from './services/auth/auth.service';
import { ConfigurationService } from './services/configuration/configuration.service';
import { ContactSupportService } from './services/contact-support/contact-support.service';
import { CultureService } from './services/culture/culture-service';
import { LanguageInfoService } from './services/culture/language-info-service';
import { CurrencyService } from './services/currency/currency.service';
import { DashboardService } from './services/dashboard/dashboard.service';
import { DatasetProfileService } from './services/dataset-profile/dataset-profile.service';
import { DatasetWizardService } from './services/dataset-wizard/dataset-wizard.service';
import { DatasetExternalAutocompleteService } from './services/dataset/dataset-external-autocomplete.service';
import { DatasetService } from './services/dataset/dataset.service';
import { DmpInvitationService } from './services/dmp/dmp-invitation.service';
import { DmpProfileService } from './services/dmp/dmp-profile.service';
import { DmpService } from './services/dmp/dmp.service';
import { EmailConfirmationService } from './services/email-confirmation/email-confirmation.service';
import { ExternalDataRepositoryService } from './services/external-sources/data-repository/extternal-data-repository.service';
import { ExternalDatasetService } from './services/external-sources/dataset/external-dataset.service';
import { ExternalSourcesConfigurationService } from './services/external-sources/external-sources-configuration.service';
import { ExternalSourcesService } from './services/external-sources/external-sources.service';
import { ExternalRegistryService } from './services/external-sources/registry/external-registry.service';
import { ExternalResearcherService } from './services/external-sources/researcher/external-researcher.service';
import { ExternalServiceService } from './services/external-sources/service/external-service.service';
import { FunderService } from './services/funder/funder.service';
import { GrantFileUploadService } from './services/grant/grant-file-upload.service';
import { GrantService } from './services/grant/grant.service';
import { BaseHttpService } from './services/http/base-http.service';
import { LanguageService } from './services/language/language.service';
import { LockService } from './services/lock/lock.service';
import { LoggingService } from './services/logging/logging-service';
import { MergeEmailConfirmationService } from './services/merge-email-confirmation/merge-email-confirmation.service';
import { UiNotificationService } from './services/notification/ui-notification-service';
import { OrganisationService } from './services/organisation/organisation.service';
import { ProgressIndicationService } from './services/progress-indication/progress-indication-service';
import { ProjectService } from './services/project/project.service';
import { QuickWizardService } from './services/quick-wizard/quick-wizard.service';
import { SearchBarService } from './services/search-bar/search-bar.service';
import { TimezoneService } from './services/timezone/timezone-service';
import { UserGuideService } from './services/user-guide/user-guide.service';
import { UserService } from './services/user/user.service';
import { CollectionUtils } from './services/utilities/collection-utils.service';
import { TypeUtils } from './services/utilities/type-utils.service';
import { SpecialAuthGuard } from './special-auth-guard.service';
import {PrefillingService} from "@app/core/services/prefilling.service";
import { DepositRepositoriesService } from './services/deposit-repositories/deposit-repositories.service';
import { AboutService } from './services/about/about.service';
import { FaqService } from './services/faq/faq.service';
import { GlossaryService } from './services/glossary/glossary.service';
import { TermsOfServiceService } from './services/terms-of-service/terms-of-service.service';
import { UnlinkAccountEmailConfirmationService } from './services/unlink-account-email-confirmation/unlink-account-email-confirmation.service';
import { DescriptionTemplateTypeService } from './services/description-template-type/description-template-type.service';
//
//
// This is shared module that provides all the services. Its imported only once on the AppModule.
//
//

export function ConfigurationFactory(appConfig: ConfigurationService) {
	return () => appConfig.loadConfiguration();
}

@NgModule({
})
export class CoreServiceModule {
	constructor(@Optional() @SkipSelf() parentModule: CoreServiceModule) {
		if (parentModule) {
			throw new Error(
				'CoreModule is already loaded. Import it in the AppModule only');
		}
	}
	static forRoot(): ModuleWithProviders<CoreServiceModule> {
		return {
			ngModule: CoreServiceModule,
			providers: [
				AuthService,
				CookieService,
				BaseHttpService,
				AdminAuthGuard,
				SpecialAuthGuard,
				AuthGuard,
				CultureService,
				TimezoneService,
				TypeUtils,
				CollectionUtils,
				UiNotificationService,
				ProgressIndicationService,
				LoggingService,
				SearchBarService,
				DashboardService,
				GrantService,
				ProjectService,
				FunderService,
				GrantFileUploadService,
				DmpService,
				DepositRepositoriesService,
				DmpProfileService,
				ExternalSourcesService,
				ExternalSourcesConfigurationService,
				DatasetService,
				DatasetWizardService,
				ExternalDatasetService,
				ExternalDataRepositoryService,
				ExternalRegistryService,
				ExternalResearcherService,
				ExternalServiceService,
				DatasetProfileService,
				UserService,
				DmpInvitationService,
				DatasetExternalAutocompleteService,
				QuickWizardService,
				OrganisationService,
				EmailConfirmationService,
				ContactSupportService,
				LanguageService,
				LockService,
				UserGuideService,
				AboutService,
				FaqService,
				GlossaryService,
				TermsOfServiceService,
				CurrencyService,
				MergeEmailConfirmationService,
				UnlinkAccountEmailConfirmationService,
				ConfigurationService,
				{
					provide: APP_INITIALIZER,
					useFactory: ConfigurationFactory,
					deps: [ConfigurationService, HttpClient],
					multi: true
				},
				LanguageInfoService,
				PrefillingService,
				DescriptionTemplateTypeService
			],
		};
	}
}
