import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanLoad, Route, Router, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './services/auth/auth.service';
import { AppRole } from './common/enum/app-role';

@Injectable()
export class AdminAuthGuard implements CanActivate, CanLoad {
	constructor(private auth: AuthService, private router: Router) {
	}

	isAdmin(): boolean {
		if (!this.auth.current()) { return false; }
		const principalRoles = this.auth.current().authorities;
		for (let i = 0; i < principalRoles.length; i++) {
			if (principalRoles[i] === AppRole.Admin) {
				return true;
			}
		}
		return false;
	}

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
		const url: string = state.url;
		if (!this.isAdmin()) {
			this.router.navigate(['/unauthorized'], { queryParams: { returnUrl: url } });
			return false;
		}
		return true;
	}

	canLoad(route: Route): boolean {
		const url = `/${route.path}`;
		if (!this.isAdmin()) {
			this.router.navigate(['/unauthorized'], { queryParams: { returnUrl: url } });
			return false;
		}
		return true;
	}
}
