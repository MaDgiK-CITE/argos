import { BaseCriteria } from "../base-criteria";

export class ServiceCriteria extends BaseCriteria {
	public type: string;
}
