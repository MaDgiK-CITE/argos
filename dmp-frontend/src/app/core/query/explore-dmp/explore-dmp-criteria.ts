import { BaseCriteria } from "../base-criteria";
import { GrantStateType } from "../../common/enum/grant-state-type";
import { Role } from '../../common/enum/role';

export class ExploreDmpCriteriaModel extends BaseCriteria {
	public grantStatus: GrantStateType;
	public role: Role;
	public grants: string[] = [];
	public datasetProfile: string[] = [];
	public dmpOrganisations: string[] = [];
	public allVersions: boolean;
	public groupIds: string[] = [];
}
