import { BaseCriteria } from "../base-criteria";

export class DataRepositoryCriteria extends BaseCriteria {
	public type: string;
}
