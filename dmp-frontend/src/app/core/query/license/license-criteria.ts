import { BaseCriteria } from "../base-criteria";

export class LicenseCriteria extends BaseCriteria {
	public type: string;
}
