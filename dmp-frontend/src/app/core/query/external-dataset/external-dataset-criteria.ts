import { BaseCriteria } from "../base-criteria";

export class ExternalDatasetCriteria extends BaseCriteria {
	public type: string;
}
