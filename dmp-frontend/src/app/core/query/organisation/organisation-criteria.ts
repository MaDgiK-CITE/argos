import { BaseCriteria } from "../base-criteria";

export class OrganisationCriteria extends BaseCriteria {
	public labelLike: string;
	public like?: string;
}
