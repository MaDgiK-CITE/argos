import { BaseCriteria } from "../base-criteria";

export class TagCriteria extends BaseCriteria {
	public type: string;
}
