import { BaseCriteria } from "../base-criteria";

export class RegistryCriteria extends BaseCriteria {
	public type: string;
}
