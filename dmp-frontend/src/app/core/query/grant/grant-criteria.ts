import { GrantStateType } from "../../common/enum/grant-state-type";
import { BaseCriteria } from "../base-criteria";

export class GrantCriteria extends BaseCriteria {
	public periodStart: Date;
	public periodEnd: Date;
	public funderReference: String;
	public grantStateType: GrantStateType;
	public exactReference: string;
}
