import { BaseCriteria } from "../base-criteria";

export class DatasetProfileCriteria extends BaseCriteria {
	public id: String;
	public groupIds: string[];
	public ids: string[];
	public allVersions: boolean;
	public finalized: boolean = true;
	public status: number;
}
