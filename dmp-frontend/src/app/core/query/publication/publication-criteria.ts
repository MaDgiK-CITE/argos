import { BaseCriteria } from "../base-criteria";
export class PublicationCriteria extends BaseCriteria {
	public type: string;
}
