import { GrantStateType } from "../../common/enum/grant-state-type";
import { BaseCriteria } from "../base-criteria";
import { Role } from '../../common/enum/role';

export class ExploreDatasetCriteriaModel extends BaseCriteria {
	public grantStatus: GrantStateType;
	public role: Role;
	public dmpIds: string[] = [];
	public grants: string[] = [];
	public datasetProfile: string[] = [];
	public dmpOrganisations: string[] = [];
	public tags = [];
}
