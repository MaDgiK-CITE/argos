import { RecentActivityOrder } from '@app/core/common/enum/recent-activity-order';
import { BaseCriteria } from '../base-criteria';

export class RecentActivityCriteria extends BaseCriteria{
	public status?: Number;
	public order: RecentActivityOrder = RecentActivityOrder.MODIFIED;
}
