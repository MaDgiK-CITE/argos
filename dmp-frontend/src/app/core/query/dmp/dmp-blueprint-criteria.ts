import { BaseCriteria } from "../base-criteria";

export class DmpBlueprintCriteria extends BaseCriteria {
    public status?: number;
}