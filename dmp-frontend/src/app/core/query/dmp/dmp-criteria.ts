import { GrantListingModel } from "../../model/grant/grant-listing";
import { BaseCriteria } from "../base-criteria";
import { OrganizationModel } from "../../model/organisation/organization";

export class DmpCriteria extends BaseCriteria {
	public organisations?: string[] = [];
	public grants?: GrantListingModel[] = [];
	public groupIds?: string[];
	public allVersions?: boolean;
	public status?: number;
	public role?: number;
	public collaborators?: string[] = [];
	public datasetTemplates?: string[] = [];
	public isPublic?: boolean;
	public onlyPublic?: boolean;
	public grantStatus?: boolean;
}
