import { BaseCriteria } from "../base-criteria";

export class DmpProfileExternalAutocompleteCriteria extends BaseCriteria {
	public profileID: String;
	public fieldID: String;
}
