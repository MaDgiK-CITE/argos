import { AutoCompleteSingleData } from "@app/core/model/dataset-profile-definition/field-data/field-data";
import { BaseCriteria } from "../base-criteria";

export class DatasetExternalAutocompleteCriteria extends BaseCriteria {
	public profileID: String;
	public fieldID: String;
}

export class DatasetExternalAutocompleteOptionsCriteria extends BaseCriteria {
	public autoCompleteSingleDataList: AutoCompleteSingleData[];
}