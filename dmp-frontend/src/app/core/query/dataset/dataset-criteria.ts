import { ExternalSourceItemModel } from "../../model/external-sources/external-source-item";
import { BaseCriteria } from "../base-criteria";

export class DatasetCriteria extends BaseCriteria {
	public grants?: string[] = [];
	public status?: Number;
	public dmpIds?: string[] = [];
	public tags?: ExternalSourceItemModel[] = [];
	public allVersions?: boolean = false;
	public role?: number;
	public organisations?: string[] = [];
	public collaborators?: string[] = [];
	public datasetTemplates?: string[] = [];
	public groupIds?: string[] = [];
	public isPublic?: boolean = false;
	public grantStatus?: number;
}
