﻿import { AppRole } from "../../common/enum/app-role";
import { BaseCriteria } from "../base-criteria";

export class UserCriteria extends BaseCriteria {
	public label: String;
	public appRoles: AppRole[];
	public collaboratorLike: string;
}
