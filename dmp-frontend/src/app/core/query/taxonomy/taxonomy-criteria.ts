import { BaseCriteria } from "../base-criteria";

export class TaxonomyCriteria extends BaseCriteria {
	public type: string;
}
