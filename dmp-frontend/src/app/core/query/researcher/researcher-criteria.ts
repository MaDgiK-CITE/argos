import { BaseCriteria } from "../base-criteria";

export class ResearcherCriteria extends BaseCriteria {
	public name: String;
	public reference?: string;
}
