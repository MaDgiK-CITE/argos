import { Pipe, PipeTransform } from "@angular/core";
import { FormGroup } from "@angular/forms";

@Pipe({
	name: 'datasetInSection'
})
export class DatasetInSectioPipe implements PipeTransform{

	transform(datasets: FormGroup[], args: string): FormGroup[] {
		let values = [];
        for(var dataset of datasets){
            if(dataset.get('dmpSectionIndex').value == args){
                values.push(dataset);
            }
        }
        return values;
	}
}