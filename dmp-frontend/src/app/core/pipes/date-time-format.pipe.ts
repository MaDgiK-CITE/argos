import { DatePipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
import 'moment-timezone';
import { TimezoneService } from '../services/timezone/timezone-service';

@Pipe({
	name: 'dateTimeFormatter'
})
export class DateTimeFormatPipe implements PipeTransform {

	constructor(private datePipe: DatePipe, private timezoneService: TimezoneService) {

	}

	transform(value: any, format?: string, timezone?: string, locale?: string): string | null {
		// using timezone set in timezoneService by default. can be overwritten with pipe arguments
		const timezoneToUse = timezone ? timezone : moment(value).tz(this.timezoneService.getCurrentTimezone()).format('Z');
		return this.datePipe.transform(value, format, timezoneToUse, locale);
	}
}
