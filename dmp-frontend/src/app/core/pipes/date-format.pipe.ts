import { DatePipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
import 'moment-timezone';

@Pipe({
	name: 'dateFormatter'
})
export class DateFormatPipe implements PipeTransform {

	constructor(private datePipe: DatePipe) {

	}

	transform(value: any, format?: string, locale?: string): string | null {
		// using timezone set in timezoneService by default. can be overwritten with pipe arguments
		const timezoneToUse = moment(value).tz('UTC').format('Z');
		return this.datePipe.transform(value, format, timezoneToUse, locale);
	}
}
