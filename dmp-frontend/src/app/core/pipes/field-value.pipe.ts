import {Pipe, PipeTransform} from "@angular/core";
import {DatePipe} from "@angular/common";
import {DatasetProfileFieldViewStyle} from "@app/core/common/enum/dataset-profile-field-view-style";

@Pipe({
	name: 'fieldValue'
})
export class FieldValuePipe implements PipeTransform {

	constructor(private date: DatePipe) {
	}

	transform(controlValue: any): string | null {
		let value = controlValue.value;
		let renderStyle = controlValue.viewStyle?.renderStyle;
		if (renderStyle && value) {
			switch (renderStyle) {
				case DatasetProfileFieldViewStyle.Currency:
					if (value) {
						return JSON.parse(value).name;
					}
					break;
				case DatasetProfileFieldViewStyle.BooleanDecision:
					return value == 'true' ? 'DATASET-PROFILE-EDITOR.STEPS.FORM.FIELD.DEFAULT-VALUES.BOOLEAN-DECISION.YES' : 'DATASET-PROFILE-EDITOR.STEPS.FORM.FIELD.DEFAULT-VALUES.BOOLEAN-DECISION.NO';
				case DatasetProfileFieldViewStyle.CheckBox:
					return value ? 'DATASET-PROFILE-EDITOR.STEPS.FORM.FIELD.DEFAULT-VALUES.BOOLEAN-DECISION.YES' : 'DATASET-PROFILE-EDITOR.STEPS.FORM.FIELD.DEFAULT-VALUES.BOOLEAN-DECISION.NO';
				case DatasetProfileFieldViewStyle.RadioBox:
					if (value && controlValue.data.options) {
						return controlValue.data.options.find(option => option.value === value).label;
					}
					break;
				case DatasetProfileFieldViewStyle.DatePicker:
					return this.date.transform(controlValue.value, 'dd/MM/yyyy');
				case DatasetProfileFieldViewStyle.FreeText:
					return value;
				case DatasetProfileFieldViewStyle.ComboBox:
					if (value && controlValue.data.options && !controlValue.data.multiList) {
						return controlValue.data.options.find(option => value == option.value).label;
					} else if (value && controlValue.data.options && controlValue.data.multiList) {
						return controlValue.data.options.filter(option => value.includes(option.value)).map(option => option.label).join(',');
					}
					break;
				case DatasetProfileFieldViewStyle.RichTextArea:
					if(value) {
						return value.replace(/&nbsp;/g, ' ').replace(/(\r\n|\n|\r| +(?= ))|\s\s+/gm, " ").replace(/<[^>]*>/g, '');
					}
					break;
				case DatasetProfileFieldViewStyle.TextArea:
					return value;
				case DatasetProfileFieldViewStyle.Registries:
				case DatasetProfileFieldViewStyle.Services:
				case DatasetProfileFieldViewStyle.Researchers:
				case DatasetProfileFieldViewStyle.Organizations:
				case DatasetProfileFieldViewStyle.ExternalDatasets:
				case DatasetProfileFieldViewStyle.DataRepositories:
				case DatasetProfileFieldViewStyle.PubRepositories:
				case DatasetProfileFieldViewStyle.JournalRepositories:
				case DatasetProfileFieldViewStyle.Taxonomies:
				case DatasetProfileFieldViewStyle.Licenses:
				case DatasetProfileFieldViewStyle.Publications:
				case DatasetProfileFieldViewStyle.Tags:
					return this.parseJson(value);
				case DatasetProfileFieldViewStyle.InternalDmpEntities:
					return this.parseJson(value, 'label');
				case DatasetProfileFieldViewStyle.DatasetIdentifier:
				case DatasetProfileFieldViewStyle.Validation:
					if(value && value.identifier) {
						return value.identifier;
					}
					break;
				default:
					return null;
			}
		}
		return null;
	}

	public parseJson(value: any, field: string = 'name') {
		if(Array.isArray(value)) {
			return value.map(element => JSON.parse(element)[field]).join(',');
		} else {
			return JSON.parse(value)[field];
		}
	}
}
