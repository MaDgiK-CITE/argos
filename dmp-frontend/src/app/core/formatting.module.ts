import { DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { DateFormatPipe } from './pipes/date-format.pipe';
import { DateTimeFormatPipe } from './pipes/date-time-format.pipe';
import { NgForLimitPipe } from './pipes/ng-for-limit.pipe';
import { TimezoneInfoDisplayPipe } from './pipes/timezone-info-display.pipe';
import { EnumUtils } from './services/utilities/enum-utils.service';
import { JsonParserPipe } from './pipes/json-parser.pipe';
import { DateTimeCultureFormatPipe } from './pipes/date-time-culture-format.pipe';
import {FieldValuePipe} from "@app/core/pipes/field-value.pipe";
import {ColumnClassPipe} from "@app/core/pipes/column-class.pipe";
import { DatasetInSectioPipe } from './pipes/dataset-in-section.pipe';

//
//
// This is shared module that provides all formatting utils. Its imported only once on the AppModule.
//
//

@NgModule({
	declarations: [
		NgForLimitPipe,
		TimezoneInfoDisplayPipe,
		DateFormatPipe,
		DateTimeFormatPipe,
		DateTimeCultureFormatPipe,
		JsonParserPipe,
		FieldValuePipe,
		ColumnClassPipe,
		DatasetInSectioPipe
	],
	exports: [
		NgForLimitPipe,
		TimezoneInfoDisplayPipe,
		DateFormatPipe,
		DateTimeFormatPipe,
		DateTimeCultureFormatPipe,
		JsonParserPipe,
		FieldValuePipe,
		ColumnClassPipe,
		DatasetInSectioPipe
	],
	providers: [
		EnumUtils,
		DatePipe,
		NgForLimitPipe,
		TimezoneInfoDisplayPipe,
		DateFormatPipe,
		DateTimeFormatPipe,
		DateTimeCultureFormatPipe,
		JsonParserPipe,
		FieldValuePipe,
		ColumnClassPipe,
		DatasetInSectioPipe
	]
})
export class FormattingModule { }
