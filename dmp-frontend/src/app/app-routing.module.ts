import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReloadHelperComponent } from '@app/ui/misc/reload-helper/reload-helper.component';
import { B2AccessLoginComponent } from './ui/auth/login/b2access/b2access-login.component';
import { Oauth2DialogModule } from './ui/misc/oauth2-dialog/oauth2-dialog.module';
import { Oauth2DialogComponent } from './ui/misc/oauth2-dialog/oauth2-dialog.component';
import { AppComponent } from './app.component';

const appRoutes: Routes = [
	{
		path: '',
		component: AppComponent,
		data: {
			breadcrumbs: false,
			title: 'GENERAL.TITLES.GENERAL'
		},
		pathMatch: 'full'
	},
	{
		path: 'datasetcreatewizard',
		loadChildren: () => import('./ui/dataset-create-wizard/dataset-create-wizard.module').then(m => m.DatasetCreateWizardModule),
		data: {
			breadcrumb: true,
			title: 'GENERAL.TITLES.DATASETCREATEWIZARD'
		}
	},
	{
		path: 'explore',
		// loadChildren: () => import('./ui/explore-dataset/explore-dataset.module').then(m => m.ExploreDatasetModule),
		loadChildren: () => import('./ui/dataset/dataset.module').then(m => m.DatasetModule),
		data: {
			breadcrumb: true,
			title: 'GENERAL.TITLES.EXPLORE'
		}
	},
	{
		path: 'explore-plans',
		// loadChildren: () => import('./ui/explore-dmp/explore-dmp.module').then(m => m.ExploreDmpModule),
		loadChildren: () => import('./ui/dmp/dmp.module').then(m => m.DmpModule),
		data: {
			breadcrumb: true,
			title: 'GENERAL.TITLES.EXPLORE-PLANS'
		}
	},
	{
		path: 'datasets',
		loadChildren: () => import('./ui/dataset/dataset.module').then(m => m.DatasetModule),
		data: {
			breadcrumb: true,
			title: 'GENERAL.TITLES.DESCRIPTIONS'
		}
	},
	{
		path: 'about',
		loadChildren: () => import('./ui/about/about.module').then(m => m.AboutModule),
		data: {
			breadcrumb: true,
			title: 'GENERAL.TITLES.ABOUT'
		}
	},
	// ----------- UNCOMMENT TO ADD AGAIN GRANTS --------
	// {
	// 	path: 'grants',
	// 	loadChildren: () => import('./ui/grant/grant.module').then(m => m.GrantModule),
	// 	data: {
	// 		breadcrumb: true,
	// 		title: 'GENERAL.TITLES.GRANTS'
	// 	}
	// },
	{
		path: 'plans',
		loadChildren: () => import('./ui/dmp/dmp.module').then(m => m.DmpModule),
		data: {
			breadcrumb: true,
			title: 'GENERAL.TITLES.PLANS'
		}
	},
	{
		path: 'dmp-profiles',
		loadChildren: () => import('./ui/admin/dmp-profile/dmp-profile.module').then(m => m.DmpProfileModule),
		data: {
			breadcrumb: true,
			title: 'GENERAL.TITLES.DMP-BLUEPRINTS'
		}
	},
	{
		path: 'quick-wizard',
		loadChildren: () => import('./ui/quick-wizard/quick-wizard.module').then(m => m.OuickWizardModule),
		data: {
			breadcrumb: true,
			title: "GENERAL.TITLES.QUICK-WIZARD"
		}
	},
	{
		path: 'dataset-profiles',
		loadChildren: () => import('./ui/admin/dataset-profile/dataset-profile.module').then(m => m.DatasetProfileModule),
		data: {
			breadcrumb: true,
			title: 'GENERAL.TITLES.DATASET-PROFILES'
		}
	},
	{
		path: 'description-types',
		loadChildren: () => import('./ui/admin/description-types/description-types.module').then(m => m.DescriptionTypesModule),
		data: {
			breadcrumb: true,
			title: 'GENERAL.TITLES.DESCRIPTION-TYPES'
		}
	},
	{
		path: 'contact-support',
		loadChildren: () => import('./ui/contact/contact.module').then(m => m.ContactModule),
		data: {
			breadcrumb: true,
			title: 'CONTACT.SUPPORT.TITLE'
		}
	},
	{
		path: 'glossary',
		loadChildren: () => import('./ui/glossary/glossary.module').then(m => m.GlossaryModule),
		data: {
			breadcrumb: true,
			title: 'GLOSSARY.TITLE'
		}
	},
	{
		path: 'faq',
		loadChildren: () => import('./ui/faq/faq.module').then(m => m.FaqModule),
		data: {
			breadcrumb: true,
			title: 'FAQ.TITLE'
		}
	},
	{
		path: 'user-guide',
		loadChildren: () => import('./ui/user-guide/user-guide.module').then(m => m.UserGuideModule),
		data: {
			breadcrumb: true,
			title: 'GUIDE.TITLE'
		}
	},
	{
		path: 'privacy-policy',
		loadChildren: () => import('./ui/sidebar/sidebar-footer/privacy/privacy.module').then(m => m.PrivacyModule),
		data: {
			breadcrumb: true,
			title: 'GENERAL.TITLES.PRIVACY'
		}
	},
	{
		path: 'opensource-licences',
		loadChildren: () => import('./ui/sidebar/sidebar-footer/opensource-licences/opensource-licenses.module').then(m => m.OpensourceLicencesModule),
		data: {
			breadcrumb: true,
			title: 'GENERAL.TITLES.OPENSOURCE-LICENCES'
		}
	},
	{
		path: 'terms-and-conditions',
		loadChildren: () => import('./ui/sidebar/sidebar-footer/terms/terms.module').then(m => m.TermsModule),
		data: {
			breadcrumb: true,
			title: 'GENERAL.TITLES.TERMS'
		}
	},
	{
		path: 'cookies-policy',
		loadChildren: () => import('./ui/sidebar/sidebar-footer/cookies-policy/cookies-policy.module').then(m => m.CookiesPolicyModule),
		data: {
			breadcrumb: true,
			title: 'GENERAL.TITLES.COOKIES-POLICY'
		}
	},
	{
		path: 'home',
		loadChildren: () => import('./ui/dashboard/dashboard.module').then(m => m.DashboardModule),
		data: {
			breadcrumb: true
		}
	},
	// {
	// 	path: 'splash',
	// 	loadChildren: () => import('./ui/splash/splash.module').then(m => m.SplashModule),
	// 	data: {
	// 		breadcrumb: true
	// 	}
	// },
	{
		path: 'unauthorized',
		loadChildren: () => import('./ui/misc/unauthorized/unauthorized.module').then(m => m.UnauthorizedModule),
		data: {
			breadcrumb: true
		},
	},
	{
		path: 'users',
		loadChildren: () => import('./ui/admin/user/user.module').then(m => m.UserModule),
		data: {
			breadcrumb: true,
			title: 'GENERAL.TITLES.USERS'
		},
	},
	{
		path: 'profile',
		loadChildren: () => import('./ui/user-profile/user-profile.module').then(m => m.UserProfileModule),
		data: {
			breadcrumb: true,
			title: 'GENERAL.TITLES.PROFILE'
		},
	},
	{
		path: 'language-editor',
		loadChildren: () => import('./ui/language-editor/language-editor.module').then(m => m.LanguageEditorModule),
		data: {
			breadcrumb: true,
			title: 'GENERAL.TITLES.LANGUAGE-EDITOR'
		},
	},
	{
		path: 'user-guide-editor',
		loadChildren: () => import('./ui/user-guide-editor/user-guide-editor.module').then(m => m.UserGuideEditorModule),
		data: {
			breadcrumb: true,
			title: 'GENERAL.TITLES.GUIDE-EDITOR'
		},
	},
	{
		path: 'index-managment',
		loadChildren: () => import('./ui/admin/index-managment/index-managment.module').then(m => m.IndexManagmentModule),
		data: {
			breadcrumb: true,
			title: 'GENERAL.TITLES.INDEX-MANAGMENT'
		},
	},
	{
		path: 'maintenance-tasks',
		loadChildren: () => import('./ui/admin/maintenance-tasks/maintenance-tasks.module').then(m => m.MaintenanceTasksModule),
		data: {
			breadcrumb: true
		},
	},
	{
		path: 'login/admin',
		loadChildren: () => import('./ui/auth/admin-login/admin-login.module').then(m => m.AdminLoginModule),
		data: {
			breadcrumb: true
		},
	},
	{
		path: 'login',
		loadChildren: () => import('./ui/auth/login/login.module').then(m => m.LoginModule),
		data: {
			breadcrumb: true,
			title: 'GENERAL.TITLES.LOGIN'
		},
	},
	{
		path: 'api/oauth/authorized/b2access',
		component: B2AccessLoginComponent,
		data: {
		},
	},
	{ path: 'reload', component: ReloadHelperComponent },
	{ path: 'oauth2', component: Oauth2DialogComponent },
	{
		path: 'external',
		loadChildren: () => import('./ui/external/external.module').then(m => m.ExternalModule),
		data: {
			breadcrumb: true,

		},
	}
];

@NgModule({
	imports: [RouterModule.forRoot(appRoutes, { relativeLinkResolution: 'legacy' })],
	exports: [RouterModule],
})
export class AppRoutingModule { }

