import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DatasetListingModel } from '../../../../core/model/dataset/dataset-listing';
import { Router } from '@angular/router';
import { DatasetStatus } from '../../../../core/common/enum/dataset-status';
import { EnumUtils } from '@app/core/services/utilities/enum-utils.service';
import { DatasetWizardService } from '@app/core/services/dataset-wizard/dataset-wizard.service';
import { BaseComponent } from '@common/base/base.component';
import { takeUntil } from 'rxjs/operators';
import * as FileSaver from 'file-saver';
import { DmpInvitationDialogComponent } from '@app/ui/dmp/invitation/dmp-invitation-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { DatasetCopyDialogueComponent } from '../../dataset-wizard/dataset-copy-dialogue/dataset-copy-dialogue.component';
import { FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ConfirmationDialogComponent } from '@common/modules/confirmation-dialog/confirmation-dialog.component';
import { UiNotificationService, SnackBarNotificationLevel } from '@app/core/services/notification/ui-notification-service';
import { ValidationErrorModel } from '@common/forms/validation/error-model/validation-error-model';
import { Principal } from '@app/core/model/auth/principal';
import { AuthService } from '@app/core/services/auth/auth.service';
import { LockService } from '@app/core/services/lock/lock.service';
import { Role } from '@app/core/common/enum/role';
import { Location } from '@angular/common';
import { MatomoService } from '@app/core/services/matomo/matomo-service';
import { HttpClient } from '@angular/common/http';

@Component({
	selector: 'app-dataset-listing-item-component',
	templateUrl: './dataset-listing-item.component.html',
	styleUrls: ['./dataset-listing-item.component.scss']
})
export class DatasetListingItemComponent extends BaseComponent implements OnInit {

	@Input() dataset: DatasetListingModel;
	@Input() showDivider: boolean = true;
	@Input() isPublic: boolean = false;
	@Output() onClick: EventEmitter<DatasetListingModel> = new EventEmitter();

	isDraft: boolean;
	isDeleted: boolean;
	isUserOwner: boolean;

	constructor(
		private router: Router,
		public enumUtils: EnumUtils,
		private datasetWizardService: DatasetWizardService,
		public dialog: MatDialog,
		private language: TranslateService,
		private authentication: AuthService,
		private uiNotificationService: UiNotificationService,
		private lockService: LockService,
		private location: Location,
		private httpClient: HttpClient,
		private matomoService: MatomoService
	) {
		super();
	}

	ngOnInit() {
		this.matomoService.trackPageView('Dataset Listing Item');
		if (this.dataset.status === DatasetStatus.Draft) {
			this.isDraft = true;
			this.isDeleted = false;
			this.setIsUserOwner();
		} else if (this.dataset.status === DatasetStatus.Deleted) {
			this.isDeleted = true;
		}
		else {
			this.isDraft = false;
			this.isDeleted = false;
			this.setIsUserOwner();
		}
	}

	setIsUserOwner() {
		if (this.dataset) {
			const principal: Principal = this.authentication.current();
			if (principal) this.isUserOwner = !!this.dataset.users.find(x => (x.role === Role.Owner) && (principal.id === x.id));
		}
	}

	public isAuthenticated(): boolean {
		return !(!this.authentication.current());
	}

	getItemLink(): string[] {
		// return this.isPublic ? [`/datasets/publicEdit/${this.dataset.id}`] : [`/datasets/edit/${this.dataset.id}`];
		return this.isPublic ? ['/datasets/publicOverview/' + this.dataset.id] : ['/datasets/overview/' + this.dataset.id];
	}

	getDmpLink(): string[] {
		return this.isPublic ? [`/explore-plans/publicOverview/${this.dataset.dmpId}`] : [`/plans/edit/${this.dataset.dmpId}`];
	}

	downloadPDF(dataset: DatasetListingModel): void {
		this.datasetWizardService.downloadPDF(dataset.id as string)
			.pipe(takeUntil(this._destroyed))
			.subscribe(response => {
				const blob = new Blob([response.body], { type: 'application/pdf' });
				const filename = this.getFilenameFromContentDispositionHeader(response.headers.get('Content-Disposition'));

				FileSaver.saveAs(blob, filename);
				this.matomoService.trackDownload('datasets', "pdf", dataset.id);
			});
	}

	downloadDOCX(dataset: DatasetListingModel): void {
		this.datasetWizardService.downloadDOCX(dataset.id as string)
			.pipe(takeUntil(this._destroyed))
			.subscribe(response => {
				const blob = new Blob([response.body], { type: 'application/msword' });
				const filename = this.getFilenameFromContentDispositionHeader(response.headers.get('Content-Disposition'));

				FileSaver.saveAs(blob, filename);
				this.matomoService.trackDownload('datasets', "docx", dataset.id);
			});

	}

	downloadXML(dataset: DatasetListingModel): void {
		this.datasetWizardService.downloadXML(dataset.id as string)
			.pipe(takeUntil(this._destroyed))
			.subscribe(response => {
				const blob = new Blob([response.body], { type: 'application/xml' });
				const filename = this.getFilenameFromContentDispositionHeader(response.headers.get('Content-Disposition'));

				FileSaver.saveAs(blob, filename);
				this.matomoService.trackDownload('datasets', "xml", dataset.id);
			});
	}

	getFilenameFromContentDispositionHeader(header: string): string {
		const regex: RegExp = new RegExp(/filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/g);

		const matches = header.match(regex);
		let filename: string;
		for (let i = 0; i < matches.length; i++) {
			const match = matches[i];
			if (match.includes('filename="')) {
				filename = match.substring(10, match.length - 1);
				break;
			} else if (match.includes('filename=')) {
				filename = match.substring(9);
				break;
			}
		}
		return filename;
	}

	openShareDialog(dmpRowId: any, dmpRowName: any) {
		const dialogRef = this.dialog.open(DmpInvitationDialogComponent, {
			// height: '250px',
			// width: '700px',
			autoFocus: false,
			restoreFocus: false,
			data: {
				dmpId: dmpRowId,
				dmpName: dmpRowName
			}
		});
	}

	openDmpSearchDialogue(dataset: DatasetListingModel) {
		const formControl = new FormControl();
		const dialogRef = this.dialog.open(DatasetCopyDialogueComponent, {
			width: '500px',
			restoreFocus: false,
			data: {
				formControl: formControl,
				datasetId: dataset.id,
				datasetProfileId: dataset.profile.id,
				datasetProfileExist: false,
				confirmButton: this.language.instant('DATASET-WIZARD.DIALOGUE.COPY'),
				cancelButton: this.language.instant('DATASET-WIZARD.DIALOGUE.CANCEL')
			}
		});

		dialogRef.afterClosed().pipe(takeUntil(this._destroyed))
			.subscribe(result => {
				if (result && result.datasetProfileExist) {
					const newDmpId = result.formControl.value.id;
					this.router.navigate(['/datasets/copy/' + result.datasetId], { queryParams: { newDmpId: newDmpId } });
					// let url = this.router.createUrlTree(['/datasets/copy/', result.datasetId, { newDmpId: newDmpId }]);
					// window.open(url.toString(), '_blank');
				}
			});
	}

	deleteClicked(id: string) {
		this.lockService.checkLockStatus(id).pipe(takeUntil(this._destroyed))
			.subscribe(lockStatus => {
				if (!lockStatus) {
					this.openDeleteDialog(id);
				} else {
					this.openLockedByUserDialog();
				}
			});
	}

	openDeleteDialog(id: string): void {
		const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
			maxWidth: '300px',
			restoreFocus: false,
			data: {
				message: this.language.instant('GENERAL.CONFIRMATION-DIALOG.DELETE-ITEM'),
				confirmButton: this.language.instant('GENERAL.CONFIRMATION-DIALOG.ACTIONS.DELETE'),
				cancelButton: this.language.instant('GENERAL.CONFIRMATION-DIALOG.ACTIONS.CANCEL'),
				isDeleteConfirmation: true
			}
		});
		dialogRef.afterClosed().pipe(takeUntil(this._destroyed)).subscribe(result => {
			if (result) {
				this.datasetWizardService.delete(id)
					.pipe(takeUntil(this._destroyed))
					.subscribe(
						complete => this.onDeleteCallbackSuccess(),
						error => this.onDeleteCallbackError(error)
					);
			}
		});
	}

	openLockedByUserDialog() {
		const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
			maxWidth: '400px',
			restoreFocus: false,
			data: {
				message: this.language.instant('DATASET-WIZARD.ACTIONS.LOCK')
			}
		});
	}

	reloadPage(): void {
		const path = this.location.path();
		this.router.navigateByUrl('/reload', { skipLocationChange: true }).then(() => {
			this.router.navigate([path]);
		});
	}

	onDeleteCallbackSuccess(): void {
		this.uiNotificationService.snackBarNotification(this.language.instant('GENERAL.SNACK-BAR.SUCCESSFUL-DELETE'), SnackBarNotificationLevel.Success);
		this.reloadPage();
	}

	onDeleteCallbackError(error) {
		this.uiNotificationService.snackBarNotification(error.error.message ? error.error.message : this.language.instant('GENERAL.SNACK-BAR.UNSUCCESSFUL-DELETE'), SnackBarNotificationLevel.Error);
	}

	onCallbackSuccess(id?: String): void {
		this.uiNotificationService.snackBarNotification(this.language.instant('GENERAL.SNACK-BAR.SUCCESSFUL-UPDATE'), SnackBarNotificationLevel.Success);
		id ? this.router.navigate(['/reload']).then(() => { this.router.navigate(['/datasets', 'edit', id]); }) : this.router.navigate(['/datasets']);
	}

	roleDisplay(value: any) {
		const principal: Principal = this.authentication.current();
		let role: number;
		if (principal) {
			value.forEach(element => {
				if (principal.id === element.id) {
					role = element.role;
				}
			});
		}
		if (role === 0) {
			return this.language.instant('DMP-LISTING.OWNER');
		}
		else if (role === 1) {
			return this.language.instant('DMP-LISTING.MEMBER');
		}
		else {
			return this.language.instant('DMP-LISTING.OWNER');
		}
	}

	isUserDatasetRelated() {
		const principal: Principal = this.authentication.current();
		let isRelated: boolean = false;
		if (this.dataset && principal) {
			this.dataset.users.forEach(element => {
				if (element.id === principal.id) {
					isRelated = true;
				}
			})
		}
		return isRelated;
	}



	// onCallbackError(error: any) {
	// 	this.setErrorModel(error.error);
	// }

	// public setErrorModel(validationErrorModel: ValidationErrorModel) {
	// 	Object.keys(validationErrorModel).forEach(item => {
	// 		(<any>this.dataset.validationErrorModel)[item] = (<any>validationErrorModel)[item];
	// 	});
	// }

	// grantClicked(dataset: DatasetListingModel) {
	// 	this.router.navigate(['/grants/edit/' + dataset.grantId]);
	// }

	// itemClicked() {
	// 	this.onClick.emit(this.dataset);
	// }

	// datasetClicked(dataset: DatasetListingModel) {
	// 	this.router.navigate(['/plans/edit/' + dataset.dmpId])
	// }

}
