import { Inject, Component, ViewChild, OnInit, Output, EventEmitter } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormGroup } from '@angular/forms';
import { DatasetCriteriaComponent } from '../dataset-criteria.component';
import { DatasetCriteria } from '@app/core/query/dataset/dataset-criteria';
import { MatomoService } from '@app/core/services/matomo/matomo-service';
import { HttpClient } from '@angular/common/http';

@Component({
	selector: 'dataset-criteria-dialog-component',
	templateUrl: './dataset-criteria-dialog.component.html',
	styleUrls: ['./dataset-criteria-dialog.component.scss']
})

export class DatasetCriteriaDialogComponent implements OnInit {

	@ViewChild(DatasetCriteriaComponent, { static: true }) criteria: DatasetCriteriaComponent;

	constructor(
		public dialogRef: MatDialogRef<DatasetCriteriaDialogComponent>,
		private httpClient: HttpClient,
		private matomoService: MatomoService,
		@Inject(MAT_DIALOG_DATA) public data: { isPublic: boolean, status: Number, criteria: DatasetCriteria, formGroup: FormGroup, updateDataFn: Function }
	) {
	}

	ngOnInit() {
		this.matomoService.trackPageView('Dataset Criteria');
		this.criteria.setCriteria(this.data.criteria);
	}

	onNoClick(): void {
		this.dialogRef.close();
	}

	onClose(): void {
		this.dialogRef.close();
	}

	onFiltersChanged(event) {
		this.data.updateDataFn(this.criteria);
	}

}
