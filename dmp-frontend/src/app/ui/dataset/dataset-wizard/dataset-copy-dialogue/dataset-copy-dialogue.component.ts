
import { map, filter } from 'rxjs/operators';
import { Component } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { SingleAutoCompleteConfiguration } from "../../../../library/auto-complete/single/single-auto-complete-configuration";
import { Observable } from "rxjs";
import { DataTableRequest } from "../../../../core/model/data-table/data-table-request";
import { DmpCriteria } from "../../../../core/query/dmp/dmp-criteria";
import { DmpListingModel } from "../../../../core/model/dmp/dmp-listing";
import { DmpService } from "../../../../core/services/dmp/dmp.service";
import { Inject } from "@angular/core";
import { DmpModel } from "../../../../core/model/dmp/dmp";
import { TranslateService } from "@ngx-translate/core";
import { DmpAssociatedProfileModel } from '../../../../core/model/dmp-profile/dmp-associated-profile';

@Component({
	selector: 'dataset-copy-dialogue-component',
	templateUrl: 'dataset-copy-dialogue.component.html',
	styleUrls: ['./dataset-copy-dialogue.component.scss'],
})
export class DatasetCopyDialogueComponent {

	dmpAutoCompleteConfiguration: SingleAutoCompleteConfiguration;
	dmpModel: DmpModel;
	datasetDescriptionTemplateLabel: String;

	constructor(
		public dialogRef: MatDialogRef<DatasetCopyDialogueComponent>,
		public dmpService: DmpService,
		public language: TranslateService,
		@Inject(MAT_DIALOG_DATA) public data: any
	) { }

	ngOnInit() {
		this.dmpAutoCompleteConfiguration = {
			filterFn: this.searchDmp.bind(this),
			initialItems: (extraData) => this.searchDmp(''),
			displayFn: (item) => item['label'],
			titleFn: (item) => item['label'],
		};
	}

	cancel() {
		this.dialogRef.close(this.data);
	}

	confirm() {
		this.datasetProfileValidate().subscribe(x => {
			if (this.data.datasetProfileExist) {
				this.dialogRef.close(this.data);
			}
			else if (!this.data.datasetProfileExist) {
				this.data.formControl.setErrors({ 'incorrect': true });
			}
		});
	}

	searchDmp(query: string): Observable<DmpListingModel[]> {
		const fields: Array<string> = new Array<string>();
		fields.push('asc');
		const dmpDataTableRequest: DataTableRequest<DmpCriteria> = new DataTableRequest(0, null, { fields: fields });
		dmpDataTableRequest.criteria = new DmpCriteria();
		dmpDataTableRequest.criteria.like = query;
		dmpDataTableRequest.criteria.datasetTemplates = [this.data.datasetProfileId];
		return this.dmpService.getPaged(dmpDataTableRequest, "profiles").pipe(map(x => x.data));
	}

	existsDatasetDescriptionTemplate(associatedProfiles: DmpAssociatedProfileModel[]): boolean {
		return associatedProfiles.some((profile) => profile.id === this.data.datasetProfileId);
	}

	datasetProfileValidate() {
		return this.dmpService.getSingle(this.data.formControl.value.id).pipe(map(result => result as DmpModel),
			map(result => {
				this.dmpModel = result
				this.dmpModel.profiles.forEach((element) => {
					if (element.descriptionTemplateId == this.data.datasetProfileId) {
						this.data.datasetProfileExist = true;
					}
				})
			}));
	}

	getErrorMessage() {
		return this.language.instant('DATASET-WIZARD.DIALOGUE.ERROR-MESSAGE');
	}

	hasValidDatasetProfile() {
		if (this.data.datasetProfileExist) {
			return true;
		}
		else {
			return false;
		}
	}

	close() {
		this.dialogRef.close(false);
	}
}
