import { NgModule } from '@angular/core';
import { CommonUiModule } from '@common/ui/common-ui.module';

import { AutoCompleteModule } from '@app/library/auto-complete/auto-complete.module';
import { CommonFormsModule } from '@common/forms/common-forms.module';
import { DatasetCopyDialogueComponent } from './dataset-copy-dialogue.component';

@NgModule({
	imports: [
		CommonUiModule,
		CommonFormsModule,
		AutoCompleteModule
	],
	declarations: [
		DatasetCopyDialogueComponent
	],
	entryComponents: [
		DatasetCopyDialogueComponent
	]
})
export class DatasetCopyDialogModule { }
