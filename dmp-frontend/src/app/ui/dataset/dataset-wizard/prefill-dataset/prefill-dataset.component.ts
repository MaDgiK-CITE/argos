import {Component, Inject, OnInit} from "@angular/core";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {map, takeUntil} from "rxjs/operators";
import {ProgressIndicationService} from "@app/core/services/progress-indication/progress-indication-service";
import {BaseComponent} from "@common/base/base.component";
import {SingleAutoCompleteConfiguration} from "@app/library/auto-complete/single/single-auto-complete-configuration";
import {Observable, of} from "rxjs";
import {Prefilling} from "@app/core/model/dataset/prefilling";
import {PrefillingService} from "@app/core/services/prefilling.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import { DatasetProfileModel } from "@app/core/model/dataset/dataset-profile";
import { DmpProfileService } from "@app/core/services/dmp/dmp-profile.service";
import { PopupNotificationDialogComponent } from "@app/library/notification/popup/popup-notification.component";
import { TranslateService } from "@ngx-translate/core";

@Component({
	selector: 'prefill-dataset-component',
	templateUrl: 'prefill-dataset.component.html',
	styleUrls: ['prefill-dataset.component.scss']
})
export class PrefillDatasetComponent extends BaseComponent implements OnInit {

	progressIndication = false;
	prefillAutoCompleteConfiguration: SingleAutoCompleteConfiguration;
	isPrefilled: boolean = false;
	prefillForm: FormGroup;

	constructor(public dialogRef: MatDialogRef<PrefillDatasetComponent>,
				private prefillingService: PrefillingService,
				private dmpProfileService: DmpProfileService,
				private dialog: MatDialog,
				private language: TranslateService,
				private progressIndicationService: ProgressIndicationService,
				private fb: FormBuilder,
				@Inject(MAT_DIALOG_DATA) public data: any) {
		super();
	}

	ngOnInit() {
		this.progressIndicationService.getProgressIndicationObservable().pipe(takeUntil(this._destroyed)).subscribe(x => {
			setTimeout(() => { this.progressIndication = x; });
		});
		this.prefillForm = this.fb.group({
			type: this.fb.control(false),
			profile: this.fb.control('', Validators.required),
			prefill: this.fb.control(null, Validators.required)
		})
		if(this.data.availableProfiles && this.data.availableProfiles.length === 1) {
			this.addProfileIfUsedLessThanMax(this.data.availableProfiles[0]);
		}
		this.prefillAutoCompleteConfiguration = {
			filterFn: this.searchDatasets.bind(this),
			loadDataOnStart: false,
			displayFn: (item) => (item['name'].length > 60)?(item['name'].substr(0, 60) + "..." ):item['name'],
			titleFn: (item) => item['name'],
			subtitleFn: (item) => item['pid']
		};

		this.prefillForm.get('profile').valueChanges.pipe(takeUntil(this._destroyed)).subscribe(x => {
				this.checkMinMax(x);
		});
	}

	addProfileIfUsedLessThanMax(profile: DatasetProfileModel) {
		const dmpSectionIndex = this.data.datasetFormGroup.get('dmpSectionIndex').value;
		const blueprintId = this.data.datasetFormGroup.get('dmp').value.profile.id;
		this.dmpProfileService.getSingleBlueprint(blueprintId)
			.pipe(takeUntil(this._destroyed))
			.subscribe(result => {
				const section = result.definition.sections[dmpSectionIndex];
				if(section.hasTemplates){
					const foundTemplate = section.descriptionTemplates.find(template => template.descriptionTemplateId === profile.id);
					if (foundTemplate !== undefined) {
						let count = 0;
						if(this.data.datasetFormGroup.get('dmp').value.datasets != null){
							for(let dataset of this.data.datasetFormGroup.get('dmp').value.datasets){
								if(dataset.dmpSectionIndex === dmpSectionIndex && dataset.profile.id === foundTemplate.descriptionTemplateId){
									count++;
								}
							}
							if(count < foundTemplate.maxMultiplicity){
								this.prefillForm.get('profile').patchValue(profile);
							}
						}
					}
					else{
						this.prefillForm.get('profile').patchValue(profile);
					}
				}
				else{
					this.prefillForm.get('profile').patchValue(profile);
				}
			});
	}

	checkMinMax(profile: DatasetProfileModel) {
		const dmpSectionIndex = this.data.datasetFormGroup.get('dmpSectionIndex').value;
		const blueprintId = this.data.datasetFormGroup.get('dmp').value.profile.id;
		this.dmpProfileService.getSingleBlueprint(blueprintId)
			.pipe(takeUntil(this._destroyed))
			.subscribe(result => {
				const section = result.definition.sections[dmpSectionIndex];
				if(section.hasTemplates){
					const foundTemplate = section.descriptionTemplates.find(template => template.descriptionTemplateId === profile.id);
					if (foundTemplate !== undefined) {
						let count = 0;
						if(this.data.datasetFormGroup.get('dmp').value.datasets != null){
							for(let dataset of this.data.datasetFormGroup.get('dmp').value.datasets){
								if(dataset.dmpSectionIndex === dmpSectionIndex && dataset.profile.id === foundTemplate.descriptionTemplateId){
									count++;
								}
							}
							if(count === foundTemplate.maxMultiplicity){
								this.dialog.open(PopupNotificationDialogComponent, {
									data: {
										title: this.language.instant('DATASET-EDITOR.MAX-DESCRIPTION-DIALOG.TITLE'),
										message: this.language.instant('DATASET-EDITOR.MAX-DESCRIPTION-DIALOG.MESSAGE')
									}, maxWidth: '30em'
								});
								this.prefillForm.get('profile').reset();
							}
						}
					}
				}
			});
	}

	public compareWith(object1: any, object2: any) {
		return object1 && object2 && object1.id === object2.id;
	}

	searchDatasets(query: string): Observable<Prefilling[]> {
		return this.prefillingService.getPrefillingList(query).pipe(map(prefilling => prefilling.sort((a, b) => {
			if(a.name > b.name) {
				return 1;
			} else if(a.name < b.name) {
				return -1;
			} else {
				return 0;
			}
		})));
	}

	next() {
		if(this.isPrefilled) {
			if(this.prefillForm.get('prefill').value.data == null) {
				this.prefillingService.getPrefillingDataset(this.prefillForm.get('prefill').value.pid, this.prefillForm.get('profile').value.id, this.prefillForm.get('prefill').value.key).subscribe(wizard => {
					wizard.profile = this.prefillForm.get('profile').value;
					this.closeDialog(wizard);
				});
			}
			else {
				this.prefillingService.getPrefillingDatasetUsingData(this.prefillForm.get('prefill').value.data, this.prefillForm.get('profile').value.id, this.prefillForm.get('prefill').value.key).subscribe(wizard => {
					wizard.profile = this.prefillForm.get('profile').value;
					this.closeDialog(wizard);
				});
			}
		} else {
			this.closeDialog();
		}
	}

	closeDialog(result = null): void {
		this.dialogRef.close(result);
	}
}
