import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { BaseComponent } from '@common/base/base.component';
import { GuidedTourService } from '@app/library/guided-tour/guided-tour.service';
import { GuidedTour, Orientation } from '@app/library/guided-tour/guided-tour.constants';
import { TranslateService } from '@ngx-translate/core';
import { DatasetProfileModel } from '@app/core/model/dataset/dataset-profile';
import { takeUntil } from 'rxjs/operators';
import { DmpProfileService } from '@app/core/services/dmp/dmp-profile.service';
import { MatDialog } from '@angular/material/dialog';
import { PopupNotificationDialogComponent } from '@app/library/notification/popup/popup-notification.component';

@Component({
	selector: 'app-dataset-editor-component',
	templateUrl: 'dataset-editor.component.html',
	styleUrls: ['./dataset-editor.component.scss']
})
export class DatasetEditorComponent extends BaseComponent {

	@Input() formGroup: FormGroup;
	// @Input() formGroup: FormGroup = null;
	@Input() availableProfiles: DatasetProfileModel[];
	@Input() dmpId: string;
	showUri: boolean = false;
	dmpText: string = null;
	viewOnly = false;

	constructor(
		private router: Router,
		private dmpProfileService: DmpProfileService,
		private dialog: MatDialog,
		private guidedTourService: GuidedTourService,
		private language: TranslateService
	) { super(); }

	public dashboardTourDmp: GuidedTour = {
		tourId: 'only-dmp-tour',
		useOrb: true,
		steps: [
			{
				title: this.dmpText,
				content: 'Step 1',
				orientation: Orientation.Bottom,
				highlightPadding: 3,
				isStepUnique: true,
				customTopOffset: 8
			}
		]
	};

	ngOnInit() {
		this.formGroup.get('profile').valueChanges.pipe(takeUntil(this._destroyed)).subscribe(x => {
			this.checkMinMax(x);
		});
	}

	checkMinMax(profile: DatasetProfileModel) {
		const dmpSectionIndex = this.formGroup.get('dmpSectionIndex').value;
		const blueprintId = this.formGroup.get('dmp').value.profile.id;
		this.dmpProfileService.getSingleBlueprint(blueprintId)
			.pipe(takeUntil(this._destroyed))
			.subscribe(result => {
				const section = result.definition.sections[dmpSectionIndex];
				if (section.hasTemplates) {
					const foundTemplate = section.descriptionTemplates.find(template => template.descriptionTemplateId === profile.id);
					if (foundTemplate !== undefined) {
						let count = 0;
						if (this.formGroup.get('dmp').value.datasets != null) {
							for (let dataset of this.formGroup.get('dmp').value.datasets) {
								if (dataset.dmpSectionIndex === dmpSectionIndex && dataset.profile.id === foundTemplate.descriptionTemplateId) {
									count++;
								}
							}
							if (count === foundTemplate.maxMultiplicity) {
								this.dialog.open(PopupNotificationDialogComponent, {
									data: {
										title: this.language.instant('DATASET-EDITOR.MAX-DESCRIPTION-DIALOG.TITLE'),
										message: this.language.instant('DATASET-EDITOR.MAX-DESCRIPTION-DIALOG.MESSAGE')
									}, maxWidth: '30em'
								});
								this.formGroup.get('profile').reset();
							}
						}
					}
				}
			});
	}

	getDmpText(): string {
		return this.language.instant('DMP-LISTING.TEXT-INFO') + '\n\n' +
			this.language.instant('DMP-LISTING.TEXT-INFO-QUESTION') + ' ' +
			this.language.instant('DMP-LISTING.LINK-ZENODO') + ' ' +
			this.language.instant('DMP-LISTING.GET-IDEA');
	}

	setDashboardTourDmp(label: string): void {
		this.dashboardTourDmp.steps[0].title = this.getDmpText();
		this.dashboardTourDmp.steps[0].selector = '.dmp-tour-' + label;
	}

	public restartTour(label: string): void {
		this.setDashboardTourDmp(label);
		this.guidedTourService.startTour(this.dashboardTourDmp);

	}

	public cancel(): void {
		this.router.navigate(['/datasets']);
	}

	public compareWith(object1: any, object2: any) {
		return object1 && object2 && object1.id === object2.id;
	}
}
