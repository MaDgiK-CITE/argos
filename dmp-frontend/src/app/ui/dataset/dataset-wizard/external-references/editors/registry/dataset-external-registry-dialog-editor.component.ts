import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ExternalRegistryService } from '@app/core/services/external-sources/registry/external-registry.service';
import { ExternalRegistryEditorModel } from '@app/ui/dataset/dataset-wizard/dataset-wizard-editor.model';
import { BaseComponent } from '@common/base/base.component';
import { FormService } from '@common/forms/form-service';
import { takeUntil } from 'rxjs/operators';

@Component({
	templateUrl: 'dataset-external-registry-dialog-editor.component.html',
	styleUrls: ['./dataset-external-registry-dialog-editor.component.scss']
})
export class DatasetExternalRegistryDialogEditorComponent extends BaseComponent implements OnInit {
	public formGroup: FormGroup;

	constructor(
		private externalRegistryService: ExternalRegistryService,
		public dialogRef: MatDialogRef<DatasetExternalRegistryDialogEditorComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any,
		private formService: FormService
	) { super(); }

	ngOnInit(): void {
		const registryModel = new ExternalRegistryEditorModel();
		this.formGroup = registryModel.buildForm();
	}

	send(value: any) {
		this.formService.touchAllFormFields(this.formGroup);
		if (!this.formGroup.valid) { return; }
		this.externalRegistryService.create(this.formGroup.value)
			.pipe(takeUntil(this._destroyed))
			.subscribe(
				(item) => this.dialogRef.close(item)
			);
	}

	close() {
		this.dialogRef.close(false);
	}
}
