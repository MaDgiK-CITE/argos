import { NgModule } from '@angular/core';
import { FormattingModule } from '@app/core/formatting.module';
import { AutoCompleteModule } from '@app/library/auto-complete/auto-complete.module';
import { ExportMethodDialogModule } from '@app/library/export-method-dialog/export-method-dialog.module';
import { UrlListingModule } from '@app/library/url-listing/url-listing.module';
import { DatasetEditorComponent } from '@app/ui/dataset/dataset-wizard/dataset-editor/dataset-editor.component';
import { DatasetWizardComponent } from '@app/ui/dataset/dataset-wizard/dataset-wizard.component';
import { DatasetExternalReferencesEditorComponent } from '@app/ui/dataset/dataset-wizard/external-references/dataset-external-references-editor.component';
import { DatasetExternalDataRepositoryDialogEditorComponent } from '@app/ui/dataset/dataset-wizard/external-references/editors/data-repository/dataset-external-data-repository-dialog-editor.component';
import { DatasetExternalDatasetDialogEditorComponent } from '@app/ui/dataset/dataset-wizard/external-references/editors/external-dataset/dataset-external-dataset-dialog-editor.component';
import { DatasetExternalRegistryDialogEditorComponent } from '@app/ui/dataset/dataset-wizard/external-references/editors/registry/dataset-external-registry-dialog-editor.component';
import { DatasetExternalServiceDialogEditorComponent } from '@app/ui/dataset/dataset-wizard/external-references/editors/service/dataset-external-service-dialog-editor.component';
import { DatasetRoutingModule } from '@app/ui/dataset/dataset.routing';
import { DatasetCriteriaComponent } from '@app/ui/dataset/listing/criteria/dataset-criteria.component';
import { DatasetUploadDialogue } from '@app/ui/dataset/listing/criteria/dataset-upload-dialogue/dataset-upload-dialogue.component';
import { DatasetListingComponent } from '@app/ui/dataset/listing/dataset-listing.component';
import { DatasetListingItemComponent } from '@app/ui/dataset/listing/listing-item/dataset-listing-item.component';
import { DatasetDescriptionFormModule } from '@app/ui/misc/dataset-description-form/dataset-description-form.module';
import { TableOfContentsModule } from '@app/ui/misc/dataset-description-form/tableOfContentsMaterial/table-of-contents.module';
import { ExternalSourcesModule } from '@app/ui/misc/external-sources/external-sources.module';
import { CommonFormsModule } from '@common/forms/common-forms.module';
import { FormValidationErrorsDialogModule } from '@common/forms/form-validation-errors-dialog/form-validation-errors-dialog.module';
import { ConfirmationDialogModule } from '@common/modules/confirmation-dialog/confirmation-dialog.module';
import { CommonUiModule } from '@common/ui/common-ui.module';
import { AngularStickyThingsModule } from '@w11k/angular-sticky-things';
import { FormProgressIndicationModule } from '../misc/dataset-description-form/components/form-progress-indication/form-progress-indication.module';
import { DatasetCopyDialogModule } from './dataset-wizard/dataset-copy-dialogue/dataset-copy-dialogue.module';
import { DatasetCriteriaDialogComponent } from './listing/criteria/dataset-criteria-dialogue/dataset-criteria-dialog.component';
import { DatasetOverviewModule } from './overview/dataset-overview.module';
import {RichTextEditorModule} from "@app/library/rich-text-editor/rich-text-editor.module";
import {PrefillDatasetComponent} from "@app/ui/dataset/dataset-wizard/prefill-dataset/prefill-dataset.component";

@NgModule({
	imports: [
		CommonUiModule,
		CommonFormsModule,
		UrlListingModule,
		FormattingModule,
		ConfirmationDialogModule,
		AutoCompleteModule,
		ExternalSourcesModule,
		ExportMethodDialogModule,
		DatasetDescriptionFormModule,
		TableOfContentsModule,
		AngularStickyThingsModule,
		DatasetRoutingModule,
		FormValidationErrorsDialogModule,
		DatasetCopyDialogModule,
		DatasetOverviewModule,
		FormProgressIndicationModule,
		RichTextEditorModule
	],
	declarations: [
		DatasetListingComponent,
		DatasetCriteriaComponent,
		DatasetWizardComponent,
		DatasetEditorComponent,
		DatasetExternalReferencesEditorComponent,
		DatasetExternalDataRepositoryDialogEditorComponent,
		DatasetExternalDatasetDialogEditorComponent,
		DatasetExternalRegistryDialogEditorComponent,
		DatasetExternalServiceDialogEditorComponent,
		DatasetUploadDialogue,
		DatasetListingItemComponent,
		DatasetCriteriaDialogComponent,
		PrefillDatasetComponent
	],
	entryComponents: [
		DatasetExternalDataRepositoryDialogEditorComponent,
		DatasetExternalDatasetDialogEditorComponent,
		DatasetExternalRegistryDialogEditorComponent,
		DatasetExternalServiceDialogEditorComponent,
		DatasetUploadDialogue,
		DatasetCriteriaDialogComponent
	],
	exports: [
		DatasetExternalReferencesEditorComponent,
		DatasetExternalDataRepositoryDialogEditorComponent,
		DatasetExternalDatasetDialogEditorComponent,
		DatasetExternalRegistryDialogEditorComponent,
		DatasetExternalServiceDialogEditorComponent,

		DatasetEditorComponent,
		DatasetDescriptionFormModule
	]
})
export class DatasetModule { }
