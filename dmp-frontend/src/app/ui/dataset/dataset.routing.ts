import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanDeactivateGuard } from '@app/library/deactivate/can-deactivate.guard';
import { AuthGuard } from '../../core/auth-guard.service';
import { DatasetWizardComponent } from './dataset-wizard/dataset-wizard.component';
import { DatasetListingComponent } from './listing/dataset-listing.component';
import { DatasetOverviewComponent } from './overview/dataset-overview.component';

const routes: Routes = [
	{
		path: 'new/:dmpId/:dmpSectionIndex',
		component: DatasetWizardComponent,
		canActivate: [AuthGuard],
		data: {
			breadcrumb: true,
			title: 'GENERAL.TITLES.DATASET-NEW'
		},
		canDeactivate:[CanDeactivateGuard]
	},
	{
		path: 'edit/:id',
		component: DatasetWizardComponent,
		canActivate: [AuthGuard],
		data: {
			breadcrumb: true,
			public: false,
			title: 'GENERAL.TITLES.DATASET-EDIT'
		},
		canDeactivate:[CanDeactivateGuard]
	},
	{
		path: 'edit/:id/finalize',
		component: DatasetWizardComponent,
		canActivate: [AuthGuard],
		data: {
			breadcrumb: true,
			public: false,
			title: 'GENERAL.TITLES.DATASET-EDIT',
			finalize: true
		},
		canDeactivate:[CanDeactivateGuard]
	},
	{
		path: 'publicEdit/:publicId',
		component: DatasetWizardComponent,
		//canActivate: [AuthGuard],
		data: {
			public: true,
			title: 'GENERAL.TITLES.DATASET-PUBLIC-EDIT'
		},
		canDeactivate:[CanDeactivateGuard]
	},
	{
		path: 'new',
		component: DatasetWizardComponent,
		canActivate: [AuthGuard],
		data: {
			breadcrumb: true,
			title: 'GENERAL.TITLES.DATASET-NEW'
		},
		canDeactivate:[CanDeactivateGuard]
	},
	{
		path: '',
		component: DatasetListingComponent,
		// canActivate: [AuthGuard],
		data: {
			breadcrumb: true
		},
	},
	{
		path: 'dmp/:dmpId',
		component: DatasetListingComponent,
		canActivate: [AuthGuard],
		data: {
			breadcrumb: true
		},
	},
	{
		path: 'copy/:id',
		component: DatasetWizardComponent,
		canActivate: [AuthGuard],
		data: {
			breadcrumb: true,
			title: 'GENERAL.TITLES.DATASET-COPY'
		},
		canDeactivate:[CanDeactivateGuard]
	},
	{
		path: 'profileupdate/:updateId',
		component: DatasetWizardComponent,
		canActivate: [AuthGuard],
		data: {
			breadcrumb: true,
			title: 'GENERAL.TITLES.DATASET-UPDATE'
		},
		canDeactivate:[CanDeactivateGuard]
	},
	{
		path: 'overview/:id',
		component: DatasetOverviewComponent,
		data: {
			breadcrumb: true,
			title: 'GENERAL.TITLES.DATASET-OVERVIEW'
		},
	},
	{
		path: 'publicOverview/:publicId',
		component: DatasetOverviewComponent,
		data: {
			breadcrumb: true,
			title: 'GENERAL.TITLES.DATASET-OVERVIEW'
		},
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class DatasetRoutingModule { }
