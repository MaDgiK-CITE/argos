import { NgModule } from '@angular/core';
import { FormattingModule } from '@app/core/formatting.module';
import { AutoCompleteModule } from '@app/library/auto-complete/auto-complete.module';
import { ConfirmationDialogModule } from '@common/modules/confirmation-dialog/confirmation-dialog.module';
import { ExportMethodDialogModule } from '@app/library/export-method-dialog/export-method-dialog.module';
import { UrlListingModule } from '@app/library/url-listing/url-listing.module';
import { CommonFormsModule } from '@common/forms/common-forms.module';
import { CommonUiModule } from '@common/ui/common-ui.module';
import { DatasetOverviewComponent } from './dataset-overview.component';

@NgModule({
	imports: [
		CommonUiModule,
		CommonFormsModule,
		UrlListingModule,
		ConfirmationDialogModule,
		ExportMethodDialogModule,
		FormattingModule,
		AutoCompleteModule
	],
	declarations: [
		DatasetOverviewComponent
	]
})
export class DatasetOverviewModule { }