import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { AboutService } from '@app/core/services/about/about.service';
import { LanguageService } from '@app/core/services/language/language.service';
import { MatomoService } from '@app/core/services/matomo/matomo-service';
import { BaseComponent } from '@common/base/base.component';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/operators';

@Component({
	selector: 'app-about-componet',
	templateUrl: './about.component.html',
	styleUrls: ['./about.component.scss']
})
export class AboutComponent extends BaseComponent implements OnInit {

	aboutHTMLUrl: SafeResourceUrl;
	sanitizedGuideUrl: any;

	constructor(
		private aboutService: AboutService,
		private sanitizer: DomSanitizer,
		private languageService: LanguageService,
		private matomoService: MatomoService,
		private translate: TranslateService,
		private router: Router
	) { super(); }

	ngOnInit() {
		this.matomoService.trackPageView('About');
		this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
			this.router.navigate(['/reload'], { skipLocationChange: true }).then(() => this.router.navigate(['/about']));
		});
		this.aboutService.getAbout(this.languageService.getCurrentLanguage())
			.pipe(takeUntil(this._destroyed))
			.subscribe(response => {
				const blob = new Blob([response.body], { type: 'text/html' });
				this.aboutHTMLUrl = this.sanitizer.bypassSecurityTrustResourceUrl((window.URL ? URL : webkitURL).createObjectURL(blob));
			});
	}

}

