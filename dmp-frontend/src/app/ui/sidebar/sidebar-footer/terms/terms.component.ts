import { Component, OnInit } from '@angular/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { LanguageService } from '@app/core/services/language/language.service';
import { MatomoService } from '@app/core/services/matomo/matomo-service';
import { TermsOfServiceService } from '@app/core/services/terms-of-service/terms-of-service.service';
import { BaseComponent } from '@common/base/base.component';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.scss']
})
export class TermsComponent extends BaseComponent implements OnInit {

  termsHTMLUrl: SafeResourceUrl;
	sanitizedGuideUrl: any;

	constructor(
		private termsService: TermsOfServiceService,
		private sanitizer: DomSanitizer,
		private languageService: LanguageService,
		private matomoService: MatomoService,
		private translate: TranslateService,
		private router: Router
	) { super(); }

	ngOnInit() {
		this.matomoService.trackPageView('Terms of Service');
		this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
			this.router.navigate(['/reload'], { skipLocationChange: true }).then(() => this.router.navigate(['/terms-and-conditions']));
		});
		this.termsService.getTermsOfService(this.languageService.getCurrentLanguage())
			.pipe(takeUntil(this._destroyed))
			.subscribe(response => {
				const blob = new Blob([response.body], { type: 'text/html' });
				this.termsHTMLUrl = this.sanitizer.bypassSecurityTrustResourceUrl((window.URL ? URL : webkitURL).createObjectURL(blob));
			});
	}

}
