import { NgModule } from '@angular/core';
import { AdminLoginComponent } from '@app/ui/auth/admin-login/admin-login.component';
import { AdminLoginRoutingModule } from '@app/ui/auth/admin-login/admin-login.routing';
import { CommonFormsModule } from '@common/forms/common-forms.module';
import { CommonUiModule } from '@common/ui/common-ui.module';

@NgModule({
	imports: [
		CommonUiModule,
		CommonFormsModule,
		AdminLoginRoutingModule
	],
	declarations: [
		AdminLoginComponent
	]
})
export class AdminLoginModule { }
