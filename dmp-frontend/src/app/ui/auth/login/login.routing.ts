import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { B2AccessLoginComponent } from './b2access/b2access-login.component';
import { EmailConfirmation } from './email-confirmation/email-confirmation.component';
import { LinkedInLoginComponent } from './linkedin-login/linkedin-login.component';
import { LoginComponent } from './login.component';
import { OrcidLoginComponent } from './orcid-login/orcid-login.component';
import { TwitterLoginComponent } from './twitter-login/twitter-login.component';
import { OpenAireLoginComponent } from "./openaire-login/openaire-login.component";
import { ConfigurableLoginComponent } from "./configurable-login/configurable-login.component";
import { ZenodoLoginComponent } from './zenodo-login/zenodo-login.component';
import { Oauth2DialogComponent } from '@app/ui/misc/oauth2-dialog/oauth2-dialog.component';
import { MergeEmailConfirmation } from './merge-email-confirmation/merge-email-confirmation.component';
import { SamlResponseLoginComponent } from './saml/saml-login-response/saml-login-response.component';
import { UnlinkEmailConfirmation } from './unlink-email-confirmation/unlink-email-confirmation.component';

const routes: Routes = [
	{ path: '', component: LoginComponent },
	{ path: 'linkedin', component: Oauth2DialogComponent },
	{ path: 'twitter', component: Oauth2DialogComponent },
	{ path: 'external/orcid', component: Oauth2DialogComponent },
	{ path: 'external/b2access', component: Oauth2DialogComponent },
	{ path: 'confirmation/:token', component: EmailConfirmation },
	{ path: 'merge/confirmation/:token', component: MergeEmailConfirmation },
	{ path: 'unlink/confirmation/:token', component: UnlinkEmailConfirmation },
	{ path: 'confirmation', component: EmailConfirmation },
	{ path: 'openaire', component: Oauth2DialogComponent},
	{ path: 'configurable/:id', component: ConfigurableLoginComponent},
	{ path: 'external/zenodo', component: Oauth2DialogComponent },
	{ path: 'external/saml', component: SamlResponseLoginComponent }
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class LoginRoutingModule { }
