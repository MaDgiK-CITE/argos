import { Injectable } from "@angular/core";
import { BaseService } from '@common/base/base.service';
import { ConfigurableProvider } from '@app/core/model/configurable-provider/configurableProvider';

@Injectable()
export class ConfigurableProvidersService extends BaseService {

	public providers: ConfigurableProvider[];

	constructor(
	) {
		super();
	}

}
