import { Injectable } from '@angular/core';
import { UserMergeRequestModel } from '@app/core/model/merge/user-merge-request';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class MergeLoginService {
	private serviceStore = new Subject<UserMergeRequestModel>();

	getObservable(): Observable<UserMergeRequestModel> {
		return this.serviceStore.asObservable();
	}

	setRequest(request: UserMergeRequestModel) {
		this.serviceStore.next(request);
	}
}
