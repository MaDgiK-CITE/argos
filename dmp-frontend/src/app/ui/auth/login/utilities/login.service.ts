import { Injectable, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '@app/core/services/auth/auth.service';
import { CultureService } from '@app/core/services/culture/culture-service';
import { SnackBarNotificationLevel, UiNotificationService } from '@app/core/services/notification/ui-notification-service';
import { BaseService } from '@common/base/base.service';
import { TranslateService } from '@ngx-translate/core';
import { TranslateServerLoader } from '@app/core/services/language/server.loader';
import { LanguageService } from '@app/core/services/language/language.service';

@Injectable()
export class LoginService extends BaseService {

	constructor(
		private router: Router,
		private authService: AuthService,
		private translate: TranslateService,
		private zone: NgZone,
		private cultureService: CultureService,
		private uiNotificationService: UiNotificationService,
		private language: LanguageService
	) {
		super();
	}




	// 	/*
	//    * NATIVE LOGIN
	//    */

	// 	public nativeLogin(credentials: Credential) {
	// 		this.authService.nativeLogin(credentials)
	// 			.pipe(takeUntil(this._destroyed))
	// 			.subscribe(
	// 				res => this.onLogInSuccess(res),
	// 				error => this.onLogInError(error)
	// 			);
	// 	}


	// 	/*
	//    * LOGIN HANDLERS
	//    */


	public onLogInSuccess(loginResponse: any, returnUrl: string) {
		this.zone.run(() => {
			this.uiNotificationService.snackBarNotification(this.translate.instant('GENERAL.SNACK-BAR.SUCCESSFUL-LOGIN'), SnackBarNotificationLevel.Success);
			if (this.authService.current().culture) { this.cultureService.cultureSelected(this.authService.current().culture); }
			if (this.authService.current().language) { this.language.changeLanguage(this.authService.current().language); }
			const redirectUrl = returnUrl || '/';
			this.router.navigate([redirectUrl]);
		});
	}

	public onLogInError(errorMessage: string) {
		this.uiNotificationService.snackBarNotification(this.translate.instant('GENERAL.SNACK-BAR.UNSUCCESSFUL-LOGIN'), SnackBarNotificationLevel.Error);
	}
}
