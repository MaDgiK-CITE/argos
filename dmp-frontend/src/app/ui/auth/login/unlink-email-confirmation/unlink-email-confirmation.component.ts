import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UnlinkAccountEmailConfirmationService } from '@app/core/services/unlink-account-email-confirmation/unlink-account-email-confirmation.service';
import { SnackBarNotificationLevel, UiNotificationService } from '@app/core/services/notification/ui-notification-service';
import { BaseComponent } from '@common/base/base.component';
import { TranslateService } from '@ngx-translate/core';
import { takeUntil } from "rxjs/operators";

@Component({
  selector: 'app-unlink-email-confirmation-component',
  templateUrl: './unlink-email-confirmation.component.html'
})
export class UnlinkEmailConfirmation extends BaseComponent implements OnInit {

  constructor(
		private emailConfirmationService: UnlinkAccountEmailConfirmationService,
		private route: ActivatedRoute,
		private router: Router,
		private language: TranslateService,
		private uiNotificationService: UiNotificationService
	) { super(); }

  ngOnInit(): void {
    this.route.params
			.pipe(takeUntil(this._destroyed))
			.subscribe(params => {
				const token = params['token']
				if (token != null) {
					this.emailConfirmationService.emailConfirmation(token)
						.pipe(takeUntil(this._destroyed))
						.subscribe(
							result => this.onCallbackEmailConfirmationSuccess(),
							error => this.onCallbackError(error)
						)
				}
			});
  }

  	onCallbackEmailConfirmationSuccess() {
		this.router.navigate(['home']);
	}

	onCallbackError(error: any) {
		if (error.status === 302) {
			this.uiNotificationService.snackBarNotification(this.language.instant('EMAIL-CONFIRMATION.EMAIL-FOUND'), SnackBarNotificationLevel.Warning);
			this.router.navigate(['home']);
		} 
    else {
		  this.uiNotificationService.snackBarNotification(this.language.instant('EMAIL-CONFIRMATION.EXPIRED-EMAIL'), SnackBarNotificationLevel.Error);
		  this.router.navigate(['login']);
		}
	}

}
