import { NgModule } from '@angular/core';
import { B2AccessLoginComponent } from '@app/ui/auth/login/b2access/b2access-login.component';
import { ConfigurableLoginComponent } from '@app/ui/auth/login/configurable-login/configurable-login.component';
import { EmailConfirmation } from '@app/ui/auth/login/email-confirmation/email-confirmation.component';
import { LinkedInLoginComponent } from '@app/ui/auth/login/linkedin-login/linkedin-login.component';
import { LoginComponent } from '@app/ui/auth/login/login.component';
import { LoginRoutingModule } from '@app/ui/auth/login/login.routing';
import { OpenAireLoginComponent } from '@app/ui/auth/login/openaire-login/openaire-login.component';
import { OrcidLoginComponent } from '@app/ui/auth/login/orcid-login/orcid-login.component';
import { TwitterLoginComponent } from '@app/ui/auth/login/twitter-login/twitter-login.component';
import { ConfigurableProvidersService } from '@app/ui/auth/login/utilities/configurableProviders.service';
import { LoginService } from '@app/ui/auth/login/utilities/login.service';
import { Oauth2DialogModule } from '@app/ui/misc/oauth2-dialog/oauth2-dialog.module';
import { CommonFormsModule } from '@common/forms/common-forms.module';
import { CommonUiModule } from '@common/ui/common-ui.module';
import { MergeEmailConfirmation } from './merge-email-confirmation/merge-email-confirmation.component';
import { MergeLoginService } from './utilities/merge-login.service';
import { ZenodoLoginComponent } from './zenodo-login/zenodo-login.component';
import { SamlLoginService } from '@app/core/services/saml-login.service';
import { SamlResponseLoginComponent } from './saml/saml-login-response/saml-login-response.component';
import { UnlinkEmailConfirmation } from './unlink-email-confirmation/unlink-email-confirmation.component';

@NgModule({
	imports: [
		CommonUiModule,
		CommonFormsModule,
		LoginRoutingModule,
		Oauth2DialogModule
	],
	declarations: [
		LoginComponent,
		LinkedInLoginComponent,
		TwitterLoginComponent,
		B2AccessLoginComponent,
		OrcidLoginComponent,
		EmailConfirmation,
		OpenAireLoginComponent,
		ConfigurableLoginComponent,
		ZenodoLoginComponent,
		MergeEmailConfirmation,
		UnlinkEmailConfirmation,
		SamlResponseLoginComponent
	],
	exports: [
		LoginComponent
	],
	providers: [LoginService, MergeLoginService, ConfigurableProvidersService, SamlLoginService]
})
export class LoginModule { }
