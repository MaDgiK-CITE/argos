import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ExploreDmpListingComponent } from './explore-dmp-listing.component';
import { DmpOverviewComponent } from '../dmp/overview/dmp-overview.component';

const routes: Routes = [
	{
		path: '',
		component: ExploreDmpListingComponent,
		data: {
			breadcrumb: true
		}
	},
	{
		path: 'versions/:groupId',
		component: ExploreDmpListingComponent,
		data: {
			breadcrumb: true
		},
	},
	{
		path: 'overview/:publicId',
		component: DmpOverviewComponent,
		data: {
			breadcrumb: true,
			title: 'GENERAL.TITLES.EXPLORE-PLANS-OVERVIEW'
		},
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class ExploreDmpRoutingModule { }
