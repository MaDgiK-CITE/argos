import { NgModule } from "@angular/core";
import { AutoCompleteModule } from '@app/library/auto-complete/auto-complete.module';
import { DmpOverviewModule } from '@app/ui/dmp/overview/dmp-overview.module';
import { ExploreDmpFilterItemComponent } from '@app/ui/explore-dmp/dmp-explore-filters/explore-dmp-filter-item/explore-dmp-filter-item.component';
import { ExploreDmpFiltersComponent } from '@app/ui/explore-dmp/dmp-explore-filters/explore-dmp-filters.component';
import { ExploreDmpListingComponent } from '@app/ui/explore-dmp/explore-dmp-listing.component';
import { ExploreDmpRoutingModule } from '@app/ui/explore-dmp/explore-dmp.routing';
import { ExploreDmpListingItemComponent } from '@app/ui/explore-dmp/listing-item/explore-dmp-listing-item.component';
import { CommonFormsModule } from '@common/forms/common-forms.module';
import { CommonUiModule } from '@common/ui/common-ui.module';

@NgModule({
	imports: [
		CommonUiModule,
		CommonFormsModule,
		AutoCompleteModule,
		ExploreDmpRoutingModule,
		DmpOverviewModule
	],
	declarations: [
		ExploreDmpListingComponent,
		ExploreDmpFiltersComponent,
		ExploreDmpFilterItemComponent,
		ExploreDmpListingItemComponent
	]
})
export class ExploreDmpModule { }
