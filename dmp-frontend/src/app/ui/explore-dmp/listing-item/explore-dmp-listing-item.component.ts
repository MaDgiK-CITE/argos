import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DmpListingModel } from '../../../core/model/dmp/dmp-listing';
import { Principal } from '../../../core/model/auth/principal';
import { AuthService } from '../../../core/services/auth/auth.service';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
	selector: 'app-explore-dmp-listing-item-component',
	templateUrl: './explore-dmp-listing-item.component.html',
	styleUrls: ['./explore-dmp-listing-item.component.scss'],
})
export class ExploreDmpListingItemComponent implements OnInit {

	@Input() dmp: DmpListingModel;
	@Input() showDivider: boolean = true;
	@Output() onClick: EventEmitter<DmpListingModel> = new EventEmitter();

	constructor(
		private authentication: AuthService,
		private translate: TranslateService,
		private router: Router,
		private route: ActivatedRoute
	) { }

	ngOnInit() {
	}

	// itemClicked() {
	// 	this.onClick.emit(this.dmp);
	// }

	roleDisplay(value: any) {
		const principal: Principal = this.authentication.current();
		let role: number;
		if (principal) {
			value.forEach(element => {
				if (principal.id === element.id) {
					role = element.role;
				}
			});
		}
		if (role === 0) {
			return this.translate.instant('DMP-PUBLIC-LISTING.OWNER');
		}
		else if (role === 1) {
			return this.translate.instant('DMP-PUBLIC-LISTING.MEMBER');
		}
		else {
			return this.translate.instant('DMP-PUBLIC-LISTING.VIEW-ONLY');
		}
	}
}
