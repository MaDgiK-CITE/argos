import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DmpEditorComponent } from './editor/dmp-editor.component';
import { InvitationAcceptedComponent } from './invitation/accepted/dmp-invitation-accepted.component';
import { DmpListingComponent } from './listing/dmp-listing.component';
import { DmpWizardComponent } from './wizard/dmp-wizard.component';
import { DmpOverviewComponent } from './overview/dmp-overview.component';
import { DmpCloneComponent } from './clone/dmp-clone.component';
import { AuthGuard } from '@app/core/auth-guard.service';
import { CanDeactivateGuard } from '@app/library/deactivate/can-deactivate.guard';
import { DmpEditorBlueprintComponent } from './dmp-editor-blueprint/dmp-editor-blueprint.component';

const routes: Routes = [
	{
		path: '',
		component: DmpListingComponent,
		data: {
			breadcrumb: true
		},
	},
	{
		path: 'versions/:groupId',
		component: DmpListingComponent,
		data: {
			breadcrumb: true
		},
	},
	// Uncomment to get dmp plans for grant with grantId
	{
		path: 'grant/:grantId',
		component: DmpListingComponent,
		data: {
			breadcrumb: true
		},
	},
	{
		path: 'edit/:id',
		component: DmpEditorBlueprintComponent,
		data: {
			breadcrumb: true,
			title: 'GENERAL.TITLES.DMP-EDIT'
		},
		canDeactivate:[CanDeactivateGuard]
	},
	{
		path: 'publicEdit/:publicId',
		component: DmpEditorComponent,
		data: {
			breadcrumb: true,
			title: 'GENERAL.TITLES.DMP-PUBLIC-EDIT'
		},
		canDeactivate:[CanDeactivateGuard]
	},
	{
		path: 'overview/:id',
		component: DmpOverviewComponent,
		data: {
			breadcrumb: true,
			title: 'GENERAL.TITLES.DMP-OVERVIEW'
		},
	},
	{
		path: 'publicOverview/:publicId',
		component: DmpOverviewComponent,
		data: {
			breadcrumb: true,
			title: 'GENERAL.TITLES.DMP-OVERVIEW'
		},
	},
	// ----------- UNCOMMENT TO ADD AGAIN GRANTS --------
	// {
	// 	path: 'new/grant/:grantId',
	// 	component: DmpEditorComponent,
	// 	data: {
	// 		breadcrumbs: 'new'
	// 	}
	// },
	{
		path: 'new',
		component: DmpEditorBlueprintComponent,
		canActivate: [AuthGuard],
		data: {
			breadcrumbs: 'new',
			title: 'GENERAL.TITLES.DMP-NEW'
		},
		canDeactivate:[CanDeactivateGuard]
	},
	// {
	// 	path: 'new/dataset',
	// 	component: DmpEditorComponent,
	// 	canActivate: [AuthGuard],
	// 	data: {
	// 		breadcrumbs: 'new/dataset',
	// 		title: 'GENERAL.TITLES.DATASET-NEW'
	// 	}
	// },
	// {
	// 	path: 'new/dataset/:dmpId',
	// 	component: DmpEditorComponent,
	// 	canActivate: [AuthGuard],
	// 	data: {
	// 		breadcrumbs: 'new/dataset',
	// 		title: 'GENERAL.TITLES.DATASET-NEW'
	// 	}
	// },
	{
		path: 'new_version/:id',
		// component: DmpWizardComponent,
		component: DmpCloneComponent,
		data: {
			clone: false,
			breadcrumb: true,
			title: 'GENERAL.TITLES.DMP-NEW-VERSION'
		},
	},
	{
		path: 'clone/:id',
		component: DmpCloneComponent,
		data: {
			clone: false,
			breadcrumb: true,
			title: 'GENERAL.TITLES.DMP-CLONE'
		},
	},
	{
		path: 'invitation/:id',
		component: InvitationAcceptedComponent,
		data: {
			breadcrumb: true
		},
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class DmpRoutingModule { }
