import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
	selector: 'app-to-dataset-dialog-component',
	templateUrl: 'dmp-to-dataset-dialog.component.html',
	styleUrls: ['./dmp-to-dataset-dialog.component.scss'],
})
export class DmpToDatasetDialogComponent implements OnInit {

	constructor(
		public dialogRef: MatDialogRef<DmpToDatasetDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any
	) {	}

	ngOnInit(): void {
	}

	start(): void {
		this.dialogRef.close(true);
	}

	closeDialog(): void {
		this.dialogRef.close();
	}

}
