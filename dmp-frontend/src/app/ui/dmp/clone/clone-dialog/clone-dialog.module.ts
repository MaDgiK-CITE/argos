import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonUiModule } from '@common/ui/common-ui.module';
import { CloneDialogComponent } from './clone-dialog.component';
import { AutoCompleteModule } from '@app/library/auto-complete/auto-complete.module';
import {RichTextEditorModule} from "@app/library/rich-text-editor/rich-text-editor.module";

@NgModule({
  imports: [CommonUiModule, FormsModule, ReactiveFormsModule, AutoCompleteModule, RichTextEditorModule],
	declarations: [CloneDialogComponent],
	exports: [CloneDialogComponent],
	entryComponents: [CloneDialogComponent]
})
export class CloneDialogModule {
	constructor() { }
}
