
import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, FormArray } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { DmpModel } from '@app/core/model/dmp/dmp';
import { DmpService } from '@app/core/services/dmp/dmp.service';
import { SnackBarNotificationLevel, UiNotificationService } from '@app/core/services/notification/ui-notification-service';
import { FunderFormModel } from '@app/ui/dmp/editor/grant-tab/funder-form-model';
import { GrantTabModel } from '@app/ui/dmp/editor/grant-tab/grant-tab-model';
import { ProjectFormModel } from '@app/ui/dmp/editor/grant-tab/project-form-model';
import { DmpWizardEditorModel } from '@app/ui/dmp/wizard/dmp-wizard-editor.model';
import { BreadcrumbItem } from '@app/ui/misc/breadcrumb/definition/breadcrumb-item';
import { BaseComponent } from '@common/base/base.component';
import { ValidationErrorModel } from '@common/forms/validation/error-model/validation-error-model';
import { TranslateService } from '@ngx-translate/core';
import { Observable, of as observableOf } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { DmpStatus } from '@app/core/common/enum/dmp-status';
import { ExtraPropertiesFormModel } from '../editor/general-tab/extra-properties-form.model';
import { Principal } from '@app/core/model/auth/principal';
import { AuthService } from '@app/core/services/auth/auth.service';
import { Role } from '@app/core/common/enum/role';
import { DatasetListingModel } from '@app/core/model/dataset/dataset-listing';
import { DataTableRequest } from '@app/core/model/data-table/data-table-request';
import { DatasetCriteria } from '@app/core/query/dataset/dataset-criteria';
import { DatasetService } from '@app/core/services/dataset/dataset.service';
import { DmpEditorModel } from '../editor/dmp-editor.model';
import { DatasetWizardEditorModel } from '@app/ui/dataset/dataset-wizard/dataset-wizard-editor.model';
import { ConfirmationDialogComponent } from '@common/modules/confirmation-dialog/confirmation-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { isNullOrUndefined } from '@app/utilities/enhancers/utils';
import { DmpProfileService } from '@app/core/services/dmp/dmp-profile.service';
import { DmpBlueprintDefinition, SystemFieldType } from '@app/core/model/dmp/dmp-blueprint/dmp-blueprint';


@Component({
	selector: 'app-dmp-clone',
	templateUrl: './dmp-clone.component.html',
	styleUrls: ['./dmp-clone.component.scss']
})
export class DmpCloneComponent extends BaseComponent implements OnInit {

	breadCrumbs: Observable<BreadcrumbItem[]>;
	// dmp: DmpWizardEditorModel;
	dmp: DmpEditorModel;
	formGroup: FormGroup;
	itemId: string;
	datasetId: string = null;
	isFinalized: false;
	isPublic: false;
	parentDmpLabel: string;
	isNewVersion: boolean = false;
	isClone: boolean = true;
	isNew: boolean = false;
	isUserOwner: boolean = true;
	// datasets: DatasetListingModel[] = [];
	datasets = new FormArray([]);
	removeIndexes = [];

	selectedTab = 0;
	step: number = 0;
	stepsBeforeDatasets: number = 3;
	maxStep: number = 3;

	constructor(
		private route: ActivatedRoute,
		private router: Router,
		private language: TranslateService,
		private dmpService: DmpService,
		private dmpProfileService: DmpProfileService,
		private authentication: AuthService,
		private uiNotificationService: UiNotificationService,
		private datasetService: DatasetService,
		private dialog: MatDialog
	) {
		super();
	}

	ngOnInit() {
		this.route.params
			.pipe(takeUntil(this._destroyed))
			.subscribe((params: Params) => {
				this.itemId = params['id'];
				this.dmpService.getSingle(this.itemId).pipe(map(data => data as DmpModel))
					.pipe(takeUntil(this._destroyed))
					.subscribe(data => {
						this.dmp = new DmpEditorModel();
						this.dmp.grant = new GrantTabModel();
						this.dmp.project = new ProjectFormModel();
						this.dmp.funder = new FunderFormModel();
						this.dmp.extraProperties = new ExtraPropertiesFormModel();
						this.dmp.fromModel(data);
						this.dmp.status = DmpStatus.Draft;
						this.formGroup = this.dmp.buildForm();

						if (!isNullOrUndefined(this.formGroup.get('profile').value)) { 
							this.dmpProfileService.getSingleBlueprint(this.formGroup.get('profile').value)
								.pipe(takeUntil(this._destroyed))
								.subscribe(result => {
									this.checkForGrant(result.definition);
									this.checkForFunder(result.definition);
									this.checkForProject(result.definition);
								});
						}

						this.datasets = this.formGroup.get('datasets') as FormArray;

						this.parentDmpLabel = this.formGroup.get('label').value;
						if (this.route.routeConfig.path.startsWith('new_version/')) {
							this.formGroup.get('version').setValue(this.formGroup.get('version').value + 1);
							this.formGroup.controls['label'].disable();
							this.formGroup.controls['grant'].disable();
							this.isNewVersion = true;
						} else if (this.route.routeConfig.path.startsWith('clone/')) {
							this.formGroup.get('label').setValue(this.dmp.label + " New");
							this.isNewVersion = false;
						}

						// const request: DataTableRequest<DatasetCriteria> = new DataTableRequest<DatasetCriteria>(null, null, null);
						// request.criteria = new DatasetCriteria();
						// request.criteria.dmpIds = [this.formGroup.get('id').value];
						// request.criteria.allVersions = true;
						// this.datasetService.getPaged(request)
						// 	.pipe(takeUntil(this._destroyed))
						// 	.subscribe(items => {
						// 		this.datasets = items.data;
						// 		this.formGroup.addControl('datasets', new FormBuilder().array(new Array<FormControl>()));
						// 		this.datasets.forEach(element => {
						// 			console.log(element);
						// 			(<FormArray>this.formGroup.get('datasets')).push(new FormBuilder().group({ id: element.value }));
						// 		});
						// 	});


						const breadCrumbs = [];
						breadCrumbs.push({ parentComponentName: null, label: this.language.instant('NAV-BAR.MY-DMPS'), url: "/plans" });
						breadCrumbs.push({ parentComponentName: 'DmpListingComponent', label: this.dmp.label, url: '/plans/clone/' + this.dmp.id });
						this.breadCrumbs = observableOf(breadCrumbs);
					});
			});

	}

	private checkForGrant(blueprint: DmpBlueprintDefinition) {
		let hasGrant = false;
		blueprint.sections.forEach(section => section.fields.forEach(
			field => {
				if (field.category as unknown === 'SYSTEM' && field.type === SystemFieldType.GRANT) {
					hasGrant = true;
				}
			}
		));
		if (!hasGrant) {
			this.formGroup.removeControl('grant');
		}
	}

	private checkForFunder(blueprint: DmpBlueprintDefinition) {
		let hasFunder = false;
		blueprint.sections.forEach(section => section.fields.forEach(
			field => {
				if (field.category as unknown === 'SYSTEM' && field.type === SystemFieldType.FUNDER) {
					hasFunder = true;
				}
			}
		));
		if (!hasFunder) {
			this.formGroup.removeControl('funder');
		}
	}

	private checkForProject(blueprint: DmpBlueprintDefinition) {
		let hasProject = false;
		blueprint.sections.forEach(section => section.fields.forEach(
			field => {
				if (field.category as unknown === 'SYSTEM' && field.type === SystemFieldType.PROJECT) {
					hasProject = true;
				}
			}
		));
		if (!hasProject) {
			this.formGroup.removeControl('project');
		}
	}

	public cancel(id: String): void {
		if (id != null) {
			this.router.navigate(['/plans/overview/' + id]);
		} else {
			this.router.navigate(['/plans']);
		}
	}

	public isFormValid(): boolean {
		return this.formGroup.valid;
	}

	formSubmit(): void {
		if (!this.isFormValid()) { return; }
		if (this.isNewVersion) {
			this.dmpService.newVersion(this.formGroup.getRawValue(), this.itemId)
				.pipe(takeUntil(this._destroyed))
				.subscribe(
					complete => this.onCallbackSuccess(),
					error => this.onCallbackErrorNewVersion(error)
				);
		}
		else {
			this.removeDatasetsFromClone();
			this.dmpService.clone(this.formGroup.getRawValue(), this.itemId)
				.pipe(takeUntil(this._destroyed))
				.subscribe(
					complete => this.onCallbackSuccess(),
					error => this.onCallbackError(error)
				);
		}
	}

	onCallbackSuccess(): void {
		this.uiNotificationService.snackBarNotification(this.language.instant('GENERAL.SNACK-BAR.SUCCESSFUL-UPDATE'), SnackBarNotificationLevel.Success);
		this.router.navigate(['/plans']);
	}

	onCallbackError(error: any) {
		this.setErrorModel(error.error);
	}

	onCallbackErrorNewVersion(errorResponse: HttpErrorResponse) {
		this.uiNotificationService.snackBarNotification(errorResponse.error.message, SnackBarNotificationLevel.Error);
	}

	public setErrorModel(validationErrorModel: ValidationErrorModel) {
		Object.keys(validationErrorModel).forEach(item => {
			(<any>this.dmp.validationErrorModel)[item] = (<any>validationErrorModel)[item];
		});
	}

	isDirty() {

	}

	discardChanges() {

	}

	public changeStep(index: number, dataset?: DatasetListingModel) {
		this.step = index;
		if (dataset) { this.datasetId = dataset.id };
	}

	public nextStep() {
		this.step = this.step < this.maxStep ? this.step + 1 : this.step;
		if (this.step >= this.stepsBeforeDatasets) {
			this.datasetId = this.datasets[this.step - this.stepsBeforeDatasets].id;
		}
	}

	public previousStep() {
		this.step = this.step !== 0 ? this.step - 1 : this.step;
		if (this.step >= this.stepsBeforeDatasets) {
			this.datasetId = this.datasets[this.step - this.stepsBeforeDatasets].id;
		}
	}

	setIsUserOwner() {
		if (this.dmp) {
			const principal: Principal = this.authentication.current();
			if (principal) {
				this.isUserOwner = !!this.dmp.users.find(x => (x.role === Role.Owner) && (principal.id === x.id));
			} else {
				this.isUserOwner = false;
			}
		}
	}

	formChanged() {

	}

	addDataset() {
		if (!this.formGroup.get('datasets')) {
			this.formGroup.addControl('datasets', new FormBuilder().array(new Array<FormControl>()));
		}
		this.formGroup.get('datasets')['controls'].push(new DatasetWizardEditorModel().buildForm());
		this.datasets = this.formGroup.get('datasets') as FormArray;
		this.step = this.stepsBeforeDatasets + this.formGroup.get('datasets')['controls'].length - 1;
		this.maxStep = this.maxStep + this.formGroup.get('datasets')['controls'].length - 1;
	}

	editDataset(id: string) {
		this.router.navigate(['/datasets', 'edit', id]);
	}

	hasProfile(): boolean {
		return this.formGroup.get('profiles') && this.formGroup.get('profiles').value && this.formGroup.get('profiles').value.length > 0;
	}

	selectionChanged(event, dataset) {
		var datasetIndex = this.formGroup.get('datasets').value.map(function (item) { return item.id; }).indexOf(dataset.value.id);
		if (!event.checked) {
			if (this.removeIndexes.indexOf(datasetIndex) === -1) { this.removeIndexes.push(datasetIndex); }
		} else {
			var index = this.removeIndexes.indexOf(datasetIndex);
			this.removeIndexes.splice(index, 1)
		}
	}

	removeDatasetsFromClone() {
		this.removeIndexes.forEach(index => {
			this.formGroup.get('datasets')['controls'].splice(index, 1);
			this.formGroup.get('datasets').value.splice(index, 1)
		})
	}

	public removeDataset(datasetId: string, index: number) {
		const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
			maxWidth: '300px',
			restoreFocus: false,
			data: {
				message: this.language.instant('GENERAL.CONFIRMATION-DIALOG.DELETE-ITEM'),
				confirmButton: this.language.instant('GENERAL.CONFIRMATION-DIALOG.ACTIONS.DELETE'),
				cancelButton: this.language.instant('GENERAL.CONFIRMATION-DIALOG.ACTIONS.CANCEL'),
				isDeleteConfirmation: true
			}
		});
		dialogRef.afterClosed().pipe(takeUntil(this._destroyed)).subscribe(result => {
			if (result) {
				if (datasetId) {
					this.datasetService.delete(datasetId)
						.pipe(takeUntil(this._destroyed))
						.subscribe(
							complete => {
								this.onDeleteCallbackSuccess();
							},
							error => this.onDeleteCallbackError(error)
						);
				}
				this.formGroup.get('datasets')['controls'].splice(index, 1);
				this.step = 0;
			}
		});
	}
	onDeleteCallbackSuccess(): void {
		this.uiNotificationService.snackBarNotification(this.language.instant('GENERAL.SNACK-BAR.SUCCESSFUL-DELETE'), SnackBarNotificationLevel.Success);
		this.dmp.id != null ? this.router.navigate(['/plans', 'edit', this.dmp.id]) : this.router.navigate(['/plans']);
	}
	onDeleteCallbackError(error) {
		this.uiNotificationService.snackBarNotification(error.error.message ? error.error.message : this.language.instant('GENERAL.SNACK-BAR.UNSUCCESSFUL-DELETE'), SnackBarNotificationLevel.Error);
	}
}
