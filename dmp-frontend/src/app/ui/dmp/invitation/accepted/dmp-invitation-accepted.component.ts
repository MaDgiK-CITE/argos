import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '@app/core/services/auth/auth.service';
import { DmpInvitationService } from '@app/core/services/dmp/dmp-invitation.service';
import { BaseComponent } from '@common/base/base.component';
import { takeUntil } from 'rxjs/operators';

@Component({
	selector: 'app-dmp-invitation-accepted-component',
	templateUrl: 'dmp-invitation-accepted.component.html',

})
export class InvitationAcceptedComponent extends BaseComponent implements OnInit {
	constructor(
		private dmpInvitationService: DmpInvitationService,
		private route: ActivatedRoute,
		private router: Router,
		private authentication: AuthService,
	) { super(); }

	ngOnInit(): void {


		this.route.params
			.pipe(takeUntil(this._destroyed))
			.subscribe(params => {
				const id = params['id'];

				if(this.isAuthenticated()){
					this.dmpInvitationService.exchange(id)
					.pipe(takeUntil(this._destroyed))
					.subscribe(result => {
						this.router.navigate(['plans/edit/' + result]);
					});
				}else{
					this.router.navigate(['/login'], {queryParams:{returnUrl:'plans/invitation/'+id}});
				}
				
				
			});
		


	}


	public isAuthenticated(): boolean {
		return !(!this.authentication.current());
	}
}
