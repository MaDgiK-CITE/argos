import {NgModule} from '@angular/core';
import {FormattingModule} from '@app/core/formatting.module';
import {AutoCompleteModule} from '@app/library/auto-complete/auto-complete.module';
import {ConfirmationDialogModule} from '@common/modules/confirmation-dialog/confirmation-dialog.module';
import {ExportMethodDialogModule} from '@app/library/export-method-dialog/export-method-dialog.module';
import {UrlListingModule} from '@app/library/url-listing/url-listing.module';
import {DmpCloneComponent} from '@app/ui/dmp/clone/dmp-clone.component';
import {DmpRoutingModule} from '@app/ui/dmp/dmp.routing';
import {AddResearcherComponent} from '@app/ui/dmp/editor/add-researcher/add-researcher.component';
import {AvailableProfilesComponent} from '@app/ui/dmp/editor/available-profiles/available-profiles.component';
import {DatasetsTabComponent} from '@app/ui/dmp/editor/datasets-tab/datasets-tab.component';
import {DmpEditorComponent} from '@app/ui/dmp/editor/dmp-editor.component';
import {DmpFinalizeDialogComponent} from '@app/ui/dmp/editor/dmp-finalize-dialog/dmp-finalize-dialog.component';
import {
	DynamicDmpFieldResolverComponent
} from '@app/ui/dmp/editor/dynamic-field-resolver/dynamic-dmp-field-resolver.component';
import {
	DynamicFieldGrantComponent
} from '@app/ui/dmp/editor/dynamic-fields-grant/dynamic-field-grant/dynamic-field-grant.component';
import {DynamicFieldsGrantComponent} from '@app/ui/dmp/editor/dynamic-fields-grant/dynamic-fields-grant.component';
import {GeneralTabComponent} from '@app/ui/dmp/editor/general-tab/general-tab.component';
import {GrantTabComponent} from '@app/ui/dmp/editor/grant-tab/grant-tab.component';
import {PeopleTabComponent} from '@app/ui/dmp/editor/people-tab/people-tab.component';
import {InvitationAcceptedComponent} from '@app/ui/dmp/invitation/accepted/dmp-invitation-accepted.component';
import {DmpInvitationDialogComponent} from '@app/ui/dmp/invitation/dmp-invitation-dialog.component';
import {DmpCriteriaComponent} from '@app/ui/dmp/listing/criteria/dmp-criteria.component';
import {DmpUploadDialogue} from '@app/ui/dmp/listing/upload-dialogue/dmp-upload-dialogue.component';
import {DmpListingComponent} from '@app/ui/dmp/listing/dmp-listing.component';
import {DmpListingItemComponent} from '@app/ui/dmp/listing/listing-item/dmp-listing-item.component';
import {DmpOverviewModule} from '@app/ui/dmp/overview/dmp-overview.module';
import {DmpWizardComponent} from '@app/ui/dmp/wizard/dmp-wizard.component';
import {DmpWizardEditorComponent} from '@app/ui/dmp/wizard/editor/dmp-wizard-editor.component';
import {DmpWizardDatasetListingComponent} from '@app/ui/dmp/wizard/listing/dmp-wizard-dataset-listing.component';
import {CommonFormsModule} from '@common/forms/common-forms.module';
import {
	FormValidationErrorsDialogModule
} from '@common/forms/form-validation-errors-dialog/form-validation-errors-dialog.module';
import {CommonUiModule} from '@common/ui/common-ui.module';
import {MultipleChoiceDialogModule} from '@common/modules/multiple-choice-dialog/multiple-choice-dialog.module';
import {AddOrganizationComponent} from './editor/add-organization/add-organization.component';
import {AddCostComponent} from './editor/cost-editor/add-cost/add-cost.component';
import {CostListingComponent} from './editor/cost-editor/cost-listing/cost-listing.component';
import {DmpCriteriaDialogComponent} from './listing/criteria/dmp-criteria-dialog.component';
import {StartNewDmpDialogComponent} from './start-new-dmp-dialogue/start-new-dmp-dialog.component';
import {MainInfoComponent} from './editor/main-info/main-info.component';
import {FundingInfoComponent} from './editor/funding-info/funding-info.component';
import {DatasetInfoComponent} from './editor/dataset-info/dataset-info.component';
import {DatasetEditorDetailsModule} from './editor/dataset-editor-details/dataset-editor-details.module';
import {DatasetEditorDetailsComponent} from './editor/dataset-editor-details/dataset-editor-details.component';
import {DatasetDescriptionFormModule} from '../misc/dataset-description-form/dataset-description-form.module';
import {LicenseInfoComponent} from './editor/license-info/license-info.component';
import {StartNewDatasetDialogComponent} from './start-new-dataset-dialogue/start-new-dataset-dialog.component';
import {NgxDropzoneModule} from 'ngx-dropzone';
import {DmpToDatasetDialogComponent} from './dmp-to-dataset/dmp-to-dataset-dialog.component';
import {
	FormProgressIndicationModule
} from '../misc/dataset-description-form/components/form-progress-indication/form-progress-indication.module';
import {DatasetPreviewDialogComponent} from './dataset-preview/dataset-preview-dialog.component';
import {RichTextEditorModule} from "@app/library/rich-text-editor/rich-text-editor.module";
import { DmpEditorBlueprintComponent } from './dmp-editor-blueprint/dmp-editor-blueprint.component';

@NgModule({
	imports: [
		CommonUiModule,
		CommonFormsModule,
		UrlListingModule,
		ConfirmationDialogModule,
		ExportMethodDialogModule,
		FormattingModule,
		AutoCompleteModule,
		DmpRoutingModule,
		DmpOverviewModule,
		FormValidationErrorsDialogModule,
		MultipleChoiceDialogModule,
		DatasetEditorDetailsModule,
		DatasetDescriptionFormModule,
		NgxDropzoneModule,
		FormProgressIndicationModule,
		RichTextEditorModule
	],
	declarations: [
		DmpListingComponent,
		DmpCriteriaComponent,
		DmpEditorComponent,
		DmpInvitationDialogComponent,
		InvitationAcceptedComponent,
		DmpToDatasetDialogComponent,
		DmpWizardComponent,
		DmpWizardEditorComponent,
		DmpWizardDatasetListingComponent,
		AddResearcherComponent,
		AvailableProfilesComponent,
		DmpFinalizeDialogComponent,
		DynamicDmpFieldResolverComponent,
		DynamicFieldsGrantComponent,
		DynamicFieldGrantComponent,
		DmpUploadDialogue,
		DmpListingItemComponent,
		GeneralTabComponent,
		PeopleTabComponent,
		GrantTabComponent,
		DatasetsTabComponent,
		DmpCloneComponent,
		AddOrganizationComponent,
		DmpCriteriaDialogComponent,
		AddCostComponent,
		CostListingComponent,
		StartNewDmpDialogComponent,
		StartNewDatasetDialogComponent,
		MainInfoComponent,
		FundingInfoComponent,
		DatasetInfoComponent,
		LicenseInfoComponent,
		DatasetPreviewDialogComponent,
  		DmpEditorBlueprintComponent
	],
	entryComponents: [
		DmpInvitationDialogComponent,
		DmpToDatasetDialogComponent,
		AddResearcherComponent,
		AvailableProfilesComponent,
		DmpFinalizeDialogComponent,
		DmpUploadDialogue,
		AddOrganizationComponent,
		DmpCriteriaDialogComponent,
		AddOrganizationComponent,
		AddCostComponent,
		StartNewDmpDialogComponent,
		StartNewDatasetDialogComponent,
		DatasetEditorDetailsComponent,
		DatasetPreviewDialogComponent
	]
})
export class DmpModule { }
