import { NgModule } from '@angular/core';
import { FormattingModule } from '@app/core/formatting.module';
import { AutoCompleteModule } from '@app/library/auto-complete/auto-complete.module';
import { ConfirmationDialogModule } from '@common/modules/confirmation-dialog/confirmation-dialog.module';
import { ExportMethodDialogModule } from '@app/library/export-method-dialog/export-method-dialog.module';
import { UrlListingModule } from '@app/library/url-listing/url-listing.module';
import { DmpRoutingModule } from '@app/ui/dmp/dmp.routing';
import { DmpOverviewModule } from '@app/ui/dmp/overview/dmp-overview.module';
import { CommonFormsModule } from '@common/forms/common-forms.module';
import { FormValidationErrorsDialogModule } from '@common/forms/form-validation-errors-dialog/form-validation-errors-dialog.module';
import { CommonUiModule } from '@common/ui/common-ui.module';
import { MultipleChoiceDialogModule } from '@common/modules/multiple-choice-dialog/multiple-choice-dialog.module';
import { DatasetEditorDetailsComponent } from './dataset-editor-details.component';
import { DatasetExternalReferencesEditorComponent } from '@app/ui/dataset/dataset-wizard/external-references/dataset-external-references-editor.component';
import { DatasetModule } from '@app/ui/dataset/dataset.module';
import { DatasetExternalServiceDialogEditorComponent } from '@app/ui/dataset/dataset-wizard/external-references/editors/service/dataset-external-service-dialog-editor.component';
import { DatasetExternalRegistryDialogEditorComponent } from '@app/ui/dataset/dataset-wizard/external-references/editors/registry/dataset-external-registry-dialog-editor.component';
import { DatasetExternalDatasetDialogEditorComponent } from '@app/ui/dataset/dataset-wizard/external-references/editors/external-dataset/dataset-external-dataset-dialog-editor.component';
import { DatasetExternalDataRepositoryDialogEditorComponent } from '@app/ui/dataset/dataset-wizard/external-references/editors/data-repository/dataset-external-data-repository-dialog-editor.component';
import { DatasetDescriptionFormModule } from '@app/ui/misc/dataset-description-form/dataset-description-form.module';


@NgModule({
	imports: [
		CommonUiModule,
		CommonFormsModule,
		UrlListingModule,
		ConfirmationDialogModule,
		ExportMethodDialogModule,
		FormattingModule,
		AutoCompleteModule,
		DmpRoutingModule,
		DmpOverviewModule,
		FormValidationErrorsDialogModule,
		MultipleChoiceDialogModule,
		DatasetModule,
		DatasetDescriptionFormModule
	],
	declarations: [
		DatasetEditorDetailsComponent
	],
	entryComponents: [
		DatasetEditorDetailsComponent,
		DatasetExternalReferencesEditorComponent,
		DatasetExternalDataRepositoryDialogEditorComponent,
		DatasetExternalDatasetDialogEditorComponent,
		DatasetExternalRegistryDialogEditorComponent,
		DatasetExternalServiceDialogEditorComponent
	],
	exports: [
		DatasetEditorDetailsComponent
	]
})
export class DatasetEditorDetailsModule { }
