import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { DatasetStatus } from '@app/core/common/enum/dataset-status';
import { DatasetService } from '@app/core/services/dataset/dataset.service';
import { BaseComponent } from '@common/base/base.component';
import { Observable, of } from 'rxjs';
import { catchError, map, takeUntil } from 'rxjs/operators';


@Component({
	selector: 'app-dmp-finalize-dialog-component',
	templateUrl: 'dmp-finalize-dialog.component.html',
	styleUrls: ['./dmp-finalize-dialog.component.scss']
})
export class DmpFinalizeDialogComponent extends BaseComponent implements OnInit {

	inputModel: DmpFinalizeDialogInput;
	outputModel: DmpFinalizeDialogOutput;

	protected datasetLookupStatus:DatasetStatusLookup[] = [];
	protected datasetLookupStatusEnum = DatasetStatusLookup;
	constructor(
		public router: Router,
		public dialogRef: MatDialogRef<DmpFinalizeDialogComponent>,
		public datasetService: DatasetService,
		@Inject(MAT_DIALOG_DATA) public data: any
	) {
		super();
		this.inputModel = data['dialogInputModel'];
		this.outputModel = { datasetsToBeFinalized: [] };
	}

	ngOnInit(): void {
		this.inputModel.datasets.forEach(ds=>{
			
			this.validateDataset(ds.id).subscribe(_=>{
				console.log('response', _);
			});
		})

		const drafts = this.getDraftDatasets();

		drafts.forEach(draft=>{
			this.datasetLookupStatus[draft.id] == DatasetStatusLookup.PENDING_VALIDATION;
			this.validateDataset(draft.id)
				.subscribe(isValid=>{
					if(isValid){
						this.datasetLookupStatus[draft.id] = DatasetStatusLookup.VALID;
					}else{
						this.datasetLookupStatus[draft.id] = DatasetStatusLookup.INVALID;
					}
				})
		});


	 }

	onSubmit() {
		this.dialogRef.close(this.outputModel);
	}

	getFinalizedDatasets() {
		return this.inputModel.datasets.filter(x => x.status === DatasetStatus.Finalized);
	}

	getDraftDatasets() {
		return this.inputModel.datasets.filter(x => x.status === DatasetStatus.Draft);
	}

	close() {
		this.dialogRef.close({ cancelled: true } as DmpFinalizeDialogOutput);
	}

	validateDataset(datasetId: string) : Observable<boolean>{
		return this.datasetService
		.validateDataset(datasetId)
		.pipe(
			takeUntil(this._destroyed),				
			map(_=>{
				return true
			}),
			catchError(error=>{
				return of(false);
			})
		);
	}

	get validDrafts(){
		return this.getDraftDatasets().filter(x=> this.datasetLookupStatus[x.id] && (this.datasetLookupStatus[x.id] == DatasetStatusLookup.VALID));
	}
}

export interface DmpFinalizeDialogInput {
	dmpLabel: string;
	dmpDescription: string;
	datasets: DmpFinalizeDialogDataset[];
	accessRights: boolean;
}

export interface DmpFinalizeDialogDataset {
	label: string;
	id?: string;
	status: DatasetStatus;
}

export interface DmpFinalizeDialogOutput {
	cancelled?: boolean;
	datasetsToBeFinalized?: string[];
}
enum DatasetStatusLookup{
	VALID = "VALID",
	INVALID = "INVALID",
	PENDING_VALIDATION = "PENDING"
}
