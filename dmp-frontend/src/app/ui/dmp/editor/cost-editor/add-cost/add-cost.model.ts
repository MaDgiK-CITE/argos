import { FormBuilder, FormGroup } from '@angular/forms';
import { ResearcherModel } from '@app/core/model/researcher/researcher';
import { BackendErrorValidator } from '@common/forms/validation/custom-validator';
import { ValidationErrorModel } from '@common/forms/validation/error-model/validation-error-model';
import { ValidationContext } from '@common/forms/validation/validation-context';
import { OrganizationModel } from '@app/core/model/organisation/organization';
import { CostModel } from '@app/core/model/dmp/cost';

export class CostEditorModel implements CostModel{
	public code: string;
	public description: string;
	public title: string;
	public value: number;
	public validationErrorModel: ValidationErrorModel = new ValidationErrorModel();

	fromModel(item: CostModel): CostEditorModel {
		this.code = item.code;
		this.description = item.description;
		this.title = item.title;
		this.value = item.value;
		return this;
	}

	buildForm(context: ValidationContext = null, disabled: boolean = false): FormGroup {
		if (context == null) { context = this.createValidationContext(); }
		const formGroup = new FormBuilder().group({
			code: [{ value: this.code, disabled: disabled }, context.getValidation('code').validators],
			description: [{ value: this.description, disabled: disabled }, context.getValidation('description').validators],
			title: [{ value: this.title, disabled: disabled }, context.getValidation('title').validators],
			value: [{ value: this.value, disabled: disabled }, context.getValidation('value').validators]
		});

		return formGroup;
	}

	createValidationContext(): ValidationContext {
		const baseContext: ValidationContext = new ValidationContext();
		baseContext.validation.push({ key: 'code', validators: [] });
		baseContext.validation.push({ key: 'description', validators: [] });
		baseContext.validation.push({ key: 'title', validators: [BackendErrorValidator(this.validationErrorModel, 'title')] });
		baseContext.validation.push({ key: 'value', validators: [] });
		return baseContext;
	}
}
