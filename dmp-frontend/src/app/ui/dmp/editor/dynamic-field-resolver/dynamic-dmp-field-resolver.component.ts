import { Component, Input, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { DmpProfileFieldDataType } from '../../../../core/common/enum/dmp-profile-field-type';
import { DmpProfileType } from '../../../../core/common/enum/dmp-profile-type';
import { DmpProfileDefinition } from '../../../../core/model/dmp-profile/dmp-profile';
import { SingleAutoCompleteConfiguration } from '../../../../library/auto-complete/single/single-auto-complete-configuration';
import { MultipleAutoCompleteConfiguration } from '../../../../library/auto-complete/multiple/multiple-auto-complete-configuration';
import { DmpProfileService } from '../../../../core/services/dmp/dmp-profile.service';
import { DmpProfileExternalAutocompleteCriteria } from '../../../../core/query/dmp/dmp-profile-external-autocomplete-criteria';
import { RequestItem } from '../../../../core/query/request-item';
import { DmpProfileField } from '../../../../core/model/dmp-profile/dmp-profile-field';

@Component({
	selector: 'app-dynamic-dmp-field-resolver',
	templateUrl: 'dynamic-dmp-field-resolver.component.html',
	styleUrls: ['./dynamic-dmp-field-resolver.component.scss']
})
export class DynamicDmpFieldResolverComponent implements OnInit, OnDestroy {


	dmpProfileFieldDataType = DmpProfileFieldDataType;
	dmpProfileTypeEnum = DmpProfileType;
	singleAutocompleteMap: { [id: string]: SingleAutoCompleteConfiguration; } = {};
	multiAutocompleteMap: { [id: string]: MultipleAutoCompleteConfiguration; } = {};

	@Input() dmpProfileId: string;
	@Input() dmpProfileDefinition: DmpProfileDefinition;
	@Input() formGroup: FormGroup;
	@Input() isUserOwner: boolean;

	constructor(
		private dmpProfileService: DmpProfileService
	) { }

	ngOnInit(): void {
		this.createControleFields();

		if (this.dmpProfileDefinition) {
			this.dmpProfileDefinition.fields.forEach(
				field => {
					if (field.externalAutocomplete) {
						if (field.externalAutocomplete.multiAutoComplete) {
							const multiConf: MultipleAutoCompleteConfiguration = {
								filterFn: this.externalAutocomplete.bind(this, field),
								initialItems: (extraData) => this.externalAutocomplete('', field.id),
								displayFn: (item) => item['label'],
								titleFn: (item) => item['label']
							}
							this.multiAutocompleteMap[field.id] = multiConf;
						} else {
							const singleConf: SingleAutoCompleteConfiguration = {
								filterFn: this.externalAutocomplete.bind(this, field),
								initialItems: (extraData) => this.externalAutocomplete('', field.id),
								displayFn: (item) => item['label'],
								titleFn: (item) => item['label']
							}
							this.singleAutocompleteMap[field.id] = singleConf;
						}
					}
				}
			);
		}
	}

	ngOnChanges(changes: SimpleChanges) {
		if (changes['dmpProfileDefinition'] && !changes['dmpProfileDefinition'].isFirstChange()) {
			this.createControleFields();
		}
	}

	createControleFields(): void {
		if (this.dmpProfileDefinition != null) {
			const diasableBoolean = this.formGroup.disabled;
			this.formGroup.addControl('properties', new FormBuilder().group([]));
			(<FormGroup>this.formGroup.get('properties')).addControl('fields', new FormBuilder().array([]));
			this.dmpProfileDefinition.fields.forEach(item => {
				(<FormArray>this.formGroup.get('properties').get('fields')).push(new FormBuilder().group({
					id: [{ value: item.id, disabled: diasableBoolean }],
					value: [{ value: item.value, disabled: diasableBoolean }]
				}));
			});
		}
		if (this.dmpProfileDefinition == null) {
			this.formGroup.removeControl('properties');
		}
	}

	ngOnDestroy(): void {
		this.formGroup.removeControl('properties');
	}

	externalAutocomplete(query: any, extFieldID: any) {
		const autocompleteRequestItem: RequestItem<DmpProfileExternalAutocompleteCriteria> = new RequestItem();
		autocompleteRequestItem.criteria = new DmpProfileExternalAutocompleteCriteria();

		if (typeof extFieldID == "string" && typeof query == "string") {
			autocompleteRequestItem.criteria.like = query;
			autocompleteRequestItem.criteria.profileID = this.dmpProfileId;
			autocompleteRequestItem.criteria.fieldID = extFieldID;
		} else {
			autocompleteRequestItem.criteria.like = extFieldID;
			autocompleteRequestItem.criteria.profileID = this.dmpProfileId;
			autocompleteRequestItem.criteria.fieldID = query.id;
		}

		return this.dmpProfileService.externalAutocomplete(autocompleteRequestItem);
	}
}
