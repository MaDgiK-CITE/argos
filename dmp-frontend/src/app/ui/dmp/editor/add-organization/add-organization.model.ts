import { FormBuilder, FormGroup } from '@angular/forms';
import { ResearcherModel } from '@app/core/model/researcher/researcher';
import { BackendErrorValidator } from '@common/forms/validation/custom-validator';
import { ValidationErrorModel } from '@common/forms/validation/error-model/validation-error-model';
import { ValidationContext } from '@common/forms/validation/validation-context';
import { OrganizationModel } from '@app/core/model/organisation/organization';

export class OrganizationEditorModel {
	public id: String;
	public name: String;
	public validationErrorModel: ValidationErrorModel = new ValidationErrorModel();
	public reference: string;

	fromModel(item: OrganizationModel): OrganizationEditorModel {
		this.id = item.id;
		this.name = item.name;
		this.reference = item.reference;
		return this;
	}

	buildForm(context: ValidationContext = null, disabled: boolean = false): FormGroup {
		if (context == null) { context = this.createValidationContext(); }
		const formGroup = new FormBuilder().group({
			reference: [{ value: this.reference, disabled: disabled }, context.getValidation('reference').validators],
			name: [{ value: this.name, disabled: disabled }, context.getValidation('name').validators]
		});

		return formGroup;
	}

	createValidationContext(): ValidationContext {
		const baseContext: ValidationContext = new ValidationContext();
		baseContext.validation.push({ key: 'name', validators: [BackendErrorValidator(this.validationErrorModel, 'name')] });
		baseContext.validation.push({ key: 'reference', validators: [BackendErrorValidator(this.validationErrorModel, 'reference')] });
		return baseContext;
	}
}
