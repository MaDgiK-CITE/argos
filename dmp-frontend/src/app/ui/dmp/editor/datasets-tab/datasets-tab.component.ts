import { Component, OnInit, Input } from '@angular/core';
import { DmpEditorModel } from '../dmp-editor.model';
import { Router } from '@angular/router';
import { DatasetOverviewModel } from '../../../../core/model/dataset/dataset-overview';
import { DatasetStatus } from '../../../../core/common/enum/dataset-status';

@Component({
	selector: 'app-datasets-tab',
	templateUrl: './datasets-tab.component.html',
	styleUrls: ['./datasets-tab.component.scss']
})
export class DatasetsTabComponent implements OnInit {

	@Input() dmp: DmpEditorModel;
	@Input() isPublic: boolean;
	@Input() isFinalized: boolean;

	constructor(
		private router: Router
	) { }

	ngOnInit() {
	}

	datasetClicked(datasetId: String) {
		this.router.navigate(this.isPublic ? ['/datasets/publicEdit/' + datasetId] : ['/datasets/edit/' + datasetId]);
	}

	datasetsClicked(dmpId: String) {
		this.router.navigate(['/datasets'], { queryParams: { dmpId: dmpId } });
	}

	isDraft(dataset: DatasetOverviewModel) {
		if (dataset.status == DatasetStatus.Draft) { return true }
		else { return false }
	}

	addDataset(rowId: String) {
		this.router.navigate(['/datasets/new/' + rowId]);
	}

}
