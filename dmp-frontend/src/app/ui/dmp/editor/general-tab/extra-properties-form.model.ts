import { ValidationContext } from '@common/forms/validation/validation-context';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BackendErrorValidator } from '@common/forms/validation/custom-validator';
import { CostModel } from '@app/core/model/dmp/cost';
import { isNullOrUndefined } from '@app/utilities/enhancers/utils';
import { CostEditorModel } from '../cost-editor/add-cost/add-cost.model';
import { ValidationErrorModel } from '@common/forms/validation/error-model/validation-error-model';

export class ExtraPropertiesFormModel {
	public language: string;
	public license: string;
	public visible: boolean;
	public publicDate: Date;
	public contact: string;
	public costs: CostEditorModel[] = [];
	public validationErrorModel: ValidationErrorModel = new ValidationErrorModel();

	fromModel(item: any): ExtraPropertiesFormModel {
		this.language = item.language;
		this.license = item.license;
		this.visible = item.visible;
		this.publicDate = item.publicDate;
		this.contact = item.contact;
		if (!isNullOrUndefined(item.costs)) {
			(<any[]>item.costs).forEach(element => {
				this.costs.push(new CostEditorModel().fromModel(element));
			});
		}
		return this;
	}

	buildForm(context: ValidationContext = null, disabled: boolean = false): FormGroup {
		if (context == null) { context = this.createValidationContext(); }
		const formBuilder = new FormBuilder();
		const formGroup = new FormBuilder().group({
			language: [{ value: this.language, disabled: disabled }, context.getValidation('language').validators],
			license: [{ value: this.license, disabled: disabled }, context.getValidation('license').validators],
			visible: [{ value: this.visible, disabled: disabled }, context.getValidation('visible').validators],
			publicDate: [{ value: this.publicDate, disabled: disabled }, context.getValidation('publicDate').validators],
			contact: [{ value: this.contact, disabled: disabled }, context.getValidation('contact').validators],
			// costs: [{ value: this.costs, disabled: disabled }, context.getValidation('costs').validators]
		});

		const costArray = new Array<FormGroup>();
		//if (this.externalDatasets && this.externalDatasets.length > 0) {
		this.costs.forEach(item => {
			costArray.push(item.buildForm(context.getValidation('costs').descendantValidations, true));
		});
		// } else {
		// 	//externalDatasetsFormArray.push(new ExternalDatasetModel().buildForm(context.getValidation('externalDatasets').descendantValidations, disabled));
		// }
		formGroup.addControl('costs', formBuilder.array(costArray));
		return formGroup;
	}

	createValidationContext(): ValidationContext {
		const baseContext: ValidationContext = new ValidationContext();
		baseContext.validation.push({ key: 'language', validators: [Validators.required, BackendErrorValidator(this.validationErrorModel, 'language')] });
		baseContext.validation.push({ key: 'license', validators: [BackendErrorValidator(this.validationErrorModel, 'license')] });
		baseContext.validation.push({ key: 'visible', validators: [BackendErrorValidator(this.validationErrorModel, 'visible')] });
		baseContext.validation.push({ key: 'publicDate', validators: [BackendErrorValidator(this.validationErrorModel, 'publicDate')] });
		baseContext.validation.push({ key: 'contact', validators: [Validators.required, BackendErrorValidator(this.validationErrorModel, 'contact')] });
		baseContext.validation.push({ key: 'costs', validators: [BackendErrorValidator(this.validationErrorModel, 'costs')] });
		return baseContext;
	}

}
