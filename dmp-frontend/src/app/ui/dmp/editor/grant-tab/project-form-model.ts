import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Status } from '@app/core/common/enum/status';
import { ProjectModel } from '@app/core/model/project/project';
import { BackendErrorValidator } from '@common/forms/validation/custom-validator';
import { ValidationErrorModel } from '@common/forms/validation/error-model/validation-error-model';
import { ValidationContext } from '@common/forms/validation/validation-context';


export class ProjectFormModel {
	public id: string;
	public label?: string;
	public status: Status = Status.Active;
	public description: String;
	public existProject: ProjectModel;
	public validationErrorModel: ValidationErrorModel = new ValidationErrorModel();
	public reference: string;


	fromModel(item: ProjectModel): ProjectFormModel {
		this.existProject = item;
		return this;
	}

	buildForm(context: ValidationContext = null, disabled: boolean = false): FormGroup {
		if (context == null) { context = this.createValidationContext(); }

		const formGroup = new FormBuilder().group({
			id: [{ value: this.id, disabled: disabled }, context.getValidation('id').validators],
			label: [{ value: this.label, disabled: disabled }, context.getValidation('label').validators],
			status: [{ value: this.status, disabled: disabled }, context.getValidation('status').validators],
			description: [{ value: this.description, disabled: disabled }, context.getValidation('description').validators],
			existProject: [{ value: this.existProject, disabled: disabled }, context.getValidation('existProject').validators],
			reference: [{ value: this.reference, disabled: disabled }, context.getValidation('reference').validators],
		});
		return formGroup;
	}

	createValidationContext(): ValidationContext {
		const baseContext: ValidationContext = new ValidationContext();
		baseContext.validation.push({ key: 'id', validators: [] });
		baseContext.validation.push({ key: 'label', validators: [BackendErrorValidator(this.validationErrorModel, 'label')] });
		baseContext.validation.push({ key: 'status', validators: [] });
		baseContext.validation.push({ key: 'description', validators: [BackendErrorValidator(this.validationErrorModel, 'description')] });
		baseContext.validation.push({ key: 'existProject', validators: [BackendErrorValidator(this.validationErrorModel, 'existProject')] });
		baseContext.validation.push({ key: 'reference', validators: [BackendErrorValidator(this.validationErrorModel, 'reference')] });
		return baseContext;
	}

}
