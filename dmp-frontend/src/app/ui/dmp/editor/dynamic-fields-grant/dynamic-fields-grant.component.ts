import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { DmpDynamicFieldDependency } from '../../../../core/model/dmp/dmp-dynamic-field-dependency';

@Component({
	selector: 'app-dynamic-fields-grant',
	templateUrl: 'dynamic-fields-grant.component.html',
	styleUrls: ['./dynamic-fields-grant.component.scss']
})
export class DynamicFieldsGrantComponent implements OnInit {

	@Input()
	formGroup: FormGroup;

	ngOnInit(): void {
	}

	findDependencies(id: number) {
		const formGroupDependencies: Array<FormGroup> = new Array<FormGroup>();
		const dynamicFieldDependency: DmpDynamicFieldDependency[] = (<FormArray>this.formGroup.get('dynamicFields')).at(id).get('dependencies').value;
		if (dynamicFieldDependency.length > 0) {
			dynamicFieldDependency.forEach(item => {
				const length = (<FormArray>this.formGroup.get('dynamicFields')).length;
				for (let i = 0; i < length; i++) {
					const formGroup = (<FormArray>this.formGroup.get('dynamicFields')).at(i);
					if (formGroup.get('id').value === item.id) { formGroupDependencies.push(<FormGroup>formGroup); }
				}
			});
		}
		return formGroupDependencies;
	}
}
