import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { RequestItem } from '../../../../../core/query/request-item';
import { DmpService } from '../../../../../core/services/dmp/dmp.service';
import { SingleAutoCompleteConfiguration } from '../../../../../library/auto-complete/single/single-auto-complete-configuration';
import { DynamicFieldGrantCriteria, DynamicFieldGrantCriteriaDependencies } from '../../../../../models/dynamic-field-grant/DynamicFieldGrantCriteria';

@Component({
	selector: 'app-dynamic-field-grant',
	templateUrl: 'dynamic-field-grant.component.html',
	styleUrls: ['./dynamic-field-grant.component.scss']
})
export class DynamicFieldGrantComponent implements OnInit {

	constructor(
		private dmpService: DmpService
	) { }

	@Input()dependencies: Array<FormGroup>;

	@Input()formGroup: FormGroup;

	autoCompleteConfiguration: SingleAutoCompleteConfiguration;

	ngOnInit(): void {

		this.autoCompleteConfiguration = {
			filterFn: this.searchDynamicField.bind(this),
			initialItems: (extraData) => this.searchDynamicField(''),
			displayFn: (item) => item['label'],
			titleFn: (item) => item['label']
		};

	}

	searchDynamicField(query: string) {
		const requestItem = new RequestItem<DynamicFieldGrantCriteria>();
		requestItem.criteria = { id: this.formGroup.get('id').value, dynamicFields: this.buildDependencies() };
		return this.dmpService.getDynamicField(requestItem);
	}

	hasUnResolvedDependencies() {
		if (this.dependencies == null || this.dependencies.length === 0) {
			if (this.formGroup.get('value').disabled) {
				this.updateConfiguration();
				this.formGroup.get('value').enable({ onlySelf: true, emitEvent: false });
			}
			return false;
		}
		for (let i = 0; i < this.dependencies.length; i++) {
			if (!this.dependencies[i].get('value').value) {
				this.formGroup.get('value').disable({ onlySelf: true, emitEvent: false });
				return true;
			}
		}
		if (this.formGroup.get('value').disabled) {
			this.updateConfiguration();
			this.formGroup.get('value').enable({ onlySelf: true, emitEvent: false });
		}
		return false;
	}

	updateConfiguration() {
		// const requestItem = new RequestItem<DynamicFieldGrantCriteria>();
		// requestItem.criteria = { id: this.formGroup.get('id').value, dynamicFields: this.buildDependencies() };
		// this.autoCompleteConfiguration = new AutoCompleteConfiguration(this.dmpService.getDynamicField.bind(this.dmpService), requestItem);
		// this.autocomplete.inputData = this.autoCompleteConfiguration;
	}

	buildDependencies(): Array<DynamicFieldGrantCriteriaDependencies> {
		if (!this.dependencies || this.dependencies.length === 0) { return []; }
		const dependencies = new Array<DynamicFieldGrantCriteriaDependencies>();
		for (let i = 0; i < this.dependencies.length; i++) {
			dependencies.push({ property: this.dependencies[i].get('id').value, value: this.assignFunction(this.dependencies[i].get('value').value) });
		}
		return dependencies;
	}

	assignFunction(item: any): any {
		if (!item) { return null; }
		return item['id'];
	}
}
