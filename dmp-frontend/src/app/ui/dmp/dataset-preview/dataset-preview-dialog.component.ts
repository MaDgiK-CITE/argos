import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DatasetWizardService } from '@app/core/services/dataset-wizard/dataset-wizard.service';
import { ProgressIndicationService } from '@app/core/services/progress-indication/progress-indication-service';
import { DatasetDescriptionFormEditorModel } from '@app/ui/misc/dataset-description-form/dataset-description-form.model';
import { BaseComponent } from '@common/base/base.component';
import { takeUntil } from 'rxjs/operators';

@Component({
	selector: 'app-dataset-preview-dialog-component',
	templateUrl: 'dataset-preview-dialog.component.html',
	styleUrls: ['./dataset-preview-dialog.component.scss'],
})
export class DatasetPreviewDialogComponent extends BaseComponent implements OnInit {

	datasetProfileDefinitionModel: DatasetDescriptionFormEditorModel;
	datasetProfileDefinitionFormGroup: FormGroup;
	progressIndication = false;

	constructor(
		public dialogRef: MatDialogRef<DatasetPreviewDialogComponent>,
		private datasetWizardService: DatasetWizardService,
		private progressIndicationService: ProgressIndicationService,
		@Inject(MAT_DIALOG_DATA) public data: any
	) {
		super();
	}

	ngOnInit(): void {
		this.progressIndicationService.getProgressIndicationObservable().pipe(takeUntil(this._destroyed)).subscribe(x => {
			setTimeout(() => { this.progressIndication = x; });
		});

		if (this.data && this.data.template) {
			this.datasetWizardService.getDefinition(this.data.template.id)
				.pipe(takeUntil(this._destroyed))
				.subscribe(item => {
					this.datasetProfileDefinitionModel = new DatasetDescriptionFormEditorModel().fromModel(item);
					this.datasetProfileDefinitionFormGroup = this.datasetProfileDefinitionModel.buildForm();
					this.datasetProfileDefinitionFormGroup.disable();
				});
		}
	}

	select(): void {
		this.dialogRef.close(true);
	}

	closeDialog(): void {
		this.dialogRef.close();
	}

}
