import { SelectionModel } from '@angular/cdk/collections';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { DataTableRequest } from '@app/core/model/data-table/data-table-request';
import { DatasetListingModel } from '@app/core/model/dataset/dataset-listing';
import { DatasetCriteria } from '@app/core/query/dataset/dataset-criteria';
import { DatasetService } from '@app/core/services/dataset/dataset.service';
import { DmpService } from '@app/core/services/dmp/dmp.service';
import { DatasetCriteriaComponent } from '@app/ui/dataset/listing/criteria/dataset-criteria.component';
import { BaseComponent } from '@common/base/base.component';
import { TranslateService } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/operators';

@Component({
	selector: 'app-dmp-wizard-dataset-listing-component',
	templateUrl: 'dmp-wizard-dataset-listing.component.html',
	styleUrls: ['./dmp-wizard-dataset-listing.component.scss'],
})
export class DmpWizardDatasetListingComponent extends BaseComponent implements OnInit {

	@ViewChild(MatPaginator) _paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;
	@ViewChild(DatasetCriteriaComponent) criteria: DatasetCriteriaComponent;


	titlePrefix: String;
	datasets: DatasetListingModel[] = [];

	@Input() dmpId: string;
	@Input() formGroup: FormGroup;
	selection = new SelectionModel<Element>(true, []);

	statuses = [
		{ value: '0', viewValue: 'Active' },
		{ value: '1', viewValue: 'Inactive' }
	];

	constructor(
		private datasetService: DatasetService,
		private router: Router,
		private languageService: TranslateService,
		public snackBar: MatSnackBar,
		public route: ActivatedRoute,
		public dmpService: DmpService
	) {
		super();
	}


	ngOnInit() {
		this.route.params
			.pipe(takeUntil(this._destroyed))
			.subscribe((params: Params) => {
				if (this.dmpId != null) {
					if (params['dmpLabel'] !== undefined) {
						this.titlePrefix = 'for ' + params['dmpLabel'];
					}
				}
			});
		const request: DataTableRequest<DatasetCriteria> = new DataTableRequest<DatasetCriteria>(null, null, null);
		request.criteria = new DatasetCriteria();
		request.criteria.dmpIds = [this.dmpId];
		request.criteria.allVersions = true;
		this.datasetService.getPaged(request)
			.pipe(takeUntil(this._destroyed))
			.subscribe(items => {
				this.datasets = items.data;
			});
		this.formGroup.addControl('datasets', new FormBuilder().array(new Array<FormControl>()));
	}

	selectionChanged(event, selectedItems) {
		this.formGroup.removeControl('datasets');
		this.formGroup.addControl('datasets', new FormBuilder().array(new Array<FormControl>()));
		selectedItems.selectedOptions.selected.forEach(element => {
			(<FormArray>this.formGroup.get('datasets')).push(new FormBuilder().group({ id: element.value }));
		});
	}
}
