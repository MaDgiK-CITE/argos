import { NgModule } from '@angular/core';
import { FormattingModule } from '@app/core/formatting.module';
import { AutoCompleteModule } from '@app/library/auto-complete/auto-complete.module';
import { ConfirmationDialogModule } from '@common/modules/confirmation-dialog/confirmation-dialog.module';
import { ExportMethodDialogModule } from '@app/library/export-method-dialog/export-method-dialog.module';
import { UrlListingModule } from '@app/library/url-listing/url-listing.module';
import { DmpOverviewComponent } from '@app/ui/dmp/overview/dmp-overview.component';
import { CommonFormsModule } from '@common/forms/common-forms.module';
import { CommonUiModule } from '@common/ui/common-ui.module';
import { RouterModule } from '@angular/router';
import { CloneDialogModule } from '../clone/clone-dialog/clone-dialog.module';
import { NgDialogAnimationService } from 'ng-dialog-animation';
import {DmpModule} from "@app/ui/dmp/dmp.module";
import {DmpDepositDropdown} from "@app/ui/dmp/editor/dmp-deposit-dropdown/dmp-deposit-dropdown.component";

@NgModule({
  imports: [
    CommonUiModule,
    CommonFormsModule,
    UrlListingModule,
    ConfirmationDialogModule,
    CloneDialogModule,
    ExportMethodDialogModule,
    FormattingModule,
    AutoCompleteModule,
    RouterModule
  ],
	declarations: [
		DmpOverviewComponent,
		DmpDepositDropdown
	],
	providers: [
		NgDialogAnimationService
	]
})
export class DmpOverviewModule { }
