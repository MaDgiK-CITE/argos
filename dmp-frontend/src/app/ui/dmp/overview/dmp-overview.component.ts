import { Location } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { DatasetStatus } from '@app/core/common/enum/dataset-status';
import { DmpStatus } from '@app/core/common/enum/dmp-status';
import { Role } from "@app/core/common/enum/role";
import { Principal } from '@app/core/model/auth/principal';
import { DatasetOverviewModel } from '@app/core/model/dataset/dataset-overview';
import { DatasetsToBeFinalized } from '@app/core/model/dataset/datasets-toBeFinalized';
import { DepositConfigurationModel } from '@app/core/model/deposit/deposit-configuration';
import { DmpModel } from '@app/core/model/dmp/dmp';
import { DmpBlueprintDefinition, SystemFieldType } from '@app/core/model/dmp/dmp-blueprint/dmp-blueprint';
import { DmpOverviewModel } from '@app/core/model/dmp/dmp-overview';
import { DoiModel } from '@app/core/model/doi/doi';
import { UserInfoListingModel } from '@app/core/model/user/user-info-listing';
import { VersionListingModel } from '@app/core/model/version/version-listing.model';
import { AuthService } from '@app/core/services/auth/auth.service';
import { ConfigurationService } from '@app/core/services/configuration/configuration.service';
import { DepositRepositoriesService } from '@app/core/services/deposit-repositories/deposit-repositories.service';
import { DmpProfileService } from '@app/core/services/dmp/dmp-profile.service';
import { DmpService } from '@app/core/services/dmp/dmp.service';
import { LockService } from '@app/core/services/lock/lock.service';
import { MatomoService } from '@app/core/services/matomo/matomo-service';
import {
	SnackBarNotificationLevel,
	UiNotificationService
} from '@app/core/services/notification/ui-notification-service';
import { PopupNotificationDialogComponent } from '@app/library/notification/popup/popup-notification.component';
import {
	DmpFinalizeDialogComponent,
	DmpFinalizeDialogInput,
	DmpFinalizeDialogOutput
} from '@app/ui/dmp/editor/dmp-finalize-dialog/dmp-finalize-dialog.component';
import { BreadcrumbItem } from '@app/ui/misc/breadcrumb/definition/breadcrumb-item';
import { isNullOrUndefined } from '@app/utilities/enhancers/utils';
import { BaseComponent } from '@common/base/base.component';
import { ConfirmationDialogComponent } from '@common/modules/confirmation-dialog/confirmation-dialog.component';
import { TranslateService } from '@ngx-translate/core';
import * as FileSaver from 'file-saver';
import { Observable, of as observableOf } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { CloneDialogComponent } from '../clone/clone-dialog/clone-dialog.component';
import { DmpEditorModel } from '../editor/dmp-editor.model';
import { ExtraPropertiesFormModel } from '../editor/general-tab/extra-properties-form.model';
import { FunderFormModel } from '../editor/grant-tab/funder-form-model';
import { GrantTabModel } from '../editor/grant-tab/grant-tab-model';
import { ProjectFormModel } from '../editor/grant-tab/project-form-model';
import { DmpInvitationDialogComponent } from '../invitation/dmp-invitation-dialog.component';
import { StartNewDmpDialogComponent } from '../start-new-dmp-dialogue/start-new-dmp-dialog.component';

@Component({
	selector: 'app-dmp-overview',
	templateUrl: './dmp-overview.component.html',
	styleUrls: ['./dmp-overview.component.scss']
})
export class DmpOverviewComponent extends BaseComponent implements OnInit {

	dmp: DmpOverviewModel;
	dmpModel: DmpEditorModel;
	isNew = true;
	isFinalized = false;
	isPublicView = true;
	hasPublishButton: boolean = true;
	breadCrumbs: Observable<BreadcrumbItem[]> = observableOf();
	isUserOwner: boolean;
	expand = false;
	lockStatus: Boolean;
	textMessage: any;
	versions: VersionListingModel[];
	version: VersionListingModel;
	selectedModel: DoiModel;

	@ViewChild('doi')
	doi: ElementRef;

	depositRepos: DepositConfigurationModel[] = [];

	formGroup: FormGroup;

	constructor(
		private route: ActivatedRoute,
		private router: Router,
		private dmpService: DmpService,
		private dmpProfileService: DmpProfileService,
		private depositRepositoriesService: DepositRepositoriesService,
		private translate: TranslateService,
		private authentication: AuthService,
		private dialog: MatDialog,
		private language: TranslateService,
		private uiNotificationService: UiNotificationService,
		private configurationService: ConfigurationService,
		private location: Location,
		private lockService: LockService,
		private matomoService: MatomoService
	) {
		super();
	}

	ngOnInit() {
		this.matomoService.trackPageView('DMP Overview');
		// Gets dmp data using parameter id
		this.route.params
			.pipe(takeUntil(this._destroyed))
			.subscribe((params: Params) => {
				const itemId = params['id'];
				const publicId = params['publicId'];
				if (itemId != null) {
					this.isNew = false;
					this.isPublicView = false;
					this.dmpService.getOverviewSingle(itemId)
						.pipe(takeUntil(this._destroyed))
						.subscribe(data => {
							this.dmp = data;
							if (!this.hasDoi()) {
								this.selectedModel = this.dmp.dois[0];
							}
							this.checkLockStatus(this.dmp.id);
							this.setIsUserOwner();
							this.getAllVersions(this.dmp);
							const breadCrumbs = [];
							breadCrumbs.push({ parentComponentName: null, label: this.language.instant('NAV-BAR.MY-DMPS'), url: "/plans" });
							breadCrumbs.push({ parentComponentName: 'DmpListingComponent', label: this.dmp.label, url: '/plans/overview/' + this.dmp.id });
							this.breadCrumbs = observableOf(breadCrumbs);
						}, (error: any) => {
							if (error.status === 404) {
								return this.onFetchingDeletedCallbackError('/plans/');
							}
							if (error.status === 403) {
								return this.onFetchingForbiddenCallbackError('/plans/');
							}
						});
				}
				else if (publicId != null) {
					this.isNew = false;
					this.isFinalized = true;
					this.isPublicView = true;
					this.dmpService.getOverviewSinglePublic(publicId)
						.pipe(takeUntil(this._destroyed))
						.subscribe(data => {
							this.dmp = data;
							if (!this.hasDoi()) {
								this.selectedModel = this.dmp.dois[0];
							}
							// this.checkLockStatus(this.dmp.id);
							this.getAllVersions(this.dmp);
							const breadCrumbs = [];
							breadCrumbs.push({ parentComponentName: null, label: this.language.instant('NAV-BAR.PUBLIC-DMPS'), url: "/explore-plans" });
							breadCrumbs.push({ parentComponentName: 'DmpListingComponent', label: this.dmp.label, url: '/plans/publicOverview/' + this.dmp.id });
							this.breadCrumbs = observableOf(breadCrumbs);
						}, (error: any) => {
							if (error.status === 404) {
								return this.onFetchingDeletedCallbackError('/explore-plans');
							}
							if (error.status === 403) {
								return this.onFetchingForbiddenCallbackError('/explore-plans');
							}
						});
				}
			});
		this.depositRepositoriesService.getAvailableRepos()
			.pipe(takeUntil(this._destroyed))
			.subscribe(
				repos => {
					this.depositRepos = repos;
				},
				error => this.depositRepos = []);
	}

	onFetchingDeletedCallbackError(redirectRoot: string) {
		this.uiNotificationService.snackBarNotification(this.language.instant('DMP-OVERVIEW.ERROR.DELETED-DMP'), SnackBarNotificationLevel.Error);
		this.router.navigate([redirectRoot]);
	}

	onFetchingForbiddenCallbackError(redirectRoot: string) {
		this.uiNotificationService.snackBarNotification(this.language.instant('DMP-OVERVIEW.ERROR.FORBIDEN-DMP'), SnackBarNotificationLevel.Error);
		this.router.navigate([redirectRoot]);
	}

	setIsUserOwner() {
		if (this.dmp) {
			const principal: Principal = this.authentication.current();
			if (principal) this.isUserOwner = !!this.dmp.users.find(x => (x.role === Role.Owner) && (principal.id === x.id));
		}
	}

	isUserAuthor(userId: string): boolean {
		if (this.isAuthenticated()) {
			const principal: Principal = this.authentication.current();
			return userId === principal.id;
		} else return false;
	}

	editClicked(dmp: DmpOverviewModel) {
		this.router.navigate(['/plans/edit/', dmp.id]);
		// let url = this.router.createUrlTree(['/plans/edit/', dmp.id]);
		// window.open(url.toString(), '_blank');
	}

	cloneOrNewVersionClicked(dmp: DmpOverviewModel, isNewVersion: boolean) {
		this.dmpService.getSingle(this.dmp.id).pipe(map(data => data as DmpModel))
			.pipe(takeUntil(this._destroyed))
			.subscribe(data => {
				this.dmpModel = new DmpEditorModel();
				this.dmpModel.grant = new GrantTabModel();
				this.dmpModel.project = new ProjectFormModel();
				this.dmpModel.funder = new FunderFormModel();
				this.dmpModel.extraProperties = new ExtraPropertiesFormModel();
				this.dmpModel.fromModel(data);
				this.dmpModel.status = DmpStatus.Draft;
				this.formGroup = this.dmpModel.buildForm();

				if (!isNullOrUndefined(this.formGroup.get('profile').value)) {
					this.dmpProfileService.getSingleBlueprint(this.formGroup.get('profile').value)
						.pipe(takeUntil(this._destroyed))
						.subscribe(result => {
							this.checkForGrant(result.definition);
							this.checkForFunder(result.definition);
							this.checkForProject(result.definition);
						});
				}

				if (!isNewVersion) {
					this.formGroup.get('label').setValue(this.dmp.label + " New");
				}
				this.openCloneDialog(isNewVersion);
			});
	}

	private checkForGrant(blueprint: DmpBlueprintDefinition) {
		let hasGrant = false;
		blueprint.sections.forEach(section => section.fields.forEach(
			field => {
				if (field.category as unknown === 'SYSTEM' && field.type === SystemFieldType.GRANT) {
					hasGrant = true;
				}
			}
		));
		if (!hasGrant) {
			this.formGroup.removeControl('grant');
		}
	}

	private checkForFunder(blueprint: DmpBlueprintDefinition) {
		let hasFunder = false;
		blueprint.sections.forEach(section => section.fields.forEach(
			field => {
				if (field.category as unknown === 'SYSTEM' && field.type === SystemFieldType.FUNDER) {
					hasFunder = true;
				}
			}
		));
		if (!hasFunder) {
			this.formGroup.removeControl('funder');
		}
	}

	private checkForProject(blueprint: DmpBlueprintDefinition) {
		let hasProject = false;
		blueprint.sections.forEach(section => section.fields.forEach(
			field => {
				if (field.category as unknown === 'SYSTEM' && field.type === SystemFieldType.PROJECT) {
					hasProject = true;
				}
			}
		));
		if (!hasProject) {
			this.formGroup.removeControl('project');
		}
	}

	openCloneDialog(isNewVersion: boolean) {
		const dialogRef = this.dialog.open(CloneDialogComponent, {
			maxWidth: '900px',
			maxHeight: '80vh',
			data: {
				formGroup: this.formGroup,
				datasets: this.dmp.datasets,
				isNewVersion: isNewVersion,
				confirmButton: this.language.instant('DMP-EDITOR.CLONE-DIALOG.SAVE'),
				cancelButton: this.language.instant('DMP-EDITOR.CLONE-DIALOG.CANCEL'),
			}
		});
		dialogRef.afterClosed().pipe(takeUntil(this._destroyed)).subscribe(result => {
			if (result) {
				if (!isNewVersion) {
					this.dmpService.clone(this.formGroup.getRawValue(), this.dmp.id)
						.pipe(takeUntil(this._destroyed))
						.subscribe(
							complete => this.onCallbackSuccess(complete),
							error => this.onCallbackError(error)
						);
				} else if (isNewVersion) {
					this.dmpService.newVersion(this.formGroup.getRawValue(), this.dmp.id)
						.pipe(takeUntil(this._destroyed))
						.subscribe(
							complete => this.onCallbackSuccess(complete),
							error => this.onCallbackError(error)
						);
				}
			}
		});
	}

	onCallbackSuccess(dmpId: String): void {
		this.uiNotificationService.snackBarNotification(this.language.instant('GENERAL.SNACK-BAR.SUCCESSFUL-UPDATE'), SnackBarNotificationLevel.Success);
		this.router.navigate(['/plans/edit/', dmpId]);
	}

	onCallbackError(error: any) {
		this.uiNotificationService.snackBarNotification(error.error.message ? error.error.message : this.language.instant('GENERAL.SNACK-BAR.UNSUCCESSFUL-UPDATE'), SnackBarNotificationLevel.Error);
	}

	grantClicked(grantId: String) {
		// ----------- UNCOMMENT TO ADD AGAIN GRANTS --------
		// this.router.navigate(['/grants/edit/' + grantId]);
	}

	expandDesc() {
		this.expand = !this.expand;
	}

	checkOverflow(element) {
		if (this.expand || (element.offsetHeight < element.scrollHeight ||
			element.offsetWidth < element.scrollWidth)) {
			return true;
		} else {
			return false;
		}
	}

	datasetClicked(datasetId: String) {
		// if (this.isPublicView) {
		// 	this.router.navigate(['/datasets/publicEdit/' + datasetId]);
		// } else {
		// 	this.router.navigate(['/datasets/edit/' + datasetId]);
		// }
		this.router.navigate(['/datasets/overview/' + datasetId]);
	}

	datasetsClicked(dmpId: String) {
		if (!this.isFinalized)
			this.router.navigate(['/plans/edit/' + dmpId], { queryParams: { tab: "datasetDescriptions" } });
		else
			this.router.navigate(['/explore'], { queryParams: { dmpId: dmpId } });
	}

	goToUri(uri: string) {
		window.open(uri, "_blank");
	}

	deleteClicked() {
		const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
			maxWidth: '300px',
			data: {
				message: this.language.instant('GENERAL.CONFIRMATION-DIALOG.DELETE-ITEM'),
				confirmButton: this.language.instant('GENERAL.CONFIRMATION-DIALOG.ACTIONS.DELETE'),
				cancelButton: this.language.instant('GENERAL.CONFIRMATION-DIALOG.ACTIONS.CANCEL'),
				isDeleteConfirmation: true
			}
		});
		dialogRef.afterClosed().pipe(takeUntil(this._destroyed)).subscribe(result => {
			if (result) {
				this.dmpService.delete(this.dmp.id)
					.pipe(takeUntil(this._destroyed))
					.subscribe(
						complete => { this.onDeleteCallbackSuccess() },
						error => this.onDeleteCallbackError(error)
					);
			}
		});
	}

	onDeleteCallbackSuccess(): void {
		this.uiNotificationService.snackBarNotification(this.language.instant('GENERAL.SNACK-BAR.SUCCESSFUL-DELETE'), SnackBarNotificationLevel.Success);
		this.router.navigate(['/plans']);
	}

	onDeleteCallbackError(error) {
		this.uiNotificationService.snackBarNotification(error.error.message ? this.language.instant(error.error.message) : this.language.instant('GENERAL.SNACK-BAR.UNSUCCESSFUL-DELETE'), SnackBarNotificationLevel.Error);
	}

	onUpdateCallbackSuccess(): void {
		this.uiNotificationService.snackBarNotification(this.language.instant('GENERAL.SNACK-BAR.SUCCESSFUL-UPDATE'), SnackBarNotificationLevel.Success);
		this.reloadPage();
	}

	onUpdateCallbackError(error) {
		this.uiNotificationService.snackBarNotification(error.error.message ? error.error.message : this.language.instant('DATASET-UPLOAD.SNACK-BAR.UNSUCCESSFUL'), SnackBarNotificationLevel.Error);
	}

	downloadXml(id: string) {
		this.dmpService.downloadXML(id)
			.pipe(takeUntil(this._destroyed))
			.subscribe(response => {
				const blob = new Blob([response.body], { type: 'application/xml' });
				const filename = this.getFilenameFromContentDispositionHeader(response.headers.get('Content-Disposition'));

				FileSaver.saveAs(blob, filename);
				this.matomoService.trackDownload('dmps', "xml", id);
			});
	}

	downloadDocx(id: string) {
		this.dmpService.downloadDocx(id)
			.pipe(takeUntil(this._destroyed))
			.subscribe(response => {
				const blob = new Blob([response.body], { type: 'application/msword' });
				const filename = this.getFilenameFromContentDispositionHeader(response.headers.get('Content-Disposition'));

				FileSaver.saveAs(blob, filename);
				this.matomoService.trackDownload('dmps', "docx", id);
			});
	}

	downloadPDF(id: string) {
		this.dmpService.downloadPDF(id)
			.pipe(takeUntil(this._destroyed))
			.subscribe(response => {
				const blob = new Blob([response.body], { type: 'application/pdf' });
				const filename = this.getFilenameFromContentDispositionHeader(response.headers.get('Content-Disposition'));

				FileSaver.saveAs(blob, filename);
				this.matomoService.trackDownload('dmps', "pdf", id);
			});
	}

	downloadJson(id: string) {
		this.dmpService.downloadJson(id)
			.pipe(takeUntil(this._destroyed))
			.subscribe(complete => {
				const blob = new Blob([complete.body], { type: 'application/json' });
				const filename = this.getFilenameFromContentDispositionHeader(complete.headers.get('Content-Disposition'));
				FileSaver.saveAs(blob, filename);
				this.matomoService.trackDownload('dmps', "json", id);
			}, async error => {
				this.onExportCallbackError(error);
			});
	}

	async onExportCallbackError(error: any) {
		const errorJsonText = await error.error.text();
		const errorObj = JSON.parse(errorJsonText);
		this.uiNotificationService.snackBarNotification(errorObj.message, SnackBarNotificationLevel.Error);
	}

	getFilenameFromContentDispositionHeader(header: string): string {
		const regex: RegExp = new RegExp(/filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/g);

		const matches = header.match(regex);
		let filename: string;
		for (let i = 0; i < matches.length; i++) {
			const match = matches[i];
			if (match.includes('filename="')) {
				filename = match.substring(10, match.length - 1);
				break;
			} else if (match.includes('filename=')) {
				filename = match.substring(9);
				break;
			}
		}
		return filename;
	}

	roleDisplayFromList(value: UserInfoListingModel[]) {
		const principal: Principal = this.authentication.current();
		let role: number;
		if (principal) {
			value.forEach(element => {
				if (principal.id === element.id) {
					role = element.role;
				}
			});
		}
		if (role === Role.Owner) {
			return this.translate.instant('DMP-LISTING.OWNER');
		} else if (role === Role.Member) {
			return this.translate.instant('DMP-LISTING.MEMBER');
		} else {
			return this.translate.instant('DMP-LISTING.OWNER');
		}
	}

	isUserDMPRelated() {
		const principal: Principal = this.authentication.current();
		let isRelated: boolean = false;
		if (this.dmp && principal) {
			this.dmp.users.forEach(element => {
				if (element.id === principal.id) {
					isRelated = true;
				}
			})
		}
		return isRelated;
	}

	roleDisplay(value: UserInfoListingModel) {
		if (value.role === Role.Owner) {
			return this.translate.instant('DMP-LISTING.OWNER');
		} else if (value.role === Role.Member) {
			return this.translate.instant('DMP-LISTING.MEMBER');
		} else {
			return this.translate.instant('DMP-LISTING.OWNER');
		}
	}

	isDraftDataset(dataset: DatasetOverviewModel) {
		return dataset.status == DatasetStatus.Draft;
	}

	isDraftDmp(dmp: DmpOverviewModel) {
		return dmp.status == DmpStatus.Draft;
	}

	isFinalizedDmp(dmp: DmpOverviewModel) {
		return dmp.status == DmpStatus.Finalized;
	}

	isPublishedDMP(dmp: DmpOverviewModel) {
		return (dmp.status == DmpStatus.Finalized && dmp.isPublic);
	}

	hasDoi(dmp: DmpOverviewModel = null) {
		return (this.dmp.dois == null || this.dmp.dois.length == 0);
	}

	getAllVersions(dmp: DmpOverviewModel) {
		this.dmpService.getAllVersions(dmp.groupId, this.isPublicView)
			.pipe(takeUntil(this._destroyed))
			.subscribe(items => {
				this.versions = items;
			});
	}

	showPublishButton(dmp: DmpOverviewModel) {
		return this.isFinalizedDmp(dmp) && !dmp.isPublic && this.hasPublishButton;
	}

	publish(dmpId: String) {
		const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
			maxWidth: '500px',
			restoreFocus: false,
			data: {
				message: this.language.instant('GENERAL.CONFIRMATION-DIALOG.PUBLISH-ITEM'),
				privacyPolicyNames: this.language.instant('GENERAL.CONFIRMATION-DIALOG.PRIVACY-POLICY-NAMES'),
				confirmButton: this.language.instant('GENERAL.CONFIRMATION-DIALOG.ACTIONS.CONFIRM'),
				cancelButton: this.language.instant('GENERAL.CONFIRMATION-DIALOG.ACTIONS.CANCEL'),
				isDeleteConfirmation: false
			}
		});
		dialogRef.afterClosed().pipe(takeUntil(this._destroyed)).subscribe(result => {
			if (result) {
				this.dmpService.publish(dmpId)
					.pipe(takeUntil(this._destroyed))
					.subscribe(() => {
						this.hasPublishButton = false;
						this.reloadPage();
					});
			}
		});
	}

	afterDeposit(result: DoiModel[]) {
		if (result.length > 0) {
			this.dmp.dois = result;
			this.selectedModel = this.dmp.dois[this.dmp.dois.length - 1];
		}
	}

	get inputRepos() {
		return this.depositRepos.filter(repo => !this.dmp.dois.find(doi => doi.repositoryId === repo.repositoryId));
	}

	moreDeposit() {
		return (this.dmp.dois.length < this.depositRepos.length);
	}

	finalize(dmp: DmpOverviewModel) {

		const extraProperties = new ExtraPropertiesFormModel();
		this.dmpService.getSingle(this.dmp.id).pipe(map(data => data as DmpModel))
			.pipe(takeUntil(this._destroyed))
			.subscribe(data => {

				if (!isNullOrUndefined(data.extraProperties)) {
					extraProperties.fromModel(data.extraProperties);
				}
				const dialogInputModel: DmpFinalizeDialogInput = {
					dmpLabel: this.dmp.label,
					dmpDescription: this.dmp.description,
					datasets: this.dmp.datasets.map(x => {
						return { label: x.label, id: x.id, status: x.status }
					}),
					accessRights: extraProperties.visible
				}

				const dialogRef = this.dialog.open(DmpFinalizeDialogComponent, {
					maxWidth: '500px',
					restoreFocus: false,
					autoFocus: false,
					data: {
						dialogInputModel: dialogInputModel,
						message: this.language.instant('GENERAL.CONFIRMATION-DIALOG.FINALIZE-ITEM'),
						confirmButton: this.language.instant('DMP-FINALISE-DIALOG.SUBMIT'),
						cancelButton: this.language.instant('GENERAL.CONFIRMATION-DIALOG.ACTIONS.CANCEL'),
					}
				});
				dialogRef.afterClosed().pipe(takeUntil(this._destroyed)).subscribe((result: DmpFinalizeDialogOutput) => {
					if (result && !result.cancelled) {
						this.checkIfAnyProfileIsUsedLessThanMin(data).subscribe(checked => {
							if (!checked) {
								var datasetsToBeFinalized: DatasetsToBeFinalized = {
									uuids: result.datasetsToBeFinalized
								};
								this.dmpService.finalize(datasetsToBeFinalized, this.dmp.id)
									.pipe(takeUntil(this._destroyed))
									.subscribe(
										complete => {
											if (extraProperties.visible) {
												//this.publish(this.dmp.id);
												this.dmpService.publish(this.dmp.id)
													.pipe(takeUntil(this._destroyed))
													.subscribe(() => {
														//this.hasPublishButton = false;
														this.dmp.status = DmpStatus.Finalized;
														this.onUpdateCallbackSuccess();
													});
											}
											else {
												this.dmp.status = DmpStatus.Finalized;
												this.onUpdateCallbackSuccess();
											}
										},
										error => this.onUpdateCallbackError(error)
									);
							}
						})
					}
				});

			});

	}

	private checkIfAnyProfileIsUsedLessThanMin(dmpModel: DmpModel): Observable<boolean> {
		const blueprintId = dmpModel.profile.id;
		return this.dmpProfileService.getSingleBlueprint(blueprintId)
			.pipe(map(result => {
				return result.definition.sections.some(section => {
					if (!section.hasTemplates)
						return false;
					return section.descriptionTemplates.some(template => {
						if (!(template.minMultiplicity > 0))
							return false;
						let count = 0;
						dmpModel.datasets.filter(dataset => dataset.dmpSectionIndex === (section.ordinal - 1)).forEach(dataset => {
							if (dataset.profile.id === template.descriptionTemplateId) {
								count++;
							}
						})
						if (count < template.minMultiplicity) {
							this.dialog.open(PopupNotificationDialogComponent, {
								data: {
									title: this.language.instant('DMP-OVERVIEW.MIN-DESCRIPTIONS-DIALOG.TITLE', { 'minMultiplicity': template.minMultiplicity }),
									message: this.language.instant('DMP-OVERVIEW.MIN-DESCRIPTIONS-DIALOG.MESSAGE',)
								}, maxWidth: '30em'
							});
							return true;
						}
						else
							return false;
					})
				})
			}), takeUntil(this._destroyed));
	}

	// newVersion(id: String, label: String) {
	// 	let url = this.router.createUrlTree(['/plans/new_version/', id, { dmpLabel: label }])
	// 	window.open(url.toString(), '_blank')
	// }

	viewVersions(rowId: String, rowLabel: String) {
		if (this.isPublicView) {
			this.router.navigate(['/explore-plans/versions/' + rowId], { queryParams: { groupLabel: rowLabel } });
		} else {
			this.router.navigate(['/plans/versions/' + rowId], { queryParams: { groupLabel: rowLabel } });
		}
	}

	public isAuthenticated(): boolean {
		return !(!this.authentication.current());
	}

	versionChanged(versionId: string): void {
		if (this.isPublicView) {
			this.router.navigate(['/explore-plans/publicOverview/' + versionId]);
		} else {
			this.router.navigate(['/plans/overview/' + versionId]);
		}
	}

	openShareDialog(rowId: any, rowName: any) {
		const dialogRef = this.dialog.open(DmpInvitationDialogComponent, {
			// height: '250px',
			// width: '700px',
			autoFocus: false,
			restoreFocus: false,
			data: {
				dmpId: rowId,
				dmpName: rowName
			}
		});
	}

	openNewDmpDialog() {
		if (this.dialog.openDialogs.length > 0) {
			this.dialog.closeAll();
		}
		else {
			const dialogRef = this.dialog.open(StartNewDmpDialogComponent, {
				disableClose: false,
				data: {
					isDialog: true
				}
			});
		}
	}

	// addDataset(rowId: String) {
	// 	this.router.navigate(['/datasets/new/' + rowId]);
	// }

	addNewDataset() {
		this.router.navigate(['/datasets', 'new', this.dmp.id]);
	}

	selectDoi(doiModel: DoiModel) {
		this.selectedModel = doiModel;
		const foundIdx = this.dmp.dois.findIndex(el => el.id == doiModel.id);
		this.dmp.dois.splice(foundIdx, 1);
		this.dmp.dois.unshift(doiModel);
	}

	createDoiLink(doiModel: DoiModel): string {
		const repository = this.depositRepos.find(r => r.repositoryId == doiModel.repositoryId);
		if (typeof repository !== "undefined") {
			if (doiModel.repositoryId == "Zenodo") {
				const doiarr = doiModel.doi.split('.');
				const id = doiarr[doiarr.length - 1];
				return repository.repositoryRecordUrl + id;
			}
			else {
				return repository.repositoryRecordUrl + doiModel.doi;
			}
		}
		else {
			return "";
		}
	}

	reverse() {
		const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
			restoreFocus: false,
			data: {
				message: this.language.instant('GENERAL.CONFIRMATION-DIALOG.UNFINALIZE-ITEM'),
				confirmButton: this.language.instant('QUICKWIZARD.SAVE-DIALOG.ACTIONS.AFFIRMATIVE'),
				cancelButton: this.language.instant('QUICKWIZARD.SAVE-DIALOG.ACTIONS.NEGATIVE'),
				isDeleteConfirmation: false
			}
		});
		dialogRef.afterClosed().pipe(takeUntil(this._destroyed)).subscribe(result => {
			if (result) {
				this.dmpService.unfinalize(this.dmp.id).pipe(takeUntil(this._destroyed))
					.subscribe(
						complete => {
							this.dmp.status = DmpStatus.Draft;
							this.onUpdateCallbackSuccess()
						},
						error => this.onUpdateCallbackError(error)
					);
			}
		});
	}

	goBack(): void {
		this.location.back();
	}

	reloadPage(): void {
		const path = this.location.path();
		this.router.navigateByUrl('/reload', { skipLocationChange: true }).then(() => {
			this.router.navigate([path]);
		});
	}

	updateUsers() {
		return this.dmpService.updateUsers(this.dmp.id, this.dmp.users).pipe(takeUntil(this._destroyed))
			.subscribe(
				complete => {
					this.onUpdateCallbackSuccess();
				},
				error => this.onUpdateCallbackError(error)
			);
	}

	removeUserFromDmp(user: UserInfoListingModel) {
		const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
			data: {
				message: this.language.instant('GENERAL.CONFIRMATION-DIALOG.DELETE-USER'),
				confirmButton: this.language.instant('GENERAL.CONFIRMATION-DIALOG.ACTIONS.REMOVE'),
				cancelButton: this.language.instant('GENERAL.CONFIRMATION-DIALOG.ACTIONS.CANCEL'),
				isDeleteConfirmation: false
			}
		});
		dialogRef.afterClosed().subscribe(result => {
			if (result) {
				const list = this.dmp.users;
				const index = this.dmp.users.indexOf(user);
				if (index > -1) {
					this.dmp.users.splice(index, 1);
				}
				this.updateUsers();
			}
		});
	}

	copyDoi(doi) {
		const el = document.createElement('textarea');
		el.value = doi;
		el.setAttribute('readonly', '');
		el.style.position = 'absolute';
		el.style.left = '-9999px';
		document.body.appendChild(el);
		el.select();
		document.execCommand('copy');
		document.body.removeChild(el);
		this.uiNotificationService.snackBarNotification(this.language.instant('GENERAL.SNACK-BAR.SUCCESSFUL-COPY-TO-CLIPBOARD'), SnackBarNotificationLevel.Success);
	}

	public getOrcidPath(): string {
		return this.configurationService.orcidPath;
	}

	isOrcid(reference: string) {
		const head = reference.split(':')[0];
		return head === 'orcid';
	}

	getOrcidPathForResearcher(reference: string): string {
		const path = this.getOrcidPath();
		const userId = reference.split(':')[1];
		return path + userId;
	}

	checkLockStatus(id: string) {
		this.lockService.checkLockStatus(id).pipe(takeUntil(this._destroyed))
			.subscribe(lockStatus => {
				this.lockStatus = lockStatus
				if (lockStatus) {
					this.dialog.open(PopupNotificationDialogComponent, {
						data: {
							title: this.language.instant('DMP-OVERVIEW.LOCKED-DIALOG.TITLE'),
							message: this.language.instant('DMP-OVERVIEW.LOCKED-DIALOG.MESSAGE')
						}, maxWidth: '30em'
					});
				}
			});
	}

	getUserFromDMP(): any {
		if (this.dmp) {
			const principal: Principal = this.authentication.current();
			return this.dmp.users.find(x => x.id === principal.id);
		}
	}




	// advancedClicked() {
	// 	const dialogRef = this.dialog.open(ExportMethodDialogComponent, {
	// 		maxWidth: '500px',
	// 		data: {
	// 			message: "Download as:",
	// 			XMLButton: "XML",
	// 			documentButton: "Document",
	// 			pdfButton: "PDF",
	// 			jsonButton: "JSON"
	// 		}
	// 	});
	// 	dialogRef.afterClosed().pipe(takeUntil(this._destroyed)).subscribe(result => {
	// 		if (result == "pdf") {
	// 			this.downloadPDF(this.dmp.id);
	// 		} else if (result == "xml") {
	// 			this.downloadXml(this.dmp.id);
	// 		} else if (result == "doc") {
	// 			this.downloadDocx(this.dmp.id);
	// 		} else if (result == "json") {
	// 			this.downloadJson(this.dmp.id);
	// 		}
	// 	});
	// }

	// advancedClickedFinalized() {
	// 	const dialogRef = this.dialog.open(ExportMethodDialogComponent, {
	// 		maxWidth: '250px',
	// 		data: {
	// 			message: "Download as:",
	// 			documentButton: "Document",
	// 			pdfButton: "PDF",
	// 			isFinalized: true
	// 		}
	// 	});
	// 	dialogRef.afterClosed().pipe(takeUntil(this._destroyed)).subscribe(result => {
	// 		if (result == "pdf") {
	// 			this.downloadPDF(this.dmp.id);
	// 		} else if (result == "doc") {
	// 			this.downloadDocx(this.dmp.id);
	// 		}
	// 	});
	// }

	// getAccessUrl(): string {
	// 	const redirectUri = this.configurationService.app + 'oauth2';
	// 	const url = this.configurationService.loginProviders.zenodoConfiguration.oauthUrl
	// 		+ '?client_id=' + this.configurationService.loginProviders.zenodoConfiguration.clientId
	// 		+ '&response_type=code&scope=deposit:write+deposit:actions+user:email&state=astate&redirect_uri='
	// 		+ redirectUri;
	// 	return url;
	// }

}
