import { Inject, Component, ViewChild, OnInit, Output, EventEmitter } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { DmpCriteriaComponent } from './dmp-criteria.component';
import { DmpCriteria } from '@app/core/query/dmp/dmp-criteria';
import { FormGroup } from '@angular/forms';
import { MatomoService } from '@app/core/services/matomo/matomo-service';
import { HttpClient } from '@angular/common/http';

@Component({
	selector: 'dmp-criteria-dialog-component',
	templateUrl: './dmp-criteria-dialog.component.html',
	styleUrls: ['./dmp-criteria-dialog.component.scss']
})

export class DmpCriteriaDialogComponent implements OnInit {

	@ViewChild(DmpCriteriaComponent, { static: true }) criteria: DmpCriteriaComponent;

	constructor(
		public dialogRef: MatDialogRef<DmpCriteriaDialogComponent>,
		private httpClient: HttpClient,
		private matomoService: MatomoService,
		@Inject(MAT_DIALOG_DATA) public data: { showGrant: boolean, isPublic: boolean, criteria: DmpCriteria, formGroup: FormGroup, updateDataFn: Function }
	) {
	}

	ngOnInit() {
		this.matomoService.trackPageView('DMP Criteria');
		this.criteria.setCriteria(this.data.criteria);
	}

	onNoClick(): void {
		this.dialogRef.close();
	}

	onClose(): void {
		this.dialogRef.close();
	}

	onFiltersChanged(event) {
		this.data.updateDataFn(this.criteria);
	}

}
