import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { GlossaryService } from '@app/core/services/glossary/glossary.service';
import { LanguageService } from '@app/core/services/language/language.service';
import { MatomoService } from '@app/core/services/matomo/matomo-service';
import { BaseComponent } from '@common/base/base.component';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/operators';

@Component({
	selector: 'app-glossary-content',
	templateUrl: './glossary-content.component.html',
	styleUrls: ['./glossary-content.component.scss']
})
export class GlossaryContentComponent extends BaseComponent implements OnInit {

	@Input() isDialog: boolean;

	glossaryHTMLUrl: SafeResourceUrl;
	sanitizedGuideUrl: any;

	constructor(
		private glossaryService: GlossaryService,
		private sanitizer: DomSanitizer,
		private languageService: LanguageService,
		private matomoService: MatomoService,
		private translate: TranslateService,
		private router: Router
	) { super(); }

	ngOnInit() {
		this.matomoService.trackPageView('Glossary');
		this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
			this.router.navigate(['/reload'], { skipLocationChange: true }).then(() => this.router.navigate(['/glossary']));

		});
		this.glossaryService.getGlossary(this.languageService.getCurrentLanguage())
			.pipe(takeUntil(this._destroyed))
			.subscribe(response => {
				const blob = new Blob([response.body], { type: 'text/html' });
				this.glossaryHTMLUrl = this.sanitizer.bypassSecurityTrustResourceUrl((window.URL ? URL : webkitURL).createObjectURL(blob));
			});
	}

}
