/**
 * Created by stefania on 7/17/17.
 */
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationStart, Router } from '@angular/router';
import { PageHelpContent } from '@app/core/model/help-content/page-help-content';
import { HelpContentService } from '@app/core/services/help-content/help-content.service';
import { BaseComponent } from '@common/base/base.component';
import { takeUntil } from 'rxjs/operators';

@Component({
	selector: 'app-help-content',
	template: `
        <ng-template [ngIf]="contents && contents.length>0">
            <ng-template ngFor let-content [ngForOf]="contents">
                <div [innerHTML]="content.content" class="uk-margin-medium-bottom"></div>
            </ng-template>
        </ng-template>
    `,
})
export class HelpContentComponent extends BaseComponent implements OnInit {
	@Input() position: string;
	contents: any[];
	errorMessage: string = null;
	constructor(private _helpContentService: HelpContentService, private route: ActivatedRoute, private router: Router) {
		super();
	}

	ngOnInit() {
		this.errorMessage = null;
		this.router.events
			.pipe(takeUntil(this._destroyed))
			.subscribe(event => {
				if (event instanceof NavigationStart) {
					// this._helpContentService.getActivePageContent(event['url'])
					// 	.pipe(takeUntil(this._destroyed))
					// 	.subscribe(
					// 		pageContent => this.shiftThroughContent(pageContent),
					// 		error => this.handleError(<any>error));
				}
			});
	}
	shiftThroughContent(pageContent: PageHelpContent) {
		this.contents = pageContent.content[this.position];
	}
	isPresent() {
		return (this.contents && this.contents.length > 0);
	}
	handleError(error) {
		this.contents = [];
		this.errorMessage = 'System error retrieving page content (Server responded: ' + error + ')';
	}
}

// @Component({
// 	selector: 'app-aside-help-content',
// 	template: `
//         <ng-template [ngIf]="contents && contents.length>0">
//             <ng-template ngFor let-content [ngForOf]="contents">
//                 <div [innerHTML]="content.content" class="uk-card uk-card-body uk-card-default sidemenu uk-margin-bottom"></div>
//             </ng-template>
//         </ng-template>
//     `,
// })
// export class AsideHelpContentComponent extends HelpContentComponent {
// }
