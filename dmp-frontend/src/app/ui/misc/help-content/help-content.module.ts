import { NgModule } from '@angular/core';
import { HelpContentComponent } from '@app/ui/misc/help-content/help-content.component';
import { CommonUiModule } from '@common/ui/common-ui.module';

@NgModule({
	imports: [
		CommonUiModule
	],
	declarations: [
		HelpContentComponent
	],
	exports: [HelpContentComponent]
})
export class HelpContentModule { }
