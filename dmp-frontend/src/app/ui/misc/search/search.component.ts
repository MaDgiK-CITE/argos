
import { mergeMap, distinctUntilChanged, debounceTime } from 'rxjs/operators';
import { Component, OnInit } from "@angular/core";
import { AuthService } from '../../../core/services/auth/auth.service';
import { FormControl } from "@angular/forms";
import { SearchBarService } from '../../../core/services/search-bar/search-bar.service';
import { Router } from '@angular/router';
import { Observable } from "rxjs";
import { SearchBarItem } from "../../../core/model/dashboard/search-bar-item";
import { TranslateService } from "@ngx-translate/core";

export enum SearchBarType {
	Dataset = 0,
	Dmp = 1,
	Grant = 2
}

@Component({
	selector: "app-search",
	templateUrl: "./search.component.html",
	styleUrls: ["./search.component.css"]
})
export class SearchComponent implements OnInit {
	public search = false;
	public searchControl = new FormControl();
	filteredOptions: Observable<SearchBarItem[]>;

	constructor(
		private authentication: AuthService,
		private router: Router,
		private searchBarService: SearchBarService,
		private language: TranslateService
	) { }

	ngOnInit() {
		this.filteredOptions = this.searchControl.valueChanges.pipe(debounceTime(500), distinctUntilChanged(), mergeMap(x => {
			return this.searchBarService.search(x);
		}));
	}

	public isAuthenticated(): boolean {
		return !!this.authentication.current();
	}

	onOptionSelected(event: any) {
		this.search = false;
		this.searchControl.patchValue(null);
		const selectedSearchBarItem = event.option.value;
		if (!selectedSearchBarItem.isPublished) {
			if (selectedSearchBarItem.type === SearchBarType.Dataset) { this.router.navigate(['datasets/edit/' + selectedSearchBarItem.id]); }
			if (selectedSearchBarItem.type === SearchBarType.Grant) { this.router.navigate(['plans/grant/' + selectedSearchBarItem.id]); }
			if (selectedSearchBarItem.type === SearchBarType.Dmp) { this.router.navigate(['plans/overview/' + selectedSearchBarItem.id]); }
		} else {
			if (selectedSearchBarItem.type === SearchBarType.Dataset) { this.router.navigate(['datasets/publicEdit/' + selectedSearchBarItem.id]); }
			if (selectedSearchBarItem.type === SearchBarType.Dmp) { this.router.navigate(['explore-plans/overview/' + selectedSearchBarItem.id]); }
		}
	}

	transformType(type, isPublished) {
		let subtitle: string;
		switch (type) {
			case SearchBarType.Dataset: {
				subtitle = this.language.instant('NAV-BAR.SEARCH.DATASET');
				break;
			}
			case SearchBarType.Dmp: {
				subtitle = this.language.instant('NAV-BAR.SEARCH.DMP');
				break;
			}
			case SearchBarType.Grant: {
				subtitle = this.language.instant('NAV-BAR.SEARCH.GRANT');
				break;
			}
		}
		if (isPublished) {
			return subtitle + " - " + this.language.instant('NAV-BAR.SEARCH.PUBLISHED')
		} else {
			return subtitle;
		}
	}
}
