﻿import { Component, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CompositeField } from '../../../../../core/model/dataset-profile-definition/composite-field';
import { BaseTableOfContent } from '../base-table-of-content.component';

// @Component({
// 	selector: 'app-table-of-contents-fieldset',
// 	templateUrl: './table-of-content-fieldset.component.html'
// })
export class TableOfContentsFieldSetComponent extends BaseTableOfContent {

	@Input() model: CompositeField;
	@Input() index: number;
	@Input() public path: string;
	@Input() public page: number;
	constructor(public router: Router, public route: ActivatedRoute) {
		super(router, route);
	}
}
