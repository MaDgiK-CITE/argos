import { Component, Input, OnInit } from '@angular/core';
import { DatasetProfileDefinitionModel } from '../../../../core/model/dataset-profile-definition/dataset-profile-definition';

// @Component({
// 	selector: 'app-table-of-content',
// 	templateUrl: './table-of-contents.component.html',
// 	styleUrls: ['./toc.component.css']
// })
export class TableOfContentsComponent implements OnInit {

	@Input()
	public model: DatasetProfileDefinitionModel;
	public path = '';

	ngOnInit() {

	}

}
