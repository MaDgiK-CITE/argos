﻿import { ActivatedRouteSnapshot, Router, ActivatedRoute } from '@angular/router';

export class BaseTableOfContent {
	constructor(public router: Router, public route: ActivatedRoute) { }

	scrollToId(elementId, page: number) {
		//this.paginationService.setCurrentIndex(page-1);
		this.router.navigate([this.route.snapshot.url[0] + '/' + this.route.snapshot.url[1]], { fragment: elementId, queryParams: { page: page } });
	}
}
