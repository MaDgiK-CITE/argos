﻿import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Section } from '../../../../../core/model/dataset-profile-definition/section';
import { BaseTableOfContent } from '../base-table-of-content.component';

// @Component({
// 	selector: 'app-table-of-contents-section',
// 	templateUrl: './table-of-content-section.component.html'
// })
export class TableOfContentsSectionComponent extends BaseTableOfContent implements OnInit {

	@Input() model: Section;
	@Input() index: number;
	@Input() public path: string;
	@Input() public page: number;
	constructor(public router: Router, public route: ActivatedRoute) {
		super(router, route);
	}
	ngOnInit() {

	}

}
