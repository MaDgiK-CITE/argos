﻿import { Component, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Field } from '../../../../../core/model/dataset-profile-definition/field';
import { BaseTableOfContent } from '../base-table-of-content.component';


// @Component({
// 	selector: 'app-table-of-contents-field',
// 	templateUrl: './table-of-content-field.component.html'
// })
export class TableOfContentsFieldComponent extends BaseTableOfContent {
	@Input() model: Field;
	@Input() index: number;
	@Input() public path: string;
	@Input() public page: number;
	constructor(public router: Router, public route: ActivatedRoute, ) {
		super(router, route);
	}
}
