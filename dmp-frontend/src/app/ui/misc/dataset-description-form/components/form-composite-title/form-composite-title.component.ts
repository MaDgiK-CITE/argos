import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ToCEntry } from '../../dataset-description.component';

@Component({
  selector: 'app-form-composite-title',
  templateUrl: './form-composite-title.component.html',
  styleUrls: ['./form-composite-title.component.scss']
})
export class FormCompositeTitleComponent implements OnInit {

	@Input() form: FormGroup;
	@Input() isChild: Boolean = false;
  @Input() tocentry:ToCEntry;

  public showExtendedDescription: boolean = false;

  constructor() { }

  ngOnInit() {
    if(this.tocentry){
      this.form = this.tocentry.form as FormGroup;
    }
  }

}
