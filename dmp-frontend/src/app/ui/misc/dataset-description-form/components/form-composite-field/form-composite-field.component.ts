import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input} from '@angular/core';
import {FormArray, FormGroup} from '@angular/forms';
import {BaseComponent} from '@common/base/base.component';
import {takeUntil} from 'rxjs/operators';
import {ToCEntry} from '../../dataset-description.component';
import {VisibilityRulesService} from '../../visibility-rules/visibility-rules.service';
import {
	FormCompositeFieldDialogComponent
} from "@app/ui/misc/dataset-description-form/components/form-composite-field-dialog/form-composite-field-dialog.component";
import {MatDialog} from "@angular/material/dialog";
import {cloneAbstractControl} from "@app/utilities/enhancers/utils";

@Component({
	selector: 'app-form-composite-field, [app-form-composite-field]',
	templateUrl: './form-composite-field.component.html',
	styleUrls: ['./form-composite-field.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormCompositeFieldComponent extends BaseComponent {

	@Input() datasetProfileId: String;
	@Input() form: FormGroup;
	@Input() isChild: Boolean = false;
	@Input() showDelete: Boolean = false;
	@Input() tocentry: ToCEntry;
	@Input() tableRow: boolean = false;
	@Input() showTitle: boolean = true;
	@Input() placeholderTitle: boolean = false;
	@Input() altVisibilityRulesService: VisibilityRulesService;

	constructor(
		private dialog: MatDialog,
		public visibilityRulesService: VisibilityRulesService,
		private changeDetector: ChangeDetectorRef
	) {
		super();
	}

	ngOnInit() {
		if(this.altVisibilityRulesService) {
			this.visibilityRulesService = this.altVisibilityRulesService;
		}
		this.visibilityRulesService.getElementVisibilityMapObservable().pipe(takeUntil(this._destroyed)).subscribe(x => {
			this.changeDetector.markForCheck();
		});
		if (this.tocentry) {
			this.form = this.tocentry.form as FormGroup;
		}
	}

	// addMultipleField(fieldIndex: number) {
	// 	const compositeFieldToBeCloned = (this.form.get('compositeFields').get('' + fieldIndex) as FormGroup).getRawValue();
	// 	const compositeField: DatasetDescriptionCompositeFieldEditorModel = new DatasetDescriptionCompositeFieldEditorModel().cloneForMultiplicity(compositeFieldToBeCloned);
	// 	(<FormArray>(this.form.get('compositeFields').get('' + fieldIndex).get('multiplicityItems'))).push(compositeField.buildForm());
	// }

	// markForConsideration() {
	// 	this.markForConsiderationService.markForConsideration(this.compositeField);
	// }

	editCompositeFieldInDialog() {
		const dialogRef = this.dialog.open(FormCompositeFieldDialogComponent, {
			width: '750px',
			disableClose: true,
			data: {
				formGroup: cloneAbstractControl(this.form),
				datasetProfileId: this.datasetProfileId,
				visibilityRulesService: this.visibilityRulesService
			}
		});
		dialogRef.afterClosed().pipe(takeUntil(this._destroyed)).subscribe(data => {
			if (data) {
				this.form.patchValue(data.value);
				this.changeDetector.detectChanges();
			}
		});
	}

	deleteCompositeField() {
		if (this.isChild) {
			this.deleteMultipeFieldFromCompositeFormGroup();
		} else {
			this.deleteCompositeFieldFormGroup();
		}
	}

	deleteCompositeFieldFormGroup() {

		const compositeFieldId = ((this.form.get('multiplicityItems') as FormArray).get('' + 0) as FormGroup).getRawValue().id;
		const fieldIds = (this.form.get('fields') as FormArray).controls.map(control => control.get('id').value) as string[];

		const numberOfItems = this.form.get('multiplicityItems').get('' + 0).get('fields').value.length;
		for (let i = 0; i < numberOfItems; i++) {
			const multiplicityItem = this.form.get('multiplicityItems').get('' + 0).get('fields').get('' + i).value;
			this.form.get('fields').get('' + i).patchValue(multiplicityItem);
		}
		(<FormArray>(this.form.get('multiplicityItems'))).removeAt(0);


		this.visibilityRulesService.removeAllIdReferences(compositeFieldId);
		fieldIds.forEach(x => this.visibilityRulesService.removeAllIdReferences(x));
	}

	deleteMultipeFieldFromCompositeFormGroup() {
		const parent = this.form.parent;
		const index = (parent as FormArray).controls.indexOf(this.form);

		const currentId = this.form.get('id').value;
		const fieldIds = (this.form.get('fields') as FormArray).controls.map(control => control.get('id').value) as string[];


		this.visibilityRulesService.removeAllIdReferences(currentId);
		fieldIds.forEach(x => this.visibilityRulesService.removeAllIdReferences(x));

		(parent as FormArray).removeAt(index);
		(parent as FormArray).controls.forEach((control, i) => {
			try {
				control.get('ordinal').setValue(i);
			} catch {
				throw 'Could not find ordinal';
			}
		});
		// (<FormArray>(this.form as AbstractControl)).removeAt(fildIndex);
	}
}
