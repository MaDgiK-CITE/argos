import {Component, Inject} from "@angular/core";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {VisibilityRulesService} from "@app/ui/misc/dataset-description-form/visibility-rules/visibility-rules.service";

@Component({
	selector: 'app-form-composite-field-dialog',
	templateUrl: 'form-composite-field-dialog.component.html'
})
export class FormCompositeFieldDialogComponent {

	public visibilityRulesService: VisibilityRulesService;

	constructor(
		private dialogRef: MatDialogRef<FormCompositeFieldDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any
	) {
		this.visibilityRulesService = data.visibilityRulesService;
	}

	cancel() {
		this.dialogRef.close();
	}

	save() {
		this.dialogRef.close(this.data.formGroup);
	}

	public close() {
		this.dialogRef.close(false);
	}
}
