import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {TableOfContents} from './table-of-contents';
import {RouterModule} from '@angular/router';
import { TableOfContentsInternal } from './table-of-contents-internal/table-of-contents-internal';
import { VisibilityRulesService } from '../visibility-rules/visibility-rules.service';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  imports: [CommonModule, RouterModule, MatIconModule],
  declarations: [TableOfContents, TableOfContentsInternal],
  exports: [TableOfContents],
  entryComponents: [TableOfContents],
  providers:[VisibilityRulesService]
})
export class TableOfContentsModule { }
