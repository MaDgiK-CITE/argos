import { VisibilityRuleSource } from "./visibility-rule-source";

export class VisibilityRule {
	public targetControlId: string;
	public sourceVisibilityRules: Array<VisibilityRuleSource>;
}
