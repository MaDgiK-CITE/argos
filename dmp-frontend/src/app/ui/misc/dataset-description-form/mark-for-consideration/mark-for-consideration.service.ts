import { Injectable } from '@angular/core';
import { CompositeField } from '../../../../core/model/dataset-profile-definition/composite-field';

@Injectable()
export class MarkForConsiderationService {

	private compositeFields: CompositeField[] = [];

	markForConsideration(field: CompositeField) {
		if (this.exists(field)) {
			this.compositeFields.push(field);
		}
	}

	getFields() {
		return this.compositeFields;
	}

	exists(field: CompositeField) {
		return this.compositeFields.map(x => x.id).indexOf(field.id) === -1;
	}
}
