import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { CompositeField } from '../../../../../core/model/dataset-profile-definition/composite-field';
import { DatasetProfileDefinitionModel } from '../../../../../core/model/dataset-profile-definition/dataset-profile-definition';
import { Rule } from '../../../../../core/model/dataset-profile-definition/rule';
import { Section } from '../../../../../core/model/dataset-profile-definition/section';
import { DatasetWizardModel } from '../../../../../core/model/dataset/dataset-wizard';
import { MarkForConsiderationService } from '../../mark-for-consideration/mark-for-consideration.service';
import { VisibilityRulesService } from '../../visibility-rules/visibility-rules.service';

// @Component({
// 	selector: 'app-dynamic-form-pending-questions-display',
// 	templateUrl: './dynamic-form-pending-questions-display.component.html',
// 	styleUrls: ['./dynamic-form-pending-questions-display.component.scss']
// })
export class DynamicFormPendingQuestionsDisplayComponent implements OnInit {

	constructor(
		private visibilityRulesService: VisibilityRulesService,
		public markForConsideration: MarkForConsiderationService) {
	}

	datasetProfileDefinitionModel: DatasetProfileDefinitionModel;

	@Input()
	form: FormGroup;

	@Input() dataModel: DatasetWizardModel;

	fields: CompositeField[];

	ngOnInit(): void {
		const rules: Rule[] = this.dataModel.datasetProfileDefinition.rules;
		this.datasetProfileDefinitionModel = this.dataModel.datasetProfileDefinition;
		const sections: Section[] = this.datasetProfileDefinitionModel.pages.flatMap(page => page.sections).filter(x => x);
		const compositeFields: CompositeField[] = sections.flatMap(section => section.compositeFields).filter(x => x);
		const nestedSections: Section[] = sections.flatMap(section => section.sections).filter(x => x);
		const nestedCompositeFiels: CompositeField[] = nestedSections.flatMap(section => section.compositeFields).filter(x => x);
		const compositeFieldsUnion = compositeFields.concat(nestedCompositeFiels);
		//const fields: Field[] = compositeFields.flatMap(composite => composite.fields).concat(nestedCompositeFiels.flatMap(composite => composite.fields));
		// const fields = compositeFieldsUnion.filter(compositeField => this.visibilityRulesService.checkElementVisibility(compositeField.id))
		// 	.filter(compositeField => compositeField.fields.filter(x => x && x.validationRequired && this.visibilityRulesService.getFormGroup(x.id).value == null).length > 0);
		// fields.forEach(x => this.markForConsideration.markForConsideration(x));
	}
}
