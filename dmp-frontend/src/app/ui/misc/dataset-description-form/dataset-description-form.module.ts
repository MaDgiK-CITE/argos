import {NgModule} from '@angular/core';
import {AutoCompleteModule} from '@app/library/auto-complete/auto-complete.module';
import {
	FormCompositeFieldComponent
} from '@app/ui/misc/dataset-description-form/components/form-composite-field/form-composite-field.component';
import {FormFieldComponent} from '@app/ui/misc/dataset-description-form/components/form-field/form-field.component';
import {
	FormSectionComponent
} from '@app/ui/misc/dataset-description-form/components/form-section/form-section.component';
import {
	DatasetDescriptionFormComponent
} from '@app/ui/misc/dataset-description-form/dataset-description-form.component';
import {FormFocusService} from '@app/ui/misc/dataset-description-form/form-focus/form-focus.service';
import {VisibilityRulesService} from '@app/ui/misc/dataset-description-form/visibility-rules/visibility-rules.service';
import {CommonFormsModule} from '@common/forms/common-forms.module';
import {CommonUiModule} from '@common/ui/common-ui.module';
import {FormCompositeTitleComponent} from './components/form-composite-title/form-composite-title.component';
import {ExternalSourcesModule} from '../external-sources/external-sources.module';
import {DatasetDescriptionComponent} from './dataset-description.component';
import {FormProgressIndicationModule} from './components/form-progress-indication/form-progress-indication.module';
import {FormSectionInnerComponent} from './components/form-section/form-section-inner/form-section-inner.component';
import {RichTextEditorModule} from "@app/library/rich-text-editor/rich-text-editor.module";
// import {TableEditorModule} from "@app/library/table-editor/table-editor.module";
import {FileService} from "@app/core/services/file/file.service";
import {NgxDropzoneModule} from "ngx-dropzone";
import {
	FormCompositeFieldDialogComponent
} from "@app/ui/misc/dataset-description-form/components/form-composite-field-dialog/form-composite-field-dialog.component";
import {FormattingModule} from "@app/core/formatting.module";


@NgModule({
	imports: [
		CommonUiModule,
		CommonFormsModule,
		AutoCompleteModule,
		ExternalSourcesModule,
		FormProgressIndicationModule,
		RichTextEditorModule,
		// TableEditorModule,
		NgxDropzoneModule,
		FormattingModule
	],
	declarations: [
		DatasetDescriptionFormComponent,
		DatasetDescriptionComponent,
		FormSectionComponent,
		FormSectionInnerComponent,
		FormCompositeFieldComponent,
		FormFieldComponent,
		FormCompositeTitleComponent,
		FormCompositeFieldDialogComponent
	],
	exports: [
		DatasetDescriptionFormComponent,
		DatasetDescriptionComponent,
		FormCompositeFieldComponent,
		FormFieldComponent,
		FormSectionInnerComponent
	],
	providers: [
		VisibilityRulesService,
		FormFocusService,
		FileService
	]
})
export class DatasetDescriptionFormModule { }

