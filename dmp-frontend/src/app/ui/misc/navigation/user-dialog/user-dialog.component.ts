import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Principal } from '../../../../core/model/auth/principal';
import { AuthService } from '../../../../core/services/auth/auth.service';

@Component({
	selector: 'app-user-dialog-component',
	templateUrl: 'user-dialog.component.html',
	styleUrls: ['user-dialog.component.scss']
})
export class UserDialogComponent implements OnInit {

	public formGroup: FormGroup;

	constructor(
		private authentication: AuthService,
		private router: Router,
		public dialogRef: MatDialogRef<UserDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any
	) { }

	ngOnInit(): void {
	}

	public logout(): void {
		this.dialogRef.close();
		this.authentication.logout();
	}

	public getPrincipalName(): string {
		const principal: Principal = this.authentication.current();
		if (principal) { return principal.name; }
		return '';
	}

	public getPrincipalEmail(): string {
		const principal: Principal = this.authentication.current();
		if (principal) { return principal.email; }
		return '';
	}

	public principalHasAvatar(): boolean {
		return this.authentication.current() && this.authentication.current().avatarUrl != null && this.authentication.current().avatarUrl.length > 0;
	}

	public getPrincipalAvatar(): string {
		return this.authentication.current() && this.authentication.current().avatarUrl;
	}

	public getDefaultAvatar(): string {
		return 'assets/images/profile-placeholder.png';
	}

	public applyFallbackAvatar(ev: Event) {
		(ev.target as HTMLImageElement).src = this.getDefaultAvatar();
	}

	public navigateToProfile() {
		this.dialogRef.close();
		this.router.navigate(['/profile']);
	}

	public navigateToMyDmps() {
		this.dialogRef.close();
		this.router.navigate(['/plans']);
	}
}
