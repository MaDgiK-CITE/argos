import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NavigationComponent } from '@app/ui/misc/navigation/navigation.component';
import { UserDialogComponent } from '@app/ui/misc/navigation/user-dialog/user-dialog.component';
import { CommonFormsModule } from '@common/forms/common-forms.module';
import { CommonUiModule } from '@common/ui/common-ui.module';

@NgModule({
	imports: [
		CommonUiModule,
		CommonFormsModule,
		RouterModule,
	],
	declarations: [
		NavigationComponent,
		UserDialogComponent
	],
	entryComponents: [UserDialogComponent],
	exports: [NavigationComponent]
})
export class NavigationModule { }
