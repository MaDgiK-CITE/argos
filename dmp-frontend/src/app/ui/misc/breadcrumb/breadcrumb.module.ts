import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BreadcrumbComponent } from '@app/ui/misc/breadcrumb/breadcrumb.component';
import { BreadCrumbResolverService } from '@app/ui/misc/breadcrumb/service/breadcrumb.service';
import { CommonUiModule } from '@common/ui/common-ui.module';

@NgModule({
	imports: [
		CommonUiModule,
		RouterModule
	],
	declarations: [
		BreadcrumbComponent
	],
	providers: [
		BreadCrumbResolverService
	],
	exports: [BreadcrumbComponent]
})
export class BreadcrumbModule { }
