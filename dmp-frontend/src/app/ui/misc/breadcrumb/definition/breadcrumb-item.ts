
export class BreadcrumbItem {
	parentComponentName?: string;
	label: string;
	url: string;
	params?: any = {};
	icon?: string;
	notFoundResolver?: any[];
}
