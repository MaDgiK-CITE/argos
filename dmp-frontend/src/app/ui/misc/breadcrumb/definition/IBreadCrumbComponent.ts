import { BreadcrumbItem } from './breadcrumb-item';
import { Observable } from 'rxjs';

export interface IBreadCrumbComponent {
	breadCrumbs: Observable<BreadcrumbItem[]>;
}
