import { NgModule } from '@angular/core';

import { Oauth2DialogComponent } from './oauth2-dialog.component';
import { CommonUiModule } from '@common/ui/common-ui.module';
import { Oauth2DialogService } from './service/oauth2-dialog.service';


@NgModule({
  declarations: [Oauth2DialogComponent],
  imports: [
    CommonUiModule
  ],
  providers: [
	  Oauth2DialogService
  ]
})
export class Oauth2DialogModule { }
