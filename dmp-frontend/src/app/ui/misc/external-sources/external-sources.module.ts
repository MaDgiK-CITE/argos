import { NgModule } from '@angular/core';
import { AutoCompleteModule } from '@app/library/auto-complete/auto-complete.module';
import { ExternalItemListingComponent } from '@app/ui/misc/external-sources/listing/external-item-listing.component';
import { CommonFormsModule } from '@common/forms/common-forms.module';
import { CommonUiModule } from '@common/ui/common-ui.module';

@NgModule({
	imports: [
		CommonUiModule,
		CommonFormsModule,
		AutoCompleteModule
	],
	declarations: [
		ExternalItemListingComponent,
	],
	exports: [
		ExternalItemListingComponent
	]
})
export class ExternalSourcesModule { }
