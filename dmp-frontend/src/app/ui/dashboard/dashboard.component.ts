
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { RecentActivityType } from '@app/core/common/enum/recent-activity-type';
import { DashboardStatisticsModel } from '@app/core/model/dashboard/dashboard-statistics-model';
import { SearchBarItem } from '@app/core/model/dashboard/search-bar-item';
import { DataTableRequest } from '@app/core/model/data-table/data-table-request';
import { DatasetListingModel } from '@app/core/model/dataset/dataset-listing';
import { DmpListingModel } from '@app/core/model/dmp/dmp-listing';
import { GrantCriteria } from '@app/core/query/grant/grant-criteria';
import { RequestItem } from '@app/core/query/request-item';
import { AuthService } from '@app/core/services/auth/auth.service';
import { DashboardService } from '@app/core/services/dashboard/dashboard.service';
import { DatasetService } from '@app/core/services/dataset/dataset.service';
import { DmpService } from '@app/core/services/dmp/dmp.service';
import { GrantService } from '@app/core/services/grant/grant.service';
import { SearchBarService } from '@app/core/services/search-bar/search-bar.service';
import { UserService } from '@app/core/services/user/user.service';
import { SingleAutoCompleteConfiguration } from '@app/library/auto-complete/single/single-auto-complete-configuration';
import { BaseComponent } from '@common/base/base.component';
import { Observable, of as observableOf } from 'rxjs';
import { mergeMap, takeUntil } from 'rxjs/operators';
import { BreadcrumbItem } from '../misc/breadcrumb/definition/breadcrumb-item';
import { IBreadCrumbComponent } from '../misc/breadcrumb/definition/IBreadCrumbComponent';
import { DmpCriteria } from '@app/core/query/dmp/dmp-criteria';
import { DatasetCriteria } from '@app/core/query/dataset/dataset-criteria';
import { MatDialog } from '@angular/material/dialog';
import { StartNewDmpDialogComponent } from '../dmp/start-new-dmp-dialogue/start-new-dmp-dialog.component';
import { StartNewDatasetDialogComponent } from '../dmp/start-new-dataset-dialogue/start-new-dataset-dialog.component';
import { DatasetWizardEditorModel } from '../dataset/dataset-wizard/dataset-wizard-editor.model';
import { DatasetWizardService } from '@app/core/services/dataset-wizard/dataset-wizard.service';
import { TranslateService } from '@ngx-translate/core';
import { UiNotificationService, SnackBarNotificationLevel } from '@app/core/services/notification/ui-notification-service';
import { GuidedTourService } from '@app/library/guided-tour/guided-tour.service';
import { GuidedTour, Orientation } from '@app/library/guided-tour/guided-tour.constants';
import { MatomoService } from '@app/core/services/matomo/matomo-service';
import { ConfigurationService } from '@app/core/services/configuration/configuration.service';
import * as moment from 'moment';
import { CookieService } from 'ngx-cookie-service';


@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent extends BaseComponent implements OnInit, IBreadCrumbComponent {

	breadCrumbs: Observable<BreadcrumbItem[]>;

	public isVisible = true

	public userInfo: any;
	datasetActivities: any[];
	grantActivities: any[];
	dmpActivities: any[];
	organisationActivities: any[];
	public dashboardStatisticsData: DashboardStatisticsModel;
	public formControl = new FormControl();
	grantAutoCompleteConfiguration: SingleAutoCompleteConfiguration;
	public searchControl = new FormControl();
	filteredOptions: Observable<SearchBarItem[]>;
	recentActivityTypeEnum = RecentActivityType;
	public search = false;
	dmpListingItems: DmpListingModel[] = [];
	datasetListingItems: DatasetListingModel[] = [];

	totalDatasets: number;
	totalDmps: number;
	totalDraftDatasets: number;
	totalRecents: number;

	dmpText: string;
	datasetText: string;
	importFileText: string;
	startWizardText: string;
	currentType: string = "recent";
	newReleaseNotificationVisible = false;

	constructor(
		private router: Router,
		private route: ActivatedRoute,
		private grantService: GrantService,
		private dmpService: DmpService,
		private datasetService: DatasetService,
		private datasetWizardService: DatasetWizardService,
		private dashboardService: DashboardService,
		private searchBarService: SearchBarService,
		private authentication: AuthService,
		private userService: UserService,
		private dialog: MatDialog,
		private language: TranslateService,
		private uiNotificationService: UiNotificationService,
		private guidedTourService: GuidedTourService,
		private matomoService: MatomoService,		
		private cookieService: CookieService,
		public configurationService: ConfigurationService
	) {
		super();
		// this.dashboardStatisticsData.totalDataManagementPlanCount = 0;
		// this.dashboardStatisticsData.totalDataSetCount = 0;
		// this.dashboardStatisticsData.totalGrantCount = 0;
	}


	ngOnInit() {
		this.route.queryParams.subscribe(params => {
			let type = params['type'];
			if(type || type == "recent" || (type == "drafts" && this.isAuthenticated()) || type == "dmps" || type == "datasets") {
				this.currentType = type;
			} else {
				this.currentType = "recent";
			}
		});

		this.matomoService.trackPageView('Home Dashboard');
		// if (this.isAuthenticated()) {
		// 	this.userService.getRecentActivity()
		// 		.pipe(takeUntil(this._destroyed))
		// 		.subscribe(response => {
		// 			this.datasetActivities = response['recentDatasetActivities'];
		// 			this.dmpActivities = response['recentDmpActivities'];
		// 			this.grantActivities = response['recentGrantActivities'];
		// 			this.organisationActivities = response['totalOrganisationCount'];
		// 		});
		// }

		// this.grantAutoCompleteConfiguration = {
		// 	filterFn: this.searchGrant.bind(this),
		// 	items: this.searchGrant(''),
		// 	displayFn: (item) => item['label'],
		// 	titleFn: (item) => item['label']
		// };

		const breadCrumbs = [];
		this.breadCrumbs = observableOf(breadCrumbs);

		if (!this.isAuthenticated()) {
			this.dashboardService.getStatistics()
				.pipe(takeUntil(this._destroyed))
				.subscribe(results => {
					//let data = results['payload'];
					this.dashboardStatisticsData = results;
				});
			// this.getPublicDmps();
			// this.getPublicDatasets();
		} else {
			this.dashboardService.getUserStatistics()
				.pipe(takeUntil(this._destroyed))
				.subscribe(results => {
					this.dashboardStatisticsData = results;

					if (this.dashboardStatisticsData && this.dashboardStatisticsData.totalDataManagementPlanCount === 0) {
						this.openDashboardTour();
					}
				});
		}

		this.filteredOptions = this.searchControl.valueChanges.pipe(mergeMap(x => {
			return this.searchBarService.search(x);
		}));

		this.newReleaseNotificationVisible = this.isNewReleaseNotificationVisible();
	}

	public get indexFromCurrentType() {
		if(this.currentType == "recent") {
			return 0;
		}
		if(this.currentType == "drafts") {
			return 1;
		}
		if(this.currentType == "dmps") {
			return this.isAuthenticated() ? 2 : 1;
		}
		if(this.currentType == "datasets") {
			return this.isAuthenticated() ? 3 : 2;
		}

		return 0;
	}

	// ngAfterContentChecked(): void {
	// 	if (this.dashboardStatisticsData && this.dashboardStatisticsData.totalDataManagementPlanCount === 0) {
	// 		this.openTour();
	// 	}
	// }

	public isAuthenticated(): boolean {
		return !(!this.authentication.current());
	}

	searchGrant(query: string) {
		const grantRequestItem: RequestItem<GrantCriteria> = new RequestItem();
		grantRequestItem.criteria = new GrantCriteria();
		grantRequestItem.criteria.like = query;
		return this.grantService.getWithExternal(grantRequestItem);
	}

	redirect(id: string, type: RecentActivityType) {
		switch (type) {
			case RecentActivityType.Grant: {
				this.router.navigate(['grants/edit/' + id]);
				return;
			}
			case RecentActivityType.Dataset: {
				this.router.navigate(['datasets/edit/' + id]);
				return;
			}
			case RecentActivityType.Dmp: {
				this.router.navigate(['plans/edit/' + id]);
				return;
			}
			default: throw new Error('Unsupported Activity Type ');
		}
	}

	getPublicDmps() {
		const dmpCriteria = new DmpCriteria();
		dmpCriteria.allVersions = false;
		dmpCriteria.isPublic = true;
		dmpCriteria.onlyPublic = true;
		const fields: Array<string> = new Array<string>();
		fields.push('-finalizedAt');
		const dataTableRequest: DataTableRequest<DmpCriteria> = new DataTableRequest(0, 2, { fields: fields });
		dataTableRequest.criteria = dmpCriteria;
		return this.dmpService.getPaged(dataTableRequest, "listing").pipe(takeUntil(this._destroyed)).subscribe(result => { this.dmpListingItems = result.data; });
	}

	getPublicDatasets() {
		const dmpCriteria = new DatasetCriteria();
		dmpCriteria.allVersions = false;
		dmpCriteria.isPublic = true;
		const fields: Array<string> = new Array<string>();
		fields.push('-modified');
		const dataTableRequest: DataTableRequest<DatasetCriteria> = new DataTableRequest(0, 4, { fields: fields });
		dataTableRequest.criteria = dmpCriteria;
		return this.datasetService.getPaged(dataTableRequest).pipe(takeUntil(this._destroyed)).subscribe(result => { this.datasetListingItems = result.data; });
	}

	dmpClicked(dmp: DmpListingModel) {
		if (!this.isAuthenticated()) {
			this.router.navigate(['../explore-plans/publicOverview', dmp.id], { relativeTo: this.route });
		} else {
			this.router.navigate(['/plans/publicEdit/' + dmp.id]);
		}
	}

	datasetClicked(dataset: DatasetListingModel) {
		this.router.navigate(['/datasets/publicEdit/' + dataset.id]);
	}

	public closeCard(): void {
		this.isVisible = false;
	}

	onCountDmps(event): void {
		this.totalDmps = event;
	}

	onCountDatasets(event): void {
		this.totalDatasets = event;
	}

	onCountDraftDatasets(event): void {
		this.totalDraftDatasets = event;
	}

	onCountAllRecent(event): void {
		this.totalRecents = event;
	}

	openNewDmpDialog() {
		if (this.dialog.openDialogs.length > 0) {
			this.dialog.closeAll();
		}
		else {
			const dialogRef = this.dialog.open(StartNewDmpDialogComponent, {
				disableClose: false,
				data: {
					isDialog: true
				}
			});
		}
	}

	public hasDmps(): boolean {
		if (this.dashboardStatisticsData) {
			return this.dashboardStatisticsData.totalDataManagementPlanCount !== 0
				|| this.dashboardStatisticsData.totalDataSetCount !== 0
				|| this.dashboardStatisticsData.totalGrantCount !== 0
				|| this.dashboardStatisticsData.totalOrganisationCount !== 0;
		} else {
			return false;
		}
	}

	addNewDataset() {
		const dialogRef = this.dialog.open(StartNewDatasetDialogComponent, {
			disableClose: false,
			restoreFocus: false,
			data: {
				startNewDmp: false,
				formGroup: new DatasetWizardEditorModel().buildForm()
			}
		});
		dialogRef.afterClosed().pipe(takeUntil(this._destroyed)).subscribe(result => {
			if (result) {
				if (result.startNewDmp) {
					this.openNewDmpDialog();
				} else {
					this.router.navigate(['/plans', 'edit', result.formGroup.get('dmp').value.id]);
					// Save dataset direct but missing title and template
					// this.datasetWizardService.createDataset(result.formGroup.getRawValue())
					// 	.pipe(takeUntil(this._destroyed))
					// 	.subscribe(
					// 		data => {
					// 			this.onCallbackSuccess(data);
					// 		},
					// 		error => this.onCallbackError(error)
					// 	);
				}
			}
		});
	}

	public dashboardTour: GuidedTour = {
		tourId: 'dashboard-tour',
		useOrb: true,
		steps: [
			{
				selector: '.new-dmp-dialog',
				content: 'Step 1',
				orientation: Orientation.BottomRight,
				isStepUnique: false,
				highlightPadding: 10,
				closeAction: () => this.openNewDmpDialog()
			},
			{
				selector: '.import-file',
				content: 'Step 2',
				orientation: Orientation.Bottom,
				isStepUnique: false,
				highlightPadding: 10
			},
			{
				selector: '.start-wizard',
				content: 'Step 3',
				orientation: Orientation.Bottom,
				isStepUnique: false,
				highlightPadding: 10,
				closeAction: () => this.dialog.closeAll()
			},
			{
				selector: '.new-dataset-tour',
				content: 'Step 4',
				orientation: Orientation.BottomLeft,
				isStepUnique: false,
				highlightPadding: 10
			}
		]
	};

	public setDashboardTourDmpText(): void {
		this.dmpText = this.language.instant('DASHBOARD.TOUR-GUIDE.DMP') + '\n\n' +
			this.language.instant('DASHBOARD.TOUR-GUIDE.START-NEW');
		this.dashboardTour.steps[0].title = this.dmpText;
	}

	public setDashboardImportFileText(): void {
		this.importFileText = this.language.instant('DASHBOARD.TOUR-GUIDE.IMPORT-DMP');
		this.dashboardTour.steps[1].title = this.importFileText;
	}

	public setDashboardStartWizardText(): void {
		this.startWizardText = this.language.instant('DASHBOARD.TOUR-GUIDE.START-WIZARD');
		this.dashboardTour.steps[2].title = this.startWizardText;
	}

	public setDatasetText(): void {
		this.datasetText = this.language.instant('DASHBOARD.TOUR-GUIDE.DATASET') + '\n\n' +
			this.language.instant('DASHBOARD.TOUR-GUIDE.NEW-DATASET');
		this.dashboardTour.steps[3].title = this.datasetText;
	}

	openDashboardTour() {
		this.setDashboardTourDmpText();
		this.setDashboardImportFileText();
		this.setDashboardStartWizardText();
		this.setDatasetText();
		this.guidedTourService.startTour(this.dashboardTour);
	}

	// onCallbackSuccess(datasetId: String): void {
	// 	this.uiNotificationService.snackBarNotification(this.language.instant('GENERAL.SNACK-BAR.SUCCESSFUL-CREATION'), SnackBarNotificationLevel.Success);
	// 	this.router.navigate(['/reload']).then(() => { this.router.navigate(['/datasets', 'edit', datasetId]); });
	// }

	// onCallbackError(error: any) {
	// 	this.uiNotificationService.snackBarNotification(error.error.message ? error.error.message : this.language.instant('GENERAL.SNACK-BAR.UNSUCCESSFUL-UPDATE'), SnackBarNotificationLevel.Error);
	// }

	// viewAllPublicDmpsClicked() {
	// 	this.router.navigate(['/explore-plans']);
	// }

	// viewAllPublicDatasetsClicked() {
	// 	this.router.navigate(['explore']);
	// }

	dismissNewReleaseNotification() {
		this.cookieService.set('new-release-dismiss-' + this.configurationService.newReleaseNotificationVersionCode, 'true', 5000, null, null, false, 'Lax');
		this.newReleaseNotificationVisible = false;
	}

	isNewReleaseNotificationVisible() {
		if (this.configurationService.newReleaseNotificationVersionCode == null) {
			return false;
		}
		if (this.configurationService.newReleaseNotificationExpires == null && this.configurationService.newReleaseNotificationLink == null) {
			return false;
		}
		if (this.configurationService.newReleaseNotificationExpires != null && moment(this.configurationService.newReleaseNotificationExpires).tz('UTC') < moment.utc()) {
			return false;
		}
		if (this.cookieService.get('new-release-dismiss-' + this.configurationService.newReleaseNotificationVersionCode) === 'true') {
			return false;
		}

		return true;
	}
}
