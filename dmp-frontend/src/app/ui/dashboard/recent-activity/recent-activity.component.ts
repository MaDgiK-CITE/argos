import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RecentActivityType } from '@app/core/common/enum/recent-activity-type';
import { UserService } from '@app/core/services/user/user.service';
import { BaseComponent } from '@common/base/base.component';
import { takeUntil } from 'rxjs/operators';

@Component({
	selector: 'app-recent-activity',
	templateUrl: './recent-activity.component.html',
	styleUrls: ['./recent-activity.component.scss']
})
export class RecentActivityComponent extends BaseComponent implements OnInit {

	datasetActivities: any[];
	grantActivities: any[];
	dmpActivities: any[];
	recentActivityTypeEnum = RecentActivityType;

	constructor(
		private router: Router,
		private userService: UserService
	) { super(); }

	ngOnInit() {
		this.userService.getRecentActivity()
			.pipe(takeUntil(this._destroyed))
			.subscribe(response => {
				this.datasetActivities = response['recentDatasetActivities'];
				this.dmpActivities = response['recentDmpActivities'];
				this.grantActivities = response['recentGrantActivities'];
			});
	}

	redirect(id: string, type: RecentActivityType) {
		switch (type) {
			case RecentActivityType.Grant: {
				this.router.navigate(['grants/edit/' + id]);
				return;
			}
			case RecentActivityType.Dataset: {
				this.router.navigate(['datasets/edit/' + id]);
				return;
			}
			case RecentActivityType.Dmp: {
				this.router.navigate(['plans/edit/' + id]);
				return;
			}
			default: throw new Error('Unsupported Activity Type ');
		}

	}
}
