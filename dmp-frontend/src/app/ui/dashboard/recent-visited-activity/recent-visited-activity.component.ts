import { Component, OnInit } from "@angular/core";
import { Router } from '@angular/router';
import { RecentActivityType } from '@app/core/common/enum/recent-activity-type';
import { DataTableRequest } from '@app/core/model/data-table/data-table-request';
import { DmpListingModel } from '@app/core/model/dmp/dmp-listing';
import { DmpCriteria } from '@app/core/query/dmp/dmp-criteria';
import { AuthService } from '@app/core/services/auth/auth.service';
import { DmpService } from '@app/core/services/dmp/dmp.service';
import { EnumUtils } from '@app/core/services/utilities/enum-utils.service';
import { BaseComponent } from '@common/base/base.component';
import { takeUntil } from "rxjs/operators";

@Component({
	selector: "app-recent-visited-activity",
	templateUrl: "./recent-visited-activity.component.html",
	styleUrls: ["./recent-visited-activity.component.css"]
})
export class RecentVisitedActivityComponent extends BaseComponent
	implements OnInit {
	dmpActivities: DmpListingModel[];
	recentActivityItems: any[];
	recentActivityTypeEnum = RecentActivityType;
	public: boolean = false;

	constructor(
		private router: Router,
		private authentication: AuthService,
		private dmpService: DmpService,
		public enumUtils: EnumUtils
	) {
		super();
	}

	ngOnInit() {
		if (this.isAuthenticated()) {
			const fields: Array<string> = ["-created"];
			const dmpDataTableRequest: DataTableRequest<DmpCriteria> = new DataTableRequest(0, null, { fields: fields });
			dmpDataTableRequest.criteria = new DmpCriteria();
			dmpDataTableRequest.criteria.like = "";
			this.dmpService
				.getPaged(dmpDataTableRequest, "listing")
				.pipe(takeUntil(this._destroyed))
				.subscribe(response => {
					this.dmpActivities = response.data;
				});
		}
	}

	redirect(id: string, type: RecentActivityType) {
		switch (type) {
			case RecentActivityType.Grant: {
				this.router.navigate(["grants/edit/" + id]);
				return;
			}
			case RecentActivityType.Dataset: {
				this.router.navigate(["datasets/edit/" + id]);
				return;
			}
			case RecentActivityType.Dmp: {
				this.router.navigate(["plans/edit/" + id]);
				return;
			}
			default:
				throw new Error("Unsupported Activity Type ");
		}
	}

	public isAuthenticated(): boolean {
		return !!this.authentication.current();
	}

	navigateToUrl() { }
}
