import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Router } from "@angular/router";

@Component({
	selector: "app-wizard",
	templateUrl: "./wizard.component.html",
	styleUrls: ["./wizard.component.css"]
})
export class WizardComponent {
	@Input() title: string;
	@Input() subtitle: string;
	@Input() icon: string;
	@Input() routerLink: string;
	@Output() onClick: EventEmitter<void> = new EventEmitter<void>();

	wizardItems: any[];

	constructor(private router: Router) {}

	cardClicked() {
		this.onClick.emit();
	}
	navigateToCreate() {
		this.router.navigate(["/quick-wizard"]);
	}

	navigateToAdd() {
		this.router.navigate(["/datasetcreatewizard"]);
	}
}
