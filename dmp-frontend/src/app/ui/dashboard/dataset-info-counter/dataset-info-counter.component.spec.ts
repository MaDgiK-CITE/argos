import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DatasetInfoCounterComponent } from './dataset-info-counter.component';

describe('DatasetInfoCounterComponent', () => {
  let component: DatasetInfoCounterComponent;
  let fixture: ComponentFixture<DatasetInfoCounterComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DatasetInfoCounterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatasetInfoCounterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
