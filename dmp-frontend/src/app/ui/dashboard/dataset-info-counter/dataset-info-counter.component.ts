import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DatasetListingModel } from '../../../core/model/dataset/dataset-listing';

@Component({
	selector: 'app-dataset-info-counter',
	templateUrl: './dataset-info-counter.component.html',
	styleUrls: ['./dataset-info-counter.component.css']
})
export class DatasetInfoCounterComponent implements OnInit {

	@Input() dataset: DatasetListingModel;
	@Output() onClick: EventEmitter<DatasetListingModel> = new EventEmitter();

	constructor() { }

	ngOnInit() {
	}

	itemClicked() {
		this.onClick.emit(this.dataset);
	}
}
