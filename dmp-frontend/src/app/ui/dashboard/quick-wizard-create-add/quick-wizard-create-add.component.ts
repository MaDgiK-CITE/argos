import { Component, OnInit } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { ActivatedRoute, Router } from "@angular/router";
import { BaseComponent } from '@common/base/base.component';
import { TranslateService } from "@ngx-translate/core";

@Component({
	selector: 'app-quick-wizard-create-add-component',
	templateUrl: 'quick-wizard-create-add.component.html',
	styleUrls: ['./quick-wizard-create-add.component.scss']
})
export class QuickWizardCreateAdd extends BaseComponent implements OnInit {

	constructor(
		private router: Router,
		private languageService: TranslateService,
		public snackBar: MatSnackBar,
		public route: ActivatedRoute
	) {
		super();
	}


	ngOnInit(): void {

	}

	navigateToCreate() {
		this.router.navigate(["/quick-wizard"]);
	}


	navigateToAdd() {
		this.router.navigate(["/datasetcreatewizard"]);
	}


}
