import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { QuickWizardCreateAdd } from './quick-wizard-create-add/quick-wizard-create-add.component';

const routes: Routes = [
	{
		path: '',
		component: DashboardComponent,
		data: {
			breadcrumb: true
		},
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class DashboardRoutingModule { }
