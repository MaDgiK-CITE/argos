import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../../core/services/auth/auth.service';

@Component({
	selector: 'app-dashboard-card',
	templateUrl: './card.component.html',
	styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
	@Input() headerIcon: string;
	@Input() category: string;
	@Input() title: string;
	@Input() footerIcon: string;
	@Input() footContent: string;
	@Input() linearColor: string;
	@Input() boxShadow: string;
	@Input() routelLink: string;
	@Input() hasFootContent = false;
	@Input() buttonTitle: string;
	@Input() buttonRedirectLink: string;

	constructor(private router: Router, private authService: AuthService) { }

	ngOnInit() {
	}

	navigateToUrl() {
		if (!this.isAuthenticated()) { return; }
		this.router.navigate([this.routelLink]);
	}

	createNew() {
		this.router.navigate([this.buttonRedirectLink]);
	}

	isAuthenticated() {
		return this.authService.current() != null;
	}

}
