import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Router } from "@angular/router";
import { AuthService } from "../../../core/services/auth/auth.service";

// declare interface RouteInfo {
//   path: string;
//   title: string;
//   subtitle: string;
//   icon: string;
// }
// export const INFO_COUNTER_ROUTES: RouteInfo[] = [
//   { path: '', title: 'DATA MANAGEMENT PLANS', subtitle: '4', icon: 'view_agenda' },
//   { path: '', title: 'RELATED DATA DESCRIPTIONS', subtitle: '20', icon: 'library_books' },
//   { path: '', title: 'RELATED GRANTS', subtitle: '4', icon: 'work_outline' },
//   { path: '', title: 'ORGANIZATIONS', subtitle: '20', icon: 'scatter_plot' }
// ];

@Component({
	selector: "app-info-counter",
	templateUrl: "./info-counter.component.html",
	styleUrls: ["./info-counter.component.css"]
})
export class InfoCounterComponent implements OnInit {
	@Input() title: string;
	@Input() subtitle: string;
	@Input() icon: string;
	@Input() routerLink: string;
	@Input() buttonRedirectLink: string;
	@Output() onClick: EventEmitter<void> = new EventEmitter<void>();

	infoCounterItems: any[];

	constructor(private router: Router, private authService: AuthService) {}

	ngOnInit() {
		// this.infoCounterItems = INFO_COUNTER_ROUTES.filter(infoCounterItem => infoCounterItem);
	}

	navigateToUrl() {
		if (!this.isAuthenticated()) { return; }
		this.router.navigate([this.routerLink]);
	}

	createNew() {
		this.router.navigate([this.buttonRedirectLink]);
	}

	isAuthenticated() {
		return this.authService.current() != null;
	}

	viewAllClicked() {
		this.onClick.emit();
	}
}
