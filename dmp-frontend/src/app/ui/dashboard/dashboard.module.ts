import { NgModule } from '@angular/core';
import { ExportMethodDialogModule } from '@app/library/export-method-dialog/export-method-dialog.module';
import { CardComponent } from '@app/ui/dashboard/card/card.component';
import { DashboardComponent } from '@app/ui/dashboard/dashboard.component';
import { DashboardRoutingModule } from '@app/ui/dashboard/dashboard.routing';
import { DatasetInfoCounterComponent } from '@app/ui/dashboard/dataset-info-counter/dataset-info-counter.component';
import { DmpInfoCounterComponent } from '@app/ui/dashboard/dmp-info-counter/dmp-info-counter.component';
import { DraftsComponent } from '@app/ui/dashboard/drafts/drafts.component';
import { InfoCounterComponent } from '@app/ui/dashboard/info-counter/info-counter.component';
import { QuickWizardCreateAdd } from '@app/ui/dashboard/quick-wizard-create-add/quick-wizard-create-add.component';
import { RecentActivityComponent } from '@app/ui/dashboard/recent-activity/recent-activity.component';
import { RecentEditedActivityComponent } from '@app/ui/dashboard/recent-edited-activity/recent-edited-activity.component';
import { RecentVisitedActivityComponent } from '@app/ui/dashboard/recent-visited-activity/recent-visited-activity.component';
import { WizardComponent } from '@app/ui/dashboard/wizard/wizard.component';
import { CommonUiModule } from '@common/ui/common-ui.module';
import { ConfirmationDialogModule } from '@common/modules/confirmation-dialog/confirmation-dialog.module';
import { RecentEditedDatasetActivityComponent } from './recent-edited-dataset-activity/recent-edited-dataset-activity.component';
import { DatasetCopyDialogModule } from '../dataset/dataset-wizard/dataset-copy-dialogue/dataset-copy-dialogue.module';
import { RecentEditedDmpActivityComponent } from './recent-edited-dmp-activity/recent-edited-dmp-activity.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormattingModule } from '@app/core/formatting.module';

@NgModule({
	imports: [
		CommonUiModule,
		FormattingModule,
		DashboardRoutingModule,
		ExportMethodDialogModule,
		ConfirmationDialogModule,
		DatasetCopyDialogModule,
		FormsModule,
		ReactiveFormsModule
	],
	declarations: [
		DashboardComponent,
		RecentActivityComponent,
		CardComponent,
		QuickWizardCreateAdd,
		WizardComponent,
		InfoCounterComponent,
		RecentVisitedActivityComponent,
		RecentEditedActivityComponent,
		DraftsComponent,
		DmpInfoCounterComponent,
		DatasetInfoCounterComponent,
		RecentEditedDatasetActivityComponent,
		RecentEditedDmpActivityComponent
	],
	entryComponents: [
		QuickWizardCreateAdd
	]
})
export class DashboardModule { }
