import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { DmpListingModel } from '../../../core/model/dmp/dmp-listing';

@Component({
	selector: 'app-dmp-info-counter',
	templateUrl: './dmp-info-counter.component.html',
	styleUrls: ['./dmp-info-counter.component.css']
})
export class DmpInfoCounterComponent implements OnInit {

	@Input() dmp: DmpListingModel;
	@Output() onClick: EventEmitter<DmpListingModel> = new EventEmitter();

	constructor(public router: Router) { }

	ngOnInit() {
	}

	itemClicked() {
		this.onClick.emit(this.dmp);
	}

	grantClicked(grantId: String) {
		// this.router.navigate(['/datasets/publicEdit/' + grantId]);
	}

}
