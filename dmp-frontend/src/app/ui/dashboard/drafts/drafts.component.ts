import {Component, OnInit, Input, EventEmitter, Output, ViewChild} from '@angular/core';
import { DatasetService } from '../../../core/services/dataset/dataset.service';
import {DataTableMultiTypeRequest, DataTableRequest} from '../../../core/model/data-table/data-table-request';
import { DatasetCriteria } from '../../../core/query/dataset/dataset-criteria';
import { DatasetListingModel } from '../../../core/model/dataset/dataset-listing';
import { AuthService } from '../../../core/services/auth/auth.service';
import { RecentActivityType } from '../../../core/common/enum/recent-activity-type';
import {ActivatedRoute, Router} from '@angular/router';
import { DmpStatus } from '../../../core/common/enum/dmp-status';
import { Principal } from '@app/core/model/auth/principal';
import { TranslateService } from '@ngx-translate/core';
import {debounceTime, map, takeUntil} from 'rxjs/operators';
import { ConfirmationDialogComponent } from '@common/modules/confirmation-dialog/confirmation-dialog.component';
import { DatasetCopyDialogueComponent } from '@app/ui/dataset/dataset-wizard/dataset-copy-dialogue/dataset-copy-dialogue.component';
import {FormControl, FormBuilder, FormGroup} from '@angular/forms';
import { BaseComponent } from '@common/base/base.component';
import { MatDialog } from '@angular/material/dialog';
import { DatasetWizardService } from '@app/core/services/dataset-wizard/dataset-wizard.service';
import { SnackBarNotificationLevel } from '@app/core/services/notification/ui-notification-service';
import * as FileSaver from 'file-saver';
import { EnumUtils } from '@app/core/services/utilities/enum-utils.service';
import { UiNotificationService } from '@app/core/services/notification/ui-notification-service';
import { DmpInvitationDialogComponent } from '@app/ui/dmp/invitation/dmp-invitation-dialog.component';
import { RecentActivityOrder } from '@app/core/common/enum/recent-activity-order';
import { Location } from '@angular/common';
import { Role } from '@app/core/common/enum/role';
import { LockService } from '@app/core/services/lock/lock.service';
import { MatomoService } from '@app/core/services/matomo/matomo-service';
import { HttpClient } from '@angular/common/http';
import {RecentActivityModel} from "@app/core/model/recent-activity/recent-activity.model";
import {DmpEditorModel} from "@app/ui/dmp/editor/dmp-editor.model";
import {DmpService} from "@app/core/services/dmp/dmp.service";
import {DashboardService} from "@app/core/services/dashboard/dashboard.service";
import {RecentActivityCriteria} from "@app/core/query/recent-activity/recent-activity-criteria";
import {RecentDmpModel} from "@app/core/model/recent-activity/recent-dmp-activity.model";
import {DatasetUrlListing} from "@app/core/model/dataset/dataset-url-listing";
import {RecentDatasetModel} from "@app/core/model/recent-activity/recent-dataset-activity.model";
import {DmpListingModel} from "@app/core/model/dmp/dmp-listing";
import {DmpModel} from "@app/core/model/dmp/dmp";
import {GrantTabModel} from "@app/ui/dmp/editor/grant-tab/grant-tab-model";
import {ProjectFormModel} from "@app/ui/dmp/editor/grant-tab/project-form-model";
import {FunderFormModel} from "@app/ui/dmp/editor/grant-tab/funder-form-model";
import {ExtraPropertiesFormModel} from "@app/ui/dmp/editor/general-tab/extra-properties-form.model";
import {CloneDialogComponent} from "@app/ui/dmp/clone/clone-dialog/clone-dialog.component";
import { isNullOrUndefined } from '@app/utilities/enhancers/utils';
import { DmpBlueprintDefinition, SystemFieldType } from '@app/core/model/dmp/dmp-blueprint/dmp-blueprint';
import { DmpProfileService } from '@app/core/services/dmp/dmp-profile.service';

@Component({
	selector: 'app-drafts',
	templateUrl: './drafts.component.html',
	styleUrls: ['./drafts.component.css']
})
export class DraftsComponent extends BaseComponent implements OnInit {

	@Output() totalCountRecentEdited: EventEmitter<any> = new EventEmitter();

	@ViewChild("results") resultsContainer;
	allRecentActivities: RecentActivityModel[];
	recentActivityTypeEnum = RecentActivityType;
	dmpModel: DmpEditorModel;
	isDraft: boolean;
	totalCount: number;
	startIndex: number = 0;
	dmpOffset: number = 0;
	datasetOffset: number = 0;
	offsetLess: number = 0;
	pageSize: number = 5;
	dmpFormGroup: FormGroup;
	hasMoreActivity:boolean = true;
	public formGroup = new FormBuilder().group({
		like: new FormControl(),
		order: new FormControl()
	});
	publicMode = false;

	order = RecentActivityOrder;

	page: number = 1;
	@Input() isActive: boolean = false;

	constructor(
		private route: ActivatedRoute,
		private router: Router,
		public enumUtils: EnumUtils,
		private authentication: AuthService,
		private dmpService: DmpService,
		private dmpProfileService: DmpProfileService,
		private dashboardService: DashboardService,
		private language: TranslateService,
		private dialog: MatDialog,
		private uiNotificationService: UiNotificationService,
		private datasetWizardService: DatasetWizardService,
		private location: Location,
		private lockService: LockService,
		private httpClient: HttpClient,
		private matomoService: MatomoService
	) {
		super();
	}

	ngOnInit() {
		this.matomoService.trackPageView('Drafts');
		this.route.queryParams.subscribe(params => {
			if(this.isActive) {
				let page = (params['page'] === undefined) ? 1 : +params['page'];
				this.page = (page <= 0) ? 1 : page;

				this.datasetOffset = (this.page-1)*this.pageSize;
				this.dmpOffset = (this.page-1)*this.pageSize;
				if(this.page > 1) {
					this.offsetLess = (this.page-2)*this.pageSize;
				}

				let order = params['order'];
				if(order === undefined || (order != this.order.MODIFIED && order != this.order.LABEL)) {
					order = this.order.MODIFIED;
				}
				this.formGroup.get('order').setValue(order);

				let keyword = (params['keyword'] === undefined || params['keyword'].length <= 0) ? "" : params['keyword'];
				this.formGroup.get("like").setValue(keyword);

				this.updateUrl();
			}
		});
		if (this.isAuthenticated()) {
			if (!this.formGroup.get('order').value) {
				this.formGroup.get('order').setValue(this.order.MODIFIED);
			}
			const fields: Array<string> = [((this.formGroup.get('order').value === 'status') || (this.formGroup.get('order').value === 'label') ? '+' : "-") + this.formGroup.get('order').value];
			const allDataTableRequest: DataTableMultiTypeRequest<RecentActivityCriteria> = new DataTableMultiTypeRequest(this.dmpOffset, this.datasetOffset, 5, {fields: fields});
			allDataTableRequest.criteria = new RecentActivityCriteria();
			allDataTableRequest.criteria.like = this.formGroup.get('like').value;
			allDataTableRequest.criteria.order = this.formGroup.get('order').value;
			allDataTableRequest.criteria.status = 0;

			this.dashboardService
				.getRecentActivity(allDataTableRequest)
				.pipe(takeUntil(this._destroyed))
				.subscribe(response => {
					this.allRecentActivities = response;
					this.allRecentActivities.forEach(recentActivity => {
						if (recentActivity.type === RecentActivityType.Dataset) {
							// this.datasetOffset = this.datasetOffset + 1;
							this.datasetOffset = this.page * this.pageSize;
						} else if (recentActivity.type === RecentActivityType.Dmp) {
							// this.dmpOffset = this.dmpOffset + 1;
							this.dmpOffset = this.page * this.pageSize;
						}
					});
					this.totalCountRecentEdited.emit(this.allRecentActivities.length);
					if (this.allRecentActivities.length == 0 && this.page > 1) {
						let queryParams = {type: "recent", page: 1, order: this.formGroup.get("order").value};
						if (this.formGroup.get("like").value) {
							queryParams['keyword'] = this.formGroup.get("like").value;
						}
						this.router.navigate(["/home"], {queryParams: queryParams})
					}
				});
			this.formGroup.get('like').valueChanges
				.pipe(takeUntil(this._destroyed), debounceTime(500))
				.subscribe(x => {
					this.refresh()
				});
			this.formGroup.get('order').valueChanges
				.pipe(takeUntil(this._destroyed))
				.subscribe(x => {
					this.refresh()
				});
		}
	}

	ngOnChanges() {
		if(this.isActive) {
			this.updateUrl();
		}
	}

	updateUrl() {
		let parameters = "?type=drafts"+
			(this.page != 1 ? "&page="+this.page : "") +
			(this.formGroup.get("order").value != this.order.MODIFIED ? "&order="+this.formGroup.get("order").value : "") +
			(this.formGroup.get("like").value ? ("&keyword="+this.formGroup.get("like").value) : "");
		this.location.go(this.router.url.split('?')[0]+parameters);
	}

	getDatasets(activity: RecentDmpModel): DatasetUrlListing[] {
		return activity.datasets;
	}

	getGroupId(activity: RecentDmpModel): string {
		return activity.groupId;
	}

	getDmp(activity: RecentDatasetModel): String {
		return activity.dmp;
	}

	getDmpId(activity: RecentDatasetModel): String {
		return activity.dmpId;
	}

	public isAuthenticated(): boolean {
		return !!this.authentication.current();
	}

	isUserOwner(activity: DmpListingModel): boolean {
		const principal: Principal = this.authentication.current();
		if (principal) return !!activity.users.find(x => (x.role === Role.Owner) && (principal.id === x.id));
	}

	editClicked(dmp: DmpListingModel) {
		this.router.navigate(['/plans/edit/' + dmp.id]);
	}

	deleteDmpClicked(dmp: DmpListingModel) {
		this.lockService.checkLockStatus(dmp.id).pipe(takeUntil(this._destroyed))
			.subscribe(lockStatus => {
				if (!lockStatus) {
					this.openDeleteDmpDialog(dmp);
				} else {
					this.openDmpLockedByUserDialog();
				}
			});
	}

	cloneOrNewVersionClicked(dmp: RecentActivityModel, isNewVersion: boolean) {
		this.dmpService.getSingle(dmp.id).pipe(map(data => data as DmpModel))
			.pipe(takeUntil(this._destroyed))
			.subscribe(data => {
				this.dmpModel = new DmpEditorModel();
				this.dmpModel.grant = new GrantTabModel();
				this.dmpModel.project = new ProjectFormModel();
				this.dmpModel.funder = new FunderFormModel();
				this.dmpModel.extraProperties = new ExtraPropertiesFormModel();
				this.dmpModel.fromModel(data);
				this.dmpModel.status = DmpStatus.Draft;
				this.dmpFormGroup = this.dmpModel.buildForm();

				if (!isNullOrUndefined(this.formGroup.get('profile').value)) { 
					this.dmpProfileService.getSingleBlueprint(this.formGroup.get('profile').value)
						.pipe(takeUntil(this._destroyed))
						.subscribe(result => {
							this.checkForGrant(result.definition);
							this.checkForFunder(result.definition);
							this.checkForProject(result.definition);
						});
				}
				
				if (!isNewVersion) {
					this.dmpFormGroup.get('label').setValue(dmp.title + " New");
				}
				this.openCloneDialog(isNewVersion);
			});
	}

	private checkForGrant(blueprint: DmpBlueprintDefinition) {
		let hasGrant = false;
		blueprint.sections.forEach(section => section.fields.forEach(
			field => {
				if (field.category as unknown === 'SYSTEM' && field.type === SystemFieldType.GRANT) {
					hasGrant = true;
				}
			}
		));
		if (!hasGrant) {
			this.formGroup.removeControl('grant');
		}
	}

	private checkForFunder(blueprint: DmpBlueprintDefinition) {
		let hasFunder = false;
		blueprint.sections.forEach(section => section.fields.forEach(
			field => {
				if (field.category as unknown === 'SYSTEM' && field.type === SystemFieldType.FUNDER) {
					hasFunder = true;
				}
			}
		));
		if (!hasFunder) {
			this.formGroup.removeControl('funder');
		}
	}

	private checkForProject(blueprint: DmpBlueprintDefinition) {
		let hasProject = false;
		blueprint.sections.forEach(section => section.fields.forEach(
			field => {
				if (field.category as unknown === 'SYSTEM' && field.type === SystemFieldType.PROJECT) {
					hasProject = true;
				}
			}
		));
		if (!hasProject) {
			this.formGroup.removeControl('project');
		}
	}

	openCloneDialog(isNewVersion: boolean) {
		const dialogRef = this.dialog.open(CloneDialogComponent, {
			maxWidth: '700px',
			maxHeight: '80vh',
			data: {
				formGroup: this.dmpFormGroup,
				datasets: this.dmpFormGroup.get('datasets').value,
				isNewVersion: isNewVersion,
				confirmButton: this.language.instant('DMP-EDITOR.CLONE-DIALOG.SAVE'),
				cancelButton: this.language.instant('DMP-EDITOR.CLONE-DIALOG.CANCEL'),
			}
		});
		dialogRef.afterClosed().pipe(takeUntil(this._destroyed)).subscribe(result => {
			if (result) {
				if (!isNewVersion) {
					this.dmpService.clone(this.dmpFormGroup.getRawValue(), this.dmpFormGroup.get('id').value)
						.pipe(takeUntil(this._destroyed))
						.subscribe(
							complete => this.onCloneOrNewVersionCallbackSuccess(complete),
							error => this.onCloneOrNewVersionCallbackError(error)
						);
				} else if (isNewVersion) {
					this.dmpService.newVersion(this.dmpFormGroup.getRawValue(), this.dmpFormGroup.get('id').value)
						.pipe(takeUntil(this._destroyed))
						.subscribe(
							complete => this.onCloneOrNewVersionCallbackSuccess(complete),
							error => this.onCloneOrNewVersionCallbackError(error)
						);
				}
			}
		});
	}

	openDeleteDmpDialog(dmp: DmpListingModel) {
		const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
			maxWidth: '300px',
			restoreFocus: false,
			data: {
				message: this.language.instant('GENERAL.CONFIRMATION-DIALOG.DELETE-ITEM'),
				confirmButton: this.language.instant('GENERAL.CONFIRMATION-DIALOG.ACTIONS.DELETE'),
				cancelButton: this.language.instant('GENERAL.CONFIRMATION-DIALOG.ACTIONS.CANCEL'),
				isDeleteConfirmation: true
			}
		});
		dialogRef.afterClosed().pipe(takeUntil(this._destroyed)).subscribe(result => {
			if (result) {
				this.dmpService.delete(dmp.id)
					.pipe(takeUntil(this._destroyed))
					.subscribe(
						complete => this.onDeleteCallbackSuccess(),
						error => this.onDeleteCallbackError(error)
					);
			}
		});
	}

	openDmpLockedByUserDialog() {
		const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
			maxWidth: '400px',
			restoreFocus: false,
			data: {
				message: this.language.instant('DMP-EDITOR.ACTIONS.LOCK')
			}
		});
	}

	openShareDialog(rowId: any, rowName: any) {
		const dialogRef = this.dialog.open(DmpInvitationDialogComponent, {
			// height: '250px',
			// width: '700px',
			autoFocus: false,
			restoreFocus: false,
			data: {
				dmpId: rowId,
				dmpName: rowName
			}
		});
	}

	isDraftDmp(activity: DmpListingModel) {
		return activity.status == DmpStatus.Draft;
	}

	onCallbackSuccess(): void {
		this.uiNotificationService.snackBarNotification(this.language.instant('GENERAL.SNACK-BAR.SUCCESSFUL-UPDATE'), SnackBarNotificationLevel.Success);
		this.router.navigate(['/plans']);
	}

	reloadPage(): void {
		const path = this.location.path();
		this.router.navigateByUrl('/reload', { skipLocationChange: true }).then(() => {
			this.router.navigate([path]);
		});
	}

	onDeleteCallbackSuccess(): void {
		this.uiNotificationService.snackBarNotification(this.language.instant('GENERAL.SNACK-BAR.SUCCESSFUL-DELETE'), SnackBarNotificationLevel.Success);
		this.reloadPage();
	}

	onDeleteCallbackError(error) {
		this.uiNotificationService.snackBarNotification(error.error.message ? this.language.instant(error.error.message) : this.language.instant('GENERAL.SNACK-BAR.UNSUCCESSFUL-DELETE'), SnackBarNotificationLevel.Error);
	}

	onCloneOrNewVersionCallbackSuccess(dmpId: String): void {
		this.uiNotificationService.snackBarNotification(this.language.instant('GENERAL.SNACK-BAR.SUCCESSFUL-UPDATE'), SnackBarNotificationLevel.Success);
		this.router.navigate(['/plans/edit/', dmpId]);
	}

	onCloneOrNewVersionCallbackError(error: any) {
		this.uiNotificationService.snackBarNotification(error.error.message ? this.language.instant(error.error.message) : this.language.instant('GENERAL.SNACK-BAR.UNSUCCESSFUL-CLONE'), SnackBarNotificationLevel.Error);
	}

	redirect(id: string, type: RecentActivityType) {
		switch (type) {
			case RecentActivityType.Grant: {
				this.router.navigate(["grants/edit/" + id]);
				return;
			}
			case RecentActivityType.Dataset: {
				if (this.isAuthenticated()) {
					this.router.navigate(['../datasets/overview/' + id]);
				}
				return;
			}
			case RecentActivityType.Dmp: {
				if (this.isAuthenticated()) {
					this.router.navigate(['../plans/overview/' + id]);
				}
				return;
			}
			default:
				throw new Error("Unsupported Activity Type ");
		}
	}

	navigateToUrl(id: string, type: RecentActivityType): string[] {
		switch (type) {
			case RecentActivityType.Grant: {
				return ["grants/edit/" + id];
			}
			case RecentActivityType.Dataset: {
				if (this.isAuthenticated()) {
					return ['../datasets/overview/' + id];
				} else {
					return ['../explore/publicOverview', id];
				}
			}
			case RecentActivityType.Dmp: {
				if (this.isAuthenticated()) {
					return ['../plans/overview/' + id];
				} else {
					return ['../explore-plans/publicOverview', id];
				}
			}
			default:
				throw new Error("Unsupported Activity Type ");
		}
	}

	roleDisplay(value: any) {
		const principal: Principal = this.authentication.current();
		let role: number;
		if (principal) {
			value.forEach(element => {
				if (principal.id === element.id) {
					role = element.role;
				}
			});
		}
		if (role === 0) {
			return this.language.instant('DMP-LISTING.OWNER');
		}
		else if (role === 1) {
			return this.language.instant('DMP-LISTING.MEMBER');
		}
		else {
			return this.language.instant('DMP-LISTING.OWNER');
		}
	}

	// dmpProfileDisplay(value: any) {
	// 	if (value != null) {
	// 		return value;
	// 	}
	// 	else {
	// 		return "--";
	// 	}
	// }

	downloadXml(id: string) {
		this.dmpService.downloadXML(id)
			.pipe(takeUntil(this._destroyed))
			.subscribe(response => {
				const blob = new Blob([response.body], { type: 'application/xml' });
				const filename = this.getFilenameFromContentDispositionHeader(response.headers.get('Content-Disposition'));

				FileSaver.saveAs(blob, filename);
				this.matomoService.trackDownload('dmps', "xml", id);
			});
	}

	downloadDocx(id: string) {
		this.dmpService.downloadDocx(id)
			.pipe(takeUntil(this._destroyed))
			.subscribe(response => {
				const blob = new Blob([response.body], { type: 'application/msword' });
				const filename = this.getFilenameFromContentDispositionHeader(response.headers.get('Content-Disposition'));

				FileSaver.saveAs(blob, filename);
				this.matomoService.trackDownload('dmps', "docx", id);
			});
	}

	downloadPdf(id: string) {
		this.dmpService.downloadPDF(id)
			.pipe(takeUntil(this._destroyed))
			.subscribe(response => {
				const blob = new Blob([response.body], { type: 'application/pdf' });
				const filename = this.getFilenameFromContentDispositionHeader(response.headers.get('Content-Disposition'));

				FileSaver.saveAs(blob, filename);
				this.matomoService.trackDownload('dmps', "pdf", id);
			});
	}

	downloadJson(id: string) {
		this.dmpService.downloadJson(id)
			.pipe(takeUntil(this._destroyed))
			.subscribe(response => {
				const blob = new Blob([response.body], { type: 'application/json' });
				const filename = this.getFilenameFromContentDispositionHeader(response.headers.get('Content-Disposition'));
				FileSaver.saveAs(blob, filename);
				this.matomoService.trackDownload('dmps', "json", id);
			}, async error => {
				this.onExportCallbackError(error);
			});
	}

	async onExportCallbackError(error: any) {
		const errorJsonText = await error.error.text();
		const errorObj = JSON.parse(errorJsonText);
		this.uiNotificationService.snackBarNotification(errorObj.message, SnackBarNotificationLevel.Error);
	}

	downloadPDF(dataset: DatasetListingModel): void {
		this.datasetWizardService.downloadPDF(dataset.id as string)
			.pipe(takeUntil(this._destroyed))
			.subscribe(response => {
				const blob = new Blob([response.body], { type: 'application/pdf' });
				const filename = this.getFilenameFromContentDispositionHeader(response.headers.get('Content-Disposition'));

				FileSaver.saveAs(blob, filename);
				this.matomoService.trackDownload('datasets', "pdf", dataset.id);
			});
	}

	downloadDOCX(dataset: DatasetListingModel): void {
		this.datasetWizardService.downloadDOCX(dataset.id as string)
			.pipe(takeUntil(this._destroyed))
			.subscribe(response => {
				const blob = new Blob([response.body], { type: 'application/msword' });
				const filename = this.getFilenameFromContentDispositionHeader(response.headers.get('Content-Disposition'));

				FileSaver.saveAs(blob, filename);
				this.matomoService.trackDownload('datasets', "docx", dataset.id);
			});

	}

	downloadXML(dataset: DatasetListingModel): void {
		this.datasetWizardService.downloadXML(dataset.id as string)
			.pipe(takeUntil(this._destroyed))
			.subscribe(response => {
				const blob = new Blob([response.body], { type: 'application/xml' });
				const filename = this.getFilenameFromContentDispositionHeader(response.headers.get('Content-Disposition'));

				FileSaver.saveAs(blob, filename);
				this.matomoService.trackDownload('datasets', "xml", dataset.id);
			});
	}

	getFilenameFromContentDispositionHeader(header: string): string {
		const regex: RegExp = new RegExp(/filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/g);

		const matches = header.match(regex);
		let filename: string;
		for (let i = 0; i < matches.length; i++) {
			const match = matches[i];
			if (match.includes('filename="')) {
				filename = match.substring(10, match.length - 1);
				break;
			} else if (match.includes('filename=')) {
				filename = match.substring(9);
				break;
			}
		}
		return filename;
	}

	viewVersions(rowId: String, rowLabel: String, activity: DmpListingModel) {
		if (activity.public && !this.isUserOwner(activity)) {
			let url = this.router.createUrlTree(['/explore-plans/versions/', rowId, { groupLabel: rowLabel }]);
			window.open(url.toString(), '_blank');
			// this.router.navigate(['/explore-plans/versions/' + rowId], { queryParams: { groupLabel: rowLabel } });
		} else {
			let url = this.router.createUrlTree(['/plans/versions/', rowId, { groupLabel: rowLabel }]);
			window.open(url.toString(), '_blank');
			// this.router.navigate(['/plans/versions/' + rowId], { queryParams: { groupLabel: rowLabel } });
		}
	}

	openDmpSearchDialogue(dataset: RecentDatasetModel) {
		const formControl = new FormControl();
		const dialogRef = this.dialog.open(DatasetCopyDialogueComponent, {
			width: '500px',
			restoreFocus: false,
			data: {
				formControl: formControl,
				datasetId: dataset.id,
				datasetProfileId: dataset.profile.id,
				datasetProfileExist: false,
				confirmButton: this.language.instant('DATASET-WIZARD.DIALOGUE.COPY'),
				cancelButton: this.language.instant('DATASET-WIZARD.DIALOGUE.CANCEL')
			}
		});

		dialogRef.afterClosed().pipe(takeUntil(this._destroyed))
			.subscribe(result => {
				if (result && result.datasetProfileExist) {
					const newDmpId = result.formControl.value.id;
					// let url = this.router.createUrlTree(['/datasets/copy/', result.datasetId, { newDmpId: newDmpId }]);
					// window.open(url.toString(), '_blank');
					this.router.navigate(['/datasets/copy/' + result.datasetId], { queryParams: { newDmpId: newDmpId } });
				}
			});
	}

	deleteDatasetClicked(id: string) {
		this.lockService.checkLockStatus(id).pipe(takeUntil(this._destroyed))
			.subscribe(lockStatus => {
				if (!lockStatus) {
					this.openDeleteDatasetDialog(id);
				} else {
					this.openDatasetLockedByUserDialog();
				}
			});
	}

	openDeleteDatasetDialog(id: string): void {
		const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
			maxWidth: '300px',
			restoreFocus: false,
			data: {
				message: this.language.instant('GENERAL.CONFIRMATION-DIALOG.DELETE-ITEM'),
				confirmButton: this.language.instant('GENERAL.CONFIRMATION-DIALOG.ACTIONS.DELETE'),
				cancelButton: this.language.instant('GENERAL.CONFIRMATION-DIALOG.ACTIONS.CANCEL'),
				isDeleteConfirmation: true
			}
		});
		dialogRef.afterClosed().pipe(takeUntil(this._destroyed)).subscribe(result => {
			if (result) {
				this.datasetWizardService.delete(id)
					.pipe(takeUntil(this._destroyed))
					.subscribe(
						complete => this.onDeleteCallbackSuccess(),
						error => this.onDeleteCallbackError(error)
					);
			}
		});
	}

	openDatasetLockedByUserDialog() {
		const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
			maxWidth: '400px',
			restoreFocus: false,
			data: {
				message: this.language.instant('DATASET-WIZARD.ACTIONS.LOCK')
			}
		});
	}

	refresh(): void {
		this.datasetOffset = 0;
		this.dmpOffset = 0;
		this.page = 1;
		this.updateUrl();

		const fields: Array<string> = [((this.formGroup.get('order').value === 'status') || (this.formGroup.get('order').value === 'label') ? '+' : "-") + this.formGroup.get('order').value];
		// const fields: Array<string> = ["-modified"];
		this.startIndex = 0;
		const allDataTableRequest: DataTableMultiTypeRequest<RecentActivityCriteria> = new DataTableMultiTypeRequest(0, 0, this.pageSize, { fields: fields });
		allDataTableRequest.criteria = new RecentActivityCriteria();
		allDataTableRequest.criteria.like = this.formGroup.get("like").value;
		allDataTableRequest.criteria.order = this.formGroup.get("order").value;
		allDataTableRequest.criteria.status = 0;

		this.dashboardService
			.getRecentActivity(allDataTableRequest)
			.pipe(takeUntil(this._destroyed))
			.subscribe(response => {
				this.allRecentActivities = response;
				this.allRecentActivities.forEach(recentActivity => {
					if (recentActivity.type === RecentActivityType.Dataset) {
						// this.datasetOffset = this.datasetOffset + 1;
						this.datasetOffset = this.page*this.pageSize;
					} else if (recentActivity.type === RecentActivityType.Dmp) {
						// this.dmpOffset = this.dmpOffset + 1;
						this.dmpOffset = this.page*this.pageSize;
					}
				});

				if(response.length< this.pageSize) {
					this.hasMoreActivity = false;
				} else {
					this.hasMoreActivity = true;
				}
				this.totalCountRecentEdited.emit(this.allRecentActivities.length);
			});
	}

	public loadNextOrPrevious(more: boolean = true) {
		const fields: Array<string> = [((this.formGroup.get('order').value === 'status') || (this.formGroup.get('order').value === 'label') ? '+' : "-") + this.formGroup.get('order').value];
		// const fields: Array<string> = ["-modified"];
		let request;
		if(more) {
			request = new DataTableMultiTypeRequest<RecentActivityCriteria>(this.dmpOffset, this.datasetOffset, this.pageSize, {fields: fields});
		} else {
			request = new DataTableMultiTypeRequest<RecentActivityCriteria>(this.offsetLess, this.offsetLess, this.pageSize, {fields: fields});
		}
		request.criteria = new RecentActivityCriteria();
		request.criteria.like = this.formGroup.get("like").value ? this.formGroup.get("like").value : "";
		request.criteria.order = this.formGroup.get("order").value;
		request.criteria.status = 0;

		this.dashboardService.getRecentActivity(request).pipe(takeUntil(this._destroyed)).subscribe(result => {
			if (!result || result.length == 0) {
				this.hasMoreActivity = false;
				// return [];
			} else {
				this.page = this.page + (more ? 1 : -1);
				this.updateUrl();
				// if(more) {
				// 	result.forEach(recentActivity => {
				// 		if (recentActivity.type === RecentActivityType.Dataset) {
				// this.datasetOffset = this.datasetOffset + 1;
				this.datasetOffset = this.page * this.pageSize;
				// } else if (recentActivity.type === RecentActivityType.Dmp) {
				// this.dmpOffset = this.dmpOffset + 1;
				this.dmpOffset = this.page * this.pageSize;
				// }
				// });
				// }
				if (this.page > 1) {
					this.offsetLess = (this.page - 2) * this.pageSize;
				}

				if(result.length < this.pageSize) {
					this.hasMoreActivity = false;
				} else {
					this.hasMoreActivity = true;
				}

				// this.allRecentActivities = this.allRecentActivities.concat(result);
				// this.allRecentActivities = this.allRecentActivities.length > 0 ? this.mergeTwoSortedLists(this.allRecentActivities, result, this.formGroup.get('order').value) : result;
				this.allRecentActivities = result;
				this.totalCountRecentEdited.emit(this.allRecentActivities.length);
				if (more) {
					this.resultsContainer.nativeElement.scrollIntoView();
				}
			}
		});
	}
}
