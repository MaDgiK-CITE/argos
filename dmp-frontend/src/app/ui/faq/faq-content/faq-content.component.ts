import { Component, OnInit, Input } from '@angular/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { FaqService } from '@app/core/services/faq/faq.service';
import { LanguageService } from '@app/core/services/language/language.service';
import { MatomoService } from '@app/core/services/matomo/matomo-service';
import { BaseComponent } from '@common/base/base.component';
import { takeUntil } from 'rxjs/operators';

@Component({
	selector: 'app-faq-content',
	templateUrl: './faq-content.component.html',
	styleUrls: ['./faq-content.component.scss']
})
export class FaqContentComponent extends BaseComponent implements OnInit {

	@Input() isDialog: boolean;

	faqHTMLUrl: SafeResourceUrl;
	sanitizedGuideUrl: any;

	constructor(
		private faqService: FaqService,
		private sanitizer: DomSanitizer,
		private languageService: LanguageService,
		private matomoService: MatomoService
	) { super(); }

	ngOnInit() {
		this.matomoService.trackPageView('FAQ');
		this.faqService.getFaq(this.languageService.getCurrentLanguage())
			.pipe(takeUntil(this._destroyed))
			.subscribe(response => {
				const blob = new Blob([response.body], { type: 'text/html' });
				this.faqHTMLUrl = this.sanitizer.bypassSecurityTrustResourceUrl((window.URL ? URL : webkitURL).createObjectURL(blob));
			});
	}

}
