import { NgModule } from '@angular/core';
import { FormattingModule } from '@app/core/formatting.module';
import { AutoCompleteModule } from '@app/library/auto-complete/auto-complete.module';
import { CommonFormsModule } from '@common/forms/common-forms.module';
import { FormValidationErrorsDialogModule } from '@common/forms/form-validation-errors-dialog/form-validation-errors-dialog.module';
import { CommonUiModule } from '@common/ui/common-ui.module';
import { LoginComponent } from '../auth/login/login.component';
import { LoginModule } from '../auth/login/login.module';
import { AddAccountDialogComponent } from './add-account/add-account-dialog.component';
import { AddAccountDialogModule } from './add-account/add-account-dialog.module';
import { UserProfileComponent } from './user-profile.component';
import { UserProfileRoutingModule } from './user-profile.routing';

@NgModule({
	imports: [
		CommonUiModule,
		CommonFormsModule,
		FormattingModule,
		UserProfileRoutingModule,
		AutoCompleteModule,
		AddAccountDialogModule,
		FormValidationErrorsDialogModule
	],
	declarations: [
		UserProfileComponent
	],
	entryComponents: [
		AddAccountDialogComponent
	]
})
export class UserProfileModule { }
