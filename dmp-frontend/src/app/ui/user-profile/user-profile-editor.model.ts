import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { RoleOrganizationType } from '@app/core/common/enum/role-organization-type';
import { OrganizationModel } from '@app/core/model/organisation/organization';
import { UserListingModel } from '@app/core/model/user/user-listing';

export class UserProfileEditorModel {
	public id: String;
	public name: String;
	public language: any;
	public culture: any;
	public timezone: String;
	public organization: OrganizationModel;
	public roleOrganization: RoleOrganizationType;

	fromModel(item: UserListingModel): UserProfileEditorModel {
		this.id = item.id;
		this.name = item.name;
		this.language = item.language;
		this.timezone = item.timezone;
		this.culture = item.culture;
		this.organization = item.organization;
		this.roleOrganization = item.roleOrganization;

		return this;
	}

	buildForm(availableLanguages: any[]): FormGroup {
		const formGroup = new FormBuilder().group({
			id: new FormControl(this.id),
			name: new FormControl(this.name),
			language: new FormControl(this.language ? availableLanguages.filter(x => x.value === this.language.value).pop() : '', [Validators.required]),
			timezone: new FormControl(this.timezone, [Validators.required]),
			culture: new FormControl(this.culture, [Validators.required]),
			organization: new FormControl(this.organization),
			roleOrganization: new FormControl(this.roleOrganization),
		});

		return formGroup;
	}

}
