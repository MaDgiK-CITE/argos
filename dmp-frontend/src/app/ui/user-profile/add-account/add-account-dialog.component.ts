import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserMergeRequestModel } from '@app/core/model/merge/user-merge-request';
import { MergeLoginService } from '@app/ui/auth/login/utilities/merge-login.service';
import { DatasetDescriptionFormEditorModel } from '@app/ui/misc/dataset-description-form/dataset-description-form.model';

@Component({
	selector: 'app-add-account-dialog-component',
	templateUrl: 'add-account-dialog.component.html',
	styleUrls: ['./add-account-dialog.component.scss'],
})
export class AddAccountDialogComponent implements OnInit {

	datasetProfileDefinitionModel: DatasetDescriptionFormEditorModel;
	datasetProfileDefinitionFormGroup: FormGroup;
	progressIndication = false;
	public hasEmail = true;
	private request: UserMergeRequestModel;

	constructor(
		private mergeLoginService: MergeLoginService,
		public dialogRef: MatDialogRef<AddAccountDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any
	) {

	}

	ngOnInit(): void {
		this.mergeLoginService.getObservable().subscribe(result => {
			if (result !== undefined) {
				if (!(result.email !== undefined && result.email !== null)) {
					this.request = result;
					this.hasEmail = false;
				} else {
					this.dialogRef.close(result);
				}
			}
		});
	}

	add(): void {
		this.request.email = 'email';
		this.dialogRef.close(this.request);
	}

	cancel(): void {
		this.dialogRef.close();
	}

	closeDialog(): void {
		this.dialogRef.close();
	}

}
