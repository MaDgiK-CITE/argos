import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '@app/core/services/configuration/configuration.service';
import { DomSanitizer } from '@angular/platform-browser';
import { BaseComponent } from '@common/base/base.component';
import { takeUntil } from 'rxjs/operators';

@Component({
	selector: 'app-splash',
	templateUrl: './splash.component.html',
	styleUrls: ['./splash.component.scss']
})
export class SplashComponent extends BaseComponent implements OnInit {

	splashHTML: any;

	constructor(
		private httpClient: HttpClient,
		private configurationService: ConfigurationService,
		private sanitizer: DomSanitizer
	) {
		super();
	}

	ngOnInit() {
		// this.httpClient.get(this.configurationService.splashPath, {responseType: "text"})
		// .pipe(takeUntil(this._destroyed)).subscribe(response => {
		// 	const blob = new Blob([response], { type: 'text/html' });
		// 	this.readBlob(blob);
		// });
		// this.splashHTML = this.sanitizer.bypassSecurityTrustHtml(`${this.configurationService.app}/${this.configurationService.splashPath}`);
  }

	// readBlob(blob: Blob) {
	// 	const fr = new FileReader();
	// 	fr.onload = ev => {
	// 		this.splashHTML = this.sanitizer.bypassSecurityTrustHtml(fr.result as string);
	// 		//this.parse();
	// 	};
	// 	fr.readAsText(blob);
	// }

	resizeFrame() {
		const frame: HTMLIFrameElement = (document.getElementById('splash') as HTMLIFrameElement);
		frame.style.height = frame.contentWindow.document.body.scrollHeight + 'px';
	}

	getSplashUrl() {
		// return this.sanitizer.bypassSecurityTrustHtml(this.configurationService.splashPath);
	}

}
