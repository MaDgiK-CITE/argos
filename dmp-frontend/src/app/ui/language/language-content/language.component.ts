import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '@app/core/services/auth/auth.service';
import { LanguageService } from '@app/core/services/language/language.service';
import { UserService } from '@app/core/services/user/user.service';
import { takeUntil } from 'rxjs/operators';
import { BaseComponent } from '@common/base/base.component';
import { ConfigurationService } from '@app/core/services/configuration/configuration.service';

@Component({
	selector: 'app-language',
	templateUrl: './language.component.html',
	styleUrls: ['./language.component.scss']
})
export class LanguageComponent extends BaseComponent implements OnInit {

	@Output() languageChange: EventEmitter<any> = new EventEmitter();
	languages = [];

	constructor(
		private router: Router,
		private authentication: AuthService,
		private languageService: LanguageService,
		private userService: UserService,
		private configurationService:ConfigurationService
	) {
		super();
		this.languages = this.configurationService.availableLanguages;
	}

	ngOnInit() {
		this.languageChange.emit(this.getCurrentLanguage().value)
	}

	public isAuthenticated(): boolean {
		return !(!this.authentication.current());
	}

	public getCurrentLanguage(): any {
		const lang = this.languages.find(lang => lang.value === this.languageService.getCurrentLanguage());
		return lang;
	}

	onLanguageSelected(selectedLanguage: any) {
		if (this.isAuthenticated()) {
			const langMap = new Map<string, string>();
			langMap.set('language', selectedLanguage.value);
			this.userService.updateUserSettings({ language: this.languages.find(lang => lang.value === selectedLanguage.value) })
				.pipe(takeUntil(this._destroyed))
				.subscribe((response) => {
					this.languageService.changeLanguage(selectedLanguage.value);
					this.authentication.me()
						.pipe(takeUntil(this._destroyed))
						.subscribe(innerResponse => { this.router.navigateByUrl(this.router.url); });
				},
					error => {
						console.log(error);
					});
		} else {
			this.languageService.changeLanguage(selectedLanguage.value);
			this.router.navigateByUrl(this.router.url);
		}
		this.languageChange.emit(selectedLanguage.value);
	}

}
