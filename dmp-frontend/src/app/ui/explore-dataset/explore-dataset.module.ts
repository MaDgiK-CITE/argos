import { NgModule } from '@angular/core';
import { FormattingModule } from '@app/core/formatting.module';
import { AutoCompleteModule } from '@app/library/auto-complete/auto-complete.module';
import { ExploreDatasetListingComponent } from '@app/ui/explore-dataset/explore-dataset-listing.component';
import { ExploreDatasetRoutingModule } from '@app/ui/explore-dataset/explore-dataset.routing';
import { ExploreDatasetFilterItemComponent } from '@app/ui/explore-dataset/filters/explore-dataset-filter-item/explore-dataset-filter-item.component';
import { ExploreDatasetFiltersComponent } from '@app/ui/explore-dataset/filters/explore-dataset-filters.component';
import { ExploreDatasetListingItemComponent } from '@app/ui/explore-dataset/listing-item/explore-dataset-listing-item.component';
import { CommonFormsModule } from '@common/forms/common-forms.module';
import { CommonUiModule } from '@common/ui/common-ui.module';
import { DatasetOverviewModule } from '../dataset/overview/dataset-overview.module';

@NgModule({
	imports: [
		CommonUiModule,
		CommonFormsModule,
		AutoCompleteModule,
		ExploreDatasetRoutingModule,
		FormattingModule,
		DatasetOverviewModule
	],
	declarations: [
		ExploreDatasetListingComponent,
		ExploreDatasetFiltersComponent,
		ExploreDatasetFilterItemComponent,
		ExploreDatasetListingItemComponent
	]
})
export class ExploreDatasetModule { }