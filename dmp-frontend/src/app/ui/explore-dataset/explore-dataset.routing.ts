import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ExploreDatasetListingComponent } from './explore-dataset-listing.component';
import { DatasetOverviewComponent } from '../dataset/overview/dataset-overview.component';

const routes: Routes = [
	{
		path: '',
		component: ExploreDatasetListingComponent,
		data: {
			breadcrumb: true
		},
	},
	{
		path: 'overview/:publicId',
		component: DatasetOverviewComponent,
		data: {
			breadcrumb: true,
			title: 'GENERAL.TITLES.EXPLORE-PLANS-OVERVIEW'
		},
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class ExploreDatasetRoutingModule { }