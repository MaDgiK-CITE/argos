import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DatasetListingModel } from '../../../core/model/dataset/dataset-listing';
import { DatasetStatus } from '../../../core/common/enum/dataset-status';

@Component({
	selector: 'app-explore-dataset-listing-item-component',
	templateUrl: './explore-dataset-listing-item.component.html',
	styleUrls: ['./explore-dataset-listing-item.component.scss']
})
export class ExploreDatasetListingItemComponent implements OnInit {

	@Input() dataset: DatasetListingModel;
	@Input() showDivider: boolean = true;
	@Output() onClick: EventEmitter<DatasetListingModel> = new EventEmitter();

	isDraft: boolean;

	constructor() { }

	ngOnInit() {
		if (this.dataset.status == DatasetStatus.Draft) { this.isDraft = true }
		else { this.isDraft = false }
	}

	// itemClicked() {
	// 	this.onClick.emit(this.dataset);
	// }

}
