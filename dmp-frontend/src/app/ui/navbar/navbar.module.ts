import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BreadcrumbModule } from '@app/ui/misc/breadcrumb/breadcrumb.module';
import { SearchComponent } from '@app/ui/misc/search/search.component';
import { NavbarComponent } from '@app/ui/navbar/navbar.component';
import { CommonFormsModule } from '@common/forms/common-forms.module';
import { CommonUiModule } from '@common/ui/common-ui.module';
import { LanguageModule } from '../language/language.module';

@NgModule({
	imports: [
		CommonUiModule,
		CommonFormsModule,
		RouterModule,
		BreadcrumbModule,
		LanguageModule
	],
	declarations: [
		NavbarComponent,
		SearchComponent
	],
	entryComponents: [],
	exports: [NavbarComponent]
})
export class NavbarModule { }
