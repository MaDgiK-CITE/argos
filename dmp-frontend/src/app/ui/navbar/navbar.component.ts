import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatMenuTrigger } from '@angular/material/menu';
import { Router } from '@angular/router';
import { AppRole } from '@app/core/common/enum/app-role';
import { UserListingModel } from '@app/core/model/user/user-listing';
import { AuthService } from '@app/core/services/auth/auth.service';
import { ConfigurationService } from '@app/core/services/configuration/configuration.service';
import { LanguageService } from '@app/core/services/language/language.service';
import { MatomoService } from '@app/core/services/matomo/matomo-service';
import { ProgressIndicationService } from '@app/core/services/progress-indication/progress-indication-service';
import { SideNavService } from '@app/core/services/sidenav/side-nav.sevice';
import { UserService } from '@app/core/services/user/user.service';
import { BaseComponent } from '@common/base/base.component';
import { takeUntil } from 'rxjs/operators';
import { StartNewDmpDialogComponent } from '../dmp/start-new-dmp-dialogue/start-new-dmp-dialog.component';
import { FaqDialogComponent } from '../faq/dialog/faq-dialog.component';
import { UserDialogComponent } from '../misc/navigation/user-dialog/user-dialog.component';
import { DATASETS_ROUTES, DMP_ROUTES, GENERAL_ROUTES } from '../sidebar/sidebar.component';
@Component({
	selector: 'app-navbar',
	templateUrl: './navbar.component.html',
	styleUrls: ['./navbar.component.css', './navbar.component.scss']
})
export class NavbarComponent extends BaseComponent implements OnInit {
	progressIndication = false;
	private listTitles: any[];
	location: Location;
	mobile_menu_visible: any = 0;
	private toggleButton: any;
	private sidebarVisible: boolean;
	languages = [];
	currentRoute: string;
	selectedLanguage: string;
	private user: UserListingModel;
	@Output() sidebarToggled: EventEmitter<any> = new EventEmitter();
	@ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;

	constructor(location: Location,
		private element: ElementRef,
		private router: Router,
		private authentication: AuthService,
		private dialog: MatDialog,
		private progressIndicationService: ProgressIndicationService,
		private languageService: LanguageService,
		public userService: UserService,
		private httpClient: HttpClient,
		private matomoService: MatomoService,
		private sidenavService: SideNavService,
		private configurationService: ConfigurationService
	) {
		super();
		this.location = location;
		this.sidebarVisible = false;
		this.languages = this.configurationService.availableLanguages;
		this.selectedLanguage = this.configurationService.defaultLanguage || 'en';
	}

	ngOnInit() {
		this.matomoService.trackPageView('Navbar');
		this.currentRoute = this.router.url;
		this.listTitles = GENERAL_ROUTES.filter(listTitle => listTitle);
		this.listTitles.push(DMP_ROUTES.filter(listTitle => listTitle));
		//   this.listTitles.push(HISTORY_ROUTES.filter(listTitle => listTitle));
		this.listTitles.push(DATASETS_ROUTES.filter(listTitle => listTitle));
		// const navbar: HTMLElement = this.element.nativeElement;
		// this.toggleButton = navbar.getElementsByClassName('navbar-toggler')[0];
		// this.router.events.subscribe((event) => {
		// 	this.sidebarClose();
		// 	var $layer: any = document.getElementsByClassName('close-layer')[0];
		// 	this.currentRoute = this.router.url;
		// 	if ($layer) {
		// 		$layer.remove();
		// 		this.mobile_menu_visible = 0;
		// 	}
		// });

		this.progressIndicationService.getProgressIndicationObservable().pipe(takeUntil(this._destroyed)).subscribe(x => {
			setTimeout(() => { this.progressIndication = x; });
		});
	}

	public isAuthenticated(): boolean {
		return !(!this.authentication.current());
	}

	public onInvalidUrl(): boolean {
		return this.currentRoute === '/language-editor' || this.currentRoute === '/profile';
	}

	openMyMenu() {
		this.trigger.openMenu();
	}

	closeMyMenu() {
		this.trigger.closeMenu();
	}

	sidebarOpen() {
		const toggleButton = this.toggleButton;
		const body = document.getElementsByTagName('body')[0];
		setTimeout(function () {
			toggleButton.classList.add('toggled');
		}, 500);

		body.classList.add('nav-open');

		this.sidebarVisible = true;
	};
	sidebarClose() {
		const body = document.getElementsByTagName('body')[0];
		this.toggleButton.classList.remove('toggled');
		this.sidebarVisible = false;
		body.classList.remove('nav-open');
	};
	sidebarToggle() {
		// const toggleButton = this.toggleButton;
		// const body = document.getElementsByTagName('body')[0];
		var $toggle = document.getElementsByClassName('navbar-toggler')[0];

		if (this.sidebarVisible === false) {
			this.sidebarOpen();
		} else {
			this.sidebarClose();
		}
		const body = document.getElementsByTagName('body')[0];

		if (this.mobile_menu_visible == 1) {
			// $('html').removeClass('nav-open');
			body.classList.remove('nav-open');
			if ($layer) {
				$layer.remove();
			}
			setTimeout(function () {
				$toggle.classList.remove('toggled');
			}, 400);

			this.mobile_menu_visible = 0;
		} else {
			setTimeout(function () {
				$toggle.classList.add('toggled');
			}, 430);

			var $layer = document.createElement('div');
			$layer.setAttribute('class', 'close-layer');


			if (body.querySelectorAll('.main-panel')) {
				document.getElementsByClassName('main-panel')[0].appendChild($layer);
			} else if (body.classList.contains('off-canvas-sidebar')) {
				document.getElementsByClassName('wrapper-full-page')[0].appendChild($layer);
			}

			setTimeout(function () {
				$layer.classList.add('visible');
			}, 100);

			$layer.onclick = function () { //asign a function
				body.classList.remove('nav-open');
				this.mobile_menu_visible = 0;
				$layer.classList.remove('visible');
				setTimeout(function () {
					$layer.remove();
					$toggle.classList.remove('toggled');
				}, 400);
			}.bind(this);

			body.classList.add('nav-open');
			this.mobile_menu_visible = 1;

		}
	};

	getTitle() {
		var titlee = this.location.prepareExternalUrl(this.location.path());
		if (titlee.charAt(0) === '#') {
			titlee = titlee.slice(2);
		}
		titlee = titlee.split('/').pop();

		for (var item = 0; item < this.listTitles.length; item++) {
			if (this.listTitles[item].path === titlee) {
				return this.listTitles[item].title;
			}
		}
		return 'Dashboard';
	}

	public getCurrentLanguage(): any {
		const lang = this.languages.find(lang => lang.value === this.languageService.getCurrentLanguage());
		return lang;
	}

	public getPrincipalName(): string {
		return this.authentication.current().name;
	}

	public principalHasAvatar(): boolean {
		return this.authentication.current().avatarUrl != null && this.authentication.current().avatarUrl.length > 0;
	}

	public getPrincipalAvatar(): string {
		return this.authentication.current().avatarUrl;
	}

	public getDefaultAvatar(): string {
		return 'assets/images/profile-placeholder.png';
	}

	public applyFallbackAvatar(ev: Event) {
		(ev.target as HTMLImageElement).src = this.getDefaultAvatar();
	}

	public isAdmin(): boolean {
		if (!this.authentication.current()) { return false; }
		const principalRoles = this.authentication.current().authorities;
		for (let i = 0; i < principalRoles.length; i++) {
			if (principalRoles[i] === AppRole.Admin) {
				return true;
			}
		}
		return false;
	}

	openProfile() {
		const dialogRef = this.dialog.open(UserDialogComponent, {
			hasBackdrop: true,
			autoFocus: false,
			closeOnNavigation: true,
			disableClose: false,
			position: { top: '64px', right: '1em' },
			panelClass: 'custom-userbox'
		});
	}

	openFaqDialog() {
		if (this.dialog.openDialogs.length > 0) {
			this.dialog.closeAll();
		}
		else {
			const dialogRef = this.dialog.open(FaqDialogComponent, {
				disableClose: true,
				data: {
					isDialog: true
				},
				width: '100%'
			});
		}
	}

	getLanguage(selectedLanguage: string) {
		this.selectedLanguage = selectedLanguage;
	}

	toggleNavbar(event) {
		document.getElementById('hamburger').classList.toggle("change");
	}

	sidebarToggleOutput(event) {
		this.sidebarToggled.emit(event);
	}

	toggleMyNav(event) {
		this.sidenavService.toggle();
	}

	openNewDmpDialog() {
		if (this.dialog.openDialogs.length > 0) {
			this.dialog.closeAll();
		} else if (!this.isAuthenticated()) {
			this.router.navigate(['/login']);
		} else {
			const dialogRef = this.dialog.open(StartNewDmpDialogComponent, {
				disableClose: false,
				data: {
					isDialog: true
				}
			});
		}
	}

	logout() {
		this.authentication.logout();
	}

}
