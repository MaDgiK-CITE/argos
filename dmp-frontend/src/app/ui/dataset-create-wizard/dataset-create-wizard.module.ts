import { NgModule } from '@angular/core';
import { FormattingModule } from '@app/core/formatting.module';
import { AutoCompleteModule } from '@app/library/auto-complete/auto-complete.module';
import { ConfirmationDialogModule } from '@common/modules/confirmation-dialog/confirmation-dialog.module';
import { CanDeactivateGuard } from '@app/library/deactivate/can-deactivate.guard';
import { UrlListingModule } from '@app/library/url-listing/url-listing.module';
import { DatasetCreateWizard } from '@app/ui/dataset-create-wizard/dataset-create-wizard.component';
import { DatasetCreateWizardRoutingModule } from '@app/ui/dataset-create-wizard/dataset-create-wizard.routing';
import { DatasetDmpSelector } from '@app/ui/dataset-create-wizard/dmp-selector/dataset-dmp-selector.component';
import { DatasetDescriptionFormModule } from '@app/ui/misc/dataset-description-form/dataset-description-form.module';
import { OuickWizardModule } from '@app/ui/quick-wizard/quick-wizard.module';
import { QuickWizardRoutingModule } from '@app/ui/quick-wizard/quick-wizard.routing';
import { CommonFormsModule } from '@common/forms/common-forms.module';
import { CommonUiModule } from '@common/ui/common-ui.module';

@NgModule({
	imports: [
		DatasetCreateWizardRoutingModule,
		CommonUiModule,
		CommonFormsModule,
		AutoCompleteModule,
		FormattingModule,
		UrlListingModule,
		ConfirmationDialogModule,
		QuickWizardRoutingModule,
		DatasetDescriptionFormModule,
		OuickWizardModule,

	],
	declarations: [
		DatasetCreateWizard,
		DatasetDmpSelector,
	],
	entryComponents: [
	],
	providers: [
		CanDeactivateGuard
	]
})
export class DatasetCreateWizardModule { }
