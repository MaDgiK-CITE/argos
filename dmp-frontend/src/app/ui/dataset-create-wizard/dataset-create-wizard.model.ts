import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { DmpCreateWizardFormModel } from '@app/core/model/dmp/dmp-create-wizard/dmp-create-wizard-form.model';
import { DatasetWizardEditorModel } from '@app/ui/dataset/dataset-wizard/dataset-wizard-editor.model';
import { DatasetEditorWizardModel } from '@app/ui/quick-wizard/dataset-editor/dataset-editor-wizard-model';
import { BackendErrorValidator } from '@common/forms/validation/custom-validator';
import { ValidationErrorModel } from '@common/forms/validation/error-model/validation-error-model';
import { ValidationContext } from '@common/forms/validation/validation-context';


export class DatasetCreateWizardModel {
	public dmpMeta: DmpCreateWizardFormModel;
	public datasets: DatasetEditorWizardModel;
	public validationErrorModel: ValidationErrorModel = new ValidationErrorModel();

	fromModelDmp(item: DmpCreateWizardFormModel): DatasetCreateWizardModel {
		this.dmpMeta.fromModel(item);
		return this;
	}

	fromModelDataset(item: DatasetWizardEditorModel[]): DatasetCreateWizardModel {
		this.datasets.fromModel(item);
		return this;
	}

	buildForm(context: ValidationContext = null): FormGroup {
		if (context == null) { context = this.createValidationContext(); }
		const formBuilder = new FormBuilder();
		const formGroup = formBuilder.group({
			dmpMeta: new DmpCreateWizardFormModel().buildForm(),
			datasets: new DatasetEditorWizardModel().buildForm()
		});

		return formGroup;
	}

	createValidationContext(): ValidationContext {
		const baseContext: ValidationContext = new ValidationContext();
		baseContext.validation.push({ key: 'label', validators: [Validators.required, BackendErrorValidator(this.validationErrorModel, 'label')] });
		baseContext.validation.push({ key: 'status', validators: [BackendErrorValidator(this.validationErrorModel, 'status')] });

		return baseContext;
	}
}
