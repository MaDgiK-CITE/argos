import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DatasetCreateWizard } from './dataset-create-wizard.component';
import { DatasetDmpSelector } from './dmp-selector/dataset-dmp-selector.component';
import { CanDeactivateGuard } from '../../library/deactivate/can-deactivate.guard';
import { AuthGuard } from '@app/core/auth-guard.service';

const routes: Routes = [
	{
		path: '',
		component: DatasetCreateWizard,
		canActivate: [AuthGuard],
		data: {
			breadcrumb: true
		},
		canDeactivate: [CanDeactivateGuard]
	},
	{
		path: '',
		component: DatasetDmpSelector,
		canActivate: [AuthGuard],
		data: {
			breadcrumb: true
		},
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class DatasetCreateWizardRoutingModule { }
