import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { QuickWizardEditorComponent } from './quick-wizard-editor/quick-wizard-editor.component';
import { CanDeactivateGuard } from '../../library/deactivate/can-deactivate.guard';
import { AuthGuard } from '@app/core/auth-guard.service';

const routes: Routes = [
	{
		path: '',
		component: QuickWizardEditorComponent,
		canActivate: [AuthGuard],
		data: {
			breadcrumb: true
		},
		canDeactivate: [CanDeactivateGuard]
	},
	// {
	// 	path: 'grant',
	// 	component: GrantEditorWizardComponent,
	// 	data: {
	// 		breadcrumb: true
	// 	},
	// },
	// {
	// 	path: 'dmp',
	// 	component: DmpEditorWizardComponent,
	// 	data: {
	// 		breadcrumb: true
	// 	},
	// },
	// {
	// 	path: 'dataset',
	// 	component: DatasetEditorWizardComponent,
	// 	data: {
	// 		breadcrumb: true
	// 	},
	// }
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class QuickWizardRoutingModule { }
