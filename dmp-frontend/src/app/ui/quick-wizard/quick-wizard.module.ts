import { NgModule } from '@angular/core';
import { AutoCompleteModule } from '@app/library/auto-complete/auto-complete.module';
import { ConfirmationDialogModule } from '@common/modules/confirmation-dialog/confirmation-dialog.module';
import { CanDeactivateGuard } from '@app/library/deactivate/can-deactivate.guard';
import { UrlListingModule } from '@app/library/url-listing/url-listing.module';
import { DmpModule } from '@app/ui/dmp/dmp.module';
import { DatasetDescriptionFormModule } from '@app/ui/misc/dataset-description-form/dataset-description-form.module';
import { DatasetEditorWizardComponent } from '@app/ui/quick-wizard/dataset-editor/dataset-editor-wizard.component';
import { DmpEditorWizardComponent } from '@app/ui/quick-wizard/dmp-editor/dmp-editor-wizard.component';
import { FunderEditorWizardComponent } from '@app/ui/quick-wizard/funder-editor/funder-editor-wizard.component';
import { GrantEditorWizardComponent } from '@app/ui/quick-wizard/grant-editor/grant-editor-wizard.component';
import { ProjectEditorWizardComponent } from '@app/ui/quick-wizard/project-editor/project-editor-wizard.component';
import { QuickWizardEditorComponent } from '@app/ui/quick-wizard/quick-wizard-editor/quick-wizard-editor.component';
import { QuickWizardRoutingModule } from '@app/ui/quick-wizard/quick-wizard.routing';
import { CommonFormsModule } from '@common/forms/common-forms.module';
import { CommonUiModule } from '@common/ui/common-ui.module';
import { TableOfContentsModule } from '../misc/dataset-description-form/tableOfContentsMaterial/table-of-contents.module';
import { AngularStickyThingsModule } from '@w11k/angular-sticky-things';

@NgModule({
	imports: [
		CommonUiModule,
		CommonFormsModule,
		UrlListingModule,
		AutoCompleteModule,
		ConfirmationDialogModule,
		QuickWizardRoutingModule,
		DatasetDescriptionFormModule,
		DmpModule,
		TableOfContentsModule,
		AngularStickyThingsModule
	],
	declarations: [
		GrantEditorWizardComponent,
		DmpEditorWizardComponent,
		QuickWizardEditorComponent,
		DatasetEditorWizardComponent,
		FunderEditorWizardComponent,
		ProjectEditorWizardComponent
	],
	exports: [
		DatasetEditorWizardComponent,
	],
	providers: [
		CanDeactivateGuard
	]
})
export class OuickWizardModule { }
