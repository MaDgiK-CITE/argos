import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Status } from '@app/core/common/enum/status';
import { DatasetProfileModel } from '@app/core/model/dataset/dataset-profile';
import { DmpProfileDefinition } from '@app/core/model/dmp-profile/dmp-profile';
import { DmpModel } from '@app/core/model/dmp/dmp';
import { ValidJsonValidator } from '@app/library/auto-complete/auto-complete-custom-validator';
import { BackendErrorValidator } from '@common/forms/validation/custom-validator';
import { ValidationErrorModel } from '@common/forms/validation/error-model/validation-error-model';
import { ValidationContext } from '@common/forms/validation/validation-context';
import { DmpStatus } from '@app/core/common/enum/dmp-status';

export class DmpEditorWizardModel {
	public id: string;
	public label: string;
	public status: DmpStatus = DmpStatus.Draft;
	public description: String;
	public datasetProfile: DatasetProfileModel;
	public definition: DmpProfileDefinition;
	public validationErrorModel: ValidationErrorModel = new ValidationErrorModel();
	public language: String;

	fromModel(item: DmpModel): DmpEditorWizardModel {
		this.id = item.id;
		this.label = item.label;
		this.status = item.status;
		this.description = item.description;
		this.datasetProfile = {id: item.profiles[0].descriptionTemplateId, label: item.profiles[0].label, description: ""};
		this.language = item.language;
		return this;
	}

	buildForm(context: ValidationContext = null, disabled: boolean = false): FormGroup {
		if (context == null) { context = this.createValidationContext(); }
		const formGroup = new FormBuilder().group({
			id: [{ value: this.id, disabled: disabled }, context.getValidation('id').validators],
			label: [{ value: this.label, disabled: disabled }, context.getValidation('label').validators],
			status: [{ value: this.status, disabled: disabled }, context.getValidation('status').validators],
			description: [{ value: this.description, disabled: disabled }, context.getValidation('description').validators],
			datasetProfile: [{ value: this.datasetProfile, disabled: disabled }, context.getValidation('datasetProfile').validators],
			language: [{ value: this.language, disabled: disabled }, context.getValidation('language').validators],
		});
		return formGroup;
	}

	createValidationContext(): ValidationContext {
		const baseContext: ValidationContext = new ValidationContext();
		baseContext.validation.push({ key: 'id', validators: [BackendErrorValidator(this.validationErrorModel, 'id')] });
		baseContext.validation.push({ key: 'label', validators: [Validators.required, BackendErrorValidator(this.validationErrorModel, 'label')] });
		baseContext.validation.push({ key: 'status', validators: [Validators.required, BackendErrorValidator(this.validationErrorModel, 'status')] });
		baseContext.validation.push({ key: 'description', validators: [BackendErrorValidator(this.validationErrorModel, 'description')] });
		baseContext.validation.push({ key: 'datasetProfile', validators: [Validators.required, ValidJsonValidator, BackendErrorValidator(this.validationErrorModel, 'datasetProfile')] });
		baseContext.validation.push({ key: 'language', validators: [Validators.required, ValidJsonValidator, BackendErrorValidator(this.validationErrorModel, 'language')] });
		return baseContext;
	}
}


