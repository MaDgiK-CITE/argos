import { Component, Input, OnInit } from "@angular/core";
import { FormArray, FormGroup } from "@angular/forms";
import { DatasetWizardService } from '@app/core/services/dataset-wizard/dataset-wizard.service';
import { BreadcrumbItem } from '@app/ui/misc/breadcrumb/definition/breadcrumb-item';
import { IBreadCrumbComponent } from '@app/ui/misc/breadcrumb/definition/IBreadCrumbComponent';
import { DatasetDescriptionFormEditorModel } from '@app/ui/misc/dataset-description-form/dataset-description-form.model';
import { QuickWizardDatasetDescriptionModel } from '@app/ui/quick-wizard/dataset-editor/quick-wizard-dataset-description-model';
import { BaseComponent } from '@common/base/base.component';
import { TranslateService } from "@ngx-translate/core";
import { Observable } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { LinkToScroll } from '@app/ui/misc/dataset-description-form/tableOfContentsMaterial/table-of-contents';

@Component({
	selector: 'app-dataset-editor-wizard-component',
	templateUrl: 'dataset-editor-wizard.component.html',
	styleUrls: ['./dataset-editor-wizard.component.scss']
})
export class DatasetEditorWizardComponent extends BaseComponent implements OnInit, IBreadCrumbComponent {
	breadCrumbs: Observable<BreadcrumbItem[]>;

	@Input() formGroup: FormGroup;
	@Input() datasetProfile: FormGroup; // DatasetProfileModel;
	@Input() datasetLabel: string;
	editedDataset: boolean = false;
	dataset: DatasetDescriptionFormEditorModel;
	public datasetProfileDefinition: DatasetDescriptionFormEditorModel;
	public lastIndexOfDataset = 0;
	public toggleButton = 0;
	public _inputValue: string;

	isFirst: boolean;

	constructor(
		private datasetWizardService: DatasetWizardService,
		public language: TranslateService,
	) {
		super();
	}

	ngOnInit(): void {
		this.datasetWizardService.getDefinition(this.datasetProfile.value["id"])
			.pipe(takeUntil(this._destroyed))
			.subscribe(item => {
				this.datasetProfileDefinition = new DatasetDescriptionFormEditorModel().fromModel(item);
				this.onValChange("list");
				const length = (this.formGroup.get('datasets').get('datasetsList') as FormArray).length;
				if (length == 0) {
					this.lastIndexOfDataset = length;
					this.isFirst = true;
					this.addDataset(this.isFirst);
					this.onValChange("dataset");
				}
			});
	}

	onValChange(event: any) {
		if (event == "list") {
			this.toggleButton = 0;
			this.editedDataset = false;
			this._inputValue = "list";
		} else if (event == "add") {
			this.addDataset(this.isFirst);
			this.toggleButton = 2;
			this._inputValue = "dataset";
		} else if (event == "dataset") {
			this.toggleButton = 2;
			this._inputValue = "dataset";
		}
	}

	editDataset(index: number) {
		// this.lastIndexOfDataset = index;
		this.toggleButton = 2;
		this.editedDataset = true;
		this._inputValue = "dataset"
	}

	deleteDataset(index: number) {//TODO: delete Dataset From List
		this.lastIndexOfDataset = index;
		this.toggleButton = 0;
		this.editedDataset = false;
		this._inputValue = "list";
		(this.formGroup.get('datasets').get('datasetsList') as FormArray).removeAt(index);
		if (index == 0) {
			this.isFirst = true;
		}
	}

	addDataset(isFirst: boolean) {
		const formArray: FormArray = (this.formGroup.get('datasets').get('datasetsList') as FormArray);
		let dataset = new QuickWizardDatasetDescriptionModel().fromModel(this.datasetProfileDefinition);
		let formGroup = dataset.buildForm();

		if (isFirst) {
			formGroup.get('datasetLabel').setValue(
				this.datasetProfile.value["label"] + " " +
				this.language.instant('GENERAL.NAMES.DATASET')
			);
			this.isFirst = false;
		}
		else {
			formGroup.get('datasetLabel').setValue(
				this.datasetProfile.value["label"] + " " +
				this.language.instant('GENERAL.NAMES.DATASET') +
				" (" +
				this.lastIndexOfDataset + ")"
			);
		}
		formArray.push(formGroup);
		this.lastIndexOfDataset = formArray.length - 1;
		this.editedDataset = true;
		this.scrollToTop();
	}

	listingMode() {
		if (this.toggleButton === 0) return true;
	}

	scrollToTop() {
		var currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
		if (currentScroll > 0) {
			window.scrollTo(0, 0);
		}
	}

	linkToScroll: LinkToScroll;
	onStepFound(linkToScroll: LinkToScroll) {
		this.linkToScroll = linkToScroll;
	}
}
