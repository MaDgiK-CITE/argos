import { FormControl, FormGroup } from "@angular/forms";
import { DatasetProfileDefinitionModel } from "../../../core/model/dataset-profile-definition/dataset-profile-definition";
import { DatasetDescriptionFormEditorModel } from "../../misc/dataset-description-form/dataset-description-form.model";

export class QuickWizardDatasetDescriptionModel extends DatasetDescriptionFormEditorModel {

	public datasetLabel: string;

	fromModel(item: DatasetProfileDefinitionModel): DatasetDescriptionFormEditorModel {
		super.fromModel(item);
		return this;
	}

	buildForm(): FormGroup {
		const formGroup: FormGroup = super.buildForm();
		formGroup.addControl('datasetLabel', new FormControl({ value: this.datasetLabel }));
		formGroup.addControl('status', new FormControl({ value: this.status }));
		return formGroup;
	}
}
