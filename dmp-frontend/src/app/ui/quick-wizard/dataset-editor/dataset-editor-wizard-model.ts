import { FormBuilder, FormGroup } from "@angular/forms";
import { BaseFormModel } from '@app/core/model/base-form-model';
import { DatasetWizardEditorModel } from '@app/ui/dataset/dataset-wizard/dataset-wizard-editor.model';
import { QuickWizardDatasetDescriptionModel } from '@app/ui/quick-wizard/dataset-editor/quick-wizard-dataset-description-model';
import { BackendErrorValidator } from '@common/forms/validation/custom-validator';
import { ValidationErrorModel } from '@common/forms/validation/error-model/validation-error-model';
import { ValidationContext } from '@common/forms/validation/validation-context';

export class DatasetEditorWizardModel extends BaseFormModel {

    public datasetsList: Array<QuickWizardDatasetDescriptionModel> = [];
    public validationErrorModel: ValidationErrorModel = new ValidationErrorModel();
    public viewOnly: boolean;


    fromModel(item: DatasetWizardEditorModel[]): DatasetEditorWizardModel {
        return this;
    }

    buildForm(context: ValidationContext = null): FormGroup {
        if (context == null) { context = this.createValidationContext(); }
        const formGroup = new FormBuilder().group({}, context.getValidation('datasetsList').validators);
        const formArray = new Array<FormGroup>();
        this.datasetsList.forEach(item => {
            const form: FormGroup = item.buildForm();
            formArray.push(form);
        });
        formGroup.addControl('datasetsList', this.formBuilder.array(formArray));
        return formGroup;
    }

    createValidationContext(): ValidationContext {
        const baseContext: ValidationContext = new ValidationContext();
        baseContext.validation.push({ key: 'datasetsList', validators: [BackendErrorValidator(this.validationErrorModel, 'datasetsList')] });
        return baseContext;
    }



}
