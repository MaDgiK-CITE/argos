import { FormBuilder, FormGroup } from '@angular/forms';
import { DmpModel } from '@app/core/model/dmp/dmp';
import { FunderModel } from '@app/core/model/funder/funder';
import { GrantListingModel } from '@app/core/model/grant/grant-listing';
import { ProjectModel } from '@app/core/model/project/project';
import { DatasetWizardEditorModel } from '@app/ui/dataset/dataset-wizard/dataset-wizard-editor.model';
import { FunderFormModel } from '@app/ui/dmp/editor/grant-tab/funder-form-model';
import { ProjectFormModel } from '@app/ui/dmp/editor/grant-tab/project-form-model';
import { ValidationErrorModel } from '@common/forms/validation/error-model/validation-error-model';
import { ValidationContext } from '@common/forms/validation/validation-context';
import { DatasetEditorWizardModel } from '../dataset-editor/dataset-editor-wizard-model';
import { DmpEditorWizardModel } from '../dmp-editor/dmp-editor-wizard-model';
import { GrantEditorWizardModel } from '../grant-editor/grant-editor-wizard-model';


export class QuickWizardEditorWizardModel {
    public grant: GrantEditorWizardModel;
    public funder: FunderFormModel;
    public project: ProjectFormModel;
    public dmp: DmpEditorWizardModel;
    public datasets: DatasetEditorWizardModel;
    public validationErrorModel: ValidationErrorModel = new ValidationErrorModel();

    fromModelGrant(item: GrantListingModel): QuickWizardEditorWizardModel {
        this.grant.fromModel(item);
        return this;
    }

    fromModelFunder(item: FunderModel): QuickWizardEditorWizardModel {
        this.funder.fromModel(item);
        return this;
    }

    fromModelProject(item: ProjectModel): QuickWizardEditorWizardModel {
        this.project.fromModel(item);
        return this;
    }

    fromModelDmp(item: DmpModel): QuickWizardEditorWizardModel {
        this.dmp.fromModel(item);
        return this;
    }

    fromModelDataset(item: DatasetWizardEditorModel[]): QuickWizardEditorWizardModel {
        this.datasets.fromModel(item);
        return this;
    }

    buildForm(context: ValidationContext = null): FormGroup {
        // if (context == null) { context = this.createValidationContext(); }
        const formGroup = new FormBuilder().group({
            grant: new GrantEditorWizardModel().buildForm(),
            funder: new FunderFormModel().buildForm(),
            project: new ProjectFormModel().buildForm(),
            dmp: new DmpEditorWizardModel().buildForm(),
            datasets: new DatasetEditorWizardModel().buildForm()

        });
        return formGroup;
    }

    // createValidationContext(): ValidationContext {
    //     const baseContext: ValidationContext = new ValidationContext();
    //     baseContext.validation.push({ key: 'grant', validators: [Validators.required, BackendErrorValidator(this.validationErrorModel, 'grant')] });
    //     baseContext.validation.push({ key: 'dmp', validators: [Validators.required, BackendErrorValidator(this.validationErrorModel, 'dmp')] });
    //     baseContext.validation.push({ key: 'datasets', validators: [Validators.required, BackendErrorValidator(this.validationErrorModel, 'datasets')] });
    //     return baseContext;
    // }

}
