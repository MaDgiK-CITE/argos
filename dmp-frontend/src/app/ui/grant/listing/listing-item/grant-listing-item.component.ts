import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { GrantListingModel } from '../../../../core/model/grant/grant-listing';

@Component({
	selector: 'app-grant-listing-item-component',
	templateUrl: './grant-listing-item.component.html',
	styleUrls: ['./grant-listing-item.component.scss'],
})
export class GrantListingItemComponent implements OnInit {

	@Input() grant: GrantListingModel;
	@Input() showDivider: boolean = true;
	@Output() onClick: EventEmitter<GrantListingModel> = new EventEmitter();

	constructor(

	) {
	}

	ngOnInit() {
	}

	itemClicked() {
		this.onClick.emit(this.grant);
	}
}
