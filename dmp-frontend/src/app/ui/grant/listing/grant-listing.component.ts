
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute, Router } from '@angular/router';
import { DataTableRequest } from '@app/core/model/data-table/data-table-request';
import { GrantListingModel } from '@app/core/model/grant/grant-listing';
import { GrantCriteria } from '@app/core/query/grant/grant-criteria';
import { GrantService } from '@app/core/services/grant/grant.service';
import { GrantCriteriaComponent } from '@app/ui/grant/listing/criteria/grant-criteria.component';
import { BreadcrumbItem } from '@app/ui/misc/breadcrumb/definition/breadcrumb-item';
import { IBreadCrumbComponent } from '@app/ui/misc/breadcrumb/definition/IBreadCrumbComponent';
import { BaseComponent } from '@common/base/base.component';
import { TranslateService } from '@ngx-translate/core';
import { Observable, of as observableOf } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
	selector: 'app-grant-listing-component',
	templateUrl: 'grant-listing.component.html',
	styleUrls: ['./grant-listing.component.scss']
})
export class GrantListingComponent extends BaseComponent implements OnInit, IBreadCrumbComponent {

	@ViewChild(MatPaginator, { static: true }) _paginator: MatPaginator;
	@ViewChild(MatSort, { static: false }) sort: MatSort;
	@ViewChild(GrantCriteriaComponent, { static: true }) criteria: GrantCriteriaComponent;

	breadCrumbs: Observable<BreadcrumbItem[]>;
	totalCount: number;
	listingItems: GrantListingModel[] = [];

	constructor(
		private grantService: GrantService,
		private router: Router,
		private route: ActivatedRoute,
		public language: TranslateService
	) {
		super();
	}

	ngOnInit() {

		this.criteria.setCriteria(this.getDefaultCriteria());
		this.refresh();
		this.criteria.setRefreshCallback((resetPages) => this.refresh(resetPages));

		this.breadCrumbs = observableOf([{
			parentComponentName: null,
			label: this.language.instant('NAV-BAR.GRANTS').toUpperCase(),
			url: '/grants'
		}]);

	}

	refresh(resetPages = false) {
		if (this._paginator.pageSize === undefined) this._paginator.pageSize = 10;
		if (resetPages) this._paginator.pageIndex = 0;
		const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
		let fields: Array<string> = new Array();
		if (this.sort && this.sort.active) { fields = this.sort.direction === 'asc' ? ['+' + this.sort.active] : ['-' + this.sort.active]; }
		const request = new DataTableRequest<GrantCriteria>(startIndex, this._paginator.pageSize, { fields: fields });
		request.criteria = this.criteria.criteria;
		this.grantService.getPaged(request, "listing").pipe(takeUntil(this._destroyed)).subscribe(result => {
			if (!result) { return []; }
			if (this._paginator.pageIndex === 0) { this.totalCount = result.totalCount; }
			this.listingItems = result.data;
		});
	}

	rowClicked(grant: GrantListingModel) {
		this.router.navigate(['/grants/edit/' + grant.id]);
	}

	getDefaultCriteria(): GrantCriteria {
		const defaultCriteria = new GrantCriteria();
		return defaultCriteria;
	}

	pageThisEvent(event) {
		this.refresh();
	}
}


