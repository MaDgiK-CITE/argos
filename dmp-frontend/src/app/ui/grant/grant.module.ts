import { NgModule } from '@angular/core';
import { ConfirmationDialogModule } from '@common/modules/confirmation-dialog/confirmation-dialog.module';
import { UrlListingModule } from '@app/library/url-listing/url-listing.module';
import { GrantEditorComponent } from '@app/ui/grant/editor/grant-editor.component';
import { GrantRoutingModule } from '@app/ui/grant/grant.routing';
import { GrantCriteriaComponent } from '@app/ui/grant/listing/criteria/grant-criteria.component';
import { GrantListingComponent } from '@app/ui/grant/listing/grant-listing.component';
import { GrantListingItemComponent } from '@app/ui/grant/listing/listing-item/grant-listing-item.component';
import { CommonFormsModule } from '@common/forms/common-forms.module';
import { CommonUiModule } from '@common/ui/common-ui.module';

@NgModule({
	imports: [
		CommonUiModule,
		CommonFormsModule,
		UrlListingModule,
		ConfirmationDialogModule,
		GrantRoutingModule
	],
	declarations: [
		GrantListingComponent,
		GrantCriteriaComponent,
		GrantEditorComponent,
		GrantListingItemComponent
	]
})
export class GrantModule { }
