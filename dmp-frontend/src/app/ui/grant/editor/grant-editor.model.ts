import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BackendErrorValidator } from '@common/forms/validation/custom-validator';
import { ValidationErrorModel } from '@common/forms/validation/error-model/validation-error-model';
import { ValidationContext } from '@common/forms/validation/validation-context';
import { GrantType } from '../../../core/common/enum/grant-type';
import { Status } from '../../../core/common/enum/status';
import { ContentFile, GrantListingModel } from '../../../core/model/grant/grant-listing';

export class GrantEditorModel {
	public id: string;
	public label: string;
	public abbreviation: string;
	public reference: string;
	public type: GrantType = GrantType.Internal;
	public uri: String;
	public status: Status = Status.Active;
	public startDate: Date;
	public endDate: Date;
	public description: String;
	public contentUrl: string;
	public files: ContentFile[];
	public validationErrorModel: ValidationErrorModel = new ValidationErrorModel();

	fromModel(item: GrantListingModel): GrantEditorModel {
		this.id = item.id;
		this.label = item.label;
		this.type = item.type;
		this.abbreviation = item.abbreviation;
		this.reference = item.reference;
		this.uri = item.uri;
		this.status = item.status;
		this.startDate = item.startDate ? new Date(item.startDate) : null;
		this.endDate = item.endDate ? new Date(item.endDate) : null;
		this.description = item.description;
		this.contentUrl = item.contentUrl;
		this.files = item.files;
		return this;
	}

	buildForm(context: ValidationContext = null, disabled: boolean = false): FormGroup {
		if (context == null) { context = this.createValidationContext(); }

		const formGroup = new FormBuilder().group({
			id: [{ value: this.id, disabled: disabled }, context.getValidation('id').validators],
			label: [{ value: this.label, disabled: disabled }, context.getValidation('label').validators],
			abbreviation: [{ value: this.abbreviation, disabled: disabled }, context.getValidation('abbreviation').validators],
			uri: [{ value: this.uri, disabled: disabled }, context.getValidation('uri').validators],
			status: [{ value: this.status, disabled: disabled }, context.getValidation('status').validators],
			type: [{ value: this.type, disabled: disabled }, context.getValidation('type').validators],
			description: [{ value: this.description, disabled: disabled }, context.getValidation('description').validators],
			startDate: [{ value: this.startDate, disabled: disabled }, context.getValidation('startDate').validators],
			endDate: [{ value: this.endDate, disabled: disabled }, context.getValidation('endDate').validators],
			files: [{ value: this.files, disabled: disabled }, context.getValidation('files').validators]
		}, { validator: startEndValidator });

		return formGroup;
	}

	createValidationContext(): ValidationContext {
		const baseContext: ValidationContext = new ValidationContext();
		baseContext.validation.push({ key: 'id', validators: [] });
		baseContext.validation.push({ key: 'label', validators: [Validators.required, BackendErrorValidator(this.validationErrorModel, 'label')] });
		baseContext.validation.push({ key: 'abbreviation', validators: [BackendErrorValidator(this.validationErrorModel, 'abbreviation')] });
		baseContext.validation.push({ key: 'uri', validators: [BackendErrorValidator(this.validationErrorModel, 'uri')] });
		baseContext.validation.push({ key: 'type', validators: [BackendErrorValidator(this.validationErrorModel, 'type')] });
		baseContext.validation.push({ key: 'status', validators: [] });
		baseContext.validation.push({ key: 'description', validators: [Validators.required, BackendErrorValidator(this.validationErrorModel, 'description')] });
		baseContext.validation.push({ key: 'startDate', validators: [BackendErrorValidator(this.validationErrorModel, 'startDate')] });
		baseContext.validation.push({ key: 'endDate', validators: [BackendErrorValidator(this.validationErrorModel, 'endDate')] });
		baseContext.validation.push({ key: 'files', validators: [BackendErrorValidator(this.validationErrorModel, 'files')] });

		return baseContext;
	}
}

export function startEndValidator(formGroup: FormGroup) {
	const start = formGroup.get('startDate').value;
	const end = formGroup.get('endDate').value;
	if (start != null && end != null && end < start) {
		return { 'startAfterEndError': {} };
	}
	return null;
}
