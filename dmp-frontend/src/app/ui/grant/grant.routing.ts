import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GrantEditorComponent } from './editor/grant-editor.component';
import { GrantListingComponent } from './listing/grant-listing.component';

// ----------- UNCOMMENT TO ADD AGAIN GRANTS --------
const routes: Routes = [
	// {
	// 	path: '',
	// 	component: GrantListingComponent,
	// 	data: {
	// 		breadcrumb: true
	// 	},
	// },
	// {
	// 	path: 'edit/:id',
	// 	component: GrantEditorComponent,
	// 	data: {
	// 		breadcrumb: true,
	// 		title: 'GENERAL.TITLES.GRANT-EDIT'
	// 	}
	// },
	// {
	// 	path: 'new',
	// 	component: GrantEditorComponent,
	// 	data: {
	// 		breadcrumb: true,
	// 		title: 'GENERAL.TITLES.GRANT-NEW'
	// 	},
	// }
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class GrantRoutingModule { }
