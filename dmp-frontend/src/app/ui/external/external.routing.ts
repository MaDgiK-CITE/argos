
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ZenodoComponent } from './zenodo/zenodo.component';



const routes: Routes = [
	{
		path: '',
		component: ZenodoComponent,
		data: {
			breadcrumb: true
		},
	},
	{
		path: 'zenodo/:id',
		component: ZenodoComponent,
		data: {
			breadcrumb: true
		},
	},];



@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})

export class ExternalRoutingModule { }
