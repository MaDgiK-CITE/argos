import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ZenodoComponent } from './zenodo/zenodo.component';
import { ExternalRoutingModule } from './external.routing';



@NgModule({
  declarations: [ZenodoComponent],
  imports: [
	CommonModule,
	ExternalRoutingModule
  ]
})
export class ExternalModule { }
