import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { BaseComponent } from '@common/base/base.component';
import { takeUntil } from 'rxjs/operators';

@Component({
	selector: 'app-zenodo',
	templateUrl: './zenodo.component.html',
	styleUrls: ['./zenodo.component.scss']
})
export class ZenodoComponent extends BaseComponent implements OnInit {

	constructor(
		private route: ActivatedRoute,
		private router: Router, ) {
		super();
	}

	ngOnInit() {
		this.route.paramMap.pipe(takeUntil(this._destroyed)).subscribe((params: Params) => {
			const id = params.params.id;
			this.router.navigate([`/explore-plans/publicOverview/${id}`]);
		});
	}

}
