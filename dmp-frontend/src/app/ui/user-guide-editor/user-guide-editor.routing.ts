import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserGuideEditorComponent } from './user-guide-editor.component';
import { AdminAuthGuard } from '@app/core/admin-auth-guard.service';


const routes: Routes = [
	{ path: '', component: UserGuideEditorComponent, canActivate: [AdminAuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserGuideEditorRoutingModule { }
