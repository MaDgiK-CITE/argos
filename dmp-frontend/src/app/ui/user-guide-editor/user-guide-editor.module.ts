import { NgModule } from '@angular/core';
import { EditorModule } from '@tinymce/tinymce-angular';
import { UserGuideEditorRoutingModule } from './user-guide-editor.routing';
import { UserGuideEditorComponent } from './user-guide-editor.component';
import { CommonUiModule } from '@common/ui/common-ui.module';
import { CommonFormsModule } from '@common/forms/common-forms.module';


@NgModule({
  declarations: [UserGuideEditorComponent],
  imports: [
	CommonUiModule,
	CommonFormsModule,
	UserGuideEditorRoutingModule,
	EditorModule
  ]
})
export class UserGuideEditorModule { }
