﻿import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LanguageEditorComponent } from './language-editor.component';
import { AuthGuard } from '@app/core/auth-guard.service';
import { AdminAuthGuard } from '@app/core/admin-auth-guard.service';

const routes: Routes = [
	{ path: '', component: LanguageEditorComponent, canActivate: [AdminAuthGuard] },
	// { path: ':id', component: UserProfileComponent }
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class LanguageEditorRoutingModule { }
