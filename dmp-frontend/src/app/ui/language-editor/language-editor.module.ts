import { NgModule } from '@angular/core';
import { LanguageEditorComponent } from './language-editor.component';
import { LanguageEditorRoutingModule } from './language-editor.routing';
import { CommonUiModule } from '@common/ui/common-ui.module';
import { CommonFormsModule } from '@common/forms/common-forms.module';
import { ConfirmationDialogModule } from '@common/modules/confirmation-dialog/confirmation-dialog.module';
import {TextFieldModule} from '@angular/cdk/text-field';



@NgModule({
  declarations: [LanguageEditorComponent],
  imports: [
	CommonUiModule,
	CommonFormsModule,
	ConfirmationDialogModule,
	LanguageEditorRoutingModule,
	TextFieldModule
  ]
})
export class LanguageEditorModule { }
