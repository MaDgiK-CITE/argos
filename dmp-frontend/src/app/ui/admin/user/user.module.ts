﻿import { NgModule } from '@angular/core';
import { FormattingModule } from '@app/core/formatting.module';
import { CommonFormsModule } from '@common/forms/common-forms.module';
import { CommonUiModule } from '@common/ui/common-ui.module';
import { UserCriteriaComponent } from './listing/criteria/user-criteria.component';
import { UserRoleEditorComponent } from './listing/role-editor/user-role-editor.component';
import { UserListingComponent } from './listing/user-listing.component';
import { UserRoutingModule } from './user.routing';

@NgModule({
	imports: [
		CommonUiModule,
		CommonFormsModule,
		FormattingModule,
		UserRoutingModule
	],
	declarations: [
		UserListingComponent,
		UserCriteriaComponent,
		UserRoleEditorComponent
	],
})

export class UserModule { }