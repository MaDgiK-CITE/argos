﻿import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserListingComponent } from './listing/user-listing.component';
import { AdminAuthGuard } from '@app/core/admin-auth-guard.service';

const routes: Routes = [
	{ path: '', component: UserListingComponent, canActivate: [AdminAuthGuard] },
	// { path: ':id', component: UserProfileComponent }
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class UserRoutingModule { }
