import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MaintenanceTasksComponent } from './maintenance-tasks.component';
import { AdminAuthGuard } from '@app/core/admin-auth-guard.service';


const routes: Routes = [
	{ path: '', component: MaintenanceTasksComponent, canActivate: [AdminAuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MaintenanceTasksRoutingModule { }