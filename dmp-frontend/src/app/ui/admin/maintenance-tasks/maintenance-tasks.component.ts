import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@common/base/base.component';
import { takeUntil } from 'rxjs/operators';
import { UiNotificationService, SnackBarNotificationLevel } from '@app/core/services/notification/ui-notification-service';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { MaintenanceTasksService } from '@app/core/services/maintenance-tasks/maintenance-tasks.service';

@Component({
  selector: 'app-maintenance-tasks',
  templateUrl: './maintenance-tasks.component.html',
  styleUrls: ['./maintenance-tasks.component.scss']
})
export class MaintenanceTasksComponent extends BaseComponent implements OnInit {

  constructor(
    private maintenanceTasksService: MaintenanceTasksService,
    private uiNotificationService: UiNotificationService,
	  private translate: TranslateService,
	  private router: Router,
  ) { 
    super();
  }

  ngOnInit(): void {
  }

  migrateSemantics(ev: Event) {
	  (ev.srcElement as HTMLButtonElement).disabled = true;
      this.maintenanceTasksService.migrateSemantics().pipe(takeUntil(this._destroyed)).subscribe(
      response => {
        (ev.srcElement as HTMLButtonElement).disabled = false;
        this.onCallbackSuccess();
      },
      error => {
        (ev.srcElement as HTMLButtonElement).disabled = false;
        this.onCallbackError(error);
      }
    );
  }

  addRdaInSemantics(ev: Event) {
	  (ev.srcElement as HTMLButtonElement).disabled = true;
	  this.maintenanceTasksService.addRdaInSemantics().pipe(takeUntil(this._destroyed)).subscribe(
      response => {
        (ev.srcElement as HTMLButtonElement).disabled = false;
        this.onCallbackSuccess();
      },
      error => {
        (ev.srcElement as HTMLButtonElement).disabled = false;
        this.onCallbackError(error);
      }
	  );
  }

  onCallbackSuccess(): void {
    this.uiNotificationService.snackBarNotification( this.translate.instant('GENERAL.SNACK-BAR.SUCCESSFUL-UPDATE'), SnackBarNotificationLevel.Success);
    this.router.navigate(['/reload']).then(() => this.router.navigate(['/maintenance-tasks']));
  }
  
  onCallbackError(error: any) {
    this.uiNotificationService.snackBarNotification( error, SnackBarNotificationLevel.Error);
  }

}
