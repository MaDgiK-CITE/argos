import { DragDropModule } from '@angular/cdk/drag-drop';
import { NgModule } from '@angular/core';
//matrial
import { MatBadgeModule } from '@angular/material/badge';
import { FormattingModule } from '@app/core/formatting.module';
import { AutoCompleteModule } from '@app/library/auto-complete/auto-complete.module';
import { DatasetProfileRoutingModule } from '@app/ui/admin/dataset-profile/dataset-profile.routing';
import { DatasetProfileEditorCompositeFieldComponent } from '@app/ui/admin/dataset-profile/editor/components/composite-field/dataset-profile-editor-composite-field.component';
import { DatasetProfileEditorDefaultValueComponent } from '@app/ui/admin/dataset-profile/editor/components/composite-profile-editor-default-value/component-profile-editor-default-value.component';
import { DatasetProfileEditorAutoCompleteFieldComponent } from '@app/ui/admin/dataset-profile/editor/components/field-type/auto-complete/dataset-profile-editor-auto-complete-field.component';
import { DatasetProfileEditorBooleanDecisionFieldComponent } from '@app/ui/admin/dataset-profile/editor/components/field-type/boolean-decision/dataset-profile-editor-boolean-decision-field.component';
import { DatasetProfileEditorCheckboxFieldComponent } from '@app/ui/admin/dataset-profile/editor/components/field-type/checkbox/dataset-profile-editor-checkbox-field.component';
import { DatasetProfileEditorComboBoxFieldComponent } from '@app/ui/admin/dataset-profile/editor/components/field-type/combo-box/dataset-profile-editor-combo-box-field.component';
import { DatasetProfileEditorDatasetsAutoCompleteFieldComponent } from '@app/ui/admin/dataset-profile/editor/components/field-type/datasets-auto-complete/dataset-profile-editor-datasets-autocomplete-field.component';
import { DatasetProfileEditorDatePickerFieldComponent } from '@app/ui/admin/dataset-profile/editor/components/field-type/datepicker/dataset-profile-editor-date-picker-field.component';
import { DatasetProfileEditorDmpsAutoCompleteFieldComponent } from '@app/ui/admin/dataset-profile/editor/components/field-type/dmps-auto-complete/dataset-profile-editor-dmps-autocomplete-field.component';
import { DatasetProfileEditorFreeTextFieldComponent } from '@app/ui/admin/dataset-profile/editor/components/field-type/free-text/dataset-profile-editor-free-text-field.component';
import { DatasetProfileEditorInternalDmpEntitiesFieldComponent } from '@app/ui/admin/dataset-profile/editor/components/field-type/internal-dmp-entities/dataset-profile-editor-internal-dmp-entities-field.component';
import { DatasetProfileEditorRadioBoxFieldComponent } from '@app/ui/admin/dataset-profile/editor/components/field-type/radio-box/dataset-profile-editor-radio-box-field.component';
import { DatasetProfileEditorResearchersAutoCompleteFieldComponent } from '@app/ui/admin/dataset-profile/editor/components/field-type/researchers-auto-complete/dataset-profile-editor-researchers-auto-complete-field.component';
import { DatasetProfileEditorTextAreaFieldComponent } from '@app/ui/admin/dataset-profile/editor/components/field-type/textarea/dataset-profile-editor-text-area-field.component';
import { DatasetProfileEditorWordListFieldComponent } from '@app/ui/admin/dataset-profile/editor/components/field-type/word-list/dataset-profile-editor-word-list-field.component';
import { DatasetProfileEditorFieldComponent } from '@app/ui/admin/dataset-profile/editor/components/field/dataset-profile-editor-field.component';
import { DatasetProfileEditorPageComponent } from '@app/ui/admin/dataset-profile/editor/components/page/dataset-profile-editor-page.component';
import { DatasetProfileEditorRuleComponent } from '@app/ui/admin/dataset-profile/editor/components/rule/dataset-profile-editor-rule.component';
import { DatasetProfileEditorSectionComponent } from '@app/ui/admin/dataset-profile/editor/components/section/dataset-profile-editor-section.component';
import { DatasetProfileEditorComponent } from '@app/ui/admin/dataset-profile/editor/dataset-profile-editor.component';
import { DatasetProfileCriteriaComponent } from '@app/ui/admin/dataset-profile/listing/criteria/dataset-profile.component';
import { DialogConfirmationUploadDatasetProfiles } from '@app/ui/admin/dataset-profile/listing/criteria/dialog-confirmation-upload-profile/dialog-confirmation-upload-profiles.component';
import { DatasetProfileListingComponent } from '@app/ui/admin/dataset-profile/listing/dataset-profile-listing.component';
import { DatasetModule } from '@app/ui/dataset/dataset.module';
import { FormProgressIndicationModule } from '@app/ui/misc/dataset-description-form/components/form-progress-indication/form-progress-indication.module';
import { CommonFormsModule } from '@common/forms/common-forms.module';
import { ConfirmationDialogModule } from '@common/modules/confirmation-dialog/confirmation-dialog.module';
import { CommonUiModule } from '@common/ui/common-ui.module';
import { AngularStickyThingsModule } from '@w11k/angular-sticky-things';
import { DragulaModule } from 'ng2-dragula';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { DatasetProfileEditorCurrencyFieldComponent } from './editor/components/field-type/currency/dataset-profile-editor-currency-field.component';
import { DatasetProfileEditorDataRepositoriesFieldComponent } from './editor/components/field-type/data-repositories/dataset-profile-editor-data-repositories-field.component';
import { DatasetProfileEditorDatasetIdentifierFieldComponent } from './editor/components/field-type/dataset-identifier/dataset-profile-editor-dataset-identifier-field.component';
import { DatasetProfileEditorExternalDatasetsFieldComponent } from './editor/components/field-type/external-datasets/dataset-profile-editor-external-datasets-field.component';
import { DatasetProfileEditorOrganizationsFieldComponent } from './editor/components/field-type/organizations/dataset-profile-editor-organizations-field.component';
import { DatasetProfileEditorRegistriesFieldComponent } from './editor/components/field-type/registries/dataset-profile-editor-registries-field.component';
import { DatasetProfileEditorResearchersFieldComponent } from './editor/components/field-type/researchers/dataset-profile-editor-researchers-field.component';
import { DatasetProfileEditorServicesFieldComponent } from './editor/components/field-type/services/dataset-profile-editor-services-field.component';
import { DatasetProfileEditorTagsFieldComponent } from './editor/components/field-type/tags/dataset-profile-editor-tags-field.component';
import { DatasetProfileEditorValidatorFieldComponent } from './editor/components/field-type/validator/dataset-profile-editor-validator-field.component';
import { FinalPreviewComponent } from './editor/components/final-preview/final-preview.component';
import { DatasetProfileEditorSectionFieldSetComponent } from './editor/components/section-fieldset/dataset-profile-editor-section-fieldset.component';
import { ParseStatus } from './listing/pipe/parse-status.pipe';
import { DatasetProfileTableOfContents } from './table-of-contents/table-of-contents';
import { DatasetProfileTableOfContentsInternalSection } from './table-of-contents/table-of-contents-internal-section/table-of-contents-internal-section';
import {TransitionGroupModule} from "@app/ui/transition-group/transition-group.module";
import { RichTextEditorModule } from "@app/library/rich-text-editor/rich-text-editor.module";
import {DatasetProfileEditorRichTextAreaFieldComponent} from "@app/ui/admin/dataset-profile/editor/components/field-type/rich-textarea/dataset-profile-editor-rich-text-area-field.component";
import {DatasetProfileEditorTaxonomiesFieldComponent} from "@app/ui/admin/dataset-profile/editor/components/field-type/taxonomies/dataset-profile-editor-taxonomies-field.component";
import {DatasetProfileEditorLicensesFieldComponent} from "@app/ui/admin/dataset-profile/editor/components/field-type/licenses/dataset-profile-editor-licenses-field.component";
import {DatasetProfileEditorPublicationsFieldComponent} from "@app/ui/admin/dataset-profile/editor/components/field-type/publications/dataset-profile-editor-publications-field.component";
import {DatasetProfileEditorJournalRepositoriesFieldComponent} from "@app/ui/admin/dataset-profile/editor/components/field-type/journal-repositories/dataset-profile-editor-journal-repositories-field.component";
import {DatasetProfileEditorPubRepositoriesFieldComponent} from "@app/ui/admin/dataset-profile/editor/components/field-type/pub-repositories/dataset-profile-editor-pub-repositories-field.component";
import {DatasetProfileEditorUploadFieldComponent} from "@app/ui/admin/dataset-profile/editor/components/field-type/upload/dataset-profile-editor-upload-field.component";
// import { TableEditorModule } from "@app/library/table-editor/table-editor.module";
// import {DatasetProfileEditorTableFieldComponent} from "@app/ui/admin/dataset-profile/editor/components/field-type/table/dataset-profile-editor-table-field.component";



@NgModule({
  imports: [
    CommonUiModule,
    CommonFormsModule,
    FormattingModule,
    DatasetProfileRoutingModule,
    ConfirmationDialogModule,
    NgxDropzoneModule,
    FormProgressIndicationModule,
    DatasetModule,
    AngularStickyThingsModule,
    DragDropModule,
    MatBadgeModule,
    DragulaModule,
    AutoCompleteModule,
    TransitionGroupModule,
    RichTextEditorModule,
    // TableEditorModule
  ],
	declarations: [
		DatasetProfileListingComponent,
		DatasetProfileCriteriaComponent,
		DatasetProfileEditorComponent,
		DatasetProfileEditorSectionComponent,
		DatasetProfileEditorCompositeFieldComponent,
		DatasetProfileEditorFieldComponent,
		DatasetProfileEditorPageComponent,
		DatasetProfileEditorRuleComponent,
		DatasetProfileEditorAutoCompleteFieldComponent,
		DatasetProfileEditorBooleanDecisionFieldComponent,
		DatasetProfileEditorCheckboxFieldComponent,
		DatasetProfileEditorComboBoxFieldComponent,
		DatasetProfileEditorFreeTextFieldComponent,
		DatasetProfileEditorRadioBoxFieldComponent,
		DatasetProfileEditorTextAreaFieldComponent,
		DatasetProfileEditorRichTextAreaFieldComponent,
		DatasetProfileEditorUploadFieldComponent,
		// DatasetProfileEditorTableFieldComponent,
		DatasetProfileEditorDatePickerFieldComponent,
		DatasetProfileEditorWordListFieldComponent,
		DatasetProfileEditorDefaultValueComponent,
		DialogConfirmationUploadDatasetProfiles,
		DatasetProfileEditorInternalDmpEntitiesFieldComponent,
		DatasetProfileEditorResearchersAutoCompleteFieldComponent,
		DatasetProfileEditorDatasetsAutoCompleteFieldComponent,
		DatasetProfileEditorDmpsAutoCompleteFieldComponent,
		ParseStatus,
		DatasetProfileEditorExternalDatasetsFieldComponent,
		DatasetProfileEditorDataRepositoriesFieldComponent,
		DatasetProfileEditorPubRepositoriesFieldComponent,
		DatasetProfileEditorJournalRepositoriesFieldComponent,
		DatasetProfileEditorTaxonomiesFieldComponent,
		DatasetProfileEditorLicensesFieldComponent,
		DatasetProfileEditorPublicationsFieldComponent,
		DatasetProfileEditorRegistriesFieldComponent,
		DatasetProfileEditorServicesFieldComponent,
		DatasetProfileEditorTagsFieldComponent,
		DatasetProfileEditorResearchersFieldComponent,
		DatasetProfileEditorOrganizationsFieldComponent,
		DatasetProfileEditorDatasetIdentifierFieldComponent,
		DatasetProfileEditorCurrencyFieldComponent,
		DatasetProfileEditorValidatorFieldComponent,
		DatasetProfileTableOfContents,
		DatasetProfileTableOfContentsInternalSection,
		DatasetProfileEditorSectionFieldSetComponent,
		FinalPreviewComponent
	],
	entryComponents: [
		DialogConfirmationUploadDatasetProfiles
	]
})
export class DatasetProfileModule { }

