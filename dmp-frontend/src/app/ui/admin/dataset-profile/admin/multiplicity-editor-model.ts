﻿import {FormGroup} from '@angular/forms';
import {Multiplicity} from '../../../../core/model/admin/dataset-profile/dataset-profile';
import {BaseFormModel} from '../../../../core/model/base-form-model';

export class MultiplicityEditorModel extends BaseFormModel {
	public min: number;
	public max: number;
	public placeholder: string;
	public tableView: boolean;

	fromModel(item: Multiplicity): MultiplicityEditorModel {
		this.min = item.min;
		this.max = item.max;
		this.placeholder = item.placeholder;
		this.tableView = item.tableView;
		return this;
	}

	buildForm(disabled: boolean = false, skipDisable: Array<String> = []): FormGroup {
		return this.formBuilder.group({
			min: [{value: this.min, disabled: (disabled && !skipDisable.includes('MultiplicityEditorModel.min'))}],
			max: [{value: this.max, disabled: (disabled && !skipDisable.includes('MultiplicityEditorModel.max'))}],
			placeholder: [{
				value: this.placeholder,
				disabled: (disabled && !skipDisable.includes('MultiplicityEditorModel.placeholder'))
			}],
			tableView: [{value: this.tableView, disabled: (disabled && !skipDisable.includes('MultiplicityEditorModel.tableView'))}]
		});
	}
}
