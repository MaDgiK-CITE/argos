﻿import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ValidationType } from '../../../../core/common/enum/validation-type';
import { Field } from '../../../../core/model/admin/dataset-profile/dataset-profile';
import { BaseFormModel } from '../../../../core/model/base-form-model';
import { DefaultValueEditorModel } from './default-value-editor-model';
import { AutoCompleteFieldDataEditorModel } from './field-data/auto-complete-field-data-editor-model';
import { BooleanDecisionFieldDataEditorModel } from './field-data/boolean-decision-field-data-editor-model';
import { CheckBoxFieldDataEditorModel } from './field-data/check-box-field-data-editor-model';
import { FieldDataEditorModel } from './field-data/field-data-editor-model';
import { FreeTextFieldDataEditorModel } from './field-data/free-text-field-data-editor-model';
import { RadioBoxFieldDataEditorModel } from './field-data/radio-box-field-data-editor-model';
import { TextAreaFieldDataEditorModel } from './field-data/text-area-field-data-editor-model';
import { RichTextAreaFieldDataEditorModel } from './field-data/rich-text-area-field-data-editor-model';
import {UploadFieldDataEditorModel} from "./field-data/upload-field-data-editor-model";
import { WordListFieldDataEditorModel } from './field-data/word-list-field-data-editor-model';
import { ViewStyleEditorModel } from './view-style-editor-model';
import { VisibilityEditorModel } from './visibility-editor-model';
import { DatasetProfileEditorDatePickerFieldComponent } from '../editor/components/field-type/datepicker/dataset-profile-editor-date-picker-field.component';
import { DatePickerDataEditorModel } from './field-data/date-picker-data-editor-models';
import { ResearchersAutoCompleteFieldDataEditorModel } from './field-data/researchers-auto-complete-field-data-editor-model';
import { DatasetsAutoCompleteFieldDataEditorModel } from './field-data/datasets-autocomplete-field-data-editor-mode';
import { DmpsAutoCompleteFieldDataEditorModel } from './field-data/dmps-autocomplete-field-data-editor-model';
import { ExternalDatasetsDataEditorModel } from './field-data/external-datasets-data-editor-models';
import { DataRepositoriesDataEditorModel } from './field-data/data-repositories-data-editor-models';
import { RegistriesDataEditorModel } from './field-data/registries-data-editor-models';
import { ServicesDataEditorModel } from './field-data/services-data-editor-models';
import { TagsDataEditorModel } from './field-data/tags-data-editor-models';
import { ResearchersDataEditorModel } from './field-data/researchers-data-editor-models';
import { OrganizationsDataEditorModel } from './field-data/organizations-data-editor-models';
import { DatasetIdentifierDataEditorModel } from './field-data/dataset-identifier-data-editor-models';
import { CurrencyDataEditorModel } from './field-data/currency-data-editor-models';
import { DatasetProfileComboBoxType } from '@app/core/common/enum/dataset-profile-combo-box-type';
import { EditorCustomValidators } from '../editor/custom-validators/editor-custom-validators';
import { ValidationDataEditorModel } from './field-data/validation-data-editor-models';
import {PublicationsDataEditorModel} from "@app/ui/admin/dataset-profile/admin/field-data/publications-data-editor-models";
import {LicensesDataEditorModel} from "@app/ui/admin/dataset-profile/admin/field-data/licenses-data-editor-models";
import {TaxonomiesDataEditorModel} from "@app/ui/admin/dataset-profile/admin/field-data/taxonomies-data-editor-models";

export class FieldEditorModel extends BaseFormModel {

	public id: string;
	// public title: string;
	public defaultValue: DefaultValueEditorModel = new DefaultValueEditorModel();
	public viewStyle: ViewStyleEditorModel = new ViewStyleEditorModel();
	public page: number = null;
	public ordinal: number = null;
	public visible: VisibilityEditorModel = new VisibilityEditorModel();
	public data: FieldDataEditorModel<any>;
	public validations: ValidationType[] = [];
	public schematics: string[];
	public export: boolean = true;

	fromModel(item: Field): FieldEditorModel {
		this.id = item.id;
		if (item.defaultValue) { this.defaultValue = new DefaultValueEditorModel().fromModel(item.defaultValue); }
		this.page = item.page;
		this.ordinal = item.ordinal;
		this.validations = item.validations;
		this.viewStyle = new ViewStyleEditorModel().fromModel(item.viewStyle);
		this.visible = new VisibilityEditorModel().fromModel(item.visible);
		this.schematics = item.schematics;
		this.export = item.export;

		if (item.data) {
			if (this.viewStyle.renderStyle === 'combobox') {
				if (item.data.type === 'autocomplete') { this.data = new AutoCompleteFieldDataEditorModel().fromModel(item.data); }
				if (item.data.type === 'wordlist') { this.data = new WordListFieldDataEditorModel().fromModel(item.data); }
			} else if (this.viewStyle.renderStyle === 'internalDmpEntities') {
				if (item.data.type === 'researchers') { this.data = new ResearchersAutoCompleteFieldDataEditorModel().fromModel(item.data); }
				if (item.data.type === 'datasets') { this.data = new DatasetsAutoCompleteFieldDataEditorModel().fromModel(item.data); }
				if (item.data.type === 'dmps') { this.data = new DmpsAutoCompleteFieldDataEditorModel().fromModel(item.data); }
			} else {
				if (this.viewStyle.renderStyle === 'radiobox') { this.data = new RadioBoxFieldDataEditorModel().fromModel(item.data); }
				if (this.viewStyle.renderStyle === 'checkBox') { this.data = new CheckBoxFieldDataEditorModel().fromModel(item.data); }
				if (this.viewStyle.renderStyle === 'textarea') { this.data = new TextAreaFieldDataEditorModel().fromModel(item.data); }
				if (this.viewStyle.renderStyle === 'richTextarea') { this.data = new RichTextAreaFieldDataEditorModel().fromModel(item.data); }
				if (this.viewStyle.renderStyle === 'upload') { this.data = new UploadFieldDataEditorModel().fromModel(item.data); }
				if (this.viewStyle.renderStyle === 'freetext') { this.data = new FreeTextFieldDataEditorModel().fromModel(item.data); }
				if (this.viewStyle.renderStyle === 'booleanDecision') { this.data = new BooleanDecisionFieldDataEditorModel().fromModel(item.data); }
				if (this.viewStyle.renderStyle === 'datePicker') { this.data = new DatePickerDataEditorModel().fromModel(item.data); }
				if (this.viewStyle.renderStyle === 'externalDatasets') { this.data = new ExternalDatasetsDataEditorModel().fromModel(item.data); }
				if (this.viewStyle.renderStyle === 'dataRepositories') { this.data = new DataRepositoriesDataEditorModel().fromModel(item.data); }
				if (this.viewStyle.renderStyle === 'pubRepositories') { this.data = new DataRepositoriesDataEditorModel().fromModel(item.data); }
				if (this.viewStyle.renderStyle === 'journalRepositories') { this.data = new DataRepositoriesDataEditorModel().fromModel(item.data); }
				if (this.viewStyle.renderStyle === 'taxonomies') { this.data = new TaxonomiesDataEditorModel().fromModel(item.data); }
				if (this.viewStyle.renderStyle === 'licenses') { this.data = new LicensesDataEditorModel().fromModel(item.data); }
				if (this.viewStyle.renderStyle === 'publications') { this.data = new PublicationsDataEditorModel().fromModel(item.data); }
				if (this.viewStyle.renderStyle === 'registries') { this.data = new RegistriesDataEditorModel().fromModel(item.data); }
				if (this.viewStyle.renderStyle === 'services') { this.data = new ServicesDataEditorModel().fromModel(item.data); }
				if (this.viewStyle.renderStyle === 'tags') { this.data = new TagsDataEditorModel().fromModel(item.data); }
				if (this.viewStyle.renderStyle === 'researchers') { this.data = new ResearchersAutoCompleteFieldDataEditorModel().fromModel(item.data); }
				if (this.viewStyle.renderStyle === 'organizations') { this.data = new OrganizationsDataEditorModel().fromModel(item.data); }
				if (this.viewStyle.renderStyle === 'datasetIdentifier') { this.data = new DatasetIdentifierDataEditorModel().fromModel(item.data); }
				if (this.viewStyle.renderStyle === 'currency') { this.data = new CurrencyDataEditorModel().fromModel(item.data); }
				if (this.viewStyle.renderStyle === 'validation') { this.data = new ValidationDataEditorModel().fromModel(item.data); }
			}
		}
		return this;
	}

	buildForm(disabled: boolean = false, skipDisable: Array<String> = []): FormGroup {
		const formGroup = this.formBuilder.group({
			id: [{ value: this.id, disabled: (disabled && !skipDisable.includes('FieldEditorModel.id')) }, [Validators.required, Validators.pattern('^[^<_>]+$')]],
			// title: [this.title],
			page: [{ value: this.page, disabled: (disabled && !skipDisable.includes('FieldEditorModel.page')) }],
			ordinal: [{ value: this.ordinal, disabled: (disabled && !skipDisable.includes('FieldEditorModel.ordinal')) }],
			validations: [{ value: this.validations, disabled: (disabled && !skipDisable.includes('FieldEditorModel.validations')) }],
			schematics: [{ value: this.schematics, disabled: (disabled && !skipDisable.includes('FieldEditorModel.schematics')) }],
			export: [{value: this.export, disabled: (disabled && !skipDisable.includes('FieldSetEditorModel.export'))}]
		});

		formGroup.addControl('defaultValue', this.defaultValue.buildForm(disabled, skipDisable));
		formGroup.addControl('viewStyle', this.viewStyle.buildForm(disabled, skipDisable));
		formGroup.addControl('visible', this.visible.buildForm(disabled, skipDisable));
		if (this.data) { formGroup.addControl('data', this.data.buildForm(disabled, skipDisable)); }
		else { formGroup.addControl('data', new WordListFieldDataEditorModel().buildForm(disabled, skipDisable)); }


		// //append validators

		this._appendCustomValidators(formGroup);


		// //setting up listeners
		// formGroup.get('viewStyle').valueChanges.subscribe(changes=>{
		// 	// const viewStyleChanges:{cssClass:string, renderStyle: string} = changes;

		// 	this._removeCustomValidators(formGroup);
		// 	this._appendCustomValidators(formGroup);

		// })

		return formGroup;
	}


	private _appendCustomValidators(formGroup: FormGroup){
		const renderStyleValue = formGroup.get('viewStyle').get('renderStyle').value;
		if(renderStyleValue === 'checkBox'){
			formGroup.get('defaultValue').get('value').setValidators(Validators.required);
			formGroup.get('defaultValue').get('value').updateValueAndValidity();
		}else if(renderStyleValue === 'combobox'){
			try{
				const comboType = formGroup.get('data').get('type').value;
				if(comboType === DatasetProfileComboBoxType.Autocomplete){//As 'Other' in UI
					formGroup.get('data').setValidators(EditorCustomValidators.atLeastOneElementListValidator('autoCompleteSingleDataList'));
				}else if(comboType === DatasetProfileComboBoxType.WordList){
					formGroup.get('data').setValidators(EditorCustomValidators.atLeastOneElementListValidator('options'));
				}
			}catch(e){
				console.error('Error setting validators.');
				console.error(e);
			}
		}else if(renderStyleValue === 'radiobox'){
			formGroup.get('data').setValidators(EditorCustomValidators.atLeastOneElementListValidator('options'));
		}else if(renderStyleValue === 'upload'){
			formGroup.get('data').setValidators(EditorCustomValidators.atLeastOneElementListValidator('types'));
			formGroup.get('data').get('maxFileSizeInMB').setValidators(Validators.required);
		}

		formGroup.get('data').updateValueAndValidity();
	}

	// private _removeCustomValidators(formGroup:FormGroup){
	// 	const renderStyleValue = formGroup.get('viewStyle').get('renderStyle').value;
	// 	if(renderStyleValue != 'checkBox'){
	// 		formGroup.get('defaultValue').get('value').clearValidators();
	// 	}else if(renderStyleValue === 'combobox'){
	// 		formGroup.get('data').clearValidators();
	// 	}
	// }
	// private _buildData(formGroup: FormGroup){

	// }
}
