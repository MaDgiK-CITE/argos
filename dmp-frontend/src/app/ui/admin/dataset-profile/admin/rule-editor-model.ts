﻿import { FormGroup, Validators } from "@angular/forms";
import { Rule } from "../../../../core/model/admin/dataset-profile/dataset-profile";
import { BaseFormModel } from "../../../../core/model/base-form-model";

export class RuleEditorModel extends BaseFormModel {
	public target: string;
	public value: string;
	public ruleType: string;
	public ruleStyle: string;
	public valueType: string;

	fromModel(item: Rule): RuleEditorModel {
		this.target = item.target;
		this.value = item.value;
		this.ruleType = item.ruleType;
		this.ruleStyle = item.ruleStyle;
		this.valueType = item.valueType;
		return this;
	}

	buildForm(disabled: boolean = false, skipDisable: Array<String> = []): FormGroup {
		const formGroup = this.formBuilder.group({
			// sourceField: [this.sourceField],
			target: [{ value: this.target, disabled: (disabled && !skipDisable.includes('RuleEditorModel.target')) }, [Validators.required]],
			ruleStyle: [{ value: this.ruleStyle, disabled: (disabled && !skipDisable.includes('RuleEditorModel.ruleStyle')) }],
			value: [{ value: this.value, disabled: (disabled && !skipDisable.includes('RuleEditorModel.value')) }, [Validators.required]],
			ruleType: [{ value: this.ruleType, disabled: (disabled && !skipDisable.includes('RuleEditorModel.ruleType')) }],
			valueType: [{ value: this.valueType, disabled: (disabled && !skipDisable.includes('RuleEditorModel.valueType')) }]
		});
		return formGroup;
	}
}
