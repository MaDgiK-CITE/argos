﻿import { FormGroup } from '@angular/forms';
import { DefaultValue } from '../../../../core/model/admin/dataset-profile/dataset-profile';
import { BaseFormModel } from '../../../../core/model/base-form-model';

export class DefaultValueEditorModel extends BaseFormModel {
	public type: string;
	public value: string;

	fromModel(item: DefaultValue): DefaultValueEditorModel {
		this.type = item.type;
		this.value = item.value;
		return this;
	}

	buildForm(disabled: boolean = false, skipDisable: Array<String> = []): FormGroup {
		const formGroup = this.formBuilder.group({
			type: [{ value: this.type, disabled: (disabled && !skipDisable.includes('DefaultValueEditorModel.type')) }],
			value: [{ value: this.value, disabled: (disabled && !skipDisable.includes('DefaultValueEditorModel.value')) }]
		});
		return formGroup;
	}
}
