import { FormGroup } from '@angular/forms';
import { FieldDataEditorModel } from './field-data-editor-model';
import { DatePickerFieldData } from '../../../../../core/model/dataset-profile-definition/field-data/field-data';

export class DatePickerDataEditorModel extends FieldDataEditorModel<DatePickerDataEditorModel> {
	public label: string;

	buildForm(disabled: boolean = false, skipDisable: Array<String> = []): FormGroup {
		const formGroup = this.formBuilder.group({
			label: [{ value: this.label, disabled: (disabled && !skipDisable.includes('DatePickerDataEditorModel.label')) }]
		});
		return formGroup;
	}

	fromModel(item: DatePickerFieldData): DatePickerDataEditorModel {
		this.label = item.label;
		return this;
	}
}
