import { FormGroup } from '@angular/forms';
import { FieldDataEditorModel } from './field-data-editor-model';
import { CurrencyFieldData } from '../../../../../core/model/dataset-profile-definition/field-data/field-data';

export class CurrencyDataEditorModel extends FieldDataEditorModel<CurrencyDataEditorModel> {
	public label: string;

	buildForm(disabled: boolean = false, skipDisable: Array<String> = []): FormGroup {
		const formGroup = this.formBuilder.group({
			label: [{ value: this.label, disabled: (disabled && !skipDisable.includes('CurrencyDataEditorModel.label')) }]
		});
		return formGroup;
	}

	fromModel(item: CurrencyFieldData): CurrencyDataEditorModel {
		this.label = item.label;
		return this;
	}
}
