import { FieldDataEditorModel } from "./field-data-editor-model";
import { DatasetProfileInternalDmpEntitiesType } from "../../../../../core/common/enum/dataset-profile-internal-dmp-entities-type";
import { FieldDataOptionEditorModel } from "./field-data-option-editor-model";
import { DatasetsAutoCompleteFieldData } from "../../../../../core/model/dataset-profile-definition/field-data/field-data";
import { FormGroup } from "@angular/forms";

export class DatasetsAutoCompleteFieldDataEditorModel extends FieldDataEditorModel<DatasetsAutoCompleteFieldDataEditorModel> {
	public type: DatasetProfileInternalDmpEntitiesType = DatasetProfileInternalDmpEntitiesType.Datasets;
	public multiAutoComplete: boolean = false;
	public autoCompleteOptions: FieldDataOptionEditorModel = new FieldDataOptionEditorModel();
	public autoCompleteType: number;

	buildForm(disabled: boolean = false, skipDisable: Array<String> = []): FormGroup {
		const formGroup = this.formBuilder.group({
			label: [{ value: this.label, disabled: (disabled && !skipDisable.includes('DatasetsAutoCompleteFieldDataEditorModel.label')) }],
			type: [{ value: this.type, disabled: (disabled && !skipDisable.includes('DatasetsAutoCompleteFieldDataEditorModel.type')) }],
			multiAutoComplete: [{ value: this.multiAutoComplete, disabled: (disabled && !skipDisable.includes('DatasetsAutoCompleteFieldDataEditorModel.multiAutoComplete')) }],
			autoCompleteType: [{ value: this.autoCompleteType, disabled: (disabled && !skipDisable.includes('DatasetsAutoCompleteFieldDataEditorModel.autoCompleteType')) }]
		})
		return formGroup;
	}

	fromModel(item: DatasetsAutoCompleteFieldData): DatasetsAutoCompleteFieldDataEditorModel {
		this.label = item.label;
		this.type = item.type;
		this.multiAutoComplete = item.multiAutoComplete;
		this.autoCompleteType = item.autoCompleteType;
		return this;
	}
}
