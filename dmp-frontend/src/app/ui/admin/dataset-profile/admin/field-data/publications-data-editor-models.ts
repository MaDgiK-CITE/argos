import { FormGroup } from '@angular/forms';
import { FieldDataEditorModel } from './field-data-editor-model';
import { PublicationsFieldData } from '../../../../../core/model/dataset-profile-definition/field-data/field-data';
export class PublicationsDataEditorModel extends FieldDataEditorModel<PublicationsDataEditorModel> {
	public label: string;
	public multiAutoComplete: boolean;

	buildForm(disabled: boolean = false, skipDisable: Array<String> = []): FormGroup {
		const formGroup = this.formBuilder.group({
			label: [{ value: this.label, disabled: (disabled && !skipDisable.includes('ServicesDataEditorModel.label')) }],
			multiAutoComplete: [{ value: this.multiAutoComplete, disabled: (disabled && !skipDisable.includes('ServicesDataEditorModel.multiAutoComplete')) }]
		});
		return formGroup;
	}

	fromModel(item: PublicationsFieldData): PublicationsDataEditorModel {
		this.label = item.label;
		this.multiAutoComplete = item.multiAutoComplete;
		return this;
	}
}
