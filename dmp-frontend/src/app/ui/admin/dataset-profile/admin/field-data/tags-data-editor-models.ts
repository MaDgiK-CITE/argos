import { FormGroup } from '@angular/forms';
import { FieldDataEditorModel } from './field-data-editor-model';
import { TagsFieldData } from '../../../../../core/model/dataset-profile-definition/field-data/field-data';

export class TagsDataEditorModel extends FieldDataEditorModel<TagsDataEditorModel> {
	public label: string;

	buildForm(disabled: boolean = false, skipDisable: Array<String> = []): FormGroup {
		const formGroup = this.formBuilder.group({
			label: [{ value: this.label, disabled: (disabled && !skipDisable.includes('TagsDataEditorModel.label')) }]
		});
		return formGroup;
	}

	fromModel(item: TagsFieldData): TagsDataEditorModel {
		this.label = item.label;
		return this;
	}
}
