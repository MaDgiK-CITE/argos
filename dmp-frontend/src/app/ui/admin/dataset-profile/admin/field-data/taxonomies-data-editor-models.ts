import { FormGroup } from '@angular/forms';
import { FieldDataEditorModel } from './field-data-editor-model';
import {TaxonomiesFieldData} from "@app/core/model/dataset-profile-definition/field-data/field-data";

export class TaxonomiesDataEditorModel extends FieldDataEditorModel<TaxonomiesDataEditorModel> {
	public label: string;
	public multiAutoComplete: boolean;

	buildForm(disabled: boolean = false, skipDisable: Array<String> = []): FormGroup {
		const formGroup = this.formBuilder.group({
			label: [{ value: this.label, disabled: (disabled && !skipDisable.includes('ServicesDataEditorModel.label')) }],
			multiAutoComplete: [{ value: this.multiAutoComplete, disabled: (disabled && !skipDisable.includes('ServicesDataEditorModel.multiAutoComplete')) }]
		});
		return formGroup;
	}

	fromModel(item: TaxonomiesFieldData): TaxonomiesDataEditorModel {
		this.label = item.label;
		this.multiAutoComplete = item.multiAutoComplete;
		return this;
	}
}
