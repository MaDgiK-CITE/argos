import { FormGroup } from '@angular/forms';
import {
	FieldDataOption,
	UploadFieldData
} from '../../../../../core/model/dataset-profile-definition/field-data/field-data';
import { FieldDataEditorModel } from './field-data-editor-model';
import {FieldDataOptionEditorModel} from "@app/ui/admin/dataset-profile/admin/field-data/field-data-option-editor-model";

export class UploadFieldDataEditorModel extends FieldDataEditorModel<UploadFieldDataEditorModel> {
	public types: Array<FieldDataOptionEditorModel> = [];
	public maxFileSizeInMB: number;

	buildForm(disabled: boolean = false, skipDisable: Array<String> = []): FormGroup {
		const formGroup = this.formBuilder.group({
			label: [{ value: this.label, disabled: (disabled && !skipDisable.includes('UploadFieldDataEditorModel.label')) }],
			maxFileSizeInMB: [{ value: this.maxFileSizeInMB, disabled: (disabled && !skipDisable.includes('UploadFieldDataEditorModel.maxFileSizeInMB')) }],
			// types: [{ value: this.types, disabled: (disabled && !skipDisable.includes('UploadFieldDataEditorModel.types')) }]
		});
		const optionsFormArray = new Array<FormGroup>();
		if (this.types) {
			this.types.forEach(item => {
				const form: FormGroup = item.buildForm(disabled, skipDisable);
				optionsFormArray.push(form);
			});
		}
		formGroup.addControl('types', this.formBuilder.array(optionsFormArray));
		if(disabled && !skipDisable.includes('UploadFieldDataEditorModel.types')) {
			formGroup.disable();
		}
		return formGroup;	}

	fromModel(item: UploadFieldData): UploadFieldDataEditorModel {
		if (item.types) { this.types = item.types.map(x => new FieldDataOptionEditorModel().fromModel(x)); }
		this.label = item.label;
		this.maxFileSizeInMB = item.maxFileSizeInMB;
		// this.types = item.types;
		return this;
	}
}
