import { FieldDataEditorModel } from './field-data-editor-model';
import { DatasetProfileComboBoxType } from '@app/core/common/enum/dataset-profile-combo-box-type';
import { FieldDataOptionEditorModel } from './field-data-option-editor-model';
import { FormGroup, Validators } from '@angular/forms';
import { AutoCompleteFieldData, AutoCompleteSingleData } from '@app/core/model/dataset-profile-definition/field-data/field-data';
import { AuthFieldEditorModel } from './auto-complete-auth-field-data.model';

export class AutoCompleteSingleDataEditorModel extends FieldDataEditorModel<AutoCompleteSingleDataEditorModel> {

	public url: string;
	public optionsRoot: string;

	public autoCompleteOptions: FieldDataOptionEditorModel = new FieldDataOptionEditorModel();
	public autoCompleteType: number;
	public method: string;
	public hasAuth: boolean;
	public auth: AuthFieldEditorModel = new AuthFieldEditorModel();
	//public multiAutoCompleteOptions: FieldDataOptionEditorModel = new FieldDataOptionEditorModel();

	buildForm(disabled: boolean = false, skipDisable: Array<String> = []): FormGroup {
		const formGroup = this.formBuilder.group({
			label: [{ value: this.label, disabled: (disabled && !skipDisable.includes('AutoCompleteSingleDataEditorModel.label')) }],
			url: [{ value: this.url, disabled: (disabled && !skipDisable.includes('AutoCompleteSingleDataEditorModel.url')) },[Validators.required]],
			optionsRoot: [{ value: this.optionsRoot, disabled: (disabled && !skipDisable.includes('AutoCompleteSingleDataEditorModel.optionsRoot')) }, [Validators.required]],
			autoCompleteType: [{ value: this.autoCompleteType, disabled: (disabled && !skipDisable.includes('AutoCompleteSingleDataEditorModel.autoCompleteType')) }],
			hasAuth: [{ value: this.hasAuth, disabled: (disabled && !skipDisable.includes('AutoCompleteSingleDataEditorModel.hasAuth')) }],
			method: [{ value: this.method, disabled: (disabled && !skipDisable.includes('AutoCompleteSingleDataEditorModel.method')) }]
		});
		formGroup.addControl('autoCompleteOptions', this.autoCompleteOptions.buildForm(disabled, skipDisable));
		formGroup.addControl('auth', this.auth.buildForm(disabled, skipDisable));

		return formGroup;
	}

	fromModel(item: AutoCompleteSingleData): AutoCompleteSingleDataEditorModel {
		this.url = item.url;
		this.label = item.label;
		this.optionsRoot = item.optionsRoot;
		this.autoCompleteType = item.autocompleteType;
		this.hasAuth = item.hasAuth;
		this.method = item.method ? item.method : 'GET';
		this.autoCompleteOptions = new FieldDataOptionEditorModel().fromModel(item.autoCompleteOptions);
		this.auth = new AuthFieldEditorModel().fromModel(item.auth);
		return this;
	}
}
