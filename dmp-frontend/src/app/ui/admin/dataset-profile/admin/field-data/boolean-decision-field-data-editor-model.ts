import { FormGroup } from '@angular/forms';
import { BooleanDecisionFieldData } from '../../../../../core/model/dataset-profile-definition/field-data/field-data';
import { FieldDataEditorModel } from './field-data-editor-model';

export class BooleanDecisionFieldDataEditorModel extends FieldDataEditorModel<BooleanDecisionFieldDataEditorModel> {

	buildForm(disabled: boolean = false, skipDisable: Array<String> = []): FormGroup {
		const formGroup = this.formBuilder.group({
			label: [{ value: this.label, disabled: (disabled && !skipDisable.includes('BooleanDecisionFieldDataEditorModel.label')) }]
		});
		return formGroup;
	}

	fromModel(item: BooleanDecisionFieldData): BooleanDecisionFieldDataEditorModel {
		this.label = item.label;
		return this;
	}
}
