import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FieldDataOption } from '../../../../../core/model/dataset-profile-definition/field-data/field-data';
import { FieldDataEditorModel } from './field-data-editor-model';

export class FieldDataOptionEditorModel extends FieldDataEditorModel<FieldDataOptionEditorModel> {
	public label: string;
	public value: string;
	public source: string;

	buildForm(disabled: boolean = false, skipDisable: Array<String> = []): FormGroup {
		return new FormBuilder().group({
			label: [{ value: this.label, disabled: (disabled && !skipDisable.includes('FieldDataOptionEditorModel.label')) },[Validators.required]],
			value: [{ value: this.value, disabled: (disabled && !skipDisable.includes('FieldDataOptionEditorModel.value')) },[Validators.required]],
			source: [{ value: this.source, disabled: (disabled && !skipDisable.includes('FieldDataOptionEditorModel.source')) }]
		});
	}

	fromModel(item: FieldDataOption): FieldDataOptionEditorModel {
		if (item) {
			this.label = item.label;
			this.value = item.value;
			this.source = item.source;
		}
		return this;
	}
}
