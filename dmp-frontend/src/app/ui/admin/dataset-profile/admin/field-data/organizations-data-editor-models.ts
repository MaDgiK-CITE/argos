import { FormGroup } from '@angular/forms';
import { FieldDataEditorModel } from './field-data-editor-model';
import { OrganizationsFieldData } from '../../../../../core/model/dataset-profile-definition/field-data/field-data';

export class OrganizationsDataEditorModel extends FieldDataEditorModel<OrganizationsDataEditorModel> {
	public label: string;
	public multiAutoComplete: boolean = false;

	buildForm(disabled: boolean = false, skipDisable: Array<String> = []): FormGroup {
		const formGroup = this.formBuilder.group({
			label: [{ value: this.label, disabled: (disabled && !skipDisable.includes('OrganizationsDataEditorModel.label')) }],
			multiAutoComplete: [{ value: this.multiAutoComplete, disabled: (disabled && !skipDisable.includes('OrganizationsDataEditorModel.multiAutoComplete')) }]
		});
		return formGroup;
	}

	fromModel(item: OrganizationsFieldData): OrganizationsDataEditorModel {
		this.label = item.label;
		this.multiAutoComplete = item.multiAutoComplete;
		return this;
	}
}
