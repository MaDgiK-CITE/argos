import { FormGroup } from '@angular/forms';
import { TextAreaFieldData } from '../../../../../core/model/dataset-profile-definition/field-data/field-data';
import { FieldDataEditorModel } from './field-data-editor-model';

export class TextAreaFieldDataEditorModel extends FieldDataEditorModel<TextAreaFieldDataEditorModel> {

	buildForm(disabled: boolean = false, skipDisable: Array<String> = []): FormGroup {
		const formGroup = this.formBuilder.group({
			label: [{ value: this.label, disabled: (disabled && !skipDisable.includes('TextAreaFieldDataEditorModel.label')) }]
		});
		return formGroup;
	}

	fromModel(item: TextAreaFieldData): TextAreaFieldDataEditorModel {
		this.label = item.label;
		return this;
	}
}
