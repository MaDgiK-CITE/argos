import { FieldDataEditorModel } from "./field-data-editor-model";
import { DatasetProfileInternalDmpEntitiesType } from "../../../../../core/common/enum/dataset-profile-internal-dmp-entities-type";
import { FieldDataOptionEditorModel } from "./field-data-option-editor-model";
import { FormGroup } from "@angular/forms";
import { ResearchersAutoCompleteFieldData } from "../../../../../core/model/dataset-profile-definition/field-data/field-data";

export class ResearchersAutoCompleteFieldDataEditorModel extends FieldDataEditorModel<ResearchersAutoCompleteFieldDataEditorModel> {

	public type: DatasetProfileInternalDmpEntitiesType = DatasetProfileInternalDmpEntitiesType.Researchers;
	public multiAutoComplete: boolean = false;
	public autoCompleteOptions: FieldDataOptionEditorModel = new FieldDataOptionEditorModel();

	buildForm(disabled: boolean = false, skipDisable: Array<String> = []): FormGroup {
		const formGroup = this.formBuilder.group({
			label: [{ value: this.label, disabled: (disabled && !skipDisable.includes('ResearchersAutoCompleteFieldDataEditorModel.label')) }],
			type: [{ value: this.type, disabled: (disabled && !skipDisable.includes('ResearchersAutoCompleteFieldDataEditorModel.type')) }],
			multiAutoComplete: [{ value: this.multiAutoComplete, disabled: (disabled && !skipDisable.includes('ResearchersAutoCompleteFieldDataEditorModel.multiAutoComplete')) }]
		})
		//formGroup.addControl('autoCompleteOptions', this.autoCompleteOptions.buildForm(disabled, skipDisable));

		return formGroup;
	}

	fromModel(item: ResearchersAutoCompleteFieldData): ResearchersAutoCompleteFieldDataEditorModel {
		this.label = item.label;
		this.type = item.type;
		this.multiAutoComplete = item.multiAutoComplete;
		// this.autoCompleteOptions = new FieldDataOptionEditorModel().fromModel(item.autoCompleteOptions);
		return this;
	}
}
