import { FormGroup, Validators } from "@angular/forms";
import { FieldDataEditorModel } from "./field-data-editor-model";
import { AuthAutoCompleteData, AutoCompleteSingleData } from "@app/core/model/dataset-profile-definition/field-data/field-data";

export class AuthFieldEditorModel extends FieldDataEditorModel<AuthFieldEditorModel> {
	url: string;
	method: string;
	body: string;
	path: string;
	type: string;

	buildForm(disabled: boolean = false, skipDisable: Array<String> = []): FormGroup {
		const formGroup = this.formBuilder.group({
			method: [{ value: this.method, disabled: (disabled && !skipDisable.includes('AuthFieldEditorModel.method')) }],
			url: [{ value: this.url, disabled: (disabled && !skipDisable.includes('AuthFieldEditorModel.url')) }, [Validators.required]],
			body: [{ value: this.body, disabled: (disabled && !skipDisable.includes('AuthFieldEditorModel.body')) }],
			path: [{ value: this.path, disabled: (disabled && !skipDisable.includes('AuthFieldEditorModel.path')) }, [Validators.required]],
			type: [{ value: this.type, disabled: (disabled && !skipDisable.includes('AuthFieldEditorModel.type')) }, [Validators.required]]
		});

		return formGroup;
	}

	fromModel(item: AuthAutoCompleteData): AuthFieldEditorModel {
		if (item) {
			this.url = item.url;
			this.method = item.method;
			this.body = item.body;
			this.path = item.path;
			this.type = item.type;
		}
		return this;
	}

}
