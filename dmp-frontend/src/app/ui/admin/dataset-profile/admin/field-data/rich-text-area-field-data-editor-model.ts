import { FormGroup } from '@angular/forms';
import { RichTextAreaFieldData } from '../../../../../core/model/dataset-profile-definition/field-data/field-data';
import { FieldDataEditorModel } from './field-data-editor-model';

export class RichTextAreaFieldDataEditorModel extends FieldDataEditorModel<RichTextAreaFieldDataEditorModel> {

	buildForm(disabled: boolean = false, skipDisable: Array<String> = []): FormGroup {
		const formGroup = this.formBuilder.group({
			label: [{ value: this.label, disabled: (disabled && !skipDisable.includes('RichTextAreaFieldDataEditorModel.label')) }]
		});
		return formGroup;
	}

	fromModel(item: RichTextAreaFieldData): RichTextAreaFieldDataEditorModel {
		this.label = item.label;
		return this;
	}
}
