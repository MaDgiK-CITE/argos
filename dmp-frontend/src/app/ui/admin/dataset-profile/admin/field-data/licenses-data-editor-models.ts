import { FormGroup } from '@angular/forms';
import { FieldDataEditorModel } from './field-data-editor-model';
import { LicensesFieldData } from '../../../../../core/model/dataset-profile-definition/field-data/field-data';
export class LicensesDataEditorModel extends FieldDataEditorModel<LicensesDataEditorModel> {
	public label: string;
	public multiAutoComplete: boolean;

	buildForm(disabled: boolean = false, skipDisable: Array<String> = []): FormGroup {
		const formGroup = this.formBuilder.group({
			label: [{ value: this.label, disabled: (disabled && !skipDisable.includes('ServicesDataEditorModel.label')) }],
			multiAutoComplete: [{ value: this.multiAutoComplete, disabled: (disabled && !skipDisable.includes('ServicesDataEditorModel.multiAutoComplete')) }]
		});
		return formGroup;
	}

	fromModel(item: LicensesFieldData): LicensesDataEditorModel {
		this.label = item.label;
		this.multiAutoComplete = item.multiAutoComplete;
		return this;
	}
}
