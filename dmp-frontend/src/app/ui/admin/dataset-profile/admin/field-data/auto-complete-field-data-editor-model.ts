import { FormGroup } from '@angular/forms';
import { DatasetProfileComboBoxType } from '../../../../../core/common/enum/dataset-profile-combo-box-type';
import { AutoCompleteFieldData } from '../../../../../core/model/dataset-profile-definition/field-data/field-data';
import { FieldDataEditorModel } from './field-data-editor-model';
import { FieldDataOptionEditorModel } from './field-data-option-editor-model';
import { AutoCompleteSingleDataEditorModel } from './auto-complete-single-data';

export class AutoCompleteFieldDataEditorModel extends FieldDataEditorModel<AutoCompleteFieldDataEditorModel> {

	public type: DatasetProfileComboBoxType = DatasetProfileComboBoxType.Autocomplete;
	public multiAutoComplete: boolean;
	public autoCompleteSingleDataList: Array<AutoCompleteSingleDataEditorModel> = new Array<AutoCompleteSingleDataEditorModel>();
	//public multiAutoCompleteOptions: FieldDataOptionEditorModel = new FieldDataOptionEditorModel();

	buildForm(disabled: boolean = false, skipDisable: Array<String> = []): FormGroup {
		const formGroup = this.formBuilder.group({
			label: [{ value: this.label, disabled: (disabled && !skipDisable.includes('AutoCompleteFieldDataEditorModel.label')) }],
			type: [{ value: this.type, disabled: (disabled && !skipDisable.includes('AutoCompleteFieldDataEditorModel.type')) }],
			multiAutoComplete: [{ value: this.multiAutoComplete, disabled: (disabled && !skipDisable.includes('AutoCompleteFieldDataEditorModel.multiAutoComplete')) }]
		});

		const autocompleteFormArray = new Array<FormGroup>();
		if (this.autoCompleteSingleDataList) {
			this.autoCompleteSingleDataList.forEach(item => {
				const form: FormGroup = item.buildForm(disabled, skipDisable);
				autocompleteFormArray.push(form);
			});
		}

		formGroup.addControl('autoCompleteSingleDataList', this.formBuilder.array(autocompleteFormArray));

		return formGroup;
	}

	fromModel(item: AutoCompleteFieldData): AutoCompleteFieldDataEditorModel {
		this.type = item.type;
		this.label = item.label;
		this.multiAutoComplete = item.multiAutoComplete;
		if (item.autoCompleteSingleDataList) { this.autoCompleteSingleDataList = item.autoCompleteSingleDataList.map(x => new AutoCompleteSingleDataEditorModel().fromModel(x)); }
		return this;
	}
}
