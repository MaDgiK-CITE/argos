import { FormGroup } from '@angular/forms';
import { FieldDataEditorModel } from './field-data-editor-model';
import { ExternalDatasetsFieldData } from '../../../../../core/model/dataset-profile-definition/field-data/field-data';
import { ExternalDatasetTypeEnum } from '@app/core/common/enum/external-dataset-type-enum';

export class ExternalDatasetsDataEditorModel extends FieldDataEditorModel<ExternalDatasetsDataEditorModel> {
	public label: string;
	public multiAutoComplete: boolean;
	public type: ExternalDatasetTypeEnum;

	buildForm(disabled: boolean = false, skipDisable: Array<String> = []): FormGroup {
		const formGroup = this.formBuilder.group({
			label: [{ value: this.label, disabled: (disabled && !skipDisable.includes('ExternalDatasetsDataEditorModel.label')) }],
			multiAutoComplete: [{ value: this.multiAutoComplete, disabled: (disabled && !skipDisable.includes('ExternalDatasetsDataEditorModel.multiAutoComplete')) }],
			type: [{ value: this.type, disabled: (disabled && !skipDisable.includes('ExternalDatasetsDataEditorModel.type')) }]
		});
		return formGroup;
	}

	fromModel(item: ExternalDatasetsFieldData): ExternalDatasetsDataEditorModel {
		this.label = item.label;
		this.multiAutoComplete = item.multiAutoComplete;
		this.type = item.type;
		return this;
	}
}
