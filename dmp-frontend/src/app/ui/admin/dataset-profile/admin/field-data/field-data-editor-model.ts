import { FormGroup } from '@angular/forms';
import { BaseFormModel } from '../../../../../core/model/base-form-model';

export abstract class FieldDataEditorModel<T> extends BaseFormModel {

	public label: string;

	buildForm(disabled: boolean = false, skipDisable: Array<String> = []): FormGroup {
		throw new Error('Build Form Is not not correctly overriden');
	}

	fromJSONObject(item: any): T {
		throw new Error('From Json Object is not correctly overriden');
	}
}
