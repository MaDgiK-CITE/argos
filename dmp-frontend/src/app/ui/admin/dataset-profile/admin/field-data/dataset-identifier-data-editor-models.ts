import { FormGroup } from '@angular/forms';
import { FieldDataEditorModel } from './field-data-editor-model';
import { DatasetIdentifierFieldData } from '../../../../../core/model/dataset-profile-definition/field-data/field-data';

export class DatasetIdentifierDataEditorModel extends FieldDataEditorModel<DatasetIdentifierDataEditorModel> {
	public label: string;

	buildForm(disabled: boolean = false, skipDisable: Array<String> = []): FormGroup {
		const formGroup = this.formBuilder.group({
			label: [{ value: this.label, disabled: (disabled && !skipDisable.includes('DatasetIdentifierDataEditorModel.label')) }]
		});
		return formGroup;
	}

	fromModel(item: DatasetIdentifierFieldData): DatasetIdentifierDataEditorModel {
		this.label = item.label;
		return this;
	}
}
