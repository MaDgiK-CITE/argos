import { FormGroup } from '@angular/forms';
import { RadioBoxFieldData } from '../../../../../core/model/dataset-profile-definition/field-data/field-data';
import { FieldDataEditorModel } from './field-data-editor-model';
import { FieldDataOptionEditorModel } from './field-data-option-editor-model';

export class RadioBoxFieldDataEditorModel extends FieldDataEditorModel<RadioBoxFieldDataEditorModel> {

	public options: Array<FieldDataOptionEditorModel> = [];

	buildForm(disabled: boolean = false, skipDisable: Array<String> = []): FormGroup {
		const formGroup = this.formBuilder.group({
			label: [{ value: this.label, disabled: (disabled && !skipDisable.includes('RadioBoxFieldDataEditorModel.label')) }]
		});
		const optionsFormArray = new Array<FormGroup>();
		if (this.options) {
			this.options.forEach(item => {
				const form: FormGroup = item.buildForm(disabled, skipDisable);
				optionsFormArray.push(form);
			});
		}
		formGroup.addControl('options', this.formBuilder.array(optionsFormArray));
		return formGroup;
	}

	fromModel(item: RadioBoxFieldData): RadioBoxFieldDataEditorModel {
		if (item.options) { this.options = item.options.map(x => new FieldDataOptionEditorModel().fromModel(x)); }
		this.label = item.label;
		return this;
	}
}
