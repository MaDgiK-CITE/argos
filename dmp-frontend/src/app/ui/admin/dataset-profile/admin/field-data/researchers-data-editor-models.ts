import { FormGroup } from '@angular/forms';
import { FieldDataEditorModel } from './field-data-editor-model';
import { ResearchersFieldData } from '../../../../../core/model/dataset-profile-definition/field-data/field-data';

export class ResearchersDataEditorModel extends FieldDataEditorModel<ResearchersDataEditorModel> {
	public label: string;

	buildForm(disabled: boolean = false, skipDisable: Array<String> = []): FormGroup {
		const formGroup = this.formBuilder.group({
			label: [{ value: this.label, disabled: (disabled && !skipDisable.includes('ResearchersDataEditorModel.label')) }]
		});
		return formGroup;
	}

	fromModel(item: ResearchersFieldData): ResearchersDataEditorModel {
		this.label = item.label;
		return this;
	}
}
