import { FormGroup } from '@angular/forms';
import { FieldDataEditorModel } from './field-data-editor-model';
import { ValidationFieldData } from '../../../../../core/model/dataset-profile-definition/field-data/field-data';

export class ValidationDataEditorModel extends FieldDataEditorModel<ValidationDataEditorModel> {
	public label: string;

	buildForm(disabled: boolean = false, skipDisable: Array<String> = []): FormGroup {
		const formGroup = this.formBuilder.group({
			label: [{ value: this.label, disabled: (disabled && !skipDisable.includes('ValidationDataEditorModel.label')) }]
		});
		return formGroup;
	}

	fromModel(item: ValidationFieldData): ValidationDataEditorModel {
		this.label = item.label;
		return this;
	}
}
