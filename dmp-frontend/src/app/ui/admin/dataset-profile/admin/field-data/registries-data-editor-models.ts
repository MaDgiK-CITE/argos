import { FormGroup } from '@angular/forms';
import { FieldDataEditorModel } from './field-data-editor-model';
import { RegistriesFieldData } from '../../../../../core/model/dataset-profile-definition/field-data/field-data';

export class RegistriesDataEditorModel extends FieldDataEditorModel<RegistriesDataEditorModel> {
	public label: string;
	public multiAutoComplete: boolean;

	buildForm(disabled: boolean = false, skipDisable: Array<String> = []): FormGroup {
		const formGroup = this.formBuilder.group({
			label: [{ value: this.label, disabled: (disabled && !skipDisable.includes('RegistriesDataEditorModel.label')) }],
			multiAutoComplete: [{ value: this.multiAutoComplete, disabled: (disabled && !skipDisable.includes('RegistriesDataEditorModel.multiAutoComplete')) }]
		});
		return formGroup;
	}

	fromModel(item: RegistriesFieldData): RegistriesDataEditorModel {
		this.label = item.label;
		this.multiAutoComplete = item.multiAutoComplete;
		return this;
	}
}
