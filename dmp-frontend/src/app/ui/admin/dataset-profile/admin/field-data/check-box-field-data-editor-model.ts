import { FormGroup } from '@angular/forms';
import { CheckBoxFieldData } from '../../../../../core/model/dataset-profile-definition/field-data/field-data';
import { FieldDataEditorModel } from './field-data-editor-model';

export class CheckBoxFieldDataEditorModel extends FieldDataEditorModel<CheckBoxFieldDataEditorModel> {

	buildForm(disabled: boolean = false, skipDisable: Array<String> = []): FormGroup {
		const formGroup = this.formBuilder.group({
			label: [{ value: this.label, disabled: (disabled && !skipDisable.includes('CheckBoxFieldDataEditorModel.label')) }]
		});
		return formGroup;
	}

	fromModel(item: CheckBoxFieldData): CheckBoxFieldDataEditorModel {
		this.label = item.label;
		return this;
	}
}
