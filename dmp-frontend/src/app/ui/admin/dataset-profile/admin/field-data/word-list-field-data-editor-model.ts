import { FormGroup } from '@angular/forms';
import { DatasetProfileComboBoxType } from '../../../../../core/common/enum/dataset-profile-combo-box-type';
import { WordListFieldData } from '../../../../../core/model/dataset-profile-definition/field-data/field-data';
import { FieldDataEditorModel } from './field-data-editor-model';
import { FieldDataOptionEditorModel } from './field-data-option-editor-model';

export class WordListFieldDataEditorModel extends FieldDataEditorModel<WordListFieldDataEditorModel> {
	public type: DatasetProfileComboBoxType = DatasetProfileComboBoxType.WordList;
	public options: Array<FieldDataOptionEditorModel>;
	public multipleList: boolean;

	buildForm(disabled: boolean = false, skipDisable: Array<String> = []): FormGroup {
		const formGroup = this.formBuilder.group({
			type: [{ value: this.type, disabled: (disabled && !skipDisable.includes('WordListFieldDataEditorModel.type')) }],
			label: [{ value: this.label, disabled: (disabled && !skipDisable.includes('WordListFieldDataEditorModel.label')) }],
			multiList: [{ value: this.multipleList, disabled: (disabled && !skipDisable.includes('WordListFieldDataEditorModel.multipleList'))}]
		});
		const optionsFormArray = new Array<FormGroup>();
		if (this.options) {
			this.options.forEach(item => {
				const form: FormGroup = item.buildForm(disabled, skipDisable);
				optionsFormArray.push(form);
			});
		}
		formGroup.addControl('options', this.formBuilder.array(optionsFormArray));

		return formGroup;
	}

	fromModel(item: WordListFieldData): WordListFieldDataEditorModel {
		this.type = item.type;
		if (item.options) { this.options = item.options.map(x => new FieldDataOptionEditorModel().fromModel(x)); }
		this.label = item.label;
		this.multipleList = item.multiList;
		return this;
	}
}
