import { FieldDataEditorModel } from "./field-data-editor-model";
import { DatasetProfileInternalDmpEntitiesType } from "../../../../../core/common/enum/dataset-profile-internal-dmp-entities-type";
import { FieldDataOptionEditorModel } from "./field-data-option-editor-model";
import { DmpsAutoCompleteFieldData } from "../../../../../core/model/dataset-profile-definition/field-data/field-data";
import { FormGroup } from "@angular/forms";

export class DmpsAutoCompleteFieldDataEditorModel extends FieldDataEditorModel<DmpsAutoCompleteFieldDataEditorModel> {
	public type: DatasetProfileInternalDmpEntitiesType = DatasetProfileInternalDmpEntitiesType.Dmps;
	public multiAutoComplete: boolean = false;
	public autoCompleteOptions: FieldDataOptionEditorModel = new FieldDataOptionEditorModel();

	buildForm(disabled: boolean = false, skipDisable: Array<String> = []): FormGroup {
		const formGroup = this.formBuilder.group({
			label: [{ value: this.label, disabled: (disabled && !skipDisable.includes('DmpsAutoCompleteFieldDataEditorModel.label')) }],
			type: [{ value: this.type, disabled: (disabled && !skipDisable.includes('DmpsAutoCompleteFieldDataEditorModel.type')) }],
			multiAutoComplete: [{ value: this.multiAutoComplete, disabled: (disabled && !skipDisable.includes('DmpsAutoCompleteFieldDataEditorModel.multiAutoComplete')) }]
		})
		return formGroup;
	}

	fromModel(item: DmpsAutoCompleteFieldData): DmpsAutoCompleteFieldDataEditorModel {
		this.label = item.label;
		this.type = item.type;
		this.multiAutoComplete = item.multiAutoComplete;
		return this;
	}
}
