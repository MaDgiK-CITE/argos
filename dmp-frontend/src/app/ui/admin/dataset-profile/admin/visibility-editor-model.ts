﻿import { FormGroup } from '@angular/forms';
import { Visibility } from '../../../../core/model/admin/dataset-profile/dataset-profile';
import { BaseFormModel } from '../../../../core/model/base-form-model';
import { RuleEditorModel } from './rule-editor-model';

export class VisibilityEditorModel extends BaseFormModel {

	public rules: Array<RuleEditorModel> = new Array<RuleEditorModel>();
	public style: string;

	fromModel(item: Visibility): VisibilityEditorModel {
		if (item.rules) { this.rules = item.rules.map(x => new RuleEditorModel().fromModel(x)); }
		this.style = item.style;

		return this;
	}

	buildForm(disabled: boolean = false, skipDisable: Array<String> = []): FormGroup {
		const formGroup = this.formBuilder.group({
			style: [{ value: this.style, disabled: (disabled && !skipDisable.includes('VisibilityEditorModel.style')) }]
		});

		const rulesFormArray = new Array<FormGroup>();
		if (this.rules) {
			this.rules.forEach(rule => {
				const form: FormGroup = rule.buildForm(disabled, skipDisable);
				rulesFormArray.push(form);
			});
		}

		formGroup.addControl('rules', this.formBuilder.array(rulesFormArray));

		return formGroup;
	}

}
