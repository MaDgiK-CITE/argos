﻿import { FormGroup, Validators } from "@angular/forms";
import { ViewStyle } from "../../../../core/model/admin/dataset-profile/dataset-profile";
import { BaseFormModel } from "../../../../core/model/base-form-model";

export class ViewStyleEditorModel extends BaseFormModel {
	public cssClass: string;
	public renderStyle: string;

	fromModel(item: ViewStyle): ViewStyleEditorModel {
		this.cssClass = item.cssClass;
		this.renderStyle = item.renderStyle;
		return this;
	}

	buildForm(disabled: boolean = false, skipDisable: Array<String> = []): FormGroup {
		const formGroup = this.formBuilder.group({
			cssClass: [{ value: this.cssClass, disabled: (disabled && !skipDisable.includes('ViewStyleEditorModel.cssClass')) }],
			renderStyle: [{ value: this.renderStyle, disabled: (disabled && !skipDisable.includes('ViewStyleEditorModel.renderStyle')) }, Validators.required]
		});
		return formGroup;
	}
}
