﻿import { FormGroup, Validators } from '@angular/forms';
import { Page } from '@app/core/model/admin/dataset-profile/dataset-profile';
import { BaseFormModel } from '@app/core/model/base-form-model';
import { Guid } from '@common/types/guid';

export class PageEditorModel extends BaseFormModel {
	public title: string;
	public id: string;
	public ordinal: number;

	constructor(ordinal?: number) {
		super();
		if (isNaN(ordinal)) { this.ordinal = 0; } else { this.ordinal = ordinal; }
		this.id = Guid.create().toString();
	}

	fromModel(item: Page): PageEditorModel {
		this.title = item.title;
		this.id = item.id;
		this.ordinal = item.ordinal;
		return this;
	}

	buildForm(disabled: boolean = false, skipDisable: Array<String> = []): FormGroup {
		const formGroup = this.formBuilder.group({
			title: [{ value: this.title, disabled: (disabled && !skipDisable.includes('PageEditorModel.title')) }, [Validators.required]],
			id: [{ value: this.id, disabled: (disabled && !skipDisable.includes('PageEditorModel.id')) }, [Validators.required]],
			ordinal: [{ value: this.ordinal, disabled: (disabled && !skipDisable.includes('PageEditorModel.ordinal')) }]
		});
		return formGroup;
	}
}
