﻿import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Section } from '../../../../core/model/admin/dataset-profile/dataset-profile';
import { BaseFormModel } from '../../../../core/model/base-form-model';
import { EditorCustomValidators } from '../editor/custom-validators/editor-custom-validators';
import { FieldSetEditorModel } from './field-set-editor-model';

export class SectionEditorModel extends BaseFormModel {
	public sections: Array<SectionEditorModel> = new Array<SectionEditorModel>();
	public defaultVisibility: boolean;
	public page: string;
	public id: string;
	public title: string;
	public description: string;
	public ordinal: number;
	public fieldSets: Array<FieldSetEditorModel> = new Array<FieldSetEditorModel>();

	fromModel(item: Section): SectionEditorModel {
		if (item.sections) { this.sections = item.sections.map(x => new SectionEditorModel().fromModel(x)); }
		this.page = item.page;
		this.defaultVisibility = item.defaultVisibility;
		this.id = item.id;
		this.title = item.title;
		this.description = item.description;
		this.ordinal = item.ordinal;
		if (item.fieldSets) { this.fieldSets = item.fieldSets.map(x => new FieldSetEditorModel().fromModel(x)); }
		return this;
	}

	buildForm(disabled: boolean = false, skipDisable: Array<String> = []): FormGroup {
		const formGroup: FormGroup = new FormBuilder().group({
			id: [{ value: this.id, disabled: (disabled && !skipDisable.includes('SectionEditorModel.id')) }, [Validators.required, Validators.pattern('^[^<_>]+$')]],
			page: [{ value: this.page, disabled: (disabled && !skipDisable.includes('SectionEditorModel.page')) }, [Validators.required]],
			title: [{ value: this.title, disabled: (disabled && !skipDisable.includes('SectionEditorModel.title')) } , [Validators.required]],
			description: [{ value: this.description, disabled: (disabled && !skipDisable.includes('SectionEditorModel.description')) }],
			ordinal: [{ value: this.ordinal, disabled: (disabled && !skipDisable.includes('SectionEditorModel.ordinal')) }, [Validators.required]],
			defaultVisibility: [{ value: this.defaultVisibility, disabled: (disabled && !skipDisable.includes('SectionEditorModel.defaultVisibility')) }]
		});
		const sectionsFormArray = new Array<FormGroup>();
		if (this.sections) {
			this.sections.forEach(item => {
				const form: FormGroup = item.buildForm(disabled, skipDisable);
				sectionsFormArray.push(form);
			});
		}
		const compositeFieldsFormArray = new Array<FormGroup>();
		if (this.fieldSets) {
			this.fieldSets.forEach(item => {
				const form: FormGroup = item.buildForm(disabled, skipDisable);
				compositeFieldsFormArray.push(form);
			});
		}
		formGroup.addControl('sections', this.formBuilder.array(sectionsFormArray));
		formGroup.addControl('fieldSets', this.formBuilder.array(compositeFieldsFormArray));

		if (!formGroup.controls['defaultVisibility'].value) { formGroup.controls['defaultVisibility'].setValue(true); }

		formGroup.setValidators(EditorCustomValidators.sectionHasAtLeastOneChildOf('fieldSets','sections'));
		formGroup.updateValueAndValidity();
		return formGroup;
	}
}
