import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DatasetProfileEditorComponent } from './editor/dataset-profile-editor.component';
import { DatasetProfileListingComponent } from './listing/dataset-profile-listing.component';
import { AdminAuthGuard } from '@app/core/admin-auth-guard.service';
import { AppRole } from '@app/core/common/enum/app-role';
import { SpecialAuthGuard } from '@app/core/special-auth-guard.service';
import { CanDeactivateGuard } from '@app/library/deactivate/can-deactivate.guard';

const routes: Routes = [
	{
		path: 'new',
		component: DatasetProfileEditorComponent,
		data: {
			title: 'GENERAL.TITLES.DATASET-PROFILES-NEW',
			authContext: {
				permissions: [AppRole.Admin, AppRole.DatasetTemplateEditor]
			}
		},
		canActivate: [SpecialAuthGuard],
		canDeactivate:[CanDeactivateGuard]
	},
	{
		path: ':id',
		component: DatasetProfileEditorComponent,
		data: {
			title: 'GENERAL.TITLES.DATASET-PROFILES-EDIT',
			authContext: {
				permissions: [AppRole.Admin, AppRole.DatasetTemplateEditor]
			}
		},
		canActivate: [SpecialAuthGuard],
		canDeactivate:[CanDeactivateGuard]
	},
	{
		path: 'clone/:cloneid',
		component: DatasetProfileEditorComponent,
		data: {
			title: 'GENERAL.TITLES.DATASET-PROFILES-CLONE',
			authContext: {
				permissions: [AppRole.Admin, AppRole.DatasetTemplateEditor]
			}
		},
		canActivate: [SpecialAuthGuard],
		canDeactivate:[CanDeactivateGuard]
	},
	{
		path: 'newversion/:newversionid',
		component: DatasetProfileEditorComponent,
		data: {
			title: 'GENERAL.TITLES.DATASET-PROFILES-NEW-VERSION',
			authContext: {
				permissions: [AppRole.Admin, AppRole.DatasetTemplateEditor]
			}
		},
		canActivate: [SpecialAuthGuard],
		canDeactivate:[CanDeactivateGuard]
	},
	{
		path: 'versions/:groupId',
		component: DatasetProfileListingComponent,
		data: {
			authContext: {
				permissions: [AppRole.Admin, AppRole.DatasetTemplateEditor]
			}
		},
		canActivate: [SpecialAuthGuard]
	},
	{
		path: '',
		component: DatasetProfileListingComponent,
		data: {
			authContext: {
				permissions: [AppRole.Admin, AppRole.DatasetTemplateEditor]
			}
		},
		canActivate: [SpecialAuthGuard]
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class DatasetProfileRoutingModule { }
