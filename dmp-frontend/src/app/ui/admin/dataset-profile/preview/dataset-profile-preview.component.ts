import { Inject, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DatasetProfileService } from '@app/core/services/dataset-profile/dataset-profile.service';
import { DatasetWizardEditorModel } from '@app/ui/dataset/dataset-wizard/dataset-wizard-editor.model';
import { BaseComponent } from '@common/base/base.component';
import { takeUntil } from 'rxjs/operators';

// @Component({
// 	selector: 'app-dataset-profile-preview-component',
// 	templateUrl: './dataset-profile-preview.component.html',
// 	styleUrls: ['./dataset-profile-preview.component.scss']
// })
export class DatasetProfilePreviewerComponent extends BaseComponent implements OnInit {
	formGroup: FormGroup;
	datasetWizardModel: DatasetWizardEditorModel;
	constructor(
		private datasetProfileService: DatasetProfileService,
		public dialogRef: MatDialogRef<DatasetProfilePreviewerComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any
	) { super(); }

	ngOnInit(): void {

		this.datasetProfileService.preview(this.data['model'])
			.pipe(takeUntil(this._destroyed))
			.subscribe(x => {
				this.datasetWizardModel = new DatasetWizardEditorModel().fromModel({
					datasetProfileDefinition: x
				});
				this.formGroup = <FormGroup>this.datasetWizardModel.buildForm().get('datasetProfileDefinition');
			});

	}
}
