import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FreeTextFieldDataEditorModel } from '../../../../admin/field-data/free-text-field-data-editor-model';

@Component({
	selector: 'app-dataset-profile-editor-free-text-field-component',
	styleUrls: ['./dataset-profile-editor-free-text-field.component.scss'],
	templateUrl: './dataset-profile-editor-free-text-field.component.html'
})
export class DatasetProfileEditorFreeTextFieldComponent implements OnInit {

	@Input() form: FormGroup;
	private data: FreeTextFieldDataEditorModel = new FreeTextFieldDataEditorModel();

	ngOnInit() {
		if (!this.form.get('data')) { this.form.addControl('data', this.data.buildForm()); }
	}
}
