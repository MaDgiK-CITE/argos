import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DatePickerDataEditorModel } from '../../../../admin/field-data/date-picker-data-editor-models';
import { ExternalDatasetsDataEditorModel } from '@app/ui/admin/dataset-profile/admin/field-data/external-datasets-data-editor-models';
import { DataRepositoriesDataEditorModel } from '@app/ui/admin/dataset-profile/admin/field-data/data-repositories-data-editor-models';

@Component({
	selector: 'app-dataset-profile-editor-pub-repositories-field-component',
	styleUrls: ['./dataset-profile-editor-pub-repositories-field.component.scss'],
	templateUrl: './dataset-profile-editor-pub-repositories-field.component.html'
})
export class DatasetProfileEditorPubRepositoriesFieldComponent implements OnInit {

	@Input() form: FormGroup;
	private data: DataRepositoriesDataEditorModel = new DataRepositoriesDataEditorModel();

	ngOnInit() {
		if (!this.form.get('data')) { this.form.addControl('data', this.data.buildForm()); }
	}
}
