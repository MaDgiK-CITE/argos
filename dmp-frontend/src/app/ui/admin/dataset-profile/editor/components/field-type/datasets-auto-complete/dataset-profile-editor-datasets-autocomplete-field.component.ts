import { OnInit, Input, Component } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { DatasetProfileInternalDmpEntitiesType } from "../../../../../../../core/common/enum/dataset-profile-internal-dmp-entities-type";
import { DatasetsAutoCompleteFieldDataEditorModel } from "../../../../admin/field-data/datasets-autocomplete-field-data-editor-mode";

@Component({
	selector: 'app-dataset-profile-editor-datasets-autocomplete-field-component',
	styleUrls: ['./dataset-profile-editor-datasets-autocomplete-field.component.scss'],
	templateUrl: './dataset-profile-editor-datasets-autocomplete-field.component.html'
})
export class DatasetProfileEditorDatasetsAutoCompleteFieldComponent implements OnInit {

	@Input() form: FormGroup;
	private data: DatasetsAutoCompleteFieldDataEditorModel = new DatasetsAutoCompleteFieldDataEditorModel();

	ngOnInit() {
		this.data.type = DatasetProfileInternalDmpEntitiesType.Datasets;
	}
}
