import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DatePickerDataEditorModel } from '../../../../admin/field-data/date-picker-data-editor-models';
import { ExternalDatasetsDataEditorModel } from '@app/ui/admin/dataset-profile/admin/field-data/external-datasets-data-editor-models';
import { ExternalDatasetTypeEnum } from '@app/core/common/enum/external-dataset-type-enum';

@Component({
	selector: 'app-dataset-profile-editor-external-datasets-field-component',
	styleUrls: ['./dataset-profile-editor-external-datasets-field.component.scss'],
	templateUrl: './dataset-profile-editor-external-datasets-field.component.html'
})
export class DatasetProfileEditorExternalDatasetsFieldComponent implements OnInit {

	@Input() form: FormGroup;
	private data: ExternalDatasetsDataEditorModel = new ExternalDatasetsDataEditorModel();

	externalDatasetTypes = [
		... Object.keys(ExternalDatasetTypeEnum).map(key=>{
			return {
				label: this.parseExtrernalDatasetTypeKey(key),
				value: ExternalDatasetTypeEnum[key]
			};
		})
	];
	ngOnInit() {
		if (!this.form.get('data')) { this.form.addControl('data', this.data.buildForm()); }
	}

	parseExtrernalDatasetTypeKey(key: string): string{
		if(ExternalDatasetTypeEnum[key] === ExternalDatasetTypeEnum.ProducedDataset){
			return 'DATASET-PROFILE-EDITOR.STEPS.FORM.FIELD.FIELDS.EXTERNAL-DATASET-TYPES.PRODUCED';
		}
		if(ExternalDatasetTypeEnum[key] === ExternalDatasetTypeEnum.ReusedDataset){
			return 'DATASET-PROFILE-EDITOR.STEPS.FORM.FIELD.FIELDS.EXTERNAL-DATASET-TYPES.REUSED';
		}
		if(ExternalDatasetTypeEnum[key] === ExternalDatasetTypeEnum.Other){
			return 'DATASET-PROFILE-EDITOR.STEPS.FORM.FIELD.FIELDS.EXTERNAL-DATASET-TYPES.OTHER';
		}
		return key;
	}
}
