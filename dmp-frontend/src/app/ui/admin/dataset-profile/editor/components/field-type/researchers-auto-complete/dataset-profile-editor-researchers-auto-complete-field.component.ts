import { ResearchersAutoCompleteFieldDataEditorModel } from "../../../../admin/field-data/researchers-auto-complete-field-data-editor-model";
import { FormGroup } from "@angular/forms";
import { Input, Component, OnInit } from "@angular/core";
import { DatasetProfileInternalDmpEntitiesType } from "../../../../../../../core/common/enum/dataset-profile-internal-dmp-entities-type";

@Component({
	selector: 'app-dataset-profile-editor-researchers-auto-complete-field-component',
	styleUrls: ['./dataset-profile-editor-researchers-auto-complete-field.component.scss'],
	templateUrl: './dataset-profile-editor-researchers-auto-complete-field.component.html'
})
export class DatasetProfileEditorResearchersAutoCompleteFieldComponent implements OnInit {

	@Input() form: FormGroup;
	private data: ResearchersAutoCompleteFieldDataEditorModel = new ResearchersAutoCompleteFieldDataEditorModel();

	ngOnInit() {
		this.data.type = DatasetProfileInternalDmpEntitiesType.Researchers;
	}
}
