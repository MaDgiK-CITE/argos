﻿import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { FieldDataOptionEditorModel } from '../../../../admin/field-data/field-data-option-editor-model';
import { RadioBoxFieldDataEditorModel } from '../../../../admin/field-data/radio-box-field-data-editor-model';

@Component({
	selector: 'app-dataset-profile-editor-radio-box-field-component',
	styleUrls: ['./dataset-profile-editor-radio-box-field.component.scss'],
	templateUrl: './dataset-profile-editor-radio-box-field.component.html'
})
export class DatasetProfileEditorRadioBoxFieldComponent implements OnInit {

	@Input() form: FormGroup;
	private data: RadioBoxFieldDataEditorModel = new RadioBoxFieldDataEditorModel();

	ngOnInit() {
		if (!this.form.get('data')) { this.form.addControl('data', this.data.buildForm()); }
	}

	addNewRow() {
		const radioListOptions: FieldDataOptionEditorModel = new FieldDataOptionEditorModel();
		if (!this.form.get('data').get('options')) { (<FormGroup>this.form.get('data')).addControl('options', new FormBuilder().array([])); }
		(<FormArray>this.form.get('data').get('options')).push(radioListOptions.buildForm());
	}

	deleteRow(intex: number) {
		if (this.form.get('data').get('options')) { (<FormArray>this.form.get('data').get('options')).removeAt(intex); }
	}
}
