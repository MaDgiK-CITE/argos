
import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { BooleanDecisionFieldDataEditorModel } from '../../../../admin/field-data/boolean-decision-field-data-editor-model';

@Component({
	selector: 'app-dataset-profile-editor-boolean-decision-field-component',
	styleUrls: ['./dataset-profile-editor-boolean-decision-field.component.scss'],
	templateUrl: './dataset-profile-editor-boolean-decision-field.component.html'
})
export class DatasetProfileEditorBooleanDecisionFieldComponent implements OnInit {

	@Input() form: FormGroup;
	private data: BooleanDecisionFieldDataEditorModel = new BooleanDecisionFieldDataEditorModel();

	ngOnInit() {
		if (!this.form.get('data')) { this.form.addControl('data', this.data.buildForm()); }
	}
}
