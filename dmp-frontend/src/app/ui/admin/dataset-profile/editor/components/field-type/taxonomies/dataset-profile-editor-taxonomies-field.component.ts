import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {TaxonomiesDataEditorModel} from "@app/ui/admin/dataset-profile/admin/field-data/taxonomies-data-editor-models";

@Component({
	selector: 'app-dataset-profile-editor-taxonomies-field-component',
	styleUrls: ['./dataset-profile-editor-taxonomies-field.component.scss'],
	templateUrl: './dataset-profile-editor-taxonomies-field.component.html'
})
export class DatasetProfileEditorTaxonomiesFieldComponent implements OnInit {

	@Input() form: FormGroup;
	private data: TaxonomiesDataEditorModel = new TaxonomiesDataEditorModel();

	ngOnInit() {
		if (!this.form.get('data')) { this.form.addControl('data', this.data.buildForm()); }
	}
}
