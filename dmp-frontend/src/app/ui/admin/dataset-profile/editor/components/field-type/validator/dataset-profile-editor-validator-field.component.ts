import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DataRepositoriesDataEditorModel } from '@app/ui/admin/dataset-profile/admin/field-data/data-repositories-data-editor-models';

@Component({
	selector: 'app-dataset-profile-editor-validator-field-component',
	styleUrls: ['./dataset-profile-editor-validator-field.component.scss'],
	templateUrl: './dataset-profile-editor-validator-field.component.html'
})
export class DatasetProfileEditorValidatorFieldComponent implements OnInit {

	@Input() form: FormGroup;
	private data: DataRepositoriesDataEditorModel = new DataRepositoriesDataEditorModel();

	ngOnInit() {
		if (!this.form.get('data')) { this.form.addControl('data', this.data.buildForm()); }
	}
}
