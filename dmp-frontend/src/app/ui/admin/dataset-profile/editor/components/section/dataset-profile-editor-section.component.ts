﻿import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { FieldEditorModel } from '@app/ui/admin/dataset-profile/admin/field-editor-model';
import { FieldSetEditorModel } from '@app/ui/admin/dataset-profile/admin/field-set-editor-model';
import { SectionEditorModel } from '@app/ui/admin/dataset-profile/admin/section-editor-model';
import { BaseComponent } from '@common/base/base.component';
import { Guid } from '@common/types/guid';
import { takeUntil } from 'rxjs/operators';

@Component({
	selector: 'app-dataset-profile-editor-section-component',
	templateUrl: './dataset-profile-editor-section.component.html',
	styleUrls: ['./dataset-profile-editor-section.component.scss']
})

export class DatasetProfileEditorSectionComponent extends BaseComponent implements OnInit {

	@Input() form: FormGroup;
	//@Input() dataModel: SectionEditorModel;
	@Input() indexPath: string;
	@Input() viewOnly: boolean;
	@Output() fieldsetAdded = new EventEmitter<String>(); //returns the id of the fieldset added

	constructor() { super(); }

	ngOnInit() {

		//TODO 
		// this.form.root.get('pages').valueChanges
		// 	.pipe(takeUntil(this._destroyed))
		// 	.subscribe(x =>
		// 		this.keepPageSelectionValid(x)
		// 	);
	}

	addField() {
		const fieldSet: FieldSetEditorModel = new FieldSetEditorModel();
		fieldSet.ordinal = (this.form.get('fieldSets') as FormArray).length;

		const field: FieldEditorModel = new FieldEditorModel();
		field.id = Guid.create().toString();
		field.ordinal = 0;//first field in fields
		fieldSet.fields.push(field);
	//	if (this.dataModel.fieldSets) {
			fieldSet.id = Guid.create().toString();
			//this.dataModel.fieldSets.push(fieldSet);
		//}
		const fieldsetsArray = this.form.get('fieldSets') as FormArray;
		fieldsetsArray.push(fieldSet.buildForm());

		const fieldSetForm = fieldsetsArray.at(fieldsetsArray.length-1);
		//emit id inserted
		if(fieldSetForm){
			const id: string = fieldSetForm.get('id').value;
			this.fieldsetAdded.emit(id);
		}
	}

	addSectioninSection() {
		const section: SectionEditorModel = new SectionEditorModel();
		//this.dataModel.sections.push(section);
		(<FormArray>this.form.get('sections')).push(section.buildForm());
	}

	DeleteSectionInSection(index) {
		//this.dataModel.sections.splice(index, 1);
		(<FormArray>this.form.get('sections')).removeAt(index);
	}

	deleteFieldSet(index) {
		//this.dataModel.fieldSets.splice(index, 1);
		(<FormArray>this.form.get('fieldSets')).removeAt(index);
	}

	keepPageSelectionValid(pagesJson: Array<any>) {
		const selectedPage = this.form.get('page').value as String;
		// const pages: Array<PageEditorModel> = JsonSerializer.fromJSONArray(pagesJson, Page);
		if (pagesJson.find(elem => elem.id === selectedPage) === undefined) {
			this.form.get('page').reset();
		}
	}

	getFieldTile(formGroup: FormGroup, index: number) {
		if (formGroup.get('title') && formGroup.get('title').value && formGroup.get('title').value.length > 0) { return formGroup.get('title').value; }
		return "Field " + (index + 1);
	}
}
