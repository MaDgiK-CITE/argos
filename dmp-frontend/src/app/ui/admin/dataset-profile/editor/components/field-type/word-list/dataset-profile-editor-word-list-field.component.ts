﻿import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { DatasetProfileComboBoxType } from '../../../../../../../core/common/enum/dataset-profile-combo-box-type';
import { FieldDataOptionEditorModel } from '../../../../admin/field-data/field-data-option-editor-model';
import { WordListFieldDataEditorModel } from '../../../../admin/field-data/word-list-field-data-editor-model';

@Component({
	selector: 'app-dataset-profile-editor-word-list-field-component',
	styleUrls: ['./dataset-profile-editor-word-list-field.component.scss'],
	templateUrl: './dataset-profile-editor-word-list-field.component.html'
})
export class DatasetProfileEditorWordListFieldComponent implements OnInit {

	@Input() form: FormGroup;
	private data: WordListFieldDataEditorModel = new WordListFieldDataEditorModel();

	ngOnInit() {
		this.data.type = DatasetProfileComboBoxType.WordList;
	}

	addNewRow() {
		const wordListOptions: FieldDataOptionEditorModel = new FieldDataOptionEditorModel();
		(<FormArray>this.form.get('data').get('options')).push(wordListOptions.buildForm());
	}
	
	deleteRow(intex: number) {
		if (this.form.get('data').get('options')) { (<FormArray>this.form.get('data').get('options')).removeAt(intex); }
	}
}
