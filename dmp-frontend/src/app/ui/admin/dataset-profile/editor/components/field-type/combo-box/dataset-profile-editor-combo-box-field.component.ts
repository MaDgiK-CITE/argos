﻿import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DatasetProfileComboBoxType } from '@app/core/common/enum/dataset-profile-combo-box-type';
import { EnumUtils } from '@app/core/services/utilities/enum-utils.service';
import { AutoCompleteFieldDataEditorModel } from '@app/ui/admin/dataset-profile/admin/field-data/auto-complete-field-data-editor-model';
import { WordListFieldDataEditorModel } from '@app/ui/admin/dataset-profile/admin/field-data/word-list-field-data-editor-model';
import { BaseComponent } from '@common/base/base.component';
import { takeUntil } from 'rxjs/operators';

@Component({
	selector: 'app-dataset-profile-editor-combo-box-field-component',
	styleUrls: ['./dataset-profile-editor-combo-box-field.component.scss'],
	templateUrl: './dataset-profile-editor-combo-box-field.component.html'
})
export class DatasetProfileEditorComboBoxFieldComponent extends BaseComponent implements OnInit {

	@Input() form: FormGroup;
	options = DatasetProfileComboBoxType;

	constructor(
		public enumUtils: EnumUtils
	) { super(); }

	ngOnInit() {
		// this.setupListeners();
	}

	setupListeners() {
		this.form.get('data').get('type').valueChanges
			.pipe(takeUntil(this._destroyed))
			.subscribe(x => {
				if (this.form.get('data')) { this.form.removeControl('data'); }
				if (x === DatasetProfileComboBoxType.Autocomplete) {
					this.form.addControl('data', new AutoCompleteFieldDataEditorModel().buildForm());
				} else if (x === DatasetProfileComboBoxType.WordList) {
					this.form.addControl('data', new WordListFieldDataEditorModel().buildForm());
				}
				this.setupListeners();
			});
	}
}
