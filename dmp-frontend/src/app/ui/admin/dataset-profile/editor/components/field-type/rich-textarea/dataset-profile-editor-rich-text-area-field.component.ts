import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { RichTextAreaFieldDataEditorModel } from '../../../../admin/field-data/rich-text-area-field-data-editor-model';

@Component({
	selector: 'app-dataset-profile-editor-rich-text-area-field-component',
	styleUrls: ['./dataset-profile-editor-rich-text-area-field.component.scss'],
	templateUrl: './dataset-profile-editor-rich-text-area-field.component.html'
})
export class DatasetProfileEditorRichTextAreaFieldComponent implements OnInit {

	@Input() form: FormGroup;
	private data: RichTextAreaFieldDataEditorModel = new RichTextAreaFieldDataEditorModel();

	ngOnInit() {
		if (!this.form.get('data')) { this.form.addControl('data', this.data.buildForm()); }
	}
}
