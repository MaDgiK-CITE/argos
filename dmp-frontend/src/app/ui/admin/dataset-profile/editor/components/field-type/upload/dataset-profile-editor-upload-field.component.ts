import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { UploadFieldDataEditorModel } from '../../../../admin/field-data/upload-field-data-editor-model';
import {FieldDataOption} from "@app/core/model/dataset-profile-definition/field-data/field-data";
import {FieldDataOptionEditorModel} from "@app/ui/admin/dataset-profile/admin/field-data/field-data-option-editor-model";
import {MatSelect} from "@angular/material/select";
import {ConfigurationService} from "@app/core/services/configuration/configuration.service";

@Component({
	selector: 'app-dataset-profile-editor-upload-field-component',
	styleUrls: ['./dataset-profile-editor-upload-field.component.scss'],
	templateUrl: './dataset-profile-editor-upload-field.component.html'
})
export class DatasetProfileEditorUploadFieldComponent implements OnInit {
	types: Array<FieldDataOption> = [
		// images
		{label: "Animated Portable Network Graphics (APNG)", value: "image/apng", source: ""},
		{label: "AV1 Image File Format (AVIF)", value: "image/avif", source: ""},
		{label: "Graphics Interchange Format (GIF)", value: "image/gif", source: ""},
		{label: "Joint Photographic Expert Group image (JPEG)", value: "image/jpeg", source: ""},
		{label: "Portable Network Graphics (PNG)", value: "image/png", source: ""},
		{label: "Scalable Vector Graphics (SVG)", value: "image/svg+xml", source: ""},
		{label: "Web Picture format (WEBP)", value: "image/webp", source: ""},
		{label: "Tagged Image File Format (TIFF)", value: "image/tiff", source: ""},

		// office word
		{label: "Microsoft Word 97-2003", value: "application/msword", source: ""},	// .doc, .dot
		{label: "Microsoft Word 2007-2013", value: "application/vnd.openxmlformats-officedocument.wordprocessingml.document", source: ""}, // .docx
		{label: "OpenDocument Text", value: "application/vnd.openxmlformats-officedocument.wordprocessingml.document", source: ""}, // .odt

		// office excel
		{label: "Microsoft Excel 97-2003", value: "application/vnd.ms-excel", source: ""},	// .xls, .xlt, .xla
		{label: "Microsoft Excel 2007-2013", value: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", source: ""}, // .xlsx
		{label: "OpenDocument Spreadsheet", value: "application/vnd.oasis.opendocument.spreadsheet", source: ""}, // .ods

		// office powerpoint
		{label: "Microsoft PowerPoint 97-2003", value: "application/vnd.ms-powerpoint", source: ""},	// .ppt, .pot, .pps, .ppa
		{label: "Microsoft PowerPoint 2007-2013", value: "application/vnd.openxmlformats-officedocument.presentationml.presentation", source: ""}, // .pptx
		{label: "OpenDocument Presentation", value: "application/vnd.oasis.opendocument.presentation", source: ""}, // .odp


		{label: "Comma-Seperated Values (CSV)", value: "text/csv", source: ""},
		{label: "Adobe Portable Document Format (PDF)", value: "application/pdf", source: ""}
	];

	selected: string[] = [];
	public typesFormControl = new FormControl();

	@Input() form: FormGroup;
	private data: UploadFieldDataEditorModel = new UploadFieldDataEditorModel();

	constructor(private configurationService: ConfigurationService) {}

	ngOnInit() {
		let typeValues: string[] = this.types.map(type => type.value);
		if (!this.form.get('data')) { this.form.addControl('data', this.data.buildForm()); }
		if(this.form.get('data') && this.form.get('data').get('types')) {
			for(let type of this.form.get('data').get('types').value) {
				if(typeValues.indexOf(type.value) != -1) {
					this.selected.push(type.value);
				}
			}
			this.typesFormControl.setValue(this.selected);
			// if (this.form.get('data').get('types').disabled) {
			// 	this.typesFormControl.disable();
			// }
		}
	}

	selectedType(type: FieldDataOption) {
		if (!this.form.get('data').get('types').disabled) {
			let index = this.selected.indexOf(type.value);
			if (index == -1) {
				this.selected.push(type.value);
				this.addNewRow(type);
			} else {
				this.selected.splice(index, 1);
				this.deleteRow(index);
			}
		}
	}

	isCustomType(value: string) {
		return this.selected.indexOf(value) == -1;
	}

	addNewRow(type: FieldDataOption = null) {
		const typeListOptions: FieldDataOptionEditorModel = new FieldDataOptionEditorModel();
		if(type != null) {
			typeListOptions.fromModel(type);
		}
		(<FormGroup>this.form.get('data')).addControl('types', new FormBuilder().array([]));
		(<FormArray>this.form.get('data').get('types')).push(typeListOptions.buildForm());
	}

	deleteRow(index: number) {
		if (this.form.get('data').get('types')) { (<FormArray>this.form.get('data').get('types')).removeAt(index); }
	}

	public getConfiguration() {
		return this.configurationService;
	}
}
