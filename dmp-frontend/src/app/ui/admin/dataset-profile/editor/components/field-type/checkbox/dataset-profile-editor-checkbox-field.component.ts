import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { CheckBoxFieldDataEditorModel } from '../../../../admin/field-data/check-box-field-data-editor-model';

@Component({
	selector: 'app-dataset-profile-editor-checkbox-field-component',
	styleUrls: ['./dataset-profile-editor-checkbox-field.component.scss'],
	templateUrl: './dataset-profile-editor-checkbox-field.component.html'
})
export class DatasetProfileEditorCheckboxFieldComponent implements OnInit {

	@Input() form: FormGroup;
	private data: CheckBoxFieldDataEditorModel = new CheckBoxFieldDataEditorModel();

	ngOnInit() {
		if (!this.form.get('data')) { this.form.addControl('data', this.data.buildForm()); }
	}
}
