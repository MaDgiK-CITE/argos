import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DatePickerDataEditorModel } from '../../../../admin/field-data/date-picker-data-editor-models';

@Component({
	selector: 'app-dataset-profile-editor-date-picker-field-component',
	styleUrls: ['./dataset-profile-editor-date-picker-field.component.scss'],
	templateUrl: './dataset-profile-editor-date-picker-field.component.html'
})
export class DatasetProfileEditorDatePickerFieldComponent implements OnInit {

	@Input() form: FormGroup;
	private data: DatePickerDataEditorModel = new DatePickerDataEditorModel();

	ngOnInit() {
		if (!this.form.get('data')) { this.form.addControl('data', this.data.buildForm()); }
	}
}
