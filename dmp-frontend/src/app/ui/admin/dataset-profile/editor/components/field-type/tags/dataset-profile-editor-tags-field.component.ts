import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DatePickerDataEditorModel } from '../../../../admin/field-data/date-picker-data-editor-models';
import { ExternalDatasetsDataEditorModel } from '@app/ui/admin/dataset-profile/admin/field-data/external-datasets-data-editor-models';
import { DataRepositoriesDataEditorModel } from '@app/ui/admin/dataset-profile/admin/field-data/data-repositories-data-editor-models';
import { RegistriesDataEditorModel } from '@app/ui/admin/dataset-profile/admin/field-data/registries-data-editor-models';
import { ServicesDataEditorModel } from '@app/ui/admin/dataset-profile/admin/field-data/services-data-editor-models';
import { TagsDataEditorModel } from '@app/ui/admin/dataset-profile/admin/field-data/tags-data-editor-models';
import { Router } from '@angular/router';

@Component({
	selector: 'app-dataset-profile-editor-tags-field-component',
	styleUrls: ['./dataset-profile-editor-tags-field.component.scss'],
	templateUrl: './dataset-profile-editor-tags-field.component.html'
})
export class DatasetProfileEditorTagsFieldComponent implements OnInit {

	@Input() form: FormGroup;
	private data: TagsDataEditorModel = new TagsDataEditorModel();

	constructor(private router: Router) {}

	ngOnInit() {
		if(this.router.url.includes('new')){
			this.form.patchValue({'rdaCommonStandard': 'dataset.keyword'});
		}
		if (!this.form.get('data')) { this.form.addControl('data', this.data.buildForm()); }
	}
}
