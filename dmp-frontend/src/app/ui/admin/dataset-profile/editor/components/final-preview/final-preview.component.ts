﻿import { Component, Input, OnInit} from '@angular/core';
import { Rule } from '@app/core/model/dataset-profile-definition/rule';
import { VisibilityRulesService } from '@app/ui/misc/dataset-description-form/visibility-rules/visibility-rules.service';


@Component({
	selector: 'app-final-preview-component',
	templateUrl: './final-preview.component.html',
	styleUrls: ['./final-preview.component.scss'],
	providers:[VisibilityRulesService]
})

export class FinalPreviewComponent  implements OnInit {

	
	@Input() formGroup = null;
	@Input() visibilityRules:Rule[] = [];
	constructor(private visibilityRulesService: VisibilityRulesService){
		
	}

	

	ngOnInit(): void {
		this.visibilityRulesService.buildVisibilityRules(this.visibilityRules, this.formGroup);
	}
	
}
