import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DatePickerDataEditorModel } from '../../../../admin/field-data/date-picker-data-editor-models';
import { ExternalDatasetsDataEditorModel } from '@app/ui/admin/dataset-profile/admin/field-data/external-datasets-data-editor-models';
import { DataRepositoriesDataEditorModel } from '@app/ui/admin/dataset-profile/admin/field-data/data-repositories-data-editor-models';
import { RegistriesDataEditorModel } from '@app/ui/admin/dataset-profile/admin/field-data/registries-data-editor-models';
import { ServicesDataEditorModel } from '@app/ui/admin/dataset-profile/admin/field-data/services-data-editor-models';

@Component({
	selector: 'app-dataset-profile-editor-services-field-component',
	styleUrls: ['./dataset-profile-editor-services-field.component.scss'],
	templateUrl: './dataset-profile-editor-services-field.component.html'
})
export class DatasetProfileEditorServicesFieldComponent implements OnInit {

	@Input() form: FormGroup;
	private data: ServicesDataEditorModel = new ServicesDataEditorModel();

	ngOnInit() {
		if (!this.form.get('data')) { this.form.addControl('data', this.data.buildForm()); }
	}
}
