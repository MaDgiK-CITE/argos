import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { DatasetIdentifierDataEditorModel } from '@app/ui/admin/dataset-profile/admin/field-data/dataset-identifier-data-editor-models';

@Component({
	selector: 'app-dataset-profile-editor-dataset-identifier-field-component',
	styleUrls: ['./dataset-profile-editor-dataset-identifier-field.component.scss'],
	templateUrl: './dataset-profile-editor-dataset-identifier-field.component.html'
})
export class DatasetProfileEditorDatasetIdentifierFieldComponent implements OnInit {

	@Input() form: FormGroup;
	private data: DatasetIdentifierDataEditorModel = new DatasetIdentifierDataEditorModel();

	constructor(private router: Router) {}

	ngOnInit() {
		if(this.router.url.includes('new')){
			this.form.patchValue({'rdaCommonStandard': 'dataset.dataset_id'});
		}
		if (!this.form.get('data')) { this.form.addControl('data', this.data.buildForm()); }
	}
}
