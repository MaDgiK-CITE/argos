import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormControl } from '@angular/forms';
import { DatasetProfileComboBoxType } from '../../../../../../core/common/enum/dataset-profile-combo-box-type';
import { DatasetProfileFieldViewStyle } from '../../../../../../core/common/enum/dataset-profile-field-view-style';
import { DatasetProfileInternalDmpEntitiesType } from '../../../../../../core/common/enum/dataset-profile-internal-dmp-entities-type';

@Component({
	selector: 'app-component-profile-editor-default-value-component',
	templateUrl: './component-profile-editor-default-value.component.html',
	styleUrls: ['./component-profile-editor-default-value.component.scss']
})
export class DatasetProfileEditorDefaultValueComponent implements OnInit {

	@Input() viewStyle: DatasetProfileFieldViewStyle;
	@Input() form: FormControl;
	@Input() formArrayOptions: FormArray;
	@Input() comboBoxType: DatasetProfileComboBoxType;
	@Input() internalDmpEntitiesType: DatasetProfileInternalDmpEntitiesType;
	@Input() placeHolder: String;
	// @Input() required: Boolean;

	comboBoxTypeEnum = DatasetProfileComboBoxType;
	internalDmpEntitiesTypeEnum = DatasetProfileInternalDmpEntitiesType;
	viewStyleEnum = DatasetProfileFieldViewStyle;

	constructor() { }

	ngOnInit() {
	}

}
