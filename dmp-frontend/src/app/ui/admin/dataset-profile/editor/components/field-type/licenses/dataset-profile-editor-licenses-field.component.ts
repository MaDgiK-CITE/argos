import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {LicensesDataEditorModel} from "@app/ui/admin/dataset-profile/admin/field-data/licenses-data-editor-models";

@Component({
	selector: 'app-dataset-profile-editor-licenses-field-component',
	styleUrls: ['./dataset-profile-editor-licenses-field.component.scss'],
	templateUrl: './dataset-profile-editor-licenses-field.component.html'
})
export class DatasetProfileEditorLicensesFieldComponent implements OnInit {

	@Input() form: FormGroup;
	private data: LicensesDataEditorModel = new LicensesDataEditorModel();

	ngOnInit() {
		if (!this.form.get('data')) { this.form.addControl('data', this.data.buildForm()); }
	}
}
