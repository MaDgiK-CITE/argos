﻿import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormArray, AbstractControl } from '@angular/forms';
import { DatasetProfileComboBoxType } from '../../../../../../../core/common/enum/dataset-profile-combo-box-type';
import { AutoCompleteFieldDataEditorModel } from '../../../../admin/field-data/auto-complete-field-data-editor-model';
import { AutoCompleteSingleDataEditorModel } from '@app/ui/admin/dataset-profile/admin/field-data/auto-complete-single-data';
import { HtmlMethod } from '@app/core/model/dataset-profile-definition/html-method.enum';
import { AuthType } from '@app/core/model/dataset-profile-definition/auth-type.enum';

@Component({
	selector: 'app-dataset-profile-editor-auto-complete-field-component',
	styleUrls: ['./dataset-profile-editor-auto-complete-field.component.scss'],
	templateUrl: './dataset-profile-editor-auto-complete-field.component.html'
})
export class DatasetProfileEditorAutoCompleteFieldComponent implements OnInit {

	public htmlMethods = HtmlMethod;
	public authTypes = AuthType;

	@Input() form: FormGroup;
	private data: AutoCompleteFieldDataEditorModel = new AutoCompleteFieldDataEditorModel();
	multiForm: FormArray;

	ngOnInit() {
		this.multiForm = (<FormArray>this.form.get('data').get('autoCompleteSingleDataList'));
		this.data.type = DatasetProfileComboBoxType.Autocomplete;
	}

	addSource() {
		(<FormArray>this.multiForm).push(new AutoCompleteSingleDataEditorModel().buildForm());
	}

	removeSource(index: number) {
		(<FormArray>this.multiForm).removeAt(index);
	}
}
