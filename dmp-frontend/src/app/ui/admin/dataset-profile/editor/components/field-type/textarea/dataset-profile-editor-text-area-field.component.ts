import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { TextAreaFieldDataEditorModel } from '../../../../admin/field-data/text-area-field-data-editor-model';

@Component({
	selector: 'app-dataset-profile-editor-text-area-field-component',
	styleUrls: ['./dataset-profile-editor-text-area-field.component.scss'],
	templateUrl: './dataset-profile-editor-text-area-field.component.html'
})
export class DatasetProfileEditorTextAreaFieldComponent implements OnInit {

	@Input() form: FormGroup;
	private data: TextAreaFieldDataEditorModel = new TextAreaFieldDataEditorModel();

	ngOnInit() {
		if (!this.form.get('data')) { this.form.addControl('data', this.data.buildForm()); }
	}
}
