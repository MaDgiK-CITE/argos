﻿import { Component, Input } from '@angular/core';
import { FormArray } from '@angular/forms';

@Component({
	selector: 'app-dataset-profile-editor-page-component',
	templateUrl: './dataset-profile-editor-page.component.html',
	styleUrls: ['./dataset-profile-editor-page.component.scss']
})
export class DatasetProfileEditorPageComponent {

	@Input() form: FormArray;
	@Input() viewOnly: boolean;

	removePage(index) {
		(<FormArray>this.form).removeAt(index);
	}
}
