import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {PublicationsDataEditorModel} from "@app/ui/admin/dataset-profile/admin/field-data/publications-data-editor-models";

@Component({
	selector: 'app-dataset-profile-editor-publications-field-component',
	styleUrls: ['./dataset-profile-editor-publications-field.component.scss'],
	templateUrl: './dataset-profile-editor-publications-field.component.html'
})
export class DatasetProfileEditorPublicationsFieldComponent implements OnInit {

	@Input() form: FormGroup;
	private data: PublicationsDataEditorModel = new PublicationsDataEditorModel();

	ngOnInit() {
		if (!this.form.get('data')) { this.form.addControl('data', this.data.buildForm()); }
	}
}
