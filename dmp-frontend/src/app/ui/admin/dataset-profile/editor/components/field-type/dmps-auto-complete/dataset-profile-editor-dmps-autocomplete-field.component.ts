import { OnInit, Input, Component } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { DatasetProfileInternalDmpEntitiesType } from "../../../../../../../core/common/enum/dataset-profile-internal-dmp-entities-type";
import { DatasetsAutoCompleteFieldDataEditorModel } from "../../../../admin/field-data/datasets-autocomplete-field-data-editor-mode";
import { DmpsAutoCompleteFieldDataEditorModel } from "../../../../admin/field-data/dmps-autocomplete-field-data-editor-model";

@Component({
	selector: 'app-dataset-profile-editor-dmps-autocomplete-field-component',
	styleUrls: ['./dataset-profile-editor-dmps-autocomplete-field.component.scss'],
	templateUrl: './dataset-profile-editor-dmps-autocomplete-field.component.html'
})
export class DatasetProfileEditorDmpsAutoCompleteFieldComponent implements OnInit {

	@Input() form: FormGroup;
	private data: DmpsAutoCompleteFieldDataEditorModel = new DatasetsAutoCompleteFieldDataEditorModel();

	ngOnInit() {
		this.data.type = DatasetProfileInternalDmpEntitiesType.Dmps;
	}
}
