import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DataRepositoriesDataEditorModel } from '@app/ui/admin/dataset-profile/admin/field-data/data-repositories-data-editor-models';

@Component({
	selector: 'app-dataset-profile-editor-currency-field-component',
	styleUrls: ['./dataset-profile-editor-currency-field.component.scss'],
	templateUrl: './dataset-profile-editor-currency-field.component.html'
})
export class DatasetProfileEditorCurrencyFieldComponent implements OnInit {

	@Input() form: FormGroup;
	private data: DataRepositoriesDataEditorModel = new DataRepositoriesDataEditorModel();

	ngOnInit() {
		if (!this.form.get('data')) { this.form.addControl('data', this.data.buildForm()); }
	}
}
