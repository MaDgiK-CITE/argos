﻿import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { UserInfoListingModel } from '@app/core/model/user/user-info-listing';
import { DatasetProfile } from '../../../../core/model/admin/dataset-profile/dataset-profile';
import { BaseFormModel } from '../../../../core/model/base-form-model';
import { PageEditorModel } from '../admin/page-editor-model';
import { SectionEditorModel } from '../admin/section-editor-model';
import { EditorCustomValidators } from './custom-validators/editor-custom-validators';


export class DatasetProfileEditorModel extends BaseFormModel {

	public sections: Array<SectionEditorModel> = new Array<SectionEditorModel>();
	public pages: Array<PageEditorModel> = new Array<PageEditorModel>();
	public label: string;
	public status: number;
	public version: number;
	private description: string;
	private type: string;
	public enablePrefilling: boolean;
	private language: string;
	private users: UserInfoListingModel[] = [];

	fromModel(item: DatasetProfile): DatasetProfileEditorModel {
		if (item.sections) { this.sections = item.sections.map(x => new SectionEditorModel().fromModel(x)); }
		if (item.pages) { this.pages = item.pages.map(x => new PageEditorModel().fromModel(x)); }
		this.label = item.label;
		this.type = item.type;
		this.enablePrefilling = item.enablePrefilling;
		this.status = item.status;
		this.version = item.version;
		this.description = item.description;
		this.language = item.language;
		this.users = item.users;
		return this;
	}

	buildForm(disabled: boolean = false, skipDisable: Array<String> = []): FormGroup {
		const formGroup: FormGroup = new FormBuilder().group({
			label: [{ value: this.label, disabled: (disabled && !skipDisable.includes('DatasetProfileEditorModel.label')) }, [Validators.required]],
			description: [{ value: this.description, disabled: (disabled && !skipDisable.includes('DatasetProfileEditorModel.description')) }, [Validators.required]],
			type: [{ value: this.type, disabled: (disabled && !skipDisable.includes('DatasetProfileEditorModel.type')) }, [Validators.required]],
			enablePrefilling: [{ value: this.enablePrefilling, disabled: (disabled && !skipDisable.includes('DatasetProfileEditorModel.enablePrefilling')) }, []],
			language: [{ value: this.language, disabled: (disabled && !skipDisable.includes('DatasetProfileEditorModel.language')) }, [Validators.required]],
			status: [{ value: this.status, disabled: (disabled && !skipDisable.includes('DatasetProfileEditorModel.status')) }],
			version: [{ value: this.version, disabled: (disabled && !skipDisable.includes('DatasetProfileEditorModel.version')) }],
			users: [{ value: this.users, disabled: (disabled && !skipDisable.includes('DatasetProfileEditorModel.users')) }]
		});

		const sectionsFormArray = new Array<FormGroup>();
		this.sections.forEach(item => {
			const form: FormGroup = item.buildForm(disabled, skipDisable);
			sectionsFormArray.push(form);
		});
		formGroup.addControl('sections', this.formBuilder.array(sectionsFormArray));

		const pagesFormArray = new Array<FormGroup>();
		this.pages.forEach(item => {
			const form: FormGroup = item.buildForm(disabled, skipDisable);
			pagesFormArray.push(form);
		});
		formGroup.addControl('pages', this.formBuilder.array(pagesFormArray));

		formGroup.setValidators([EditorCustomValidators.atLeastOneElementListValidator('pages'), EditorCustomValidators.pagesHaveAtLeastOneSection('pages','sections')]);
		formGroup.updateValueAndValidity();

		return formGroup;
	}
}
