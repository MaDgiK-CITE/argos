import { Pipe, PipeTransform } from '@angular/core';
import { DatasetStatus } from '@app/core/common/enum/dataset-status';

@Pipe({
	name: 'parseStatus',
	pure: true
})
export class ParseStatus implements PipeTransform {
	transform(value: any, ...args: any[]) {
		return this.parseStatus(value);
	}


	parseStatus(status: DatasetStatus): string {

		switch (status) {
			case DatasetStatus.Finalized:
				return 'DATASET-PROFILE-STATUS.FINALIZED';
			case DatasetStatus.Draft: 
				return	'DATASET-PROFILE-STATUS.DRAFT';
			case DatasetStatus.Deleted:
				return	'DATASET-PROFILE-STATUS.DRAFT.DELETED';
			default:
				return 'DATASET-PROFILE-STATUS.DRAFT.NONE';
		}
	}
}
