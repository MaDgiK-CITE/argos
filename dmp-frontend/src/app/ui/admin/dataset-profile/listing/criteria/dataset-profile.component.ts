import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DatasetProfileCriteria } from '@app/core/query/dataset-profile/dataset-profile-criteria';
import { DatasetProfileService } from '@app/core/services/dataset-profile/dataset-profile.service';
import { DialogConfirmationUploadDatasetProfiles } from '@app/ui/admin/dataset-profile/listing/criteria/dialog-confirmation-upload-profile/dialog-confirmation-upload-profiles.component';
import { BaseCriteriaComponent } from '@app/ui/misc/criteria/base-criteria.component';
import { ValidationErrorModel } from '@common/forms/validation/error-model/validation-error-model';
import { TranslateService } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/operators';

@Component({
	selector: 'app-dataset-profile-criteria-component',
	templateUrl: './dataset-profile.component.html',
	styleUrls: ['./dataset-profile.component.scss']
})
export class DatasetProfileCriteriaComponent extends BaseCriteriaComponent implements OnInit {

	public criteria: DatasetProfileCriteria = new DatasetProfileCriteria();

	constructor(
		private datasetService: DatasetProfileService,
		private dialog: MatDialog,
		private language: TranslateService,
	) {
		super(new ValidationErrorModel());
	}

	ngOnInit() {
		super.ngOnInit();
		if (this.criteria == null) { this.criteria = new DatasetProfileCriteria(); }
	}

	setCriteria(criteria: DatasetProfileCriteria): void {
		this.criteria = criteria;
	}

	onCallbackError(error: any) {
		this.setErrorModel(error.error);
	}

	controlModified(): void {
		this.clearErrorModel();
		if (this.refreshCallback != null &&
			(this.criteria.like == null || this.criteria.like.length === 0 || this.criteria.like.length > 2)
		) {
			this.refreshCallback();
		}
	}

}
