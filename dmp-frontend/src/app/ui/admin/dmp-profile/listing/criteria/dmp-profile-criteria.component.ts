import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DmpBlueprintCriteria } from '@app/core/query/dmp/dmp-blueprint-criteria';
import { DmpProfileService } from '@app/core/services/dmp/dmp-profile.service';
import { BaseCriteriaComponent } from '@app/ui/misc/criteria/base-criteria.component';
import { ValidationErrorModel } from '@common/forms/validation/error-model/validation-error-model';
import { TranslateService } from '@ngx-translate/core';

@Component({
	selector: 'app-dmp-profile-criteria-component',
	templateUrl: './dmp-profile-criteria.component.html',
	styleUrls: ['./dmp-profile-criteria.component.scss'],
})
export class DmpProfileCriteriaComponent extends BaseCriteriaComponent implements OnInit {

	public criteria: DmpBlueprintCriteria = new DmpBlueprintCriteria();

	constructor(
		private dmpProfileService: DmpProfileService,
		private dialog: MatDialog,
		private language: TranslateService
	) {
		super(new ValidationErrorModel());
	}

	ngOnInit() {
		super.ngOnInit();
		if (this.criteria == null) { this.criteria = new DmpBlueprintCriteria(); }
	}

	setCriteria(criteria: DmpBlueprintCriteria): void {
		this.criteria = criteria;
	}

	onCallbackError(error: any) {
		this.setErrorModel(error.error);
	}

	controlModified(): void {
		this.clearErrorModel();
		if (this.refreshCallback != null &&
			(this.criteria.like == null || this.criteria.like.length === 0 || this.criteria.like.length > 2)
		) {
			this.refreshCallback();
		}
	}


	// openDialog(): void {
	// 	const dialogRef = this.dialog.open(DialodConfirmationUploadDmpProfiles, {
	// 		restoreFocus: false,
	// 		data: {
	// 			message: this.language.instant('DMP-PROFILE-LISTING.UPLOAD.UPLOAD-XML-FILE-TITLE'),
	// 			confirmButton: this.language.instant('DMP-PROFILE-LISTING.UPLOAD.UPLOAD-XML'),
	// 			cancelButton: this.language.instant('DMP-PROFILE-LISTING.UPLOAD.UPLOAD-XML-FILE-CANCEL'),
	// 			name: '',
	// 			file: FileList,
	// 			sucsess: false
	// 		}
	// 	});
	// 	dialogRef.afterClosed().pipe(takeUntil(this._destroyed)).subscribe(data => {
	// 		if (data && data.sucsess && data.name != null && data.file != null) {
	// 			this.dmpProfileService.uploadFile(data.file, data.name)
	// 				.pipe(takeUntil(this._destroyed))
	// 				.subscribe();
	// 		}
	// 	});
	// }
}
