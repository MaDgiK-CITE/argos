import { NgModule } from "@angular/core";
import { ConfirmationDialogModule } from '@common/modules/confirmation-dialog/confirmation-dialog.module';
import { UrlListingModule } from '@app/library/url-listing/url-listing.module';
import { CommonFormsModule } from '@common/forms/common-forms.module';
import { CommonUiModule } from '@common/ui/common-ui.module';
import { DmpProfileRoutingModule } from './dmp-profile.routing';
import { DmpProfileEditorComponent } from './editor/dmp-profile-editor.component';
import { DmpProfileExternalAutocompleteFieldEditorComponent } from './editor/external-autocomplete/dmp-profile-external-autocomplete-field-editor.component';
import { DialodConfirmationUploadDmpProfiles } from './listing/criteria/dialog-confirmation-upload-profile/dialog-confirmation-upload-profiles.component';
import { DmpProfileCriteriaComponent } from './listing/criteria/dmp-profile-criteria.component';
import { DmpProfileListingComponent } from './listing/dmp-profile-listing.component';
import { NgxDropzoneModule } from "ngx-dropzone";
import { DragDropModule } from '@angular/cdk/drag-drop';
import { AutoCompleteModule } from "@app/library/auto-complete/auto-complete.module";

@NgModule({
	imports: [
		CommonUiModule,
		CommonFormsModule,
		UrlListingModule,
		ConfirmationDialogModule,
		DmpProfileRoutingModule,
		NgxDropzoneModule,
		DragDropModule,
		AutoCompleteModule
	],
	declarations: [
		DmpProfileEditorComponent,
		DmpProfileListingComponent,
		DmpProfileCriteriaComponent,
		DialodConfirmationUploadDmpProfiles,
		DmpProfileExternalAutocompleteFieldEditorComponent
	],
	entryComponents: [
		DialodConfirmationUploadDmpProfiles
	]
})
export class DmpProfileModule { }
