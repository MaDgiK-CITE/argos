import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../../core/auth-guard.service';
import { DmpProfileEditorComponent } from './editor/dmp-profile-editor.component';
import { DmpProfileListingComponent } from './listing/dmp-profile-listing.component';
import { AdminAuthGuard } from '@app/core/admin-auth-guard.service';

const routes: Routes = [
	{ path: '', component: DmpProfileListingComponent, canActivate: [AdminAuthGuard] },
	{ path: 'new', component: DmpProfileEditorComponent, canActivate: [AdminAuthGuard], data: { title: 'GENERAL.TITLES.DMP-BLUEPRINT-NEW' } },
	{ path: 'clone/:cloneid', component: DmpProfileEditorComponent, canActivate: [AdminAuthGuard], data: { title: 'GENERAL.TITLES.DMP-BLUEPRINT-CLONE' } },
	{ path: ':id', component: DmpProfileEditorComponent, canActivate: [AdminAuthGuard], data: { title: 'GENERAL.TITLES.DMP-BLUEPRINT-EDIT' } },
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class DmpProfileRoutingModule { }
