import { FormGroup, FormBuilder } from "@angular/forms";
import { DmpProfileExternalAutoCompleteField } from "../../../../../core/model/dmp-profile/dmp-profile-external-autocomplete";

export class DmpProfileExternalAutoCompleteFieldDataEditorModel {

	public url: string;
	public optionsRoot: string;
	public multiAutoComplete: boolean = false;
	public label: string;
	public value: string;

	buildForm(disabled: boolean = false, skipDisable: Array<String> = []): FormGroup {
		const formGroup = new FormBuilder().group({
			url: [{ value: this.url, disabled: (disabled && !skipDisable.includes('DmpProfileExternalAutoCompleteFieldDataEditorModel.url')) }],
			optionsRoot: [{ value: this.optionsRoot, disabled: (disabled && !skipDisable.includes('DmpProfileExternalAutoCompleteFieldDataEditorModel.optionsRoot')) }],
			multiAutoComplete: [{ value: this.multiAutoComplete, disabled: (disabled && !skipDisable.includes('DmpProfileExternalAutoCompleteFieldDataEditorModel.multiAutoComplete')) }],
			label: [{ value: this.label, disabled: (disabled && !skipDisable.includes('DmpProfileExternalAutoCompleteFieldDataEditorModel.label')) }],
			value: [{ value: this.value, disabled: (disabled && !skipDisable.includes('DmpProfileExternalAutoCompleteFieldDataEditorModel.value')) }],

		});
		return formGroup;
	}

	fromModel(item: DmpProfileExternalAutoCompleteField): DmpProfileExternalAutoCompleteFieldDataEditorModel {
		this.url = item.url;
		this.optionsRoot = item.optionsRoot;
		this.multiAutoComplete = item.multiAutoComplete;
		this.label = item.label;
		this.value = item.value;
		return this;
	}
}
