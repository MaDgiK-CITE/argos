import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DmpProfileExternalAutoCompleteFieldDataEditorModel } from './dmp-profile-external-autocomplete-field-editor.model';

@Component({
	selector: 'app-dmp-profile-external-autocomplete-field-editor-component',
	styleUrls: ['./dmp-profile-external-autocomplete-field-editor.component.scss'],
	templateUrl: './dmp-profile-external-autocomplete-field-editor.component.html'
})
export class DmpProfileExternalAutocompleteFieldEditorComponent implements OnInit {

	@Input() form: FormGroup
	private externalAutocomplete: DmpProfileExternalAutoCompleteFieldDataEditorModel;

	ngOnInit() {
	}
}
