import { FormBuilder, FormGroup } from '@angular/forms';
import { DmpProfileFieldDataType } from '@app/core/common/enum/dmp-profile-field-type';
import { DmpProfileType } from '@app/core/common/enum/dmp-profile-type';
import { DmpProfile, DmpProfileDefinition } from '@app/core/model/dmp-profile/dmp-profile';
import { DmpProfileField } from '@app/core/model/dmp-profile/dmp-profile-field';
import { DmpProfileExternalAutoCompleteFieldDataEditorModel } from '@app/ui/admin/dmp-profile/editor/external-autocomplete/dmp-profile-external-autocomplete-field-editor.model';
import { ValidationErrorModel } from '@common/forms/validation/error-model/validation-error-model';

export class DmpProfileEditorModel {

	public id: string;
	public label: string;
	public definition: DmpProfileDefinitionEditorModel = new DmpProfileDefinitionEditorModel();
	public status: number;
	public created: Date;
	public modified: Date;
	public validationErrorModel: ValidationErrorModel = new ValidationErrorModel();

	fromModel(item: DmpProfile): DmpProfileEditorModel {
		this.id = item.id;
		this.label = item.label;
		this.definition = new DmpProfileDefinitionEditorModel().fromModel(item.definition);
		this.status = item.status;
		this.created = item.created;
		this.modified = item.modified;
		return this;
	}

	buildForm(): FormGroup {
		const formGroup = new FormBuilder().group({
			id: [this.id],
			label: [this.label],
			status: [this.status],
			created: [this.created],
			modified: [this.modified]
		});
		formGroup.addControl('definition', this.definition.buildForm());
		return formGroup;
	}
}

export class DmpProfileDefinitionEditorModel {

	public fields: DmpProfileFieldEditorModel[] = new Array<DmpProfileFieldEditorModel>();

	fromModel(item: DmpProfileDefinition): DmpProfileDefinitionEditorModel {
		if (item.fields) { item.fields.map(x => this.fields.push(new DmpProfileFieldEditorModel().fromModel(x))); }
		return this;
	}

	buildForm(): FormGroup {
		const formBuilder = new FormBuilder();
		const formGroup = formBuilder.group({});
		const fieldsFormArray = new Array<FormGroup>();
		this.fields.forEach(item => {
			const form: FormGroup = item.buildForm();
			fieldsFormArray.push(form);
		});
		formGroup.addControl('fields', formBuilder.array(fieldsFormArray));
		return formGroup;
	}
}

export class DmpProfileFieldEditorModel {
	public id: string;
	public type: DmpProfileType;
	public dataType: DmpProfileFieldDataType;
	public required = false;
	public label: string;
	public value: any;
	public externalAutocomplete?: DmpProfileExternalAutoCompleteFieldDataEditorModel;

	fromModel(item: DmpProfileField): DmpProfileFieldEditorModel {
		this.type = item.type;
		this.dataType = item.dataType;
		this.required = item.required;
		this.label = item.label;
		this.id = item.id;
		this.value = item.value;
		if (item.externalAutocomplete)
			this.externalAutocomplete = new DmpProfileExternalAutoCompleteFieldDataEditorModel().fromModel(item.externalAutocomplete);
		return this;
	}

	buildForm(): FormGroup {
		const formGroup = new FormBuilder().group({
			type: [this.type],
			id: [this.id],
			dataType: [this.dataType],
			required: [this.required],
			label: [this.label]
		});
		if (this.externalAutocomplete) {
			formGroup.addControl('externalAutocomplete', this.externalAutocomplete.buildForm());
		}
		return formGroup;
	}
}
