import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { DescriptionTemplatesInSection, DmpBlueprint, DmpBlueprintDefinition, FieldCategory, FieldInSection, SectionDmpBlueprint } from "@app/core/model/dmp/dmp-blueprint/dmp-blueprint";
import { BackendErrorValidator } from "@common/forms/validation/custom-validator";
import { ValidationErrorModel } from "@common/forms/validation/error-model/validation-error-model";
import { ValidationContext } from "@common/forms/validation/validation-context";

export class DmpBlueprintEditor {
	public id: string;
	public label: string;
	public definition: DmpBlueprintDefinitionEditor = new DmpBlueprintDefinitionEditor();
	public status: number;
	public created: Date;
	public modified: Date;
	public validationErrorModel: ValidationErrorModel = new ValidationErrorModel();

	fromModel(item: DmpBlueprint): DmpBlueprintEditor {
		this.id = item.id;
		this.label = item.label;
		this.definition = new DmpBlueprintDefinitionEditor().fromModel(item.definition);
		this.status = item.status;
		this.created = item.created;
		this.modified = item.modified;
		return this;
	}

	buildForm(context: ValidationContext = null, disabled: boolean = false): FormGroup {
		if (context == null) { context = this.createValidationContext(); }
		const formGroup = new FormBuilder().group({
			id: [{ value: this.id, disabled: disabled }, context.getValidation('id')],
			label: [{ value: this.label, disabled: disabled }, context.getValidation('label')],
			status: [{ value: this.status, disabled: disabled }, context.getValidation('status')],
			created: [{ value: this.created, disabled: disabled }, context.getValidation('created')],
			modified: [{ value: this.modified, disabled: disabled }, context.getValidation('modified')],
		});
		formGroup.addControl('definition', this.definition.buildForm());
		return formGroup;
	}

	createValidationContext(): ValidationContext {
		const baseContext: ValidationContext = new ValidationContext();
		baseContext.validation.push({ key: 'id', validators: [BackendErrorValidator(this.validationErrorModel, 'id')] });
		baseContext.validation.push({ key: 'label', validators: [Validators.required, BackendErrorValidator(this.validationErrorModel, 'label')] });
		baseContext.validation.push({ key: 'status', validators: [Validators.required, BackendErrorValidator(this.validationErrorModel, 'status')] });
		baseContext.validation.push({ key: 'definition', validators: [Validators.required, BackendErrorValidator(this.validationErrorModel, 'definition')] });
		baseContext.validation.push({ key: 'created', validators: [] });
		baseContext.validation.push({ key: 'modified', validators: [] });
		return baseContext;
	}

}

export class DmpBlueprintDefinitionEditor {

	public sections: SectionDmpBlueprintEditor[] = new Array<SectionDmpBlueprintEditor>();

	fromModel(item: DmpBlueprintDefinition): DmpBlueprintDefinitionEditor {
		if (item.sections) { item.sections.map(x => this.sections.push(new SectionDmpBlueprintEditor().fromModel(x))); }
		return this;
	}

	buildForm(): FormGroup {
		const formBuilder = new FormBuilder();
		const formGroup = formBuilder.group({});
		const sectionsFormArray = new Array<FormGroup>();
		this.sections.sort((a, b) => a.ordinal - b.ordinal).forEach(item => {
			const form: FormGroup = item.buildForm();
			sectionsFormArray.push(form);
		});
		formGroup.addControl('sections', formBuilder.array(sectionsFormArray));
		return formGroup;
	}
}

export class SectionDmpBlueprintEditor {
	public id: string;
	public label: string;
	public description: string;
	public ordinal: number;
	public fields: FieldInSectionEditor[] = new Array<FieldInSectionEditor>();
	public hasTemplates: boolean;
	public descriptionTemplates: DescriptionTemplatesInSectionEditor[] = new Array<DescriptionTemplatesInSectionEditor>();
	public validationErrorModel: ValidationErrorModel = new ValidationErrorModel();

	fromModel(item: SectionDmpBlueprint): SectionDmpBlueprintEditor {
		this.id = item.id;
		this.label = item.label;
		this.description = item.description;
		this.ordinal = item.ordinal;
		if (item.fields) { item.fields.map(x => this.fields.push(new FieldInSectionEditor().fromModel(x))); }
		this.hasTemplates = item.hasTemplates;
		if (item.descriptionTemplates) { item.descriptionTemplates.map(x => this.descriptionTemplates.push(new DescriptionTemplatesInSectionEditor().fromModel(x))); }
		return this;
	}

	buildForm(context: ValidationContext = null, disabled: boolean = false): FormGroup {
		if (context == null) { context = this.createValidationContext(); }
		const formGroup = new FormBuilder().group({
			id: [{ value: this.id, disabled: disabled }, context.getValidation('id')],
			label: [{ value: this.label, disabled: disabled }, context.getValidation('label')],
			description: [{ value: this.description, disabled: disabled }, context.getValidation('description')],
			ordinal: [{ value: this.ordinal, disabled: disabled }, context.getValidation('ordinal')],
			hasTemplates: [{ value: this.hasTemplates, disabled: disabled }, context.getValidation('hasTemplates')]
		});
		const formBuilder = new FormBuilder();
		const fieldsFormArray = new Array<FormGroup>();
		this.fields.sort((a, b) => a.ordinal - b.ordinal).forEach(item => {
			const form: FormGroup = item.buildForm();
			fieldsFormArray.push(form);
		});
		formGroup.addControl('fields', formBuilder.array(fieldsFormArray));
		const descriptionTemplatesFormArray = new Array<FormGroup>();
		this.descriptionTemplates.forEach(item => {
			const form: FormGroup = item.buildForm();
			descriptionTemplatesFormArray.push(form);
		});
		formGroup.addControl('descriptionTemplates', formBuilder.array(descriptionTemplatesFormArray));
		return formGroup;
	}

	createValidationContext(): ValidationContext {
		const baseContext: ValidationContext = new ValidationContext();
		baseContext.validation.push({ key: 'id', validators: [BackendErrorValidator(this.validationErrorModel, 'id')] });
		baseContext.validation.push({ key: 'label', validators: [Validators.required, BackendErrorValidator(this.validationErrorModel, 'label')] });
		baseContext.validation.push({ key: 'description', validators: [BackendErrorValidator(this.validationErrorModel, 'description')] });
		baseContext.validation.push({ key: 'ordinal', validators: [Validators.required, BackendErrorValidator(this.validationErrorModel, 'ordinal')] });
		baseContext.validation.push({ key: 'hasTemplates', validators: [BackendErrorValidator(this.validationErrorModel, 'hasTemplates')] });
		baseContext.validation.push({ key: 'descriptionTemplates', validators: [BackendErrorValidator(this.validationErrorModel, 'descriptionTemplates')] });
		return baseContext;
	}
}

export class FieldInSectionEditor {
	public id: string;
	public category: FieldCategory;
	public type: number;
	public label: string;
	public placeholder: string;
	public description: string;
	public required: boolean;
	public ordinal: number;
	public validationErrorModel: ValidationErrorModel = new ValidationErrorModel();

	fromModel(item: FieldInSection): FieldInSectionEditor {
		this.id = item.id;
		this.category = item.category;
		this.type = item.type;
		this.label = item.label;
		this.placeholder = item.placeholder;
		this.description = item.description;
		this.required = item.required;
		this.ordinal = item.ordinal;
		return this;
	}

	buildForm(context: ValidationContext = null, disabled: boolean = false): FormGroup {
		if (context == null) { context = this.createValidationContext(); }
		const formGroup = new FormBuilder().group({
			id: [{ value: this.id, disabled: disabled }, context.getValidation('id')],
			category: [{ value: this.category, disabled: disabled }, context.getValidation('category')],
			type: [{ value: this.type, disabled: disabled }, context.getValidation('type')],
			label: [{ value: this.label, disabled: disabled }, context.getValidation('label')],
			placeholder: [{ value: this.placeholder, disabled: disabled }, context.getValidation('placeholder')],
			description: [{ value: this.description, disabled: disabled }, context.getValidation('description')],
			required: [{ value: this.required, disabled: disabled }, context.getValidation('required')],
			ordinal: [{ value: this.ordinal, disabled: disabled }, context.getValidation('ordinal')]
		});
		return formGroup;
	}

	createValidationContext(): ValidationContext {
		const baseContext: ValidationContext = new ValidationContext();
		baseContext.validation.push({ key: 'id', validators: [BackendErrorValidator(this.validationErrorModel, 'id')] });
		baseContext.validation.push({ key: 'category', validators: [Validators.required, BackendErrorValidator(this.validationErrorModel, 'category')] });
		baseContext.validation.push({ key: 'type', validators: [BackendErrorValidator(this.validationErrorModel, 'type')] });
		baseContext.validation.push({ key: 'label', validators: [Validators.required, BackendErrorValidator(this.validationErrorModel, 'label')] });
		baseContext.validation.push({ key: 'placeholder', validators: [BackendErrorValidator(this.validationErrorModel, 'placeholder')] });
		baseContext.validation.push({ key: 'description', validators: [BackendErrorValidator(this.validationErrorModel, 'description')] });
		baseContext.validation.push({ key: 'required', validators: [BackendErrorValidator(this.validationErrorModel, 'required')] });
		baseContext.validation.push({ key: 'ordinal', validators: [Validators.required, BackendErrorValidator(this.validationErrorModel, 'ordinal')] });
		return baseContext;
	}
}

export class DescriptionTemplatesInSectionEditor {
	public id: string;
	public descriptionTemplateId: string;
	public label: string;
	public minMultiplicity: number;
	public maxMultiplicity: number;
	public validationErrorModel: ValidationErrorModel = new ValidationErrorModel();

	fromModel(item: DescriptionTemplatesInSection): DescriptionTemplatesInSectionEditor {
		this.id = item.id;
		this.descriptionTemplateId = item.descriptionTemplateId;
		this.label = item.label;
		this.minMultiplicity = item.minMultiplicity;
		this.maxMultiplicity = item.maxMultiplicity;
		return this;
	}

	buildForm(context: ValidationContext = null, disabled: boolean = false): FormGroup {
		if (context == null) { context = this.createValidationContext(); }
		const formGroup = new FormBuilder().group({
			id: [{ value: this.id, disabled: disabled }, context.getValidation('id')],
			descriptionTemplateId: [{ value: this.descriptionTemplateId, disabled: disabled }, context.getValidation('descriptionTemplateId')],
			label: [{ value: this.label, disabled: disabled }, context.getValidation('label')],
			minMultiplicity: [{ value: this.minMultiplicity, disabled: disabled }, context.getValidation('minMultiplicity')],
			maxMultiplicity: [{ value: this.maxMultiplicity, disabled: disabled }, context.getValidation('maxMultiplicity')]
		});
		return formGroup;
	}

	createValidationContext(): ValidationContext {
		const baseContext: ValidationContext = new ValidationContext();
		baseContext.validation.push({ key: 'id', validators: [BackendErrorValidator(this.validationErrorModel, 'id')] });
		baseContext.validation.push({ key: 'descriptionTemplateId', validators: [Validators.required, BackendErrorValidator(this.validationErrorModel, 'descriptionTemplateId')] });
		baseContext.validation.push({ key: 'label', validators: [Validators.required, BackendErrorValidator(this.validationErrorModel, 'label')] });
		baseContext.validation.push({ key: 'minMultiplicity', validators: [BackendErrorValidator(this.validationErrorModel, 'minMultiplicity')] });
		baseContext.validation.push({ key: 'maxMultiplicity', validators: [BackendErrorValidator(this.validationErrorModel, 'maxMultiplicity')] });
		return baseContext;
	}
}

// export class ExtraFieldsInSectionEditor {
// 	public id: string;
//     public label: string;
//     public description: string;
// 	public placeholder: string;
// 	public type: ExtraFieldType;
//     public required: boolean;
//     public ordinal: number;

// 	fromModel(item: ExtraFieldInSection): ExtraFieldsInSectionEditor {
// 		this.id = item.id;
// 		this.label = item.label;
// 		this.description = item.description;
//         this.placeholder = item.placeholder;
// 		this.type = item.type;
//         this.required = item.required;
//         this.ordinal = item.ordinal;
// 		return this;
// 	}

// 	buildForm(): FormGroup {
// 		const formGroup = new FormBuilder().group({
// 			id: [this.id],
//             label: [this.label],
// 			description: [this.description],
//             placeholder: [this.placeholder],
// 			type: [this.type],
//             required: [this.required],
// 			ordinal: [this.ordinal]
// 		});
// 		return formGroup;
// 	}
// }
