import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AdminAuthGuard } from "@app/core/admin-auth-guard.service";
import { DescriptionTypeEditorComponent } from './editor/description-type-editor.component';
import { DescriptionTypesComponent } from "./listing/description-types.component";

const routes: Routes = [
    { path: '', component: DescriptionTypesComponent, canActivate: [AdminAuthGuard] },
    { path: 'new', component: DescriptionTypeEditorComponent, canActivate: [AdminAuthGuard], data: { title: 'GENERAL.TITLES.DESCRIPTION-TYPE-NEW' } },
    { path: ':id', component: DescriptionTypeEditorComponent, canActivate: [AdminAuthGuard], data: { title: 'GENERAL.TITLES.DESCRIPTION-TYPE-EDIT' } }
]

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class DescriptionTypesRoutingModule { }