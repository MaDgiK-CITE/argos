import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DescriptionTypesRoutingModule } from './description-types.routing';
import { DescriptionTypesComponent } from './listing/description-types.component';
import { CommonUiModule } from '@common/ui/common-ui.module';
import { DescriptionTypeEditorComponent } from './editor/description-type-editor.component';
import { CommonFormsModule } from '@common/forms/common-forms.module';

@NgModule({
  declarations: [
    DescriptionTypesComponent,
    DescriptionTypeEditorComponent
  ],
  imports: [
    CommonModule,
    CommonUiModule,
    CommonFormsModule,
    DescriptionTypesRoutingModule
  ]
})
export class DescriptionTypesModule { }
