import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { DescriptionTemplateTypeStatus } from '@app/core/common/enum/description-template-type-status';
import { DescriptionTemplateType } from '@app/core/model/description-template-type/description-template-type';
import { DescriptionTemplateTypeService } from '@app/core/services/description-template-type/description-template-type.service';
import { SnackBarNotificationLevel, UiNotificationService } from '@app/core/services/notification/ui-notification-service';
import { BaseComponent } from '@common/base/base.component';
import { FormService } from '@common/forms/form-service';
import { BackendErrorValidator } from '@common/forms/validation/custom-validator';
import { ValidationErrorModel } from '@common/forms/validation/error-model/validation-error-model';
import { ValidationContext } from '@common/forms/validation/validation-context';
import { TranslateService } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-description-type-editor',
  templateUrl: './description-type-editor.component.html',
  styleUrls: ['./description-type-editor.component.scss']
})
export class DescriptionTypeEditorComponent extends BaseComponent implements OnInit {

	formGroup: FormGroup = null;
	descriptionTypeModel: DescriptionTypeEditorModel;

	isNew = true;
	viewOnly = false;
	descriptionTemplateTypeId: string;

	constructor(
		private descriptionTemplateTypeService: DescriptionTemplateTypeService,
		private formService: FormService,
		private uiNotificationService: UiNotificationService,
		private language: TranslateService,
		private route: ActivatedRoute,
		private router: Router
	) { 
		super();
	}

	ngOnInit(): void {
		this.route.paramMap.pipe(takeUntil(this._destroyed)).subscribe((paramMap: ParamMap) => {
			this.descriptionTemplateTypeId = paramMap.get('id');

			if (this.descriptionTemplateTypeId != null) {
				this.isNew = false;
				this.descriptionTemplateTypeService.getSingle(this.descriptionTemplateTypeId)
					.pipe(takeUntil(this._destroyed)).subscribe(
						type => {
							this.descriptionTypeModel = new DescriptionTypeEditorModel().fromModel(type);
							if(this.descriptionTypeModel.status === DescriptionTemplateTypeStatus.Finalized){
								this.formGroup = this.descriptionTypeModel.buildForm(null, true);
								this.viewOnly = true;
							}
							else{
								this.formGroup = this.descriptionTypeModel.buildForm();
							}
						},
						error => this.uiNotificationService.snackBarNotification(error.message, SnackBarNotificationLevel.Error)
					);
			}
			else {
				this.descriptionTypeModel = new DescriptionTypeEditorModel();
				this.descriptionTypeModel.status = DescriptionTemplateTypeStatus.Draft;
				this.formGroup = this.descriptionTypeModel.buildForm();
			}
		});
	}

  	formSubmit(): void {
		this.formService.touchAllFormFields(this.formGroup);
		if (!this.isFormValid()) { return; }
		this.onSubmit();
	}

	public isFormValid() {
		return this.formGroup.valid;
	}

	finalize() {
		this.formGroup.get('status').setValue(DescriptionTemplateTypeStatus.Finalized);
		this.onSubmit();
	}

  	onSubmit(): void {
		if(this.isNew){
			this.descriptionTemplateTypeService.createType(this.formGroup.value)
				.pipe(takeUntil(this._destroyed))
				.subscribe(
					complete => this.onCallbackSuccess(true),
					error => this.onCallbackError(error)
				);
		}
		else{
			this.descriptionTemplateTypeService.updateType(this.formGroup.value)
				.pipe(takeUntil(this._destroyed))
				.subscribe(
					complete => this.onCallbackSuccess(false),
					error => this.onCallbackError(error)
				);
		}
	}

	onCallbackSuccess(creation: boolean): void {
		if(creation){
			this.uiNotificationService.snackBarNotification(this.language.instant('GENERAL.SNACK-BAR.SUCCESSFUL-CREATION'), SnackBarNotificationLevel.Success);
		}
		else{
			this.uiNotificationService.snackBarNotification(this.language.instant('GENERAL.SNACK-BAR.SUCCESSFUL-UPDATE'), SnackBarNotificationLevel.Success);
		}
		this.router.navigate(['/description-types']);
	}

	onCallbackError(errorResponse: any) {
		this.setErrorModel(errorResponse.error);
		this.formService.validateAllFormFields(this.formGroup);
		this.uiNotificationService.snackBarNotification(errorResponse.error.message, SnackBarNotificationLevel.Error);
	}

	public setErrorModel(validationErrorModel: ValidationErrorModel) {
		Object.keys(validationErrorModel).forEach(item => {
			(<any>this.descriptionTypeModel.validationErrorModel)[item] = (<any>validationErrorModel)[item];
		});
	}

  	public cancel(): void {
		this.router.navigate(['/description-types']);
	}

}

export class DescriptionTypeEditorModel {
  	public id: string;
  	public name: string;
	public status: DescriptionTemplateTypeStatus;
	public validationErrorModel: ValidationErrorModel = new ValidationErrorModel();

	fromModel(item: DescriptionTemplateType): DescriptionTypeEditorModel {
		this.id = item.id;
		this.name = item.name;
		this.status = item.status;
		return this;
	}

	buildForm(context: ValidationContext = null, disabled: boolean = false): FormGroup {
		if (context == null) { context = this.createValidationContext(); }
		const formGroup = new FormBuilder().group({
			id: [this.id],
			name: [{ value: this.name, disabled: disabled }, context.getValidation('name').validators],
			status: [{ value: this.status, disabled: disabled }, context.getValidation('status').validators]
		});
		return formGroup;
	}
		
	createValidationContext(): ValidationContext {
		const baseContext: ValidationContext = new ValidationContext();
		baseContext.validation.push({ key: 'name', validators: [Validators.required, BackendErrorValidator(this.validationErrorModel, 'name')] });
		baseContext.validation.push({ key: 'status', validators: [Validators.required, BackendErrorValidator(this.validationErrorModel, 'status')] });
		return baseContext;
	}
}
