import { DataSource } from '@angular/cdk/table';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';
import { DescriptionTemplateType } from '@app/core/model/description-template-type/description-template-type';
import { DescriptionTemplateTypeService } from '@app/core/services/description-template-type/description-template-type.service';
import { SnackBarNotificationLevel, UiNotificationService } from '@app/core/services/notification/ui-notification-service';
import { BaseComponent } from '@common/base/base.component';
import { ConfirmationDialogComponent } from '@common/modules/confirmation-dialog/confirmation-dialog.component';
import { TranslateService } from '@ngx-translate/core';
import { merge as observableMerge, Observable, of } from 'rxjs';
import { map, startWith, switchMap, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-description-types',
  templateUrl: './description-types.component.html',
  styleUrls: ['./description-types.component.scss']
})
export class DescriptionTypesComponent extends BaseComponent implements OnInit {

  	@ViewChild(MatPaginator, { static: true }) _paginator: MatPaginator;
	@ViewChild(MatSort, { static: true }) sort: MatSort;

	dataSource: DescriptionTypesDataSource | null;
	displayedColumns: String[] = ['label', 'status', 'delete'];

	statuses = [
		{ value: '0', viewValue: 'DESCRIPTION-TYPES-LISTING.STATUS.DRAFT' },
		{ value: '1', viewValue: 'DESCRIPTION-TYPES-LISTING.STATUS.FINALIZED' }
	];

	constructor(
		private descriptionTemplateTypeService: DescriptionTemplateTypeService,
		private dialog: MatDialog,
		private language: TranslateService,
		private uiNotificationService: UiNotificationService,
		private router: Router
	) { 
		super();
	}

	ngOnInit(): void {
		this.refresh();
	}

  	refresh() {
		this.dataSource = new DescriptionTypesDataSource(this.descriptionTemplateTypeService, this._paginator, this.sort/*, this.criteria*/);
	}

	rowClick(rowId: String) {
		this.router.navigate(['description-types/' + rowId]);
	}

	parseStatus(value: number): string{
		const stringVal = value.toString()
		try{
			return this.statuses.find(status => status.value === stringVal).viewValue;
		}catch{
			return stringVal;
		}
	}

	deleteTemplate(id: string){
		if(id){
			const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
				restoreFocus: false,
				data: {
					message: this.language.instant('GENERAL.CONFIRMATION-DIALOG.DELETE-ITEM'),
					confirmButton: this.language.instant('GENERAL.CONFIRMATION-DIALOG.ACTIONS.CONFIRM'),
					cancelButton: this.language.instant('GENERAL.CONFIRMATION-DIALOG.ACTIONS.CANCEL'),
					isDeleteConfirmation: true
				}
			});

			dialogRef.afterClosed().pipe(takeUntil(this._destroyed)).subscribe(result => {
				if (result) {
					this.descriptionTemplateTypeService.deleteType(id)
					.pipe(takeUntil(this._destroyed))
					.subscribe(
						complete => {
							this.uiNotificationService.snackBarNotification(this.language.instant('GENERAL.SNACK-BAR.SUCCESSFUL-DATASET-PROFILE-DELETE'), SnackBarNotificationLevel.Success);
							this.refresh();
						},
						error => {
							this.onCallbackError(error);
							if (error.error.statusCode == 674) {
								this.uiNotificationService.snackBarNotification(this.language.instant('GENERAL.SNACK-BAR.UNSUCCESSFUL-DATASET-PROFILE-DELETE'), SnackBarNotificationLevel.Error);
							} else {
								this.uiNotificationService.snackBarNotification(this.language.instant(error.message), SnackBarNotificationLevel.Error);
							}
						}
					);
				}
			});

		}
	}

	onCallbackError(errorResponse: HttpErrorResponse) {
		this.uiNotificationService.snackBarNotification(errorResponse.message, SnackBarNotificationLevel.Warning);
	}

	getStatusClass(status: number): string {
		
		if(status == 1){
			return 'status-chip-finalized'
		}

		return 'status-chip-draft';
	}

}

export class DescriptionTypesDataSource extends DataSource<DescriptionTemplateType> {

	totalCount = 0;

	constructor(
		private _service: DescriptionTemplateTypeService,
		private _paginator: MatPaginator,
		private _sort: MatSort
	) {
		super();

	}

	connect(): Observable<DescriptionTemplateType[]> {
		const displayDataChanges = [
			this._paginator.page
			//this._sort.matSortChange
		];

		return observableMerge(...displayDataChanges).pipe(
			startWith(null),
			switchMap(() => {
				return this._service.getTypes();
			}),
			map(result => {
				return result;
			}),
			map(result => {
				if (!result) { return []; }
				if (this._paginator.pageIndex === 0) { this.totalCount = result.totalCount; }
				return result.data;
			}));
	}

	disconnect() {
		// No-op
	}
}
