import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexManagmentComponent } from './index-managment.component';
import { AdminAuthGuard } from '@app/core/admin-auth-guard.service';


const routes: Routes = [
	{ path: '', component: IndexManagmentComponent, canActivate: [AdminAuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IndexManagmentRoutingModule { }
