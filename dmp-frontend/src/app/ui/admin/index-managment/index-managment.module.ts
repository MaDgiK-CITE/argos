import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IndexManagmentRoutingModule } from './index-managment.routing';
import { IndexManagmentComponent } from './index-managment.component';
import { CommonUiModule } from '@common/ui/common-ui.module';
import { CommonFormsModule } from '@common/forms/common-forms.module';
import { ConfirmationDialogModule } from '@common/modules/confirmation-dialog/confirmation-dialog.module';


@NgModule({
  declarations: [IndexManagmentComponent],
  imports: [
    CommonUiModule,
	CommonFormsModule,
	ConfirmationDialogModule,
    IndexManagmentRoutingModule
  ]
})
export class IndexManagmentModule { }
