import { Component, OnInit } from '@angular/core';
import { DatasetService } from '@app/core/services/dataset/dataset.service';
import { BaseComponent } from '@common/base/base.component';
import { takeUntil } from 'rxjs/operators';
import { UiNotificationService, SnackBarNotificationLevel } from '@app/core/services/notification/ui-notification-service';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { DmpService } from '@app/core/services/dmp/dmp.service';

@Component({
  selector: 'app-index-managment',
  templateUrl: './index-managment.component.html',
  styleUrls: ['./index-managment.component.scss']
})
export class IndexManagmentComponent extends BaseComponent implements OnInit {

  constructor(
	  private datasetService: DatasetService,
	  private uiNotificationService: UiNotificationService,
	  private translate: TranslateService,
	  private router: Router,
	  private dmpService: DmpService
	)
   {
	   super();
    }

  ngOnInit() {
  }

  generateIndex(ev: Event) {
	  (ev.srcElement as HTMLButtonElement).disabled = true;
	//   this.datasetService.generateIndex().pipe(takeUntil(this._destroyed)).subscribe(
	// 	  response => {
	// 		(ev.srcElement as HTMLButtonElement).disabled = false;
	// 		  this.onCallbackSuccess();
	// 	  },
	// 	  error => {
	// 		(ev.srcElement as HTMLButtonElement).disabled = false;
	// 		  this.onCallbackError(error);
	// 	  }
	//   );
	  this.dmpService.generateIndex().pipe(takeUntil(this._destroyed)).subscribe(
		response => {
		  (ev.srcElement as HTMLButtonElement).disabled = false;
			this.onCallbackSuccess();
		},
		error => {
		  (ev.srcElement as HTMLButtonElement).disabled = false;
			this.onCallbackError(error);
		}
	);
  }

  clearIndex(ev: Event) {
	(ev.srcElement as HTMLButtonElement).disabled = true;
	// this.datasetService.clearIndex().pipe(takeUntil(this._destroyed)).subscribe(
	// 	response => {
	// 	  (ev.srcElement as HTMLButtonElement).disabled = false;
	// 		this.onCallbackSuccess();
	// 	},
	// 	error => {
	// 	  (ev.srcElement as HTMLButtonElement).disabled = false;
	// 		this.onCallbackError(error);
	// 	}
	// );
	this.dmpService.clearIndex().pipe(takeUntil(this._destroyed)).subscribe(
		response => {
		  (ev.srcElement as HTMLButtonElement).disabled = false;
			this.onCallbackSuccess();
		},
		error => {
		  (ev.srcElement as HTMLButtonElement).disabled = false;
			this.onCallbackError(error);
		}
	);
}

  onCallbackSuccess(): void {
	this.uiNotificationService.snackBarNotification( this.translate.instant('GENERAL.SNACK-BAR.SUCCESSFUL-UPDATE'), SnackBarNotificationLevel.Success);
	this.router.navigate(['/reload']).then(() => this.router.navigate(['/index-managment']));
}

onCallbackError(error: any) {
	this.uiNotificationService.snackBarNotification( error, SnackBarNotificationLevel.Error);
	//this.validateAllFormFields(this.formGroup);
}

}
