import { OverlayModule } from '@angular/cdk/overlay';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatMomentDateModule, MAT_MOMENT_DATE_FORMATS } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatFormFieldDefaultOptions, MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { BrowserModule, Title } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from '@app/app-routing.module';
import { AppComponent } from '@app/app.component';
import { CoreServiceModule } from '@app/core/core-service.module';
import { NotificationModule } from '@app/library/notification/notification.module';
import { LoginModule } from '@app/ui/auth/login/login.module';
import { DatasetCreateWizardModule } from '@app/ui/dataset-create-wizard/dataset-create-wizard.module';
import { BreadcrumbModule } from '@app/ui/misc/breadcrumb/breadcrumb.module';
import { HelpContentModule } from '@app/ui/misc/help-content/help-content.module';
import { NavigationModule } from '@app/ui/misc/navigation/navigation.module';
import { ReloadHelperComponent } from '@app/ui/misc/reload-helper/reload-helper.component';
import { NavbarModule } from '@app/ui/navbar/navbar.module';
import { SidebarModule } from '@app/ui/sidebar/sidebar.module';
import { MomentUtcDateAdapter } from '@common/date/moment-utc-date-adapter';
import { CommonHttpModule } from '@common/http/common-http.module';
import { CommonUiModule } from '@common/ui/common-ui.module';
import { TranslateCompiler, TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { DragulaModule } from 'ng2-dragula';
import { CookieService } from 'ngx-cookie-service';
import { NgcCookieConsentConfig, NgcCookieConsentModule } from 'ngx-cookieconsent';
import { MatomoModule } from 'ngx-matomo';
import { ConfigurationService } from './core/services/configuration/configuration.service';
import { CultureService } from './core/services/culture/culture-service';
import { TranslateServerLoader } from './core/services/language/server.loader';
import { MatomoService } from './core/services/matomo/matomo-service';
import { GuidedTourModule } from './library/guided-tour/guided-tour.module';
import { OpenDMPCustomTranslationCompiler } from './utilities/translate/opendmp-custom-translation-compiler';
import { Oauth2DialogModule } from './ui/misc/oauth2-dialog/oauth2-dialog.module';

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient, appConfig: ConfigurationService) {
	return new TranslateServerLoader(http, appConfig);
	//GK: In case we want the original translation provider uncomment the line below.
	//return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}

const cookieConfig: NgcCookieConsentConfig = {
	enabled: true,
	cookie: {
		domain: ""//environment.App // or 'your.domain.com' // it is mandatory to set a domain, for cookies to work properly (see https://goo.gl/S2Hy2A)
	},
	palette: {
		popup: {
			background: "#000000",
			text: "#ffffff",
			link: "#ffffff"
		},
		button: {
			background: "#00b29f",
			text: "#ffffff",
			border: "transparent"
		}
	},
	content: {
		message: "This website uses cookies to enhance the user experience.",
		dismiss: "Got it!",
		deny: "Refuse cookies",
		link: "Learn more",
		href: "",//environment.App + "terms-of-service",
		policy: "Cookies Policy"
	},
	position: "bottom-right",
	theme: 'edgeless',
	type: 'info'
};

const appearance: MatFormFieldDefaultOptions = {
	appearance: 'outline'
	// appearance: 'standard'
};

@NgModule({
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		CoreServiceModule.forRoot(),
		AppRoutingModule,
		CommonUiModule,
		TranslateModule.forRoot({
			compiler: { provide: TranslateCompiler, useClass: OpenDMPCustomTranslationCompiler },
			loader: {
				provide: TranslateLoader,
				useFactory: HttpLoaderFactory,
				deps: [HttpClient, ConfigurationService]
			}
		}),
		HttpClientModule,
		OverlayModule,
		CommonHttpModule,
		MatMomentDateModule,
		LoginModule,
		MatomoModule,
		//Ui
		NotificationModule,
		NavigationModule,
		BreadcrumbModule,
		HelpContentModule,
		ReactiveFormsModule,
		FormsModule,
		DatasetCreateWizardModule,
		NavbarModule,
		SidebarModule,
		NgcCookieConsentModule.forRoot(cookieConfig),
		Oauth2DialogModule,
		GuidedTourModule.forRoot(),
		DragulaModule.forRoot()
	],
	declarations: [
		AppComponent,
		ReloadHelperComponent
	],
	providers: [
		{
			provide: MAT_DATE_LOCALE,
			deps: [CultureService],
			useFactory: (cultureService) => cultureService.getCurrentCulture().name
		},
		{ provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
		{ provide: DateAdapter, useClass: MomentUtcDateAdapter },
		{
			provide: LOCALE_ID,
			deps: [CultureService],
			useFactory: (cultureService) => cultureService.getCurrentCulture().name
		},
		{
			provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
			useValue: appearance
		},
		Title,
		CookieService,
		MatomoService
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
