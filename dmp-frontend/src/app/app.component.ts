
import { of as observableOf, Subscription } from 'rxjs';

import { switchMap, filter, map, takeUntil } from 'rxjs/operators';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../environments/environment';
import { AuthService } from './core/services/auth/auth.service';
import { CultureService } from './core/services/culture/culture-service';
import { BreadCrumbResolverService } from './ui/misc/breadcrumb/service/breadcrumb.service';
import { Title } from '@angular/platform-browser';
import { NgcCookieConsentService, NgcStatusChangeEvent } from "ngx-cookieconsent";
import { CookieService } from "ngx-cookie-service";
import { LanguageService } from './core/services/language/language.service';
import { ConfigurationService } from './core/services/configuration/configuration.service';

import { Location } from '@angular/common';
import { MatomoInjector } from 'ngx-matomo';
import { MatomoService } from './core/services/matomo/matomo-service';
import { SideNavService } from './core/services/sidenav/side-nav.sevice';
import { MatSidenav } from '@angular/material/sidenav';
import { runInThisContext } from 'vm';
import * as moment from 'moment';


declare const gapi: any;
declare var $: any;

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {

	hasBreadCrumb = observableOf(false);
	// sideNavOpen = false;
	private sideNavSubscription: Subscription;
	helpContentEnabled: boolean;
	private statusChangeSubscription: Subscription;
	onlySplash = true;

	@ViewChild('sidenav') sidenav: MatSidenav;

	constructor(
		private router: Router,
		private route: ActivatedRoute,
		private authentication: AuthService,
		private translate: TranslateService,
		private breadCrumbResolverService: BreadCrumbResolverService,
		private titleService: Title,
		private cultureService: CultureService,
		private cookieService: CookieService,
		private ccService: NgcCookieConsentService,
		private language: LanguageService,
		public configurationService: ConfigurationService,
		private location: Location,
		private matomoService: MatomoService,
		private sidenavService: SideNavService
	) {
		this.initializeServices();
		this.matomoService.init();
		this.helpContentEnabled = configurationService.helpService.enabled;
	}
	ngAfterViewInit(): void {
		setTimeout(() => {
			this.sideNavSubscription = this.sidenavService.status().subscribe(isopen => {
				const hamburger = document.getElementById('hamburger');
				if (isopen) {
					//update value of hamburfer
					if (!hamburger) {//try later
						setTimeout(() => {
							const hamburger = document.getElementById('hamburger');
							if (hamburger) {
								hamburger.classList.add('change');
							}
						}, 300);
					} else {
						hamburger.classList.add('change');
					}
					this.sidenav.open()
				} else {//closed
					if (!hamburger) {//try later
						setTimeout(() => {
							const hamburger = document.getElementById('hamburger');
							if (hamburger) {
								hamburger.classList.remove('change');
							}
						}, 300);
					} else {
						hamburger.classList.remove('change');
					}
					this.sidenav.close();

				}
			});
		});
	}

	onActivate(event: any) {
		this.breadCrumbResolverService.push(event);
	}

	onDeactivate(event: any) {
		//this.breadCrumbResolverService.clear()
	}

	ngOnInit() {
		if (this.location.path() === '') {
			if (!this.configurationService.useSplash) {
				this.onlySplash = false;
				this.router.navigate(['/reload']).then(() => this.router.navigate(['/home']));
			} else {
				this.onlySplash = true;
				this.router.navigate(['/reload']).then(() => this.router.navigate(['/splash']));
			}
		} else if (this.location.path() === '/splash') {
			this.onlySplash = true;
		} else {
			this.onlySplash = false;
		}
		if (!this.cookieService.check("cookiesConsent")) {
			// this.cookieService.set("cookiesConsent", "false", 356);
			this.cookieService.set("cookiesConsent", "false", 356, null, null, false, 'Lax');

		}

		this.hasBreadCrumb = this.router.events.pipe(
			filter(event => event instanceof NavigationEnd),
			map(() => this.route),
			map(route => route.firstChild),
			switchMap(route => route.data),
			map(data => data['breadcrumb']));

		const appTitle = this.titleService.getTitle();
		this.router
			.events.pipe(
				filter(event => event instanceof NavigationEnd),
				map(() => {
					let child = this.route.firstChild;
					if (child != null) {
						while (child.firstChild) {
							child = child.firstChild;
						}
						if (child.snapshot.data['title']) {
							return child.snapshot.data['title'];
						}
					}
					return appTitle;
				})
			).subscribe((ttl: string) => {
				this.translateTitle(ttl);
				this.translate.onLangChange.subscribe(() => this.translateTitle(ttl));
			});

		this.statusChangeSubscription = this.ccService.statusChange$.subscribe((event: NgcStatusChangeEvent) => {
			if (event.status == "dismiss") {
				// this.cookieService.set("cookiesConsent", "true", 365);
				this.cookieService.set("cookiesConsent", "true", 356, null, null, false, 'Lax');
			}
		});

		this.ccService.getConfig().content.href = this.configurationService.app + "cookies-policy";
		this.ccService.getConfig().cookie.domain = this.configurationService.app;
		this.translate
			.get(['COOKIE.MESSAGE', 'COOKIE.DISMISS', 'COOKIE.DENY', 'COOKIE.LINK', 'COOKIE.POLICY'])
			.subscribe(data => {
				this.ccService.getConfig().content = this.ccService.getConfig().content || {};
				// Override default messages with the translated ones
				this.ccService.getConfig().content.message = data['COOKIE.MESSAGE'];
				this.ccService.getConfig().content.dismiss = data['COOKIE.DISMISS'];
				this.ccService.getConfig().content.deny = data['COOKIE.DENY'];
				this.ccService.getConfig().content.link = data['COOKIE.LINK'];
				this.ccService.getConfig().content.policy = data['COOKIE.POLICY'];

				if (this.cookieService.get("cookiesConsent") == "true") {
					this.ccService.getConfig().enabled = false;
				}
				this.ccService.destroy();
				this.ccService.init(this.ccService.getConfig());
			});
	}

	translateTitle(ttl: string) {
		if (ttl.length > 0) {
			this.translate.get(ttl).subscribe((translated: string) => {
				this.translate.get('GENERAL.TITLES.PREFIX').subscribe((titlePrefix: string) => {
					this.titleService.setTitle(titlePrefix + translated);
				});
			});
		} else {
			this.translate.get('GENERAL.TITLES.GENERAL').subscribe((translated: string) => {
				this.titleService.setTitle(translated);
			});
		}
	}

	ngOnDestroy() {
		this.statusChangeSubscription.unsubscribe();
		if (this.sideNavSubscription) {
			this.sideNavSubscription.unsubscribe();
		}
	}

	login() {
		//redirect to login page
		this.router.navigate(['/login'], { queryParams: { /*refresh : Math.random() ,returnUrl: this.state.url*/ } });
	}

	logout() {

	}

	public isAuthenticated(): boolean {
		return !(!this.authentication.current());
	}

	goToDMPs() {
		this.router.navigate(['/plans'], { queryParams: { /*refresh : Math.random() ,returnUrl: this.state.url*/ } });
	}

	// ----------- UNCOMMENT TO ADD AGAIN GRANTS --------
	// goToGrants() {
	// 	this.router.navigate(['/grants'], { queryParams: { /*refresh : Math.random() ,returnUrl: this.state.url*/ } });
	// }

	initializeServices() {
		this.translate.setDefaultLang(this.configurationService.defaultLanguage || 'en');
		this.authentication.current() && this.authentication.current().culture ? this.cultureService.cultureSelected(this.authentication.current().culture) : this.cultureService.cultureSelected(this.configurationService.defaultCulture);
		this.authentication.current() && this.authentication.current().language ? this.language.changeLanguage(this.authentication.current().language) : (this.configurationService.defaultLanguage || 'en');
	}

	toggleNavbar(event) {
		document.getElementById('hamburger').classList.toggle("change");
	}
}

