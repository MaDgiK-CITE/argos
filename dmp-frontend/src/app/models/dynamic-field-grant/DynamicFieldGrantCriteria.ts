export class DynamicFieldGrantCriteria {
	public id: string;
	public dynamicFields: DynamicFieldGrantCriteriaDependencies[];
}

export class DynamicFieldGrantCriteriaDependencies {
	public value;
	public property;
}
