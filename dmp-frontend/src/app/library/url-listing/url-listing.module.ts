import { NgModule } from '@angular/core';
import { FormattingModule } from '@app/core/formatting.module';
import { UrlListingComponent } from '@app/library/url-listing/url-listing.component';
import { CommonUiModule } from '@common/ui/common-ui.module';

@NgModule({
	imports: [
		CommonUiModule,
		FormattingModule
	],
	declarations: [
		UrlListingComponent
	],
	exports: [
		UrlListingComponent
	]
})
export class UrlListingModule {
	constructor() { }
}
