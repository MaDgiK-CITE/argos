import { Component, Input } from "@angular/core";
import { Router } from "@angular/router";
import { UrlListingItem } from "./url-listing-item";

@Component({
	selector: 'app-url-listing',
	templateUrl: './url-listing.component.html',
	styleUrls: ['./url-listing.component.scss']
})
export class UrlListingComponent {

	@Input() items: UrlListingItem[];
	@Input() parameters: any
	@Input() urlLimit: number = 3;

	constructor(
		private router: Router
	) { }

	ngOnInit() {

	}

	navigate(link: string) {
		this.router.navigate([link], { queryParams: this.parameters });
	}
}
