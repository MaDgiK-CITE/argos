import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';


@Component({
	selector: 'app-export-method-dialog',
	templateUrl: './export-method-dialog.component.html',
	styleUrls: ['./export-method-dialog.component.scss']
})
export class ExportMethodDialogComponent implements OnInit {

	constructor(
		public dialogRef: MatDialogRef<ExportMethodDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any
	) { }

	ngOnInit() {
	}

	close() {
		this.dialogRef.close(false);
	}

	downloadXML() {
		this.dialogRef.close("xml");
	}

	downloadDocument() {
		this.dialogRef.close("doc");
	}

	downloadPdf() {
		this.dialogRef.close("pdf");
	}

	downloadJson() {
		this.dialogRef.close("json");
	}

	isFinalized() {
		return this.data.isFinalized;
	}
}
