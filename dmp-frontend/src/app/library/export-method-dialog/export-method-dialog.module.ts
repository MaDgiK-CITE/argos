import { NgModule } from '@angular/core';
import { ExportMethodDialogComponent } from '@app/library/export-method-dialog/export-method-dialog.component';
import { CommonUiModule } from '@common/ui/common-ui.module';

@NgModule({
	imports: [CommonUiModule],
	declarations: [ExportMethodDialogComponent],
	exports: [ExportMethodDialogComponent],
	entryComponents: [ExportMethodDialogComponent]
})
export class ExportMethodDialogModule {
	constructor() { }
}
