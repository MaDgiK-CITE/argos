import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MultipleChoiceDialogComponent } from './multiple-choice-dialog.component';
import { CommonUiModule } from '@common/ui/common-ui.module';
import { FormsModule } from '@angular/forms';



@NgModule({
	imports: [CommonUiModule, FormsModule],
	declarations: [MultipleChoiceDialogComponent],
	exports: [MultipleChoiceDialogComponent],
	entryComponents: [MultipleChoiceDialogComponent]
})
export class MultipleChoiceDialogModule { }
