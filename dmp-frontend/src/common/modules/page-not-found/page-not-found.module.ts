import { NgModule } from '@angular/core';
import { PageNotFoundComponent } from '@common/modules/page-not-found/page-not-found.component';

@NgModule({
	imports: [
	],
	declarations: [
		PageNotFoundComponent
	],
	entryComponents: []
})
export class PageNotFoundModule { }
