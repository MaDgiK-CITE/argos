import { Component, Inject, AfterViewInit, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ConfigurableProvidersService } from '@app/ui/auth/login/utilities/configurableProviders.service';
import { LoginService } from '@app/ui/auth/login/utilities/login.service';
import { AuthService } from '@app/core/services/auth/auth.service';
import { ConfigurationService } from '@app/core/services/configuration/configuration.service';
import { BaseComponent } from '@common/base/base.component';
import { AuthProvider } from '@app/core/common/enum/auth-provider';
import { ConfigurableProvider } from '@app/core/model/configurable-provider/configurableProvider';
import { takeUntil } from 'rxjs/operators';

/// <reference types="gapi" />
/// <reference types="facebook-js-sdk" />

declare const gapi: any;
declare const FB: any;

@Component({
	selector: 'app-sign-in-dialog',
	templateUrl: './sign-in-dialog.component.html',
	styleUrls: ['./sign-in-dialog.component.scss']
})
export class SignInDialogComponent extends BaseComponent implements OnInit, AfterViewInit {

	agreePrivacyPolicyNames = false;
	public auth2: any;
	private returnUrl: string;
	//public cofigurableProviders: ConfigurableProvider[];

	constructor(
		public dialogRef: MatDialogRef<SignInDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any,

		private router: Router,
		private route: ActivatedRoute,
		private loginService: LoginService,
		private authService: AuthService,
		public configurableProviderService: ConfigurableProvidersService,
		private configurationService: ConfigurationService
	) {
		super();
	}

	close() {
		this.dialogRef.close(false);
	}

	// cancel() {
	// 	this.dialogRef.close(false);
	// }

	// confirm() {
	// 	this.dialogRef.close(true);
	// }

	ngOnInit(): void {
		this.route.queryParams
			.pipe(takeUntil(this._destroyed))
			.subscribe((params: Params) => {
				const returnUrl = params['returnUrl'];
				if (returnUrl) { this.returnUrl = returnUrl; }
			});
		this.authService.getConfigurableProviders()
			.pipe(takeUntil(this._destroyed))
			.subscribe((data: any) => {
				this.configurableProviderService.providers = data;
			})
	}

	ngAfterViewInit() {
		this.initProviders();
	}

	public linkedInLogin() {
		this.router.navigate(['/login/linkedin']);
	}

	public twitterLogin() {
		this.router.navigate(['/login/twitter']);
	}

	public b2AccessLogin() {
		this.router.navigate(['/login/external/b2access']);
	}

	public orcidLogin() {
		this.router.navigate(['/login/external/orcid']);
	}

	public openaireLogin() {
		this.router.navigate(['/login/openaire']);
	}

	public configurableLogin(provider: ConfigurableProvider) {
		this.router.navigate(['/login/configurable/' + provider.configurableLoginId])
	}

	public zenodoLogin() {
		this.router.navigate(['/login/external/zenodo']);
	}

	public hasFacebookOauth(): boolean {
		return this.hasProvider(AuthProvider.Facebook);
	}

	public hasLinkedInOauth(): boolean {
		return this.hasProvider(AuthProvider.LinkedIn);
	}

	public hasTwitterOauth(): boolean {
		return this.hasProvider(AuthProvider.Twitter);
	}

	public hasGoogleOauth(): boolean {
		return this.hasProvider(AuthProvider.Google);
	}

	public hasB2AccessOauth(): boolean {
		return this.hasProvider(AuthProvider.B2Access);
	}

	public hasOrcidOauth(): boolean {
		return this.hasProvider(AuthProvider.ORCID);
	}

	public hasOpenAireOauth(): boolean {
		return this.hasProvider(AuthProvider.OpenAire);
	}

	public hasZenodoOauth(): boolean {
		return this.hasProvider(AuthProvider.Zenodo);
	}

	public initProviders() {
		if (this.hasProvider(AuthProvider.Google)) { this.initializeGoogleOauth(); }
		if (this.hasProvider(AuthProvider.Facebook)) { this.initializeFacebookOauth(); }
	}

	public hasProvider(provider: AuthProvider) {
		for (let i = 0; i < this.configurationService.loginProviders.enabled.length; i++) {
			if (provider === this.configurationService.loginProviders.enabled[i]) { return this.isProviderProperlyConfigured(provider); }
		}
		return false;
	}

	private isProviderProperlyConfigured(provider: AuthProvider) {
		switch (provider) {
			case AuthProvider.Facebook: return this.hasAllRequiredFieldsConfigured(this.configurationService.loginProviders.facebookConfiguration);
			case AuthProvider.Google: return this.hasAllRequiredFieldsConfigured(this.configurationService.loginProviders.googleConfiguration);
			case AuthProvider.LinkedIn: return this.hasAllRequiredFieldsConfigured(this.configurationService.loginProviders.linkedInConfiguration);
			case AuthProvider.Twitter: return this.hasAllRequiredFieldsConfigured(this.configurationService.loginProviders.twitterConfiguration);
			case AuthProvider.B2Access: return this.hasAllRequiredFieldsConfigured(this.configurationService.loginProviders.b2accessConfiguration);
			case AuthProvider.ORCID: return this.hasAllRequiredFieldsConfigured(this.configurationService.loginProviders.orcidConfiguration);
			case AuthProvider.OpenAire: return this.hasAllRequiredFieldsConfigured(this.configurationService.loginProviders.openAireConfiguration);
			case AuthProvider.Zenodo: return this.hasAllRequiredFieldsConfigured(this.configurationService.loginProviders.zenodoConfiguration);
			default: throw new Error('Unsupported Provider Type');
		}
	}

	private hasAllRequiredFieldsConfigured(configuration: any) {
		if (configuration != null && configuration.clientId != null) { return true; }
		return false;
	}

	/*
	* GOOGLE SIGN IN
	*/
	private initializeGoogleOauth(): void {
		gapi.load('auth2', () => {
			this.auth2 = gapi.auth2.init({
				client_id: this.configurationService.loginProviders.googleConfiguration.clientId,
				scope: 'profile email'
			});
			this.attachGoogleSignΙn(document.getElementById('googleSignInButton'));
		});
	}

	public attachGoogleSignΙn(element) {
		if (!element) { return; }
		this.auth2.attachClickHandler(element, {},
			(googleUser) => {
				const id_token = googleUser.getAuthResponse().id_token;
				if (id_token) {
					this.authService.login({ ticket: id_token, provider: AuthProvider.Google })
						.pipe(takeUntil(this._destroyed))
						.subscribe(
							res => this.loginService.onLogInSuccess(res, this.returnUrl),
							error => this.loginService.onLogInError(error)
						);
				}
			}, (error) => {
			});
	}

	/*
	* FACEBOOK SIGN IN
	*/
	private initializeFacebookOauth(): void {
		FB.init({
			appId: this.configurationService.loginProviders.facebookConfiguration.clientId,
			cookie: false,
			xfbml: true,
			version: 'v2.8'
		});
	}

	public facebookLogin() {
		FB.login((response: any) => {
			if (response.status === 'connected' || 'not_authorized') {
				this.authService.login({ ticket: response.authResponse.accessToken, provider: AuthProvider.Facebook })
					.pipe(takeUntil(this._destroyed))
					.subscribe(
						res => this.loginService.onLogInSuccess(res, this.returnUrl),
						error => this.loginService.onLogInError(error)
					);
			}
		}, { scope: 'email' });
	}

	public hasConfigurableProviders(): boolean {
		return !(this.configurableProviderService.providers == undefined) && this.configurableProviderService.providers.length > 0
	}
}
