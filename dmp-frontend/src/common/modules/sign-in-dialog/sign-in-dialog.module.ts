import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonUiModule } from '@common/ui/common-ui.module';
import { SignInDialogComponent } from './sign-in-dialog.component';

@NgModule({
	imports: [CommonUiModule, FormsModule],
	declarations: [SignInDialogComponent],
	exports: [SignInDialogComponent],
	entryComponents: [SignInDialogComponent]
})
export class SignInDialogModule {
	constructor() { }
}
