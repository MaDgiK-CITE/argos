
import {tap} from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { BaseInterceptor } from "./base.interceptor";
import { InterceptorType } from "./interceptor-type";
import { HttpHandler, HttpRequest, HttpEvent } from "@angular/common/http";
import { Observable } from "rxjs";
import { Router } from "@angular/router";
import { ConfigurationService } from '@app/core/services/configuration/configuration.service';

@Injectable()
export class StatusCodeInterceptor extends BaseInterceptor {

	type: InterceptorType;
	interceptRequest(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent <any>> {
		return next.handle(req).pipe(tap(event => { }, err => {
			if (err.status === 480) {
				this.router.navigate(['confirmation']);
			}
		}));
	}

	constructor(
		private router: Router,
		configurationService: ConfigurationService
	) { super(configurationService); }
}
