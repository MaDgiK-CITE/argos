import { HttpHandler, HttpHeaderResponse, HttpProgressEvent, HttpRequest, HttpResponse, HttpSentEvent, HttpUserEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { BaseInterceptor } from './base.interceptor';
import { InterceptorType } from './interceptor-type';
import { ConfigurationService } from '@app/core/services/configuration/configuration.service';

@Injectable()
export class ResponsePayloadInterceptor extends BaseInterceptor {

	constructor(
		private snackBar: MatSnackBar,
		configurationService: ConfigurationService
	) { super(configurationService); }

	get type(): InterceptorType { return InterceptorType.ResponsePayload; }

	interceptRequest(req: HttpRequest<any>, next: HttpHandler): Observable<HttpSentEvent | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any>> {

		return next.handle(req).pipe(
			map(response => {
				// if (!(response instanceof HttpResponse) || (response instanceof Blob)) { return response; }
				// if (response.status == 200) {
				// 	if (response.body.statusCode === ApiMessageCode.SUCCESS_MESSAGE) {
				// 		//throw new Error('Request failed');
				// 		// this.snackBar.openFromComponent(SnackBarNotificationComponent, {
				// 		// 	data: { message: response['message'], language: null },
				// 		// 	duration: 3000,
				// 		// });
				// 		return response.body.payload;

				// 	} else if (response.body.statusCode === ApiMessageCode.NO_MESSAGE) {
				// 		return response.body.payload;
				// 	} else {
				// 		return response.body.payload;
				// 	}
				// }
				return response;
			}));
	}
}
