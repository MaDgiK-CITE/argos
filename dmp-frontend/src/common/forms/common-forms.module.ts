import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PendingChangesGuard } from '@common/forms/pending-form-changes/pending-form-changes-guard.service';
import { FormService } from './form-service';

@NgModule({
	imports: [
		FormsModule,
		ReactiveFormsModule,
	],
	exports: [
		FormsModule,
		ReactiveFormsModule,
	],
	providers: [
		FormService,
		PendingChangesGuard
	]
})
export class CommonFormsModule { }
